import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from 'react-router-dom';

import { createBrowserHistory } from "history";
import ReactGA from 'react-ga';

import { PrivateRoute } from "./shared/private-route";
//import logo from './logo.svg';
//import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/style.css';
import Dashboard from "./component/Dashboard/dashboard";
import Products from "./component/Products/products";
import MyTeam from "./component/MyTeam/myteam";
//import Complaints from "./component/Complaints/complaints";
import Login from "./component/Login/login";
import QALogin from "./component/QALogin/login";
import QAForm from  "./component/QALogin/form";
import Register from "./component/Signup/register";
import Password from "./component/Signup/password";
import PasswordExpiry from "./component/Signup/password_expiry";
import ResetPassword from "./component/Signup/resetPassword";
import setPassword from "./component/Signup/setPassword";
import generalRequestForm from "./component/Requests/GeneralRequest/form";
import RequestPriceQuote from "./component/Requests/RequestPriceQuote/form"

import sampleForm from "./component/Requests/Sample/form";
import workingStandardForm from "./component/Requests/WorkingStandards/form";
import vendorQuestionnaire from "./component/Requests/VendorQuestionnaire/form";
import specAndMoa from "./component/Requests/SpecAndMoa/form";
import methodRelatedQueries from "./component/Requests/MethodQueries/form";
import typicalCoa from "./component/Requests/TypicalCoa/form";
import recertificationCoa from "./component/Requests/RecertificationCoa/form";
import auditVisitRequest from "./component/Requests/AuditVisitRequest/form";
import customizedSpecRequest from "./component/Requests/CustomizedSpecRequest/form";
import qualityEquivalanceRequest from "./component/Requests/QualityEquivalenceRequest/form";
import apostallationDoc from "./component/Requests/ApostallationDoc/form";
import qualityGeneralRequest from "./component/Requests/QualityGeneralRequest/form";

import stabilityDataRequest from "./component/Requests/StabilityDataRequest/form";
import elementalImpurityDeclaration from "./component/Requests/ElementalImpurityDeclaration/form";
import residualSolventsDeclaration from "./component/Requests/ResidualSolventsDeclaration/form";
import storageTransportDeclaration from "./component/Requests/StorageTransportDeclaration/form";
import requestAdditionalDeclarations from "./component/Requests/RequestAdditionalDeclarations/form";
import requestGenotoxicImpurityAssessment from "./component/Requests/RequestGenotoxicImpurityAssessment/form";

import requestLocLoe from "./component/Requests/RequestLocLoe/form";
import requestLoa from "./component/Requests/RequestLoa/form";
import requestCepCopy from "./component/Requests/RequestCepCopy/form";
import requestDmf from "./component/Requests/RequestDmf/form";
import regulatoryGeneralRequest from "./component/Requests/RegulatoryGeneralRequest/form"
import QueriesRelatedDMF from "./component/Requests/QueriesRelatedDMF/form"
import QueriesRelatedDeficiencies from "./component/Requests/QueriesRelatedDeficiencies/form"
import QueriesRelatedNotifications from './component/Requests/QueiresRelatedToNotification/form'
import QualityOverallSummary from "./component/Requests/QualityOverallSummary/form"

import NitroSoamine from "./component/Requests/NitroSoamine/form"
import TgaClearance from "./component/Requests/TgaClearance/form"

import cdaSalesAgreement from "./component/Requests/CdaSalesAgreement/form";
import legalGeneralRequest from "./component/Requests/LegalGeneralRequest/form";
import qualityAgreement from "./component/Requests/QualityAgreement/form";
import ipRelatedEnquiries from "./component/Requests/IPRelatedEnquiries/form"
import masterFileAgreement from "./component/Requests/MasterFileAgreement/form"

import OrderGeneralRequest from "./component/Requests/OrderGeneralRequest/form"




import orderRequest from "./component/Requests/OrderRequest/form";
import proformaInvoice from "./component/Requests/ProformaInvoice/form";

import qualityComplaints from "./component/Requests/QualityComplaints/form";
import logisticsComplaints from "./component/Requests/LogisticsComplaints/form";

import forecast from "./component/Requests/Forecast/form";

import ProductEnquiry from "./component/Requests/ProductEnquiry/form";
import ProductEnquiry2 from "./component/Requests/ProductEnquiry/form-accordian";


import myRequests from "./component/MyRequests/myRequests";
import myOrdersOld from "./component/MyOrdersOld/myOrdersOld";
import myOrders from "./component/MyOrders/myOrders";
import myEnquiries from "./component/MyEnquiries/myEnquiries";
import myProductEnquiries from "./component/MyProducts/myProductEnquiries";
import myComplaints from "./component/MyComplaints/myComplaints";
import myForecast from "./component/MyForecast/myForecast";
import myPayments from "./component/MyPayments/myPayments";
import myAccount from "./component/MyAccount/myAccount";
import productCatalog from "./component/ProductCatalog/productCatalog";
import productDisplay from "./component/ProductDisplay/productDisplay";
import myProduct from "./component/MyProducts/myProducts";
import news from "./component/News/news";
import ContactUs from "./component/ContactUs/contactus";

import taskDetails from "./component/TaskDetails/taskDetails";
import taskDetailsOne from "./component/TaskDetailsOne/taskDetailsOne";
import taskDetailsTwo from "./component/TaskDetailsTwo/taskDetailsTwo";
import shadowlogin from "./component/ShadowLogin/shadowlogin";
import stockEnquiry from "./component/StockEnquiry/stockEnquiry";
import editStockEnquiryOld from "./component/EditStockEnquiry/editStockEnquiryOld";
import editStockEnquiry from "./component/EditStockEnquiry/editStockEnquiry";
import viewProductEnquiry from "./component/MyProducts/viewProductEnquiry";

import checkHash from "./component/CheckHash/CheckHash";

import CustomerResponse from "./component/process/CustomerResponse";
// import CustomerApproval from './component/process/CustomerApproval';

import Layout from "./component/Layout/layout";
import { BasicUserData } from "./shared/helper";

import { connect } from "react-redux";
import { fetchUserData, setUserCompany } from "./store/actions/user";
import { stat } from "fs";
//import Tracker from "./tracker";
import { withRouter } from 'react-router'

class App extends Component {

  componentDidMount = () => {
    let userData = BasicUserData();
    if (userData) {
      if (userData.role === 1) { 
        this.props.fetchUser(userData, () => {
          this.setState({
            companyList: this.props.company,
            dashboardData: this.props.dashboardData
          })
        });
      } else if (userData.role === 2) {
        if (this.props.selCompany) {
          this.props.setCompany(this.props.selCompany, () => {
            this.setState({ selectedCompany: this.props.selCompany })
          })
        }
      }
    }
    
  }

  // getBaseName = () => {
  //   if(process.env.NODE_ENV === 'production'){
  //     return `new_customer_portal`
  //   }else if(process.env.NODE_ENV === 'production'){

  //   }
  // }

  render() {
    const isLoggedIn = this.props.isLoggedIn;

    //let basename = this.getBaseName();

    return (
      <Router basename={process.env.NODE_ENV === 'production' ? '/customer_portal' : ''}>
        <Layout>
          <Switch>
            <Route exact path='/login/:id' component={Login} />
            <Route exact path='/login' component={Login} />
            <Route exact path='/qa_login' component={QALogin} />
            <Route exact path='/qa_form' component={QAForm} />
            
            <Route exact path='/forgot_password' component={Password} />
            <Route exact path='/password_expired' component={PasswordExpiry} />
            <Route exact path='/register' component={Register} />
            <Route exact path='/reset_password/:token' component={ResetPassword} />
            <Route exact path='/set_password/:token' component={setPassword} />
            <Route exact path='/setToken/:token' component={shadowlogin} />

            <Route exact path='/process_customer_response/:token' component={CustomerResponse} />
            {/* <Route exact path='/process_customer_approval/:token' component={CustomerApproval} /> */}

            <PrivateRoute path='/my_requests' component={myRequests} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/my_orders_old' component={myOrdersOld} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/my_orders' component={myOrders} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/my_enquiries' component={myEnquiries} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/my_product_enquiries' component={myProductEnquiries} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/my_complaints' component={myComplaints} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/my_forecast' component={myForecast} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/my_payments' component={myPayments} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/my_products' component={myProduct} isLoggedIn={isLoggedIn} />;
                <PrivateRoute path='/dashboard' component={Dashboard} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/my-team' component={MyTeam} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/products' component={Products} isLoggedIn={isLoggedIn} />
            {/* <PrivateRoute path='/complaints' component={Complaints}/> */}
            <PrivateRoute exact path='/general-request' component={generalRequestForm} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/request_price_quote' component={RequestPriceQuote} isLoggedIn={isLoggedIn} />

            <PrivateRoute exact path='/samples-working-standards' component={workingStandardForm} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/vendor-questionnaire' component={vendorQuestionnaire} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/spec-and-moa' component={specAndMoa} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/method-related-queries' component={methodRelatedQueries} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/typical-coa' component={typicalCoa} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/recertification-of-coa' component={recertificationCoa} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/audit-visit-request' component={auditVisitRequest} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/customized-spec-request' component={customizedSpecRequest} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/quality-equivalence-request' component={qualityEquivalanceRequest} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/quality-general-request' component={qualityGeneralRequest} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/apostallation-doc' component={apostallationDoc} isLoggedIn={isLoggedIn} />

            <PrivateRoute exact path='/stability-data' component={stabilityDataRequest} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/elemental-impurity-declaration' component={elementalImpurityDeclaration} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/residual-solvents-declaration' component={residualSolventsDeclaration} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/storage-transport-declaration' component={storageTransportDeclaration} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/request-additional-declarations' component={requestAdditionalDeclarations} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/genotoxic-impurity-assessment' component={requestGenotoxicImpurityAssessment} isLoggedIn={isLoggedIn} />{/* mono */}

            <PrivateRoute exact path='/request-for-loc-loe' component={requestLocLoe} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/request-for-loa' component={requestLoa} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/request-for-cep-copy' component={requestCepCopy} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/request-for-dmf' component={requestDmf} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/regulatory-general-request' component={regulatoryGeneralRequest} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/queries-related-dmf' component={QueriesRelatedDMF} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/queries-related-deficiencies' component={QueriesRelatedDeficiencies} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/queries-related-notifications' component={QueriesRelatedNotifications} isLoggedIn={isLoggedIn} />

            <PrivateRoute exact path='/quality_overall_summary' component={QualityOverallSummary} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/nitrosoamine' component={NitroSoamine} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/tga-clearance' component={TgaClearance} isLoggedIn={isLoggedIn} />

            <PrivateRoute exact path='/cda-sales-agreement' component={cdaSalesAgreement} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/legal-general-request' component={legalGeneralRequest} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/quality-agreement' component={qualityAgreement} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/ip_related_enquiries' component={ipRelatedEnquiries} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/master_file_agreement' component={masterFileAgreement} isLoggedIn={isLoggedIn} />

            <PrivateRoute exact path='/new-order' component={orderRequest} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/order-general-request' component={OrderGeneralRequest} isLoggedIn={isLoggedIn} />

            

            <PrivateRoute exact path='/proforma-invoice' component={proformaInvoice} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/quality-complaints' component={qualityComplaints} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/logistic-complaints' component={logisticsComplaints} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/forecast' component={forecast} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/sample' component={sampleForm} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/resetpassword' component={ResetPassword} isLoggedIn={isLoggedIn} />            
            <PrivateRoute exact path='/stock-enquiry' component={stockEnquiry} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/product-enquiry' component={ProductEnquiry} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/product-enquiry-2' component={ProductEnquiry2} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/view_product_enquiry/:id' component={viewProductEnquiry} isLoggedIn={isLoggedIn} />

            <PrivateRoute exact path='/edit-stock-enquiry-old/:id' component={editStockEnquiryOld} isLoggedIn={isLoggedIn} />
            <PrivateRoute exact path='/edit-stock-enquiry/:id' component={editStockEnquiry} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/contact-us' component={ContactUs} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/my-account' component={myAccount} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/task-details/:id/' component={taskDetails} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/task-details-one/:id/' component={taskDetailsOne} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/task-details-two/:id/' component={taskDetailsTwo} isLoggedIn={isLoggedIn} />
            <PrivateRoute path='/check_hash/:id/' component={checkHash} isLoggedIn={isLoggedIn} />

            <Route path='/product-catalogue' component={productCatalog} />;
                <Route path='/product-details/:id' component={productDisplay} />;
                <Route path='/coming-soon' component={news} />;
                <Redirect from="/" to="/login" />
          </Switch>
          {/* <Tracker/>          */}
        </Layout>
      </Router>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLoggedIn: state.auth.token !== null ? true : false,
    selCompany: state.user.selCompany
  }
}

const mapDispatchToProps = dispatch => {
  return {
    fetchUser: (data, onSuccess) => dispatch(fetchUserData(data, onSuccess)),
    setCompany: (data, onSuccess) => dispatch(setUserCompany(data, onSuccess))
  }
}

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(App));
