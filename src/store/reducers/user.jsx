import {
    UPDATE_USER_IMAGE,
    REMOVE_USER_IMAGE,
    FETCH_USER_DATA,
    FETCH_USER_NOTIFICATION,
    UPDATE_USER_NOTIFICATION,
    REMOVE_USER_NOTIFICATION,
    SET_USER_COMPANY,
    REMOVE_USER_COMPANY,
    GET_TASK_STATUS,
    AUTH_LOGOUT,
    SHOW_PRE_ACTIVATION_MODAL,
    HIDE_PRE_ACTIVATION_MODAL
} from "../actions/actionTypes";

const initialState = {
    name : null,
    url  : null,
    ncounter: 0,
    nlist : [],
    company: null,
    selCompany: null,
    dashboardData:
    {
      product:0,
      request:0,
      orders:0,
      complaints:0,
      forecasts:0,
      notification:0,
      payment:0,
      discussion:0
    },
    customerList: null,
    dynamic_lang: null,
    show_pre_activation_modal: false,
    has_disabled_pre_activation_modal: false,
}

const user = ( state = initialState, action) => {

    switch( action.type ) {
        case FETCH_USER_DATA :
            return {
                ...state, 
                url: action.payload.url, 
                name: action.payload.name,
                company: action.payload.company,
                ncounter: action.payload.ncounter,
                nlist: [],
                selCompany: null,
                dashboardData: action.payload.dashboardData,
                customerList: action.payload.customerList
            }
        case GET_TASK_STATUS:      
            return { 
                ...state, 
                task_status_arr : action.payload
            };  
        case FETCH_USER_NOTIFICATION : 
            return {
                ...state, 
                notifications: action.payload,
                nlist:  action.payload
            }
        case UPDATE_USER_NOTIFICATION: 
            return { 
                ...state, 
                ncounter     : action.payload.ncounter, 
                notifications  : action.payload.notifications,
                nlist : action.payload.notifications
            }
        case UPDATE_USER_IMAGE       : 
            return { 
                ...state, 
                url : action.payload
            };
        case REMOVE_USER_IMAGE: 
            return { 
                ...state, 
                url: action.payload
            };   
        case REMOVE_USER_NOTIFICATION: 
            return { 
                ...state, 
                ncounter: action.payload
            };
        case SET_USER_COMPANY : 
            return {
                ...state, 
                selCompany: action.payload.selCompany, 
                dashboardData: action.payload.dashboardData, 
                ncounter: action.payload.ncounter,
                nlist : [],
                customerList: action.payload.customerList
            };
        case REMOVE_USER_COMPANY : 
            return { 
                ...state, 
                selCompany               : null,
                ncounter                 : 0, 
                nlist                    : [],
                dashboardData            : 
                {
                    product              : 0,
                    request              : 0,
                    orders               : 0,
                    complaints           : 0,
                    forecasts            : 0,
                    notification         : 0,
                    payment              : 0,
                    discussion           : 0
                },
                customerList: null
            }     
        case SHOW_PRE_ACTIVATION_MODAL :  
            return {
                ...state,
                show_pre_activation_modal: true
            };  
        case HIDE_PRE_ACTIVATION_MODAL :  
            return {
                ...state,
                show_pre_activation_modal: false,
                has_disabled_pre_activation_modal: true,
            }; 
        case AUTH_LOGOUT :  
            return {};
        default: 
            return state;
    }
}

export default user;