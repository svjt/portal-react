import {
    LOAD_MENU,
    SHOW_MENU,
    AUTH_LOGOUT
} from "../actions/actionTypes";


const initialState = {
    display : false
}

const menu = ( state = initialState, action) => {
    switch( action.type ) {
        case LOAD_MENU      : return {...state, display: false}
        case SHOW_MENU      : return { ...state, display: action.payload}
        case AUTH_LOGOUT    : return { ...state, display: false};
        default: return state;
    }
}

export default menu;