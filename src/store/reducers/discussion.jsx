import { 
    USER_DISCUSSION,
    ADD_DISCUSSION,
    GET_DISCUSSION_FAILURE,
    AUTH_LOGOUT
} from "../actions/actionTypes";

const initialState = {
    discussion : "",
    errors: null
}

const user = ( state = initialState, action) => {
    switch( action.type ) {
        case USER_DISCUSSION :  return { ...state, errors: null, discussion: action.payload };
        case ADD_DISCUSSION: return { ...state, errors: null, discussion:  action.payload};  
        case GET_DISCUSSION_FAILURE :  return {...state, errors: action.payload };
        case AUTH_LOGOUT :  return {};
        default: return state;
    }
}

export default user;