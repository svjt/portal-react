import {    
    AUTH_LOGOUT,
    AUTH_LOGOUT_RELOAD,
    PORTAL_LOGIN_SUCCESS,
    PORTAL_LOGIN_FAILURE,
    PORTAL_QA_LOGIN_SUCCESS,
    PORTAL_QA_LOGIN_FAILURE,
    SHOW_LOADER,
    UPDATE_TOKEN,
    LANGUAGE_SETTINGS,
    FEATURE_SETTINGS,
    UPDATE_AUTO_RATING,
    SET_LANGUAGE,
    SET_ENGLISH,
    CHANGE_LANGUAGE,
    EXPLICIT_POPUP,
    LANGUAGE_SETTINGS_EXPLICIT,
    UPDATE_AUTO_EXPLICIT_RATING,
    FA_TOOL,
    FA_MENU_TOGGLE
} from "../actions/actionTypes";

const initialState = {
    token: null,
    showLoader: false,
    dynamic_lang:require(`../../assets/lang/en.json`),
    set_lang:true,
    faSidebar:'',
    faLayout:'',
    faAcess:false,
    switch_active:false
}

const auth = ( state = initialState, action) => {
    //console.log('action ===>', action)
    switch( action.type ) {
        case AUTH_LOGOUT: return {...state, token: null, showLoader: false}
        case AUTH_LOGOUT_RELOAD: return {...state, token: null, showLoader: false}
        case SHOW_LOADER: return { ...state,  showLoader: action.payload }
        case UPDATE_TOKEN: return {...state, token: action.payload.token,dynamic_lang: action.payload.dynamic_lang,new_feature_send_mail:action.payload.new_feature_send_mail,tour_counter:action.payload.tour_counter,tour_counter_new:action.payload.tour_counter_new,tour_counter_closed_task:action.payload.tour_counter_closed_task,faAcess:action.payload.faAcess }
        case PORTAL_LOGIN_SUCCESS: return { ...state,  token: action.payload.token,dynamic_lang: action.payload.dynamic_lang,multi_lang_access:action.payload.multi_lang_access,new_feature:action.payload.new_feature,new_feature_send_mail:action.payload.new_feature_send_mail,tour_counter:action.payload.tour_counter,tour_counter_new:action.payload.tour_counter_new,tour_counter_closed_task:action.payload.tour_counter_closed_task,show_rating_popup: action.payload.show_rating_popup,showLoader: false,faSidebar:action.payload.faSidebar,faLayout:action.payload.faLayout,faAcess:action.payload.faAcess,errors: null }
        case PORTAL_QA_LOGIN_SUCCESS: return { ...state,  qa_token: action.payload.qa_token,errors: null }
        case EXPLICIT_POPUP: return { ...state, show_explicit_rating_popup: action.payload.show_explicit_rating_popup,task_id_cypher:action.payload.task_id_cypher}
        case LANGUAGE_SETTINGS: return {...state, dynamic_lang : action.payload}
        case SET_LANGUAGE: return {...state, set_lang : action.payload}
        case SET_ENGLISH: return {...state, dynamic_lang : action.payload}
        
        case CHANGE_LANGUAGE: return {...state, token: action.payload.token,dynamic_lang: action.payload.dynamic_lang}
        case FEATURE_SETTINGS: return {...state, multi_lang_access:action.payload.multi_lang_access,new_feature:action.payload.new_feature,new_feature_send_mail:action.payload.new_feature_send_mail,faAcess:action.payload.faAcess}
        case LANGUAGE_SETTINGS_EXPLICIT: return {...state, dynamic_lang : action.payload.lang_data,switch_active: action.payload.switch_active}
        case UPDATE_AUTO_RATING: return {...state, show_rating_popup : action.payload}
        case UPDATE_AUTO_EXPLICIT_RATING: return {...state, show_explicit_rating_popup : action.payload}
        case PORTAL_LOGIN_FAILURE: return { ...state,  showLoader: false, errors: action.payload }
        case PORTAL_QA_LOGIN_FAILURE: return { ...state,  showLoader: false, errors: action.payload }
        case FA_TOOL: return { ...state,  faSidebar: action.payload.faSidebar,faLayout:action.payload.faLayout,toggleFa:action.payload.toggleFa }
        case FA_MENU_TOGGLE: return { ...state,toggleFa:action.payload.toggleFa }
        default: return state;
    }
}

export default auth;