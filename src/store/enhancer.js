import { applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { createLogger } from 'redux-logger'

//const logger = createLogger();

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

// export const Enhancers = composeEnhancers(applyMiddleware( thunk, logger ));
export const Enhancers = composeEnhancers(applyMiddleware( thunk ));