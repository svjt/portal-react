import axios from "../../shared/axios";
import {
    GET_DISCUSSION_FAILURE,
    USER_DISCUSSION,
    ADD_DISCUSSION
} from "../actions/actionTypes";

export const fetchUserDiscussion = (request, onSuccess) => {
    return dispatch => {
        //console.log("overriden");
        axios.get(`/tasks/${request}/`)
        .then(res => {
            axios.get(`/tasks/update_task_highlight/${request}`);
            let discussionData = {
                discussionExists: res.data.data.discussionExists,
                discussionFiles : res.data.data.discussionFiles,
                comments : res.data.data.comments,
                taskDetails: res.data.data.taskDetails,
                taskDetailsFiles: res.data.data.taskDetailsFiles,
                taskDetailsFilesCoa: res.data.data.taskDetailsFilesCoa,
                taskDetailsFilesInvoice: res.data.data.taskDetailsFilesInvoice,
                taskDetailsFilesPacking: res.data.data.taskDetailsFilesPacking,
                shippingInfo: res.data.data.shippingInfo,
                shippingTab: res.data.data.shippingTab,
                paymentInfo: res.data.data.paymentInfo,
                downloadAllLink: res.data.data.downloadAllLink,
                trackShipment: res.data.data.trackShipment,
                preShipmentInfo: res.data.data.preShipmentInfo,
                preShipmentTab: res.data.data.preShipmentTab,
                is_report: false
            }
            dispatch({
                type: USER_DISCUSSION,
                payload: discussionData
            });
            onSuccess && onSuccess();
        })
        .catch(err => {
            dispatch({
                type: GET_DISCUSSION_FAILURE,
                payload: err
            });
            onSuccess && onSuccess();
        });
    }
}

export const submitDiscussion = (request, onSuccess) => {
   
    return dispatch => {
        
        var formData = new FormData();
        if (request.files && request.files.length > 0) {
            for (let index = 0; index < request.files.length; index++) {
                formData.append("file", request.files[index]);
            }
        } else {
            formData.append("file", []);
        }
        formData.append("task_id", request.task_id);
        formData.append("comment", request.comment);
        formData.append("owner", request.owner);
        if(request.cancelSap && request.cancelSap==true){
            formData.append("sap_cancel", 1);
        }
        

        axios.post(`/tasks/discussion`, formData)
        .then(res => {
            
            axios.get(`/tasks/${request.task_id}/`)
            .then(res => {
                let discussionData = {
                    discussionExists: res.data.data.discussionExists,
                    discussionFiles : res.data.data.discussionFiles,
                    comments : res.data.data.comments,
                    taskDetails: res.data.data.taskDetails,
                    taskDetailsFiles: res.data.data.taskDetailsFiles,
                    taskDetailsFilesCoa: res.data.data.taskDetailsFilesCoa,
                    taskDetailsFilesInvoice: res.data.data.taskDetailsFilesInvoice,
                    taskDetailsFilesPacking: res.data.data.taskDetailsFilesPacking,
                    shippingInfo: res.data.data.shippingInfo,
                    shippingTab: res.data.data.shippingTab,
                    reportIssue : request.is_report,
                }
                dispatch({
                    type: ADD_DISCUSSION,
                    payload: discussionData
                });
                onSuccess && onSuccess();
            })
            .catch(err => {
                dispatch({
                    type: GET_DISCUSSION_FAILURE,
                    payload: err
                });
                onSuccess && onSuccess();
            });
        })
        .catch(err => {
            dispatch({
                type: GET_DISCUSSION_FAILURE,
                payload: err
            });
            onSuccess && onSuccess();
        });
    }
}

export const setDiscussionTask = (request) => {
    return dispatch => {
            let discussionData = {
                taskDetails: request,
            }
            dispatch({
                type: USER_DISCUSSION,
                payload: discussionData
            });
    }
}