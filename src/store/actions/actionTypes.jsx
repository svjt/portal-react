export const AUTH_LOGOUT = 'AUTH_LOGOUT';
export const AUTH_LOGOUT_RELOAD = 'AUTH_LOGOUT_RELOAD';
export const LOADER_START = 'LOADER_START';
export const LOADER_STOP = 'LOADER_STOP';
export const UPDATE_USER_IMAGE = 'UPDATE_USER_IMAGE'
export const REMOVE_USER_IMAGE = 'REMOVE_USER_IMAGE'
export const FETCH_USER_DATA = 'FETCH_USER_DATA'
export const GET_DISCUSSION = 'GET_DISCUSSION'
export const GET_DISCUSSION_FAILURE = 'GET_DISCUSSION_FAILURE'
export const LOAD_MENU = 'LOAD_MENU'
export const SHOW_MENU = 'SHOW_MENU'
//export const USER_NOTIFICATION = 'USER_NOTIFICATION'
export const SET_USER_COMPANY = 'SET_USER_COMPANY'
export const FETCH_USER_NOTIFICATION = 'FETCH_USER_NOTIFICATION'
export const UPDATE_USER_NOTIFICATION = 'UPDATE_USER_NOTIFICATION'
export const USER_DISCUSSION = 'USER_DISCUSSION'
export const ADD_DISCUSSION = 'ADD_DISCUSSION'
export const GET_LOGGED_IN_DETAILS = 'GET_LOGGED_IN_DETAILS';
export const REMOVE_USER_NOTIFICATION = 'REMOVE_USER_NOTIFICATION'
export const PORTAL_LOGIN_SUCCESS = 'PORTAL_LOGIN_SUCCESS'
export const PORTAL_LOGIN_FAILURE = 'PORTAL_LOGIN_FAILURE'
export const SHOW_LOADER = 'SHOW_LOADER'
export const REMOVE_USER_COMPANY = 'REMOVE_USER_COMPANY'
export const UPDATE_TOKEN = 'UPDATE_TOKEN'
export const CHANGE_LANGUAGE = 'CHANGE_LANGUAGE'
export const SET_LANGUAGE = 'SET_LANGUAGE'
export const LANGUAGE_SETTINGS = 'LANGUAGE_SETTINGS'
export const LANGUAGE_SETTINGS_EXPLICIT = 'LANGUAGE_SETTINGS_EXPLICIT'
export const GET_TASK_STATUS = 'GET_TASK_STATUS'
export const UPDATE_AUTO_RATING = 'UPDATE_AUTO_RATING'
export const FEATURE_SETTINGS = 'FEATURE_SETTINGS'
export const EXPLICIT_POPUP = 'EXPLICIT_POPUP'
export const UPDATE_AUTO_EXPLICIT_RATING = 'UPDATE_AUTO_EXPLICIT_RATING'
export const SET_ENGLISH = 'SET_ENGLISH'
export const FA_TOOL = 'FA_TOOL'
export const FA_MENU_TOGGLE = 'FA_MENU_TOGGLE'
export const PORTAL_QA_LOGIN_SUCCESS = 'PORTAL_QA_LOGIN_SUCCESS'
export const PORTAL_QA_LOGIN_FAILURE = 'PORTAL_QA_LOGIN_FAILURE'
export const FEATURE_11_POPUP_SESSION = 'FEATURE_11_POPUP_SESSION'
export const SHOW_PRE_ACTIVATION_MODAL = 'SHOW_PRE_ACTIVATION_MODAL'
export const HIDE_PRE_ACTIVATION_MODAL = 'HIDE_PRE_ACTIVATION_MODAL'