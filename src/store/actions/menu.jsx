
import {
    LOAD_MENU,
    SHOW_MENU
} from "../actions/actionTypes";

export const loadSidebarMenu = (onSuccess) => {
    return dispatch => {
        dispatch({
            type: LOAD_MENU
        });
        onSuccess && onSuccess();
    }
}

export const showSidebarMenu = (data, onSuccess) => {
    return dispatch => {
        dispatch({
            type: SHOW_MENU,
            payload: !data
        });
        onSuccess && onSuccess();
    }    
}
