import axios from "../../shared/axios";
import {
    UPDATE_USER_IMAGE,
    FETCH_USER_DATA,
    AUTH_LOGOUT,
    REMOVE_USER_IMAGE,
    FETCH_USER_NOTIFICATION,
    UPDATE_USER_NOTIFICATION,
    REMOVE_USER_NOTIFICATION,
    GET_TASK_STATUS,
    SET_USER_COMPANY,
    REMOVE_USER_COMPANY,
    SHOW_PRE_ACTIVATION_MODAL,
    HIDE_PRE_ACTIVATION_MODAL
} from "../actions/actionTypes";
import { updateToken } from "./auth";

import { htmlDecode} from "../../shared/helper";

export const updateUserImage = (request, onSuccess, setErrors ) => {
    
    var formData = new FormData();
    formData.append("file", request);

    return dispatch => {

        axios
            .post("/update/pic", formData)
            .then(res => {                   
                    axios
                        .get("/generate_token")
                        .then( response => {

                            dispatch(updateToken(response.data.token))

                            dispatch({
                                type: UPDATE_USER_IMAGE,
                                payload: res.data.url
                            });

                            onSuccess && onSuccess();
                        })
                        .catch(error => {
                            dispatch({
                                type: AUTH_LOGOUT,
                                payload: error
                            });
                            setErrors(error.data.errors);
                        })
            })
            .catch(err => {
                dispatch({
                    type: AUTH_LOGOUT,
                    payload: err
                });
                setErrors(err.data.errors);
            });
    }
}

export const removeUserImage = (onSuccess, setErrors ) => {    
    return dispatch => {
        axios
            .get("/remove/pic")
            .then(res => {                                 
                    axios
                        .get("/generate_token")
                        .then( response => {                            

                            dispatch(updateToken(response.data.token))

                            dispatch({
                                type: REMOVE_USER_IMAGE,
                                payload: res.data.url
                            });

                            onSuccess && onSuccess();
                        })
                        .catch(error => {
                            dispatch({
                                type: AUTH_LOGOUT,
                                payload: error
                            });
                            setErrors(error.data.errors);
                        })
            })
            .catch(err => {
                dispatch({
                    type: AUTH_LOGOUT,
                    payload: err
                });
                setErrors(err.data.errors);
            });
    }
}

export const fetchUserData = (request, onSuccess) => {
    
    return dispatch => {
        if(request.role === 2){

            axios
            .get("/agents/company")
            .then(res => {

                var company_list = [];
                var comp_length = res.data.data.length;

                if(comp_length > 0){
                    for (let index = 0; index < comp_length; index++) {
                        const element = res.data.data[index];
                        company_list.push({
                            value: element["company_id"],
                            label: htmlDecode(element["company_name"])
                        });
                    }
                }                

                dispatch({
                    type: FETCH_USER_DATA,
                    payload: {
                        name      : request.name,   // form parsed token
                        lang      : request.lang,   // form parsed token
                        url       : request.profile_pic,  // from parsed token
                        company   : company_list,   // from API through selected company
                        ncounter  : 0,
                        dashboardData:
                        {
                            product:0,
                            request:0,
                            orders:0,
                            complaints:0,
                            forecasts:0,
                            notification:0,
                            payment:0,
                            discussion:0
                        },
                        customerList: null
                    }
                });

                onSuccess && onSuccess();
            })
            .catch(err => {
                dispatch({
                    type: AUTH_LOGOUT,
                    payload: err
                });
            });
        }else if(request.role === 1){
            axios
            .get("/dashboard")
            .then(response => {
                
                axios
                    .get("/count_notification")
                    .then(res => {

                        dispatch({
                            type: FETCH_USER_DATA,
                            payload: {
                                name         : request.name, 
                                lang         : request.lang,   // form parsed token
                                url          : request.profile_pic,
                                company      : null,
                                ncounter     : res.data.counter,
                                dashboardData: response.data.data,
                                customerList: null
                            }
                        });
                
                        onSuccess && onSuccess();
                    })
                    .catch(err => {
                        dispatch({
                            type: AUTH_LOGOUT,
                            payload: err
                        });
                    });
            
            })
            .catch(err => {
                dispatch({
                    type: AUTH_LOGOUT,
                    payload: err
                });
            });
        }
    }
}


export const fetchUserNotification = (request,  callback) => {

    if(request.role === 2){
        return dispatch => {
            axios
            .get(`/agents/list_notification/${request.company_id}`)
            .then(res => {
    
                dispatch({
                    type: FETCH_USER_NOTIFICATION,
                    payload: res.data.data
                });
        
                callback && callback();
            })
            .catch(err => {
                dispatch({
                    type: AUTH_LOGOUT,
                    payload: err
                });
            });
        }
    }else{
        return dispatch => {
            axios
            .get("/list_notification")
            .then(res => {
    
                dispatch({
                    type: FETCH_USER_NOTIFICATION,
                    payload: res.data.data
                });
        
                callback && callback();
            })
            .catch(err => {
                dispatch({
                    type: AUTH_LOGOUT,
                    payload: err
                });
            });
        }
    }
}

export const getTStatus = (request,  callback) => {

    return dispatch => {
        axios
        .get(`/tasks/get_task_status/${request.type}/${request.lang}`)
        .then(res => {

            dispatch({
                type: GET_TASK_STATUS,
                payload: res.data.data
            });
    
            callback && callback();
        })
        .catch(err => {
            dispatch({
                type: AUTH_LOGOUT,
                payload: err
            });
        });
    }
}

export const updateNotification = (request, onSuccess) => {
    return dispatch => {

        let update_notification_url = ''
        let count_notification_url = ''
        let list_notification_url = ''

        if(request.role === 2){
            update_notification_url = '/agents/update_notification'
            count_notification_url = `/agents/count_notification/${request.company_id}`
            list_notification_url = `/agents/list_notification/${request.company_id}`
        }else{
            update_notification_url = '/update_notification'
            count_notification_url = '/count_notification'
            list_notification_url = '/list_notification'
        }

        const params = {
            notification_id: request.notification_id,
            task_id: request.task_id
        }

        axios
        .put(update_notification_url, params)
        .then(res => {

            axios
            .get(count_notification_url)
            .then(cs => {

                axios
                .get(list_notification_url)
                .then( x => {

                    var notifications = x.data.data

                    dispatch({
                        type: UPDATE_USER_NOTIFICATION,
                        payload: {
                            ncounter: cs.data.counter,
                            notifications: notifications
                        }
                    });
        
                    onSuccess && onSuccess();
                })
            })
        })
        .catch(err => {
            dispatch({
                type: AUTH_LOGOUT,
                payload: err
            });
        });
    }
}

export const removeUserNotification = (onSuccess) => {
    return dispatch => {        
        dispatch({
            type: REMOVE_USER_NOTIFICATION,
            payload: 0
        });
        onSuccess && onSuccess();
    }
}

export const setUserCompany = ( request, onSuccess ) => {
    return dispatch => {

        if(request === null){
            dispatch({
                type: REMOVE_USER_COMPANY,
                payload: null
            });
            onSuccess && onSuccess();
        }else{

            // call dashboard data
            axios
            .get(`/dashboard?cid=${request.value}`)
            .then(res => {
                axios
                .get(`/agents/count_notification/${request.value}`)
                .then(response => {
                    axios
                    .get(`/agents/customers?cid=${request.value}`)
                    .then(cust => {
                        var customer_list = [];
                        var customer_arr = cust.data.data;

                        for (let index = 0; index <customer_arr.length; index++) {
                            const element =customer_arr[index];
                            customer_list.push({
                                value: element["customer_id"],
                                label: htmlDecode(element["cust_name"])
                            });
                        }

                        dispatch({
                            type: SET_USER_COMPANY,
                            payload: {
                                selCompany: request,
                                dashboardData: res.data.data,
                                ncounter : response.data.counter,
                                customerList: customer_list
                            }
                        });
                        onSuccess && onSuccess();
                    })
                    .catch(err => {
                        dispatch({
                            type: AUTH_LOGOUT,
                            payload: err
                        });
                    });
                })
                .catch(err => {
                    dispatch({
                        type: AUTH_LOGOUT,
                        payload: err
                    });
                });
            })
            .catch(err => {
                dispatch({
                    type: AUTH_LOGOUT,
                    payload: err
                });
            });
        }
    }
}

export const showPreActivationModal = () => {
    return dispatch => {
        dispatch({
            type: SHOW_PRE_ACTIVATION_MODAL,
        });
    }
}

export const hidePreActivationModal = () => {
    return dispatch => {
        dispatch({
            type: HIDE_PRE_ACTIVATION_MODAL,
        });
    }
}