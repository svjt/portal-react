import axios from "../../shared/axios";
import {persistor} from '../../store/createStore';
import jwt_decode from 'jwt-decode';

import {
    AUTH_LOGOUT,
    AUTH_LOGOUT_RELOAD,
    PORTAL_LOGIN_SUCCESS,
    PORTAL_LOGIN_FAILURE,
    PORTAL_QA_LOGIN_SUCCESS,
    PORTAL_QA_LOGIN_FAILURE,
    SHOW_LOADER,
    UPDATE_TOKEN,
    LANGUAGE_SETTINGS,
    CHANGE_LANGUAGE,
    SET_LANGUAGE,
    SET_ENGLISH,
    FEATURE_SETTINGS,
    LANGUAGE_SETTINGS_EXPLICIT,
    UPDATE_AUTO_RATING,
    EXPLICIT_POPUP,
    UPDATE_AUTO_EXPLICIT_RATING,
    FA_TOOL,
    FA_MENU_TOGGLE,
    FEATURE_11_POPUP_SESSION
} from "../actions/actionTypes";

import { getUserLanguage } from "../../shared/helper";

export const authLogout = () => {
    persistor.flush();
    persistor.purge();
    return {
        type: AUTH_LOGOUT
    }
}

export const authLogoutReload = (request, onSuccess) => {
    persistor.flush();
    persistor.purge();
    //window.location.reload;
    onSuccess && onSuccess();
    return {
        type: AUTH_LOGOUT_RELOAD
    }
}

export const portalLogin = (request, onSuccess, onError) => {
    
    return dispatch => {

        dispatch({
            type: SHOW_LOADER,
            payload: true
        });

        axios
            .post("/login", {"username": request.username,"password": request.password})
            .then(res => {
                localStorage.clear();
                let token = jwt_decode(res.data.token);
                let token_lang = token.lang;
                let new_feature = (token.new_feature == 1)?true:false;
                let new_feature_fa = (token.new_feature_fa == 1)?true:false;
                let new_feature_send_mail = (typeof token.new_feature_send_mail == 'undefined')?token.new_feature_send_mail:(token.new_feature_send_mail == 1)?true:false;
                let new_feature_11 = (typeof token.new_feature_11 == 'undefined')?token.new_feature_11:(token.new_feature_11 == 1)?true:false;
                let multi_lang_access = (token.multi_lang_access == 1)?true:false;
                let show_stock_enquiry = (token.show_stock_enquiry!= undefined && token.show_stock_enquiry==1) ? true : false;
                let role_finance = (token.role_finance!= undefined && token.role_finance==true) ? true : false;

                let tour_counter = (token.tour_counter >3)?false:true;
                let tour_counter_new = (token.tour_counter_new >3)?false:true;
                let tour_counter_11 = (token.tour_counter_11 >2)?false:true;
                let tour_counter_closed_task = (token.tour_counter_closed_task >3)?false:true; 
                
                let pre_activated = token.pre_activated !== undefined ? token.pre_activated : '';

                let lang_data = '';
                if(token_lang && token_lang !== null){
                    lang_data = require(`../../assets/lang/${token_lang}.json`);
                }else{
                    lang_data = require(`../../assets/lang/en.json`);
                }

                let payload = {
                    token:res.data.token,dynamic_lang:lang_data,show_rating_popup:res.data.show_rating,new_feature:new_feature,new_feature_send_mail:new_feature_send_mail,new_feature_11:new_feature_11,multi_lang_access:multi_lang_access,tour_counter_new:tour_counter_new,tour_counter_11:tour_counter_11,tour_counter:tour_counter,tour_counter_closed_task:tour_counter_closed_task,faSidebar:'',faLayout:'',faAcess:new_feature_fa,show_stock_enquiry:show_stock_enquiry,
                    role_finance:role_finance,pre_activated
                }
                
                dispatch({
                    type: PORTAL_LOGIN_SUCCESS,
                    payload: payload
                });

                onSuccess && onSuccess();                        
            })
            .catch(err => {
                dispatch({
                    type: PORTAL_LOGIN_FAILURE,
                    payload: err.data
                });
                onError && onError();
            });
    }
}

export const portalQALogin = (request, onSuccess, onError) => {
    
    return dispatch => {

        dispatch({
            type: SHOW_LOADER,
            payload: true
        });

        axios
            .post("/qa_login", {"username": request.username,"password": request.password})
            .then(res => {
                
                dispatch({
                    type: PORTAL_QA_LOGIN_SUCCESS,
                    payload: {qa_token:res.data.token}
                });

                onSuccess && onSuccess();                        
            })
            .catch(err => {
                dispatch({
                    type: PORTAL_QA_LOGIN_FAILURE,
                    payload: err.data
                });
                onError && onError();
            });
    }
}

export const setExplicitPopup = (request, onSuccess, onError) => {
    
    return dispatch => {

        dispatch({
            type: SHOW_LOADER,
            payload: true
        });

        dispatch({
            type: EXPLICIT_POPUP,
            payload: {show_explicit_rating_popup:true,task_id_cypher:request.task_id_cypher}
        });
    }
}

export const updateToken = ( request ) => {
    return dispatch => {

        let token = jwt_decode(request);
        let token_lang = token.lang;
        let lang_data = '';
        if(token_lang && token_lang !== null){
            lang_data = require(`../../assets/lang/${token_lang}.json`);
        }else{
            lang_data = require(`../../assets/lang/en.json`);
        }

        let new_feature_fa = (token.new_feature_fa == 1)?true:false;
        let new_feature_send_mail = (typeof token.new_feature_send_mail == 'undefined')?token.new_feature_send_mail:(token.new_feature_send_mail == 1)?true:false;
        let new_feature_11 = (typeof token.new_feature_11 == 'undefined')?token.new_feature_11:(token.new_feature_11 == 1)?true:false;

        let tour_counter = (token.tour_counter >3)?false:true;
        let tour_counter_new = (token.tour_counter_new >3)?false:true;
        let tour_counter_11 = (token.tour_counter_11 >2)?false:true;
        let tour_counter_closed_task = (token.tour_counter_closed_task >3)?false:true;
        let show_stock_enquiry = (token.show_stock_enquiry!= undefined && token.show_stock_enquiry==1) ? true : false;
        let role_finance = (token.role_finance!= undefined && token.role_finance==true) ? true : false;

        let pre_activated = token.pre_activated !== undefined ? token.pre_activated : '';

        const payload = {
            token:request,dynamic_lang:lang_data,new_feature_send_mail:new_feature_send_mail,new_feature_11:new_feature_11,tour_counter:tour_counter,tour_counter_new:tour_counter_new,tour_counter_11:tour_counter_11,tour_counter_closed_task:tour_counter_closed_task,faAcess:new_feature_fa,show_stock_enquiry:show_stock_enquiry,
            role_finance: role_finance,pre_activated
        }
        dispatch({
            type: UPDATE_TOKEN,
            payload: payload
        });
    }
}

export const feature11PopupSession = () => {
    let tour_counter_11 = false;
    return dispatch => {
        dispatch({
            type: FEATURE_11_POPUP_SESSION,
            payload: {tour_counter_11:tour_counter_11}
        });
    }
}

export const hideRatingPopup = () => {
    return dispatch => {
        dispatch({
            type: UPDATE_AUTO_RATING,
            payload: false
        });
    }
}

export const faHideLeftMenu = () => {
    let faSidebar = '';
    let faLayout  = '';
    let toggleFa = true;
    if(window.location.pathname === "/product-enquiry" || window.location.pathname === "/customer_portal/product-enquiry" || window.location.pathname === "/new_customer_portal/product-enquiry"){
        faSidebar = 'fa-sidebar';
        faLayout  = 'fa-layout';
        toggleFa = false;
    }

    return dispatch => {
        dispatch({
            type: FA_TOOL,
            payload: {faSidebar:faSidebar,faLayout:faLayout,toggleFa:toggleFa}
        });
    }
}

export const onFAToggleRedux = () => {
    let isClassExist = document.querySelector(".menu-left-sec").classList.contains("fa-sidebar");
    let toggleFa;
    if(isClassExist){
      document.querySelector(".menu-left-sec").classList.remove("fa-sidebar");
      document.querySelector("#themecontent_body").classList.remove("fa-layout");
      toggleFa = true;
    }else{
      document.querySelector(".menu-left-sec").classList.add("fa-sidebar");
      document.querySelector("#themecontent_body").classList.add("fa-layout");
      toggleFa = false;
    }
    
    return dispatch => {
        dispatch({
            type: FA_MENU_TOGGLE,
            payload: {toggleFa:toggleFa}
        });
    }

}

export const hideExplicitRatingPopup = () => {
    return dispatch => {
        dispatch({
            type: UPDATE_AUTO_EXPLICIT_RATING,
            payload: false
        });
    }
}

export const ChangeToEnglish = (request, onSuccess,setErrors) => {
    return dispatch => {
        axios
        .post("/change_language")
        .then( response => {

            if(response.data.status === 1){

                axios
                .get("/generate_token")
                .then( res => {

                    let token = jwt_decode(res.data.token);
                    let token_lang = token.lang;
                    let lang_data = '';
                    if(token_lang && token_lang !== null){
                        lang_data = require(`../../assets/lang/${token_lang}.json`);
                    }else{
                        lang_data = require(`../../assets/lang/en.json`);
                    }

                    dispatch({
                        type    : CHANGE_LANGUAGE,
                        payload : {token:res.data.token,dynamic_lang:lang_data}
                    });
                    if(res.data.token != ''){
                        onSuccess && onSuccess(1);
                    }
                    
                })
                .catch(error => {
                    dispatch({
                        type: AUTH_LOGOUT,
                        payload: error
                    });
                    setErrors(error.data.errors);
                });
            }
        })
        .catch(error => {
            dispatch({
                type: AUTH_LOGOUT,
                payload: error
            });
            setErrors(error.data.errors);
        })
    }
}

export const setLanguage = (request, onSuccess,setErrors) => {
    return dispatch => {

        dispatch({
            type: SET_LANGUAGE,
            payload: false
        });

        onSuccess && onSuccess();

    }
}



export const refreshToken = ( request, onSuccess,setErrors ) => {

    return dispatch => {
        axios
        .get("/generate_token")
        .then( response => {

            dispatch(updateToken(response.data.token))

            let token_lang = getUserLanguage();
            let lang_data = '';
            if(token_lang !== null){
                sessionStorage.website_lang = token_lang;
                lang_data = require(`../../assets/lang/${token_lang}.json`);
            }else{
                sessionStorage.website_lang ='en';
                lang_data = require(`../../assets/lang/en.json`);
            }

            dispatch({
                type    : LANGUAGE_SETTINGS,
                payload : lang_data
            });

            onSuccess && onSuccess();
        })
        .catch(error => {
            dispatch({
                type: AUTH_LOGOUT,
                payload: error
            });
            setErrors(error.data.errors);
        })
    }
}

export const refreshFeatureToken = ( request, onSuccess,setErrors ) => {

    return dispatch => {
        axios
        .get("/generate_token")
        .then( response => {

            dispatch(updateToken(response.data.token))

            localStorage.clear();
            let token = jwt_decode(response.data.token);

            let new_feature = (token.new_feature == 1)?true:false;
            let new_feature_fa = (token.new_feature_fa == 1)?true:false;
            let new_feature_send_mail = (typeof token.new_feature_send_mail == 'undefined')?token.new_feature_send_mail:(token.new_feature_send_mail == 1)?true:false;
            let new_feature_11 = (typeof token.new_feature_11 == 'undefined')?token.new_feature_11:(token.new_feature_11 == 1)?true:false;
            let multi_lang_access = (token.multi_lang_access == 1)?true:false;

            dispatch({
                type    : FEATURE_SETTINGS,
                payload : {new_feature:new_feature,new_feature_send_mail:new_feature_send_mail,new_feature_11:new_feature_11,multi_lang_access:multi_lang_access,faAcess:new_feature_fa}
            });

            onSuccess && onSuccess();
        })
        .catch(error => {
            dispatch({
                type: AUTH_LOGOUT,
                payload: error
            });
            setErrors(error.data.errors);
        })
    }
}

export const getLanguage = (onSuccess) => {
    return dispatch => {        

        let token_lang = getUserLanguage();
        let lang_data = '';
        if(token_lang !== null){
            lang_data = require(`../../assets/lang/${token_lang}.json`);
        }else{
            lang_data = require(`../../assets/lang/en.json`);
        }
        dispatch({
            type    : LANGUAGE_SETTINGS,
            payload : lang_data
        });
        onSuccess && onSuccess();
    }
}

export const featureLangAccess = (request) => {
    return dispatch => {        
        let token = jwt_decode(request);
        let multi_lang_access = (token.multi_lang_access == 1)?true:false;
        let new_feature_send_mail = (typeof token.new_feature_send_mail == 'undefined')?token.new_feature_send_mail:(token.new_feature_send_mail == 1)?true:false;
        let new_feature_11 = (typeof token.new_feature_11 == 'undefined')?token.new_feature_11:(token.new_feature_11 == 1)?true:false;
        dispatch({
            type    : FEATURE_SETTINGS,
            payload : {new_feature:false,new_feature_send_mail:new_feature_send_mail,new_feature_11:new_feature_11,multi_lang_access:multi_lang_access}
        });
    }
}

export const setEnglishLang = (request, onSuccess,setErrors) => {
    return dispatch => {

        let lang_data = require(`../../assets/lang/en.json`);
        localStorage.clear();

        dispatch({
            type: SET_ENGLISH,
            payload: lang_data
        });

        onSuccess && onSuccess();

    }
}
