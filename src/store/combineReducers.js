import { combineReducers } from "redux";

// Import reducer files
import Auth from "./reducers/auth";
import User from "./reducers/user";
import Discussion from "./reducers/discussion";
import Menu from "./reducers/menu";

export const AppCombineReducers = combineReducers({
    auth: Auth,
    user: User,
    discussion: Discussion,
    menu: Menu
});