import React from 'react';
import { withRouter } from 'react-router'
import { BasicUserData } from "./shared/helper";

class Tracker extends React.Component {

    componentWillReceiveProps = () => {
        console.log('props received')
        let userData = BasicUserData();
        var _paq = window._paq || [];
        if(userData){
            _paq.push(['setUserId', userData.email]);        
        }else{
            _paq.push(['resetUserId']);        
        }
        _paq.push(['setDocumentTitle', document.title]);
        _paq.push(['setCustomUrl', this.props.location.pathname]);
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        
        (function() {
            var u=`${process.env.REACT_APP_MATOMO_URL}`;
            _paq.push(['setTrackerUrl', u+'matomo.php']);
            _paq.push(['setSiteId', `${process.env.REACT_APP_MATOMO_SITE_ID}`]);
            var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
            console.log('===== d ==========>', d)
            console.log('===== s ==========>', s)
            g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
            
            console.log('===== g ==========>', g)
        })();

        window._paq = _paq;

        //console.log( 'paq', window._paq)
    }

    render() {
      return null
    }
  }
  
  export default withRouter(Tracker)