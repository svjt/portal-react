const passport       = require('passport');
const JwtStrategy    = require('passport-jwt').Strategy;
const { ExtractJwt } = require('passport-jwt');
const LocalStrategy  = require('passport-local').Strategy;
const bcrypt         = require('bcryptjs');
const Entities       = require('html-entities').AllHtmlEntities;
const entities       = new Entities();
const Config         = require('./configuration/config');
const Cryptr         = require('cryptr');
const cryptr         = new Cryptr(Config.cryptR.secret);
const Adm            = require('./models/adm');
const empObj         = require('./models/employees');


  isValidPassword = async function(newPassword,existingPassword) {
    try {
      return await bcrypt.compare(newPassword,existingPassword);
    } catch(error) {
      throw new Error(error);
    }
  }


  passport.use('jwtAdm',new JwtStrategy({

    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: Config.jwt.secret
  }, async (payload, done) => {

    try { 
      if(!payload.admin){
        return done(null, {id:0});
      }
    
      if(!payload.sub){
        return done(null, {id:0});
      }
      if(!payload.ag){
        return done(null, {id:0});
      }
      if(!payload.exp){
        return done(null, {id:0});
      }else{
        var current_time = Math.round((new Date().getTime())/1000);
        if(current_time>payload.exp){
          return done(null, {id:0});
        }
      }
      
      const user = await Adm.findByAdminId(cryptr.decrypt(payload.sub));
      //CHECK POSTED GROUP == CURRENT GROUP
      //console.log('payload===>',payload,'user===>',user);
      
      if (user.length>0) {
        if(payload.gp != user[0].group_id){
          return done(null, {id:0});
        }else if(payload.sa != user[0].super_admin){
          return done(null, {id:0});
        }else{
          user[0].ag = cryptr.decrypt(payload.ag);
          done(null, user[0]);
        }
      }else{
        return done(null, {id:0});
      }

    } catch(error) {
      done(null, user[0]);
    }
  }));

  passport.use('jwtEmp',new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: Config.jwt.secret
  }, async (payload, done) => {

    try { 

      if(!payload.did){
        return done(null, {id:0});
      }
      if(!payload.sid){
        return done(null, {id:0});
      }
      if(!payload.ag){
        return done(null, {id:0});
      }
      if(!payload.exp){
        return done(null, {id:0});
      }else{
        var current_time = Math.round((new Date().getTime())/1000);
        if(current_time>payload.exp){
          return done(null, {id:0});
        }
      }

      //const user = await empObj.findById(cryptr.decrypt(payload.did)); 
      const user = await empObj.findById(payload.did);
      if (user.length>0) {
        user[0].ag = cryptr.decrypt(payload.ag);
        done(null, user[0]);
      }else{
        return done(null, {id:0});
      }

    } catch(error) {
      done(null, user[0]);
    }
  }));

  
  passport.use('localAdm',new LocalStrategy(
    async (username, password, done) => {
      try {

        const user = await Adm.findByUsername(entities.encode(username));
        if (user.length>0) {

          const isMatch = await isValidPassword(password,user[0].password);
          if (!isMatch) {
            return done(null, {id:0});
          }
          done(null, user[0]);
        }else{
          return done(null, {id:0});
        }
        
      } catch(error) {
        done(error, false);
      }
    }
  ));

  passport.use('localEmp',new LocalStrategy({
    usernameField: 'email'
  },async (email, password, done) => {
      try {

        const user = await empObj.findByEmail(entities.encode(email));
        if (user.length>0) {

          const isMatch = await isValidPassword(password,user[0].password);

          if (!isMatch) {
            return done(null, {id:0});
          }
          done(null, user[0]);
        }else{
          return done(null, {id:0});
        }
        
      } catch(error) {
        done(error, false);
      }
    }
  ));

  passport.use('outlook',new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: Config.jwt.secret
  }, async (payload, done) => {

    try { 

      if(!payload.iss){
        return done(null, {id:0});
      }else{
        done(null, 1);
      }
    } catch(error) {
      done(null, false);
    }
  })); 

  passport.use('crm',new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: Config.jwt.secret
  }, async (payload, done) => {

    try { 

      if(!payload.iss){
        return done(null, {id:0});
      }
      if(!payload.exp){
        return done(null, {id:0});
      }else{
        var current_time = Math.round((new Date().getTime())/1000);
        if(current_time>payload.exp){
          return done(null, {id:0});
        }
      }
      if(!payload.crm || payload.crm != 1){
        return done(null, {id:0});
      }else{
        done(null, 1);
      }
    } catch(error) {
      done(null, false);
    }
  })); 
  passport.use('wgos',new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: Config.jwt.secret
  }, async (payload, done) => {

    try { 

      if(!payload.iss){
        return done(null, {id:0});
      }
      if(!payload.exp){
        return done(null, {id:0});
      }else{
        var current_time = Math.round((new Date().getTime())/1000);
        if(current_time>payload.exp){
          return done(null, {id:0});
        }
      }
      if(!payload.wgos || payload.wgos != 1){
        return done(null, {id:0});
      }else{
        done(null, 1);
      }
    } catch(error) {
      done(null, false);
    }
  })); 