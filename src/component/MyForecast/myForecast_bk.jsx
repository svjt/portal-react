import React, { Component } from "react";
import { Link } from "react-router-dom";
import Layout from "../Layout/layout";
import axios from "../../shared/axios";
import { htmlDecode, BasicUserData, checkEmployee } from "../../shared/helper";
import Pagination from "react-js-pagination";
import { Row, Col, ButtonToolbar, Button, Modal } from "react-bootstrap";
import { Formik, Form } from "formik";
import { Tooltip } from "reactstrap";
import infoIcon from "../../assets/images/info-circle-solid.svg";
import closeIconSelect from "../../assets/images/close.svg";
import Loader from "../Loader/loader";
import Autosuggest from "react-autosuggest";
import Select from "react-select";
import { isMobile } from "react-device-detect";
import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

// SATYAJIT
import { connect } from "react-redux";
import { getTStatus } from "../../store/actions/user";

import Rating from "react-rating";
// SATYAJIT

var message = "";
class MyForecast extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    message = this.props.location.state
      ? this.props.location.state.message
      : "";
  }

  state = {
    requestsList: [],
    ccList: [],
    showCcPopup: false,
    activePage: 1,
    totalCount: 0,
    itemPerPage: 10,
    tooltipOpen: false,
    searchkey: "",
    finalchecked: [],
    task_id: 0,
    showLoader: false,
    isLoading: true,
    value: "", // for auto-suggest
    suggestions: [], // for auto-suggest
    selectedCompany: null,
    shared_with_agent: 0,
    shared_with_agent_show: false,
    openMobileView: false,
    selectedFrom: "",
    selectedTo: "",
    displayMobileText: "",
    rating_err: "",
    showRRPopup: false,
    showPendingPopup: false,
    showThankyouPopup: false,
    pendingRatings: false,
    task_rating: 0,
    options_text: "",
    question_text: "",
    ratingHtml: "",
    ratingHtml2: "",
    selOptions: [],
    pending_requests: [],
    ratingComment: "",
    tooltipOpen2: false,
    popup_lang: {},
  };
  componentDidMount() {
    if (isMobile) {
      const datePickers = document.getElementsByClassName(
        "react-datepicker__input-container"
      );
      //console.log('datePickers', datePickers)
      Array.from(datePickers).forEach((el) =>
        el.childNodes[0].setAttribute("readOnly", true)
      );
    }
    this.getRequests();

    if (message !== null) {
      this.props.history.push({
        pathname: "",
        state: "",
      });
    }
    document.title =
      this.props.dynamic_lang.dashboard.m_forecasts + " | Dr.Reddys API";

    this.state.selectedFrom = "";
    this.state.selectedTo = "";
    this.state.fromMaxDate = new Date(
      new Date().setFullYear(new Date().getFullYear() - 10)
    );
    this.state.toMaxDate = new Date(
      new Date().setFullYear(new Date().getFullYear() + 10)
    );

    let userData = BasicUserData();
    if (userData) {
      this.setState({
        isLoading: false,
      });
    }
  }

  toggle2 = () => {
    this.setState({
      tooltipOpen2: !this.state.tooltipOpen2,
    });
  };

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    var searchValue = "";
    if (this.state.searchkey != "") searchValue = this.state.searchkey;
    this.getRequests(
      pageNumber > 0 ? pageNumber : 1,
      searchValue,
      null,
      this.state.query_string
    );
  };

  handleSearchChange = (event) => {
    this.setState({ searchkey: event.target.value });
  };

  enterPressed = (event) => {
    var code = event.keyCode || event.which;
    if (code === 13) {
      //13 is the enter keycode
      this.setState({
        searchkey: event.target.value,
      });
      this.getRequests(1, event.target.value, null, this.state.query_string);
    }
  };

  getRequests(page = 1, search = "", sel_evnt = null, manual_search = "") {
    this.setState({
      isLoading: true,
    });
    let userData = BasicUserData();
    if (userData.role === 2) {
      if (sel_evnt != null) {
        this.setState({ selectedCompany: sel_evnt });
      }

      if (this.props.selCompany && this.props.selCompany.value > 0) {
        let url = `/forecast-list?cid=${this.props.selCompany.value}&page=${page}`;

        if (search != "") {
          url += `&s=${search}`;
        }
        if (manual_search != "") {
          url += `&${manual_search}`;
        }

        axios
          .get(url)
          .then((res) => {
            //console.log('data',res.data.data)
            this.setState({
              requestsList: res.data.data,
              count_req: res.data.count,
              isLoading: false,
            });
          })
          .catch((err) => { });
      } else {
        this.setState({ isLoading: false });
      }
    } else {
      let url = `/forecast-list?page=${page}`;
      if (search != "") {
        url += `&s=${search}`;
      }
      if (manual_search != "") {
        url += `&${manual_search}`;
      }

      axios
        .get(url)
        .then((res) => {
          //console.log('data',res.data.data)
          this.setState({
            requestsList: res.data.data,
            count_req: res.data.count,
            isLoading: false,
          });
        })
        .catch((err) => { });
    }
  }

  showCCList = (task_id, task_language) => {
    this.setState({ ccList: [], finalchecked: [], cc_lang: '' });
    let lang_data = require(`../../assets/lang/${task_language}.json`);
    let final_array = [];
    let userData = BasicUserData();

    axios
      .get(`tasks/get-cc-customers/${task_id}`)
      .then((res) => {
        this.setState({ ccList: res.data.data, showCcPopup: true, cc_lang: lang_data });
        if (res.data.data.selCCCustList.length > 0) {
          for (
            let index = 0;
            index < res.data.data.selCCCustList.length;
            index++
          ) {
            const selected = res.data.data.selCCCustList[index];
            final_array.push(selected.customer_id.toString());
          }
        }

        this.setState({ finalchecked: final_array });
        this.setState({ task_id: task_id });
      })
      .catch((err) => { });

    if (userData.role !== 2) {
      this.setState({ shared_with_agent: 0, shared_with_agent_show: false });

      axios
        .get(`/tasks/${task_id}`)
        .then((res) => {
          this.setState({
            shared_with_agent: res.data.data.taskDetails.share_with_agent,
          });
          if (res.data.data.taskDetails.submitted_by > 0) {
          } else {
            this.setState({ shared_with_agent_show: true });
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  makeDynamicHtml = () => {
    if (
      this.state.ccList !== "" &&
      this.state.ccList.ccCutomerList.length > 0
    ) {
      let elements = this.state.ccList.ccCutomerList.map((element) => {
        let checked = false;
        if (
          this.state.ccList.selCCCustList &&
          this.state.ccList.selCCCustList.length > 0
        ) {
          for (
            let index = 0;
            index < this.state.ccList.selCCCustList.length;
            index++
          ) {
            const selected = this.state.ccList.selCCCustList[index];
            if (selected.customer_id === element.customer_id) {
              checked = true;
            }
          }
        }
        return (
          <li key={element.customer_id} style={{ listStyleType: "none" }}>
            <input
              type="checkbox"
              defaultChecked={checked}
              name={`checkCC_${element.customer_id}`}
              id={`checkCC_${element.customer_id}`}
              onChange={(e) => {
                this.toggleCheckbox(e);
              }}
              value={element.customer_id}
            />
            &nbsp;{element.first_name} {element.last_name} (
            {element.company_name})
          </li>
        );
      });
      return elements;
    } else {
      return "";
    }
  };
  handleSubmitAssignTask = (values, actions) => {
    //console.log("Values",this.state.finalchecked);
    this.setState({ showLoader: true });
    let userData = BasicUserData();
    if (userData.role != 2) {
      axios
        .post("tasks/update-cc", {
          task_id: this.state.task_id,
          cc: this.state.finalchecked,
          share_with_agent: this.state.shared_with_agent,
        })
        .then((res) => {
          this.setState({ showLoader: false });
          this.handleClose();
          this.props.history.push({
            pathname: "/my_forecast",
          });
        })
        .catch((err) => { });
    } else {
      axios
        .post("tasks/update-cc", {
          task_id: this.state.task_id,
          cc: this.state.finalchecked,
        })
        .then((res) => {
          this.setState({ showLoader: false });
          this.handleClose();
          this.props.history.push({
            pathname: "/my_forecast",
          });
        })
        .catch((err) => { });
    }
  };

  setRatedTooltip = (request) => {
    let lang_data = require(`../../assets/lang/${request.task_language}.json`);
    let html = "";
    if (request.rated_by_type === 2) {
      if (request.rated_by == request.submitted_by) {
        html = null;
      } else {
        html = (
          <>
            <img
              src={infoIcon}
              width="15.44"
              height="18"
              id={`DisabledAutoHideExampleRating_${request.task_id}`}
              type="button"
              className={`itooltip`}
            />
            <Tooltip
              placement="left"
              isOpen={this.state[request.task_id]}
              autohide={false}
              target={`DisabledAutoHideExampleRating_${request.task_id}`}
              toggle={() => {
                this.setState({
                  [request.task_id]: !this.state[request.task_id],
                });
              }}
            >
              {lang_data.my_requests.feedback_given_by}{" "}
              {request.first_name_rated_by_agent}{" "}
              {request.last_name_rated_by_agent}{" "}
              {lang_data.my_requests.on_behalf_of}{" "}
              {request.first_name} {request.last_name}
            </Tooltip>
          </>
        );
      }
    } else if (request.rated_by_type === 1) {
      if (request.rated_by == request.customer_id) {
        html = null;
      } else {
        html = (
          <>
            <img
              src={infoIcon}
              width="15.44"
              height="18"
              id={`DisabledAutoHideExampleRating_${request.task_id}`}
              type="button"
              className={`itooltip`}
            />
            <Tooltip
              placement="right"
              isOpen={this.state[request.task_id]}
              autohide={false}
              target={`DisabledAutoHideExampleRating_${request.task_id}`}
              toggle={() => {
                this.setState({
                  [request.task_id]: !this.state[request.task_id],
                });
              }}
            >
              {lang_data.my_requests.feedback_given_by}{" "}
              {request.first_name_rated_by} {request.last_name_rated_by}{" "}
              {lang_data.my_requests.on_behalf_of}{" "}
              {request.first_name} {request.last_name}
            </Tooltip>
          </>
        );
      }
    }

    return html;
  };

  getRatings = (request) => {
    let lang_data = require(`../../assets/lang/${request.task_language}.json`);
    if (request.close_status == 1) {
      if (request.rating_skipped == 0) {
        if (request.rating > 0) {
          return (
            <>
              <Rating
                emptySymbol={
                  <img
                    src={require("../../assets/images/uncheck-star.svg")}
                    alt=""
                    className="icon"
                  />
                }
                placeholderSymbol={
                  <img
                    src={require("../../assets/images/uncheck-stars.svg")}
                    alt=""
                    className="icon"
                  />
                }
                fullSymbol={
                  <img
                    src={require("../../assets/images/uncheck-stars.svg")}
                    alt=""
                    className="icon"
                  />
                }
                initialRating={request.rating}
                readonly
              />
              {request.rated_by > 0 ? this.setRatedTooltip(request) : null}
            </>
          );
        } else {
          if (checkEmployee() == "") {
            return (
              <>
                <a
                  id={`DisabledAutoHideExampleRatingToolTIp_${request.task_id}`}
                  href="#"
                  style={{ textDecoration: "none" }}
                  onClick={(e) =>
                    this.showRatingPopup(
                      e,
                      request.task_id,
                      request.task_ref,
                      request.req_name,
                      request.task_language
                    )
                  }
                >
                  {lang_data.my_requests.share_feedback}
                </a>

                <Tooltip
                  placement="left"
                  isOpen={this.state[request.task_id]}
                  autohide={false}
                  target={`DisabledAutoHideExampleRatingToolTIp_${request.task_id}`}
                  toggle={() => {
                    this.setState({
                      [request.task_id]: !this.state[request.task_id],
                    });
                  }}
                >
                  {lang_data.my_requests.share_feedback_tt}
                </Tooltip>
              </>
            );
          } else {
            return null;
          }
        }
      } else {
        if (checkEmployee() == "") {
          return (
            <>
              <a
                id={`DisabledAutoHideExampleRatingToolTIp_${request.task_id}`}
                href="#"
                style={{ textDecoration: "none" }}
                onClick={(e) =>
                  this.showRatingPopup(
                    e,
                    request.task_id,
                    request.task_ref,
                    request.req_name,
                    request.task_language
                  )
                }
              >
                {lang_data.my_requests.share_feedback}
              </a>

              <Tooltip
                placement="left"
                isOpen={this.state[request.task_id]}
                autohide={false}
                target={`DisabledAutoHideExampleRatingToolTIp_${request.task_id}`}
                toggle={() => {
                  this.setState({
                    [request.task_id]: !this.state[request.task_id],
                  });
                }}
              >
                {lang_data.my_requests.share_feedback_tt}
              </Tooltip>
            </>
          );
        } else {
          return null;
        }
      }
    } else {
      return null;
    }
  };

  explicitText = (request, flag) => {
    let lang_data = require(`../../assets/lang/${request.task_language}.json`);
    if (flag === 1) {
      return lang_data.my_requests.discussion
    } else if (flag === 2) {
      return lang_data.my_requests.edit
    } else if (flag === 3) {
      return lang_data.my_requests.raised_on_behalf
    } else if (flag === 4) {
      return lang_data.my_requests.by
    }
  }

  showRatingPopup = (e, task_id, task_ref, req_name, lang) => {
    e.preventDefault();
    let lang_data = require(`../../as