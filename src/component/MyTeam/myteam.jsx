import React, { Component } from "react";
// import Layout from "../Layout/layout";
import axios from "../../shared/axios";

import { BasicUserData } from "../../shared/helper";
import { connect } from "react-redux";

class MyTeam extends Component {
  state = {
    teamsList: [],
    isLoading: true,
  };

  componentDidMount() {
    this.getTeamList();
    document.title = "My Dr. Reddy's Team | Dr. Reddy's API";
    let userData = BasicUserData();
    if (userData) {
      this.setState({
        isLoading: false
      });
    }
  }

  getTeamList() {
    this.setState({ showLoader: true });
    axios
      .get("/team")
      .then((res) => {
        // console.log("team", res.data.data);
        this.setState(
          {
            showLoader: false,
            teamsList: res.data.data,
          },
          () => {}
        );
      })
      .catch((err) => {
        this.setState({
          teamsList: [],
        });
      });
  }

  render() {
    if (this.state.isLoading === true) {
      return (
        <>
          <div className="loginLoader">
            <div className="lds-spinner">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <span>Please Wait…</span>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <div className="container-fluid clearfix">
          <div className="dashboard-content-sec">
            <div>
              <div className="block block-core block-page-title-block">
                <h2>{this.props.dynamic_lang.my_team.header}</h2>
              </div>
              <div
                id="block-reddy-admin-content"
                className="block block-system block-system-main-block"
              >
                <div className="content">
                  <div className="row clearfix">
                    {this.state.teamsList &&
                      this.state.teamsList.map((team, teamKey) => (
                        <div
                          key={teamKey}
                          className="col-lg-4 col-md-6 col-sm-12"
                        >
                          <div className="team-member">
                            {team.profile_pic != "" &&
                            team.profile_pic != null ? (
                              <figure>
                                <img
                                  src={team.profile_pic}
                                  width="94"
                                  height="94"
                                />
                              </figure>
                            ) : (
                              <figure>
                                <img
                                  src={require("../../assets/images/default-user.png")}
                                  width="94"
                                  height="94"
                                />
                              </figure>
                            )}
                            <div className="name">
                              {team.first_name} {team.last_name}
                            </div>
                            <div className="designation">{team.desig_name}</div>
                            <p>{team.dept_name}</p>
                            <div className="social-link">
                              <ul>
                                <li>
                                  <a href={"mailto:" + team.email}>
                                    {/*<img src={mailIcon} width="15" height="15" />*/}
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </div>
                      ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}
//export default MyTeam;
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyTeam);
