import React, { Component } from "react";
import { Link } from "react-router-dom";
// import Layout from "../Layout/layout";

import axios from "../../shared/axios";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";

import closeIconSuccess from "../../assets/images/times-solid-green.svg";

// import whitelogo from "../../assets/images/info-circle-solid.svg";
import whitelogo from "../../assets/images/logo.png";


// SATYAJIT
import { connect } from "react-redux";
import {
  updateUserImage,
  removeUserImage,
  fetchUserData,
} from "../../store/actions/user";
import { refreshToken } from "../../store/actions/auth";
import { BasicUserData, parseToken } from "../../shared/helper";
import closeIcon from "../../assets/images/times-solid-red.svg";
import Loader from "../Loader/loader";
import { Tooltip } from "reactstrap";
import infoIcon from "../../assets/images/info-circle-solid.svg";
// SATYAJIT

const initialValues = {
  first_name: "",
  last_name: "",
  email: "",
  phone_no: "",
  company_name: "",
  country_id: "",
  language_code: "",
  user_image: "",
  current_password: "",
  new_password: "",
  confirm_password: "",
  dnd: "",
};

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

/*Validation*/
const validationSchema = (refObj) =>
  Yup.object().shape({
    first_name: Yup.string().trim().required(refObj.props.dynamic_lang.login.title),
    last_name: Yup.string().trim().required(
      refObj.props.dynamic_lang.display_error.my_account.last_name
    ),
    email: Yup.string().trim()
      .required(refObj.props.dynamic_lang.display_error.my_account.email)
      .email(refObj.props.dynamic_lang.display_error.my_account.valid_email),
    phone_no: Yup.string().trim()
      .required(refObj.props.dynamic_lang.display_error.my_account.phone_no)
      .matches(
        phoneRegExp,
        refObj.props.dynamic_lang.display_error.my_account.valid_phone_no
      ),
    country_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.my_account.country_id
    ),
    language_code: Yup.string().required(
      refObj.props.dynamic_lang.display_error.my_account.language_code
    ),
    company_name: Yup.string().trim().required(
      refObj.props.dynamic_lang.display_error.my_account.company_name
    ),
    current_password: Yup.string()
      .when("new_password", {
        is: (val) => val !== undefined,
        then: Yup.string().required(
          refObj.props.dynamic_lang.display_error.my_account.current_password
        ),
      })
      .when("confirm_password", {
        is: (val) => val !== undefined,
        then: Yup.string().required(
          refObj.props.dynamic_lang.display_error.my_account.current_password
        ),
      }),

    confirm_password: Yup.string()
      .label("Confirm password")
      .test(
        "passwords-match",
        refObj.props.dynamic_lang.display_error.my_account.confirm_password,
        function (value) {
          return this.parent.new_password === value;
        }
      ),
    dnd: Yup.string()
      .trim()
      .required(refObj.props.dynamic_lang.display_error.my_account.dnd)
      .matches(
        /^[1|2]$/,
        refObj.props.dynamic_lang.display_error.my_account.dnd_match
      ),
  });

class MyAccount extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_image: "",
      role: 0,
      accountDetails: [],
      country: [],
      language: [],
      company: [],
      message: null,
      errMsg: null,
      showLoader: false,
      isLoading: false,
      mainLoader: true,
      tooltipOpen: false,
      showPasswordMatch: false
    };
  }
  componentDidMount = () => {
    this.getCountryList();
    this.getLanguageList();
    this.getAccountDetails();

    let userData = BasicUserData();
    if (userData) {
      this.setState({ role: userData.role });
    }
    this.setState({
      user_image: this.props.profile_pic,
      // showLoader: false,
    });
  };

  toggle = () => {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    });
  };

  componentDidUpdate = (prevProps) => {
    if (this.props.profile_pic !== prevProps.profile_pic) {
      this.setState({
        user_image: this.props.profile_pic,
      });
    }
  };

  getCountryList() {
    axios
      .get("/country")
      .then((res) => {
        this.setState({
          country: [
            {
              country_id: "",
              country_name: this.props.dynamic_lang.display_error.my_account
                .country_id,
            },
          ].concat(res.data.data),
        });
      })
      .catch((err) => {});
  }

  getLanguageList() {
    axios
      .get("/language")
      .then((res) => {
        this.setState({
          language: [
            {
              id: "",
              language: this.props.dynamic_lang.display_error.my_account
                .language_code,
            },
          ].concat(res.data.data),
        });
      })
      .catch((err) => {});
  }

  getAccountDetails() {
    this.setState({isLoading: true});
    axios
      .get("/user/show")
      .then((res) => {
        this.setState({ accountDetails: res.data.data[0] }, () => {
          document.title = `${
            this.state.accountDetails.first_name.charAt(0).toUpperCase() +
            this.state.accountDetails.first_name.slice(1)
          } ${
            this.state.accountDetails.last_name.charAt(0).toUpperCase() +
            this.state.accountDetails.last_name.slice(1)
          } | Dr. Reddy's API`;
          setTimeout(() => this.getCountryList(), 1000);
          setTimeout(() => this.getLanguageList(), 1000);
          // setTimeout(() => this.setState({ showLoader: false }), 1000);
          this.setState({ isLoading: false });
        });
      })
      .catch((err) => {});
  }

  fileChangedHandler = (event, setErrors) => {
    this.setState({ showLoader: true });

    this.props.updateProfileImage(
      event.target.files[0],
      () => {
        this.setState({
          user_image: this.props.profile_pic,
          showLoader: false,
        });
      },
      setErrors
    );
  };

  handleSubmit = (values, actions) => {
    this.setState({ errMsg: "" });
    this.setState({ message: "" });
    this.setState({ showLoader: true });

    var formData = new FormData();

    if (values.current_password !== "" && values.new_password !== "") {
      formData.append("current_password", values.current_password);
      formData.append("password", values.new_password);
    }

    formData.append("phone_no", values.phone_no.trim());
    formData.append("country_id", values.country_id);
    formData.append("language_code", values.language_code.trim());

    formData.append("dnd", values.dnd);

    axios
      .post("/user/edit", formData)
      .then((res) => {
        if (res.status === 200) {
          this.setState({ showLoader: false });
          this.setState({
            message: this.props.dynamic_lang.display_error.my_account.success,
          });
          actions.setSubmitting(false);
          this.getAccountDetails();
          this.props.refreshTokenEdit();
        }
      })
      .catch((err) => {
        this.setState({ showLoader: false });
        actions.setSubmitting(false);

        if (err.data.status === 2) {
          if (err.data.errors.password) {
            err.data.errors.current_password = err.data.errors.password;
          }
          actions.setErrors(err.data.errors);
        } else if (err.data.status === 3) {
          this.setState({ errMsg: err.data.message });
        } else {
          this.setState({ errMsg: "Invalid access" });
        }
      });
  };

  removePicture(setErrors) {
    this.props.removeProfileImage(() => {
      this.setState({ user_image: this.props.profile_pic, showLoader: false });
    }, setErrors);
  }

  hideSuccessMsg = () => {
    this.setState({ message: "" });
  };
  hideError = () => {
    this.setState({ errMsg: "" });
  };
  removeError = (setErrors) => {
    setErrors({});
  };

  checkHandler = (event) => {
    event.preventDefault();
  };

  render() {
    var choose = parseToken(this.props.token);

    const { accountDetails, user_image, tooltipOpen } = this.state;
    const newInitialValues = Object.assign(initialValues, {
      first_name: accountDetails.first_name ? accountDetails.first_name : "",
      last_name: accountDetails.last_name ? accountDetails.last_name : "",
      email: accountDetails.email ? accountDetails.email : "",
      phone_no: accountDetails.phone_no ? accountDetails.phone_no : "",
      country_id: accountDetails.country_id ? accountDetails.country_id : "",
      language_code: accountDetails.language_code
        ? accountDetails.language_code
        : "",
      dnd:
        accountDetails.dnd || +accountDetails.dnd === 0
          ? accountDetails.dnd.toString()
          : "2",
      company_name: accountDetails.company_name
        ? accountDetails.company_name
        : "",
    });

    if (this.state.isLoading === true) {
      return (
        <>
          {/* <div className="loderOuter">
            <div className="loading_reddy_outer">
              <div className="loading_reddy">
                <img src={whitelogo} alt="logo" />
              </div>
            </div>
          </div> */}
          
          <Loader />
        </>
      );
    } else {
      return (
        <div className="container-fluid clearfix myAccountSec">
          <div className="dashboard-content-sec">
            <div className="service-request-form-sec">
              {this.state.message !== null && this.state.message !== "" && (
                <div className="messagessuccess">
                  <Link to="#" className="close">
                    <img
                      src={closeIconSuccess}
                      width="17.69px"
                      height="22px"
                      onClick={(e) => this.hideSuccessMsg()}
                      alt=""
                    />
                  </Link>
                  {this.state.message}
                </div>
              )}

              {this.state.errMsg !== null && this.state.errMsg !== "" && (
                <div className="messageserror">
                  <Link to="#" className="close">
                    <img
                      src={closeIcon}
                      width="17.69px"
                      height="22px"
                      onClick={(e) => this.hideError()}
                      alt=""
                    />
                  </Link>
                  {this.state.errMsg}
                </div>
              )}
              {this.state.showLoader === true ? <Loader /> : null}
              <Formik
                initialValues={newInitialValues}
                validationSchema={() => validationSchema(this)}
                onSubmit={this.handleSubmit}
              >
                {({
                  values,
                  errors,
                  isValid,
                  touched,
                  isSubmitting,
                  setFieldTouched,
                  setErrors,
                  setFieldValue,
                }) => {
                  //console.log("Errors",errors);
                  // console.log("Values",values);
                  return (
                    <Form encType="multipart/form-data">
                      {(errors.hasOwnProperty("phone_no") ||
                        errors.hasOwnProperty("country_id") ||
                        errors.hasOwnProperty("language_code") ||
                        errors.hasOwnProperty("current_password") ||
                        errors.hasOwnProperty("new_password") ||
                        errors.hasOwnProperty("confirm_password")) && (
                        <div className="messageserror">
                          <div className="close-button">
                            <Link to="#" className="close">
                              <img
                                src={closeIcon}
                                width="17.69px"
                                height="22px"
                                onClick={(e) => this.removeError(setErrors)}
                                alt=""
                              />
                            </Link>
                          </div>
                          {errors.phone_no ? (
                            <span className="errorMsg">{errors.phone_no}</span>
                          ) : null}

                          {errors.country_id ? (
                            <span className="errorMsg">
                              {errors.country_id}
                            </span>
                          ) : null}

                          {errors.language_code ? (
                            <span className="errorMsg">
                              {errors.language_code}
                            </span>
                          ) : null}

                          {errors.current_password ? (
                            <span className="errorMsg">
                              {errors.current_password}
                            </span>
                          ) : null}

                          {errors.new_password ? (
                            <span className="errorMsg">
                              {errors.new_password}
                            </span>
                          ) : null}

                          {errors.confirm_password ? (
                            <span className="errorMsg">
                              {errors.confirm_password}
                            </span>
                          ) : null}
                        </div>
                      )}
                      <div className="image-widget">
                        <div className="row">
                          <div className="col-md-4">
                            <div className="nav-link-icon-wrapper">
                              <img
                                style={{
                                  height: "auto",
                                  width: "100px",
                                  borderRadius: "100%",
                                }}
                                src={user_image}
                                alt=""
                              />
                            </div>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-12">
                            {choose === 1 && (
                              <Field
                                name="profile_pic"
                                type="file"
                                className={`form-control`}
                                className="customFile"
                                autoComplete="off"
                                onChange={(e) => {
                                  setFieldTouched("profile_pic");
                                  this.fileChangedHandler(e, setErrors);
                                }}
                              />
                            )}

                            {errors.profile_pic && touched.profile_pic && (
                              <label>{errors.profile_pic}</label>
                            )}

                            {choose === 0 && (
                              <>
                                <Link
                                  to="#"
                                  onClick={() => this.removePicture(setErrors)}
                                  className="removeBtn"
                                >
                                  {this.props.dynamic_lang.my_account.remove}
                                </Link>
                              </>
                            )}
                          </div>
                        </div>
                        <div className="clearfix" />
                      </div>

                      <div className="row">
                        <div className="col-md-4">
                          <label>
                            {this.props.dynamic_lang.my_account.first_name}
                          </label>
                          <Field
                            disabled={true}
                            name="first_name"
                            type="text"
                            className="form-control customInput"
                            //placeholder="First Name"
                            value={values.first_name}
                          />
                        </div>
                        <div className="col-md-4">
                          <label>
                            {this.props.dynamic_lang.my_account.last_name}
                          </label>
                          <Field
                            disabled={true}
                            name="last_name"
                            type="text"
                            className="form-control customInput"
                            //placeholder="Last Name"
                            value={values.last_name}
                          />
                        </div>
                        <div className="col-md-4">
                          <label>
                            {this.props.dynamic_lang.my_account.mobile_no}
                          </label>
                          <Field
                            name="phone_no"
                            type="text"
                            className="form-control customInput"
                            //placeholder="Mobile No"
                            value={values.phone_no}
                          />
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-md-6">
                          <label>
                            {this.props.dynamic_lang.my_account.country}
                          </label>
                          <Field
                            component="select"
                            name="country_id"
                            className="form-control customeSelect"
                            value={values.country_id}
                          >
                            {this.state.country.map((country) => (
                              <option
                                key={country.country_id}
                                value={country.country_id}
                              >
                                {country.country_name}
                              </option>
                            ))}
                          </Field>
                        </div>

                        <div className="col-md-6">
                          <label>
                            {this.props.dynamic_lang.my_account.language}
                          </label>
                          <Field
                            component="select"
                            name="language_code"
                            className="form-control customeSelect"
                            value={values.language_code}
                            disabled={!this.props.multi_lang_access}
                          >
                            {this.state.language.map((lang) => (
                              <option key={lang.code} value={lang.code}>
                                {lang.language}
                              </option>
                            ))}
                          </Field>
                        </div>
                        <div className="col-md-6">
                          <label>
                            {this.props.dynamic_lang.my_account.email}
                          </label>
                          <Field
                            disabled={true}
                            name="email"
                            type="text"
                            className="form-control customInput"
                            //placeholder="Email"
                            value={values.email}
                          />
                        </div>

                        {this.state.role === 1 && (
                          <div className="col-md-6">
                            <label>
                              {this.props.dynamic_lang.my_account.company}
                            </label>
                            <Field
                              disabled={true}
                              name="company_name"
                              type="text"
                              className="form-control customInput"
                              // placeholder="Mobile No"
                              value={values.company_name}
                            />
                          </div>
                        )}

                        <div className="col-md-6">
                          <label>
                            {
                              this.props.dynamic_lang.my_account
                                .current_password
                            }
                          </label>
                          <Field
                            name="current_password"
                            type="password"
                            className="form-control customInput"
                            //placeholder="Current Password"
                          />
                        </div>

                        <div className="col-md-6">
                          <label>
                            {this.props.dynamic_lang.my_account.password}
                          </label>
                          <Field
                            name="new_password"
                            type="password"
                            className="form-control customInput"
                            //placeholder="New Password"
                          />
                        </div>

                        <div className="col-md-6">
                          <label>
                            {this.props.dynamic_lang.my_account.conf_password}
                          </label>
                          <Field
                            name="confirm_password"
                            type="password"
                            className="form-control customInput"
                            //placeholder="Confirm Password"
                          />
                        </div>

                        <div className="col-md-6">
                          <label>
                            {
                              this.props.dynamic_lang.my_account
                                .dis_notifications
                            }
                            {` `}
                            <img
                              src={infoIcon}
                              width="15.44px"
                              height="18px"
                              id="DisabledAutoHideExample"
                              type="button"
                            />
                            <Tooltip
                              placement="right"
                              isOpen={tooltipOpen}
                              autohide={false}
                              target="DisabledAutoHideExample"
                              toggle={this.toggle}
                            >
                              {
                                this.props.dynamic_lang.my_account
                                  .tool_dis_notifications
                              }
                            </Tooltip>
                          </label>
                          <Field
                            name="dnd"
                            component="select"
                            className={`customeSelect form-control`}
                            autoComplete="off"
                            value={values.dnd}
                          >
                            <option key="2" value="2">
                              {this.props.dynamic_lang.my_account.no}
                            </option>
                            <option key="1" value="1">
                              {this.props.dynamic_lang.my_account.yes}
                            </option>
                          </Field>
                          {errors.dnd && touched.dnd ? (
                            <span className="errorMsg">{errors.dnd}</span>
                          ) : null}
                        </div>
                      </div>
                      <div
                        className="row btnSec"
                        style={{ textAlign: "center" }}
                      >
                        <div className="col-md-12">
                          <button
                            id=""
                            className="btn btn-default btn-secondary btn-md"
                            type="submit"
                          >
                            {isSubmitting
                              ? "Saving ..."
                              : this.props.dynamic_lang.my_account.save}
                          </button>
                        </div>
                      </div>
                    </Form>
                  );
                }}
              </Formik>
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  //console.log('masp state to props', state);
  return {
    token: state.auth.token,
    profile_pic: state.user.url,
    dynamic_lang: state.auth.dynamic_lang,
    name: state.user.name,
    multi_lang_access: state.auth.multi_lang_access,
    new_feature: state.auth.new_feature,
    errors: state.user.errors,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUser: (data, onSuccess) => dispatch(fetchUserData(data, onSuccess)),
    updateProfileImage: (data, onSuccess, setErrors) =>
      dispatch(updateUserImage(data, onSuccess, setErrors)),
    removeProfileImage: (onRemove, setErrors) =>
      dispatch(removeUserImage(onRemove, setErrors)),
    refreshTokenEdit: (data, onSuccess, setErrors) =>
      dispatch(refreshToken(data, onSuccess, setErrors)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyAccount);
