import React, { Component, Fragment } from "react";
import Header from "../Header/header";
import Sidebar from "../Sidebar/sidebar";
import { BasicUserData,checkEmployee } from "../../shared/helper";
import Footer from "../Footer/footer";

import { connect } from "react-redux";
import { setEnglishLang } from "../../store/actions/auth";

class Layout extends Component {

  componentDidMount() {
    document.body.classList.add("body-sidebar");
  }


  render() {

    const pathIgnoreList = ['process_customer_response','process_customer_approval'];
    const currentPath = window.location ? window.location.pathname.split('/')[1] : '';

    const isLoggedIn =  this.props.isLoggedIn;    
    let lang_class = "";
    if(isLoggedIn){

      const logged_User = BasicUserData();
      let user_lang = "en"
      if(typeof logged_User.lang == 'undefined'){
        this.props.setEng();
      }
      if(logged_User){
        user_lang = logged_User.lang;
      }

      if(user_lang){
        if (user_lang == "ja") {
          lang_class = 'ja_layout';
        }else if (user_lang == "es") {
          lang_class = 'es_layout';
        }else if (user_lang == "zh") {
          lang_class = 'zh_layout';
        }else if (user_lang == "pt") {
          lang_class = 'pt_layout';
        }else{
          lang_class = 'en_layout';
        }
      }
    }
   
    return (
      pathIgnoreList.includes(currentPath) ? (
        <div id="themecontent_body" >
          {this.props.children}
        </div>
      ) : (
        <Fragment>
          {isLoggedIn && <Header isLoggedIn={isLoggedIn}/>}
          <Sidebar isLoggedIn={isLoggedIn}/>
          <div className={isLoggedIn? `theme-content clearfix ${lang_class} ${this.props.faLayout}`:""} id="themecontent_body" >
            {this.props.children}
          </div>
          <Footer isLoggedIn={isLoggedIn}/>
        </Fragment>
      )
    );
  }
}

const mapStateToProps = state => { 
  return {
    isLoggedIn : state.auth.token !== null ? true : false,
    faLayout   : state.auth.faLayout
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    setEng: () => dispatch(setEnglishLang())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
