import React, { Component } from "react";
import { Link } from "react-router-dom";
import * as Yup from "yup";
import Select from "react-select";
import { Formik, Form, Field } from "formik";
import closeIcon from "../../assets/images/times-solid-red.svg";
//import downloadIcon from "../../assets/images/download-icon.svg";
import { connect } from "react-redux";
import Loader from "../Loader/loader";
import { supportedFileType, htmlDecode } from "../../shared/helper";
import Dropzone from "react-dropzone";
import swal from "sweetalert";
import axios from "../../shared/axios";

import ReactHtmlParser from "react-html-parser";


class viewProductEnquiry extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enquiry_id: this.props.match.params.id,
      enquiry_date:[],
      request_id: "",
      errMsg: "",
      showLoader: false,
      showBlock: false,
      buttonNext: false,
      sold_to_party: "",
      requested_for_list: [
        {
          id: 1,
          question: 'Want to know more Orange Book details of the particular product?',
          answer: `Retrieve details from the USFDA Orange Book Details for a product. This will provide information on the player market, NDC (National Drug Code), and the relevant application numbers. If you want more details on the dosage forms, pill details
          (like color, shape, size), strength, whether RLD is available, please click on Request for more information.`
        },
        {
          id: 2,
          question: 'Want to know the TGA details of the product (Australia)?',
          answer: `Retrieve details from the Therapeutics Goods Administration (Australia) for a product.`
        },
        {
          id: 3,
          question: 'Want to know the EMA details of the product?',
          answer: `Retrieve details from the European Medicenes Agency for a product. If you require information on last modified date or indications it is approved for, please click on Request for more information.`
        },
        {
          id: 4,
          question: 'What are the details of the API of the product?',
          answer: `Retrieve details from the API DrugBank for a particular API. This will provide information on the product structure & identification, experimental properties, calculated properties, and its pharmcology.`
        },
        {
          id: 5,
          question: 'What are the various patents associated with a product?',
          answer: `Retrieve list of all patents associated with a product along with a patent expiry date, with links to the patent offices (USPTO) for dossiers. If you require more information on claims on the patents (independent or dependent claims), please
          click on Request for more information`
        },
        {
          id: 6,
          question: 'For a particular product, what are the various excipients used?',
          answer: `Retrieve information on a product (please provide either the specific NDC code or the organization). You will receive all the Orange book details of the product, various excipients used with the following details for each excipient: Name, functional
          category, daily dosage limit, regulatory status, incompatibilities, links for each to Inxight: Drugs`
        }
        // ,
        // {
        //   id: 7,
        //   question: 'What are alternate excipients that can be used?',
        //   answer:`Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
        // }
      ]
    };
  }

  componentDidMount() {
    this.getTaskDetails();
  }

  getTaskDetails() {
    this.setState({ showLoader: true});
    axios
      .get(`fa/enquiry_details/${this.props.match.params.id}`)
      .then((res) => {
        this.setState({ enquiry_date:res.data.data[0],showLoader: false });      
      })
      .catch((err) => {
        //this.setState({ showLoader: false, errMsg: "Unable to fetch product." });  
      });
  };

  openAttachment = (filepath) => {
    // axios.get(filepath)
    //   .then((res) => {
    //     window.open(res.data.data, "_blank");
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
  };

  removeError = (setErrors) => {
    setErrors({});
  };  


  getEnquiryType = (enquiry_type) => {
    let ret = ''
    for (let index = 0; index < this.state.requested_for_list.length; index++) {
      const element = this.state.requested_for_list[index];
      if(element.id === enquiry_type){
        ret = element.question;
      }
    }
    return ret;
  }

  
  render() {
    const {
      sold_to_party
    } = this.state;
    if (this.state.showLoader) {
      return <Loader />;
      // return <h2>Loading....</h2>
    } else {
      return (
        <>
          <div className="container-fluid clearfix formSec viewProductEnquiry">
            <div className="dashboard-content-sec">
              <div className="service-request-form-sec newFormSec stockWrapper">
                <div className="form-page-title-block">
                  <h2>View Product Enquiry</h2>
                  <Link
                    to="/my_product_enquiries"
                    className="backLink"
                    id="showHistory"
                  >
                    Back
                  </Link>
                </div>

                <div className="row stockDisplayWrapper">
                    <div className="col-md-12">
                      <table className="table table-bordered customTable">
                        <tbody>
                        <tr>
                          <td width="150" className="tablePrimaryBg">
                            <strong>Enquiry Type:</strong>
                          </td>
                          <td>{this.getEnquiryType(this.state.enquiry_date.enquiry_type)}</td>

                        </tr>
<tr>
                         
<td width="150" className="tablePrimaryBg">
                            <strong>Product Name:</strong>
                          </td>
                          <td>{this.state.enquiry_date.product_name}</td>
</tr>
                        <tr>
                          <td width="150" className="tablePrimaryBg">
                            <strong>Requested Date:</strong>
                          </td>
                          <td>{this.state.enquiry_date.date_added_converted}</td>

                        </tr>
<tr>
<td width="150" className="tablePrimaryBg">
                            <strong>Status:</strong>
                          </td>
                          <td>{this.state.enquiry_date.processed === 1?'Completed':'Processing'}</td>
</tr>
                        <tr>
                          <td width="150" className="tablePrimaryBg">
                            <strong>Additional Details:</strong>
                          </td>
                          <td>{ReactHtmlParser(
                            htmlDecode(this.state.enquiry_date.comment)
                          )}</td>

                        </tr>
                        <tr>

                        {this.state.enquiry_date.processed === 1 && <><td width="150" className="tablePrimaryBg">
                            <strong>Download Excel:</strong>
                          </td>
                          <td> <a href={`${this.state.enquiry_date.download_url}`}>Click Here</a></td></>}
                        </tr>
                        </tbody>
                      </table>                      
                    </div>
                   
                  </div>
          
              </div>
            </div>
          </div>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(viewProductEnquiry);
