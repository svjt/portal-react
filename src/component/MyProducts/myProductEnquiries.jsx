import React, { Component } from "react";
import { Link } from "react-router-dom";
import Layout from "../Layout/layout";
import axios from "../../shared/axios";
import { htmlDecode, BasicUserData, checkEmployee } from "../../shared/helper";
import Pagination from "react-js-pagination";
import swal from "sweetalert";
import { Row, Col, ButtonToolbar, Button, Modal } from "react-bootstrap";
import { Formik, Form } from "formik";
import { Tooltip } from "reactstrap";
import infoIcon from "../../assets/images/info-circle-solid.svg";
import closeIconSuccess from "../../assets/images/times-solid-green.svg";
import closeIconSelect from "../../assets/images/close.svg";
import closeIcon from "../../assets/images/times-solid-red.svg";
import Loader from "../Loader/loader";
import Autosuggest from "react-autosuggest";
import Select from "react-select";
import { isMobile } from "react-device-detect";

// SATYAJIT
import { connect } from "react-redux";
import { getTStatus } from "../../store/actions/user";
import { faHideLeftMenu} from "../../store/actions/auth";

// SATYAJIT

var message = "";

class myProductEnquiries extends Component {
  constructor(props) {
    super(props);
    message = this.props.location.state ? this.props.location.state.message : "";
    this.props.history.push({
      state: { message: '' }
    })
  }
  state = {
    enquiriesList: [],
    activePage: 1,
    totalCount: 0,
    itemPerPage: 10,

  };
  componentDidMount() {
    this.getProductEnquiries();
    document.title = "My Product Enquiries | Dr. Reddy's API";

    let userData = BasicUserData();
    if (userData) {
      this.setState({
        isLoading: false,
      });
    }
    this.props.faLeftMenu();
  }

  hideError = () => {
    this.setState({ errMsg: "" });
  };
  hideSuccessMsg = () => {
    message = "";
    this.props.history.push({
      state: { message: '' }
    })
  };

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getProductEnquiries(pageNumber > 0 ? pageNumber : 1);
  };
  getProductEnquiries(page = 1) {
    this.setState({ showLoader: true });
    axios
      .get(`/fa/task_enquiry?page=${page}`)
      .then((res) => {
        this.setState({
          enquiriesList: res.data.data,
          count_req: res.data.cnt,
          isLoading: false,
          showLoader : false
        });
      })
      .catch((err) => { });
  };
  

  render() {
    if (this.state.isLoading === true) {
      return (
        <>
          <div className="loginLoader">
            <div className="lds-spinner">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <span>Please Wait…</span>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="container-fluid clearfix listingSec">
            <div className="dashboard-content-sec">
              <div className="col-md-12 form-search-section formSearchrequest">
              </div>
              
                
              <div className="service-request-form-sec">
                {this.state.showLoader === true ? <Loader /> : null}

                {message !== null && message !== "" && (
                  <div className="messagessuccess">
                    <Link to="#" className="close">
                      <img
                        src={closeIconSuccess}
                        width="17.69px"
                        height="22px"
                        onClick={(e) => this.hideSuccessMsg()}
                      />
                    </Link>
                    {message}
                  </div>
                )}
                <div
                  className="messageserror"
                  style={{
                    display:
                      this.state.errMsg &&
                      this.state.errMsg !== null &&
                      this.state.errMsg !== ""
                        ? "block"
                        : "none",
                  }}
                >
                  <Link to="#" className="close">
                    <img
                      src={closeIcon}
                      width="17.69px"
                      height="22px"
                      onClick={(e) => this.hideError()}
                    />
                  </Link>
                  <div>
                    <ul className="">
                      <li className="">{this.state.errMsg}</li>
                    </ul>
                  </div>
                </div>

                <div>
                  <div className="row">
                    <div className="col-md-8 form-page-title2">
                      <h2>My Product Enquiries</h2>
                      <Link
                        to={{ pathname: `/product-enquiry` }}
                        className="addBtn"
                      >
                        New Enquiry
                      </Link>
                    </div>

                    <div className="col-md-4 form-search-section formSearchrequest">

                    </div>
                    <div className="clearfix" />
                  </div>
                  {this.state.isLoading === true ? (
                    <Loader />
                  ) : (
                      <div className="listing-table table-responsive">
                        <table className="" style={{ width: "100%" }}>
                          <thead>
                            <tr>
                              <th>Enquiry</th>
                              <th>Product Name</th>                              
                              <th>Requested Date</th>
                              <th>Status</th>                               
                              <th>Action </th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.enquiriesList.map((enquiry, key) => (
                          
                              <tr key={key} className={"tr_"+enquiry.id}>
                                <td>{enquiry.enquiry_name}</td>
                                <td>{enquiry.product_name}</td>
                                <td>{enquiry.date_added}</td>
                                <td>{enquiry.processed== 1 ? "Completed" : "Processing" }</td>                                
                                <td>
                                  <Link
                                    to={{
                                      pathname: `/view_product_enquiry/${enquiry.id}`,
                                      state: { fromDashboard: true },
                                    }}
                                    className="action-btn"
                                  >
                                    View Enquiry
                                  </Link>
                                </td>
                              </tr>
                            ))}
                            {this.state.enquiriesList.length === 0 && (
                              <tr>
                                <td colSpan="5" align="center">
                                  
                                    No Product Enquiry found
                                  
                                </td>                                
                              </tr>
                            )}
                          </tbody>
                          
                        </table>
                      </div>
                    )}
                </div>
                <div className="clearfix" />
                {this.state.count_req > this.state.itemPerPage ? (
                  <div className="pagination-area">
                    <div className="paginationOuter text-right">
                      <Pagination
                        hideDisabled
                        hideFirstLastPages
                        prevPageText="‹‹"
                        nextPageText="››"
                        activePage={this.state.activePage}
                        itemsCountPerPage={this.state.itemPerPage}
                        totalItemsCount={this.state.count_req}
                        itemClass="nav-item"
                        linkClass="nav-link"
                        activeClass="active"
                        pageRangeDisplayed="1"
                        onChange={this.handlePageChange}
                      />
                    </div>
                  </div>
                ) : null}
                <div className="clearfix" />
              </div>
            </div>
          </div>
        </>
      );
    }
  }
}

// SATYAJIT
const mapStateToProps = (state) => {
  return {
    display_menu: state.menu.display,
    task_status_arr: state.user.task_status_arr,
    selCompany: state.user.selCompany,
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getTaskStatus: (data, onSuccess) => dispatch(getTStatus(data, onSuccess)),
    faLeftMenu: () => dispatch(faHideLeftMenu())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(myProductEnquiries);
