import React, { Component } from "react";

import Layout from "../Layout/layout";

import { BasicUserData } from "../../shared/helper";
import { connect } from "react-redux";

class News extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
    };
  }

  componentDidMount() {
    document.title =
      this.props.dynamic_lang.left_menu.news + " | Dr. Reddy's API";
    let userData = BasicUserData();
    if (userData) {
      this.setState({
        isLoading: false,
      });
    }
  }

  render() {
    if (this.state.isLoading === true) {
      return (
        <>
          <div className="loginLoader">
            <div className="lds-spinner">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <span>Please Wait…</span>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <div className="container-fluid clearfix">
          <h2>{this.props.dynamic_lang.coming_soon.header}</h2>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(News);
//export default News;
