import React, { Component } from "react";
import { Link } from "react-router-dom";
import Layout from "../Layout/layout";
import axios from "../../shared/axios";

import { htmlDecode, BasicUserData, checkEmployee } from "../../shared/helper";
import Pagination from "react-js-pagination";
import {
  Row,
  ButtonToolbar,
  Button,
  Modal,
  Col,
  OverlayTrigger,
  Tooltip
} from "react-bootstrap";
import { Formik, Form } from "formik";
// import { Tooltip } from "reactstrap";
import infoIcon from "../../assets/images/info-circle-solid.svg";
import closeIconSuccess from "../../assets/images/times-solid-green.svg";
import closeIconSelect from "../../assets/images/close.svg";
import Loader from "../Loader/loader";
import Autosuggest from "react-autosuggest";
import Select from "react-select";
import { isMobile } from "react-device-detect";
import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

// SATYAJIT
import { connect } from "react-redux";
import { showSidebarMenu } from "../../store/actions/menu";
import { getTStatus } from "../../store/actions/user";
// SATYAJIT

import Rating from "react-rating";

function LinkWithTooltip({ id, children, href, tooltip, clicked }) {
  return (
    <OverlayTrigger
      overlay={<Tooltip id={id} className="feedback-tooltip">{tooltip}</Tooltip>}
      placement="bottom"
      delayShow={300}
      delayHide={150}
      trigger={["hover"]}
    >
      <Link to={href} onClick={clicked}>
        {children}
      </Link>
    </OverlayTrigger>
  );
}

var message = "";

class MyRequests extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    message = this.props.location.state
      ? this.props.location.state.message
      : "";
  }

  state = {
    requestsList: [],
    ccList: [],
    showCcPopup: false,
    showStatusPopup: false,
    activePage: 1,
    totalCount: 0,
    itemPerPage: 10,
    tooltipOpen: false,
    searchkey: "",
    toggleMenuClass: false,
    finalchecked: [],
    task_id: 0,
    task_ref: "",
    req_name: "",
    showLoader: false,
    isLoading: true,
    value: "", // for auto-suggest
    suggestions: [], // for auto-suggest
    selectedCompany: null,
    status_active: [],
    request_type: "",
    shared_with_agent: 0,
    shared_with_agent_show: false,
    openMobileView: false,
    displayMobileText: "",
    selectedFrom: "",
    selectedTo: "",
    query_string: "",
    rating_err: "",
    showRRPopup: false,
    showPendingPopup: false,
    showThankyouPopup: false,
    pendingRatings: false,
    task_rating: 0,
    options_text: "",
    question_text: "",
    ratingHtml: "",
    ratingHtml2: "",
    selOptions: [],
    pending_requests: [],
    ratingComment: "",
    tooltipOpen2: false,
    popup_lang: {},
  };

  onMobileClose() {
    console.log("Event");
    if (isMobile) {
      var isClassExist = document
        .querySelector(".mobile-menu-icon")
        .classList.contains("mobile-menu-icon-active");

      console.log("isClassExist", isClassExist);
      if (isClassExist) {
        document.querySelector(".body-sidebar").classList.remove("menu-open");
        document
          .querySelector(".mobile-menu-icon")
          .classList.remove("mobile-menu-icon-active");
      } else {
        document.querySelector(".body-sidebar").classList.add("menu-open");
        document
          .querySelector(".mobile-menu-icon")
          .classList.add("mobile-menu-icon-active");
      }
    }
  }

  componentDidMount() {
    if (isMobile) {
      const datePickers = document.getElementsByClassName(
        "react-datepicker__input-container"
      );
      //console.log('datePickers', datePickers)
      Array.from(datePickers).forEach((el) =>
        el.childNodes[0].setAttribute("readOnly", true)
      );
    }
    this.getRequests();
    if (message !== null) {
      this.props.history.push({
        pathname: "",
        state: "",
      });
    }
    document.title = "My Request";

    this.state.selectedFrom = "";
    this.state.selectedTo = "";
    this.state.fromMaxDate = new Date(
      new Date().setFullYear(new Date().getFullYear() - 10)
    );
    this.state.toMaxDate = new Date(
      new Date().setFullYear(new Date().getFullYear() + 10)
    );

    let userData = BasicUserData();
    if (userData) {
      this.setState({
        isLoading: false,
      });
    }
  }

  componentDidUpdate = (prevProps) => {
    if (this.props.display_menu !== prevProps.display_menu) {
      this.setState({ toggleMenuClass: this.props.display_menu });
    }

    if (this.props.selCompany === null && prevProps.selCompany !== null) {
      this.setState({
        requestsList: [],
        count_req: 0,
        isLoading: false,
      });
    } else {
      if (this.props.selCompany !== prevProps.selCompany) {
        this.getRequests();
        this.setState({ activePage: 1 });
      }
    }

    document.title = "My Requests | Dr. Reddy's API";
  };

  toggle2 = () => {
    this.setState({
      tooltipOpen2: !this.state.tooltipOpen2,
    });
  };

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    var searchValue = "";
    if (this.state.searchkey != "") searchValue = this.state.searchkey;
    this.getRequests(
      pageNumber > 0 ? pageNumber : 1,
      searchValue,
      null,
      this.state.query_string
    );
  };

  handleSearchChange = (event) => {
    this.setState({ searchkey: event.target.value });
  };

  enterPressed = (event) => {
    var code = event.keyCode || event.which;
    if (code === 13) {
      //13 is the enter keycode

      this.setState({
        searchkey: event.target.value,
      });
      this.getRequests(1, event.target.value, null, this.state.query_string);
    }
  };

  getRequests(page = 1, search = "", sel_evnt = null, manual_search = "") {
    this.setState({
      isLoading: true,
    });
    let userData = BasicUserData();
    if (userData.role === 2) {
      if (sel_evnt != null) {
        this.setState({ selectedCompany: sel_evnt });
      }

      if (this.props.selCompany && this.props.selCompany.value > 0) {
        let url = `/request-list?cid=${this.props.selCompany.value}&page=${page}`;
        if (search != "") {
          url += `&s=${search}`;
        }
        if (manual_search != "") {
          url += `&${manual_search}`;
        }

        axios
          .get(url)
          .then((res) => {
            this.setState({
              requestsList: res.data.data,
              count_req: res.data.count,
              isLoading: false,
            });
          })
          .catch((err) => {});
      } else {
        this.setState({ isLoading: false });
      }
    } else {
      let url = `/request-list?page=${page}`;
      if (search != "") {
        url += `&s=${search}`;
      }
      if (manual_search != "") {
        url += `&${manual_search}`;
      }

      axios
        .get(url)
        .then((res) => {
          this.setState({
            requestsList: res.data.data,
            count_req: res.data.count,
            isLoading: false,
          });
        })
        .catch((err) => {});
    }
  }

  showCCList = (task_id,task_language) => {
    this.setState({ ccList: [], finalchecked: [],cc_lang:'' });
    let lang_data = require(`../../assets/lang/${task_language}.json`);
    let final_array = [];
    let userData = BasicUserData();
    axios
      .get(`tasks/get-cc-customers/${task_id}`)
      .then((res) => {
        this.setState({ ccList: res.data.data, showCcPopup: true,cc_lang:lang_data });
        if (res.data.data.selCCCustList.length > 0) {
          for (
            let index = 0;
            index < res.data.data.selCCCustList.length;
            index++
          ) {
            const selected = res.data.data.selCCCustList[index];
            final_array.push(selected.customer_id.toString());
          }
        }

        this.setState({ finalchecked: final_array });
        this.setState({ task_id: task_id });
      })
      .catch((err) => {});

    if (userData.role !== 2) {
      this.setState({ shared_with_agent: 0, shared_with_agent_show: false });
      axios
        .get(`/tasks/${task_id}`)
        .then((res) => {
          this.setState({
            shared_with_agent: res.data.data.taskDetails.share_with_agent,
          });
          if (res.data.data.taskDetails.submitted_by > 0) {
          } else {
            this.setState({ shared_with_agent_show: true });
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };

  showStatus = (task_id) => {
    axios
      .get(`/tasks/${task_id}/`)
      .then((res) => {
        /* let order_status = [
          "PO Acknowledged",
          "PO Accepted",
          "Production In-Progress",
          "Shipping Documents Shared",
          "Dispatched",
        ];
        let other_status = [
          "Acknowledge",
          "Confirmation",
          "In Progress",
          "Closed",
        ];
        let complaint_status = [
          "Acknowledge",
          "Accepted / Rejected",
          "In Progress",
          "Closed",
        ]; */
        let order_status = [
          this.props.dynamic_lang.my_orders.po_acknowledged,
          this.props.dynamic_lang.my_orders.po_accepted,
          this.props.dynamic_lang.my_orders.production_progress,
          this.props.dynamic_lang.my_orders.shipping_documents_shared,
          this.props.dynamic_lang.my_orders.dispatched,
        ];
        let other_status = [
          this.props.dynamic_lang.my_orders.acknowledge,
          this.props.dynamic_lang.my_orders.confirmation,
          this.props.dynamic_lang.my_orders.in_progress,
          this.props.dynamic_lang.my_orders.closed,
        ];
        let complaint_status = [
          this.props.dynamic_lang.my_orders.acknowledge,
          this.props.dynamic_lang.my_orders.accepted_rejected,
          this.props.dynamic_lang.my_orders.in_progress,
          this.props.dynamic_lang.my_orders.closed,
        ];
        let post_arr = { type: "G", lang: res.data.data.taskDetails.language };
        if (
          res.data.data.taskDetails.request_type == 23 ||
          res.data.data.taskDetails.request_type == 41
        ) {
          post_arr = { type: "N", lang: res.data.data.taskDetails.language };
        } else if (
          res.data.data.taskDetails.request_type == 7 ||
          res.data.data.taskDetails.request_type == 34
        ) {
          post_arr = { type: "C", lang: res.data.data.taskDetails.language };
        }

        this.props.getTaskStatus(post_arr, (res_arr) => {
          if (
            res.data.data.taskDetails.request_type == 23 ||
            res.data.data.taskDetails.request_type == 41
          ) {
            this.setState({ order_status: this.props.task_status_arr });
          } else if (
            res.data.data.taskDetails.request_type == 7 ||
            res.data.data.taskDetails.request_type == 34
          ) {
            this.setState({ complaint_status: this.props.task_status_arr });
          } else {
            this.setState({ other_status: this.props.task_status_arr });
          }

          this.setState({
            request_type: res.data.data.taskDetails.request_type,
            showStatusPopup: true,
          });
          //this.setState({other_status : other_status});
          //this.setState({order_status : order_status});
          //this.setState({complaint_status : complaint_status});
          let active_stat = null;
          let push_stat = [];
          // if(res.data.data.taskDetails.status_details != null && res.data.data.taskDetails.request_type == 23)
          // {
          //   active_stat = order_status.indexOf(res.data.data.taskDetails.status_details);
          //   for (let index = 0; index <= active_stat; index++) {
          //     push_stat.push(order_status[index]);
          //   }
          // }
          // else if(res.data.data.taskDetails.status_details != null && res.data.data.taskDetails.request_type == 7)
          // {
          //   active_stat = complaint_status.indexOf(res.data.data.taskDetails.status_details);
          //   for (let index = 0; index <= active_stat; index++) {
          //     push_stat.push(complaint_status[index]);
          //   }
          // }
          // else
          // {
          active_stat = this.props.task_status_arr.indexOf(
            res.data.data.taskDetails.status_details
          );
          for (let index = 0; index <= active_stat; index++) {
            push_stat.push(this.props.task_status_arr[index]);
          }
          // }
          this.setState({ status_active: push_stat });
        });
      })
      .catch((err) => {
        console.log("error", err);
      });
  };

  makeDynamicHtml = () => {
    if (
      this.state.ccList !== "" &&
      this.state.ccList.ccCutomerList.length > 0
    ) {
      let elements = this.state.ccList.ccCutomerList.map((element) => {
        let checked = false;

        if (
          this.state.ccList.selCCCustList &&
          this.state.ccList.selCCCustList.length > 0
        ) {
          for (
            let index = 0;
            index < this.state.ccList.selCCCustList.length;
            index++
          ) {
            const selected = this.state.ccList.selCCCustList[index];
            if (selected.customer_id === element.customer_id) {
              checked = true;
            }
          }
        }

        return (
          <li key={element.customer_id} style={{ listStyleType: "none" }}>
            <input
              type="checkbox"
              defaultChecked={checked}
              name={`checkCC_${element.customer_id}`}
              id={`checkCC_${element.customer_id}`}
              onChange={(e) => {
                this.toggleCheckbox(e);
              }}
              value={element.customer_id}
            />
            &nbsp;{element.first_name} {element.last_name} (
            {element.company_name})
          </li>
        );
      });

      return elements;
    } else {
      return "";
    }
  };

  handleSubmitAssignTask = (values, actions) => {
    //console.log("Values",this.state.finalchecked);
    this.setState({ showLoader: true });
    let userData = BasicUserData();
    if (userData.role != 2) {
      axios
        .post("tasks/update-cc", {
          task_id: this.state.task_id,
          cc: this.state.finalchecked,
          share_with_agent: this.state.shared_with_agent,
        })
        .then((res) => {
          this.setState({ showLoader: false });
          this.handleClose();
          this.props.history.push({
            pathname: "/my_requests",
          });
        })
        .catch((err) => {});
    } else {
      axios
        .post("tasks/update-cc", {
          task_id: this.state.task_id,
          cc: this.state.finalchecked,
        })
        .then((res) => {
          this.setState({ showLoader: false });
          this.handleClose();
          this.props.history.push({
            pathname: "/my_requests",
          });
        })
        .catch((err) => {});
    }
  };

  setRatedTooltip = (request) => {
    let lang_data = require(`../../assets/lang/${request.task_language}.json`);
    let html = "";
    if (request.rated_by_type === 2) {
      if (request.rated_by == request.submitted_by) {
        html = null;
      } else {
        html = (
          <>
            <img
              src={infoIcon}
              width="15.44"
              height="18"
              id={`DisabledAutoHideExampleRating_${request.task_id}`}
              type="button"
              className={`itooltip`}
            />
            <Tooltip
              placement="bottom"
              isOpen={this.state[request.task_id]}
              autohide={false}
              target={`DisabledAutoHideExampleRating_${request.task_id}`}
              toggle={() => {
                this.setState({
                  [request.task_id]: !this.state[request.task_id],
                });
              }}
            >
              {lang_data.my_requests.feedback_given_by}{" "}
              {request.first_name_rated_by_agent}{" "}
              {request.last_name_rated_by_agent}{" "}
              {lang_data.my_requests.on_behalf_of}{" "}
              {request.first_name} {request.last_name}
            </Tooltip>
          </>
        );
      }
    } else if (request.rated_by_type === 1) {
      if (request.rated_by == request.customer_id) {
        html = null;
      } else {
        html = (
          <>
            <img
              src={infoIcon}
              width="15.44"
              height="18"
              id={`DisabledAutoHideExampleRating_${request.task_id}`}
              type="button"
              className={`itooltip`}
            />
            <Tooltip
              placement="bottom"
              isOpen={this.state[request.task_id]}
              autohide={false}
              target={`DisabledAutoHideExampleRating_${request.task_id}`}
              toggle={() => {
                this.setState({
                  [request.task_id]: !this.state[request.task_id],
                });
              }}
            >
              {lang_data.my_requests.feedback_given_by}{" "}
              {request.first_name_rated_by} {request.last_name_rated_by}{" "}
              {lang_data.my_requests.on_behalf_of}{" "}
              {request.first_name} {request.last_name}
            </Tooltip>
          </>
        );
      }
    }

    return html;
  };

  getRatings = (request) => {
    let lang_data = require(`../../assets/lang/${request.task_language}.json`);
    if (request.close_status == 1 && request.duplicate_task == 0) {
      if (request.rating_skipped == 0) {
        if (request.rating > 0) {
          return (
            <>
              <Rating
                emptySymbol={
                  <img
                    src={require("../../assets/images/uncheck-star.svg")}
                    alt=""
                    className="icon"
                  />
                }
                placeholderSymbol={
                  <img
                    src={require("../../assets/images/uncheck-stars.svg")}
                    alt=""
                    className="icon"
                  />
                }
                fullSymbol={
                  <img
                    src={require("../../assets/images/uncheck-stars.svg")}
                    alt=""
                    className="icon"
                  />
                }
                initialRating={request.rating}
                readonly
              />
              {request.rated_by > 0 ? this.setRatedTooltip(request) : null}
            </>
          );
        } else {
          if (checkEmployee() == "") {
            return (
              <>
                <a
                  id={`DisabledAutoHideExampleRatingToolTIp_${request.task_id}`}
                  href="#"
                  style={{ textDecoration: "none" }}
                  onClick={(e) =>
                    this.showRatingPopup(
                      e,
                      request.task_id,
                      request.task_ref,
                      request.req_name,
                      request.task_language
                    )
                  }
                >
                  {lang_data.my_requests.share_feedback}
                </a>

                
                <LinkWithTooltip
                    tooltip={`${lang_data.my_requests.share_feedback_tt}`}
                    href="#"
                    id="tooltip-1"
                    // clicked={e => this.checkHandler(e)}
                    >
                    <img
                      src={infoIcon}
                      width="15.44"
                      height="18"
                      id="DisabledAutoHideExample"
                      type="button"
                    />
                   {/* <i>{lang_data.my_requests.share_feedback_tt}</i> */}
                </LinkWithTooltip>
                {/* <Tooltip
                  placement="bottom"
                  isOpen={this.state[request.task_id]}
                  autohide={false}
                  target={`DisabledAutoHideExampleRatingToolTIp_${request.task_id}`}
                  toggle={() => {
                    this.setState({
                      [request.task_id]: !this.state[request.task_id],
                    });
                  }}
                >
                  {lang_data.my_requests.share_feedback_tt}
                </Tooltip> */}
              </>
            );
          } else {
            return null;
          }
        }
      } else {
        if (checkEmployee() == "") {
          return (
            <>
              <a
                id={`DisabledAutoHideExampleRatingToolTIp_${request.task_id}`}
                href="#"
                style={{ textDecoration: "none" }}
                onClick={(e) =>
                  this.showRatingPopup(
                    e,
                    request.task_id,
                    request.task_ref,
                    request.req_name,
                    request.task_language
                  )
                }
              >
                {lang_data.my_requests.share_feedback}
              </a>
              <LinkWithTooltip
                tooltip={`${lang_data.my_requests.share_feedback_tt}`}
                href="#"
                id="tooltip-1"
                // clicked={e => this.checkHandler(e)}
                >
                <img
                  src={infoIcon}
                  width="15.44"
                  height="18"
                  id="DisabledAutoHideExample"
                  type="button"
                />
                  {/* <i>{lang_data.my_requests.share_feedback_tt}</i> */}
              </LinkWithTooltip>
              {/* <Tooltip
                placement="bottom"
                isOpen={this.state[request.task_id]}
                autohide={false}
                target={`DisabledAutoHideExampleRatingToolTIp_${request.task_id}`}
                toggle={() => {
                  this.setState({
                    [request.task_id]: !this.state[request.task_id],
                  });
                }}
              >
                {lang_data.my_requests.share_feedback_tt}
              </Tooltip> */}
            </>
          );
        } else {
          return null;
        }
      }
    } else {
      return null;
    }
  };

  explicitText = (request,flag) => {
    let lang_data = require(`../../assets/lang/${request.task_language}.json`);
    if(flag === 1){
      return lang_data.my_requests.discussion
    }else if(flag === 2){
      return lang_data.my_requests.edit
    }else if(flag === 3){
      return lang_data.my_requests.raised_on_behalf
    }else if(flag === 4){
      return lang_data.my_requests.by
    }
  }

  showRatingPopup = (e, task_id, task_ref, req_name, lang) => {
    e.preventDefault();
    let lang_data = require(`../../assets/lang/${lang}.json`);
    this.setState({ popup_lang: lang_data }, () => {
      this.setState({
        optionsArr: [],
        questionsArr: [],
        showPendingPopup: false,
        showThankyouPopup: false,
        pendingRatings: false,
      });
      axios
        .get(`tasks/rating-options/${task_id}`)
        .then((res) => {
          this.setState(
            {
              optionsArr: res.data.options,
              questionsArr: res.data.questions,
              task_rating: 0,
              task_id: task_id,
              task_ref: task_ref,
              req_name: req_name,
            },
            () => {
              this.setQuestion();
            }
          );
        })
        .catch((err) => {});
      this.getRequests(this.state.activePage);
    });
  };

  setQuestion = () => {
    let question = this.state.popup_lang.my_requests.initial_review_text;
    let options = "";
    let html2 = "";
    for (let index = 0; index < this.state.questionsArr.length; index++) {
      const element = this.state.questionsArr[index];

      let rate = element.rating.split(",");
      if (rate.length == 1) {
        if (rate[0] == this.state.task_rating) {
          question = element.name;
          break;
        }
      } else {
        for (let index = 0; index < rate.length; index++) {
          if (rate[index] == this.state.task_rating) {
            question = element.name;
            break;
          }
        }
        break;
      }
    }

    let html = question;

    this.setState({
      ratingHtml: html,
      ratingHtml2: html2,
      showRRPopup: true,
      rating_err: "",
    });
  };

  changeQuestion = (submitted_rating) => {
    let question = "";
    let options = [];
    let html2 = "";

    for (let index = 0; index < this.state.questionsArr.length; index++) {
      const element = this.state.questionsArr[index];

      let rate = element.rating.split(",");
      if (rate.length == 1) {
        if (rate[0] == submitted_rating) {
          question = element.name;
          break;
        }
      } else {
        for (let index = 0; index < rate.length; index++) {
          if (rate[index] == submitted_rating) {
            question = element.name;
            break;
          }
        }
        //break;
      }
    }

    for (let index = 0; index < this.state.optionsArr.length; index++) {
      const element = this.state.optionsArr[index];

      let rate = element.rating.split(",");
      if (rate.length == 1) {
        if (rate[0] == submitted_rating) {
          options.push(
            <>
              <Col sm={6}>
                <label className="customCheckBoxbtn">
                  <input
                    key={index}
                    type="checkbox"
                    onClick={(e) => {
                      let prev_state = this.state.selOptions;
                      if (e.target.checked) {
                        prev_state.push(element.o_id);
                      } else {
                        const index = prev_state.indexOf(element.o_id);
                        if (index > -1) {
                          prev_state.splice(index, 1);
                        }
                      }
                      this.setState({ selOptions: prev_state });
                    }}
                  />
                  <span className="checkmark ">{htmlDecode(element.name)}</span>
                </label>
              </Col>
            </>
          );
        }
      } else {
        for (let index = 0; index < rate.length; index++) {
          if (rate[index] == submitted_rating) {
            options.push(
              <>
                <Col sm={6}>
                  <label className="customCheckBoxbtn">
                    <input
                      key={index}
                      type="checkbox"
                      onClick={(e) => {
                        let prev_state = this.state.selOptions;
                        if (e.target.checked) {
                          prev_state.push(element.o_id);
                        } else {
                          const index = prev_state.indexOf(element.o_id);
                          if (index > -1) {
                            prev_state.splice(index, 1);
                          }
                        }
                        this.setState({ selOptions: prev_state });
                      }}
                    />
                    <span className="checkmark ">
                      {htmlDecode(element.name)}
                    </span>
                  </label>
                </Col>
              </>
            );
            break;
          }
        }
      }
    }
    html2 = options;

    let html = question;

    this.setState({
      ratingHtml: html,
      ratingHtml2: html2,
      task_rating: submitted_rating,
      rating_err: "",
    });
  };

  handleRatings = (values, actions) => {
    if (this.state.task_rating === 0) {
      this.setState({
        rating_err: this.state.popup_lang.display_error.form_error
          .ratings_before_submitting,
      });
      return false;
    } else {
      this.setState({ showLoader: true });
      axios
        .post("tasks/post-ratings", {
          task_id: this.state.task_id,
          rating: this.state.task_rating,
          comment: this.state.ratingComment,
          options: this.state.selOptions,
        })
        .then((res) => {
          this.setState({
            optionsArr: [],
            selOptions: [],
            questionsArr: [],
            showRRPopup: false,
            rating_err: "",
            ratingComment: "",
            ratingHtml: "",
            ratingHtml2: "",
            task_rating: 5,
            task_id: 0,
            task_ref: "",
            req_name: "",
            showLoader: false,
          });
          this.openThankyouPopup();
        })
        .catch((err) => {});
    }
  };

  handleRRClose = (e) => {
    e.preventDefault();
    this.setState({
      optionsArr: [],
      selOptions: [],
      questionsArr: [],
      showRRPopup: false,
      ratingComment: "",
      ratingHtml: "",
      ratingHtml2: "",
      task_rating: 5,
      task_id: 0,
      task_ref: "",
      req_name: "",
      rating_err: "",
    });
    this.getRequests(this.state.activePage);
    this.props.history.push({
      pathname: "/my_requests",
    });
    // axios
    //   .post("tasks/skip-ratings", {
    //     task_id: this.state.task_id,
    //   })
    //   .then((res) => {
    //     this.setState({
    //       optionsArr: [],
    //       selOptions: [],
    //       questionsArr: [],
    //       showRRPopup: false,
    //       ratingComment: "",
    //       ratingHtml: "",
    //       ratingHtml2: "",
    //       task_rating: 5,
    //       task_id: 0,
    //       task_ref: "",
    //       showLoader: false,
    //       rating_err: "",
    //     });
    //     this.getRequests();
    //     this.props.history.push({
    //       pathname: "/my_requests",
    //     });
    //   })
    //   .catch((err) => { });
  };

  openPendingPopup = (e) => {
    this.setState({ showThankyouPopup: false });
    e.preventDefault();
    axios
      .get("tasks/pending-rating", {
        task_id: this.state.task_id,
      })
      .then((res) => {
        if (res.data.single_entry === 1) {
          this.showRatingPopup(
            e,
            res.data.data[0].task_id,
            res.data.data[0].task_ref,
            res.data.data[0].req_name,
            res.data.data[0].task_language
          );
        } else {
          this.setState({ pending_requests: res.data.data }, () => {
            this.setState({ showPendingPopup: true });
          });
        }
      })
      .catch((err) => {});
    this.getRequests(this.state.activePage);
  };

  handlePendingClose = (e) => {
    //e.preventDefault();
    this.setState({ showPendingPopup: false });
    this.getRequests(this.state.activePage);
    this.props.history.push({
      pathname: "/my_requests",
    });
  };

  openThankyouPopup = () => {
    axios
      .get("tasks/count-pending-rating")
      .then((res) => {
        if (res.data.count > 0) {
          this.setState({ showThankyouPopup: true, pendingRatings: true });
        } else {
          this.setState({ showThankyouPopup: true, pendingRatings: false });
        }
      })
      .catch((err) => {});
    this.getRequests(this.state.activePage);
  };

  handleThankyouClose = (e) => {
    //e.preventDefault();
    this.setState({ showThankyouPopup: false });
    this.getRequests(this.state.activePage);
    this.props.history.push({
      pathname: "/my_requests",
    });
  };

  getLanguagePending = (pending) => {
    let lang_data = require(`../../assets/lang/${pending.task_language}.json`);

    return (
      <>
        <p>
          {lang_data.my_requests.task_ref} <strong> {pending.task_ref} </strong>
        </p>
        <p>
          {lang_data.my_requests.description}
          <strong> {pending.req_name}</strong>
        </p>
      </>
    );
  };

  getPendingText = (pending) => {
    let lang_data = require(`../../assets/lang/${pending.task_language}.json`);
    return lang_data.my_requests.rate_now
   }

  toggleCheckbox = (e) => {
    let final_array = this.state.finalchecked;
    console.log(this.state.finalchecked);
    if (
      final_array.indexOf(e.target.value) >= 0 &&
      e.target.checked === false
    ) {
      if (final_array.indexOf(e.target.value) >= 0) {
        final_array.splice(final_array.indexOf(e.target.value), 1);
      }
    } else if (
      !final_array.includes(e.target.value) &&
      e.target.checked === true
    ) {
      final_array.push(e.target.value);
    }
    this.setState({ finalchecked: final_array });
  };

  handleClose = () => {
    this.setState({
      ccList: "",
      showCcPopup: false,
      showMobileAlert: false,
      displayMobileText: "",
    });
  };

  handleCloseProgress = () => {
    this.setState({ showStatusPopup: false });
  };

  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    });
  }

  hideSuccessMsg = () => {
    message = "";
  };

  // ==== Autosuggest for Reference Query DRL ==== //
  onReferenceChange = (event, { newValue }) => {
    this.setState(
      {
        value: newValue,
      },
      () => console.log("value", this.state.value)
    );
  };

  // ==== Autosuggest for Reference Query DRL ==== //
  onSuggestionsFetchRequested = ({ value }) => {
    const inputValue = value.toLowerCase();
    const inputLength = inputValue.length;

    if (inputLength === 0) {
      return [];
    } else {
      if (inputLength > 0) {
        let userData = BasicUserData();
        if (userData.role === 2) {
          if (this.props.selCompany && this.props.selCompany.value > 0) {
            axios
              .get(
                `/request-search/${value}/?cid=${this.props.selCompany.value}`
              )
              .then((res) => {
                console.log("==========", res);
                this.setState({
                  suggestions: res.data.data,
                });
                return this.state.suggestions;
              })
              .catch((err) => {
                console.log("==========", err);
              });
          }
        } else {
          axios
            .get(`/request-search/${value}`)
            .then((res) => {
              console.log("==========", res);
              this.setState({
                suggestions: res.data.data,
              });
              return this.state.suggestions;
            })
            .catch((err) => {
              console.log("==========", err);
            });
        }
      }
    }
  };

  // ==== Autosuggest for Reference Query DRL ==== //
  onSuggestionsClearRequested = () => {
    console.log("clearrrr");
    this.setState({
      suggestions: [],
    });
  };

  getSuggestionValue = (suggestion) => {
    return suggestion.label;
  };

  renderSuggestion = (suggestion) => {
    return (
      <div>
        <Link to="#">{suggestion.label}</Link>
      </div>
    );
  };

  changeDateFormat = (input_date) => {
    var dateFormat = require("dateformat");
    var now = new Date(input_date);
    return dateFormat(input_date, "d mmm yyyy");
  };

  getTaskHighlight = (highlight_status) => {
    if (highlight_status) {
      if (highlight_status === 1) {
        return "row-circle";
      } else {
        return "";
      }
    } else {
      return "";
    }
  };

  getPreSelected = () => {
    if (this.state.ccList !== "" && this.state.ccList.preSelected.length > 0) {
      var html = this.state.ccList.preSelected.map((element, index) => {
        return (
          <li key={element.customer_id} style={{ listStyleType: "none" }}>
            <input
              id={`checkCC_${element.customer_id}`}
              type="checkbox"
              defaultChecked={true}
              disabled={true}
            />
            &nbsp;{element.first_name} {element.last_name} (
            {element.company_name})
          </li>
        );
      });
      return html;
    }
  };

  isListingHover = (targetName) => {
    return this.state[targetName] ? this.state[targetName].hoverTip : false;
  };

  toggleListingTip = (targetName) => {
    if (!this.state[targetName]) {
      this.setState({
        [targetName]: {
          hoverTip: true,
        },
      });
    } else {
      this.setState({
        ...this.state,
        [targetName]: {
          hoverTip: !this.state[targetName].hoverTip,
        },
      });
    }
  };

  getSubmittedBy = (request) => {
    if (request.submitted_by > 0) {
      return `${request.agent_fname} ${request.agent_lname}`;
    } else if (request.ccp_posted_by > 0) {
      let posted_from = "";
      // if(request.ccp_posted_with === 'S'){
      //   posted_from = '(Sales Force)'
      // }

      return `${request.emp_posted_by_fname} ${request.emp_posted_by_lname} ${posted_from}`;
    } else {
      return `${request.first_name} ${request.last_name}`;
    }
  };

  shareWithAgent = (event, setFieldValue) => {
    if (event.target.checked === true) {
      this.setState({ shared_with_agent: 1 });
      setFieldValue("share_with_agent", 1);
    } else {
      this.setState({ shared_with_agent: 0 });
      setFieldValue("share_with_agent", 0);
    }
  };

  clearAutoSuggest = () => {
    this.getRequests(1, "", null, this.state.query_string);
    this.setState({ value: "" });
  };

  openMobileView = (onBehalf) => {
    this.setState({
      showMobileAlert: true,
      displayMobileText:
        this.props.dynamic_lang.my_requests.raised_on_behalf +
        ` ${onBehalf} ` +
        this.props.dynamic_lang.my_requests.by_dr,
    });
  };

  openSideMenu = () => {
    this.props.showMenu(this.state.toggleMenuClass, () => {
      this.onMobileClose();
      this.setState({ toggleMenuClass: this.props.display_menu });
    });
  };

  createLink = (request) => {
    let ret;
    if (request.required_action == "No Action Required") {
      ret = request.required_action;
    } else {
      ret = (
        <Link
          to={{
            pathname: `/task-details/${request.task_id}`,
            state: { fromDashboard: true },
          }}
          style={{ cursor: "pointer" }}
        >
          {request.required_action}
        </Link>
      );
    }
    return ret;
  };

  searchDate = (e) => {
    let query_string = "";
    if (this.state.selectedFrom !== "" && this.state.selectedFrom !== null) {
      query_string += `fdt=${dateFormat(
        this.state.selectedFrom,
        "yyyy-mm-dd"
      )}&`;
    }
    if (this.state.selectedTo !== "" && this.state.selectedTo !== null) {
      query_string += `tdt=${dateFormat(this.state.selectedTo, "yyyy-mm-dd")}&`;
    }
    if (query_string !== "") {
      query_string = query_string.substring(0, query_string.length - 1);
      this.setState({ query_string: query_string, activePage: 1 });
      this.getRequests(1, this.state.searchkey, null, query_string);
    }
  };

  revertDateSearch = () => {
    this.setState({
      selectedFrom: "",
      selectedTo: "",
      query_string: "",
      activePage: 1,
    });
    this.getRequests(1, this.state.searchkey);
  };

  render() {
    const {
      value,
      suggestions,
      selectedCompany,
      tooltipOpen,
      tooltipOpen2,
    } = this.state;
    const inputProps = {
      placeholder: this.props.dynamic_lang.my_requests.keywords_attachments,
      value,
      type: "search",
      onChange: this.onReferenceChange,
      onKeyPress: this.enterPressed.bind(this),
      className:
        this.state.value.length > 0 ? "customInputLessBg" : "customInput",
    };
    console.log("=========", this.state.requestsList);
    return (
      <>
        <div className="container-fluid clearfix listingSec">
          <div className="dashboard-content-sec">
            <div className="col-md-12 form-search-section formSearchrequest">
              <ul className="mb-0">
                <li>
                  <DatePicker
                    className="form-control customInput"
                    name="fromDate"
                    showMonthDropdown
                    showYearDropdown
                    minDate={this.state.fromMaxDate}
                    maxDate={this.state.toMaxDate}
                    dropdownMode="select"
                    onChange={(e) => {
                      this.setState({ selectedFrom: e });
                    }}
                    selected={this.state.selectedFrom}
                    dateFormat="dd/MM/yyyy"
                    autoComplete="off"
                    placeholderText={
                      this.props.dynamic_lang.my_requests.from_date
                    }
                  />
                  {/* {this.state.selectedFrom !== '' && <b style={{cursor:'pointer'}} onClick={e=>{
                          this.setState({selectedFrom:''});
                        }} >X</b>} */}
                </li>
                <li>
                  <DatePicker
                    className="form-control customInput"
                    showMonthDropdown
                    showYearDropdown
                    minDate={this.state.fromMaxDate}
                    maxDate={this.state.toMaxDate}
                    dropdownMode="select"
                    onChange={(e) => {
                      this.setState({ selectedTo: e });
                    }}
                    selected={this.state.selectedTo}
                    dateFormat="dd/MM/yyyy"
                    autoComplete="off"
                    placeholderText={
                      this.props.dynamic_lang.my_requests.to_date
                    }
                  />
                  {/* {this.state.selectedTo !== '' && <b style={{cursor:'pointer'}} onClick={e=>{
                          this.setState({selectedTo:''});
                        }} >X</b>} */}
                </li>
                <li>
                  <Button
                    className={`btn-default m-r-10`}
                    onClick={(e) => this.searchDate(e)}
                  >
                    {this.props.dynamic_lang.my_requests.filter}
                  </Button>

                  {(this.state.selectedFrom != "" ||
                    this.state.selectedTo != "") && (
                    <Button
                      className={`btn btn-default btn-cancel`}
                      onClick={(e) => this.revertDateSearch(e)}
                    >
                      {this.props.dynamic_lang.my_requests.cancel}
                    </Button>
                  )}
                </li>
              </ul>
            </div>
            <div className="service-request-form-sec">
              {this.state.showLoader === true ? <Loader /> : null}
              {message !== null && message !== "" && (
                <div className="messagessuccess">
                  <Link to="#" className="close">
                    <img
                      src={closeIconSuccess}
                      width="17.69px"
                      height="22px"
                      onClick={(e) => this.hideSuccessMsg()}
                    />
                  </Link>
                  {message}
                </div>
              )}

              <div>
                <div className="row">
                  <div className="col-md-8 form-page-title">
                    <h2>{this.props.dynamic_lang.my_requests.header}</h2>
                    <Link
                      to="#"
                      className="plus-request"
                      onClick={(e) => this.openSideMenu(e)}
                    >
                      &nbsp;
                    </Link>
                  </div>
                  <div className="col-md-4 form-search-section formSearchrequest">
                    <ul className="mb-0">
                      <li>
                        <Autosuggest
                          autoComplete="off"
                          suggestions={suggestions}
                          onSuggestionsFetchRequested={
                            this.onSuggestionsFetchRequested
                          }
                          onSuggestionsClearRequested={
                            this.onSuggestionsClearRequested
                          }
                          getSuggestionValue={(suggestion) =>
                            this.getSuggestionValue(suggestion)
                          }
                          renderSuggestion={this.renderSuggestion}
                          inputProps={inputProps}
                        />
                        {this.state.value !== "" && (
                          <div className="close-icon-container">
                            <a
                              className="close-icon"
                              onClick={this.clearAutoSuggest}
                            >
                              <img
                                src={closeIconSelect}
                                width="10"
                                height="10"
                              />
                            </a>
                          </div>
                        )}
                      </li>
                    </ul>
                  </div>
                  <div className="clearfix" />
                </div>
                {this.state.isLoading === true ? (
                  <Loader />
                ) : (
                  <div className="listing-table table-responsive">
                    <table className="" style={{ width: "100%" }}>
                      <thead>
                        <tr>
                          <th>
                            {this.props.dynamic_lang.my_requests.request_number}
                          </th>
                          <th>
                            {this.props.dynamic_lang.my_requests.request_title}
                          </th>
                          <th>{this.props.dynamic_lang.my_requests.product}</th>
                          <th>
                            {this.props.dynamic_lang.my_requests.logged_date}
                          </th>
                          <th>
                            {this.props.dynamic_lang.my_requests.submitted_by}
                          </th>
                          <th>
                            {
                              this.props.dynamic_lang.my_requests
                                .expected_closure_date
                            }
                          </th>
                          <th>{this.props.dynamic_lang.my_requests.status}</th>
                          <th>
                            {
                              this.props.dynamic_lang.my_requests
                                .actual_closure_date
                            }
                          </th>
                          <th>
                            {
                              this.props.dynamic_lang.my_requests
                                .action_requested
                            }
                          </th>
                          <th>
                            {this.props.dynamic_lang.my_requests.feedback}
                          </th>
                          <th> </th>
                          <th> </th>
                          {/* <th>Action</th> */}
                        </tr>
                      </thead>
                      <tbody>
                        {this.state.requestsList.map((request, key) => (
                          <tr
                            key={key}
                            className={this.getTaskHighlight(
                              request.highlight_status
                            )}
                          >
                            <td>
                              <Link
                                to={{
                                  pathname: `/task-details/${request.task_id}`,
                                }}
                                style={{ cursor: "pointer" }}
                              >
                                {request.task_ref}
                              </Link>
                            </td>
                            <td>{request.req_name}</td>
                            <td>{htmlDecode(request.product_name)}</td>
                            <td>{request.date_added}</td>

                            <td>
                              {/* //SATYAJIT */}
                              {isMobile ? (
                                <span
                                  id={`tip-${request.task_id}`}
                                  onClick={() =>
                                    (request.submitted_by > 0 ||
                                      request.ccp_posted_by > 0) &&
                                    this.openMobileView(
                                      `${request.first_name} ${request.last_name}`
                                    )
                                  }
                                >
                                  {this.getSubmittedBy(request)}
                                </span>
                              ) : (
                                <>
                                  <span id={`tip-${request.task_id}`}>
                                    {this.getSubmittedBy(request)}
                                  </span>

                                  {(request.submitted_by > 0 ||
                                    request.ccp_posted_by > 0) && (
                                    <Tooltip
                                      placement="right"
                                      isOpen={this.isListingHover(
                                        `tip-${request.task_id}`
                                      )}
                                      autohide={false}
                                      target={`tip-${request.task_id}`}
                                      toggle={() =>
                                        this.toggleListingTip(
                                          `tip-${request.task_id}`
                                        )
                                      }
                                      className="listingTip"
                                    >
                                      {this.explicitText(request,3)}{" "}
                                      {`${request.first_name} ${request.last_name}`}{" "}
                                      {this.explicitText(request,4)}{" "}
                                      {this.getSubmittedBy(request)}
                                    </Tooltip>
                                  )}
                                </>
                              )}
                            </td>
                            <td>
                              {request.expected_closure_date != null
                                ? request.expected_closure_date
                                : ""}
                            </td>
                            <td>
                              {request.status != null ? (
                                <Link
                                  to="#"
                                  className=""
                                  style={{ cursor: "pointer" }}
                                  onClick={() =>
                                    this.showStatus(request.task_id)
                                  }
                                >
                                  {request.status}
                                </Link>
                              ) : (
                                ""
                              )}
                            </td>
                            <td>
                              {request.closed_date != null
                                ? request.closed_date
                                : ""}
                            </td>
                            <td>
                              {request.required_action != null
                                ? this.createLink(request)
                                : ""}
                            </td>
                            <td className="ratnow W130">
                            <div className="d-flex">
                              {this.getRatings(request)}
                            </div>
                            </td>
                            <td>
                              <Link
                                to={{
                                  pathname: `/task-details/${request.task_id}`,
                                  state: { fromDashboard: true },
                                }}
                                className="action-btn"
                              >
                                {this.explicitText(request,1)}
                              </Link>

                              
                            </td>
                            <td>
                              <Link
                                to="#"
                                className="action-btn"
                                onClick={() => this.showCCList(request.task_id,request.task_language)}
                              >
                                {this.explicitText(request,2)}
                              </Link>
                            </td>
                            {/* <td>Germany</td> */}
                          </tr>
                        ))}
                        {this.state.requestsList.length === 0 && (
                          <tr>
                            <td colSpan="11" align="center">
                              {
                                this.props.dynamic_lang.my_requests
                                  .no_data_display
                              }
                            </td>
                          </tr>
                        )}
                      </tbody>
                    </table>
                  </div>
                )}
              </div>

              <div className="clearfix" />

              {this.state.count_req > this.state.itemPerPage ? (
                <div className="pagination-area">
                  <div className="paginationOuter text-right">
                    <Pagination
                      hideDisabled
                      hideFirstLastPages
                      prevPageText="‹‹"
                      nextPageText="››"
                      activePage={this.state.activePage}
                      itemsCountPerPage={this.state.itemPerPage}
                      totalItemsCount={this.state.count_req}
                      itemClass="nav-item"
                      linkClass="nav-link"
                      activeClass="active"
                      pageRangeDisplayed="1"
                      onChange={this.handlePageChange}
                    />
                  </div>
                </div>
              ) : null}

              {/* <div className="pagination-area">
                <ul className="pager">
                  <li>
                    <Link to="/" title="Go to previous page" >
                    <span aria-hidden="true">‹‹</span>
                    </Link>
                  </li>
                  <li> Page 2 </li>
                  <li>
                    <Link to="/" title="Go to next page">
                    <span aria-hidden="true">››</span>
                    </Link>
                  </li>
                </ul>
            </div> */}
              <div className="clearfix" />
            </div>
          </div>
        </div>

        {this.state.showCcPopup === true && (
          <Modal
            show={this.state.showCcPopup}
            onHide={this.handleClose}
            backdrop="static"
            className="grantmodal"
          >
            <Formik
              initialValues={false}
              validationSchema={false}
              onSubmit={this.handleSubmitAssignTask}
            >
              {({
                values,
                errors,
                touched,
                isValid,
                isSubmitting,
                handleChange,
                setFieldValue,
                setFieldTouched,
                setErrors,
              }) => {
                return (
                  <Form>
                    <Modal.Header>
                      <Modal.Title>
                        <span
                          className="fieldset-legend"
                          id="DisabledAutoHideExample"
                        >
                          {
                            this.state.cc_lang.my_requests
                              .grant_access_request
                          }
                          {/* <OverlayTrigger 
                            overlay={
                              <Tooltip id="tooltip-disabled">{this.state.cc_lang.my_requests.grant_access_request_tooltip}</Tooltip>
                            }
                          >
                            <a href="#" className="infoIcon">
                              <img className="m-l-5" src={infoIcon} alt="" width="15.44" height="18"/>
                            </a>
                          </OverlayTrigger> */}
                          <img
                            src={infoIcon}
                            width="15.44"
                            height="18"
                            id="DisabledAutoHideExample"
                            type="button"
                          />
                          <Tooltip
                            placement="right"
                            isOpen={this.state.tooltipOpen}
                            autohide={false}
                            target="DisabledAutoHideExample"
                            toggle={this.toggle}
                          >
                            {
                              this.state.cc_lang.my_requests
                                .grant_access_request_tooltip
                            }
                          </Tooltip>
                        </span>
                      </Modal.Title>
                      <button className="close" onClick={this.handleClose} />
                    </Modal.Header>
                    <Modal.Body>
                      <div className="contBox">
                        <Row>
                          <ul>
                            {this.state.ccList && this.getPreSelected()}
                            {this.state.ccList && this.makeDynamicHtml()}
                          </ul>
                        </Row>
                        {this.state.shared_with_agent_show ? (
                          <div className="form-check m-b-25">
                            <label className="form-check-label">
                              <input
                                type="checkbox"
                                className="form-check-input"
                                //value={true}
                                defaultChecked={this.state.shared_with_agent}
                                onChange={(e) =>
                                  this.shareWithAgent(e, setFieldValue)
                                }
                              />
                              {
                                this.state.cc_lang.my_requests
                                  .display_request_agent
                              }
                            </label>
                          </div>
                        ) : (
                          ""
                        )}
                      </div>

                      <ButtonToolbar>
                        <Button
                          className={`submit btn-fill mycanclbtn pdbtn ${
                            isValid ? "btn-custom-green" : "btn-disable"
                          }`}
                          type="submit"
                        >
                          {this.state.cc_lang.my_requests.cancel}
                        </Button>

                        <Button
                          className={`submit btn-fill pdbtn ${
                            isValid ? "btn-custom-green" : "btn-disable"
                          }`}
                          type="submit"
                          //disabled={isValid ? false : true}
                        >
                          {this.state.cc_lang.my_requests.submit}
                        </Button>
                      </ButtonToolbar>
                    </Modal.Body>
                    {/* <Modal.Footer>
                      
                    </Modal.Footer> */}
                  </Form>
                );
              }}
            </Formik>
          </Modal>
        )}

        {/* RATING POPUP */}
        {this.state.showRRPopup === true && (
          <Modal
            show={this.state.showRRPopup}
            onHide={this.handleRRClose}
            backdrop="static"
            className="modalRating"
          >
            <Modal.Header>
              <div className="cntrimg">
                <img src={require("../../assets/images/rat-2.svg")} alt="" />
              </div>

              {/* <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Empty </Tooltip>}>
                <ButtonToolbar>
               
                </ButtonToolbar>
              </OverlayTrigger> */}

              <img
                src={closeIconSelect}
                width="15.44"
                height="18"
                id="DisabledAutoHideExample"
                type="button"
                onClick={(e) => this.handleRRClose(e)}
                className="close"
              />
              <Tooltip
                placement="left"
                isOpen={tooltipOpen2}
                autohide={false}
                target="DisabledAutoHideExample"
                toggle={this.toggle2}
              >
                {this.state.popup_lang.my_requests.give_rating}
              </Tooltip>

              {/* <Button
                className="close"
                onClick={(e) => this.handleRRClose(e)}
              /> */}
            </Modal.Header>
            <Formik
              initialValues={false}
              validationSchema={false}
              onSubmit={this.handleRatings}
            >
              {({
                values,
                errors,
                touched,
                isValid,
                isSubmitting,
                handleChange,
                setFieldValue,
                setFieldTouched,
                setErrors,
              }) => {
                return (
                  <Form>
                    <Modal.Body>
                      <Modal.Title>
                        <div className="ratinfo">
                          <h4>
                            {
                              this.state.popup_lang.my_requests
                                .overall_experience
                            }{" "}
                            {this.state.req_name}-{" "}
                            <Link
                              to={{
                                pathname: `/task-details/${this.state.task_id}`,
                              }}
                              // style={{
                              //   cursor: "pointer",
                              //   color: "#0056b3",
                              //   textDecoration: "underline",
                              //   fontSize: "19px"
                              // }}
                            >
                              {this.state.task_ref}{" "}
                            </Link>
                          </h4>
                          <h3></h3>
                        </div>
                      </Modal.Title>
                      <div className="ratinfo starimg">
                        <Rating
                          initialRating={this.state.task_rating}
                          onChange={(rate) => this.changeQuestion(rate)}
                          emptySymbol={
                            <img
                              src={require("../../assets/images/uncheck-star.svg")}
                              alt=""
                              className="icon"
                            />
                          }
                          placeholderSymbol={
                            <img
                              src={require("../../assets/images/uncheck-stars.svg")}
                              alt=""
                              className="icon"
                            />
                          }
                          fullSymbol={
                            <img
                              src={require("../../assets/images/uncheck-stars.svg")}
                              alt=""
                              className="icon"
                            />
                          }
                        />
                        {this.state.rating_err && (
                          <p
                            style={{
                              color: "red",
                              fontSize: "14px",
                              marginBottom: "0px",
                              marginTop: "4px",
                            }}
                          >
                            {this.state.popup_lang.my_requests.forgot_to_rate}
                          </p>
                        )}
                        <textarea
                          className="inputratng"
                          placeholder={htmlDecode(this.state.ratingHtml)}
                          onChange={(e) => {
                            this.setState({ ratingComment: e.target.value });
                          }}
                        ></textarea>
                        <br />
                        <center>
                          <Row className="checkmargin">
                            {this.state.ratingHtml2}
                          </Row>
                        </center>

                        <ButtonToolbar className="txtcntr">
                          <Button
                            className={` submit m-b-0 btn-fill ${
                              isValid ? "btn-custom-green" : "btn-disable"
                            } m-r-10`}
                            type="submit"
                            disabled={this.state.task_rating > 0 ? false : true}
                          >
                            {this.state.popup_lang.my_requests.submit}
                          </Button>
                        </ButtonToolbar>
                      </div>
                    </Modal.Body>
                    {/* <Modal.Footer>
                        
                      </Modal.Footer> */}
                  </Form>
                );
              }}
            </Formik>
          </Modal>
        )}

        {/* PENDING POPUP */}
        {this.state.showPendingPopup === true && (
          <Modal
            show={this.state.showPendingPopup}
            onHide={this.handlePendingClose}
            backdrop="static"
            className="modlscroll"
          >
            <Modal.Header>
              <div className="cntrimg">
                <img src={require("../../assets/images/rat-2.svg")} alt="" />
              </div>

              <img
                src={closeIconSelect}
                width="15.44"
                height="18"
                id="DisabledAutoHideExample"
                type="button"
                onClick={(e) => this.handlePendingClose(e)}
                className="close"
              />
              <Tooltip
                placement="left"
                isOpen={tooltipOpen2}
                autohide={false}
                target="DisabledAutoHideExample"
                toggle={this.toggle2}
              >
                {this.props.dynamic_lang.my_requests.give_rating}
              </Tooltip>
              {/* <button
                className="close"
                onClick={(e) => this.handlePendingClose(e)}
              /> */}
            </Modal.Header>

            <Modal.Body>
              <div className="content-wrapper">
                <section className="content-header">
                  <div>
                    <div>
                      {/* <div className="d-flex justify-content-between"> */}
                      <Row>
                        <Col sm={6}>
                          <div className="past">
                            <p>
                              {
                                this.props.dynamic_lang.my_requests
                                  .pending_feedback
                              }{" "}
                              <span className="bedge">
                                {this.state.pending_requests.length}
                              </span>
                            </p>
                          </div>
                        </Col>
                        {/* <Col sm={4}>
                          <div className="feedbck">
                            {" "}
                            <p>
                              Feedback Due{" "}
                              <span className="bedge">
                                {this.state.pending_requests.length}
                              </span>
                            </p>
                          </div>
                        </Col> */}
                      </Row>
                      {/* </div> */}
                      {/* <div className="revbox">
                                    <div className="p-d-15"> */}
                      {this.state.pending_requests.map((pending, i) => {
                        return (
                          <div className="revbox">
                            <div className="p-d-15">
                              <Row>
                                <Col sm={8}>
                                  <div className="enqnmbr">
                                    {this.getLanguagePending(pending)}
                                  </div>
                                </Col>
                                <Col sm={4}>
                                  {/* <div className="pndngfeedbck"> */}
                                  <div className="pndng">
                                    {/* <p>Pending feedback 
                                                      <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Empty</Tooltip>}>
                                                          <a href="#" className="infoIcon"><img src={require('../../assets/images/tooltip-i.svg')} alt=""
                                                          className="m-l-5"/></a>
                                                          </OverlayTrigger>
                                                      </p> */}
                                    <Link
                                      className="ratebtn"
                                      to="#"
                                      onClick={(e) =>
                                        this.showRatingPopup(
                                          e,
                                          pending.task_id,
                                          pending.task_ref,
                                          pending.req_name,
                                          pending.task_language
                                        )
                                      }
                                    >
                                      {this.getPendingText(pending)}
                                    </Link>
                                  </div>
                                  {/* </div> */}
                                </Col>
                              </Row>
                            </div>
                          </div>
                        );
                      })}
                      ;{/* </div>
                                </div> */}
                    </div>
                  </div>
                </section>
              </div>
            </Modal.Body>
          </Modal>
        )}

        {/* THANK YOU POPUP */}
        {this.state.showThankyouPopup === true && (
          <Modal
            show={this.state.showThankyouPopup}
            onHide={this.handleThankyouClose}
            backdrop="static"
            className="modalthnx"
          >
            <Modal.Header>
              <div className="thankuhead">
                <img
                  src={require("../../assets/images/thank-You-1.svg")}
                  alt=""
                />
                <button
                  className="close"
                  onClick={(e) => this.handleThankyouClose(e)}
                />
                <p className="thankutxt">
                  {this.state.popup_lang.my_requests.thank_you}
                </p>
              </div>
            </Modal.Header>

            <Modal.Body>
              <div className="infopara">
                <p>{this.state.popup_lang.my_requests.sharing_feedback}</p>
                <p>{this.state.popup_lang.my_requests.overall_process}</p>
                {this.state.pendingRatings && (
                  <>
                    <p className="txtcntr">
                      {this.state.popup_lang.my_requests.pending_reviews}
                    </p>
                    <div className="d-flex justify-content-center infopara">
                      <Link to="#" onClick={(e) => this.openPendingPopup(e)}>
                        {this.state.popup_lang.my_requests.share_your_feedback}
                      </Link>
                    </div>
                  </>
                )}
              </div>
            </Modal.Body>
          </Modal>
        )}

        <Modal
          show={this.state.showStatusPopup}
          onHide={this.handleCloseProgress}
          backdrop="static"
        >
          <Modal.Header>
            <button className="close" onClick={this.handleCloseProgress} />
          </Modal.Header>
          <Modal.Body>
            {this.state.request_type === 23 && this.state.request_type !== 24 && (
              <ul className="progressbar">
                {this.state.order_status
                  ? this.state.order_status.map((player, i) => {
                      return (
                        <li
                          key={i}
                          className={
                            this.state.status_active &&
                            this.state.status_active.length > 0 &&
                            this.state.status_active.includes(player)
                              ? "active"
                              : ""
                          }
                        >
                          {player}
                        </li>
                      );
                      // -------------------^^^^^^^^^^^---------^^^^^^^^^^^^^^
                    })
                  : ""}
              </ul>
            )}
            {this.state.request_type === 7 && this.state.request_type !== 24 && (
              <ul className="progressbar">
                {this.state.complaint_status
                  ? this.state.complaint_status.map((player, i) => {
                      return (
                        <li
                          key={i}
                          className={
                            this.state.status_active &&
                            this.state.status_active.length > 0 &&
                            this.state.status_active.includes(player)
                              ? "active"
                              : ""
                          }
                        >
                          {player}
                        </li>
                      );
                      // -------------------^^^^^^^^^^^---------^^^^^^^^^^^^^^
                    })
                  : ""}
              </ul>
            )}

            {this.state.request_type !== 7 &&
              this.state.request_type !== 23 &&
              this.state.request_type !== 24 && (
                <ul className="progressbar">
                  {this.state.other_status
                    ? this.state.other_status.map((player, i) => {
                        return (
                          <li
                            key={i}
                            className={
                              this.state.status_active &&
                              this.state.status_active.length > 0 &&
                              this.state.status_active.includes(player)
                                ? "active"
                                : ""
                            }
                          >
                            {player}
                          </li>
                        );
                        // -------------------^^^^^^^^^^^---------^^^^^^^^^^^^^^
                      })
                    : ""}
                </ul>
              )}
          </Modal.Body>
        </Modal>

        <Modal
          show={this.state.showMobileAlert}
          onHide={this.handleClose}
          backdrop="static"
        >
          <Modal.Header>
            <button className="close" onClick={this.handleClose} />
          </Modal.Header>
          <Modal.Body>{this.state.displayMobileText}</Modal.Body>
        </Modal>
      </>
    );
  }
}

// SATYAJIT
const mapStateToProps = (state) => {
  //console.log('masp state to props', state);
  return {
    display_menu: state.menu.display,
    task_status_arr: state.user.task_status_arr,
    selCompany: state.user.selCompany,
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    showMenu: (data, onSuccess) => dispatch(showSidebarMenu(data, onSuccess)),
    getTaskStatus: (data, onSuccess) => dispatch(getTStatus(data, onSuccess)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyRequests);
