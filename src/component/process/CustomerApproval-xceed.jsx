import React, { Component } from 'react';
import Axios from 'axios';
import { Row, Col, Button, ButtonToolbar } from 'react-bootstrap';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import Select from 'react-select';
import swal from 'sweetalert';

import Loader from '../Loader/loader';

let initialValues = {
    assign_to: '',
};

const validationSchema = (refObj) =>
    Yup.object().shape({
        assign_to: Yup.object().required('Please select a team member'),
    });

class CustomerApproval extends Component {
    state = {
        showLoader: false,
        token: '',
        errMsg: null,
        successMsg: '',
        type: '', // 1 = Approval, 2 = Assignment
        customerDetails: {},
        teamList: [],
    };

    componentDidMount() {
        this.handleValidateToken();
    }

    handleValidateToken = () => {
        const config = {
            baseURL: process.env.REACT_APP_API_URL,
            headers: { Authorization: `Bearer ${this.props.match.params.token}` },
        };

        Axios.get('/process_validate_token', config)
            .then((res) => {
                if (res.data && res.data.status === 1) {
                    this.setState({
                        showLoader: false,
                        type: res.data.type,
                        customerDetails: res.data.customerDetails,
                        token: res.data.token,
                    });
                } else {
                    this.setState({
                        showLoader: false,
                        errMsg:
                            res.data && res.data.message
                                ? res.data.message
                                : 'Something went wrong, please try again later!',
                    });
                }
            })
            .catch((err) => {
                if (err.response && err.response.status === 401) {
                    this.setState({
                        showLoader: false,
                        errMsg: err.data && err.data.message ? err.data.message : 'Invalid Access.',
                    });
                } else {
                    this.setState({
                        showLoader: false,
                        errMsg:
                            err.data && err.data.message
                                ? err.data.message
                                : 'Something went wrong, please try again later!',
                    });
                }
                console.log({ err });
            });
    };

    handleConfirmApproval = (assign_id) => {
        swal({
            closeOnClickOutside: false,
            title: 'Request review',
            text: 'Are you sure you want to approve this user ?',
            icon: 'warning',
            buttons: true,
            dangerMode: true,
        }).then((didApprove) => {
            if (didApprove) {
                this.setState({ showLoader: true });
                this.handleApproval(assign_id);
            }
        });
    };

    handleApproval = () => {
        if (this.props.match.params.token) {
            const config = {
                baseURL: process.env.REACT_APP_API_URL,
                headers: { Authorization: `Bearer ${this.props.match.params.token}` },
            };

            Axios.get('/process_customer_approval', config)
                .then((res) => {
                    if (res.data && res.data.status === 1) {
                        if (res.data.type === 2) {
                            this.setState({
                                showLoader: false,
                                type: res.data.type,
                                token: res.data.token,
                            });
                        } else {
                            this.setState({
                                showLoader: false,
                                successMsg: res.data.message ? res.data.message : 'Document Successfully Approved!',
                            });
                        }
                    } else {
                        this.setState({
                            showLoader: false,
                            errMsg:
                                res.data && res.data.message
                                    ? res.data.message
                                    : 'Something went wrong, please try again later!',
                        });
                    }
                })
                .catch((err) => {
                    if (err.response && err.response.status === 401) {
                        this.setState({
                            showLoader: false,
                            errMsg: err.data && err.data.message ? err.data.message : 'Invalid Access.',
                        });
                    } else {
                        this.setState({
                            showLoader: false,
                            errMsg:
                                err.data && err.data.message
                                    ? err.data.message
                                    : 'Something went wrong, please try again later!',
                        });
                    }
                    console.log({ err });
                });
        }
    };

    handleSubmitEvent = (values, actions) => {
        const { token } = this.state;

        if (token) {
            const config = {
                baseURL: process.env.REACT_APP_API_URL,
                headers: { Authorization: `Bearer ${token}` },
            };

            const payload = {
                reason_of_rejection: values.reason_of_rejection,
            };

            Axios.post('/process_customer_rejection', payload, config)
                .then((res) => {
                    actions.setSubmitting(false);
                    if (res.data && res.data.status === 1) {
                        this.setState({
                            showLoader: false,
                            showForm: false,
                            successMsg: res.data.message ? res.data.message : 'Document Successfully Rejected!',
                        });
                        actions.resetForm();
                    } else {
                        this.setState({
                            showLoader: false,
                            errMsg:
                                res.data && res.data.message
                                    ? res.data.message
                                    : 'Something went wrong, please try again later!',
                        });
                    }
                })
                .catch((err) => {
                    actions.setSubmitting(false);
                    if (err.response && err.response.status === 401) {
                        this.setState({
                            showLoader: false,
                            errMsg: err.data && err.data.message ? err.data.message : 'Invalid Access.',
                        });
                    } else {
                        this.setState({
                            showLoader: false,
                            errMsg:
                                err.data && err.data.message
                                    ? err.data.message
                                    : 'Something went wrong, please try again later!',
                        });
                    }
                    console.log({ err });
                });
        }
    };

    customerAssignForm = (customerDetails) => {
        return (
            <div className='content-wrapper qaWrapper newFormSection'>
                <section className='content'>
                    <div className='boxPapanel content-padding form-min-hight'>
                        <div className='taskdetails'>
                            { this.showCustomerDetails(customerDetails)}
                            <div className='clearfix tdBtmlist'>
                                <Formik
                                    initialValues={initialValues}
                                    validationSchema={validationSchema(this)}
                                    onSubmit={this.handleSubmitEvent}
                                >
                                    {({ values, errors, touched, setFieldValue, isSubmitting, setFieldTouched }) => {
                                        return (
                                            <div className='messageBox rejectionForm'>
                                                {/* <h1>Assign Team</h1> */}
                                                <Form>
                                                    <Row className='eaditRow'>
                                                        <Col xs={12} sm={12} md={12}>
                                                            <div className='form-group'>
                                                                <h1>Assign</h1>
                                                                {/* <label>
                                                                Assign To
                                                                <span class='required-field'>*</span>
                                                                </label> */}
                                                                <Select
                                                                    className='basic-single'
                                                                    classNamePrefix='select'
                                                                    name='assign_to'
                                                                    options={this.state.teamList}
                                                                    value={values.assign_to}
                                                                    onChange={(e) => {
                                                                        if (e) {
                                                                            setFieldValue('assign_to', e);
                                                                        } else {
                                                                            setFieldValue('assign_to', '');
                                                                        }
                                                                    }}
                                                                    onBlur={() => setFieldTouched('assign_to')}
                                                                />

                                                                {errors.assign_to && touched.assign_to ? (
                                                                    <span className='errorMsg'>{errors.assign_to}</span>
                                                                ) : null}
                                                            </div>
                                                        </Col>
                                                    </Row>
                                                    <Row>
                                                        <Col>
                                                            {errors.message ? (
                                                                <span className='errorMsg'>{errors.message}</span>
                                                            ) : null}
                                                        </Col>
                                                    </Row>
                                                    <ButtonToolbar>
                                                        <Button className='jcustomBtn' type='submit'>
                                                            {isSubmitting ? 'Submitting...' : 'Submit'}
                                                        </Button>
                                                    </ButtonToolbar>
                                                </Form>
                                            </div>
                                        );
                                    }}
                                </Formik>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    };

    customerApproveForm = (customerDetails) => {
        return (
            <div className='content-wrapper qaWrapper newFormSection'>
                <section className='content'>
                    <div className='boxPapanel content-padding form-min-hight'>
                        <div className='taskdetails'>
                            { this.showCustomerDetails (customerDetails)}
                            <div className='clearfix tdBtmlist'>
                                <Formik
                                    initialValues={{}}
                                    validationSchema={{}}
                                    onSubmit={this.handleConfirmApproval}
                                >
                                    {({ errors, isSubmitting }) => {
                                        return (
                                            <div className='messageBox rejectionForm'>
                                                <h1>Approve</h1>
                                                <Form>
                                                    <Row>
                                                        <Col>
                                                            {errors.message ? (
                                                                <span className='errorMsg'>{errors.message}</span>
                                                            ) : null}
                                                        </Col>
                                                    </Row>
                                                    <ButtonToolbar>
                                                        <Button className='jcustomBtn' type='submit'>
                                                            Approve
                                                        </Button>
                                                    </ButtonToolbar>
                                                </Form>
                                            </div>
                                        );
                                    }}
                                </Formik>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }

    customerErrorMessage = (errMsg) => {
        return (
            <div className='messageBox'>
                <img src={require('../../assets/images/invalid-file.png')} /> <h1>{errMsg}</h1>
            </div>
        );
    };

    customerSuccessMessage = (successMsg) => {
        return (
            <div className='messageBox successBox'>
                <img src={require('../../assets/images/successful.png')} /> <h1>{successMsg}</h1>
            </div>
        );
    };

    showCustomerDetails = (customerDetails) => {
        if (customerDetails && Object.keys(customerDetails).length > 0) {
            return (
                <div className='contBox'>
                    <Row>
                        <Col xs={12} sm={12} md={12}>
                            <div className='form-group'>
                                <table className='table table-bordered'>
                                    <tr>
                                        <td>User Name</td>
                                        <td>
                                            {customerDetails.first_name} {customerDetails.last_name}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Customer Name</td>
                                        <td>{customerDetails.company_name}</td>
                                    </tr>
                                    <tr>
                                        <td>Email</td>
                                        <td>{customerDetails.email}</td>
                                    </tr>
                                    <tr>
                                        <td>Phone no</td>
                                        <td>{customerDetails.phone_no}</td>
                                    </tr>
                                    <tr>
                                        <td>Country</td>
                                        <td>{customerDetails.country_name}</td>
                                    </tr>
                                    <tr>
                                        <td>Language</td>
                                        <td>{customerDetails.language}</td>
                                    </tr>

                                    <tr>
                                        <td>Key Account</td>
                                        <td>{customerDetails.vip_customer == 1 ? 'Yes' : 'No'}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>{customerDetails.status == 1 ? 'Active' : 'Inactive'}</td>
                                    </tr>
                                    <tr>
                                        <td>User Type</td>
                                        <td>{customerDetails.customer_type}</td>
                                    </tr>
                                    <tr>
                                        <td>Assign Team</td>
                                        <td>
                                            {customerDetails.teams &&
                                                customerDetails.teams.map((answer, i) => {
                                                    return (
                                                        <p>
                                                            {answer.first_name} {answer.last_name} ({answer.desig_name}){' '}
                                                        </p>
                                                    );
                                                })}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Is Admin</td>
                                        <td>{customerDetails.role_type == 0 ? 'Yes' : 'No'}</td>
                                    </tr>
                                    <tr>
                                        <td>Disable Notifications</td>
                                        <td>{customerDetails.dnd == 1 ? 'Yes' : 'No'}</td>
                                    </tr>
                                    <tr>
                                        <td>Multi Language Access</td>
                                        <td>{customerDetails.multi_lang_access == 1 ? 'Yes' : 'No'}</td>
                                    </tr>
                                    {customerDetails.role_type != 0 && (
                                        <>
                                            <tr>
                                                <td>Role</td>
                                                <td>
                                                    {customerDetails.role &&
                                                        customerDetails.role.map((rl, j) => {
                                                            return <p>{rl.role_name} </p>;
                                                        })}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Role Type</td>
                                                <td>{customerDetails.role_type == 1 ? 'Admin' : 'Regular'}</td>
                                            </tr>
                                        </>
                                    )}
                                </table>
                            </div>
                        </Col>
                    </Row>
                </div>
            );
        }
        return <div className='contBox'>Invalid Data Provided</div>;
    };

    render() {
        const { showLoader, successMsg, errMsg, type, customerDetails } = this.state;

        return (
            <div className='content-wrapper qaWrapper newFormSection'>
                <div className='logoWrapper'>
                    <a href={process.env.REACT_APP_PORTAL_URL} target='_blank'>
                        <img src={require('../../assets/images/logo-blue.jpg')} />
                    </a>
                </div>

                {showLoader && <Loader />}

                {errMsg && this.customerErrorMessage(errMsg)}

                {successMsg && this.customerSuccessMessage(successMsg)}

                {type === 1 && this.customerApproveForm(customerDetails)}

                {type === 2 && this.customerAssignForm(customerDetails)}

                <div className='footerCopy'>
                    <p>© 2021 Dr. Reddy’s Laboratories Ltd. All rights reserved(v2.9.0)</p>
                </div>
            </div>
        );
    }
}

export default CustomerApproval;
