import React, { Component } from 'react';
import Axios from 'axios';
import { Row, Col, Button, ButtonToolbar } from 'react-bootstrap';
import { Formik, Field, Form } from 'formik';
import * as Yup from 'yup';
import swal from 'sweetalert';

import Loader from '../Loader/loader';
import { connect } from 'react-redux';

let initialValues = {
    reason_of_rejection: '',
};

const validationSchema = (refObj) =>
    Yup.object().shape({
        reason_of_rejection: Yup.string()
            .min(2, 'Reason For Rejection can be minimum 2 digits long')
            .max(200, 'Reason For Rejection can be maximum 200 characters long')
            .required('Please Enter Reason For Rejection'),
    });

class CustomerResponse extends Component {
    state = {
        showLoader: true,
        token: '',
        errMsg: null,
        successMsg: '',
        showForm: false,
    };

    componentDidMount() {
        if (this.props.match.params.token) {
            const config = {
                baseURL: process.env.REACT_APP_API_URL,
                headers: { Authorization: `Bearer ${this.props.match.params.token}` },
            };

            Axios.get('/process_customer_response', config)
                .then((res) => {
                    if (res.data && res.data.status === 1) {
                        if (res.data.need_form === 1) {
                            this.setState({
                                showLoader: false,
                                showForm: true,
                                token: res.data.token,
                            });
                        } else {
                            this.setState({
                                showLoader: false,
                                successMsg: res.data.message ? res.data.message : 'Document Successfully Approved!'
                            });
                        }
                    } else {
                        this.setState({
                            showLoader: false,
                            errMsg:
                                res.data && res.data.message
                                    ? res.data.message
                                    : 'Something went wrong, please try again later!',
                        });
                    }
                })
                .catch((err) => {
                    if (err.response && err.response.status === 401) {
                        this.setState({
                            showLoader: false,
                            errMsg: err.data && err.data.message ? err.data.message : "Invalid Access."
                        });
                    } else {
                        this.setState({
                            showLoader: false,
                            errMsg:
                                err.data && err.data.message
                                    ? err.data.message
                                    : 'Something went wrong, please try again later!',
                        });
                    }
                    console.log({ err });
                });
        }
    }

    handleSubmitEvent = (values, actions) => {
        const { token } = this.state;

        if (token) {
            const config = {
                baseURL: process.env.REACT_APP_API_URL,
                headers: { Authorization: `Bearer ${token}` },
            };

            const payload = {
                reason_of_rejection: values.reason_of_rejection,
            };

            Axios.post('/process_customer_rejection', payload, config)
                .then((res) => {
                    actions.setSubmitting(false);
                    if (res.data && res.data.status === 1) {
                        this.setState({
                            showLoader: false,
                            showForm: false,
                            successMsg: res.data.message ? res.data.message : 'Document Successfully Rejected!'
                        });
                        actions.resetForm();
                    } else {
                        this.setState({
                            showLoader: false,
                            errMsg:
                                res.data && res.data.message
                                    ? res.data.message
                                    : 'Something went wrong, please try again later!',
                        });
                    }
                })
                .catch((err) => {
                    actions.setSubmitting(false);
                    if (err.response && err.response.status === 401) {
                        this.setState({
                            showLoader: false,
                            errMsg: err.data && err.data.message ? err.data.message : "Invalid Access."
                        });
                    } else {
                        this.setState({
                            showLoader: false,
                            errMsg:
                                err.data && err.data.message
                                    ? err.data.message
                                    : 'Something went wrong, please try again later!',
                        });
                    }
                    console.log({ err });
                });
        }
    };

    customerRejectionForm = () => {
        return (
            <div className='content-wrapper qaWrapper newFormSection'>
                <div className="logoWrapper"> <a
                          href={process.env.REACT_APP_PORTAL_URL}
                          target="_blank"
                        >
                          <img
                            src={require("../../assets/images/logo-blue.jpg")}
                          />
                        </a></div>
                <section className='content'>
                    <div className='boxPapanel content-padding form-min-hight'>
                        <div className='taskdetails'>
                            <div className='clearfix tdBtmlist'>
                                
                                <Formik
                                    initialValues={initialValues}
                                    validationSchema={validationSchema(this)}
                                    onSubmit={this.handleSubmitEvent}
                                >
                                    {({ values, errors, touched, setFieldValue, isSubmitting }) => {
                                        return (
                                            <div className="messageBox rejectionForm">
                                                {/* <h1>Enter Reason For Rejection</h1> */}
                                            <Form>
                                                <Row className='eaditRow'>
                                                    <Col xs={12} sm={12} md={12}>
                                                        <div className='form-group'>
                                                            <h1>Enter Reason For Rejection</h1>
                                                            {/* <label>
                                                                Reason For Rejection{' '}
                                                                <span class='required-field'>*</span>
                                                            </label> */}
                                                            <Field
                                                                name='reason_of_rejection'
                                                                component="textarea"
                                                                className='form-control'
                                                                autoComplete='off'
                                                                // placeholder={`Enter Reason Of Rejection`}
                                                            ></Field>

                                                            {errors.reason_of_rejection &&
                                                            touched.reason_of_rejection ? (
                                                                <span className='errorMsg'>
                                                                    {errors.reason_of_rejection}
                                                                </span>
                                                            ) : null}
                                                        </div>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col>
                                                        {errors.message ? (
                                                            <span className='errorMsg'>{errors.message}</span>
                                                        ) : null}
                                                    </Col>
                                                </Row>
                                                <ButtonToolbar>
                                                    <Button className="jcustomBtn" type='submit'>
                                                        {isSubmitting ? 'Submitting...' : 'Submit'}
                                                    </Button>
                                                </ButtonToolbar>
                                            </Form>
                                            </div>
                                        );
                                    }}
                                </Formik>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="footerCopy"><p>© 2021 Dr. Reddy’s Laboratories Ltd. All rights reserved(v2.9.0)</p></div>
            </div>
        
        );
    };



    customererrorForm = () => {
        const { showLoader, successMsg, errMsg, showForm } = this.state;
        return (
            <div className='content-wrapper qaWrapper newFormSection'>
                                <div className="logoWrapper"> <a
                          href={process.env.REACT_APP_PORTAL_URL}
                          target="_blank"
                        >
                          <img
                            src={require("../../assets/images/logo-blue.jpg")}
                          />
                        </a></div>
                        <div className="messageBox">
                        <img
                            src={require("../../assets/images/invalid-file.png")}
                          />  <h1>{errMsg}</h1>
                        </div>
                
                <div className="footerCopy"><p>© 2021 Dr. Reddy’s Laboratories Ltd. All rights reserved(v2.9.0)</p></div>
            </div>
        
        );
    };

    customersuccessForm = () => {
        const { showLoader, successMsg, errMsg, showForm } = this.state;
        return (
            <div className='content-wrapper qaWrapper newFormSection'>
                                <div className="logoWrapper"> <a
                          href={process.env.REACT_APP_PORTAL_URL}
                          target="_blank"
                        >
                          <img
                            src={require("../../assets/images/logo-blue.jpg")}
                          />
                        </a></div>
                        <div className="messageBox successBox">
                        <img
                            src={require("../../assets/images/successful.png")}
                          />  <h1>{successMsg}</h1>
                        </div>
                
                <div className="footerCopy"><p>© 2021 Dr. Reddy’s Laboratories Ltd. All rights reserved(v2.9.0)</p></div>
            </div>
        
        );
    };



    render() {
        const { showLoader, successMsg, errMsg, showForm } = this.state;

        if (showLoader) return <Loader />;

        if (errMsg) return this.customererrorForm();

        if (successMsg) return this.customersuccessForm();

        if (showForm) return this.customerRejectionForm();
    }
}

const mapStateToProps = (state) => {
    return {
        token: state.auth.token !== null ? true : false,
    };
};

const mapDispatchToProps = (dispatch) => {
    return null;
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomerResponse);
