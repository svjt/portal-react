import React, { Component } from "react";
import { Link } from "react-router-dom";
// import Layout from "../Layout/layout";
import axios from "../../shared/axios";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
} from "reactstrap";
import Loader from "../Loader/loader";

import classnames from "classnames";
import { BasicUserData } from "../../shared/helper";
import { connect } from "react-redux";

class productDisplay extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: "1",
      productDetail: {},
      documentsArray: [],
      documentsDescriptionArray: [],
      redirect: false,
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  componentDidMount() {
    this.getProductDetails();
    let userData = BasicUserData();
    if (userData) {
      this.setState({
        isLoading: false,
      });
    }
  }
  getProductDetails() {
    // axios.get(process.env.REACT_APP_PORTAL_URL + 'ext/api/get-product-details/' + this.props.match.params.id + '?_format=json')
    //.get("/products/" + this.props.match.params.id)
    let filter_data = {
      product_id: this.props.match.params.id,
    };
    axios
      .post("/products/product_details", filter_data)
      .then((res) => {
        // console.log(res.data.data);
        if (Object.keys(res.data.data).length > 0) {
          var objData = Object.values(res.data.data);
          objData = objData[0];
          //console.log("mono", objData);
          //this.setState({ productDetail: prodDetails });
          this.setState(
            {
              productDetail: objData,
            },
            () => {
              if (this.state.productDetail.doc != "") {
                var documents = Object.values(this.state.productDetail.doc);
                //console.log("documents", documents);

                /* var documents_description = this.state.productDetail
                  .description; */
                this.setState({ documentsArray: documents });
                /* this.setState({
                  documentsDescriptionArray: documents_description,
                }); */
              }
            }
          );
        } else {
          this.props.history.push("/");
          //return <Redirect to='/' />
        }
      })
      .catch((err) => {});
  }
  noData = () => {
    return (
      <>
        <div className="documents-box" key={1}>
          <div className="documents-box-left">
            <b>TEST</b>
          </div>
        </div>
      </>
    );
  };
  render() {
    //console.log("========", this.state.documentsArray);
    if (
      JSON.stringify(this.state.productDetail) != "{}" &&
      this.state.productDetail
    ) {
      return (
        <div className="container-fluid clearfix">
          <div className="dashboard-content-sec">
            <div className="breadcrumb">
              <ul>
                <li>
                  <Link to="/product-catalogue">
                    {this.props.dynamic_lang.product_details.header}
                  </Link>
                </li>
                <li>{this.state.productDetail.title}</li>
              </ul>
            </div>
            <div className="product-details-box clearfix">
              {/* process.env.REACT_APP_PORTAL_URL + */}
              <figure>
                <img
                  src={this.state.productDetail.image_url}
                  alt={this.state.productDetail.title}
                  typeof="Image"
                  className="img-fluid"
                  width="480"
                  height="354"
                />
              </figure>
              <div className="product-details-content">
                <h2>{this.state.productDetail.title}</h2>
                <ul className="status">
                  <li>
                    <h3>{this.props.dynamic_lang.product_details.cas_no}</h3>
                    {this.state.productDetail.cas_no}
                  </li>
                  <li>
                    <h3>
                      {this.props.dynamic_lang.product_details.innovator_brand}
                    </h3>
                    {this.state.productDetail.innovator_brand}
                  </li>
                  <li>
                    <h3>
                      {this.props.dynamic_lang.product_details.therapy_area}
                    </h3>
                    {this.state.productDetail.therapy_area}
                  </li>
                  <li>
                    <h3>
                      {this.props.dynamic_lang.product_details.therapy_category}
                    </h3>
                    {this.state.productDetail.therapeutic_category}
                  </li>
                  <li>
                    <h3>
                      {this.props.dynamic_lang.product_details.api_technology}
                    </h3>
                    {this.state.productDetail.api_technology}
                  </li>
                  <li>
                    <h3>{this.props.dynamic_lang.product_details.dose_from}</h3>
                    {this.state.productDetail.dose_form}
                  </li>
                  <li>
                    <h3>
                      {
                        this.props.dynamic_lang.product_details
                          .development_status
                      }
                    </h3>
                    {this.state.productDetail.development_status}
                  </li>
                </ul>
              </div>
            </div>
            <div className="quicktabs-main">
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === "1",
                    })}
                    onClick={() => {
                      this.toggle("1");
                    }}
                  >
                    {this.props.dynamic_lang.product_details.details}
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({
                      active: this.state.activeTab === "2",
                    })}
                    onClick={() => {
                      this.toggle("2");
                    }}
                  >
                    {this.props.dynamic_lang.product_details.documents}
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={this.state.activeTab}>
                <TabPane tabId="1">
                  <Row>
                    <Col sm="12">
                      <h3>
                        {
                          this.props.dynamic_lang.product_details
                            .mechanism_action
                        }
                      </h3>
                      {this.state.productDetail.mechanism_of_action}
                      <h3>
                        {this.props.dynamic_lang.product_details.indication}
                      </h3>
                      <p>{this.state.productDetail.indication}</p>
                      <h3>
                        {
                          this.props.dynamic_lang.product_details
                            .available_regulatory
                        }{" "}
                      </h3>
                      {this.state.productDetail.available_regulatory}
                    </Col>
                  </Row>
                </TabPane>
                <TabPane tabId="2">
                  <Row>
                    <Col sm="12">
                      {this.state.documentsArray &&
                      this.state.documentsArray.length > 0 ? (
                        this.state.documentsArray.map((document, key) => (
                          <div className="documents-box" key={key}>
                            <div className="documents-box-left">
                              <b>
                                {this.state.documentsArray[key].description}
                              </b>
                            </div>
                            <div className="row clearfix">
                              <div className="documents-box-right">
                                <div className="download-btn">
                                  {/* process.env.REACT_APP_PORTAL_URL + */}{" "}
                                  <a
                                    href={this.state.documentsArray[key].url}
                                    target="_blank"
                                    type="application/pdf; length=36922"
                                  >
                                    {
                                      this.props.dynamic_lang.product_details
                                        .download
                                    }
                                  </a>{" "}
                                </div>
                              </div>
                            </div>
                          </div>
                        ))
                      ) : (
                        <>
                          <div className="documents-box" key={1}>
                            <div className="">
                              {
                                this.props.dynamic_lang.product_details
                                  .no_documents
                              }
                            </div>
                          </div>
                        </>
                      )}
                    </Col>
                  </Row>
                </TabPane>
              </TabContent>
            </div>
          </div>
        </div>
      );
    } else {
      return <Loader />;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

//export default productDisplay;
export default connect(mapStateToProps, mapDispatchToProps)(productDisplay);
