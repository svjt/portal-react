import React, { Component, Alert } from "react";
import { Link } from "react-router-dom";
import Layout from "../Layout/layout";
import Select from "react-select";
import axios from "../../shared/axios";
import { Formik, Field, Form, withFormik } from "formik";
import * as Yup from "yup";
import {
  htmlDecode,
  supportedFileType,
  BasicUserData,
} from "../../shared/helper";
import closeIcon from "../../assets/images/times-solid-red.svg";
//SATYAJIT
//import { Editor } from "./node_modules/@tinymce/tinymce-react";

import { Editor } from "@tinymce/tinymce-react";

import { Tooltip } from "reactstrap";
import infoIcon from "../../assets/images/info-circle-solid.svg";
import Dropzone from "react-dropzone";
import Loader from "../Loader/loader";

import downloadFile from "../../assets/File/Mutual_CDA_Template Final.docx";
import downloadIcon from "../../assets/images/download-solid.svg";

import downloadFileAgrement from "../../assets/File/DRL_Standard template_(quality agreement).doc";
import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { connect } from "react-redux";
import { isMobile } from "react-device-detect";

var cc_Customer = [];

var minDate = "";
var maxDate = "";

const selectOptions = [
  { value: "ACC", label: "ACC" },
  { value: "Long Term", label: "Long Term" },
  { value: "Others", label: "Others" },
];

const options = [
  { value: "Mgs", label: "Mgs" },
  { value: "Gms", label: "Grams" },
  { value: "Kgs", label: "Kgs" },
];

const path = require("path");

class FormComponent extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      tooltipOpen: false,
      startDate: "",
      reqDeadline: "",
      share_with_agent: true,
      selectedOption: null,
      products: [],
      document_type: [],
      units: [],
      stability_data: [],
      countryList: [],
      selectedCountry: null,
      files: [],
      filesHtml: "",
      rejectedFile: [],
      cc_Customers: [],
      errMsg: null,
      selectedType: null,
      selectedUnit: "",
      agent_customer_id: null,
      displayCCList: false,
      isLoading: true,
    };
    this.inputRef = React.createRef();
  }

  deleteFile = (e, setFieldValue) => {
    this.inputRef.current.value = null; // clear input value - SATYAJIT
    e.preventDefault();
    var tempArray = [];

    this.state.files.map((file, fileIndex) => {
      if (file.checked === true) {
      } else {
        tempArray.push(file);
      }
    });

    setFieldValue("file_name", tempArray);

    this.setState({ files: tempArray });
  };
  customerChecked = (event) => {
    var tempArray = [];

    if (event.target.checked === true) {
      this.state.cc_Customers.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 1;
        }
        tempArray.push(user);
      });
    } else {
      this.state.cc_Customers.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 0;
        }
        tempArray.push(user);
      });
    }
    this.setState({ cc_Customers: tempArray });
  };

  recId = (event) => {
    var tempArray = [];

    if (event.target.checked === true) {
      this.state.files.map((file, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          file.checked = event.target.checked;
        }
        tempArray.push(file);
      });
    } else {
      this.state.files.map((file, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          file.checked = event.target.checked;
        }
        tempArray.push(file);
      });
    }

    this.setState({ files: tempArray });
  };

  getCc_CustomerList() {
    axios
      .get("tasks/get-cc-customers")
      .then((res) => {
        cc_Customer = res.data.data;
        this.setState({ cc_Customers: cc_Customer });
      })
      .catch((err) => {});
  }

  fileChangedHandler = (event) => {
    this.setState({ upload_file: event.target.files[0] });
  };

  componentDidMount() {
    window.scrollTo(0, 0);
    if (isMobile) {
      const datePickers = document.getElementsByClassName(
        "react-datepicker__input-container"
      );
      //console.log('datePickers', datePickers)
      Array.from(datePickers).forEach((el) =>
        el.childNodes[0].setAttribute("readOnly", true)
      );
    }
    this.getProductList();
    this.getDocumentTypeList();
    this.getProductUnit();
    this.getStabilityData();
    this.getCountryList();
    this.getCc_CustomerList();
    this.props.state.initialValue.share_with_agent = 1;
    // console.log(this.props.dynamic_lang);
    if (
      this.props.state.auditDate === true ||
      this.props.state.displayRequested_date_response === true
    ) {
      var today = new Date();
      var tomorrow = new Date();
      tomorrow.setDate(today.getDate() + 1);

      minDate = tomorrow;
      maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 10));
    }

    this.setState({ files: [], filesHtml: "" });
    document.title = `${this.props.state.heading}| Dr. Reddy's API`;

    // display cc list for agent
    let userData = BasicUserData();
    if (userData) {
      if (userData.role === 2) {
        this.setState({ displayCCList: true });
      }
      this.setState({
        isLoading: false,
      });
    }

    // set product while coming from product catalouge -- SATYAJIT
    if (this.props.history.location.state !== undefined) {
      this.setState({
        selectedOption: {
          value: this.props.history.location.state.pid,
          label: this.props.history.location.state.pname,
        },
      });

      this.props.state.initialValue.product_id = this.props.history.location.state.pid;
    }
    // ===================//
  }

  componentDidUpdate = (prevProps) => {
    if (this.props.selCompany !== prevProps.selCompany) {
      this.setState({ agent_customer_id: null, ccAgentCustomerList: [] });
    }
  };

  getProductList() {
    axios
      .get("/products")
      .then((res) => {
        var product_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          product_list.push({
            value: element["product_id"],
            //label: htmlDecode(element['medicalCondition'])
            label: htmlDecode(element["product_name"]),
          });
        }
        this.setState({ products: product_list });
      })
      .catch((err) => {});
  }

  getDocumentTypeList() {
    axios
      .get("/document_type")
      .then((res) => {
        var document_type_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          document_type_list.push({
            value: element["doc_id"],
            label: htmlDecode(element["doc_name"]),
          });
        }
        this.setState({ document_type: document_type_list });
      })
      .catch((err) => {});
  }

  getProductUnit() {
    axios
      .get("/product_unit")
      .then((res) => {
        var product_unit_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          product_unit_list.push({
            value: element["unit_id"],
            label: htmlDecode(element["unit_name"]),
          });
        }
        this.setState({ units: product_unit_list });
      })
      .catch((err) => {});
  }

  getStabilityData() {
    axios
      .get("/stability_data")
      .then((res) => {
        var stability_data_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          stability_data_list.push({
            value: element["id"],
            label: htmlDecode(element["name"]),
          });
        }
        this.setState({ stability_data: stability_data_list });
      })
      .catch((err) => {});
  }
  getCountryList() {
    axios
      .get("/country")
      .then((res) => {
        var country_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          country_list.push({
            value: element["country_id"],

            label: htmlDecode(element["country_name"]),
          });
        }
        this.setState({ countryList: country_list });
      })
      .catch((err) => {});
  }

  removeError = (setErrors) => {
    setErrors({});
  };
  removeFile_Error = () => {
    this.setState({ rejectedFile: [] });
  };
  hideError = () => {
    this.setState({ errMsg: "" });
  };
  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    });
  }

  handleChange = (selectedUnit) => {
    this.setState({ selectedUnit });
  };

  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue) => {
    var rejectedFiles = [];
    var uploadFile = [];

    var totalfile = acceptedFiles.length;

    for (var index = 0; index < totalfile; index++) {
      var error = 0;
      var filename = acceptedFiles[index].name.toLowerCase();
      var extension_list = supportedFileType();
      var ext_with_dot = path.extname(filename);
      var file_extension = ext_with_dot.split(".").join("");

      var obj = {};

      var fileErrText = this.props.dynamic_lang.display_error.form_error
        .following_extensions;

      if (extension_list.indexOf(file_extension) === -1) {
        error = error + 1;

        if (totalfile > 1) {
          if (index === 0) {
            obj["errorText"] =
              this.props.dynamic_lang.display_error.form_error.error_text_1.replace(
                "[file_name]",
                filename
              ) + fileErrText;
          } else {
            obj["errorText"] =
              this.props.dynamic_lang.display_error.form_error.error_text_2.replace(
                "[file_name]",
                filename
              ) + fileErrText;
          }
        } else {
          obj["errorText"] =
            this.props.dynamic_lang.display_error.form_error.error_text_2.replace(
              "[file_name]",
              filename
            ) + fileErrText;
        }
        rejectedFiles.push(obj);
      }

      if (acceptedFiles[index].size > 50000000) {
        obj[
          "errorText"
        ] = this.props.dynamic_lang.display_error.form_error.allowed_size.replace(
          "[file_name]",
          filename
        );
        error = error + 1;
        rejectedFiles.push(obj);
      }

      if (error === 0) {
        uploadFile.push(acceptedFiles[index]);
        setErrors({ file_name: false });
      }
    }

    var prevFiles = this.state.files;
    var newFiles = [];
    if (prevFiles.length > 0) {
      for (let index = 0; index < uploadFile.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (uploadFile[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        if (remove === 0) {
          prevFiles.push(uploadFile[index]);
        }
      }

      prevFiles.map((file) => {
        file.checked = false;
        newFiles.push(file);
      });
    } else {
      uploadFile.map((file) => {
        file.checked = false;
        newFiles.push(file);
      });
    }

    this.setState(
      {
        files: newFiles,
        // filesHtml: fileListHtml
      },
      () => {
        setErrors({ file_name: false });
        setFieldValue("file_name", this.state.files);
      }
    );

    this.setState({
      rejectedFile: rejectedFiles,
    });
  };

  setADocType = (event, setFieldValue) => {
    setFieldValue("doctype", event);
    this.setState({ doctype: event });
  };

  handleSubmit = (values, actions) => {
    this.setState({ showLoader: true });
    this.setState({ errMsg: "" });
    var cc_Cust = [];

    var formData = new FormData();

    let userData = BasicUserData();
    if (userData.role === 2) {
      if (this.state.ccAgentCustomerList) {
        this.state.ccAgentCustomerList.map((user, index) => {
          if (user.checked === 1) {
            cc_Cust.push({ customer_id: user.customer_id });
          }
        });
      }
      if (
        this.state.agent_customer_id &&
        this.state.agent_customer_id !== null &&
        this.state.agent_customer_id.value > 0
      ) {
        formData.append(
          "agent_customer_id",
          this.state.agent_customer_id.value
        );
      }
    } else {
      this.state.cc_Customers.map((user, index) => {
        if (user.checked === 1) {
          cc_Cust.push({ customer_id: user.customer_id });
        }
      });
    }

    if (this.state.files && this.state.files.length > 0) {
      for (let index = 0; index < this.state.files.length; index++) {
        const element = this.state.files[index];
        formData.append("file", element);
      }
    } else {
      formData.append("file", []);
    }

    formData.append("product_id", JSON.stringify(values.product_id));

    if (this.props.state.displayCountry) {

      if(this.checkSingularCountry()){
        formData.append(
          "country_id",
          values.country_id != "" && values.country_id != null
            ? JSON.stringify([values.country_id])
            : ""
        );
      }else{
        formData.append(
          "country_id",
          values.country_id != "" && values.country_id != null
            ? JSON.stringify(values.country_id)
            : ""
        );
      }
    }

    if (this.props.state.disPlayPharmacopoeia === true) {
      formData.append("pharmacopoeia", values.Pharmacopoeia.trim());
    }

    if (this.props.state.displayDoctype === true) {
      formData.append("apos_document_type", JSON.stringify(values.doctype));
    }

    if (this.props.state.displayNotification_number === true) {
      formData.append("notification_number", values.notification_number.trim());
    }

    if (this.props.state.disPlatDMFNumber === true) {
      formData.append("dmf_number", values.dmf_number.trim());
    }

    if (this.props.state.disPlayDMF === true) {
      formData.append("dmf_number", values.dmf_number.trim());
    }

    if (this.props.state.requestDeadline === true) {
      formData.append("rdd", values.req_deadline.trim());
    }

    if (this.props.state.displayRequested_date_response === true) {
      formData.append("rdfrc", values.requested_date_response);
    }

    if (this.props.state.auditDate === true) {
      formData.append("audit_visit_site_name", values.siteName.trim());
      formData.append("request_audit_visit_date", values.auditDate.trim());
    }

    if (this.props.state.selectedType === true) {
      formData.append("stability_data_type", values.stabilityType);
    }

    if (this.props.state.disPlayPolymorPhicForm === true) {
      formData.append("polymorphic_form", values.polymorphicForm.trim());
    }

    if (
      this.props.state.displayQuantity === true &&
      this.props.state.displayUnit === true
    ) {
      formData.append("quantity", values.quantity + " " + values.unit);
    }
    //============//
    if (
      this.props.state.displayGmpClearanceId === true &&
      this.props.state.displayGmpClearanceId === true
    ) {
      formData.append("gmp_clearance_id", values.gmp_clearance_id.trim());
    }
    if (
      this.props.state.displayTgaEmailId === true &&
      this.props.state.displayTgaEmailId === true
    ) {
      formData.append("tga_email_id", values.tga_email_id.trim());
    }
    if (
      this.props.state.displayApplicantName === true &&
      this.props.state.displayApplicantName === true
    ) {
      formData.append("applicant_name", values.applicant_name.trim());
    }
    if (
      this.props.state.displayDocReq === true &&
      this.props.state.displayDocReq === true
    ) {
      formData.append("doc_required", values.doc_required.trim());
    }

    formData.append("request_type_id", this.props.state.request_type_heading.trim());
    formData.append("description", values.messages.trim());
    formData.append("share_with_agent", values.share_with_agent);

    formData.append("cc_customers", JSON.stringify(cc_Cust));
    axios
      .post(this.props.state.apiPath, formData)
      .then((res) => {
        if (res.status === 200) {
          this.setState({ showLoader: false });
          this.props.history.push({
            pathname: "/my_requests/",

            state: {
              message: this.props.dynamic_lang.form_component.thank_for_request,
            },
          });
        }
      })
      .catch((err) => {
        this.setState({ showLoader: false });
        // err = {
        //   data : {
        //     status : 3,
        //     message : "tettetetetette"
        //   }
        // }
        //  err.data.status = 2
        //  err.data.errors = {"product_id": 'Please select' }
        actions.setSubmitting(false);

        if (err.data && err.data.status === 2) {
          actions.setErrors(err.data.errors);
        } else {
          if(err.data && err.data.message){
            this.setState({ errMsg: err.data.message });
          }
        }
      });
  };

  shareWithAgent = (event, setFieldValue) => {
    if (event.target.checked === true) {
      setFieldValue("share_with_agent", 1);
    } else {
      setFieldValue("share_with_agent", 0);
    }
  };

  getAgentCCList = (event, setFieldValue) => {
    this.setState({ agent_customer_id: event });

    if (event === null || event === "") {
      setFieldValue("agent_customer_id", "");
      this.setState({ ccAgentCustomerList: [] });
    } else {
      axios
        .get(`/agents/cc_customers?custid=${event.value}`)
        .then((res) => {
          this.setState({
            ccAgentCustomerList: res.data.data,
            pre_selected: res.data.pre_selected,
          });
          setFieldValue("agent_customer_id", event.value);
        })
        .catch((err) => {});
    }
  };

  agentcustomerChecked = (event) => {
    var tempArray = [];

    if (event.target.checked === true) {
      this.state.ccAgentCustomerList.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 1;
        }
        tempArray.push(user);
      });
    } else {
      this.state.ccAgentCustomerList.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 0;
        }
        tempArray.push(user);
      });
    }
    this.setState({ ccAgentCustomerList: tempArray });
  };

  setCountry = (event, setFieldValue) => {
    setFieldValue("country_id", event);
  };

  checkSingularCountry = () => {
    console.log(this.props.state.request_type_heading.trim().toLowerCase());
    if(['general request','quality general request','vendor questionnaire','spec and moa','method related queries','typical coa','recertification of coa','audit/visit request','customized spec request','quality equivalence request','apostallation of documents','stability data','elemental impurity (ei) declaration','residual solvents declaration','storage and transport declaration','request for additional declarations','genotoxic impurity assessment'].includes(this.props.state.request_type_heading.trim().toLowerCase())){
      return true;
    }else{
      return false;
    }
  }

  render() {
    const {
      showLoader,
      errMsg,
      startDate,
      reqDeadline,
      displayCCList,
      agent_customer_id,
      share_with_agent,
      files,
      ccAgentCustomerList,
      pre_selected,
      tooltipOpen,
      cc_Customers,
      rejectedFile,
      selectedOption,
      products,
      document_type,
      units,
      stability_data,
      countryList,
      selectedCountry,
      selectedType,
      selectedUnit,
    } = this.state;

    if (this.state.isLoading === true) {
      return (
        <>
          <div className="loginLoader">
            <div className="lds-spinner">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <span>Please Wait…</span>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <div className="container-fluid clearfix formSec">
          <div className="dashboard-content-sec">
            <div className="service-request-form-sec newFormSec">
              <div className="form-page-title-block">
                <h2>{this.props.state.heading}</h2>
              </div>
              {showLoader === true ? <Loader /> : null}
              <Formik
                initialValues={this.props.state.initialValue}
                validationSchema={this.props.validationSchema}
                onSubmit={this.handleSubmit}
              >
                {({
                  errors,
                  values,
                  touched,
                  setErrors,
                  setFieldValue,
                  setFieldTouched,
                }) => {
                  return (
                    <Form>
                      <div
                        className="messageserror"
                        style={{
                          display:
                            errMsg && errMsg !== null && errMsg !== ""
                              ? "block"
                              : "none",
                        }}
                      >
                        <Link to="#" className="close">
                          <img
                            src={closeIcon}
                            width="17.69px"
                            height="22px"
                            onClick={(e) => this.hideError()}
                          />
                        </Link>
                        <div>
                          <ul className="">
                            <li className="">{errMsg}</li>
                          </ul>
                        </div>
                      </div>
                      <div
                        className="messageserror"
                        style={{
                          display:
                            (errors.product_id && touched.product_id) ||
                            (errors.applicant_name && touched.applicant_name) ||
                            (errors.doc_required && touched.doc_required) ||
                            (errors.gmp_clearance_id &&
                              touched.gmp_clearance_id) ||
                            (errors.tga_email_id && touched.tga_email_id) ||
                            (errors.country_id && touched.country_id) ||
                            (errors.dmf_number && touched.dmf_number) ||
                            (errors.file_name && touched.file_name) ||
                            (errors.siteName && touched.siteName) ||
                            (errors.auditDate && touched.auditDate) ||
                            (errors.stabilityType && touched.stabilityType) ||
                            (errors.quantity && touched.quantity) ||
                            (errors.unit && touched.unit) ||
                            (errors.message)
                              ? "block"
                              : "none",
                        }}
                      >
                        <Link to="#" className="close">
                          <img
                            src={closeIcon}
                            width="17.69px"
                            height="22px"
                            onClick={(e) => this.removeError(setErrors)}
                          />
                        </Link>
                        <div>
                          <ul className="">
                            {errors.product_id && touched.product_id ? (
                              <li className="">{errors.product_id}</li>
                            ) : null}
                            {errors.country_id && touched.country_id ? (
                              <li className="">{errors.country_id}</li>
                            ) : null}
                            {errors.file_name && touched.file_name ? (
                              <li className="">{errors.file_name}</li>
                            ) : null}
                            {errors.siteName && touched.siteName ? (
                              <li className="">{errors.siteName}</li>
                            ) : null}
                            {errors.auditDate && touched.auditDate ? (
                              <li className="">{errors.auditDate}</li>
                            ) : null}
                            {errors.dmf_number && touched.dmf_number ? (
                              <li className="">{errors.dmf_number}</li>
                            ) : null}
                            {errors.stabilityType && touched.stabilityType ? (
                              <li className="">{errors.stabilityType}</li>
                            ) : null}
                            {errors.quantity && touched.quantity ? (
                              <li className="">{errors.quantity}</li>
                            ) : null}
                            {errors.unit && touched.unit ? (
                              <li className="">{errors.unit}</li>
                            ) : null}
                            {errors.applicant_name && touched.applicant_name ? (
                              <li className="">{errors.applicant_name}</li>
                            ) : null}
                            {errors.doc_required && touched.doc_required ? (
                              <li className="">{errors.doc_required}</li>
                            ) : null}
                            {errors.gmp_clearance_id &&
                            touched.gmp_clearance_id ? (
                              <li className="">{errors.gmp_clearance_id}</li>
                            ) : null}
                            {errors.tga_email_id && touched.tga_email_id ? (
                              <li className="">{errors.tga_email_id}</li>
                            ) : null}
                            {errors.message ? (
                              <li className="">{errors.message}</li>
                            ) : null}
                          </ul>
                        </div>
                      </div>
                      <div className="row">
                        <div className={this.props.state.customClassName}>
                          <Select
                            isMulti
                            value={selectedOption}
                            name="product_id"
                            options={products}
                            isSearchable={true}
                            isClearable={true}
                            placeholder={
                              this.props.dynamic_lang.form_component
                                .select_product
                            }
                            onChange={(e) => {
                                if(e === null || e === ""){
                                  setFieldValue("product_id", "");
                                  this.setState({ selectedOption: "" });
                                }else if(e.length > 5){
                                  //DO NOTHING
                                }else {
                                  setFieldValue("product_id", e);
                                  this.setState({ selectedOption: e });
                                }
                            }}
                          />
                        </div>

                        {this.props.state.displayCountry === true && (
                          <div className={this.props.state.customClassName}>

                            {this.checkSingularCountry()?
                            <Select
                              onChange={(e) => {
                                this.setCountry(e, setFieldValue);
                              }}
                              options={countryList}
                              isSearchable={true}
                              isClearable={true}
                              name="country_id"
                              placeholder={this.props.state.placeHolderText}
                            />:<Select
                              isMulti
                              onChange={(e) => {
                                this.setCountry(e, setFieldValue);
                              }}
                              options={countryList}
                              isSearchable={true}
                              isClearable={true}
                              name="country_id"
                              placeholder={this.props.state.placeHolderText}
                            />
                            }
                          </div>
                        )}

                        {this.props.state.displayNotification_number ===
                        true ? (
                          <div className={this.props.state.customClassName}>
                            <Field
                              name="notification_number"
                              type="text"
                              className="form-control customInput"
                              placeholder={
                                this.props.dynamic_lang.form_component
                                  .notification_number
                              }
                            />
                          </div>
                        ) : null}

                        {this.props.state.selectedType === true ? (
                          <div className={this.props.state.customClassName}>
                            <Select
                              isClearable={true}
                              value={selectedType}
                              onChange={(e) => {
                                if (e === null || e === "") {
                                  this.setState({ selectedType: "" });

                                  setFieldValue("stabilityType", "");
                                } else {
                                  this.setState({ selectedType: e });

                                  setFieldValue("stabilityType", e.value);
                                }
                              }}
                              options={stability_data}
                              isSearchable={true}
                              placeholder={
                                this.props.dynamic_lang.form_component
                                  .select_type
                              }
                            />
                          </div>
                        ) : null}

                        {this.props.state.displayQuantity === true ? (
                          <div className={this.props.state.customClassName}>
                            <Field
                              name="quantity"
                              type="number"
                              className="form-control customInput"
                              placeholder={
                                this.props.dynamic_lang.form_component.quantity
                              }
                            />
                          </div>
                        ) : null}
                        {this.props.state.displayUnit === true ? (
                          <div className={this.props.state.customClassName}>
                            <Select
                              value={selectedUnit}
                              name="unit"
                              isClearable={true}
                              onChange={(e) => {
                                if (e === null || e === "") {
                                  this.setState({ selectedUnit: "" });

                                  setFieldValue("unit", "");
                                } else {
                                  this.setState({ selectedUnit: e });

                                  setFieldValue("unit", e.value);
                                }
                              }}
                              options={units}
                              placeholder={
                                this.props.dynamic_lang.form_component.unit
                              }
                            />
                          </div>
                        ) : null}

                        {this.props.state.siteName === true ? (
                          <div className={this.props.state.customClassName}>
                            <Field
                              name="siteName"
                              type="text"
                              className="form-control customInput"
                              placeholder={
                                this.props.dynamic_lang.form_component.site_name
                              }
                            />
                          </div>
                        ) : null}

                        {this.props.state.displayGmpClearanceId && (
                          <div className={this.props.state.customClassName}>
                            <Field
                              name="gmp_clearance_id"
                              type="text"
                              className="form-control customInput"
                              placeholder={
                                this.props.dynamic_lang.form_component
                                  .gmp_clearance_id
                              }
                            />
                          </div>
                        )}

                        {this.props.state.displayRequested_date_response ===
                        true ? (
                          <div className={this.props.state.customClassName}>
                            <DatePicker
                              className="form-control customInput"
                              selected={startDate}
                              name="requested_date_response"
                              showMonthDropdown
                              showYearDropdown
                              minDate={minDate}
                              maxDate={maxDate}
                              dropdownMode="select"
                              onChange={(e) => {
                                if (e === null) {
                                  setFieldValue("requested_date_response", "");
                                } else {
                                  setFieldValue(
                                    "requested_date_response",
                                    dateFormat(e, "yyyy-mm-dd")
                                  );
                                }

                                this.setState({ startDate: e });
                              }}
                              // onChange={e => {
                              //   this.changeDate(e, setFieldValue);
                              // }}
                              dateFormat="dd/MM/yyyy"
                              autoComplete="off"
                              placeholderText={
                                this.props.dynamic_lang.form_component
                                  .requested_date_response_closure
                              }
                            />
                          </div>
                        ) : null}

                        {this.props.state.auditDate === true ? (
                          <div className={this.props.state.customClassName}>
                            <DatePicker
                              className="form-control customInput"
                              selected={startDate}
                              name="auditDate"
                              showMonthDropdown
                              showYearDropdown
                              minDate={minDate}
                              maxDate={maxDate}
                              dropdownMode="select"
                              onChange={(e) => {
                                if (e === null) {
                                  setFieldValue("auditDate", "");
                                } else {
                                  setFieldValue(
                                    "auditDate",
                                    dateFormat(e, "mm/dd/yyyy")
                                  );
                                }

                                this.setState({ startDate: e });
                              }}
                              // onChange={e => {
                              //   this.changeDate(e, setFieldValue);
                              // }}
                              dateFormat="dd/MM/yyyy"
                              autoComplete="off"
                              placeholderText={
                                this.props.dynamic_lang.form_component
                                  .audit_visit_date
                              }
                            />
                          </div>
                        ) : null}

                        {this.props.state.displayTgaEmailId && (
                          <div className={this.props.state.customClassName}>
                            <Field
                              name="tga_email_id"
                              type="text"
                              className="form-control customInput"
                              placeholder={
                                this.props.dynamic_lang.form_component.email_id
                              }
                            />
                          </div>
                        )}

                        {this.props.state.requestDeadline === true ? (
                          <div className={this.props.state.customClassName}>
                            <DatePicker
                              className="form-control customInput"
                              selected={reqDeadline}
                              name="req_deadline"
                              showMonthDropdown
                              showYearDropdown
                              minDate={new Date()}
                              maxDate={maxDate}
                              dropdownMode="select"
                              onChange={(e) => {
                                if (e === null) {
                                  setFieldValue("req_deadline", "");
                                } else {
                                  setFieldValue(
                                    "req_deadline",
                                    dateFormat(e, "yyyy-mm-dd")
                                  );
                                }

                                this.setState({ reqDeadline: e });
                              }}
                              // onChange={e => {
                              //   this.changeDate(e, setFieldValue);
                              // }}
                              dateFormat="dd/MM/yyyy"
                              autoComplete="off"
                              placeholderText={
                                this.props.dynamic_lang.form_component
                                  .requested_deadline
                              }
                            />
                          </div>
                        ) : null}

                        {this.props.state.disPlayPharmacopoeia === true ? (
                          <div className={this.props.state.customClassName}>
                            <Field
                              name="Pharmacopoeia"
                              type="text"
                              className="form-control customInput"
                              placeholder={
                                this.props.dynamic_lang.form_component
                                  .pharmacopoeia
                              }
                            />
                          </div>
                        ) : null}

                        {this.props.state.displayDoctype === true ? (
                          <div className={this.props.state.customClassName}>
                            <Select
                              isMulti
                              className="basic-single"
                              classNamePrefix="select"
                              defaultValue=""
                              isClearable={true}
                              isSearchable={true}
                              name="doctype"
                              /* options={[
                                { label: "Site GMP", value: "Site GMP" },
                                {
                                  label: "Site Manufacturing License",
                                  value: "Site Manufacturing License",
                                },
                                {
                                  label: "Written Confirmation",
                                  value: "Written Confirmation",
                                },
                              ]} */
                              options={document_type}
                              onChange={(e) => {
                                this.setADocType(e, setFieldValue);
                              }}
                              onBlur={() => setFieldTouched("doctype")}
                              placeholder={
                                this.props.dynamic_lang.form_component
                                  .document_type
                              }
                            />
                          </div>
                        ) : null}

                        {this.props.state.disPlatDMFNumber === true ? (
                          <div className={this.props.state.customClassName}>
                            <Field
                              name="dmf_number"
                              type="text"
                              className="form-control customInput"
                              placeholder={
                                this.props.dynamic_lang.form_component
                                  .dmf_number
                              }
                            />
                          </div>
                        ) : null}

                        {this.props.state.disPlayDMF === true ? (
                          <div className={this.props.state.customClassName}>
                            <Field
                              name="dmf_number"
                              type="text"
                              className="form-control customInput"
                              placeholder={
                                this.props.dynamic_lang.form_component.dmf_cmp
                              }
                            />
                          </div>
                        ) : null}

                        {this.props.state.disPlayPolymorPhicForm === true ? (
                          <div className="col-md-3 formInfo">
                            <Field
                              name="polymorphicForm"
                              type="text"
                              className="form-control customInput"
                              placeholder={
                                this.props.dynamic_lang.form_component
                                  .polymorphic_form
                              }
                            />
                            <span className="info">
                              {" "}
                              (
                              {
                                this.props.dynamic_lang.form_component
                                  .if_applicable
                              }
                              )
                            </span>
                            <div className="clearfix"></div>
                          </div>
                        ) : null}

                        {this.props.state.downloadFile === true ? (
                          <div className="col-md-4 text-center my-auto">
                            <a href={downloadFile} download>
                              <img
                                src={downloadIcon}
                                width="15"
                                height="15"
                                id="UncontrolledPopover"
                                type="button"
                                style={{ marginRight: "8px" }}
                              />
                              {/* // <span className="glyphicon glyphicon-download-alt"></span> */}
                              {
                                this.props.dynamic_lang.form_component
                                  .drl_template_for_cda
                              }
                            </a>

                            {/* <Link
                            to={downloadFile}
                            download
                            className="download-product"
                            style={{fontWeight:'normal', fontSize:'15px', textAlign:'center'}}
                          >
                            DRL Template for CDA


                          </Link> */}
                          </div>
                        ) : null}
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          {this.props.state.displayApplicantName && (
                            <textarea
                              name="applicant_name"
                              placeholder={
                                this.props.dynamic_lang.form_component
                                  .applicants_name_address
                              }
                              className="form-control customInput"
                              onChange={(event) =>
                                setFieldValue(
                                  "applicant_name",
                                  event.target.value
                                )
                              }
                              style={{
                                height: "auto",
                                minHeight: 90,
                                borderRadius: 10,
                                paddingTop: 10,
                              }}
                            ></textarea>
                          )}

                          {this.props.state.displayDocReq && (
                            <textarea
                              name="doc_required"
                              placeholder={
                                this.props.dynamic_lang.form_component
                                  .documents_required
                              }
                              className="form-control customInput"
                              onChange={(event) =>
                                setFieldValue(
                                  "doc_required",
                                  event.target.value
                                )
                              }
                              style={{
                                height: "auto",
                                minHeight: 90,
                                borderRadius: 10,
                                paddingTop: 10,
                              }}
                            ></textarea>
                          )}
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <Editor
                            name="messages"
                            content={values.messages}
                            init={{
                              menubar: false,
                              branding: false,
                              placeholder: this.props.dynamic_lang
                                .form_component.textarea_requirements,
                              plugins:
                                "link image table hr visualblocks code placeholder textcolor",
                              toolbar:
                                "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | bullist numlist | blockquote image table  hr | formatselect | visualblocks code ",
                              content_css: ["/css/editor.css"],
                              color_map: [
                                "000000",
                                "Black",
                                "808080",
                                "Gray",
                                "FFFFFF",
                                "White",
                                "FF0000",
                                "Red",
                                "FFFF00",
                                "Yellow",
                                "008000",
                                "Green",
                                "0000FF",
                                "Blue",
                              ],
                            }}
                            onEditorChange={(value) =>
                              setFieldValue("messages", value)
                            }
                          />
                        </div>
                      </div>
                      <br />
                      <div
                        className="messageserror"
                        style={{
                          display:
                            (rejectedFile && rejectedFile.length) > 0
                              ? "block"
                              : "none",
                        }}
                      >
                        <Link to="#" className="close">
                          <img
                            src={closeIcon}
                            width="17.69px"
                            height="22px"
                            onClick={(e) => this.removeFile_Error()}
                          />
                        </Link>
                        {this.state.rejectedFile &&
                          this.state.rejectedFile.map((file, index) => {
                            return (
                              <div key={index}>
                                <ul className="">
                                  <li className="">{file.errorText}</li>
                                </ul>
                              </div>
                            );
                          })}
                      </div>

                      {displayCCList && (
                        <div className="row">
                          <div className="col-md-12">
                            <Select
                              options={
                                this.props.customerList !== null
                                  ? this.props.customerList
                                  : []
                              }
                              isSearchable={true}
                              isClearable={true}
                              value={agent_customer_id}
                              placeholder="Select User *"
                              onChange={(e) =>
                                this.getAgentCCList(e, setFieldValue)
                              }
                            />
                          </div>
                        </div>
                      )}

                      {this.props.state.downloadFileBelow === true ? (
                        <div className="col-md-12">
                          <label className="mb-0">
                            {
                              this.props.dynamic_lang.form_component
                                .please_upload_agreement
                            }
                            <span className="astrix">*</span>
                          </label>
                        </div>
                      ) : (
                        <div className="col-md-4">
                          <label className="mb-0">
                            {this.props.state.labelAttachment}
                            {this.props.state.astrixRequired === true ? (
                              <span className="astrix">*</span>
                            ) : null}
                          </label>
                        </div>
                      )}

                      {share_with_agent === true && displayCCList === false && (
                        <div className="row mt-10">
                          <div className="col-md-12">
                            <div className="form-check">
                              <label className="form-check-label">
                                <input
                                  type="checkbox"
                                  className="form-check-input"
                                  value={1}
                                  defaultChecked={true}
                                  onChange={(e) =>
                                    this.shareWithAgent(e, setFieldValue)
                                  }
                                />
                                {
                                  this.props.dynamic_lang.form_component
                                    .display_request_agent
                                }
                              </label>
                            </div>
                          </div>
                        </div>
                      )}

                      <div className="clearfix" />
                      <div className="row uploadSec">
                        {/* <div className="col-md-4">
                          <input type="file" className="customFile" />
                        </div> */}

                        <div className="col-md-3">
                          <Dropzone
                            //accept = 'image/jpeg, image/png'
                            onDrop={(acceptedFiles) =>
                              this.setDropZoneFiles(
                                acceptedFiles,
                                setErrors,
                                setFieldValue
                              )
                            }
                          >
                            {({ getRootProps, getInputProps }) => (
                              <section>
                                <div {...getRootProps()} className="customFile">
                                  <input
                                    {...getInputProps()}
                                    ref={this.inputRef}
                                    style={{ display: "block" }}
                                  />
                                </div>
                              </section>
                            )}
                          </Dropzone>
                        </div>
                        <div className="col-md-9 my-auto">
                          {files &&
                            files.map((file, index) => {
                              return (
                                <div key={index} className="form-check-inline">
                                  <label className="form-check-label">
                                    <input
                                      id={index}
                                      type="checkbox"
                                      className="form-check-input"
                                      value={index}
                                      checked={file.checked}
                                      onChange={(e) => this.recId(e)}
                                    />
                                    {file.name}
                                  </label>
                                </div>
                              );
                            })}
                          {files && files.length > 0 ? (
                            <input
                              // type="submit"
                              className="button"
                              //value="Remove selected"
                              defaultValue={
                                this.props.dynamic_lang.form_component
                                  .remove_selected
                              }
                              onClick={(e) => this.deleteFile(e, setFieldValue)}
                            />
                          ) : (
                            ""
                          )}
                        </div>
                      </div>

                      {((displayCCList &&
                        ccAgentCustomerList &&
                        ccAgentCustomerList.length > 0) ||
                        (pre_selected && pre_selected.length > 0)) && (
                        <div className="row">
                          <div className="col-md-12">
                            <span
                              className="fieldset-legend"
                              id="DisabledAutoHideExample"
                            >
                              {
                                this.props.dynamic_lang.form_component
                                  .grant_access_request
                              }
                              <img
                                src={infoIcon}
                                width="15.44"
                                height="18"
                                id="DisabledAutoHideExample"
                                type="button"
                              />
                              <Tooltip
                                placement="right"
                                isOpen={tooltipOpen}
                                autohide={false}
                                target="DisabledAutoHideExample"
                                toggle={this.toggle}
                              >
                                {
                                  this.props.dynamic_lang.form_component
                                    .grant_access_request_tooltip
                                }
                              </Tooltip>
                            </span>

                            {pre_selected &&
                              pre_selected.map((cust, index) => {
                                return (
                                  <div key={index} className="form-check">
                                    <label className="form-check-label">
                                      <input
                                        id={index}
                                        type="checkbox"
                                        className="form-check-input"
                                        defaultChecked={true}
                                        disabled={true}
                                      />
                                      {htmlDecode(cust.first_name)}{" "}
                                      {htmlDecode(cust.last_name)}
                                    </label>
                                  </div>
                                );
                              })}

                            {displayCCList &&
                              ccAgentCustomerList &&
                              ccAgentCustomerList.map((user, index) => {
                                return (
                                  <div key={index} className="form-check">
                                    <label className="form-check-label">
                                      <input
                                        id={index}
                                        type="checkbox"
                                        className="form-check-input"
                                        value={index}
                                        //checked={user.status}
                                        onChange={(e) =>
                                          this.agentcustomerChecked(e)
                                        }
                                      />
                                      {htmlDecode(user.first_name)}{" "}
                                      {htmlDecode(user.last_name)} (
                                      {htmlDecode(user.company_name)})
                                    </label>
                                  </div>
                                );
                              })}
                          </div>
                        </div>
                      )}

                      {this.props.state.downloadFileBelow === true ? (
                        <div className="col-md-4 text-center my-auto">
                          <a href={downloadFileAgrement} download>
                            <img
                              src={downloadIcon}
                              width="15"
                              height="15"
                              id="UncontrolledPopover"
                              type="button"
                              style={{ marginRight: "8px" }}
                            />
                            {/* // <span className="glyphicon glyphicon-download-alt"></span> */}
                            {
                              this.props.dynamic_lang.form_component
                                .drl_agreement_format
                            }
                          </a>

                          {/* <Link
                            to={downloadFile}
                            download
                            className="download-product"
                            style={{fontWeight:'normal', fontSize:'15px', textAlign:'center'}}
                          >
                            DRL Template for CDA


                          </Link> */}
                        </div>
                      ) : null}

                      {cc_Customers && cc_Customers.length > 0 && (
                        <div className="row mt-10">
                          <div className="col-md-12">
                            <span
                              className="fieldset-legend"
                              id="DisabledAutoHideExample"
                            >
                              {
                                this.props.dynamic_lang.form_component
                                  .grant_access_request
                              }
                              <img
                                src={infoIcon}
                                width="15.44"
                                height="18"
                                id="DisabledAutoHideExample"
                                type="button"
                              />
                              <Tooltip
                                placement="right"
                                isOpen={tooltipOpen}
                                autohide={false}
                                target="DisabledAutoHideExample"
                                toggle={this.toggle}
                              >
                                {
                                  this.props.dynamic_lang.form_component
                                    .grant_access_request_tooltip
                                }
                              </Tooltip>
                            </span>
                            {cc_Customers &&
                              cc_Customers.map((user, index) => {
                                return (
                                  <div key={index} className="form-check">
                                    <label className="form-check-label">
                                      <input
                                        id={index}
                                        type="checkbox"
                                        className="form-check-input"
                                        value={index}
                                        //checked={user.status}
                                        onChange={(e) =>
                                          this.customerChecked(e)
                                        }
                                      />
                                      {htmlDecode(user.first_name)}{" "}
                                      {htmlDecode(user.last_name)} (
                                      {htmlDecode(user.company_name)})
                                    </label>
                                  </div>
                                );
                              })}
                          </div>
                        </div>
                      )}

                        

                      {/* <div className="row"> */}
                        <div className="col text-center">
                          <button className="btn btn-default">
                            {this.props.dynamic_lang.form_component.submit}
                          </button>
                          <button
                            className="btn btn-default btn-cancel"
                            onClick={() =>
                              this.props.history.push("/dashboard")
                            }
                          >
                            {this.props.dynamic_lang.form_component.cancel}
                          </button>
                        </div>
                      {/* </div> */}
                    </Form>
                  );
                }}
              </Formik>
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    customerList: state.user.customerList,
    selCompany: state.user.selCompany,
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(FormComponent);
