import React, { Component } from "react";
// import { Link } from "react-router-dom";
// import Layout from "../Layout/layout";
// import closeIcon from "../../assets/images/times-solid-red.svg";
// import phoneIcon from "../../assets/images/phone-alt-solid.svg";
// import { Formik, Field, Form } from "formik";
// import * as Yup from "yup";
// import axios from "../../shared/axios";
// import Loader from "../Loader/loader";
// import { htmlDecode } from "../../shared/helper";
// import Select from "react-select";

import { BasicUserData } from "../../shared/helper";
import { connect } from "react-redux";
import { setExplicitPopup } from "../../store/actions/auth";

class contact extends Component {

  componentDidMount() {
    const users = BasicUserData();
    if (users) {
      this.props.setPopup({task_id_cypher:this.props.match.params.id});
      this.props.history.push("/dashboard");  
    }else{
      this.props.history.push("/login");
    }
  }

  render() {
    return null;
  }
}
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setPopup: (data, onSuccess) => dispatch(setExplicitPopup(data, onSuccess))
  };
};
//export default contact;
export default connect(mapStateToProps, mapDispatchToProps)(contact);
