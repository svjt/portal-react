import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Button,
  Popover,
  UncontrolledPopover,
  PopoverHeader,
  PopoverBody,
} from "reactstrap";
import axios from "../../shared/axios";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import Autosuggest from "react-autosuggest";
import closeIcon from "../../assets/images/times-solid-red.svg";
import closeIconSuccess from "../../assets/images/times-solid-green.svg";
import { htmlDecode } from "../../shared/helper";
import Loader from "../Loader/loader";
import { connect } from "react-redux";

/*Initialization*/
const initialValues = {
  first_name: "",
  last_name: "",
  email: "",
  mobile: "",
  company_name: "",
  country_id: "",
  language_code: "",
  agreeToTerms: "",
};

const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

/*Validation*/

class Register extends Component {
  state = {
    country: [],
    language: [],
    company: [],
    errMsg: null,
    showLoader: false,
    value: "", // for auto-suggest
    suggestions: [], // for auto-suggest
    successMsg: "",
    website_lang: "",
  };

  componentDidMount() {
    if (this.props.isLoggedIn) {
      this.props.history.push("/dashbord");
    }
    this.getCountryList();
    this.getLanguageList();
    document.title =
      this.state.website_lang.signup.document_title + " | Dr. Reddy's API";
  }

  componentWillMount() {
    if (sessionStorage.website_lang && sessionStorage.website_lang != "") {
      let lang_data = require(`../../assets/lang/${sessionStorage.website_lang}.json`);
      this.setState({ website_lang: lang_data });
    } else {
      let lang_data = require(`../../assets/lang/en.json`);
      this.setState({ website_lang: lang_data });
    }
  }

  getCountryList() {
    if (sessionStorage.website_lang && sessionStorage.website_lang != "") {
      var lang_type = sessionStorage.website_lang;
    } else {
      var lang_type = "en";
    }
    axios
      .get(`/country_open/${lang_type}`)
      .then((res) => {
        this.setState({
          country: [
            {
              country_id: "",
              country_name: this.state.website_lang.signup.plh_country,
            },
          ].concat(res.data.data),
        });
      })
      .catch((err) => {});
  }

  getLanguageList() {
    if (sessionStorage.website_lang && sessionStorage.website_lang != "") {
      var lang_type = sessionStorage.website_lang;
    } else {
      var lang_type = "en";
    }
    axios
      .get(`/language_open/${lang_type}`)
      .then((res) => {
        this.setState({
          language: [
            { id: "", language: this.state.website_lang.signup.plh_language },
          ].concat(res.data.data),
        });
      })
      .catch((err) => {});
  }

  handleSubmit = (values, actions) => {
    this.setState({ errMsg: "" });
    this.setState({ showLoader: true });

    const request_body = {
      first_name: values.first_name.trim(),
      last_name: values.last_name.trim(),
      email: values.email.trim(),
      mobile: values.mobile.trim(),
      company_name: values.company_name.trim(),
      country_id: values.country_id.trim(),
      language_code: values.language_code.trim(),
    };

    axios
      .post("/user/add", request_body)
      .then((res) => {
        if (res.status == "200") {
          this.setState({ showLoader: false });
          actions.setFieldValue("company_name", "");
          actions.setFieldValue("agreeToTerms", "");
          actions.setSubmitting(false);
          actions.resetForm();
          this.setState({
            successMsg: this.state.website_lang.signup.thank_you_msg,
            value: "",
          });
        }
      })
      .catch((err) => {
        this.setState({ showLoader: false });
        actions.setSubmitting(false);
        if (err.data.status === 2) {
          actions.setErrors(err.data.errors);
        } else {
          this.setState({ errMsg: err.data.message });
        }
      });
  };
  removeError = () => {
    this.setState({ errMsg: "" });
  };

  hideSuccessMsg = () => {
    this.setState({ successMsg: "" });
  };

  enterPressed = (event) => {
    var code = event.keyCode || event.which;
    if (code === 13) {
      //13 is the enter keycode

      this.setState({
        searchkey: event.target.value,
      });
      //this.getRequests(1, event.target.value);
    }
  };

  onReferenceChange = (event, { newValue }, setFieldValue) => {
    this.setState({ value: newValue }, () =>
      setFieldValue("company_name", newValue)
    );
  };

  onSuggestionsFetchRequested = ({ value, reason }) => {
    const inputValue = value.toLowerCase();
    const inputLength = inputValue.length;

    if (inputLength === 0) {
      return [];
    } else {
      if (inputLength > 0) {
        axios
          .get(`/company-search/${value}`)
          .then((res) => {
            this.setState({
              suggestions: res.data.data,
            });
            return this.state.suggestions;
          })
          .catch((err) => {
            console.log("==========", err);
          });
      }
    }
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  setSuggesstionValue = (suggestion, setFieldValue) => {
    setFieldValue("company_name", suggestion.company_name);
    return suggestion.company_name;
  };

  renderSuggestion = (suggestion) => {
    return (
      <div>
        <Link to="#">{suggestion.company_name}</Link>
      </div>
    );
  };

  render() {
    const { country, company, suggestions, value } = this.state;

    const signupvalidation = Yup.object().shape({
      first_name: Yup.string().required(
        this.state.website_lang.display_error.login_register.first_name
      ).trim(),
      last_name: Yup.string().required(
        this.state.website_lang.display_error.login_register.last_name
      ).trim(),
      email: Yup.string()
        .required(this.state.website_lang.display_error.login_register.email)
        .email(
          this.state.website_lang.display_error.login_register.valid_email
        ).trim(),
      mobile: Yup.string()
        .required(this.state.website_lang.display_error.login_register.mobile)
        .matches(
          phoneRegExp,
          this.state.website_lang.display_error.login_register.valid_mobile
        ).trim(),
      company_name: Yup.string().required(
        this.state.website_lang.display_error.login_register.company
      ).trim(),
      country_id: Yup.string().required(
        this.state.website_lang.display_error.login_register.country
      ).trim(),
      language_code: Yup.string().required(
        this.state.website_lang.display_error.login_register.language
      ).trim(),
      agreeToTerms: Yup.boolean()
      .label()
      .test(
        "is-true",
        this.state.website_lang.display_error.login_register.terms,
        (value) => value === true
      ),
    });

    return (
      <>
        {this.state.showLoader === true || this.state.website_lang === "" ? (
          <Loader />
        ) : null}
        <div className="outside bg-white">
          <div className="row">
            <div className="col-md-6">
              <div className="row h-100 justify-content-center align-items-center">
                <div className="col-md-8 login-form-sec">
                  <div className="mx-auto text-center">
                    <div className="logo-login">
                      <a
                        href={process.env.REACT_APP_PORTAL_URL}
                        target="_blank"
                      >
                        <img
                          src={require("../../assets/images/logo-blue.jpg")}
                        />
                      </a>
                    </div>
                    <h2>{this.state.website_lang.signup.title}</h2>
                    {this.state.successMsg !== null &&
                      this.state.successMsg !== "" &&
                      this.state.successMsg !== undefined && (
                        <div className="messagessuccess">
                          <Link to="#" className="close">
                            <img
                              src={closeIconSuccess}
                              width="17.69px"
                              height="22px"
                              onClick={(e) => this.hideSuccessMsg()}
                            />
                          </Link>
                          {this.state.successMsg}
                        </div>
                      )}

                    <Formik
                      initialValues={initialValues}
                      validationSchema={signupvalidation}
                      onSubmit={this.handleSubmit}
                    >
                      {({
                        values,
                        errors,
                        isValid,
                        touched,
                        isSubmitting,
                        setFieldValue,
                      }) => {
                        return (
                          <Form>
                            <div
                              className="messageserror"
                              style={{
                                display:
                                  this.state.errMsg &&
                                  this.state.errMsg !== null &&
                                  this.state.errMsg !== ""
                                    ? "block"
                                    : "none",
                              }}
                            >
                              <Link to="#" className="close">
                                <img
                                  src={closeIcon}
                                  width="17.69px"
                                  height="22px"
                                  onClick={(e) => this.removeError()}
                                />
                              </Link>
                              <div>
                                <span className="errorMsg">
                                  {this.state.errMsg}
                                </span>
                                <Link to="password/">
                                  ={this.state.website_lang.signup.f_password}
                                </Link>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-md-6">
                                <Field
                                  name="first_name"
                                  type="text"
                                  className="form-control customInput"
                                  placeholder={
                                    this.state.website_lang.signup.plh_f_name
                                  }
                                />
                                {errors.first_name && touched.first_name ? (
                                  <span className="errorMsg">
                                    {errors.first_name}
                                  </span>
                                ) : null}
                              </div>
                              <div className="col-md-6">
                                <Field
                                  name="last_name"
                                  type="text"
                                  className="form-control customInput"
                                  placeholder={
                                    this.state.website_lang.signup.plh_l_name
                                  }
                                />
                                {errors.last_name && touched.last_name ? (
                                  <span className="errorMsg">
                                    {errors.last_name}
                                  </span>
                                ) : null}
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-md-6">
                                <Field
                                  name="email"
                                  type="text"
                                  className="form-control customInput"
                                  placeholder={
                                    this.state.website_lang.signup.plh_email
                                  }
                                />
                                {errors.email && touched.email ? (
                                  <span className="errorMsg">
                                    {errors.email}
                                  </span>
                                ) : null}
                              </div>
                              <div className="col-md-6">
                                <Field
                                  name="mobile"
                                  type="text"
                                  className="form-control customInput"
                                  placeholder={
                                    this.state.website_lang.signup.plh_mobile
                                  }                                  
                                />
                                {errors.mobile && touched.mobile ? (
                                  <span className="errorMsg">
                                    {errors.mobile}
                                  </span>
                                ) : null}
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-md-6">
                                {/* <Field
                                name="company_name"
                                type="text"
                                className="form-control customInput"
                                placeholder="Company Name"
                              /> */}

                                <Autosuggest
                                  suggestions={suggestions}
                                  onSuggestionsFetchRequested={
                                    this.onSuggestionsFetchRequested
                                  }
                                  onSuggestionsClearRequested={
                                    this.onSuggestionsClearRequested
                                  }
                                  getSuggestionValue={(suggestion) =>
                                    this.setSuggesstionValue(
                                      suggestion,
                                      setFieldValue
                                    )
                                  }
                                  renderSuggestion={this.renderSuggestion}
                                  inputProps={{
                                    placeholder: this.state.website_lang.signup
                                      .plh_comp_name,
                                    value,
                                    type: "search",
                                    onChange: (event, object) => {
                                      return this.onReferenceChange(
                                        event,
                                        object,
                                        setFieldValue
                                      );
                                    },
                                    onKeyPress: this.enterPressed.bind(this),
                                    className: "form-control customInput",
                                  }}
                                  name="company_name"
                                />

                                {/* <Field component="select" name="company_id" className="form-control customeSelect" value={values.company_id}>
                                {this.state.company.map((company) => <option key={company.company_id} value={company.company_id}>{company.company_name}</option>)}
                              </Field> */}
                                {errors.company_name && touched.company_name ? (
                                  <span className="errorMsg">
                                    {errors.company_name}
                                  </span>
                                ) : null}
                              </div>
                              <div className="col-md-6">
                                <Field
                                  component="select"
                                  name="country_id"
                                  className="form-control customeSelect"
                                  value={values.country_id}
                                >
                                  {this.state.country.map((country) => (
                                    <option
                                      key={country.country_id}
                                      value={country.country_id}
                                    >
                                      {htmlDecode(country.country_name)}
                                    </option>
                                  ))}
                                </Field>
                                {errors.country_id && touched.country_id ? (
                                  <span className="errorMsg">
                                    {errors.country_id}
                                  </span>
                                ) : null}
                              </div>
                            </div>

                            <div className="row">
                              <div className="col-md-6">
                                <Field
                                  component="select"
                                  name="language_code"
                                  className="form-control customeSelect"
                                  value={values.language_code}
                                >
                                  {this.state.language.map((lang) => (
                                    <option key={lang.code} value={lang.code}>
                                      {htmlDecode(lang.language)}
                                    </option>
                                  ))}
                                </Field>
                                {errors.language_code &&
                                touched.language_code ? (
                                  <span className="errorMsg">
                                    {errors.language_code}
                                  </span>
                                ) : null}
                              </div>
                            </div>

                            <div className="row">
                              <div className="col-md-12 my-auto">
                                <div className="form-check-inline">
                                  <label className="form-check-label">
                                    <Field
                                      type="checkbox"
                                      name="agreeToTerms"
                                      className="form-check-input"
                                      value={values.agreeToTerms}
                                      checked={values.agreeToTerms}
                                      onChange={(e) => {
                                        setFieldValue("agreeToTerms", e.target.checked);
                                      }}
                                    />
                                    {this.state.website_lang.contact_us.agree}{" "}
                                    <a
                                      href={`${process.env.REACT_APP_PORTAL_URL}${this.state.website_lang.contact_us.privacy_policy_url}`}
                                      target="_blank"
                                    >
                                      {
                                        this.state.website_lang.contact_us
                                          .privacy_policy
                                      }
                                    </a>{" "}
                                    {this.state.website_lang.contact_us.and}{" "}
                                    <a
                                      href={`${process.env.REACT_APP_PORTAL_URL}${this.state.website_lang.contact_us.terms_of_use_url}`}
                                      target="_blank"
                                    >
                                      {this.state.website_lang.contact_us.terms_use}
                                    </a>{" "}
                                    {this.state.website_lang.contact_us.website}
                                  </label>
                                </div>
                                {errors.agreeToTerms &&
                                touched.agreeToTerms ? (
                                  <span className="errorMsg">
                                    {errors.agreeToTerms}
                                  </span>
                                ) : null}
                              </div>
                            </div>

                            <div className="row">
                              <div className="col text-center">
                                <button
                                  id=""
                                  className="btn btn-default btn-secondary btn-md"
                                  type="submit"
                                >
                                  {isSubmitting
                                    ? this.state.website_lang.signup.create_new
                                    : this.state.website_lang.signup.create}
                                </button>
                              </div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>
                    <div className="row">
                      <div className="col-md-12">
                        <p>
                          <Link to="/">
                            {this.state.website_lang.signup.an_account}
                          </Link>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="regiter-graphic-area">
                <div className="login-graphic-img">
                  <img
                    src={require("../../assets/images/register-graphic-img.jpg")}
                  />
                </div>
              </div>
            </div>
            <div className="clearfix" />
          </div>
          <div className="clearfix" />
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.auth.token !== null ? true : false,
  };
};

export default connect(mapStateToProps)(Register);
