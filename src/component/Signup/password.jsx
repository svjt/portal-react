import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "../../shared/axios";
//import Layout from '../Layout/layout';
import {
  Button,
  Popover,
  UncontrolledPopover,
  PopoverHeader,
  PopoverBody,
} from "reactstrap";

//import infoIcon from '../../Linkssets/images/info-circle-solid.svg';
import { Formik, Field, Form } from "formik";
import closeIcon from "../../assets/images/times-solid-red.svg";
import * as Yup from "yup";
import closeIconSuccess from "../../assets/images/times-solid-green.svg";

import { withCookies, Cookies } from "react-cookie";

import { CookiesProvider } from "react-cookie";
import Loader from "../Loader/loader";
import { connect } from "react-redux";

/*Initialization*/
const initialValues = {
  email_address: "",
};
var message = "";
/*Validation*/

class Password extends Component {
  constructor(props) {
    super(props);
    message = this.props.location.state
      ? this.props.location.state.message
      : "";
  }
  state = {
    serverMessage: "",
    showLoader: false,
    mailSent: false,
    name: this.props.cookies.cookies.name,
    deniedPermissionCookies: false,
    website_lang: "",
  };

  componentDidMount() {
    if (this.props.isLoggedIn) {
      this.props.history.push("/dashboard");
    }

    if (message !== null) {
      this.props.history.push({
        pathname: "",
        state: "",
      });
    }

    document.title =
      this.state.website_lang.reset_pass.title + " | Dr. Reddy's API";
    this.setState({ name: this.props.cookies.cookies.name });
  }

  componentWillMount() {
    if (sessionStorage.website_lang && sessionStorage.website_lang != "") {
      let lang_data = require(`../../assets/lang/${sessionStorage.website_lang}.json`);
      this.setState({ website_lang: lang_data });
    } else {
      let lang_data = require(`../../assets/lang/en.json`);
      this.setState({ website_lang: lang_data });
    }
  }

  setCookies = () => {
    const { cookies } = this.props;
    cookies.set("name", "DrReddy", { path: "/" });
    this.setState({ name: this.props.cookies.cookies.name });
  };
  deniedCookiesStore = () => {
    this.setState({ deniedPermissionCookies: true });
  };

  handleSubmit = (values, actions) => {
    this.setState({ serverMessage: "" });
    this.setState({ showLoader: true });

    axios
      .post("forgotpassword", {
        email: values.email_address.trim(),
      })
      .then((res) => {
        if (res.status === 200) {
          this.setState({ showLoader: false });
          message = "";
          this.props.history.push({
            pathname: "/login",

            state: {
              message: this.state.website_lang.reset_pass.further_instructions,
            },
          });
          this.setState({ mailSent: true });
          // this.props
        }
      })
      .catch((err) => {
        // alert(JSON.stringify(err));
        this.setState({ showLoader: false });

        if (err.data.status === 2) {
          this.setState({ serverMessage: err.data.errors, mailSent: false });
        } else {
          this.setState({ serverMessage: err.data.message, mailSent: false });
        }
      });
  };

  hideFormikError = (setErrors) => {
    setErrors({});
    this.setState({ serverMessage: "" });
  };

  hideSuccessMsg = () => {
    this.setState({ mailSent: false });
  };
  hideErrorMsg = () => {
    message = "";
  };

  hideServerError = () => {
    this.setState({ serverMessage: "" });
  };
  render() {
    const forgetPasswordvalidation = Yup.object().shape({
      email_address: Yup.string().trim()
        .required(this.state.website_lang.display_error.login_register.email)
        .email(
          this.state.website_lang.display_error.login_register.valid_email
        ),
    });
    return (
      <>
        {this.state.showLoader === true || this.state.website_lang === "" ? (
          <Loader />
        ) : null}
        <div className="outside bg-white">
          <div className="row">
            <div className="col-md-6">
              <div className="row h-100 justify-content-center align-items-center">
                <div className="login-form-sec">
                  <div className="login-form-box mx-auto text-center">
                    <div className="login-form-wrapper">
                      <div className="logo-login">
                        <a
                          href={process.env.REACT_APP_PORTAL_URL}
                          target="_blank"
                        >
                          <img
                            src={require("../../assets/images/logo-blue.jpg")}
                          />
                        </a>
                      </div>
                      <h2>{this.state.website_lang.reset_pass.title}</h2>
                    </div>
                    {message !== null &&
                      message !== "" &&
                      message !== undefined && (
                        <div className="messageserror">
                          <Link to="#" className="close">
                            <img
                              src={closeIcon}
                              width="17.69px"
                              height="22px"
                              onClick={(e) => this.hideErrorMsg()}
                            />
                          </Link>
                          {message}
                        </div>
                      )}
                    <Formik
                      initialValues={initialValues}
                      validationSchema={forgetPasswordvalidation}
                      onSubmit={this.handleSubmit}
                    >
                      {({ errors, touched, setErrors }) => {
                        return (
                          <Form>
                            <div
                              className="messagessuccess"
                              style={{
                                display:
                                  this.state.mailSent &&
                                  (this.state.serverMessage === "" ||
                                    this.state.serverMessage === null) &&
                                  (errors.email_address === false ||
                                    errors.email_address === undefined)
                                    ? "block"
                                    : "none",
                              }}
                            >
                              <Link to="#" className="close">
                                <img
                                  src={closeIconSuccess}
                                  width="17.69px"
                                  height="22px"
                                  onClick={(e) => this.hideSuccessMsg()}
                                />
                              </Link>
                              {this.state.mailSent ? (
                                <div>
                                  {
                                    this.state.website_lang.reset_pass
                                      .email_sent
                                  }
                                </div>
                              ) : null}
                            </div>
                            <div
                              className="messageserror"
                              style={{
                                display:
                                  (errors.email_address &&
                                    touched.email_address) ||
                                  (this.state.serverMessage &&
                                    this.state.serverMessage !== "")
                                    ? "block"
                                    : "none",
                              }}
                            >
                              <Link to="#" className="close">
                                <img
                                  src={closeIcon}
                                  width="17.69px"
                                  height="22px"
                                  onClick={(e) =>
                                    this.hideFormikError(setErrors)
                                  }
                                />
                              </Link>
                              <div>
                                <ul className="">
                                  {errors.email_address &&
                                  touched.email_address ? (
                                    <span className="errorMsg">
                                      {errors.email_address}
                                    </span>
                                  ) : (
                                    <span className="errorMsg">
                                      {this.state.serverMessage}
                                    </span>
                                  )}
                                </ul>
                              </div>
                            </div>
                            {/* {console.log("error", errors.email)}
                            {console.log("touched", touched)} */}
                            {/* <div
                            className="messageserror"
                            style={{
                              display:
                                this.state.serverMessage &&
                                this.state.serverMessage !== "" &&
                                !errors.email_address
                                  ? "block"
                                  : "none"
                            }}
                          >
                            <Link to='#' className="close">
                              <img
                                src={closeIcon}
                                width="17.69px"
                                height="22px"
                                onClick={e => this.hideServerError()}
                              />
                            </Link>
                            <div>
                              <ul className="">
                                <span className="errorMsg">
                                  {this.state.serverMessage}
                                </span>
                              </ul>
                            </div>
                          </div> */}
                            <div className="login-form-wrapper">
                              <div className="row">
                                <div className="col-md-12">
                                  {/* <input
                                type="text"
                                className="form-control customInput"
                                placeholder="Username or email address"
                              />
                               {errors.email_address && touched.email_address ? (
                                <span className="errorMsg">{errors.firstname}</span>
                              ) : null} */}
                                  <Field
                                    name="email_address"
                                    type="text"
                                    className="form-control customInput"
                                    placeholder={
                                      this.state.website_lang.reset_pass
                                        .plh_email_address
                                    }
                                  />
                                  {/* {errors.email_address
                                ? this.state.mailSent
                                  ? this.setState({ mailSent: false })
                                  : null
                                : null}
                              {errors.email_address
                                ? this.state.serverMessage === ""
                                  ? null
                                  : this.setState({ serverMessage: "" })
                                : null}
                              {errors.email_address && touched.email_address ? (
                                <span className="errorMsg">
                                  {errors.email_address}
                                </span>
                              ) : null} */}
                                </div>
                              </div>

                              <div className="row">
                                <div className="col-md-12">
                                  <p>
                                    {this.state.website_lang.reset_pass.content}
                                  </p>
                                </div>
                              </div>

                              <div className="row">
                                <div className="col text-center">
                                  <button
                                    className="btn btn-default btn-secondary btn-lg"
                                    type="submit"
                                  >
                                    {this.state.website_lang.reset_pass.submit}
                                  </button>
                                </div>
                              </div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>
                    <div className="login-form-wrapper">
                      <div className="row">
                        <div className="col-md-12">
                          <p>
                            <Link to="/">
                              {this.state.website_lang.reset_pass.an_account}
                            </Link>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="login-graphic-area">
                <div className="login-graphic-img">
                  <img
                    src={require("../../assets/images/login-graphic-img.jpg")}
                  />
                </div>
              </div>
            </div>
            <div className="clearfix" />
          </div>
          <div
            className="sliding-popup-bottom"
            style={{
              display:
                (this.state.name === null ||
                  this.state.name === "" ||
                  this.state.name === undefined) &&
                this.state.deniedPermissionCookies === false
                  ? "block"
                  : "none",
            }}
          >
            <div className="eu-cookie-compliance-banner eu-cookie-compliance-banner-info">
              <div className="popup-content info eu-cookie-compliance-content">
                <div id="popup-text" className="eu-cookie-compliance-message">
                  <h2>{this.state.website_lang.cookie.title}</h2>
                  <p>{this.state.website_lang.cookie.content}</p>
                  <button
                    type="button"
                    className="find-more-button eu-cookie-compliance-more-button find-more-button-processed"
                  >
                    {this.state.website_lang.cookie.m_info}
                  </button>
                </div>
                <div
                  id="popup-buttons"
                  className="eu-cookie-compliance-buttons"
                >
                  <button
                    type="button"
                    className="agree-button eu-cookie-compliance-secondary-button"
                    onClick={(e) => this.setCookies()}
                  >
                    {this.state.website_lang.cookie.agree}
                  </button>
                  <button
                    type="button"
                    className="decline-button eu-cookie-compliance-default-button"
                    onClick={(e) => this.deniedCookiesStore()}
                  >
                    {this.state.website_lang.cookie.thanks}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.auth.token !== null ? true : false,
  };
};

export default connect(mapStateToProps)(withCookies(Password));
