import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "../../shared/axios";
//import Layout from '../Layout/layout';
import {
  Button,
  Popover,
  UncontrolledPopover,
  PopoverHeader,
  PopoverBody,
} from "reactstrap";
//import infoIcon from '../../Linkssets/images/info-circle-solid.svg';
import closeIcon from "../../assets/images/times-solid-red.svg";

import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import Loader from "../Loader/loader";
import { connect } from "react-redux";

/*Initialization*/
const initialValues = {
  password: "",
  confirm_password: "",
};

/*Validation*/

class Password extends Component {
  state = {
    token: "",
    showLoader: false,
    errMsg: null,
    website_lang: "",
  };
  componentDidMount() {
    if (this.props.isLoggedIn) {
      this.props.history.push("/dashboard");
    }
    this.setState({
      token: this.props.match.params.token,
    });

    axios
      .post("check-token", {
        token: this.props.match.params.token,
      })
      .then((res) => {})
      .catch((err) => {
        if (err.data.status === 3) {
          this.props.history.push({
            pathname: "/forgot_password",

            state: {
              message: this.state.website_lang.reset_pass.thank_you_msg,
            },
          });
        }
      });
  }

  componentWillMount() {
    if (sessionStorage.website_lang && sessionStorage.website_lang != "") {
      let lang_data = require(`../../assets/lang/${sessionStorage.website_lang}.json`);
      this.setState({ website_lang: lang_data });
    } else {
      let lang_data = require(`../../assets/lang/en.json`);
      this.setState({ website_lang: lang_data });
    }
  }

  handleSubmit = (values, actions) => {
    this.setState({ errMsg: "" });
    this.setState({ showLoader: true });

    axios
      .post("resetpassword", {
        token: this.state.token,
        password: values.password.trim(),
      })
      .then((res) => {
        if (res.status === 200) {
          this.setState({ showLoader: false });
          this.props.history.push({
            pathname: "/login",

            state: {
              message: this.state.website_lang.reset_pass.success_msg,
            },
          });

          //this.props.history.push({ pathname: })
        } else {
          alert(this.state.website_lang.reset_pass.alert);
        }
      })
      .catch((err) => {
        this.setState({ showLoader: false });
        if (err.data.status === 2) {
          actions.setErrors(err.data.errors);
        } else {
          this.setState({ errMsg: err.data.message });
        }
      });
  };
  removeError = () => {
    this.setState({ errMsg: "" });
  };
  render() {
    const resetPasswordvalidation = Yup.object().shape({
      password: Yup.string().trim().required(
        this.state.website_lang.display_error.login_register.password
      ),
      confirm_password: Yup.string().trim()
        .required(
          this.state.website_lang.display_error.login_register.confirm_password
        )
        .test(
          "passwords-match",
          this.state.website_lang.display_error.login_register.match_password,
          function (value) {
            return this.parent.password === value;
          }
        ),
    });
    return (
      <>
        {this.state.showLoader === true || this.state.website_lang === "" ? <Loader /> : null}
        <div className="outside bg-white">
          <div className="row">
            <div className="col-md-6">
              <div className="row h-100 justify-content-center align-items-center">
                <div className="login-form-sec">
                  <div className="login-form-box mx-auto text-center">
                    <div className="logo-login">
                      <a
                        href={process.env.REACT_APP_PORTAL_URL}
                        target="_blank"
                      >
                        <img
                          src={require("../../assets/images/logo-blue.jpg")}
                        />
                      </a>
                    </div>
                    <h2>Reset your password</h2>
                    <Formik
                      initialValues={initialValues}
                      validationSchema={resetPasswordvalidation}
                      onSubmit={this.handleSubmit}
                    >
                      {({ errors, touched }) => {
                        return (
                          <Form autoComplete="off">
                            <div
                              className="messageserror"
                              style={{
                                display:
                                  this.state.errMsg &&
                                  this.state.errMsg !== null &&
                                  this.state.errMsg !== ""
                                    ? "block"
                                    : "none",
                              }}
                            >
                              <Link to="#" className="close">
                                <img
                                  src={closeIcon}
                                  width="17.69px"
                                  height="22px"
                                  onClick={(e) => this.removeError()}
                                />
                              </Link>
                              <div>
                                <span className="errorMsg">
                                  {this.state.errMsg}
                                </span>
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-md-12">
                                {/* <input
                                type="text"
                                className="form-control customInput"
                                placeholder="Username or email address"
                              />
                               {errors.email_address && touched.email_address ? (
                                <span className="errorMsg">{errors.firstname}</span>
                              ) : null} */}
                                <Field
                                  name="password"
                                  type="password"
                                  className="form-control customInput"
                                  placeholder={
                                    this.state.website_lang.reset_pass
                                      .plh_password
                                  }
                                />
                                {errors.password && touched.password ? (
                                  <span className="errorMsg">
                                    {errors.password}
                                  </span>
                                ) : null}
                              </div>
                            </div>
                            <div className="row">
                              <div className="col-md-12">
                                {/* <input
                                type="text"
                                className="form-control customInput"
                                placeholder="Username or email address"
                              />
                               {errors.email_address && touched.email_address ? (
                                <span className="errorMsg">{errors.firstname}</span>
                              ) : null} */}
                                <Field
                                  name="confirm_password"
                                  type="password"
                                  className="form-control customInput"
                                  placeholder={
                                    this.state.website_lang.reset_pass
                                      .plh_conf_password
                                  }
                                />
                                {errors.confirm_password &&
                                touched.confirm_password ? (
                                  <span className="errorMsg">
                                    {errors.confirm_password}
                                  </span>
                                ) : null}
                              </div>
                            </div>

                            <div className="row">
                              <div className="col text-center">
                                <button
                                  className="btn btn-default btn-secondary btn-lg"
                                  type="submit"
                                >
                                  {this.state.website_lang.reset_pass.submit}
                                </button>
                              </div>
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-6">
              <div className="login-graphic-area">
                <div className="login-graphic-img">
                  <img
                    src={require("../../assets/images/login-graphic-img.jpg")}
                  />
                </div>
              </div>
            </div>
            <div className="clearfix" />
          </div>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.auth.token !== null ? true : false,
  };
};

export default connect(mapStateToProps)(Password);
