import React, { Component } from "react";
import { Link } from "react-router-dom";
// import Layout from "../Layout/layout";
import axios from "../../shared/axios";
// import InfiniteScroll from "react-infinite-scroller";
import Select from "react-select";
import Autocomplete from "react-autocomplete";
// import productList from "../../assets/File/API_Product_List.pdf";
// import portfolioProductList from "../../assets/File/Generics_Product_List.pdf";
import Loader from "../Loader/loader";
import closeIcon from "../../assets/images/close.svg";

import { BasicUserData } from "../../shared/helper";
import { connect } from "react-redux";

var product_url = process.env.REACT_APP_PORTAL_URL;

class ProductCatalog extends Component {
  state = {
    therapyCategory: [],
    dosageForm: [],
    developmentStage: [],
    apiTechnology: [],
    products: [],
    errMsg: null,
    showLoader: true,
    page: 0,
    hasMore: false,
    title: "",
    selectedCategory: [],
    selectedDosage: [],
    selectedApi: [],
    selectedStage: [],
    query: "",
    reset: false,
    value: "",
    autocompleteOptions: [],
    no_products: false,
    sel_product: 0,
    api_product_file: "",
    generic_product_file: "",
    create_request: false,
    isLoading: true,
    //selectedOption: null,
  };
  componentDidMount() {
    this.getTherapyCategory();
    this.getDosageForm();
    this.getDevelopmentStage();
    this.getApiTechnology();
    this.getProductList();
    this.getProductTitles();
    document.title = "Products | Dr. Reddy's API";
    let userData = BasicUserData();
    if (userData) {
      this.setState({
        isLoading: false,
      });
    }
  }
  fetchMoreData = () => {
    // a fake async api call like which sends
    // 20 more records in 1.5 secs
    // let query = this.state.query;
    let title = "";
    let development_status = [];
    let api_technology = [];
    let dose_form = [];
    let therapy_category = [];

    if (
      this.state.selectedCategory !== [] &&
      this.state.selectedCategory !== null
    ) {
      this.state.selectedCategory.map((category, key) =>
        // query = query + '&therapy_category[]=' + category.value
        therapy_category.push({ id: category.value })
      );
    }
    if (
      this.state.selectedDosage !== [] &&
      this.state.selectedDosage !== null
    ) {
      this.state.selectedDosage.map((dosage, key) =>
        // query = query + '&formulation_nfc[]=' + dosage.value
        dose_form.push({ id: dosage.value })
      );
    }
    if (this.state.selectedApi !== [] && this.state.selectedApi !== null) {
      this.state.selectedApi.map((api, key) =>
        // query = query + '&api_technology[]=' + api.value
        api_technology.push({ id: api.value })
      );
    }
    if (this.state.selectedStage !== [] && this.state.selectedStage !== null) {
      this.state.selectedStage.map((stage, key) =>
        // query = query + '&development_status[]=' + stage.value
        development_status.push({ id: stage.value })
      );
    }
    if (this.state.title !== "") {
      title = this.state.title;
    }
    //this.setState({no_products:true});
    //console.log('query', query);
    // axios.get(product_url + '/ext/api/get-products?_format=json' + query)
    let filter_data = {
      title: title,
      development_status: development_status,
      api_technology: api_technology,
      dose_form: dose_form,
      therapy_category: therapy_category,
    };
    setTimeout(() => {
      axios
        .post("/products/list_all", filter_data)
        .then((res) => {
          this.setState({
            products: this.state.products.concat(res.data.data),
          });
          this.setState({ page: this.state.page + 1 });
          this.setState({ no_products: false });
          this.setState({ showLoader: false });
          // if (res.data.length < 20) {
          //     this.setState({ hasMore: false })
          // }
        })
        .catch((err) => {});
    }, 1500);
  };

  getTherapyCategory() {
    // axios.get(product_url + 'ext/api/get-terms?_format=json&vid=therapy_area')
    axios
      .get("/products/category")
      .then((res) => {
        this.setState({ therapyCategory: res.data.data });
      })
      .catch((err) => {});
  }
  getDosageForm() {
    // axios.get(product_url + 'ext/api/get-terms?_format=json&vid=formulation_type')
    axios
      .get("/products/dosage")
      .then((res) => {
        this.setState({ dosageForm: res.data.data });
      })
      .catch((err) => {});
  }
  getDevelopmentStage() {
    // axios.get(product_url + 'ext/api/get-terms?_format=json&vid=development_stage')
    axios
      .get("/products/stage")
      .then((res) => {
        this.setState({ developmentStage: res.data.data });
      })
      .catch((err) => {});
  }
  getApiTechnology() {
    // axios.get(product_url + 'ext/api/get-terms?_format=json&vid=api_technology')
    axios
      .get("/products/technology")
      .then((res) => {
        this.setState({ apiTechnology: res.data.data });
      })
      .catch((err) => {});
  }
  getProductTitles() {
    // axios.get(product_url + 'ext/api/get-product-title?_format=json&vid=api_technology')
    axios
      .get("/products/all")
      .then((res) => {
        this.setState({ autocompleteOptions: res.data.data });
      })
      .catch((err) => {});
  }
  getProductList() {
    this.setState({ page: 0 });
    this.setState({ hasMore: false });
    this.setState({ query: "" });
    this.setState({ products: [] });
    this.setState({ selectedCategory: [] });
    this.setState({ selectedDosage: [] });
    this.setState({ selectedApi: [] });
    this.setState({ selectedStage: [] });
    this.setState({ title: "" });
    this.setState({ reset: false });
    this.setState({ showLoader: true });
    this.setState({ api_product_file: "" });
    this.setState({ generic_product_file: "" });
    //this.setState({no_products:true});
    axios
      .post("/products/list_all")
      .then((res) => {
        if (res.data.data.length > 0) {
          this.setState({ products: res.data.data });
          this.setState({ page: this.state.page + 1 });
          this.setState({ no_products: false });
          this.setState({ showLoader: false });
        } else {
          this.setState({ showLoader: false });
          this.setState({ no_products: true });
        }
        this.setState({ api_product_file: res.data.api_product_file });
        this.setState({ generic_product_file: res.data.generic_product_file });
        // if (res.data.length < 20) {
        //     this.setState({ hasMore: false })
        // }
      })
      .catch((err) => {
        this.setState({ showLoader: false });
        this.setState({ no_products: true });
        if (err.data.api_product_file !== "") {
          this.setState({ api_product_file: err.data.api_product_file });
        }
        if (err.data.generic_product_file !== "") {
          this.setState({
            generic_product_file: err.data.generic_product_file,
          });
        }
      });
  }
  handleTitleChange = (evt) => {
    // console.log("Title", evt.target.value);
    this.setState({ title: evt.target.value });
  };
  updateOptionList = (evt) => {
    // console.log("Title", evt.target.value);
    //this.setState({ title: evt.target.value })
  };
  handleCategoryChange = (selectedOption) => {
    this.setState({ selectedCategory: selectedOption });
  };
  handleDosageChange = (selectedOption) => {
    this.setState({ selectedDosage: selectedOption });
  };
  handleDevelopmentChange = (selectedOption) => {
    this.setState({ selectedStage: selectedOption });
  };
  handleApiChange = (selectedOption) => {
    this.setState({ selectedApi: selectedOption });
  };

  handleSubmit() {
    this.setState({ products: [] });
    this.setState({ hasMore: false });
    this.setState({ showLoader: true });
    this.setState({ page: 0 });
    this.setState({ query: "" });
    this.setState({ reset: true });
    let query = "";
    let title = "";
    let development_status = [];
    let api_technology = [];
    let dose_form = [];
    let therapy_category = [];

    if (
      this.state.selectedCategory !== [] &&
      this.state.selectedCategory !== null
    ) {
      this.state.selectedCategory.map((category, key) =>
        // query = query + '&therapy_category[]=' + category.value
        therapy_category.push({ id: category.value })
      );
    }
    if (
      this.state.selectedDosage !== [] &&
      this.state.selectedDosage !== null
    ) {
      this.state.selectedDosage.map((dosage, key) =>
        // query = query + '&formulation_nfc[]=' + dosage.value
        dose_form.push({ id: dosage.value })
      );
    }
    if (this.state.selectedApi !== [] && this.state.selectedApi !== null) {
      this.state.selectedApi.map((api, key) =>
        // query = query + '&api_technology[]=' + api.value
        api_technology.push({ id: api.value })
      );
    }
    if (this.state.selectedStage !== [] && this.state.selectedStage !== null) {
      this.state.selectedStage.map((stage, key) =>
        // query = query + '&development_status[]=' + stage.value
        development_status.push({ id: stage.value })
      );
    }
    if (this.state.title !== "") {
      title = this.state.title;
    }
    // this.setState({ query: query })
    // console.log("QUERY", query);
    //this.setState({no_products:true});
    //console.log('query', query);
    // axios.get(product_url + '/ext/api/get-products?_format=json' + query)
    let filter_data = {
      title: title,
      development_status: development_status,
      api_technology: api_technology,
      dose_form: dose_form,
      therapy_category: therapy_category,
    };
    axios
      .post("/products/list_all", filter_data)
      .then((res) => {
        if (res.data.data.length > 0) {
          this.setState({ products: res.data.data });
          this.setState({ page: this.state.page + 1 });
          this.setState({ no_products: false });
          this.setState({ showLoader: false });
          if (res.data.length < 20) {
            this.setState({ hasMore: false });
          }
        } else {
          this.setState({ showLoader: false });
          this.setState({ no_products: true });
        }
      })
      .catch((err) => {
        this.setState({ showLoader: false });
        this.setState({ no_products: true });
      });
  }

  showPopup = (e, product_id, product_name) => {
    e.preventDefault();
    this.setState({ sel_product: product_id, sel_product_name: product_name });
  };

  render() {
    var style = {
      //marginTop: '20px',
      display: "flex",
      justifyContent: "flex-start",
      flexWrap: "wrap",
      width: "100%",
    };
    if (this.state.isLoading === true) {
      return (
        <>
          <div className="loginLoader">
            <div className="lds-spinner">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <span>Please Wait…</span>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <div className="container-fluid clearfix productSec">
          <div className="dashboard-content-sec">
            <div>
              <div
                id="block-reddy-admin-content"
                className="block block-system block-system-main-block"
              >
                <div className="content">
                  <div className="row">
                    <div className="col-md-12">
                      <div className="float-right text-right">
                        <a
                          href={this.state.api_product_file}
                          download
                          className="download-product"
                          target="_blank"
                        >
                          {
                            this.props.dynamic_lang.product_catalogue
                              .api_product_list
                          }
                        </a>
                        <a
                          href={this.state.generic_product_file}
                          className="download-product"
                          download
                          target="_blank"
                        >
                          {
                            this.props.dynamic_lang.product_catalogue
                              .generics_product_list
                          }
                        </a>
                      </div>
                      <div
                        style={{
                          display: "flex",
                          flexDirection: "row",
                          flexWrap: "nowrap",
                          justifyContent: "space-between",
                        }}
                      ></div>
                      <div className="product-search-section">
                        <h2>
                          {this.props.dynamic_lang.product_catalogue.header}
                        </h2>
                        {/* <input placeholder="Search by Product Name..." className="customInput" onChange={this.handleTitleChange} value={this.state.title}/> */}
                        <div className="search-panel">
                          <Autocomplete
                            inputProps={{
                              placeholder: this.props.dynamic_lang
                                .product_catalogue.search_by_product,
                            }}
                            items={this.state.autocompleteOptions}
                            shouldItemRender={(item, title) =>
                              item.label
                                .toLowerCase()
                                .indexOf(title.toLowerCase()) > -1
                            }
                            getItemValue={(item) => item.label}
                            renderItem={(item, highlighted) => (
                              <div
                                key={item.id}
                                style={{
                                  backgroundColor: highlighted
                                    ? "#eee"
                                    : "transparent",
                                }}
                              >
                                {item.label}
                              </div>
                            )}
                            value={this.state.title}
                            onChange={(e) =>
                              this.setState({ title: e.target.value })
                            }
                            onSelect={(title) => this.setState({ title })}
                            //onKeyUp={this.updateOptionList}
                          />
                        </div>
                        <div className="clearfix"></div>
                        <div className="row">
                          <div className="col-md-10 searchCategories">
                            <div className="row">
                              <div className="col-md-3">
                                {/* <select className="form-control customeSelect" >
                                                                    <option></option>
                                                                </select> */}
                                <Select
                                  isMulti
                                  value={this.state.selectedCategory}
                                  placeholder={
                                    this.props.dynamic_lang.product_catalogue
                                      .therapeutic_placeholder
                                  }
                                  onChange={this.handleCategoryChange}
                                  options={this.state.therapyCategory}
                                />
                              </div>
                              <div className="col-md-3">
                                <Select
                                  isMulti
                                  value={this.state.selectedDosage}
                                  placeholder={
                                    this.props.dynamic_lang.product_catalogue
                                      .dosage_placeholder
                                  }
                                  onChange={this.handleDosageChange}
                                  options={this.state.dosageForm}
                                />
                              </div>
                              <div className="col-md-3">
                                <Select
                                  isMulti
                                  value={this.state.selectedStage}
                                  placeholder={
                                    this.props.dynamic_lang.product_catalogue
                                      .development_placeholder
                                  }
                                  onChange={this.handleDevelopmentChange}
                                  options={this.state.developmentStage}
                                />
                              </div>
                              <div className="col-md-3">
                                <Select
                                  isMulti
                                  value={this.state.selectedApi}
                                  placeholder={
                                    this.props.dynamic_lang.product_catalogue
                                      .api_technology_placeholder
                                  }
                                  onChange={this.handleApiChange}
                                  options={this.state.apiTechnology}
                                />
                              </div>
                              <div className="clearfix"></div>
                            </div>
                          </div>
                          <div className="col-md-2 pl-0">
                            <input
                              type="submit"
                              value="Search"
                              className="product-search-btn"
                              onClick={() => this.handleSubmit()}
                            />
                            {this.state.reset ? (
                              <input
                                type="submit"
                                value={
                                  this.props.dynamic_lang.product_catalogue
                                    .reset
                                }
                                className="product-reset-btn"
                                onClick={() => this.getProductList()}
                              />
                            ) : (
                              ""
                            )}
                          </div>
                          <div className="clearfix"></div>
                        </div>
                      </div>
                    </div>
                  </div>

                  {
                    <div style={style}>
                      {this.state.showLoader ? (
                        <Loader />
                      ) : this.state.no_products ? (
                        <>
                          <div className="col-md-12 mb-20">
                            <span>
                              {
                                this.props.dynamic_lang.product_catalogue
                                  .dont_see_product
                              }{" "}
                              <Link
                                to={{ pathname: `/contact-us` }}
                                style={{
                                  cursor: "pointer",
                                  color: "#5225B5",
                                  textDecoration: "underline",
                                }}
                              >
                                {
                                  this.props.dynamic_lang.product_catalogue
                                    .contact_us
                                }
                              </Link>{" "}
                              {
                                this.props.dynamic_lang.product_catalogue
                                  .product_development_opportunities
                              }
                            </span>
                          </div>
                          <div className="col-md-12">
                            <center>
                              <b>
                                {
                                  this.props.dynamic_lang.product_catalogue
                                    .no_product_found
                                }
                              </b>
                            </center>
                          </div>
                        </>
                      ) : (
                        this.state.products &&
                        this.state.products.map((product, productKey) => (
                          <div
                            key={productKey}
                            className="col-lg-3 col-md-6 col-sm-12 boxWrap"
                          >
                            <div className="products-box">
                              
                                <h3>{product.title}</h3>
                                <p>
                                  {
                                    this.props.dynamic_lang.product_catalogue
                                      .therapy_area
                                  }{" "}
                                  <span>{product.therapy_area}</span>
                                </p>
                                
                              

                              <div className="flex-sm-column flex-md-row d-flex justify-content-between know-more-btn">
                                  <div>
                                  <Link to={"/product-details/" + product.id}>
                                    {
                                      this.props.dynamic_lang.product_catalogue
                                        .know_more
                                    }
                                  </Link>
                                  </div>

                                  <div>
                                  <Link
                                    to={`#`}
                                    onClick={(e) =>
                                      this.showPopup(
                                        e,
                                        product.id,
                                        product.title
                                      )
                                    }
                                    className="createRequestBtn"
                                  >
                                    {
                                      this.props.dynamic_lang.product_catalogue
                                        .create_request
                                    }
                                  </Link>
                                  </div>
                                </div>
                             

                              {this.state.sel_product === product.id && (
                                <div className="createLink">
                                  <Link
                                    to="#"
                                    onClick={() => {
                                      this.setState({
                                        sel_product: 0,
                                        sel_product_name: "",
                                      });
                                    }}
                                  >
                                    <img
                                      src={closeIcon}
                                      width="14"
                                      height="14"
                                      style={{
                                        position: "absolute",
                                        right: 10,
                                      }}
                                    />
                                  </Link>
                                  <ul>
                                    <li>
                                      <Link
                                        to={{
                                          pathname: "/sample",
                                          state: {
                                            pid: this.state.sel_product,
                                            pname: this.state.sel_product_name,
                                          },
                                        }}
                                      >
                                        {
                                          this.props.dynamic_lang
                                            .product_catalogue.sample
                                        }
                                      </Link>
                                    </li>
                                    <li>
                                      <Link
                                        to={{
                                          pathname: "/typical-coa",
                                          state: {
                                            pid: this.state.sel_product,
                                            pname: this.state.sel_product_name,
                                          },
                                        }}
                                      >
                                        {
                                          this.props.dynamic_lang
                                            .product_catalogue.typical_coa
                                        }
                                      </Link>
                                    </li>

                                    <li>
                                      <Link
                                        to={{
                                          pathname: "/request_price_quote",
                                          state: {
                                            pid: this.state.sel_product,
                                            pname: this.state.sel_product_name,
                                          },
                                        }}
                                      >
                                        {
                                          this.props.dynamic_lang
                                            .product_catalogue.price_quote
                                        }
                                      </Link>
                                    </li>

                                    <li>
                                      <Link
                                        to={{
                                          pathname: "/general-request",
                                          state: {
                                            pid: this.state.sel_product,
                                            pname: this.state.sel_product_name,
                                          },
                                        }}
                                      >
                                        {
                                          this.props.dynamic_lang
                                            .product_catalogue.general_request
                                        }
                                      </Link>
                                    </li>

                                    <li>
                                      <Link
                                        to={{
                                          pathname: "/contact-us",
                                          state: {
                                            pid: this.state.sel_product,
                                            pname: this.state.sel_product_name,
                                          },
                                        }}
                                      >
                                        {
                                          this.props.dynamic_lang
                                            .product_catalogue.contact_us
                                        }
                                      </Link>
                                    </li>
                                  </ul>
                                </div>
                              )}
                            </div>
                          </div>
                        ))
                      )}
                    </div>
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductCatalog);
//export default ProductCatalog;
