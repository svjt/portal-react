import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import { Redirect } from "react-router";
// SATYAJIT
import { connect } from "react-redux";
import Tour from 'reactour'
import { htmlDecode, BasicUserData, getUserLanguage } from "../../shared/helper";
import infoIcon from "../../assets/images/info-circle-solid.svg";
import { authLogout, ChangeToEnglish, setLanguage, updateToken, authLogoutReload,feature11PopupSession } from "../../store/actions/auth";
import {
  fetchUserNotification,
  updateNotification,
  setUserCompany,
  fetchUserData,
  showPreActivationModal,
  hidePreActivationModal,
} from "../../store/actions/user";
import { Tooltip } from "reactstrap";
import { Modal } from "react-bootstrap";
import axios from "../../shared/axios";
import Select from "react-select";
import { showErrorMessage } from "../../shared/handle_error";

import whitelogo from "../../assets/images/info-circle-solid.svg";
import Button from 'react-bootstrap/Button'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
import Popover from 'react-bootstrap/Popover';
import PopoverContent from 'react-bootstrap/PopoverContent';
import PopoverTitle from 'react-bootstrap/PopoverTitle';
import { refreshToken, refreshFeatureToken } from "../../store/actions/auth";
import { isMobile } from "react-device-detect";


// SATYAJIT

class Header extends Component {
  constructor(props) {
    super(props);
    this.node = React.createRef();
    this.not_node = React.createRef();

    this.state = {
      user_image: "",
      username: "",
      showHelp: false,
      showNotification: false,
      ncounter: 0,
      since: null,
      nlist: [],
      companyList: [],
      selectedCompany: null,
      isLoading: true,
      tooltipOpen: false,
      explitLoader: false,
      open_tour: true,
      showStep:1,
      showFeature11Popup:true
    };
  }

  componentDidMount = () => {
    let userData = BasicUserData();
    if (userData) {
      this.setState({
        showFeature11Popup:true,
        showStep:1,
        user_image: this.props.profile_pic,
        username: this.props.name,
        ncounter: this.props.ncounter,
        since: userData ? userData.since : null,
      });
    }
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.profile_pic !== prevProps.profile_pic ||
      this.props.ncounter !== prevProps.ncounter
    ) {
      this.setState({
        showFeature11Popup:true,
        showStep:1,
        user_image: this.props.profile_pic,
        username: this.props.name,
        ncounter: this.props.ncounter,
      });

    }
  }

  handleUserMenu = () => {
    var element = document.getElementById("userLi");
    element.classList.toggle("show-dropdown-menu");

    this.setState({
      showNotification: false,
    });
  };

  hideUserMenu = () => {
    var element = document.getElementById("userLi");
    element.classList.remove("show-dropdown-menu");
  };

  handleLogout = () => {
    this.props.disconnectUser();
  };

  handleHelpClose = () => {
    this.setState({ showHelp: false });
  };

  displayHelpTour = () => {
    this.setState({ showHelp: true });
  };

  languageSwitch = () => {

    return <>
      <Link to="#" onClick={(e) => {
        e.preventDefault();
        this.setState({ explitLoader: true });
        this.props.ChangeToEnglishExplicit('en', (data) => {
          if (data === 1) {
            setTimeout(function () { window.location.reload(); }, 4000);
          }
        });
      }}>Switch to English language</Link>{` `}
      <img
        src={infoIcon}
        width="15.44"
        height="18"
        id={`DisabledAutoHideExampleRating_en`}
        type="button"
        className={`itooltip`}
      />
      <Tooltip
        placement="right"
        isOpen={this.state.tooltipOpen}
        autohide={false}
        target={`DisabledAutoHideExampleRating_en`}
        toggle={() => {
          this.setState({
            tooltipOpen: !this.state.tooltipOpen
          });
        }}
      >
        Please note by clicking the entire portal will be translated to English language
        </Tooltip>
    </>;
  }

  showExplicitLoader = () => {
    return (
      <>
        <div className="loginLoader">
          <div className="lds-spinner">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <span>Please Wait…</span>
          </div>
        </div>
      </>
    );
  }

  showHideNotification = () => {
    var element = document.getElementById("userLi");
    element.classList.remove("show-dropdown-menu");

    if (this.state.showNotification === true) {
      this.setState({
        showNotification: false,
      });
    } else {
      let userData = BasicUserData();
      if (userData.role === 2) {
        userData.company_id = this.props && this.props.selCompany.value;
      }
      this.props.fetchNotification(userData, () => {
        this.setState({
          nlist: this.props.nlist,
          showNotification: true,
        });
      });
    }
  };

  changeDateFormat = (input_date) => {
    if (input_date) {
      var dateFormat = require("dateformat");
      //return dateFormat(input_date.replace("-", "/"), "d mmm yyyy")

      var date_time = input_date.split(" ");
      var date = date_time[0].split("-");
      var date_format = new Date(date[0], date[1] - 1, date[2]);
      return dateFormat(date_format, "d mmm yyyy");
    }
  };

  readUserNotification = (notificationId, taskId) => {
    let userData = BasicUserData();
    let requestData = {
      notification_id: notificationId,
      task_id: taskId,
      role: userData.role,
      company_id: this.props.selCompany ? this.props.selCompany.value : 0,
    };

    this.props.readNotification(requestData);

    this.setState({
      showNotification: !this.state.showNotification,
    });
  };

  componentWillMount() {
    const { isLoggedIn, pre_activated, has_disabled_pre_activation_modal } = this.props;
    if (isLoggedIn) {
      document.addEventListener("mousedown", this.handleClick, false);
    }
    if(pre_activated === 1 && !has_disabled_pre_activation_modal){
      this.props.showPreActivationModal();      
    }
  }

  componentWillUnmount() {
    const { isLoggedIn } = this.props;
    if (isLoggedIn) {
      document.removeEventListener("mousedown", this.handleClick, false);
    }
  }

  handleClick = (e) => {
    if (
      this.node && this.node.contains &&
      (this.node.contains(e.target) || this.not_node.contains(e.target))
    ) {
      return;
    }

    var element = document.getElementById("userLi");
    if (element) {
      element.classList.remove("show-dropdown-menu");
    }

    if (this.state.showNotification) {
      var notificationEle = document.getElementById("notificationBlock");
      notificationEle.style.display = "none";
      this.setState({ showNotification: false });
    }
  };

  changeCompany = (event) => {
    this.props.setCompany(event, () => {
      this.setState({ selectedCompany: event });
    });
  };

  disableNewFeature = (event) => {
    event.preventDefault();
    axios
      .post("/feature-done", {})
      .then((res) => {
        this.props.refFeatureToken();
      })
      .catch((err) => { });
  }

  disableSendMailNewFeature = (event) => {
    event.preventDefault();
    axios
      .post("/feature-send-mail-done", {})
      .then((res) => {
        this.props.refFeatureToken();
      })
      .catch((err) => { });
  }

  disable11NewFeature = (event) => {
    event.preventDefault();
    axios
      .post("/feature-11-done", {})
      .then((res) => {
        this.props.refFeatureToken();
      })
      .catch((err) => { });
  }

  closeTour = () => {
    this.setState({ open_tour: false });
  };

  getTourSteps = () => {
    let ret = [];
    let next_step = false;
    if(this.props.new_feature_send_mail === true){
      next_step = true;
    }

    if(this.props.new_feature === true && this.props.multi_lang_access === true){
      ret.push({
        selector: '#first-step-header',
        content: ({ goTo, inDOM }) => (
          <div>
            {getUserLanguage() === 'en' ? 'XCEED is now multilingual, click to select language of your choice.' : 'Selecting this will turn your default language to English.'}
            <br />
            <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => {
              this.disableNewFeature(e);
            }} >{/* OK Got It */}{this.props.dynamic_lang.task_details.ok_got}</button>

          </div>
        )
      });
    }

    if(this.props.new_feature_send_mail === true){
      ret.push({
        selector: '#second-step-header',
        content: ({ goTo, inDOM }) => (
          <div>
            {/* Escalate to DRL team on the task issue. */}{this.props.dynamic_lang.header.send_an_email}
            <br />
            <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => this.disableSendMailNewFeature(e)}>{/* OK Got It */}{this.props.dynamic_lang.task_details.ok_got}</button>

          </div>
        )
      });
    }

    return ret;
  }

  showTourSteps = (steps,lang,is_mobile) => {


    if(steps === 1){
      return (
        <div >
          <h4>{this.props.dynamic_lang.header.new_feature_released}</h4>
          <p>{this.props.dynamic_lang.header.new_feature_step_text1}</p>
          <div className="popupPagination">
            <ul>
              <li className="active"><span></span></li>
              <li><span></span></li>
              <li><span></span></li>
              <li><span></span></li>
              <li><span></span></li>
            </ul>
          </div>
          <button className="btn btn-default btn-primary mt-0" onClick={()=>this.setState({showStep:2})} >{this.props.dynamic_lang.header.next}</button>
        </div>
      )
    }else if(steps === 2){
      return (
          <div>
            <p>{this.props.dynamic_lang.header.new_feature_step_text2}</p>
            <p>
              <img src={require(`../../assets/images/feature_11/${lang}${is_mobile?'-mobile':''}-step-1.png`)} />
            </p>
            <div className="popupPagination">
            <ul>
              <li><span></span></li>
              <li className="active"><span></span></li>
              <li><span></span></li>
              <li><span></span></li>
              <li><span></span></li>
            </ul>
          </div>
            <button className="btn btn-default btn-cancel mt-0" onClick={()=>this.setState({showStep:1})} >{this.props.dynamic_lang.header.previous}</button>
            <button className="btn btn-default btn-primary mt-0" onClick={()=>this.setState({showStep:3})} >{this.props.dynamic_lang.header.next}</button>
          </div>
      );
    }else if(steps === 3){
      return (
        <div>
          <p>{this.props.dynamic_lang.header.new_feature_step_text3}</p>
          <p>
            <img src={require(`../../assets/images/feature_11/${lang}${is_mobile?'-mobile':''}-step-2.png`)} />
          </p>
          <div className="popupPagination">
            <ul>
              <li><span></span></li>
              <li><span></span></li>
              <li className="active"><span></span></li>
              <li><span></span></li>
              <li><span></span></li>
            </ul>
          </div>
          <button className="btn btn-default btn-cancel mt-0" onClick={()=>this.setState({showStep:2})} >{this.props.dynamic_lang.header.previous}</button>
          <button className="btn btn-default btn-primary mt-0" onClick={()=>this.setState({showStep:4})} >{this.props.dynamic_lang.header.next}</button>
        </div>
      )
    }else if(steps === 4){
      return (
        <div>
          <p>{this.props.dynamic_lang.header.new_feature_step_text4}</p>
          <p>
            <img src={require(`../../assets/images/feature_11/${lang}${is_mobile?'-mobile':''}-step-3.png`)} />
          </p>
          <div className="popupPagination">
            <ul>
              <li><span></span></li>
              <li><span></span></li>
              <li><span></span></li>
              <li className="active"><span></span></li>
              <li><span></span></li>
            </ul>
          </div>
          <button className="btn btn-default btn-cancel mt-0" onClick={()=>this.setState({showStep:3})} >{this.props.dynamic_lang.header.previous}</button>
          <button className="btn btn-default btn-primary mt-0" onClick={()=>this.setState({showStep:5})} >{this.props.dynamic_lang.header.next}</button>
        </div>
      )
    }else if(steps === 5){
      return (
        <div>
          <p>{this.props.dynamic_lang.header.new_feature_step_text5}</p>
          <p>
            <img src={require(`../../assets/images/feature_11/${lang}${is_mobile?'-mobile':''}-step-4.png`)} />
          </p>
          <div className="popupPagination">
            <ul>
              <li><span></span></li>
              <li><span></span></li>
              <li><span></span></li>
              <li><span></span></li>
              <li className="active"><span></span></li>
            </ul>
          </div>
          <button className="btn btn-default btn-cancel mt-0" onClick={()=>this.setState({showStep:4})}>{this.props.dynamic_lang.header.previous}</button>
          <button className="btn btn-default btn-primary mt-0" onClick={(e) => this.disable11NewFeature(e)}  >{this.props.dynamic_lang.header.finish}</button>
        </div>
      )
    }

  }

  closeFeature11Tour = () => {
    this.setState({showFeature11Popup:false});
    this.props.closePopupForSession();
  }

  render() {
    const { isLoggedIn, ncounter, selCompany } = this.props;
    const {
      user_image,
      username,
      showNotification,
      nlist,
      companyList,
      selectedCompany,
      since,
    } = this.state;

    if (!isLoggedIn) return null;

    // Update this line with new variable which gets added to lang file to reset redux
    {typeof this.props.dynamic_lang.header.warning == 'undefined' && this.props.disconnectUserReload('', () => {
      window.location.reload();
    })}

    const loggedUser = BasicUserData();
    let role;
    if (loggedUser) {
      role = loggedUser.role;
    }

    const tour_steps = this.getTourSteps();
    let show = true;
    if(window.location.pathname === "/qa_login" || window.location.pathname === "/customer_portal/qa_login" || window.location.pathname === "/new_customer_portal/qa_login" || window.location.pathname === "/qa_form" || window.location.pathname === "/customer_portal/qa_form" || window.location.pathname === "/new_customer_portal/qa_form"){
        show = false
    }
    

    if(show){
      return (

        
      
        <div className={`customHeader ${isLoggedIn && getUserLanguage() === 'en' ? 'english-new' : ''}`}>

        
  
          {isMobile && ((this.props.new_feature === true && this.props.multi_lang_access === true) || this.props.new_feature_send_mail === true) && <Tour
                steps={tour_steps}
                isOpen={this.state.open_tour}
                showNavigation={false}
                showButtons={false}
                onRequestClose={this.closeTour}
                onBeforeClose={target => (document.body.style.overflowY = 'auto')}
                onAfterOpen={target => (document.body.style.overflowY = 'auto')}
          />}
  
          {this.state.explitLoader && this.showExplicitLoader()}
  
          {this.props.company && this.props.company.length > 0 && (
            <div className="my-company-drpdwn">
              <Select
                defaultValue={this.props.selCompany}
                options={this.props.company}
                isSearchable={true}
                isClearable={true}
                placeholder={this.props.dynamic_lang && this.props.dynamic_lang.header.select_customer}
                className="company_id"
                onChange={(e) => this.changeCompany(e)}
              />
            </div>
          )}
  
          <div className="p-0 container-fluid">
            <nav className="align-items-stretch flex-md-nowrap p-0 navbar navbar-light">
              <div className="col-1" />
              <div className="col-11 user-sec text-right  my-auto">
                <ul className="flex-row navbar-nav">
                  <li className="help">
  
  
  
                    {this.props.multi_lang_access && (getUserLanguage() === 'en' ? <Link to="/my-account" id="first-step-header" >Change language</Link> : this.languageSwitch())}
  
                    {this.props.new_feature && this.props.multi_lang_access && <Popover id="popover-basic" placement="left" className="popover-customLeft">
                      <Popover.Title as="h3"><img src={infoIcon} width="15" height="15" style={{ backgroundColor: 'white' }} />{` `}{this.props.dynamic_lang.header.new_feature}<button type="button" id="close" onClick={(e) => this.disableNewFeature(e)} class="close">&times;</button></Popover.Title>
                      <Popover.Content>
                        {getUserLanguage() === 'en' ? 'XCEED is now multilingual, click to select language of  your choice.' : 'Selecting this will turn your default language to English.'}
                      </Popover.Content>
                    </Popover>}
  
                  </li>
                  <li className="help">
                    <Link to="#" onClick={() => this.displayHelpTour()}>
                      {this.props.dynamic_lang && this.props.dynamic_lang.header.title}
                    </Link>
                    <span>{this.props.dynamic_lang && this.props.dynamic_lang.header.tt_content}</span>
  
                  </li>
                  <li
                    className="dropdown notifications nav-item"
                    ref={(not_node) => (this.not_node = not_node)}
                  >
                    {/* {console.log("nlis========>", nlist)}
                    {console.log("nlis========>", nlist.length)}
                    {console.log("ncounter=====>", ncounter)}
                    {console.log("selCompany=======>", selCompany)} */}
  
                    {nlist.length === 0 &&
                      (ncounter === 0 || ncounter === "0") ? (
                        <Link to="#" className="nav-link-icon text-center nav-link">
                          <span>
                            {this.props.dynamic_lang && this.props.dynamic_lang.header.no_notifications}{" "}
                          </span>
                        </Link>
                      ) : (
                        ""
                      )}
  
                    {role === 1 ? (
                      nlist.length > 0 || ncounter > 0 ? (
                        <Link
                          to="#"
                          id="second-step-header"
                          className="nav-link-icon text-center nav-link"
                          onClick={(e) => this.showHideNotification()}
                        >
                          <div className="nav-link-icon-wrapper">
                            <img
                              className=""
                              src={require("../../assets/images/notification-icon.png")}
                            />
                            {ncounter > 0 && (
                              <span className="badge badge-danger badge-pill">
                                {ncounter}
                              </span>
                            )}
                          </div>
                        </Link>
                      ) : (
                          ""
                        )
                    ) : (
                        ""
                      )}
  
                    {role === 2 &&
                      this.props.selCompany &&
                      this.props.selCompany.value > 0 ? (
                        nlist.length > 0 || ncounter > 0 ? (
                          <Link
                            to="#"
                            id="first-step-header"
                            className="nav-link-icon text-center nav-link"
                            onClick={(e) => this.showHideNotification()}
                          >
                            <div className="nav-link-icon-wrapper">
                              <img
                                className=""
                                src={require("../../assets/images/notification-icon.png")}
                              />
                              {ncounter > 0 && (
                                <span className="badge badge-danger badge-pill">
                                  {ncounter}
                                </span>
                              )}
                            </div>
                          </Link>
                        ) : (
                            ""
                          )
                      ) : (
                        ""
                      )}
  
                    {/* {
                        (( nlist === null) && (ncounter === 0 || ncounter === "0"))
  
                          ?
  
                          <Link to="#" className="nav-link-icon text-center nav-link">
                            <span>No Notifications </span>
                          </Link>
  
                          :
  
                          <Link
                            to="#"
                            className="nav-link-icon text-center nav-link"
                            onClick={e => this.showHideNotification()}
                          >
                            <div className="nav-link-icon-wrapper">
                              <img
                                className=""
                                src={require("../../assets/images/notification-icon.png")}
                              />
                              {ncounter > 0 && <span className="badge badge-danger badge-pill">{ncounter}</span>}
                            </div>
                          </Link>
                      } */}
  
                    <div
                      className="notificationContainer"
                      id="notificationBlock"
                      
                      style={{
                        display: showNotification === true ? "block" : "none",
                      }}
                    >
                      <div className="notificationTitle">
                        {this.props.dynamic_lang && this.props.dynamic_lang.header.notifications}
                      </div>
  
                      <div className="notificationWrapper">
                        <div className="views-element-container">
                          <ol className="notification-list">
                            {nlist &&
                              nlist.map((n, index) => (
                                <li
                                  key={index}
                                  className={
                                    n.read_status === 0 ? "new-notification" : ""
                                  }
                                >
                                  <Link
                                    to={{
                                      pathname: `/task-details/${n.task_id}`,
                                      state: {
                                        fromDashboard:
                                          n.n_type === 4 ? true : false,
                                      },
                                    }}
                                    onClick={() =>
                                      this.readUserNotification(n.n_id, n.task_id)
                                    }
                                    action="replace"
                                  >
                                    {n.n_type === 1 && (
                                      <p>
                                        {this.props.dynamic_lang && this.props.dynamic_lang.notification.customer.updated.replace(
                                          "[task_ref]",
                                          n.task_ref
                                        )}
                                        {/* Status for {n.task_ref} has been updated. */}
                                      </p>
                                    )}
  
                                    {n.n_type === 2 && (
                                      <p>
                                        {this.props.dynamic_lang && this.props.dynamic_lang.notification.customer.closed.replace(
                                          "[task_ref]",
                                          n.task_ref
                                        )}
                                        {/* Request {n.task_ref} has been closed. */}
                                      </p>
                                    )}
  
                                    {n.n_type === 3 && (
                                      <p>
                                        {this.props.dynamic_lang && this.props.dynamic_lang.notification.customer.input.replace(
                                          "[task_ref]",
                                          n.task_ref
                                        )}
                                        {/* Request {n.task_ref} needs your input. */}
                                      </p>
                                    )}
  
                                    {n.n_type === 4 && (
                                      <p>
                                        {this.props.dynamic_lang && this.props.dynamic_lang.notification.customer.response.replace(
                                          "[task_ref]",
                                          n.task_ref
                                        )}
                                        {/* You have a new response on {n.task_ref}{" "}
                                        from your Dr. Reddys team. */}
                                      </p>
                                    )}
  
                                    {n.n_type === 5 && (
                                      <p>
                                        {this.props.dynamic_lang && this.props.dynamic_lang.notification.customer.closure.replace(
                                          "[task_ref]",
                                          n.task_ref
                                        )}
                                        {/* Expected Closure date for {n.task_ref} has
                                        been updated. */}
                                      </p>
                                    )}
                                  </Link>
  
                                  <span className="ndate">
                                    {this.changeDateFormat(n.add_date)}
                                  </span>
                                </li>
                              ))}
  
                            {/* <tr key={key} className={key === 1 ? "row-circle" : ""}></tr> */}
                          </ol>
                        </div>
                      </div>
                    </div>
  
                    {(typeof this.props.new_feature_send_mail == 'undefined') ? this.props.disconnectUserReload('', () => {
                      window.location.reload();
                    }) :
                      (this.props.new_feature_send_mail == true) ?
                        <Popover placement="bottom" className="popover-custom" >
                          <Popover.Title as="h3"><img src={infoIcon} width="15" height="15" style={{ backgroundColor: 'white' }} />{` `}{/* New Feature */}{this.props.dynamic_lang.header.new_feature}<button type="button" id="close" onClick={(e) => this.disableSendMailNewFeature(e)} class="close">&times;</button></Popover.Title>
                          <Popover.Content>
                            {/* Now you can send an email to any user and can escalate to Dr.Reddys team on a task from task details page. */}
                            {this.props.dynamic_lang.header.send_an_email}
                          </Popover.Content>
                        </Popover>
                        : null
                    }
                  </li>
                  <li
                    className="nav-item dropdown user-sec2"
                    ref={(node) => (this.node = node)}
                  >
                    <Link
                      to={{ pathname: "" }}
                      aria-haspopup="true"
                      className="text-nowrap px-3 dropdown-toggle nav-link"
                      onClick={() => this.handleUserMenu()}
                    >
                      <img
                        className="user-avatar rounded-circle mr-2"
                        src={user_image}
                      />
                      <span className="d-none d-md-inline-block">{username}</span>
                    </Link>
  
                    <ul className="dropdown-menu" id="userLi">
                      <li className="user-header">
                        <img
                          src={user_image}
                          width="38"
                          height="38"
                          className="img-circle"
                          alt="User Image"
                        />
                        <p>
                          {username}
                          <small>
                            {this.props.dynamic_lang && this.props.dynamic_lang.header.since}
                            {since}
                          </small>
                        </p>
                      </li>
                      <li className="user-footer">
                        <div className="pull-left edit-account">
                          <Link
                            to="/my-account"
                            onClick={() => this.hideUserMenu()}
                          >
                            {this.props.dynamic_lang && this.props.dynamic_lang.header.account}
                          </Link>
                        </div>
                        <div className="pull-right logout-account">
                          <Link to="" className="" onClick={this.handleLogout}>
                            {this.props.dynamic_lang && this.props.dynamic_lang.header.logout}
                          </Link>
                        </div>
                      </li>
                    </ul>
                  </li>
                </ul>
              </div>
            </nav>
          </div>

          {(typeof this.props.new_feature_11 == 'undefined') ? this.props.disconnectUserReload('', () => {
            window.location.reload();
          }) :
            (this.props.new_feature_11 == true && this.props.tour_counter_11 == true) ?
              // <Popover placement="bottom" className="popover-custom" >
              //   <Popover.Title as="h3"><img src={infoIcon} width="15" height="15" style={{ backgroundColor: 'white' }} />{` `}{/* New Feature */}{this.props.dynamic_lang.header.new_feature}<button type="button" id="close"  class="close">&times;</button></Popover.Title>
              //   <Popover.Content>
              //     {this.props.dynamic_lang.header.ship_info}
              //   </Popover.Content>
              // </Popover>

              <div style={{ display: `${this.state.showFeature11Popup?'block':'none'}`}} className="customPopup">
                {this.state.showFeature11Popup && <Modal.Dialog
                  size="lg"
                  aria-labelledby="contained-modal-title-vcenter"
                  centered
                >
                  <Modal.Header>
                    <button className="close" onClick={()=>this.closeFeature11Tour()} />
                  </Modal.Header>
                  <Modal.Body>
                      {this.showTourSteps(this.state.showStep,getUserLanguage(),isMobile)}
                  </Modal.Body>
                </Modal.Dialog>}
              </div>

              : null
          }
  
          {/* Help Popup Start -SATYAJIT */}
          <Modal
            show={this.state.showHelp}
            onHide={this.handleHelpClose}
            backdrop="static"
            className="help"
          >
            <Modal.Header>
              <button className="close" onClick={this.handleHelpClose} />
            </Modal.Header>
            <Modal.Body className="help-modal-body">
              <Modal.Title>
                <video loop autoPlay width="100%" height="100%">
                  <source
                    src="https://api.drreddys.com/themes/custom/reddy_admin/images/tutorial.mp4"
                    type="video/mp4"
                  />
                  Your browser does not support the video tag.
                </video>
              </Modal.Title>
            </Modal.Body>
            {/* <Modal.Footer> &nbsp; </Modal.Footer> */}
          </Modal>
          {/* Help Popup End */}

          {/* Pre-Activation Modal */}
          <Modal
            show={this.props.show_pre_activation_modal}
            onHide={this.props.hidePreActivationModal}
            backdrop="static"
            className="pre-activation-warning-modal"
          >
            <Modal.Header closeButton>
              {/* <button className="close" onClick={this.props.hidePreActivationModal} /> */}
            </Modal.Header>
            <Modal.Body className="pre-activation-warning-modal-body">
              <Modal.Title>
                {this.props.dynamic_lang && this.props.dynamic_lang.header.warning}
              </Modal.Title>
              <h1>{this.props.dynamic_lang && this.props.dynamic_lang.header.logged_default_password}</h1>
            </Modal.Body>
            <Modal.Footer>
              <button 
                type="button"
                className="btn btn-default" 
                style={{ float: "right", height: '36px', width:'auto' }} 
                onClick={() => {this.props.hidePreActivationModal(); this.props.history.push("/my-account");}}
              >
                {this.props.dynamic_lang && this.props.dynamic_lang.header.goto_my_account}
              </button>
            </Modal.Footer>
          </Modal>
          {/* Pre-Activation Modal */}
        </div>
      );
    }else{
      return null;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.auth.token !== null ? true : false,
    auth_token: state.auth.token,
    dynamic_lang: state.auth.dynamic_lang,
    profile_pic: state.user.url,
    name: state.user.name,
    multi_lang_access: state.auth.multi_lang_access,
    new_feature: state.auth.new_feature,
    new_feature_send_mail: state.auth.new_feature_send_mail,
    new_feature_11: state.auth.new_feature_11,
    errors: state.user.errors,
    ncounter: state.user.ncounter ? state.user.ncounter : 0,
    nlist: state.user.nlist,
    company: state.user.company,
    selCompany: state.user.selCompany,
    switch_active: state.auth.switch_active,
    tour_counter_closed_task: state.auth.tour_counter_closed_task,
    tour_counter_11: state.auth.tour_counter_11,
    pre_activated: state.auth.pre_activated,
    show_pre_activation_modal: state.user.show_pre_activation_modal,
    has_disabled_pre_activation_modal: state.user.has_disabled_pre_activation_modal,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUser: (data, onSuccess) => dispatch(fetchUserData(data, onSuccess)),
    fetchNotification: (data, onSuccess) =>
      dispatch(fetchUserNotification(data, onSuccess)),
    readNotification: (data, onSuccess) =>
      dispatch(updateNotification(data, onSuccess)),
    setCompany: (data, onSuccess) => dispatch(setUserCompany(data, onSuccess)),
    disconnectUser: () => dispatch(authLogout()),
    disconnectUserReload: (data, onSuccess) => dispatch(authLogoutReload(data, onSuccess)),

    setLanguageExplicit: (data) => dispatch(setLanguage()),
    ChangeToEnglishExplicit: (data, onSuccess) => dispatch(ChangeToEnglish(data, onSuccess)),
    refreshTokenEdit: (data, onSuccess, setErrors) =>
      dispatch(refreshToken(data, onSuccess, setErrors)),
    refFeatureToken: (data, onSuccess, setErrors) =>
      dispatch(refreshFeatureToken(data, onSuccess, setErrors)),
    updateTourToken: (data) => dispatch(updateToken(data)),
    closePopupForSession: (data) => dispatch(feature11PopupSession(data)),
    showPreActivationModal: () => dispatch(showPreActivationModal()),
    hidePreActivationModal: () => dispatch(hidePreActivationModal()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Header));
