import React, { Component } from "react";
import { Link } from "react-router-dom";
// import Layout from "../Layout/layout";
import axios from "../../shared/axios";
import { BasicUserData } from "../../shared/helper";
import Pagination from "react-js-pagination";
import swal from "sweetalert";
// import { Row, Col, ButtonToolbar, Button, Modal } from "react-bootstrap";
// import { Formik, Form } from "formik";
// import { Tooltip } from "reactstrap";
// import infoIcon from "../../assets/images/info-circle-solid.svg";
import closeIconSuccess from "../../assets/images/times-solid-green.svg";
// import closeIconSelect from "../../assets/images/close.svg";
import closeIcon from "../../assets/images/times-solid-red.svg";


import Loader from "../Loader/loader";
// import Autosuggest from "react-autosuggest";
import Select from "react-select";
// import { isMobile } from "react-device-detect";


import { connect } from "react-redux";
import { getTStatus } from "../../store/actions/user";

//var message = "";
var select_status = [
  { value: "all", label: "All Stock Enquiries" },
  { value: "active", label: "All Active Enquiries"},
  { value: "cancel", label: "All Cancelled Enquiries"},]

class MyEnquiries extends Component {
  constructor(props) {
    super(props);    
    this.props.history.push({
      state: { message: '' }
    })
  }
  state = {
    enquiriesList: [],
    activePage: 1,
    totalCount: 0,
    itemPerPage: 10,
    selectedOption : '',
    message : this.props.location.state ? this.props.location.state.message : ""
  };
  componentDidMount() {
    this.getEnquiries();
    document.title =
      this.props.dynamic_lang.my_enquiries.header + " | Dr. Reddy's API";

    let userData = BasicUserData();
    if (userData) {
      this.setState({
        isLoading: false,
      });
    }
  }

  hideError = () => {
    this.setState({ errMsg: "" });
  };
  hideSuccessMsg = () => {
    this.setState({ message: "" });
    this.props.history.push({
      state: { message: '' }
    })
  };

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.getEnquiries(pageNumber > 0 ? pageNumber : 1 ,this.state.selectedOption.value);
  };
  handleSearchChange = (event) => {
    var status = event.value;
    this.setState({ activePage: 1 });
    this.getEnquiries(1,status);
  };
  getEnquiries(page = 1,status = 'all') {
    this.setState({ showLoader: true });
    axios
      .get(`/sap/my_enquiries?page=${page}&status=${status}`)
      .then((res) => {
        this.setState({
          enquiriesList: res.data.data,
          count_req: res.data.count,
          isLoading: false,
          showLoader : false
        });
      })
      .catch((err) => { });
  };

  cancelButton = (event) => {
    const target = event.target;
    var task_id= target.value; 
    swal({
      closeOnClickOutside: false,
      title: "Alert",
      text:
        "Are you sure you want to cancel?",
      icon: "warning",
      buttons: true,
      dangerMode: true,
      customClass: 'customAlert',
    }).then((willDelete) => {
      if (willDelete) {        
        this.setState({ showLoader: true });
        axios.post("sap/enquiry_cancel", {task_id : task_id})
          .then((res) => {
            if (res.status === 200) {
              if (res.data.status == 1) {
                //alert('Cancelled')
                this.setState({ showLoader: false });
                //var d = document.getElementsByClassName("tr_"+task_id);
                //while (d.length)   d[0].parentElement.removeChild(d[0]);
                this.getEnquiries(this.state.activePage,this.state.selectedOption.value);
                this.setState({ message: 'Stock reservation task successfully cancelled.' });
              } else {
                this.setState({ showLoader: false, errMsg: res.data.message });
              }
            } else {
              this.setState({ errMsg: "Fail to cancel" });
            }
          }).catch((err) => {
            this.setState({ showLoader: false });
            if (err.data.status === 2) {
              
              this.setState({ errMsg:  "Fail to cancel" });
            } else {
              this.setState({ errMsg: err.data.message });
            }
          });
        }
      });
  };



  render() {
    if (this.state.isLoading === true) {
      return (
        <>
          <div className="loginLoader">
            <div className="lds-spinner">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <span>Please Wait…</span>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <>

    
          <div className="container-fluid clearfix listingSec">
            <div className="dashboard-content-sec">
              <div className="col-md-12 form-search-section formSearchrequest">
              </div>
              
                
              <div className="service-request-form-sec">
                {this.state.showLoader === true ? <Loader /> : null}

                {this.state.message !== null && this.state.message !== "" && (
                  <div className="messagessuccess">
                    <Link to="#" className="close">
                      <img
                        src={closeIconSuccess}
                        width="17.69px"
                        height="22px"
                        onClick={(e) => this.hideSuccessMsg()}
                      />
                    </Link>
                    {this.state.message}
                  </div>
                )}
                <div
                  className="messageserror"
                  style={{
                    display:
                      this.state.errMsg &&
                      this.state.errMsg !== null &&
                      this.state.errMsg !== ""
                        ? "block"
                        : "none",
                  }}
                >
                  <Link to="#" className="close">
                    <img
                      src={closeIcon}
                      width="17.69px"
                      height="22px"
                      onClick={(e) => this.hideError()}
                    />
                  </Link>
                  <div>
                    <ul className="">
                      <li className="">{this.state.errMsg}</li>
                    </ul>
                  </div>
                </div>

                <div>
                  <div className="row">
                    <div className="col-md-8 form-page-title2">
                      <h2>My Stock Enquiries / Orders</h2>{" "}
                    </div>

                    <div className="col-md-4 form-search-section formSearchrequest">
                      {/* <select className="" id="serach_type">
                        <option> Select Status</option>
                        <option value="all"> All Stock Enquiries</option>
                        <option value="active"> All Active Enquiries</option>
                        <option value="cancel"> All Cancelled Enquiries</option>
                      </select> */}
                      <Select
                            value={this.state.selectedOption}
                            name="serach_type"
                            options={select_status}
                            isSearchable={false}
                            isClearable={false}
                            placeholder='Select Status'
                            onChange={(e) => {
                              if (e === null || e === "") {                                
                                this.setState({ selectedOption: "" });
                              } else {                                
                                this.setState({ selectedOption: e });
                              }                              
                              this.handleSearchChange(e);
                            }}
                          />
                    </div>
                    <div className="clearfix" />
                  </div>
                  {this.state.isLoading === true ? (
                    <Loader />
                  ) : (
                      <div className="listing-table table-responsive">
                        <table className="" style={{ width: "100%" }}>
                          <thead>
                            <tr>
                              <th>
                                {this.props.dynamic_lang.my_orders.order_no}
                              </th>
                              <th>{this.props.dynamic_lang.my_orders.product}</th>
                              <th>
                                {this.props.dynamic_lang.my_orders.quantity}
                              </th>
                              <th>
                                {this.props.dynamic_lang.my_orders.requested_date}
                              </th>
                              <th>
                                {this.props.dynamic_lang.my_orders.expected_date}
                              </th>
                              <th>{this.props.dynamic_lang.my_orders.status}</th>
                              <th>
                                {this.props.dynamic_lang.my_orders.submitted_by}
                              </th>
                              <th>
                                {this.props.dynamic_lang.my_orders.action_requested}
                              </th>
                              <th>
                                SO Number
                              </th>
                              <th>SO Date</th>
                              <th>
                                {this.props.dynamic_lang.my_orders.status}
                              </th>                              
                              <th>Action </th>
                            </tr>
                          </thead>
                          <tbody>
                            {this.state.enquiriesList.map((enquiry, key) => (
                          
                              <tr key={key} className={"tr_"+enquiry.task_id}>
                                <td style={{width: '10%'}}>{enquiry.task_ref}</td>
                                <td>{enquiry.product_name}</td>
                                <td>{enquiry.quantity.trim()}</td>
                                <td>{enquiry.date_added}</td>
                                <td>{enquiry.due_date}</td>
                                <td>
                                {(enquiry.po_status != 1 && enquiry.po_status!=undefined) ? <Link
                                    to={{
                                      pathname: `/edit-stock-enquiry/${enquiry.task_id}`,
                                      state: { fromDashboard: true },
                                    }}
                                    className=""
                                    style={{ cursor: "pointer" }}
                                    //onClick={() => this.showStatus(enquiry.task_id)}
                                  >
                                    {enquiry.status}
                                  </Link> : 
                                  <Link
                                    href="#"
                                    className=""
                                    style={{ cursor: "pointer" }}
                                  >  {enquiry.status}
                                  </Link>
                                  }
                                </td>
                                <td>{enquiry.first_name} {enquiry.last_name}</td>
                                <td>
                                {(enquiry.po_status != 1 && enquiry.po_status!=undefined) ? <Link
                                    to={{
                                      pathname: `/edit-stock-enquiry/${enquiry.task_id}`,
                                      state: { fromDashboard: true },
                                    }}
                                    className=""
                                    style={{ cursor: "pointer" }}
                                  >
                                    {enquiry.required_action}
                                  </Link> : 
                                  <Link
                                    href="#"
                                    className=""
                                    style={{ cursor: "pointer" }}
                                  >  {enquiry.required_action}
                                  </Link>
                                }
                                </td>
                                <td>{enquiry.po_number != null
                                  ? enquiry.po_number
                                  : "-"}</td>
                                <td>{enquiry.po_number != null
                                  ? enquiry.po_date_added
                                  : ""}</td>
                                <td className="ratnow W130">{enquiry.cancel_reservation != 1
                                  ? enquiry.status
                                : `Cancelled` } 
                                {(enquiry.cancel_reservation == 1 && enquiry.cancel_comment!='' && enquiry.cancel_comment!=null) ? (<p className="cancel_txt">By {enquiry.cancel_comment}</p>) : ''} </td>
                                <td>
                                  {(enquiry.po_status != 1 && enquiry.po_status!= undefined && enquiry.cancel_reservation!=1) ? (
                                    <>
                                  <Link
                                    to={{
                                      pathname: `/edit-stock-enquiry/${enquiry.task_id}`,
                                      state: { fromDashboard: true },
                                    }}
                                    className="action-btn"
                                  >
                                    Edit
                                  </Link>
                                  </>
                                  ) : (
                                    <>
                                      {(enquiry.po_number == null && enquiry.close_status!=1) ?  (
                                      <>
                                        <button
                                          className="btn btn-default btn-cancel mt-0"
                                          style={{
                                            width: "auto",cursor: "pointer"
                                          }}
                                          onClick={this.cancelButton}
                                          value = {enquiry.task_id}
                                        >
                                          Cancel
                                        </button> 
                                      </>
                                      ) : (
                                        <>
                                          <button
                                            className="btn btn-default btn-cancel mt-0"
                                            style={{
                                              width: "auto",opacity: "0.4", cursor:"text"
                                            }} 
                                          >
                                          Cancel
                                        </button> 
                                        </>
                                      )}                                 
                                    </>
                                  )}
                                </td>
                              </tr>
                            ))}
                            {this.state.enquiriesList.length === 0 && (
                              <tr>
                                <td colSpan="11" align="center">
                                  {
                                    this.props.dynamic_lang.my_requests
                                      .no_data_display
                                  }
                                </td>                                
                              </tr>
                            )}
                          </tbody>
                          {/* monosom hide */}
                          {/* <tbody>
                            <tr>
                              <td>
                                <Link
                                  to={{
                                    pathname: `/task-details-one/8231`,
                                  }}
                                  style={{ cursor: "pointer" }}
                                >
                                  PHL-O-8231-1
                              </Link>
                              </td>
                              <td>Abiraterone Acetate</td>
                              <td>100 Kgs</td>
                              <td>03 Sep 2020</td>
                              <td>31 Dec 2020</td>
                              <td>
                                <Link
                                  to="#"
                                  className=""
                                  style={{ cursor: "pointer" }}
                                  onClick={() => this.showStatus(8241)}
                                >
                                  PO pending
                              </Link>
                              </td>
                              <td>Pratikshya Portuguese</td>
                              <td>
                                <Link
                                  to="#"
                                  className=""
                                  style={{ cursor: "pointer" }}
                                >
                                  Document Required
                              </Link>
                              </td>
                              <td>{"-"}</td>
                              <td>{"-"}</td>
                              <td className="ratnow W130">-</td>
                              <td>
                                <Link
                                  to={{
                                    pathname: `/task-details-one/8231`,
                                    state: { fromDashboard: true },
                                  }}
                                  className="action-btn"
                                >
                                  Discussion
                              </Link>
                              </td>
                              <td>
                                <Link
                                  to={{
                                    pathname: `/edit-stock-enquiry`,
                                    state: { fromDashboard: true },
                                  }}
                                  className="action-btn"
                                >
                                  Edit
                              </Link>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <Link
                                  to={{
                                    pathname: `/task-details-one/8231`,
                                  }}
                                  style={{ cursor: "pointer" }}
                                >
                                  PHL-O-8231-2
                              </Link>
                              </td>
                              <td>Abiraterone Acetate</td>
                              <td>100 Kgs</td>
                              <td>03 Sep 2020</td>
                              <td>26 Dec 2020</td>
                              <td>
                                <Link
                                  to="#"
                                  className=""
                                  style={{ cursor: "pointer" }}
                                  onClick={() => this.showStatus(8203)}
                                >
                                  PO pending
                              </Link>
                              </td>
                              <td>Spanish Customer</td>
                              <td>
                                <Link
                                  to="#"
                                  className=""
                                  style={{ cursor: "pointer" }}
                                >
                                  Document Required
                              </Link>
                              </td>
                              <td>{"-"}</td>
                              <td>{"-"}</td>
                              <td className="ratnow W130">
                                -
                            </td>
                              <td>
                                <Link
                                  to={{
                                    pathname: `/task-details-one/8231`,
                                    state: { fromDashboard: true },
                                  }}
                                  className="action-btn"
                                >
                                  Discussion
                              </Link>
                              </td>
                              <td>
                                <Link
                                  to={{
                                    pathname: `/edit-stock-enquiry`,
                                    state: { fromDashboard: true },
                                  }}
                                  className="action-btn"
                                >
                                  Edit
                              </Link>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <Link
                                  to={{
                                    pathname: `/task-details-two/8231`,
                                  }}
                                  style={{ cursor: "pointer" }}
                                >
                                  PHL-O-6560
                              </Link>
                              </td>
                              <td>Amlodipine Besylate</td>
                              <td>500 Kgs</td>
                              <td>04 Sep 2020</td>
                              <td>26 Dec 2020</td>
                              <td>
                                <Link
                                  to="#"
                                  className=""
                                  style={{ cursor: "pointer" }}
                                  onClick={() => this.showStatus(8203)}
                                >
                                  PO pending
                              </Link>
                              </td>
                              <td>Spanish Customer</td>
                              <td>
                                <Link
                                  to="#"
                                  className=""
                                  style={{ cursor: "pointer" }}
                                >
                                  Document Required
                              </Link>
                              </td>
                              <td>{"-"}</td>
                              <td>{"-"}</td>
                              <td className="ratnow W130">
                                -
                            </td>
                              <td>
                                <Link
                                  to={{
                                    pathname: `/task-details-two/8231`,
                                    state: { fromDashboard: true },
                                  }}
                                  className="action-btn"
                                >
                                  Discussion
                              </Link>
                              </td>
                              <td>
                                <Link
                                  to={{
                                    pathname: `/edit-stock-enquiry`,
                                    state: { fromDashboard: true },
                                  }}
                                  className="action-btn"
                                >
                                  Edit
                              </Link>
                              </td>
                            </tr>
                          </tbody> */}
                          {/* monosom hide */}
                        </table>
                      </div>
                    )}
                </div>
                <div className="clearfix" />
                {this.state.count_req > this.state.itemPerPage ? (
                  <div className="pagination-area">
                    <div className="paginationOuter text-right">
                      <Pagination
                        hideDisabled
                        hideFirstLastPages
                        prevPageText="‹‹"
                        nextPageText="››"
                        activePage={this.state.activePage}
                        itemsCountPerPage={this.state.itemPerPage}
                        totalItemsCount={this.state.count_req}
                        itemClass="nav-item"
                        linkClass="nav-link"
                        activeClass="active"
                        pageRangeDisplayed="1"
                        onChange={this.handlePageChange}
                      />
                    </div>
                  </div>
                ) : null}
                <div className="clearfix" />
              </div>
            </div>
          </div>
        </>
      );
    }
  }
}

// SATYAJIT
const mapStateToProps = (state) => {
  return {
    display_menu: state.menu.display,
    task_status_arr: state.user.task_status_arr,
    selCompany: state.user.selCompany,
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getTaskStatus: (data, onSuccess) => dispatch(getTStatus(data, onSuccess)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyEnquiries);
