import React, { Component } from "react";
import { Link } from "react-router-dom";
import Select from "react-select";
import { Tooltip } from "reactstrap";
import infoIcon from "../../assets/images/info-circle-solid.svg";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Editor } from "@tinymce/tinymce-react";
import closeIcon from "../../assets/images/times-solid-red.svg";
import closeIconSelect from "../../assets/images/close.svg";
import closeIconSuccess from "../../assets/images/times-solid-green.svg";

import dateFormat from "dateformat";

import {
  ButtonToolbar,
  Button,
  Modal,
  OverlayTrigger
} from "react-bootstrap";

import axios from "../../shared/axios_qa";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import {
  htmlDecode,
  supportedFileType,
  BasicUserData,
} from "../../shared/helper";
import Dropzone from "react-dropzone";
import Loader from "../Loader/loader";
import { connect } from "react-redux";
import { isMobile } from "react-device-detect";

var path = require("path");
var cc_Customer = [];

var minDate = "";
var maxDate = "";
//validation
let initialValues = {
  customer_name: "",
  batch_number: "",
  lot_no: "",
  file: "",
  file_name: [],
};


const validationSchema = (refObj) =>
  Yup.object().shape({
    customer_name: Yup.string().trim().required(
      'Customer Name is required.'
    ),
    batch_number: Yup.string().trim().required(
      'Batch Number is required.'
    ),
    lot_no: Yup.string().trim().required(
      'Inspection Lot Number is required.'
    ),
    file_name: Yup.string()
      //.required("Please upload determination file")
      .test(
        "required",
        'Please attach COA doc.',
        function (value) {
          return value ? true : false;
        }
      ),
  });

class QAForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: "",
      tooltipOpen: false,
      showLoader: false,
      selectedOption: null,
      share_with_agent: true,
      message: null,
      products: [],
      units: [],
      countryList: [],
      selectedCountry: null,
      files: [],
      filesHtml: "",
      rejectedFile: [],
      cc_Customers: [],
      selectedUnit: "",
      selectedDate: "",
      errMsg: null,
      customerList: [],
      ccAgentCustomerList: [],
      pre_selected: [],
      selUser: null,
      displayCCList: false,
      agent_customer_id: null,
      closed_discussion_err: false,
      closed_discussion_err_txt: ''
    };
    this.inputRef = React.createRef();
  }

  deleteFile = (e, setFieldValue) => {
    this.inputRef.current.value = null; // clear input value - SATYAJIT
    e.preventDefault();
    var tempArray = [];

    this.state.files.map((file, fileIndex) => {
      if (file.checked === true) {
      } else {
        tempArray.push(file);
      }
    });

    setFieldValue("file_name", tempArray);

    this.setState({ files: tempArray });
  };

  componentDidMount() {
    this.setState({ files: [], filesHtml: "" });

    document.title = `QA FORM | Dr.Reddys API`;

    if (localStorage.qa_token == null || typeof localStorage.qa_token == 'undefined' ){
      this.props.history.push("/qa_login"); 
    }
  }

  fileChangedHandler = (event) => {
    this.setState({ upload_file: event.target.files[0] });
  };
  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue, errors) => {
    console.log(acceptedFiles);
    var rejectedFiles = [];
    var uploadFile = [];

    var totalfile = acceptedFiles.length;

    for (var index = 0; index < totalfile; index++) {
      var error = 0;
      var filename = acceptedFiles[index].name.toLowerCase();
      var extension_list = ["pdf"];
      var ext_with_dot = path.extname(filename);
      var file_extension = ext_with_dot.split(".").join("");

      var obj = {};

      var fileErrText = `Only files with the following extensions are allowed: pdf.`;

      if (extension_list.indexOf(file_extension) === -1) {
        error = error + 1;

        let str_1 = `One or more files could not be uploaded.The specified file [file_name] could not be uploaded.`;
        let str_2 = `The specified file [file_name] could not be uploaded.`;
        if (totalfile > 1) {
          if (index === 0) {
            obj["errorText"] =
            str_1.replace(
                "[file_name]",
                filename
              ) + fileErrText;
          } else {
            obj["errorText"] =
              str_2.replace(
                "[file_name]",
                filename
              ) + fileErrText;
          }
        } else {
          obj["errorText"] =
            str_2.replace(
              "[file_name]",
              filename
            ) + fileErrText;
        }
        rejectedFiles.push(obj);
      }

      if (acceptedFiles[index].size > 50000000) {
        let err_txt = `The file [file_name] could not be saved because it exceeds 50 MB, the maximum allowed size for uploads.`;
        obj[
          "errorText"
        ] = err_txt.replace(
          "[file_name]",
          filename
        );
        error = error + 1;
        rejectedFiles.push(obj);
      }

      if (error === 0) {
        uploadFile.push(acceptedFiles[index]);
        setErrors({ file_name: false });
      }
    }

    // //setErrors({ file_name: false });
    // errors.file_name = [];
    //setFieldValue(this.state.files);

    var prevFiles = this.state.files;
    var newFiles = [];
    if (prevFiles.length > 0) {
      for (let index = 0; index < uploadFile.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (uploadFile[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        if (remove === 0) {
          prevFiles.push(uploadFile[index]);
        }
      }

      prevFiles.map((file) => {
        file.checked = false;
        newFiles.push(file);
      });
    } else {
      uploadFile.map((file) => {
        file.checked = false;
        newFiles.push(file);
      });

      console.log("acceptedFiles", acceptedFiles);
      console.log("newFiles", newFiles);
    }

    this.setState({
      files: acceptedFiles,
      // filesHtml: fileListHtml
    });
    console.log("newFiles", acceptedFiles);

    setFieldValue("file_name", acceptedFiles);

    this.setState({
      rejectedFile: rejectedFiles,
    });
    // }
  };
  handleSubmit = (values, actions) => {
    this.setState({ showLoader: true });
    
    this.setState({ message: "" });

    this.setState({ errMsg: "" });

    var formData = new FormData();


    //alert(JSON.stringify(values))
    if (this.state.files && this.state.files.length > 0) {
      for (let index = 0; index < this.state.files.length; index++) {
        const element = this.state.files[index];
        formData.append("file", element);
      }
    } else {
      formData.append("file", []);
    }

    formData.append("customer_name", values.customer_name.trim());
    formData.append("batch_number", values.batch_number.trim());
    formData.append("lot_no", values.lot_no.trim());


    axios
      .post("qa_form", formData)
      .then((res) => {
        this.setState({
          showLoader: false,
          closed_discussion_err: true,
          closed_discussion_err_txt: `Data added successfully.`
        });

      })
      .catch((err) => {
        this.setState({ showLoader: false });
        actions.setSubmitting(false);
        actions.setErrors({
          customer_name: err.data.errors.customer_name,
          batch_number: err.data.errors.batch_number,
          lot_no: err.data.errors.lot_no,
          file_name : err.data.errors.file_name
        });
      });
  };

  hideSuccessMsg = () => {
    this.setState({ message: "" });
  };

  removeError = (setErrors) => {
    setErrors({});
  };
  removeFile_Error = () => {
    this.setState({ rejectedFile: [] });
  };
  hideError = () => {
    this.setState({ errMsg: "" });
  };

  handleClosedDiss = (e) => {
    this.setState({
      closed_discussion_err: false,
      closed_discussion_err_txt: ''
    });
    window.location.reload();
  };

  render() {
    const {
      selectedOption,
      products,
      units,
      countryList,
      selectedCountry,
      selectedUnit,
      selectedDate,
      customerList,
      ccAgentCustomerList,
    } = this.state;
    return (
      <div className="container-fluid clearfix formSec">
        <div className="dashboard-content-sec">
          <div className="service-request-form-sec newFormSec">
            {this.state.closed_discussion_err === true && (
              <Modal
                show={this.state.closed_discussion_err}
                onHide={this.handleClosedDiss}
                backdrop="static"
                className="modalRating"
              >
                <Modal.Header>
                  <img
                    src={closeIconSelect}
                    width="15.44"
                    height="18"
                    id="DisabledAutoHideExample"
                    type="button"
                    onClick={(e) => this.handleClosedDiss(e)}
                    className="close"
                  />
                </Modal.Header>
                <Modal.Body>
                  <Modal.Title>
                    <div className="ratinfo">

                    </div>
                  </Modal.Title>
                  <p style={{ textAlign: 'center' }} >{this.state.closed_discussion_err_txt}</p>
                </Modal.Body>
              </Modal>
            )}
            <div className="form-page-title-block">
              <h2>
                {`Upload COA Document`}
                <button onClick={()=>{localStorage.clear();this.props.history.push("/qa_login");}} className="btn btn-default" >Log Out</button>
              </h2>
            </div>
            {this.state.showLoader === true ? <Loader /> : null}
            <Formik
              initialValues={initialValues}
              validationSchema={() => validationSchema(this)}
              onSubmit={this.handleSubmit}
            >
              {({
                errors,
                values,
                touched,
                setErrors,
                setFieldValue,
                setFieldTouched,
              }) => {
                console.log("formik values", values);

                return (
                  <Form>
                    <div
                      className="messageserror"
                      style={{
                        display:
                          this.state.errMsg &&
                          this.state.errMsg !== null &&
                          this.state.errMsg !== ""
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.hideError()}
                        />
                      </Link>
                      <div>
                        <ul className="">
                          <li className="">{this.state.errMsg}</li>
                        </ul>
                      </div>
                    </div>
                    <div
                      className="messageserror"
                      style={{
                        display:
                          (errors.customer_name && touched.customer_name) ||
                          (errors.batch_number && touched.batch_number) ||
                          (errors.lot_no && touched.lot_no) ||
                          (errors.file_name && touched.file_name)
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.removeError(setErrors)}
                        />
                      </Link>
                      <div>
                        <ul className="">
                          {errors.customer_name && touched.customer_name ? (
                            <li className="">{errors.customer_name}</li>
                          ) : null}
                          {errors.batch_number && touched.batch_number ? (
                            <li className="">{errors.batch_number}</li>
                          ) : null}
                          {errors.lot_no && touched.lot_no ? (
                            <li className="">{errors.lot_no}</li>
                          ) : null}
                          {errors.file_name && touched.file_name ? (
                            <li className="">{errors.file_name}</li>
                          ) : null}
                        </ul>
                      </div>
                    </div>

                    <div className="row">
                      
                      <div className="col-md-4">
                        <Field
                          name="customer_name"
                          type="text"
                          className="form-control customInput"
                          placeholder={
                            `Customer Name *`
                          }
                        />
                      </div>
                      <div className="col-md-4">
                        <Field
                          name="batch_number"
                          type="text"
                          className="form-control customInput"
                          placeholder={
                            `Batch Number *`
                          }
                        />
                      </div>
                      <div className="col-md-4">
                        <Field
                          name="lot_no"
                          type="text"
                          className="form-control customInput"
                          placeholder={
                            `Inspection Lot Number *`
                          }
                        />
                      </div>
                    </div>
                    <br />

                    <div
                      className="messageserror"
                      style={{
                        display:
                          (this.state.rejectedFile &&
                            this.state.rejectedFile.length) > 0
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.removeFile_Error()}
                        />
                      </Link>
                      {this.state.rejectedFile &&
                        this.state.rejectedFile.map((file, index) => {
                          return (
                            <div key={index}>
                              <ul className="">
                                <li className="">{file.errorText}</li>
                              </ul>
                            </div>
                          );
                        })}
                    </div>

                    <div className="col-md-4">
                      <label className="mb-0">
                        Upload COA doc {" "}
                        <span className="astrix">*</span>
                      </label>
                    </div>
                    <div className="clearfix" />
                    <div className="row uploadSec">
                      {/* <div className="col-md-4">
                          <input type="file" className="customFile" />
                        </div> */}

                      <div className="col-md-3">
                        <Dropzone
                          maxFiles={1}
                          multiple={false}
                          onDrop={(acceptedFiles) =>
                            this.setDropZoneFiles(
                              acceptedFiles,
                              setErrors,
                              setFieldValue
                            )
                          }
                        >
                          {({ getRootProps, getInputProps }) => (
                            <section>
                              <div {...getRootProps()} className="customFile">
                                <input
                                  {...getInputProps()}
                                  ref={this.inputRef}
                                  style={{ display: "block" }}
                                />
                              </div>
                            </section>
                          )}
                        </Dropzone>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col text-center">
                        <button className="btn btn-default">
                          {`Submit`}
                        </button>
                      </div>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

export default QAForm;
