import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import axios from "../../shared/axios";
import closeIcon from "../../assets/images/times-solid-red.svg";
import closeIconSuccess from "../../assets/images/times-solid-green.svg";
//import { BasicUserData } from "../../shared/helper";
import { withCookies, Cookies } from "react-cookie";
import Loader from "../Loader/loader";
import { connect } from "react-redux";
import { portalQALogin } from "../../store/actions/auth";
import { stat } from "fs";
import { inArray } from "../../shared/helper";

const initialValues = {
  username: "",
  password: "",
};

var message = "";

class Login extends Component {
  constructor(props) {
    super(props);

    message = this.props.location.state
      ? this.props.location.state.message
      : "";
  }

  state = {
    errMsg: null,
    deniedPermissionCookies: false,
    errStatusCode: "",
    website_lang: "",
  };

  componentDidMount() {

    if (localStorage.qa_token !== null && typeof localStorage.qa_token != 'undefined' ){
      this.props.history.push("/qa_form");
    }

    if (message !== null) {
      this.props.history.push({
        pathname: "",
        state: "",
      });
    }
    document.title = `QA LOGIN | Dr.Reddys API`;

    
  }

  handleSubmit = (values, actions) => {
    this.setState({ errMsg: "", errStatusCode: "" });

    axios
    .post("/qa_login", {"username": values.username,"password": values.password})
    .then(res => {
        localStorage.setItem('qa_token',res.data.token);
        this.props.history.push("/qa_form");
    })
    .catch(err => {
      actions.setErrors({
        password: err.data.errors.password
      });
      //actions.setErrors(error.data.errors);
      actions.setSubmitting(false);
    });

  };

  hideSuccessMsg = () => {
    message = "";
  };

  removeError = (setErrors) => {
    this.setState({ errMsg: "" });
  };

  render() {

    const loginvalidation = Yup.object().shape({
      username: Yup.string()
        .required(`Please enter email address`)
        .email(
          `Please enter a valid email address`
        ),
      password: Yup.string().required(
        `Please enter password`
      ),
    });

    return this.props.showLoader === true ? (
      <div className="loginLoader">
        <div className="lds-spinner">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <span>Please Wait…</span>
        </div>
      </div>
    ) : (
        <div className="outside bg-white">
          <div className="row">
            <div className="col-md-6">
              <div className="row h-100 justify-content-center align-items-center">
                <div className="login-form-sec">

                  

                  <div className="login-form-box mx-auto text-center">

                    <div className="login-form-wrapper">
                      <div className="logo-login">
                        <a
                          href={process.env.REACT_APP_PORTAL_URL}
                          target="_blank"
                        >
                          <img
                            src={require("../../assets/images/logo-blue.jpg")}
                          />
                        </a>
                      </div>
                      <div className="logo-base">
                        <img
                          src={require("../../assets/images/Xceed_Logo_Purple_RGB.png")}
                        />
                      </div>
                      <h2>{`QA Login`}</h2>
                      <div className="clearfix" />
                    </div>

                    {message !== null && message !== "" && message !== undefined && (
                      <div className="messagessuccess">
                        <Link to="#" className="close">
                          <img
                            src={closeIconSuccess}
                            width="17.69px"
                            height="22px"
                            onClick={(e) => this.hideSuccessMsg()}
                          />
                        </Link>
                        {message}
                      </div>
                    )}
                    <Formik
                      initialValues={initialValues}
                      validationSchema={loginvalidation}
                      onSubmit={this.handleSubmit}
                    >
                      {({
                        values,
                        errors,
                        isValid,
                        touched,
                        isSubmitting,
                        setErrors,
                      }) => {
                        return (
                          <Form>
                            <div
                              className="messageserror"
                              style={{
                                display:
                                  this.state.errMsg &&
                                    this.state.errMsg !== null &&
                                    this.state.errMsg !== ""
                                    ? "block"
                                    : "none",
                              }}
                            >
                              <Link to="#" className="close">
                                <img
                                  src={closeIcon}
                                  width="17.69px"
                                  height="22px"
                                  onClick={(e) => this.removeError(setErrors)}
                                />
                              </Link>

                              {this.state.errStatusCode === 4 ? (
                                <div>
                                  <span className="errorMsg">
                                    {
                                      `Your password has expired. Please click`
                                    }
                                  </span>
                                </div>
                              ) : (
                                  <div>
                                    <span className="errorMsg">
                                      {this.state.errMsg}
                                    </span>
                                  </div>
                                )}
                            </div>
                            <div className="login-form-wrapper">
                              <div className="row">
                                <div className="col-md-12">
                                  <Field
                                    name="username"
                                    type="text"
                                    className="form-control customInput"
                                    placeholder={
                                      `User name`
                                    }
                                  />
                                  {errors.username && touched.username ? (
                                    <span className="errorMsg">
                                      {errors.username}
                                    </span>
                                  ) : null}
                                </div>
                                <div className="col-md-12">
                                  <Field
                                    name="password"
                                    type="password"
                                    className="form-control customInput"
                                    placeholder={
                                      `Password`
                                    }
                                  />
                                  {errors.password && touched.password ? (
                                    <span className="errorMsg">
                                      {errors.password}
                                    </span>
                                  ) : null}
                                </div>
                              </div>
                              <div className="row">
                                <div className="col text-center">
                                  <Button
                                    type="submit"
                                    className="btn btn-default btn-secondary btn-lg"
                                  >
                                    {isSubmitting
                                      ? `logging In...`
                                      : `Log in`}
                                  </Button>
                                </div>
                              </div>
                              <div className="clearfix" />
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-md-6">
              <div className="login-graphic-area">
                <div className="login-graphic-img">
                  <img
                    src={require("../../assets/images/login-graphic-img.jpg")}
                  />
                </div>
              </div>
            </div>
            <div className="clearfix" />
          </div>
        </div>
      );
  }
}


export default Login;
