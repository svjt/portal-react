import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button } from "react-bootstrap";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import axios from "../../shared/axios";
import closeIcon from "../../assets/images/times-solid-red.svg";
import closeIconSuccess from "../../assets/images/times-solid-green.svg";
//import { BasicUserData } from "../../shared/helper";
import { withCookies, Cookies } from "react-cookie";
import Loader from "../Loader/loader";
import { connect } from "react-redux";
import { portalLogin } from "../../store/actions/auth";
import { stat } from "fs";
import { inArray } from "../../shared/helper";

const initialValues = {
  username: "",
  password: "",
};

var message = "";

class Login extends Component {
  constructor(props) {
    super(props);

    message = this.props.location.state
      ? this.props.location.state.message
      : "";
  }

  state = {
    errMsg: null,
    name: this.props.cookies.cookies.name,
    deniedPermissionCookies: false,
    errStatusCode: "",
    website_lang: "",
  };

  componentDidMount() {
    if (this.props.token !== null) {
      this.props.history.push("/dashboard");
    }

    if (message !== null) {
      this.props.history.push({
        pathname: "",
        state: "",
      });
    }
    document.title = this.state.website_lang.login.login + " | Dr. Reddy's API";

    if (
      !sessionStorage.website_lang ||
      sessionStorage.website_lang == "" ||
      (this.props.match.params.id &&
        this.props.match.params.id != "" &&
        this.props.match.params.id != sessionStorage.website_lang)
    ) {
      if (
        this.props.match.params.id &&
        this.props.match.params.id != "" &&
        inArray(this.props.match.params.id, ["en", "ja", "zh", "pt", "es"])
      ) {
        sessionStorage.website_lang = this.props.match.params.id;
      } else {
        sessionStorage.website_lang = "en";
      }
    }

    let lang_data = require(`../../assets/lang/${sessionStorage.website_lang}.json`);

    this.setState({ website_lang: lang_data });

    this.props.history.push("/login");
  }

  componentWillMount() {
    if (sessionStorage.website_lang && sessionStorage.website_lang != "") {
      let lang_data = require(`../../assets/lang/${sessionStorage.website_lang}.json`);
      this.setState({ website_lang: lang_data });
    } else {
      let lang_data = require(`../../assets/lang/en.json`);
      this.setState({ website_lang: lang_data });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.token !== null) {
      this.props.history.push("/dashboard");
    }
  }

  handleSubmit = (values, actions) => {
    this.setState({ errMsg: "", errStatusCode: "" });

    this.props.submitLogin(
      values,
      () => {
        // let _paq = _paq || [];
        // _paq.push(['setUserId', values.username]);
        // _paq.push(['trackPageView']);
        // _paq.push(['enableLinkTracking']);

        // (function() {
        //     let u = "https://acumendev.mydrreddys.com/matomo/";
        //     _paq.push(['setTrackerUrl', u+'matomo.php']);
        //     _paq.push(['setSiteId', 'AYj7O86rN6r04NZD']);

        //     let d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
        //     g.type = 'text/javascript';
        //     g.async = true;
        //     g.defer = true;
        //     g.src = u + 'matomo.js';
        //     s.parentNode.insertBefore(g, s);
        // })();

        // window._paq = _paq;
        // console.log('=======>', window._paq);

        if(this.props.tour_counter_11 == true){
          axios.get(`/tasks/update_tour_counter_11`)
          .then(res => {
            axios
              .get("/generate_token")
              .then(response => {
                this.props.updateTourToken(response.data.token)
              })
              .catch(error => {
              })
          })
          .catch(err => {
          });
        }

        this.props.history.push("/dashboard");
      },
      () => {
        actions.setSubmitting(false);
        let er = this.props.errors;

        let errlogin = "";
        if (sessionStorage.website_lang == "zh") {
          errlogin = "无效的登录详细信息";
        } else if (sessionStorage.website_lang == "ja") {
          errlogin = "無効なログイン詳細";
        } else if (sessionStorage.website_lang == "pt") {
          errlogin = "detalhes de login inválidos";
        } else if (sessionStorage.website_lang == "es") {
          errlogin = "Detalles de acceso incorrectos";
        } else if (sessionStorage.website_lang == "en") {
          errlogin = "Invalid login details"
        }

        if (er) {
          if (er.status === 2) {
            //this.setState({ errMsg: er.errors.password });
            this.setState({ errMsg: errlogin });
          } else if (er.status === 3) {
            this.setState({ errMsg: er.message });
          } else if (er.status === 4) {
            this.setState({
              errMsg: er.errors.expired,
              errStatusCode: er.status,
            });
          } else {
            this.setState({
              errMsg: this.state.website_lang.login.invalid_access,
            });
          }
        }
      }
    );
  };

  hideSuccessMsg = () => {
    message = "";
  };

  removeError = (setErrors) => {
    this.setState({ errMsg: "" });
  };

  setCookies = () => {
    const { cookies } = this.props;

    cookies.set("name", "DrReddy", { path: "/" });
    this.setState({ name: this.props.cookies.cookies.name });
  };

  deniedCookiesStore = () => {
    this.setState({ deniedPermissionCookies: true });
  };

  render() {

    const loginvalidation = Yup.object().shape({
      username: Yup.string()
        .required(this.state.website_lang.display_error.login_register.email)
        .email(
          this.state.website_lang.display_error.login_register.valid_email
        ),
      password: Yup.string().required(
        this.state.website_lang.display_error.login_register.password
      ),
    });

    return this.props.showLoader === true || this.state.website_lang === "" ? (
      <div className="loginLoader">
        <div className="lds-spinner">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <div></div>
          <span>Please Wait…</span>
        </div>
      </div>
    ) : (
        <div className="outside bg-white">
          <div className="row">
            <div className="col-md-6">
              <div className="row h-100 justify-content-center align-items-center">
                <div className="login-form-sec">
                  <div className="login-form-box mx-auto text-center">
                    <div className="login-form-wrapper">
                      <div className="logo-login">
                        <a
                          href={process.env.REACT_APP_PORTAL_URL}
                          target="_blank"
                        >
                          <img
                            src={require("../../assets/images/logo-blue.jpg")}
                          />
                        </a>
                      </div>
                      <div className="logo-base">
                        <img
                          src={require("../../assets/images/Xceed_Logo_Purple_RGB.png")}
                        />
                      </div>
                      <h2>{this.state.website_lang.login.title}</h2>
                      <div className="clearfix" />
                    </div>
                    {message !== null && message !== "" && message !== undefined && (
                      <div className="messagessuccess">
                        <Link to="#" className="close">
                          <img
                            src={closeIconSuccess}
                            width="17.69px"
                            height="22px"
                            onClick={(e) => this.hideSuccessMsg()}
                          />
                        </Link>
                        {message}
                      </div>
                    )}
                    <Formik
                      initialValues={initialValues}
                      validationSchema={loginvalidation}
                      onSubmit={this.handleSubmit}
                    >
                      {({
                        values,
                        errors,
                        isValid,
                        touched,
                        isSubmitting,
                        setErrors,
                      }) => {
                        return (
                          <Form>
                            <div
                              className="messageserror"
                              style={{
                                display:
                                  this.state.errMsg &&
                                    this.state.errMsg !== null &&
                                    this.state.errMsg !== ""
                                    ? "block"
                                    : "none",
                              }}
                            >
                              <Link to="#" className="close">
                                <img
                                  src={closeIcon}
                                  width="17.69px"
                                  height="22px"
                                  onClick={(e) => this.removeError(setErrors)}
                                />
                              </Link>

                              {this.state.errStatusCode === 4 ? (
                                <div>
                                  <span className="errorMsg">
                                    {
                                      this.state.website_lang.login
                                        .password_expired
                                    }{" "}
                                    <a
                                      target="_blank"
                                      href={`${process.env.REACT_APP_PORTAL_URL}customer_portal/password_expired/`}
                                    >
                                      {this.state.website_lang.login.here}
                                    </a>{" "}
                                    {this.state.website_lang.login.reset_password}
                                  </span>
                                </div>
                              ) : (
                                  <div>
                                    <span className="errorMsg">
                                      {this.state.errMsg}{" "}
                                      <Link to="forgot_password/">
                                        {this.state.website_lang.login.f_password}
                                      </Link>
                                    </span>
                                  </div>
                                )}
                            </div>
                            <div className="login-form-wrapper">
                              <div className="row">
                                <div className="col-md-12">
                                  <Field
                                    name="username"
                                    type="text"
                                    className="form-control customInput"
                                    placeholder={
                                      this.state.website_lang.login.plh_username
                                    }
                                  />
                                  {errors.username && touched.username ? (
                                    <span className="errorMsg">
                                      {errors.username}
                                    </span>
                                  ) : null}
                                </div>
                                <div className="col-md-12">
                                  <Field
                                    name="password"
                                    type="password"
                                    className="form-control customInput"
                                    placeholder={
                                      this.state.website_lang.login.plh_password
                                    }
                                  />
                                  {errors.password && touched.password ? (
                                    <span className="errorMsg">
                                      {errors.password}
                                    </span>
                                  ) : null}
                                </div>
                              </div>
                              <div className="row">
                                <div className="col text-center">
                                  <Button
                                    type="submit"
                                    className="btn btn-default btn-secondary btn-lg"
                                  >
                                    {isSubmitting
                                      ? this.state.website_lang.login.logging
                                      : this.state.website_lang.login.login}
                                  </Button>
                                </div>
                              </div>
                              <div className="clearfix" />
                            </div>
                          </Form>
                        );
                      }}
                    </Formik>

                    <div className="row">
                      <div className="col-md-12">
                        <p>
                          <Link to="forgot_password/">
                            {this.state.website_lang.login.f_password}
                          </Link>
                          <br />
                          {this.state.website_lang.login.n_register}
                          <Link to="register/">
                            {" "}
                            {this.state.website_lang.login.signup}
                          </Link>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="col-md-6">
              <div className="login-graphic-area">
                <div className="login-graphic-img">
                  <img
                    src={require("../../assets/images/login-graphic-img.jpg")}
                  />
                </div>
              </div>
            </div>
            <div className="clearfix" />
          </div>

          <div
            className="sliding-popup-bottom"
            style={{
              display:
                (this.state.name === null ||
                  this.state.name === "" ||
                  this.state.name === undefined) &&
                  this.state.deniedPermissionCookies === false
                  ? "block"
                  : "none",
            }}
          >
            <div className="eu-cookie-compliance-banner eu-cookie-compliance-banner-info">
              <div className="popup-content info eu-cookie-compliance-content">
                <div id="popup-text" className="eu-cookie-compliance-message">
                  <h2>{this.state.website_lang.cookie.title}</h2>
                  <p>{this.state.website_lang.cookie.content}</p>
                  <a
                    className="find-more-button eu-cookie-compliance-more-button find-more-button-processed"
                    target="_blank"
                    href={`${process.env.REACT_APP_PORTAL_URL}cookie-policy`}
                  >
                    {this.state.website_lang.cookie.m_info}
                  </a>
                  {/* <button
                  type="button"
                  className="find-more-button eu-cookie-compliance-more-button find-more-button-processed"
                  onClick
                >
                  More info
                </button> */}
                </div>
                <div id="popup-buttons" className="eu-cookie-compliance-buttons">
                  <button
                    type="button"
                    className="agree-button eu-cookie-compliance-secondary-button"
                    onClick={(e) => this.setCookies()}
                  >
                    {this.state.website_lang.cookie.agree}
                  </button>
                  <button
                    type="button"
                    className="decline-button eu-cookie-compliance-default-button"
                    onClick={(e) => this.deniedCookiesStore()}
                  >
                    {this.state.website_lang.cookie.thanks}
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
  }
}

const mapStateToProps = (state) => {
  //console.log('redux state', state)
  return {
    isLoggedIn: state.auth.token !== null ? true : false,
    token: state.auth.token,
    showLoader: state.auth.showLoader,
    showLoader: state.auth.showLoader,
    dynamic_lang: state.auth.dynamic_lang,
    errors: state.auth.errors,
    tour_counter_11: state.auth.tour_counter_11
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    submitLogin: (data, onSuccess, onError) =>
      dispatch(portalLogin(data, onSuccess, onError)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withCookies(Login));
