import React, { Component } from 'react';
import Layout from '../Layout/layout';
import { Button, Popover, UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap';
import infoIcon from '../../assets/images/info-circle-solid.svg';
class Products extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      popoverOpen: false
    };
  }

    

  toggle() {
    this.setState({
      popoverOpen: !this.state.popoverOpen
    });
  }
  render() { 
    //console.log('page 01')
    return (
        <div className="container-fluid clearfix">
          <div className="dashboard-content-sec">
            <div className="service-request-form-sec">
              <div  className="form-page-title-block">
                <h2>General Request</h2>
              </div>
              <div className="row">
                <div className="col-md-4">
                  <select className="form-control customeSelect" >
                    <option></option>
                  </select>
                </div>
                <div className="col-md-4">
                  <select className="form-control customeSelect" >
                    <option></option>
                  </select>
                </div>
                <div className="col-md-4">
                  <input type="text" className="form-control customInput" />
                </div>
              </div>
              <div className="row">
                <div className="col-md-12">
                  <textarea name="description" rows="5" cols="60" placeholder="Enter Any Other Requirements" className="customTextarea"></textarea>
                </div>
              </div>
              <div className="row">
                <div className="col-md-4">
                  <input type="file" className="customFile" />
                </div>
                <div className="col-md-8 my-auto">
                  <div className="form-check-inline">
                    <label className="form-check-label">
                      <input type="checkbox" className="form-check-input" value="" />
                      Option 1 
                    </label>
                  </div>
                  <div className="form-check-inline">
                    <label className="form-check-label">
                      <input type="checkbox" className="form-check-input" value="" />
                      Option 2 
                    </label>
                  </div>
                  <input  type="submit"  value="Remove selected" className="button" />
                </div>
              </div>
              <div className="row">
                <div className="col-md-12"> <span className="fieldset-legend">Grant access for this request<img src={infoIcon} width="20" height="20" id="UncontrolledPopover" type="button" />
                  <UncontrolledPopover placement="right" target="UncontrolledPopover">
                    <PopoverHeader>Popover Title</PopoverHeader>
                    <PopoverBody>Select users within your organisation who can view and take action on this request.</PopoverBody>
                  </UncontrolledPopover>

                  </span>
                  <div className="form-check">
                    <label className="form-check-label">
                      <input type="checkbox" className="form-check-input" value="" />
                      Option 1 
                    </label>
                  </div>
                  <div className="form-check">
                    <label className="form-check-label">
                      <input type="checkbox" className="form-check-input" value="" />
                      Option 2 
                    </label>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col text-center">
                  <button className="btn btn-default">Submit</button>
                  <button className="btn btn-default btn-cancel">Cancel</button>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}
 
export default Products;