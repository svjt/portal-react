import React, { Component } from "react";
// import dateFormat from "dateformat";
// import API from "../../shared/axios";
// import Select from "react-select";
import { Link } from "react-router-dom";
// import Layout from "../Layout/layout";
import { htmlDecode, BasicUserData } from "../../shared/helper";
import ReactHtmlParser from "react-html-parser";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
} from "reactstrap";
import { Modal } from "react-bootstrap";
import classnames from "classnames";
import Discussion from "./../Discussion/discussion";
import DiscussionFile from "./../Discussion/discussionFile";
import { showErrorMessage } from "../../shared/handle_error";
import { Tooltip } from "reactstrap";
import infoIcon from "../../assets/images/download-new.svg";
import Loader from "../Loader/loader";
import { Formik, Form } from "formik";

//SATYAJIT
import { connect } from "react-redux";
import { fetchUserDiscussion } from "../../store/actions/discussion";

import { getTStatus } from "../../store/actions/user";

//SATYAJIT

const task_download_all = `${process.env.REACT_APP_API_URL}tasks/download_all/`; // SVJT

class TaskDetails extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.toggleTooltip = this.toggleTooltip.bind(this);
    this.state = {
      taskDetails: [],
      taskDetailsFiles: [],
      discussionFiles: [],
      invalid_access: false,
      status_active: [],
      task_id: this.props.match.params.id,
      activeTab:
        this.props.location.state &&
        this.props.location.state.fromDashboard === true
          ? "2"
          : "1",
      showBehalf: false,
      submittedBy: false,
      order_status: [],
      other_status: [],
      complaint_status: [],
      detailsErr: null,
      showLoader: false,
      showReportIssue: false,
      tooltipOpen: false,
      is_report: false,
      isLoading: true,
      task_language_explicit:''
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.task_id !== nextProps.match.params.id) {
      return {
        task_id: nextProps.match.params.id,
        activeTab: nextProps.location.state.fromDashboard === true ? "2" : "1",
      };
    }

    // Return null to indicate no change to state.
    return null;
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  toggleTooltip = () => {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    });
  };

  componentDidMount() {
    let param_query = this.props.location.search;
    if (param_query === "?display=files") {
      this.toggle("3");
    }

    if (param_query === "?display=discussions") {
      this.toggle("2");
    }

    this.setState({ showLoader: true });

    this.props.getDiscussion(`${this.state.task_id}`, (result) => {
      if (this.props.detailsErr) {
        document.title = `Not Found | Dr. Reddy's API`;
        var errText = "Invalid Access To This Page.";
        this.setState({ invalid_access: true, showLoader: false });
        if (this.props.detailsErr.status === 5) {
          showErrorMessage(this.props.detailsErr.errors);
        }
      } else {

        

        let lang_data = require(`../../assets/lang/${this.props.discussion.taskDetails.language}.json`);

        this.setState({task_language_explicit:lang_data});


        let post_arr = {
          type: "G",
          lang: this.props.discussion.taskDetails.language,
        };
        if (
          this.props.discussion.taskDetails.request_type == 23 ||
          this.props.discussion.taskDetails.request_type == 41
        ) {
          post_arr = {
            type: "N",
            lang: this.props.discussion.taskDetails.language,
          };
        } else if (
          this.props.discussion.taskDetails.request_type == 7 ||
          this.props.discussion.taskDetails.request_type == 34
        ) {
          post_arr = {
            type: "C",
            lang: this.props.discussion.taskDetails.language,
          };
        }

        this.props.getTaskStatus(post_arr, (res_arr) => {
          if (
            this.props.discussion.taskDetails.request_type == 23 ||
            this.props.discussion.taskDetails.request_type == 41
          ) {
            this.setState({ order_status: this.props.task_status_arr });
          } else if (
            this.props.discussion.taskDetails.request_type == 7 ||
            this.props.discussion.taskDetails.request_type == 34
          ) {
            this.setState({ complaint_status: this.props.task_status_arr });
          } else {
            this.setState({ other_status: this.props.task_status_arr });
          }

          let userData = BasicUserData();

          document.title = `${this.props.discussion.taskDetails.task_ref} | Dr. Reddy's API`;

          this.setState({ taskDetails: this.props.discussion.taskDetails });
          this.setState({
            taskDetailsFiles: this.props.discussion.taskDetailsFiles,
          });
          this.setState({
            discussionFiles: this.props.discussion.discussionFiles,
          });
          this.setState({
            downloadAllLink: this.props.discussion.downloadAllLink,
          });

          if (
            userData.role === 2 &&
            this.props.discussion.taskDetails.agent_id > 0
          )
            this.setState({ showBehalf: true });

          if (
            this.state.taskDetails.submitted_by &&
            parseInt(this.state.taskDetails.submitted_by) > 0
          )
            this.setState({ showBehalf: true });

          let active_stat = null;
          let push_stat = [];
          if (
            this.props.discussion.taskDetails.status_details !== null &&
            (this.props.discussion.taskDetails.request_type === 23 ||
              this.props.discussion.taskDetails.request_type === 41)
          ) {
            active_stat = this.props.task_status_arr.indexOf(
              this.props.discussion.taskDetails.status_details
            );
            for (let index = 0; index <= active_stat; index++) {
              push_stat.push(this.props.task_status_arr[index]);
            }
          } else if (
            this.props.discussion.taskDetails.status_details != null &&
            (this.props.discussion.taskDetails.request_type == 7 ||
              this.props.discussion.taskDetails.request_type == 34)
          ) {
            active_stat = this.props.task_status_arr.indexOf(
              this.props.discussion.taskDetails.status_details
            );
            for (let index = 0; index <= active_stat; index++) {
              push_stat.push(this.props.task_status_arr[index]);
            }
          } else {
            if (this.props.discussion.taskDetails.request_type !== 24) {
              active_stat = this.props.task_status_arr.indexOf(
                this.props.discussion.taskDetails.status_details
              );
              for (let index = 0; index <= active_stat; index++) {
                push_stat.push(this.props.task_status_arr[index]);
              }
            }
          }
          this.setState({ status_active: push_stat, showLoader: false });
        });
      }
    });
    let userData = BasicUserData();
    if (userData) {
      this.setState({
        isLoading: false,
      });
    }
  }

  displayRequestDetails = () => {
    return (
      <div>
        <div className="navSec">
          <h1 className="field-content">
            Stock Enquiry | Amlodipine Besylate
            | PHL-O-6560
          </h1>
          <Nav tabs>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === "1" })}
                onClick={() => {
                  this.toggle("1");
                }}
              >
                {this.state.task_language_explicit.task_details.details}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === "2" })}
                onClick={() => {
                  this.toggle("2");
                }}
              >
                {this.state.task_language_explicit.task_details.discussion}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === "3" })}
                onClick={() => {
                  this.toggle("3");
                }}
              >
                {this.state.task_language_explicit.task_details.files}
              </NavLink>
            </NavItem>
          </Nav>
          <div className="clearfix"></div>
        </div>
        <div className="clearfix"></div>
        <div className="requestDetails">
          <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="1">
              <Row>
                <Col sm="12">
                  <div className="views-row">
                    <div className="views-field views-field-title">
                      <strong className="views-label views-label-title">
                      Request Number:
                      </strong>
                      <div className="field-content">
                      PHL-O-6560
                      </div>
                    </div>

                    
                        <div className="views-field views-field-field-request-title">
                          <strong className="views-label views-label-field-request-title">
                          Request Title:
                          </strong>
                          <div className="field-content">
                          Stock Enquiry
                          </div>
                        </div>

                    
                      <div className="views-field views-field-field-product-name">
                        <strong className="views-label views-label-field-product-name">
                        Product:
                        </strong>
                        <div className="field-content">
                        Amlodipine Besylate
                        </div>
                      </div>

                    {this.state.taskDetails.request_type === 24 && (
                      <div className="views-field views-field-field-product-name">
                        <strong className="views-label views-label-field-product-name">
                          {
                            this.state.task_language_explicit.task_details
                              .notification_date
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {" "}
                          {this.state.taskDetails.date_added}{" "}
                        </div>
                      </div>
                    )}

                    
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                          Market:
                          </strong>
                          <div className="field-content">
                          Montserrat
                          </div>
                        </div>

                    {this.state.taskDetails.request_type === 40 && (
                      <div className="views-field views-field-field-product-name">
                        <strong className="views-label views-label-field-product-name">
                          {
                            this.state.task_language_explicit.task_details
                              .gmp_clearance_id
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {" "}
                          {this.state.taskDetails.gmp_clearance_id}{" "}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 40 && (
                      <div className="views-field views-field-field-product-name">
                        <strong className="views-label views-label-field-product-name">
                          {this.state.task_language_explicit.task_details.email_id}{" "}
                        </strong>
                        <div className="field-content">
                          {" "}
                          {this.state.taskDetails.tga_email_id}{" "}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 40 && (
                      <div className="views-field views-field-field-product-name">
                        <strong className="views-label views-label-field-product-name">
                          {
                            this.state.task_language_explicit.task_details
                              .applicants_address
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {" "}
                          {this.state.taskDetails.applicant_name}{" "}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 40 && (
                      <div className="views-field views-field-field-product-name">
                        <strong className="views-label views-label-field-product-name">
                          {
                            this.state.task_language_explicit.task_details
                              .documents_required
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {" "}
                          {this.state.taskDetails.doc_required}{" "}
                        </div>
                      </div>
                    )}

                    {(this.state.taskDetails.request_type === 7 ||
                      this.state.taskDetails.request_type === 34) && (
                      <div className="views-field views-field-field-country">
                        <strong className="views-label views-label-field-country">
                          {this.state.task_language_explicit.task_details.batch_number}{" "}
                        </strong>
                        <div className="field-content">
                          {" "}
                          {this.state.taskDetails.batch_number}{" "}
                        </div>
                      </div>
                    )}

                    {(this.state.taskDetails.request_type === 7 ||
                      this.state.taskDetails.request_type === 34) && (
                      <div className="views-field views-field-field-country">
                        <strong className="views-label views-label-field-country">
                          {this.state.task_language_explicit.task_details.nature_of_issue}{" "}
                        </strong>
                        <div className="field-content">
                          {" "}
                          {this.state.taskDetails.nature_of_issue !== null &&
                          this.state.taskDetails.nature_of_issue !== ""
                            ? this.state.taskDetails.nature_of_issue
                            : "-"}{" "}
                        </div>
                      </div>
                    )}

                    {(this.state.taskDetails.request_type === 7 ||
                      this.state.taskDetails.request_type === 23 ||
                      this.state.taskDetails.request_type === 41 ||
                      this.state.taskDetails.request_type === 31 ||
                      this.state.taskDetails.request_type === 34) && (
                      <div className="views-field views-field-field-country">
                        <strong className="views-label views-label-field-country">
                          {" "}
                          {this.state.task_language_explicit.task_details.quantity}{" "}
                        </strong>
                        <div className="field-content">
                          {" "}
                          {this.state.taskDetails.quantity !== "" &&
                          this.state.taskDetails.quantity !== null
                            ? this.state.taskDetails.quantity
                            : "-"}{" "}
                        </div>
                      </div>
                    )}

                    {(this.state.taskDetails.request_type === 23 ||
                      this.state.taskDetails.request_type === 41 ||
                      this.state.taskDetails.request_type === 39 ||
                      this.state.taskDetails.request_type === 40) && (
                      <div className="views-field views-field-field-country">
                        <strong className="views-label views-label-field-country">
                          {this.state.taskDetails.request_type === 23 ||
                          this.state.taskDetails.request_type === 41
                            ? this.state.task_language_explicit.task_details
                                .requested_date_delivery
                            : this.state.task_language_explicit.task_details
                                .requested_deadline}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.rdd !== null &&
                          this.state.taskDetails.rdd !== ""
                            ? this.state.taskDetails.rdd
                            : "-"}{" "}
                        </div>
                      </div>
                    )}

                    <div className="views-field views-field-created">
                      <strong className="views-label views-label-created">
                        Quantity
                      </strong>
                      <div className="field-content">
                      500 Kgs
                      </div>
                    </div>

                    {this.state.taskDetails.request_type === 1 &&
                      this.state.taskDetails.service_request_type.indexOf(
                        "Samples"
                      ) !== -1 && (
                        <div className="views-field views-field-created">
                          <strong className="views-label views-label-created">
                            {this.state.task_language_explicit.task_details.samples}{" "}
                          </strong>
                          {this.state.taskDetails.number_of_batches !== null &&
                            this.state.taskDetails.number_of_batches !== "" && (
                              <div className="field-content">
                                {
                                  this.state.task_language_explicit.task_details
                                    .number_of_batches
                                }{" "}
                                {this.state.taskDetails.number_of_batches}
                              </div>
                            )}
                          {this.state.taskDetails.quantity !== null &&
                            this.state.taskDetails.quantity !== "" && (
                              <div className="field-content">
                                {this.state.task_language_explicit.task_details.quantity}{" "}
                                {this.state.taskDetails.quantity}
                              </div>
                            )}
                        </div>
                      )}

                    {this.state.taskDetails.request_type === 1 &&
                      this.state.taskDetails.service_request_type.indexOf(
                        "Working Standards"
                      ) !== -1 && (
                        <div className="views-field views-field-created">
                          <strong className="views-label views-label-created">
                            {
                              this.state.task_language_explicit.task_details
                                .working_standards
                            }{" "}
                          </strong>
                          <div className="field-content">
                            {this.state.task_language_explicit.task_details.impurities}{" "}
                            {this.state.taskDetails.working_quantity}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetails.request_type === 1 &&
                      this.state.taskDetails.service_request_type.indexOf(
                        "Impurities"
                      ) !== -1 && (
                        <div className="views-field views-field-created">
                          <strong className="views-label views-label-created">
                            {this.state.task_language_explicit.task_details.impurities}{" "}
                          </strong>
                          {this.state.taskDetails.impurities_quantity !==
                            null &&
                            this.state.taskDetails.impurities_quantity !==
                              "" && (
                              <div className="field-content">
                                {
                                  this.state.task_language_explicit.task_details
                                    .impurities_quantity
                                }{" "}
                                {this.state.taskDetails.impurities_quantity}
                              </div>
                            )}
                          {this.state.taskDetails.specify_impurity_required !==
                            null &&
                            this.state.taskDetails.specify_impurity_required !==
                              "" && (
                              <div className="field-content">
                                {
                                  this.state.task_language_explicit.task_details
                                    .impurities_required
                                }{" "}
                                {htmlDecode(
                                  this.state.taskDetails
                                    .specify_impurity_required
                                )}
                              </div>
                            )}
                        </div>
                      )}

                    {this.state.taskDetails.request_type === 1 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {
                            this.state.task_language_explicit.task_details
                              .service_request_type
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.service_request_type}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 1 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {
                            this.state.task_language_explicit.task_details
                              .shipping_address
                          }
                        </strong>
                        <div className="field-content">
                          {ReactHtmlParser(
                            htmlDecode(this.state.taskDetails.shipping_address)
                          )}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 9 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {
                            this.state.task_language_explicit.task_details
                              .audit_visit_date
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.request_audit_visit_date}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 9 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {this.state.task_language_explicit.task_details.site_name}{" "}
                        </strong>
                        <div className="field-content">
                          {htmlDecode(
                            this.state.taskDetails.audit_visit_site_name
                          )}
                        </div>
                      </div>
                    )}

                    
                      <div className="views-field views-field-field-pharmacopoeia">
                        <strong className="views-label views-label-field-pharmacopoeia">
                          RDD{" "}
                        </strong>
                        <div className="field-content">
                        04 Sep 2020
                        </div>
                      </div>
                    

                    {this.state.taskDetails.request_type === 39 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {this.state.task_language_explicit.task_details.dmf_cmp}{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.dmf_number &&
                            this.state.taskDetails.dmf_number !== null &&
                            htmlDecode(this.state.taskDetails.dmf_number)}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 27 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {this.state.task_language_explicit.task_details.dmf_number}{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.dmf_number &&
                            this.state.taskDetails.dmf_number !== null &&
                            htmlDecode(this.state.taskDetails.dmf_number)}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 28 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {
                            this.state.task_language_explicit.task_details
                              .requested_date_response_closure
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.rdfrc &&
                            this.state.taskDetails.rdfrc !== null &&
                            this.state.taskDetails.rdfrc}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 29 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {
                            this.state.task_language_explicit.task_details
                              .notification_number
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.notification_number &&
                            this.state.taskDetails.notification_number !==
                              null &&
                            htmlDecode(
                              this.state.taskDetails.notification_number
                            )}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 38 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {this.state.task_language_explicit.task_details.document_type}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.apos_document_type &&
                            this.state.taskDetails.apos_document_type !==
                              null &&
                            this.state.taskDetails.apos_document_type}
                        </div>
                      </div>
                    )}

                    {(this.state.taskDetails.request_type === 2 ||
                      this.state.taskDetails.request_type === 3) && (
                      <div className="views-field views-field-field-pharmacopoeia">
                        <strong className="views-label views-label-field-pharmacopoeia">
                          {
                            this.state.task_language_explicit.task_details
                              .polymorphic_form
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.polymorphic_form !== "" &&
                          this.state.taskDetails.polymorphic_form !== null
                            ? htmlDecode(
                                this.state.taskDetails.polymorphic_form
                              )
                            : "-"}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 13 && (
                      <div className="views-field views-field-field-pharmacopoeia">
                        <strong className="views-label views-label-field-pharmacopoeia">
                          {this.state.task_language_explicit.task_details.stability_type}{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.stability_data_type}
                        </div>
                      </div>
                    )}

                    {(this.state.taskDetails.request_type === 23 ||
                      this.state.taskDetails.request_type === 41) &&
                      this.state.taskDetails.status_details != null && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {" "}
                            {
                              this.state.task_language_explicit.task_details
                                .committed_date_delivery
                            }{" "}
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.close_status == 1
                              ? this.state.taskDetails.closed_date
                              : this.state.taskDetails.expected_closure_date}
                          </div>
                        </div>
                      )}

                    {(this.state.taskDetails.request_type == 7 ||
                      this.state.taskDetails.request_type === 34) &&
                      this.state.taskDetails.status_details != null && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {" "}
                            {
                              this.state.task_language_explicit.task_details.closure_date
                            }{" "}
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.close_status == 1
                              ? this.state.taskDetails.closed_date
                              : this.state.taskDetails.expected_closure_date}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetails.request_type !== 23 &&
                      this.state.taskDetails.request_type !== 24 &&
                      this.state.taskDetails.request_type !== 7 &&
                      this.state.taskDetails.request_type !== 34 &&
                      this.state.taskDetails.status_details !== null && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {" "}
                            {
                              this.state.task_language_explicit.task_details
                                .expected_closure_date
                            }{" "}
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.expected_closure_date
                              ? this.state.taskDetails.expected_closure_date
                              : "-"}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetails.request_type !== 23 &&
                      this.state.taskDetails.request_type !== 24 &&
                      this.state.taskDetails.request_type !== 7 &&
                      this.state.taskDetails.request_type !== 34 &&
                      this.state.taskDetails.status_details !== null &&
                      this.state.taskDetails.close_status == 1 && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {" "}
                            {
                              this.state.task_language_explicit.task_details
                                .actual_closure_date
                            }{" "}
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.closed_date}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetails.request_type != 24 &&
                      this.state.taskDetails.status_details != null && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {" "}
                            {this.state.task_language_explicit.task_details.status}{" "}
                          </strong>
                          <div className="field-content">
                            {" "}
                            {this.state.taskDetails.status_details}{" "}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetails.request_type != 24 &&
                      this.state.taskDetails.status_details != null && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {
                              this.state.task_language_explicit.task_details
                                .action_requested
                            }
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.required_action
                              ? this.state.taskDetails.required_action
                              : "-"}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetailsFiles.length > 0 && (
                      <div className="views-field views-field-field-attachment">
                        <strong className="views-label views-label-field-attachment">
                          {this.state.task_language_explicit.task_details.attachment}
                          {this.state.taskDetailsFiles.length > 1 ? `s ` : ``}
                          {this.state.taskDetailsFiles.length > 1 &&
                            this.state.downloadAllLink != "" && (
                              <>
                                <a href={`${this.state.downloadAllLink}`}>
                                  <img
                                    src={infoIcon}
                                    width="28"
                                    height="28"
                                    id="DisabledAutoHideExample"
                                    type="button"
                                  />
                                  <Tooltip
                                    placement="right"
                                    isOpen={this.state.tooltipOpen}
                                    autohide={false}
                                    target="DisabledAutoHideExample"
                                    toggle={this.toggleTooltip}
                                  >
                                    {
                                      this.state.task_language_explicit.task_details
                                        .click_download_all_files
                                    }
                                  </Tooltip>
                                </a>
                              </>
                            )}{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetailsFiles
                            .map((file, p) => {
                              return (
                                <span className="file  file-image" key={p}>
                                  <a href={file.url}>{file.actual_file_name}</a>
                                </span>
                              );
                            })
                            .reduce((prev, curr) => [prev, ", ", curr])}
                        </div>
                      </div>
                    )}

                    {this.state.showBehalf == true ? (
                      <div className="views-field views-field-body">
                        <strong className="views-label views-label-body">
                          {
                            this.state.task_language_explicit.task_details
                              .submitted_behalf
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.first_name +
                            " " +
                            this.state.taskDetails.last_name}
                        </div>
                      </div>
                    ) : (
                      ""
                    )}

                    {this.state.submittedBy == true ? (
                      <div className="views-field views-field-body">
                        <strong className="views-label views-label-body">
                          {this.state.task_language_explicit.task_details.submitted_by}{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.posted_by_first_name +
                            " " +
                            this.state.taskDetails.posted_by_last_name}
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                    <div className="clearfix"></div>

                    <div className="views-field views-field-body">
                      <strong className="views-label views-label-body">
                        Logged Date
                      </strong>
                      <div className="field-content">
                      03 Sep 2020
                      </div>
                    </div>
                    {/* <div className="views-field views-field-body">
                    <strong className="views-label views-label-body">
                        Quantity
                      </strong>
                      <div className="field-content">
                      100 Kgs
                      </div>
                    </div> */}
                  </div>
                </Col>
              </Row>
            </TabPane>
            <TabPane tabId="2">
              <Discussion is_report={this.state.is_report} />
            </TabPane>
            <TabPane tabId="3">
              <DiscussionFile />
            </TabPane>
          </TabContent>
          <div className="clearfix"></div>
        </div>
      </div>
    );
  };

  displayPayments = () => {
    return (
      <div>
        <p> {this.state.task_language_explicit.task_details.not_done} </p>
      </div>
    );
  };

  displayNotification = () => {
    return (
      <div>
        <p> {this.state.task_language_explicit.task_details.not_done} </p>
      </div>
    );
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (this.props.selCompany !== prevProps.selCompany) {
      this.props.history.push("/dashboard");
    }

    let update_comp = false;
    // ============================== //
    if (this.props.match.params.id !== prevState.task_id) {
      update_comp = true;
    } else if (this.props.match.params.id === prevState.task_id) {
      if (this.props.location.state) {
        let currentActiveTab =
          this.props.location.state.fromDashboard === true ? "2" : "1"; // value can be true | false
        let previousActiveTab = prevState.activeTab; // value can be 1 | 2
        if (currentActiveTab !== previousActiveTab) {
          update_comp = true;
        }
      }
    }

    if (update_comp) {
      this.props.getDiscussion(`${this.props.match.params.id}`, () => {
        if (this.props.detailsErr) {
          document.title = `Not Found | Dr. Reddy's API`;
          var errText = "Invalid Access To This Page.";
          this.setState({ invalid_access: true, showLoader: false });
          if (this.props.detailsErr.status === 5) {
            showErrorMessage(this.props.detailsErr.errors);
          }
        } else {
          let userData = BasicUserData();

          this.setState({ taskDetails: this.props.discussion.taskDetails });
          this.setState({
            taskDetailsFiles: this.props.discussion.taskDetailsFiles,
          });
          this.setState({
            discussionFiles: this.props.discussion.discussionFiles,
          });

          if (
            userData.role === 2 &&
            this.props.discussion.taskDetails.agent_id > 0
          )
            this.setState({ showBehalf: true });

          let active_stat = null;
          let push_stat = [];
          if (
            this.props.discussion.taskDetails.status_details !== null &&
            (this.props.discussion.taskDetails.request_type === 23 ||
              this.props.discussion.taskDetails.request_type === 41)
          ) {
            active_stat = this.state.order_status.indexOf(
              this.props.discussion.taskDetails.status_details
            );
            for (let index = 0; index <= active_stat; index++) {
              push_stat.push(this.state.order_status[index]);
            }
          } else if (
            this.props.discussion.taskDetails.status_details != null &&
            (this.props.discussion.taskDetails.request_type == 7 ||
              this.state.taskDetails.request_type === 34)
          ) {
            active_stat = this.state.complaint_status.indexOf(
              this.props.discussion.taskDetails.status_details
            );
            for (let index = 0; index <= active_stat; index++) {
              push_stat.push(this.state.complaint_status[index]);
            }
          } else {
            if (this.props.discussion.taskDetails.request_type !== 24) {
              active_stat = this.state.other_status.indexOf(
                this.props.discussion.taskDetails.status_details
              );
              for (let index = 0; index <= active_stat; index++) {
                push_stat.push(this.state.other_status[index]);
              }
            }
          }
          this.setState({
            status_active: push_stat,
            activeTab:
              this.props.location.state &&
              this.props.location.state.fromDashboard === true
                ? "2"
                : "1",
          });
        }
      });
    }
  };

  openReportPopup = () => {
    this.setState({ showReportIssue: true, is_report: true });
  };

  handleClose = () => {
    this.setState({ showReportIssue: false, is_report: false });
  };

  closeDiscussPopup = () => {
    this.setState({ showReportIssue: false, is_report: false });
  };

  render() {
    if (this.state.isLoading === true || this.state.task_language_explicit === '') {
      return (
        <>
          <div className="loginLoader">
            <div className="lds-spinner">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <span>Please Wait…</span>
            </div>
          </div>
        </>
      );
    } else {
      if (this.state.showLoader) {
        return <Loader />;
        // return <h2>Loading....</h2>
      } else {
        return (
          <div className="container-fluid clearfix taskDetailsSec">
            <div className="dashboard-content-sec">
              <div className="product-content-sec">
                <div className="row breadcrumb-area">
                  <div className="col-6">
                    <div className="breadcrumb">
                      <ul>
                        <li>
                          {" "}
                          <Link to="/">
                            {this.state.task_language_explicit.task_details.home}
                          </Link>{" "}
                        </li>
                        <li>
                          {" "}
                          {this.state.task_language_explicit.task_details.dashboard}{" "}
                        </li>
                        {this.state.taskDetails.request_type === 7 ||
                        this.state.taskDetails.request_type === 34 ? (
                          <li>
                            <Link to="/my_complaints">
                              {this.state.task_language_explicit.task_details.complaints}
                            </Link>{" "}
                          </li>
                        ) : this.state.taskDetails.request_type === 23 ||
                          this.state.taskDetails.request_type === 41 ? (
                          <li>
                            <Link to="/my_orders">
                              {this.state.task_language_explicit.task_details.orders}
                            </Link>{" "}
                          </li>
                        ) : this.state.taskDetails.request_type === 24 ? (
                          <li>
                            <Link to="/my_forecast">
                              {this.state.task_language_explicit.task_details.forecast}
                            </Link>{" "}
                          </li>
                        ) : (
                          <li>
                            <Link to="#">
                            My Stock Enquiries / Orders
                            </Link>{" "}
                          </li>
                        )}
                        <li> PHL-O-6560 </li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-6 float-right">
                    <p className="text-right">
                      {this.state.taskDetails.duplicate_task !== 1 && (
                        <Link
                          to="#"
                          className="back-btn"
                          onClick={() => this.openReportPopup()}
                        >
                          {this.state.task_language_explicit.task_details.cancel_request}
                        </Link>
                      )}

                      {this.state.taskDetails.request_type === 7 ||
                      this.state.taskDetails.request_type === 34 ? (
                        <Link
                          className="back-btn"
                          to={{ pathname: `/my_complaints` }}
                          style={{ cursor: "pointer" }}
                          title={
                            this.state.task_language_explicit.task_details
                              .previous_page_viewed
                          }
                        >
                          {" "}
                          {this.state.task_language_explicit.task_details.back}{" "}
                        </Link>
                      ) : this.state.taskDetails.request_type === 23 ||
                        this.state.taskDetails.request_type === 41 ? (
                        <Link
                          className="back-btn"
                          to={{ pathname: `/my_orders` }}
                          style={{ cursor: "pointer" }}
                          title={
                            this.state.task_language_explicit.task_details
                              .previous_page_viewed
                          }
                        >
                          {" "}
                          {this.state.task_language_explicit.task_details.back}{" "}
                        </Link>
                      ) : this.state.taskDetails.request_type === 24 ? (
                        <Link
                          className="back-btn"
                          to={{ pathname: `/my_forecast` }}
                          style={{ cursor: "pointer" }}
                          title={
                            this.state.task_language_explicit.task_details
                              .previous_page_viewed
                          }
                        >
                          {" "}
                          {this.state.task_language_explicit.task_details.back}{" "}
                        </Link>
                      ) : (
                        <Link
                          className="back-btn"
                          to={{ pathname: `/my_requests` }}
                          style={{ cursor: "pointer" }}
                          title={
                            this.state.task_language_explicit.task_details
                              .previous_page_viewed
                          }
                        >
                          {" "}
                          {this.state.task_language_explicit.task_details.back}{" "}
                        </Link>
                      )}
                    </p>
                  </div>
                  <div className="clearfix"></div>

                  {/* status_active */}

                  {(this.state.taskDetails.request_type === 23 ||
                    this.state.taskDetails.request_type === 41) &&
                    this.state.taskDetails.status_details != null && (
                      <ul className="progressbar">
                        {this.state.order_status
                          ? this.state.order_status.map((player, i) => {
                              return (
                                <li
                                  key={i}
                                  className={
                                    this.state.status_active &&
                                    this.state.status_active.length > 0 &&
                                    this.state.status_active.includes(player)
                                      ? "active"
                                      : ""
                                  }
                                >
                                  {player}
                                </li>
                              );
                              // -------------------^^^^^^^^^^^---------^^^^^^^^^^^^^^
                            })
                          : ""}
                      </ul>
                    )}

                  {/* {(this.state.taskDetails.request_type === 41) && this.state.taskDetails.request_type !== 24 && this.state.taskDetails.status_details != null &&
                        <ul className="progressbar">
                        {this.state.other_status ? this.state.other_status.map((player,i) => {
                          
                          return <li key={i} className={(this.state.status_active && this.state.status_active.length > 0 && this.state.status_active.includes(player)) ? 'active' : '' }>{player}</li>
                          // -------------------^^^^^^^^^^^---------^^^^^^^^^^^^^^
                        }) : ''}
                        </ul>
                      } */}
                  {(this.state.taskDetails.request_type === 7 ||
                    this.state.taskDetails.request_type === 34) &&
                    this.state.taskDetails.status_details != null && (
                      <ul className="progressbar">
                        {this.state.complaint_status
                          ? this.state.complaint_status.map((player, i) => {
                              return (
                                <li
                                  key={i}
                                  className={
                                    this.state.status_active &&
                                    this.state.status_active.length > 0 &&
                                    this.state.status_active.includes(player)
                                      ? "active"
                                      : ""
                                  }
                                >
                                  {player}
                                </li>
                              );
                              // -------------------^^^^^^^^^^^---------^^^^^^^^^^^^^^
                            })
                          : ""}
                      </ul>
                    )}

                  {this.state.taskDetails.request_type !== 7 &&
                    this.state.taskDetails.request_type !== 34 &&
                    this.state.taskDetails.request_type !== 23 &&
                    this.state.taskDetails.request_type !== 41 &&
                    this.state.taskDetails.request_type !== 24 &&
                    this.state.taskDetails.status_details != null && (
                      <ul className="progressbar">
                        {this.state.other_status
                          ? this.state.other_status.map((player, i) => {
                              return (
                                <li
                                  key={i}
                                  className={
                                    this.state.status_active &&
                                    this.state.status_active.length > 0 &&
                                    this.state.status_active.includes(player)
                                      ? "active"
                                      : ""
                                  }
                                >
                                  {player}
                                </li>
                              );
                              // -------------------^^^^^^^^^^^---------^^^^^^^^^^^^^^
                            })
                          : ""}
                      </ul>
                    )}

                  {/* player ==='Acknowledge' ? 'Acknowledged' : player */}
                </div>

                {(this.state.taskDetails.request_type === 2 ||
                  this.state.taskDetails.request_type === 3 ||
                  this.state.taskDetails.request_type === 4 ||
                  this.state.taskDetails.request_type === 5 ||
                  this.state.taskDetails.request_type === 6 ||
                  this.state.taskDetails.request_type === 7 ||
                  this.state.taskDetails.request_type === 8 ||
                  this.state.taskDetails.request_type === 9 ||
                  this.state.taskDetails.request_type === 10 ||
                  this.state.taskDetails.request_type === 11 ||
                  this.state.taskDetails.request_type === 12 ||
                  this.state.taskDetails.request_type === 13 ||
                  this.state.taskDetails.request_type === 14 ||
                  this.state.taskDetails.request_type === 15 ||
                  this.state.taskDetails.request_type === 16 ||
                  this.state.taskDetails.request_type === 17 ||
                  this.state.taskDetails.request_type === 18 ||
                  this.state.taskDetails.request_type === 19 ||
                  this.state.taskDetails.request_type === 20 ||
                  this.state.taskDetails.request_type === 21 ||
                  this.state.taskDetails.request_type === 22 ||
                  this.state.taskDetails.request_type === 23 ||
                  this.state.taskDetails.request_type === 24 ||
                  this.state.taskDetails.request_type === 1 ||
                  this.state.taskDetails.request_type === 27 ||
                  this.state.taskDetails.request_type === 28 ||
                  this.state.taskDetails.request_type === 29 ||
                  this.state.taskDetails.request_type === 30 ||
                  this.state.taskDetails.request_type === 31 ||
                  this.state.taskDetails.request_type === 32 ||
                  this.state.taskDetails.request_type === 33 ||
                  this.state.taskDetails.request_type === 34 ||
                  this.state.taskDetails.request_type === 35 ||
                  this.state.taskDetails.request_type === 36 ||
                  this.state.taskDetails.request_type === 37 ||
                  this.state.taskDetails.request_type === 38 ||
                  this.state.taskDetails.request_type === 39 ||
                  this.state.taskDetails.request_type === 40 ||
                  this.state.taskDetails.request_type === 41 ||
                  this.state.taskDetails.request_type === 42) &&
                  this.displayRequestDetails()}

                {this.state.taskDetails.request_type === 25
                  ? this.displayPayments()
                  : ""}
                {this.state.taskDetails.request_type === 26
                  ? this.displayNotification()
                  : ""}
              </div>
            </div>

            {/* report an issue */}
            {this.state.showReportIssue === true && (
              <Modal
                show={this.state.showReportIssue}
                onHide={this.handleClose}
                backdrop="static"
                className="grantmodal canclpopup"
              >
                <Formik
                  initialValues={false}
                  validationSchema={false}
                  onSubmit={this.handleSubmitAssignTask}
                >
                  {({
                    values,
                    errors,
                    touched,
                    isValid,
                    isSubmitting,
                    handleChange,
                    setFieldValue,
                    setFieldTouched,
                    setErrors,
                  }) => {
                    return (
                      <>
                        <Modal.Header closeButton>
                          <Modal.Title>
                            <span className="fieldset-legend">
                              {
                                this.state.task_language_explicit.task_details
                                  .cancel_request
                              }
                            </span>
                          </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                          <Discussion
                            is_report={this.state.is_report}
                            closeDiscussPopup={this.closeDiscussPopup}
                          />
                        </Modal.Body>
                        {/* <Modal.Footer></Modal.Footer> */}
                      </>
                    );
                  }}
                </Formik>
              </Modal>
            )}
            {/* report an issue */}
          </div>
        );
      }
    }
  }
}

const mapStateToProps = (state) => {
  return {
    discussion: state.discussion.discussion,
    task_status_arr: state.user.task_status_arr,
    detailsErr: state.discussion.errors ? state.discussion.errors.data : null,
    selCompany: state.user.selCompany,
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getDiscussion: (data, onSuccess) =>
      dispatch(fetchUserDiscussion(data, onSuccess)),
    getTaskStatus: (data, onSuccess) => dispatch(getTStatus(data, onSuccess)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskDetails);
