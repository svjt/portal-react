import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "../../shared/axios";
//import Layout from '../Layout/layout';
import {
  Button,
  Popover,
  UncontrolledPopover,
  PopoverHeader,
  PopoverBody
} from "reactstrap";
//import infoIcon from '../../Linkssets/images/info-circle-solid.svg';
import closeIcon from "../../assets/images/times-solid-red.svg";
import Layout from "../Layout/layout";

import { Formik, Field, Form } from "formik";
import Loader from "../Loader/loader";
import * as Yup from "yup";
import { connect } from "react-redux";
import { updateToken,getLanguage,featureLangAccess } from "../../store/actions/auth";


class shadowlogin extends Component {
  state = {
    token: "",
    showLoader: true,
    mainLoader: true,
    errMsg: null
  };
  
  componentDidMount() {
    this.setState({
      token: this.props.match.params.token
    });
    if(this.props.match.params.token)
    {
      this.props.updateAuthToken(this.props.match.params.token);
      axios.get(`/setToken`).then(res => {
       if(res.data.token)
       {
        this.props.updateAuthToken(res.data.token);
        this.props.setFeatureLangAccess(res.data.token);
        this.props.getLang(()=>{
          this.props.history.push({
            pathname: "/dashboard",
          });
        });
       }
      }).catch(error => {
        this.props.history.push({
          pathname: "/login",
        });
      });
    }
    
    //console.log("Token",this.props.match.params.token);
  }
  // state = {
  //   showLoader: false
  // };

  
  render() {
    //console.log('page 01')
    return (
      <div>
           {this.state.showLoader === true ? <Loader /> : null}
      </div>
    );
  }
}

const mapStateToProps = state => {
 
  return {
    token : state.auth.token !== null ? true : false,
    dynamic_lang:state.auth.dynamic_lang,
    new_feature_send_mail:1
  }
}

const mapDispatchToProps = dispatch => {
  return {
      updateAuthToken: ( token ) => dispatch(updateToken( token )),
      setFeatureLangAccess: (token) => dispatch(featureLangAccess( token )),
      getLang: (success) => dispatch(getLanguage(success))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(shadowlogin);
