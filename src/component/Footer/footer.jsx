import React, { Component } from "react";
import dateFormat from "dateformat";

import { BasicUserData } from "../../shared/helper";
import { connect } from "react-redux";

class Footer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
    };
  }

  componentDidMount = () => {
    let userData = BasicUserData();
    if (userData) {
      this.setState({
        isLoading: false,
      });
    }
  };

  render() {
    const { isLoggedIn } = this.props;
    if (!isLoggedIn) return null;

    let show = true;
    if(window.location.pathname === "/qa_login" || window.location.pathname === "/customer_portal/qa_login" || window.location.pathname === "/new_customer_portal/qa_login" || window.location.pathname === "/qa_form" || window.location.pathname === "/customer_portal/qa_form" || window.location.pathname === "/new_customer_portal/qa_form"){
        show = false
    }

    if(show){
      return (
        <footer className="footer ">
          <span>
            &copy; {dateFormat(new Date(), "yyyy")} Dr. Reddy’s Laboratories Ltd.{" "}
            <span>{this.props.dynamic_lang && this.props.dynamic_lang.footer.footer_text}</span>
            <span className="footerVersion">(v2.9.0)</span>
          </span>
        </footer>
      );
    }else{
      return null;
    }
  }
}

const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

//export default Footer;

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
