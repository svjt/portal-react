import React, { Component } from "react";
import { Link } from "react-router-dom";
import * as Yup from "yup";
import Select from "react-select";
import { Formik, Form, Field } from "formik";
import dateFormat from "dateformat";
import { Tooltip } from "reactstrap";
import DatePicker from "react-datepicker";
import { Editor } from "@tinymce/tinymce-react";
import "react-datepicker/dist/react-datepicker.css";
import tickIcon from "../../assets/images/green-tick.svg";
import closeIcon from "../../assets/images/times-solid-red.svg";
import infoIcon from "../../assets/images/info-circle-solid.svg";
import closeIconOuter from "../../assets/images/close-outer.svg";
import { Modal } from "react-bootstrap";

import { connect } from "react-redux";
import Loader from "../Loader/loader";
import { supportedPdfFileType, htmlDecode, BasicUserData } from "../../shared/helper";
import Dropzone from "react-dropzone";
import swal from "sweetalert";
import axios from "../../shared/axios";
var path = require("path");
var minDate = "";
var maxDate = "";

var product_list = [
];
var product_code_list = [];
var market_code_list = [];
var ship_list = [];
var sold_to_party_list = [];
var quantity_unit_list = [];
var cc_Customer = [];

class StockEnquiry extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //startDate: new Date(),
      startDate: "",
      startDateModal: "",
      errMsg: "",      
      showForm: true,
      showView: false,
      showCheckout: false,
      showLoader: false,
      showAddiSpec: false,
      checkEarly: false,
      share_with_agent: true,
      product_id: "",
      product_code: "",
      market_code: 0,
      ship_to: "",
      quantity: "",
      quantity_value: "",
      quantity_unit: "",
      quantity_unit_modal: "",
      specification: "",
      request_date: "",
      as_admin: 0,
      showModal: false,
      showModalInfo: false,
      showBlock: false,
      showAditionalBlock: false,
      showFullSupply1: 2,
      //showFullSupply2: false,
      buttonNext: false,
      sold_to_party: "",
      files: [],
      rejectedFile: [],
      line_items: [],
      line_items_date: [],
      deliveryDateDisabled: false,
      selectedRevisions: 0,
      withOutRevision: false,
      dynamic_stock_list: [],
      dynamic_stock_info_html: '',
      displayCCList: false,
      agent_customer_id: null,
      cc_Customers: [],
      pre_selected: [],
      tooltipOpen: false,
      customerList: [],
      po_err:'',
      po_no : ''
    };
  }

  componentDidMount() {
    
    if(this.props.show_stock_enquiry==false){
      this.props.history.push({
        pathname: "/dashboard",        
      });
    }
    
    minDate = new Date();
    //maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 10));
    maxDate = new Date(minDate.getFullYear(), minDate.getMonth() + 6, 0);
    product_code_list = [];
    market_code_list = [];
    
    //this.setState({ showLoader: true });
    //this.setState({ showAddPopup: true });
    this.getCountryList();
    this.getUnitList();

    let userData = BasicUserData();
    if (userData.role === 2) {
      this.setState({ displayCCList: true });
    } else {
      this.getShipToList();
      this.getCc_CustomerList();
    }
  }

  handleClose = () => {
    this.setState({
      showAddPopup: false,
    });
  };

  hideError = () => {
    this.setState({ errMsg: "" });
  };

  removeError = (setErrors) => {
    setErrors({});
  };

  removeFile_Error = () => {
    this.setState({ rejectedFile: [] });
  };

  showAdditionalSpec = () => {
    this.setState({
      showAddiSpec: !this.state.showAddiSpec,
    });
  };
  toggle = () => {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    });
  };

  showPrevious = () => {
    swal({
      closeOnClickOutside: false,
      title: this.props.dynamic_lang.stock_enquiry_new.alert,
      text:
        this.props.dynamic_lang.stock_enquiry_new.sure_to_go_back_reset,
      icon: "warning",
      buttons: true,
      dangerMode: true,
      customClass: 'customAlert',
    }).then((willDelete) => {
      if (willDelete) {
        this.setState({
          checkEarly: false,
          showLoader: false,
          showForm: true,
          showView: false,
          showCheckout: false,
          as_admin: 0
        });
      }
    });
  };

  showLastPrevious = () => {
    swal({
      closeOnClickOutside: false,
      title: this.props.dynamic_lang.stock_enquiry_new.alert,
      text:
      this.props.dynamic_lang.stock_enquiry_new.sure_to_go_back,
      icon: "warning",
      buttons: true,
      dangerMode: true,
      customClass: 'customAlert',
    }).then((willDelete) => {
      if (willDelete) {
        let prev_state = this.state.dynamic_stock_list; 
        var last_index = parseInt(prev_state.length - 1);
        prev_state[last_index].available_stock.map((stock, c) => {
          prev_state[last_index].available_stock[c].disabled = true;
        });
        prev_state[last_index].disabled = true;
        // console.log(prev_state);
        this.setState({ dynamic_stock_list: prev_state });

        this.setState({
          showLoader: false,
          showForm: false,
          showView: true,
          showCheckout: false,
          buttonNext: true,
          withOutRevision:true
        });
      }
    });
  };

  getCc_CustomerList() {
    axios
      .get("tasks/get-cc-customers")
      .then((res) => {
        cc_Customer = res.data.data;
        let pre_selected = res.data.pre_selected;
        this.setState({
          cc_Customers: cc_Customer,
          pre_selected: pre_selected,
        });
      })
      .catch((err) => {});
  }
  getProductList(shipto_id) { //alert(shipto_id);
    this.setState({ showLoader: true, errMsg: "" });
    product_list = [];
    axios
      .get("sap/customer_products/" + shipto_id)
      .then((res) => {
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          product_list.push({
            value: element["id"],
            label: htmlDecode(element["name"]),
          });
        }
        this.setState({ showLoader: false, errMsg: "" });
      })
      .catch((err) => {
        this.setState({ showLoader: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.unable_to_fetch_product });
      });
  };

  getCountryList() {
    this.setState({ showLoader: true, errMsg: "" });
    market_code_list = [];
    axios
      .get("/country")
      .then((res) => {
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          market_code_list.push({
            value: element["country_id"],
            label: htmlDecode(element["country_name"]),
          });
        }//console.log(market_code_list);
        this.setState({ showLoader: false, errMsg: "" });
      })
      .catch((err) => {
        this.setState({ showLoader: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.unable_to_fetch_country });
      });
  };
  getUnitList() {
    this.setState({ showLoader: true, errMsg: "" });
    quantity_unit_list = [];
    axios
      .get("/product_unit")
      .then((res) => {
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          quantity_unit_list.push({
            value: element["unit_id"],
            label: htmlDecode(element["unit_name"]),
          });
        }//console.log(quantity_unit_list);
        this.setState({ showLoader: false, errMsg: "" });
      })
      .catch((err) => {
        this.setState({ showLoader: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.unable_to_fetch_unit });
      });
  };
  getProductCodeList(product_id) {
    this.setState({ showLoader: true, errMsg: "" });
    product_code_list = [];
    axios
      .get("sap/customer_products_code/" + product_id)
      .then((res) => {
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          product_code_list.push({
            value: element["code"],
            label: htmlDecode(element["code"]),
          });
        }
        this.setState({ showLoader: false, errMsg: "" });
      })
      .catch((err) => {
        this.setState({ showLoader: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.unable_to_fetch_product_code });
      });
  };
  getShipToList( customer = '') {
    this.setState({ showLoader: true, errMsg: "" });
    ship_list = [];
    if(customer != ''){
      axios
        .post(`sap/customer_shipto`,{customer_id : customer})
        .then((res) => {
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            ship_list.push({
              value: element["code"],
              label: htmlDecode(element["name"]) + " - " + htmlDecode(element['city']) + " (" + htmlDecode(element['country']) + ")",
            });
          }
          this.setState({ showLoader: false, errMsg: "" });
        })
        .catch((err) => {
          this.setState({ showLoader: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.unable_to_fetch_ship_to });
        });
    }else{
      axios
        .post("sap/customer_shipto")
        .then((res) => {
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            ship_list.push({
              value: element["code"],
              label: htmlDecode(element["name"]) + " - " + htmlDecode(element['city']) + " (" + htmlDecode(element['country']) + ")",
            });
          }
          this.setState({ showLoader: false, errMsg: "" });
        })
        .catch((err) => {
          this.setState({ showLoader: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.unable_to_fetch_ship_to });
        });
    }
  };
  getSoldToList(shipto_id) {
    this.setState({ showLoader: true, errMsg: "" });
    axios
      .get("sap/customer_soldto/" + shipto_id)
      .then((res) => {
        this.setState({ showLoader: false, errMsg: "", sold_to_party_list: res.data.data });
      })
      .catch((err) => {
        this.setState({ showLoader: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.unable_to_fetch_ship_to_details });
      });
  };

  shareWithAgent = (event, setFieldValue) => {
    if (event.target.checked === true) {
      setFieldValue("share_with_agent", 1);
    } else {
      setFieldValue("share_with_agent", 0);
    }
  };

  customerChecked = (event) => {
    // console.log(event.target.checked);
    var tempArray = [];

    if (event.target.checked === true) {
      this.state.cc_Customers.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 1;
        }
        tempArray.push(user);
      });
    } else {
      this.state.cc_Customers.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 0;
        }
        tempArray.push(user);
      });
    }
    // console.log("tempArray", tempArray);
    this.setState({ cc_Customers: tempArray });
  };

  getAgentCCList = (event, setFieldValue) => {
    this.setState({ agent_customer_id: event });

    if (event === null || event === "") {
      setFieldValue("agent_customer_id", "");
    } else {
      axios
        .get(`/agents/cc_customers?custid=${event.value}`)
        .then((res) => {
          this.getShipToList(event.value);
          var customer_list = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            customer_list.push({
              value: element["customer_id"],
              label: htmlDecode(element["cust_name"]),
            });
          }
          this.setState({
            cc_Customers: res.data.data
          });
          setFieldValue("agent_customer_id", event.value);
        })
        .catch((err) => {});
    }
  };

  handleSubmit = (values, actions) => {
    this.setState({ showLoader: true, errMsg: "" });
    var spec = "";
    var formData = {};
    var cc_Cust = [];
    let agent_customer_id = '';

    if (values.specifications !== undefined) {
      spec = values.specifications.replace(/(<([^>]+)>)/gi, "");
    }
    let userData = BasicUserData();
    if(userData.role === 2){
      if (
        this.state.agent_customer_id &&
        this.state.agent_customer_id !== null &&
        this.state.agent_customer_id.value > 0
      ) {
        agent_customer_id = this.state.agent_customer_id.value;
      }
    }

    this.state.cc_Customers.map((user, index) => {
      if (user.checked === 1) {
        cc_Cust.push({ customer_id: user.customer_id });
      }
    });
    
    //console.log(values.deliveryDate);
    this.setState({
      specification: spec,
      quantity: values.quantity,
      request_date: (values.deliveryDate != '' && values.deliveryDate != undefined) ? values.deliveryDate : dateFormat(this.state.startDate, "yyyy-mm-dd"),
    });

    axios
      .post("sap/check_stock_availability_new", {
        product_name: this.state.product_id.id,
        product_code: this.state.product_code.value,
        market: this.state.market_code.value,
        quantity: this.state.quantity,
        productunit: this.state.quantity_unit.value,
        rdd: (values.deliveryDate != '' && values.deliveryDate != undefined) ? values.deliveryDate : dateFormat(this.state.startDate, "yyyy-mm-dd"),
        ship_to_party: this.state.ship_to.value,
        additional_comment: this.state.specification,
        check_early: this.state.checkEarly,
        as_admin: this.state.as_admin,
        share_with_agent: values.share_with_agent,
        cc_customers: JSON.stringify(cc_Cust),
        agent_customer_id : agent_customer_id,        
      })
      .then((res) => {
        if (res.status === 200) {
          if (res.data.status == 1) {

            this.setState({
              showLoader: false,
              showForm: false,
              showView: true,
              showCheckout: false,
              stock_list: res.data.stock_list,
              dynamic_stock_list: [res.data.stock_list],
              selectedRevisions: 1,
              withOutRevision: false
            });
          } else {
            this.setState({ showLoader: false, errMsg: res.data.message, buttonNext: true, withOutRevision: true });
          }

        } else {
          //alert("Failed to add Task"); 
          this.setState({ errMsg: this.props.dynamic_lang.stock_enquiry_new.failed_to_submit, buttonNext: true, withOutRevision: true });
        }
      })
      .catch((err) => {
        this.setState({ showLoader: false });

        //actions.setSubmitting(false);
        //  alert(err.data.status)

        if (err.data.status === 2) {
          //actions.setErrors(err.data.errors);
          this.setState({ errMsg: this.props.dynamic_lang.stock_enquiry_new.validation_error, buttonNext: true, withOutRevision: true });
        } else {
          this.setState({ errMsg: err.data.message, buttonNext: true, withOutRevision: true });
        }
      });

  };


  handleCheckoutSubmit = (values, actions) => {
    this.setState({ showLoader: true, errMsg: "" });// console.log(this.state.stock_list);
    let subObj = {
      request_id: this.state.stock_list.reference_no,
      //supply_type: this.state.showFullSupply1,
      //line_items : this.state.line_items      
    }
    if (this.state.withOutRevision == false) {
      subObj.supply_type = this.state.showFullSupply1;
      subObj.line_items = this.state.line_items;
      subObj.line_items_date = this.state.line_items_date;
    } else {
      subObj.supply_type = 9;
    }
    let userData = BasicUserData();
    if(userData.role === 2){
      if (
        this.state.agent_customer_id &&
        this.state.agent_customer_id !== null &&
        this.state.agent_customer_id.value > 0
      ) {
        subObj.agent_customer_id = this.state.agent_customer_id.value;
      }
    }

    //console.log(subObj);
    axios.post("sap/reserve_stock_new", subObj)
      .then((res) => {
        if (res.status === 200) {
          if (res.data.status == 1) {
            if (res.data.redircted == 1) {
              this.setState({
                showLoader: false,
                showForm: false,
                showView: false,
                showCheckout: true,
              });
            } else {
              this.setState({
                showLoader: false,
                showForm: true,
                showView: false,
                showCheckout: false,
                as_admin: 0
              });
            }
          } else {
            this.setState({ showLoader: false, errMsg: res.data.message });
          }
        } else {
          this.setState({ showLoader: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.fail_to_reserve  });
        }
      }).catch((err) => {
        this.setState({ showLoader: false });

        //actions.setSubmitting(false);
        //  alert(err.data.status)

        if (err.data.status === 2) {
          //actions.setErrors(err.data.errors);
          //this.setState({ errMsg: err.data.errors });
        } else {
          this.setState({ errMsg: err.data.message });
        }
      });

  };

  handleReserveMoreSubmit = (values, actions) => {

    this.setState({ showLoader: true, errMsg: "" });
    let subObj = {
      request_id: this.state.stock_list.reference_no,
      //supply_type: this.state.showFullSupply1,
      //line_items : this.state.line_items
    }

    if (this.state.withOutRevision == false) {
      subObj.supply_type = this.state.showFullSupply1;
      subObj.line_items = this.state.line_items;
      subObj.line_items_date = this.state.line_items_date;
    } else {
      subObj.supply_type = 9;
    }

    let userData = BasicUserData();
    if(userData.role === 2){
      if (
        this.state.agent_customer_id &&
        this.state.agent_customer_id !== null &&
        this.state.agent_customer_id.value > 0
      ) {
        subObj.agent_customer_id = this.state.agent_customer_id.value;
      }
    }

    // console.log(subObj);
    axios.post("sap/reserve_stock_new", subObj)
      .then((res) => {
        if (res.status === 200) {
          if (res.data.status == 1) {
            if (res.data.redircted == 1) {
              this.setState({
                showLoader: false,
                showForm: false,
                showModal: true,
              });
            } else {
              this.setState({
                showLoader: false,
                showForm: true,
                showModal: false,
                showView: false,
                as_admin: 0
              });
            }
          } else {
            this.setState({ showLoader: false, errMsg: res.data.message });
          }
        } else {
          this.setState({ showLoader: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.fail_to_reserve });
        }
      }).catch((err) => {
        this.setState({ showLoader: false });

        //actions.setSubmitting(false);
        //  alert(err.data.status)

        if (err.data.status === 2) {
          this.setState({ errMsg: err.data.errors });
          //this.setState({ errMsg: err.data.errors });
        } else {
          this.setState({ errMsg: err.data.message });
        }
      });

  };

  handlePOSubmit = (values, actions) => {

    //console.log(this.state);   

    if (this.state.po_no !='') {
      this.setState({po_err : ""})
      let subObj = {
        request_id: this.state.stock_list.reference_no,
        sold_to_party: this.state.sold_to_party.value,        
      }
      let userData = BasicUserData();    
      //console.log(this.state.files); console.log(this.state.files.length);
      if (this.state.files.length === 0) {
        this.setState({ errMsg: this.props.dynamic_lang.stock_enquiry_new.select_file_to_upload});
      } else {
        this.setState({ showLoader: true, errMsg: "" });
        var formData = new FormData();
        formData.append('request_id', this.state.stock_list.reference_no);
        formData.append('po_no' ,this.state.po_no);
        if (this.state.files && this.state.files.length > 0) {
          for (let index = 0; index < this.state.files.length; index++) {
            const element = this.state.files[index];
            formData.append("file", element);
          }
        }
        if(userData.role === 2){
          if (
            this.state.agent_customer_id &&
            this.state.agent_customer_id !== null &&
            this.state.agent_customer_id.value > 0
          ) {
            formData.append("agent_customer_id", this.state.agent_customer_id.value);
          }
        }

        axios
          .post("sap/customer_approve_po", formData)
          .then((res) => {
            if (res.status === 200) {
              this.setState({ showLoader: false });
              this.props.history.push({
                pathname: "/my_orders",
                state: {
                  message: this.props.dynamic_lang.stock_enquiry_new.success_reserve_stock_and_po,
                },
              });
            } else {
              this.setState({ showLoader: false });
              this.setState({ errMsg: this.props.dynamic_lang.stock_enquiry_new.failed_to_upload_po });
            }
          })
          .catch((err) => {
            this.setState({ showLoader: false });

            //actions.setSubmitting(false);
            //  alert(err.data.status)

            //if (err.data.status === 2) {

            this.setState({ errMsg: err.data.message });
            //}
          });

      }
    }else{
      this.setState({po_err : "Purchase Order No. required"})
    }
  };

  handlePODelaySubmit = (values, actions) => {
    this.setState({ showLoader: true, errMsg: "" });
    let subObj = {
      request_id: this.state.stock_list.reference_no,
    }
    let userData = BasicUserData();
    if(userData.role === 2){
      if (
        this.state.agent_customer_id &&
        this.state.agent_customer_id !== null &&
        this.state.agent_customer_id.value > 0
      ) {
        subObj.agent_customer_id = this.state.agent_customer_id.value;
      }
    }

    axios
      .post("sap/customer_delay_po", subObj)
      .then((res) => {
        if (res.status === 200) {
          this.setState({ showLoader: false });
          this.props.history.push({
            pathname: "/my_orders",
            state: {
              message: this.props.dynamic_lang.stock_enquiry_new.success_reserve_stock_24hrs,
            },
          });
        } else {
          this.setState({ showLoader: false });
          this.setState({ errMsg: this.props.dynamic_lang.stock_enquiry_new.failed_to_reserve_stock });
        }
      })
      .catch((err) => {
        this.setState({ showLoader: false });

        if (err && err.data && err.data.status === 2) {
          actions.setErrors(err.data.errors);
        } else {
          this.setState({ errMsg: err.data.message });
        }
      });
  };

  handleSubmitModal = (value, actions) => {
    //console.log(value)
    this.setState({ showLoader: true, errMsg: "" });
    let reqObj = {
      request_id: this.state.stock_list.reference_no,
      quantity: value.quantity_modal,
      productunit: value.quantity_unit_modal,
      rdd: value.deliveryDateModal
    }
    actions.resetForm();

    let userData = BasicUserData();
    if(userData.role === 2){
      if (
        this.state.agent_customer_id &&
        this.state.agent_customer_id !== null &&
        this.state.agent_customer_id.value > 0
      ) {
        reqObj.agent_customer_id = this.state.agent_customer_id.value;
      }
    }

    axios
      .post("sap/more_stock_availability", reqObj)
      .then((res) => {
        if (res.status === 200) {
          if (res.data.status == 1) {

            let prev_state = this.state.dynamic_stock_list;

            for (let i = 0; i < prev_state.length; i++) {
              prev_state[i].disabled = true;
              if (prev_state[i].available_stock.length > 0) {
                for (let j = 0; j < prev_state[i].available_stock.length; j++) {
                  prev_state[i].available_stock[j].disabled = true;
                  prev_state[i].available_stock[j].rdd_flag = true;
                }
              }
            }

            prev_state.push(res.data.stock_list);

            // console.log(res);            
            this.setState({
              selectedRevisions: res.data.stock_list.reservation_no,
              showFullSupply2: true,
              line_items: [],
              line_items_date: [],
              withOutRevision: false,
              dynamic_stock_list: prev_state,
              showFullSupply1: 2
            });

            this.setState({
              showLoader: false,
              showForm: false,
              showModal: false,
              buttonNext: false,
              startDateModal: ''
            });

            // console.log(value)
          } else {
            this.setState({ showLoader: false, showModal: false, errMsg: res.data.message, withOutRevision: true });

            let prev_state = this.state.dynamic_stock_list; 

            var last_index = parseInt(prev_state.length - 1);
            prev_state[last_index].available_stock.map((stock, c) => {
              prev_state[last_index].available_stock[c].disabled = true;
            });
            prev_state[last_index].disabled = true;
            // console.log(prev_state);
            this.setState({ dynamic_stock_list: prev_state });
          }

        } else {
          //alert("Failed to add Task");
          this.setState({ showModal: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.failed_to_submit });
        }
      })
      .catch((err) => {
        this.setState({ showLoader: false, showModal: false });

        //actions.setSubmitting(false);
        //  alert(err.data.status)

        if (err.data.status === 2) {
          actions.setErrors(err.data.errors);
          this.setState({ showModal: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.validation_error });
        } else {
          this.setState({ showModal: false, errMsg: err.data.message });
        }
      });

  };

  recId = (event) => {
    var tempArray = [];

    if (event.target.checked === true) {
      this.state.files.map((file, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          // console.log("match");
          file.checked = event.target.checked;
        }
        tempArray.push(file);
        // console.log(tempArray);
      });
    } else {
      this.state.files.map((file, fileIndex) => {
        // console.log("fileIndex", fileIndex);
        // console.log("value", event.target.value);

        if (event.target.value.toString() === fileIndex.toString()) {
          // console.log("match");
          file.checked = event.target.checked;
        }
        tempArray.push(file);
        // console.log(tempArray);
      });
    }

    this.setState({ files: tempArray }, () => {
      console.log(this.state.files);
    });
  };
  setDropZoneFiles = (acceptedFiles) => {

    var rejectedFiles = [];
    var uploadFile = [];

    var totalfile = acceptedFiles.length;

    for (var index = 0; index < totalfile; index++) {
      var error = 0;
      var filename = acceptedFiles[index].name.toLowerCase();
      var extension_list = supportedPdfFileType();
      var ext_with_dot = path.extname(filename);
      var file_extension = ext_with_dot.split(".").join("");

      var obj = {};

      var fileErrText = this.props.dynamic_lang.display_error.form_error
      .following_extensions;

      if (extension_list.indexOf(file_extension) == '-1') {
        error = error + 1;

        //obj["fileName"] = filename;

        if (totalfile > 1) {
          if (index === 0) {
            obj["errorText"] =
            this.props.dynamic_lang.display_error.form_error.error_text_1.replace(
              "[file_name]",
              filename
            ) +
              fileErrText;
          } else {
            obj["errorText"] =
            this.props.dynamic_lang.display_error.form_error.error_text_2.replace(
              "[file_name]",
              filename
            ) +
              fileErrText;
          }
        } else {
          obj[
            "errorText"
          ] = this.props.dynamic_lang.display_error.form_error.error_text_2.replace(
            "[file_name]",
            filename
          ) + fileErrText;
        }

        rejectedFiles.push(obj);

        //console.log(rejectedFiles);
      }
      
      if (acceptedFiles[index].size > 50000000) {
        obj[
          "errorText"
        ] = this.props.dynamic_lang.display_error.form_error.allowed_size.replace(
          "[file_name]",
          filename
        );

        error = error + 1;

        rejectedFiles.push(obj);

        //console.log(rejectedFiles);
      }

      if (error === 0) {
        uploadFile.push(acceptedFiles[index]);
      }
    }

    //var prevFiles = this.state.files;
    var newFiles = [];
    // if (prevFiles.length > 0) {
    //   for (let index = 0; index < uploadFile.length; index++) {
    //     var remove = 0;

    //     for (let index2 = 0; index2 < prevFiles.length; index2++) {
    //       if (uploadFile[index].name === prevFiles[index2].name) {
    //         remove = 1;
    //         break;
    //       }
    //     }

    //     if (remove === 0) {
    //       prevFiles.push(uploadFile[index]);
    //     }
    //   }

    //   prevFiles.map((file) => {
    //     file.checked = false;
    //     newFiles.push(file);
    //   });
    // } else {
    uploadFile.map((file) => {
      file.checked = false;
      newFiles.push(file);
    });

    //console.log("acceptedFiles", acceptedFiles);
    //console.log("newFiles", newFiles);
    //}

    this.setState({
      files: newFiles,
      // filesHtml: fileListHtml
    });
    //console.log("newFiles", newFiles);

    this.setState({
      rejectedFile: rejectedFiles,
    });
    // }
  };

  modalCloseHandler = () => {
    this.setState({
      showModal: false,
      buttonNext: true,
      withOutRevision: true
    });
  };

  modalInfoCloseHandler = () => {
    this.setState({
      showModalInfo: false,
      dynamic_stock_info_html: ''
    });
  };

  setNextDate = (event) => {
    let date = new Date()
    let next_date = date.setDate(date.getDate())
    this.setState({ startDate: next_date, deliveryDateDisabled: event.target.checked });

    if (event.target.checked) {
      this.setState({ checkEarly: true })
    } else {
      this.setState({ checkEarly: false });
    }
  };

  viewInformation = (event, type, main_index) => {
    let info_state = this.state.dynamic_stock_list[main_index].information_stock;

    if (info_state) {

      let body_info_html = info_state.map((val, index) => {
        // console.log(val);
        let val_quantity_unit = '';
        quantity_unit_list.map((subItems, sIndex) => {
          if(val.quantity_unit == subItems.value){
            val_quantity_unit = subItems.label;
          }
        })
        return (
          <>
            <tr>
              <td>{val.item_number}</td>
              <td>{val.fbpo_number}</td>
              <td>{val.available_quantity} {val_quantity_unit} </td>
              <td>{val.expected_date.substring(6, 8)}-{val.expected_date.substring(4, 6)}-{val.expected_date.substring(0, 4)}</td>
            </tr>
          </>
        )
      });

      this.setState({
        dynamic_stock_info_html: body_info_html,
        showModalInfo: true,
      });
    }



  };

  enableCheckbox = (event, index, type, main_index = 0) => {
    //console.log(event.target.checked+"=="+index+"=="+type+"=="+main_index);

    let prev_state = this.state.dynamic_stock_list;
    if (type === 'display_full_quantity') {
      prev_state[index].display_full_quantity = event.target.checked;

      if (prev_state[index].available_stock.length > 0) {
        for (let i = 0; i < prev_state[index].available_stock.length; i++) {
          prev_state[index].available_stock[i].confirm = false;
        }
      }

      this.state.line_items = [];
      this.state.line_items_date = [];
      this.setState({ buttonNext: false });

      if (event.target.checked) {
        this.setState({ showFullSupply1: 1 });
      } else {
        this.setState({ showFullSupply1: 2 });

      }

    } else if (type === 'available_stock') {
      let date_check = '';
      if (!prev_state[main_index].available_stock[index].epdd_flag) {
        //console.log(`rdd_check_${prev_state[main_index].available_stock[index].act_response_id}`);
        date_check = `rdd_check_${prev_state[main_index].available_stock[index].act_response_id}`;
      } else {
        //console.log(`expected_date_check_${prev_state[main_index].available_stock[index].act_response_id}`);
        date_check = `expected_date_check_${prev_state[main_index].available_stock[index].act_response_id}`;
      }
      prev_state[main_index].available_stock[index].confirm = event.target.checked;

      let count_items = prev_state[main_index].available_stock.length;

      if (prev_state[main_index].display_full_quantity === true && (index + 1) === count_items) {
        if (event.target.checked === true) {
          this.state.line_items.push(prev_state[main_index].available_stock[index].act_response_id);
          this.state.line_items_date.push(date_check);
          this.setState({ buttonNext: true });
        } else {
          let ind = this.state.line_items.indexOf(prev_state[main_index].available_stock[index].act_response_id);
          this.state.line_items.splice(ind, 1);
          let ind1 = this.state.line_items_date.indexOf(date_check);
          this.state.line_items_date.splice(ind1, 1);
          this.setState({ buttonNext: false });
        }
      } else {
        if (event.target.checked === true) {
          this.state.line_items.push(prev_state[main_index].available_stock[index].act_response_id);
          this.state.line_items_date.push(date_check);
        } else {
          let ind = this.state.line_items.indexOf(prev_state[main_index].available_stock[index].act_response_id);
          this.state.line_items.splice(ind, 1);
          let ind1 = this.state.line_items_date.indexOf(date_check);
          this.state.line_items_date.splice(ind1, 1);
        }

        if (this.state.line_items.length > 0) {
          this.setState({ buttonNext: true });
        } else {
          this.setState({ buttonNext: false });
        }

      }
      // console.log(event.target.name);
      //console.log(this.state.line_items);
      //console.log(this.state.line_items_date);

    } else if (type == "epdd_flag") {
      prev_state[main_index].available_stock[index].epdd_flag = !prev_state[main_index].available_stock[index].epdd_flag;
      let old_date_check = '', new_date_check = '';
      if (!prev_state[main_index].available_stock[index].epdd_flag) {
        old_date_check = `expected_date_check_${prev_state[main_index].available_stock[index].act_response_id}`;
        new_date_check = `rdd_check_${prev_state[main_index].available_stock[index].act_response_id}`;

        let ind1 = this.state.line_items_date.indexOf(old_date_check);
        if (ind1 != '-1') {
          this.state.line_items_date.splice(ind1, 1);
          this.state.line_items_date.push(new_date_check);
        }
      } else {
        old_date_check = `rdd_check_${prev_state[main_index].available_stock[index].act_response_id}`;
        new_date_check = `expected_date_check_${prev_state[main_index].available_stock[index].act_response_id}`;

        let ind1 = this.state.line_items_date.indexOf(old_date_check);
        if (ind1 != '-1') {
          this.state.line_items_date.splice(ind1, 1);
          this.state.line_items_date.push(new_date_check);
        }
      }
    }

    this.setState({ dynamic_stock_list: prev_state });

  }

  removeRequest = (index, reservation_no, from) => {
    var popupText = this.props.dynamic_lang.stock_enquiry_new.sure_to_cancel;
    if (from == 2) {
      popupText = this.props.dynamic_lang.stock_enquiry_new.sure_to_go_back_reset;
    }
    swal({
      closeOnClickOutside: false,
      title: this.props.dynamic_lang.stock_enquiry_new.alert,
      text: popupText,
      icon: "warning",
      buttons: true,
      dangerMode: true,
      customClass: 'customAlert',
    }).then((willDelete) => {
      if (willDelete) {
        let prev_state = this.state.dynamic_stock_list;

        let subObj = {
          request_id: this.state.stock_list.reference_no,
          reservation_no: reservation_no
        }
        let userData = BasicUserData();
        if(userData.role === 2){
          if (
            this.state.agent_customer_id &&
            this.state.agent_customer_id !== null &&
            this.state.agent_customer_id.value > 0
          ) {
            subObj.agent_customer_id = this.state.agent_customer_id.value;
          }
        }
        //console.log(subObj);
        axios
          .post("sap/remove_reservation", subObj)
          .then((res) => {
            if (res.status === 200) {
              this.setState({ showLoader: false });

              prev_state.splice(index, 1);

              this.setState({ dynamic_stock_list: prev_state, withOutRevision: true });
              // console.log(res.data);
              if (res.data.redircted == 0) {
                //alert('Latest block removed');
                this.setState({ buttonNext: true });
              } else {
                this.setState({
                  showLoader: false,
                  showForm: true,
                  showView: false,
                  showCheckout: false,
                  checkEarly: false,
                  as_admin: 0
                });
              }

            } else {
              this.setState({ showLoader: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.failed_to_reserve_stock });
            }
          })
          .catch((err) => {
            this.setState({ showLoader: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.failed_to_connect });

          });
      }
    });
  }

  makeDynamicHtml = () => {
    let prev_state = this.state.dynamic_stock_list; //console.log(prev_state);

    let html = prev_state.map((val, index) => {
      let val_unit = '';
      quantity_unit_list.map((subItems, sIndex) => {
        if(val.unit == subItems.value){
          val_unit = subItems.label;
        }
      })
      return (
        <>
          <div className="stockDisplayBox">
            {this.state.selectedRevisions == val.reservation_no &&
              <Link
                to="#"
                className="outerClose"
                style={{ cursor: "pointer" }}
              >
                <img
                  src={closeIconOuter}
                  width="30px"
                  height="30px"
                  onClick={(e) => this.removeRequest(index, val.reservation_no, 1)}
                />
              </Link>}
            <table className="table table-bordered customTable">
              <tbody>
                <tr>
                  <td width="20%">
                    <strong>{this.props.dynamic_lang.stock_enquiry_new.label.enquiry_result} {index + 1}</strong>
                  </td>
                  <td width="30%">
                    <strong>
                      {this.props.dynamic_lang.stock_enquiry_new.label.requested_quantity} {val.quantity}{" "}  {val_unit}
                    </strong>
                  </td>
                  {/* <td width="20%">
                    <strong>RDD: {val.rdd} </strong>
                    </td> */}
                  <td width="50%">
                    {val.available_stock && val.available_stock.length > 1 && <div className="form-check">
                      <label className="form-check-label">
                        <input
                          type="checkbox"
                          className="form-check-input supplyToggle"
                          value="1"
                          onClick={(e) => this.enableCheckbox(e, index, 'display_full_quantity')}
                          checked={val.display_full_quantity}
                          disabled={val.disabled}
                        />
                          {this.props.dynamic_lang.stock_enquiry_new.full_quantity_single_supply}
                        </label>
                    </div>}
                  </td>
                </tr>
              </tbody>
            </table>


            <table className="table table-bordered customTable proInfo">
              <tbody>
                <tr>
                  {/* <td
                    width="25%"
                    height="27"
                    className="tablePrimaryBg"
                    >
                    <strong>Referance No</strong>
                    </td> */}
                  <td width="15%" className="tablePrimaryBg">
                    <strong>{this.props.dynamic_lang.stock_enquiry_new.quantity_h}</strong>
                  </td>
                  <td width="20%" className="tablePrimaryBg">
                    <strong>{this.props.dynamic_lang.stock_enquiry_new.request_delivery_date}</strong>
                  </td>
                  <td width="20%" className="tablePrimaryBg">
                    <strong>{this.props.dynamic_lang.stock_enquiry_new.earliest_delivery_date}</strong>
                  </td>
                  <td width="20%" className="tablePrimaryBg">
                    <strong>{this.props.dynamic_lang.stock_enquiry_new.rtl_based_stock}</strong>
                  </td>
                  <td width="25%" className="tablePrimaryBg">
                    <strong>{this.props.dynamic_lang.stock_enquiry_new.confirm}</strong>

                    {this.state.selectedRevisions > val.reservation_no &&
                      <Link
                        to="#"
                        className="outerClose"
                        style={{ cursor: "pointer", float: "right" }}
                      >
                        <img
                          src={closeIconOuter}
                          width="30px"
                          height="30px"
                          onClick={(e) => this.removeRequest(index, val.reservation_no, 2)}
                        />
                      </Link>}
                  </td>
                </tr>
                {val.available_stock && val.available_stock.length != 0 ? (
                  <>
                    {val.available_stock.map((stock, c) => {
                      let stock_quantity_unit = '';
                      quantity_unit_list.map((subItems, sIndex) => {
                        if(stock.quantity_unit == subItems.value){
                          stock_quantity_unit = subItems.label;
                        }
                     })
                      return (
                        <>
                          {(c + 1) < val.available_stock.length ? (
                            <tr style={{ display: val.display_full_quantity ? "none" : "" }} >
                              {/* <td>{stock.response_id}</td> */}
                              <td>{stock.available_quantity} {stock_quantity_unit} </td>
                              <td>{val.rdd}
                                <input type="radio" value={`rdd_check_${stock.act_response_id}`} name={val.disabled ? `old_delivery_choice_${index}[${c}]` : `delivery_choice[${c}]`} checked={!stock.epdd_flag} disabled={(stock.epdd_flag && stock.rdd_flag) || val.disabled} onClick={(e) => this.enableCheckbox(e, c, 'epdd_flag', index)} />
                              </td>
                              <td>{stock.expected_date.substring(6, 8)}-{stock.expected_date.substring(4, 6)}-{stock.expected_date.substring(0, 4)}
                                <input type="radio" value={`expected_date_check_${stock.act_response_id}`} name={val.disabled ? `old_delivery_choice_${index}[${c}]` : `delivery_choice[${c}]`} checked={stock.epdd_flag} disabled={(stock.epdd_flag && stock.rdd_flag) || val.disabled} onClick={(e) => this.enableCheckbox(e, c, 'epdd_flag', index)} />
                              </td>
                              <td>{stock.rtl_flag == true &&
                                <img
                                  src={tickIcon}
                                  width="20px"
                                  height="20px"
                                />
                              }</td>
                              <td>
                                <input
                                  type="checkbox"
                                  name="confirmCheck1"
                                  className="mt-0"
                                  value={stock.act_response_id}
                                  onClick={(e) => this.enableCheckbox(e, c, 'available_stock', index)}
                                  disabled={stock.disabled}
                                  checked={stock.confirm}
                                />
                              </td>
                            </tr>
                          ) : (
                            <tr>
                              {/* <td>{stock.response_id}</td> */}
                              <td>{val.display_full_quantity ? val.total_quantity : stock.available_quantity} {stock_quantity_unit} </td>
                              <td>{val.rdd}
                                <input type="radio" value={`rdd_check_${stock.act_response_id}`} name={val.disabled ? `old_delivery_choice_${index}[${c}]` : `delivery_choice[${c}]`} checked={!stock.epdd_flag} disabled={(stock.epdd_flag && stock.rdd_flag) || val.disabled} onClick={(e) => this.enableCheckbox(e, c, 'epdd_flag', index)} />
                              </td>
                              <td>{stock.expected_date.substring(6, 8)}-{stock.expected_date.substring(4, 6)}-{stock.expected_date.substring(0, 4)}
                                <input type="radio" value={`expected_date_check_${stock.act_response_id}`} name={val.disabled ? `old_delivery_choice_${index}[${c}]` : `delivery_choice[${c}]`} checked={stock.epdd_flag} disabled={(stock.epdd_flag && stock.rdd_flag) || val.disabled} onClick={(e) => this.enableCheckbox(e, c, 'epdd_flag', index)} />
                              </td>
                              <td>{stock.rtl_flag == true &&
                                <img
                                  src={tickIcon}
                                  width="20px"
                                  height="20px"
                                />
                              }</td>
                              <td>
                                <input
                                  type="checkbox"
                                  name="confirmCheck1"
                                  className="mt-0"
                                  value={stock.act_response_id}
                                  onClick={(e) => this.enableCheckbox(e, c, 'available_stock', index)}
                                  disabled={stock.disabled}
                                  checked={stock.confirm}
                                />
                              </td>
                            </tr>
                          )
                          }
                        </>
                      )
                    })}
                  </>
                ) : (
                  <tr><td colspan="5">{this.props.dynamic_lang.stock_enquiry_new.no_stock_available}</td></tr>
                )}
              </tbody>
              {val.information_stock && val.information_stock.length != 0 &&
                <tfoot>
                  <tr>
                    <td colspan="5">
                      <Link
                        to="#"
                        className=""
                        style={{ cursor: "pointer", float: "right" }}
                        onClick={(e) => this.viewInformation(e, 'information_stock', index)}
                      >{this.props.dynamic_lang.stock_enquiry_new.view_more_info}
                        </Link>
                    </td>
                  </tr>
                </tfoot>

              }
            </table>
          </div>

          <div className="clearfix"></div>
        </>
      );
    })

    return html;

  }

  render() {
    const {
      product_id,
      product_code,
      market_code,
      ship_to,
      quantity_value,
      quantity_unit,
      quantity_unit_modal,
      specification,
    } = this.state;

    const {validation : validation_lang, label: label_lang} = this.props.dynamic_lang.stock_enquiry_new;

    const validationSchema = (refObj) =>
      Yup.object().shape({
        product_id: Yup.string().required(validation_lang.product_name),
        product_code: Yup.string().required(validation_lang.product_code),
        market_code: Yup.number().required(validation_lang.market_code),
        quantity: Yup.string()
          .required(validation_lang.quantity)
          .test("customCheck1", validation_lang.should_be_integer, function (value) {
            if (value == 0|| value==undefined) {
              return false;
            }
            return true;
          })
          .matches(/^(\d*\.)?\d+$/, validation_lang.should_be_integer),
        quantity_unit: Yup.string().required(validation_lang.unit_required),
        ship_to: Yup.string().required(validation_lang.ship_to),
        deliveryDate: Yup.string().required(validation_lang.delivery_date),
      });

    const validateStopFlag = (refObj) =>
      Yup.object().shape({
        quantity_modal: Yup.string()
          .required(validation_lang.quantity)
          .test("customCheck1", validation_lang.should_be_integer, function (value) { 
            if (value == 0 || value==undefined) {
              return false;
            }
            return true;            
          })
          .matches(/^(\d*\.)?\d+$/, validation_lang.should_be_integer),
        quantity_unit_modal: Yup.string().required(validation_lang.unit_required),
        deliveryDateModal: Yup.string().required(validation_lang.delivery_date),
      })

    const initialValues = {
      product_id: product_id.value ? product_id.value : "",
      product_code: product_code.value ? product_code.value : "",
      market_code: market_code.value ? market_code.value : "",
      ship_to: ship_to.value ? ship_to : "",
      quantity_unit: quantity_unit.value ? quantity_unit.value : "",
      quantity: quantity_value ? quantity_value : "",
      specification: specification,
    }
    if (this.state.showLoader) {
      return <Loader />;
      // return <h2>Loading....</h2>
    } else {
      return (
        <>
          <div className="container-fluid clearfix formSec">
            <div className="dashboard-content-sec">
              <div
                className="messageserror"
                style={{
                  display:
                    this.state.errMsg &&
                      this.state.errMsg !== null &&
                      this.state.errMsg !== ""
                      ? "block"
                      : "none",
                }}
              >
                <Link to="#" className="close">
                  <img
                    src={closeIcon}
                    width="22px"
                    height="22px"
                    onClick={(e) => this.hideError()}
                  />
                </Link>
                <div>
                  <ul className="">
                    <li className="">{this.state.errMsg}</li>
                  </ul>
                </div>
              </div>
              {this.state.showForm && (
                <div className="service-request-form-sec newFormSec stockWrapper">
                  <div className="form-page-title-block">
                    <h2>{this.props.dynamic_lang.stock_enquiry_new.stock_enquiry}</h2>
                    <Link
                      to="#"
                      className="backLink"
                      to={{ pathname: "/my_orders" }}
                      id="backOrder"
                    >
                      {this.props.dynamic_lang.stock_enquiry_new.back}
                    </Link>
                  </div>

                  <Formik
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={this.handleSubmit}
                  >
                    {({
                      errors,
                      values,
                      touched,
                      setErrors,
                      setFieldValue,
                      setFieldTouched,
                    }) => {
                      //console.log("formik values", values);
                      // console.log(errors);console.log(touched);
                      return (
                        <Form>
                          <div
                            className="messageserror"
                            style={{
                              display:
                                (errors.product_id && touched.product_id) || (errors.product_code && touched.product_code) || (errors.market_code && touched.market_code) || (errors.quantity_unit && touched.quantity_unit) || (errors.ship_to && touched.ship_to) || (errors.quantity && touched.quantity) || (errors.deliveryDate)
                                  ? "block"
                                  : "none",
                            }}
                          >
                            <Link to="#" className="close">
                              <img
                                src={closeIcon}
                                width="22px"
                                height="22px"
                                onClick={(e) => this.removeError(setErrors)}
                              />
                            </Link>
                            <div>
                              <ul className="">
                                {errors.product_id && touched.product_id ? (
                                  <li className="">{errors.product_id}</li>
                                ) : null}
                                {errors.product_code && touched.product_code ? (
                                  <li className="">{errors.product_code}</li>
                                ) : null}
                                {errors.market_code && touched.market_code ? (
                                  <li className="">{errors.market_code}</li>
                                ) : null}
                                {errors.quantity && touched.quantity ? (
                                  <li className="">{errors.quantity}</li>
                                ) : null}
                                {errors.quantity_unit && touched.quantity_unit ? (
                                  <li className="">{errors.quantity_unit}</li>
                                ) : null}
                                {errors.ship_to && touched.ship_to ? (
                                  <li className="">{errors.ship_to}</li>
                                ) : null}
                                {errors.deliveryDate ? (
                                  <li className="">{errors.deliveryDate}</li>
                                ) : null}
                              </ul>
                            </div>
                          </div>
                          {this.state.displayCCList && (
                            <div className="row">
                              <div className="col-md-12">
                                <Select
                                  options={
                                    this.props.customerList !== null
                                      ? this.props.customerList
                                      : []
                                  }
                                  isSearchable={true}
                                  isClearable={true}
                                  value={this.state.agent_customer_id}
                                  placeholder="Select User *"
                                  onChange={(e) =>
                                    this.getAgentCCList(e, setFieldValue)
                                  }
                                />
                              </div>
                            </div>
                          )}
                          <div className="row">
                            <div className="col-md-12">
                              <Select
                                value={ship_to}
                                options={ship_list}
                                isSearchable={true}
                                isClearable={true}
                                placeholder={this.props.dynamic_lang.stock_enquiry_new.ship_to_party}
                                className=""
                                onChange={(e) => {
                                  if (e === null || e === "") {
                                    setFieldValue("ship_to", "");
                                    setFieldValue("product_id", "");
                                    setFieldValue("product_code", '');
                                    this.setState({ ship_to: "",product_id: "",product_code: "",showBlock: false,showAddiSpec: false });
                                  } else {
                                    setFieldValue("ship_to", e.value);
                                    this.setState({ ship_to: e });
                                    this.getProductList(e.value);
                                    this.getSoldToList(e.value);
                                  }
                                }}
                              />
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-4">
                              <Select
                                value={product_id}
                                options={product_list}
                                isSearchable={true}
                                isClearable={true}
                                placeholder={this.props.dynamic_lang.stock_enquiry_new.product_name}
                                className=""
                                onChange={(e) => {
                                  if (e === null || e === "") {
                                    setFieldValue("product_id", "");
                                    product_code_list = [];
                                    setFieldValue("product_code", '');
                                    this.setState({ product_id: "", showBlock: false, showAddiSpec: false, });
                                  } else {
                                    setFieldValue("product_id", e.value);
                                    this.setState({ product_id: e });
                                    //product_code_list = [{ value: e.value, label: e.value }];
                                    setFieldValue("product_code", '');
                                    this.getProductCodeList(e.value);
                                    this.setState({
                                      product_code: "",
                                      showBlock: false,
                                      showAddiSpec: false,
                                    });
                                  }
                                }}
                              />
                            </div>
                            <div className="col-md-4">
                              <Select
                                value={product_code}
                                options={product_code_list}
                                isSearchable={true}
                                isClearable={true}
                                placeholder={this.props.dynamic_lang.stock_enquiry_new.product_code}
                                className=""
                                onChange={(e) => {
                                  if (e === null || e === "") {
                                    setFieldValue("product_code", "");
                                    this.setState({
                                      product_code: "",
                                      showBlock: false,
                                      showAddiSpec: false,
                                    });
                                  } else {
                                    setFieldValue("product_code", e.value);
                                    this.setState({
                                      product_code: e,
                                      showBlock: true,
                                    });
                                  }
                                }}
                              />
                            </div>
                          
                            <div className="col-md-4">
                              <Select
                                value={market_code}
                                options={market_code_list}
                                isSearchable={true}
                                isClearable={true}
                                placeholder={this.props.dynamic_lang.stock_enquiry_new.select_market}
                                className=""
                                onChange={(e) => {
                                  if (e === null || e === "") {
                                    setFieldValue("market_code", 0);
                                    this.setState({ market_code: 0});
                                  } else {
                                    setFieldValue("market_code", e.value);
                                    this.setState({ market_code: e });
                                  }
                                }}
                              />
                            </div>
                          </div>

                          <div className="row">
                            <div className="col-md-2 col-sm-4">
                              <Field
                                name="quantity"
                                type="number"
                                step="any"
                                min="0"
                                step="1"
                                value={values.quantity}
                                className="form-control customInput"
                                onChange={(e)=>{
                                  setFieldValue("quantity", Number(e.target.value))
                                  this.setState({quantity_value: Number(e.target.value)})}}
                                placeholder={this.props.dynamic_lang.stock_enquiry_new.quantity}
                              />
                            </div>
                            <div className="col-md-2 col-sm-4">
                              <Select
                                value={quantity_unit}
                                options={quantity_unit_list}
                                isSearchable={true}
                                isClearable={true}
                                placeholder={this.props.dynamic_lang.stock_enquiry_new.unit}
                                className=""
                                onChange={(e) => {
                                  if (e === null || e === "") {
                                    setFieldValue("quantity_unit", "");
                                    this.setState({ quantity_unit: "" });
                                  } else {
                                    setFieldValue("quantity_unit", e.value);
                                    this.setState({ quantity_unit: e });
                                  }
                                }}
                              />
                            </div>
                            <div className="col-md-3">
                              <DatePicker
                                className="form-control customInput"
                                selected={this.state.startDate}
                                name="deliveryDate"
                                showMonthDropdown
                                showYearDropdown
                                minDate={minDate}
                                maxDate={maxDate}
                                dropdownMode="select"
                                disabled={this.state.deliveryDateDisabled}
                                onChange={(e) => {
                                  if (e === null) {
                                    setFieldValue("deliveryDate", "");
                                  } else {
                                    setFieldValue(
                                      "deliveryDate",
                                      dateFormat(e, "yyyy-mm-dd")
                                    );
                                  }
                                  this.setState({ startDate: e });
                                }}
                                dateFormat="dd-MM-yyyy"
                                autoComplete="off"
                                placeholderText={this.props.dynamic_lang.stock_enquiry_new.request_delivery_date}
                              />
                            </div>
                            <div className="col-md-5">
                              <div className="row mt-3">
                                <div className="col-1">{this.props.dynamic_lang.stock_enquiry_new.or}</div>
                                <div className="col-10">
                                  <div className="form-check">
                                    <label className="form-check-label">
                                      <input
                                        type="checkbox"
                                        className="form-check-input"
                                        name="check_earliest"
                                        value="1"
                                        onChange={(e) => this.setNextDate(e)}
                                      />
                                      {this.props.dynamic_lang.stock_enquiry_new.check_earliest_possible_delivery}
                                    </label>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>

                          {this.state.showBlock && (
                            <div className="row">
                              <div className="col-md-12 specificationWrapper">
                                <p style={{ color: 'black' }}>
                                  <b>{this.props.dynamic_lang.stock_enquiry_new.technical_specification}</b>
                                </p>
                                <p>
                                {this.props.dynamic_lang.stock_enquiry_new.particle_specification_drl_trend}
                                  <Link
                                    to="#"
                                    className="customLink"
                                    onClick={this.showAdditionalSpec}
                                    id="showhideSpec"
                                  >
                                    {this.state.showAddiSpec === true
                                      ?  this.props.dynamic_lang.stock_enquiry_new.remove_additional_specification
                                      : this.props.dynamic_lang.stock_enquiry_new.click_to_add_specification}
                                  </Link>
                                </p>
                              </div>
                              <div className="col-md-12 text-right">
                                <p></p>
                              </div>
                            </div>
                          )}

                          {this.state.showAddiSpec && (
                            <div className="row">
                              <div className="col-md-12">
                                <Editor
                                  name="specifications"
                                  content={values.specifications}
                                  value={specification}
                                  className="form-control customInput"
                                  init={{
                                    menubar: false,
                                    branding: false,
                                    placeholder:
                                    this.props.dynamic_lang.stock_enquiry_new.enter_additional_specification,
                                    plugins:
                                      "link table hr visualblocks code placeholder paste lists",
                                    paste_data_images: true,
                                    paste_use_dialog: true,
                                    paste_auto_cleanup_on_paste: false,
                                    paste_convert_headers_to_strong: true,
                                    paste_strip_class_attributes: "none",
                                    paste_remove_spans: false,
                                    paste_remove_styles: false,
                                    paste_retain_style_properties: "all",
                                    toolbar:
                                      "bold italic strikethrough superscript subscript | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | formatselect | visualblocks code | paste ",
                                    content_css: [`${(process.env.NODE_ENV === 'production' ? '/customer_portal' : '')}/css/editor.css`],
                                  }}
                                  onEditorChange={(value) =>
                                    setFieldValue("specifications", value)
                                  }
                                />
                                <br />
                              </div>
                            </div>
                          )}
                          {/* <div className="row">                           
                              <div className="form-check">
                                <label className="form-check-label">
                                  <input
                                    type="checkbox"
                                    className="form-check-input"
                                    name=""
                                    value="1"
                                    onChange={(e) => {
                                      if (e.target.checked) {                                        
                                        this.setState({ as_admin: 1 });
                                      } else {
                                        this.setState({ as_admin: 0 });
                                      }
                                    }}
                                  />
                                  Send request as Admin
                                </label>
                              </div>                            
                          </div> */}

                          {this.state.share_with_agent === true &&
                            this.state.displayCCList === false && (
                              <div className="row mt-10">
                                <div className="col-md-12">
                                  <div className="form-check">
                                    <label className="form-check-label">
                                      <input
                                        type="checkbox"
                                        className="form-check-input"
                                        value={1}
                                        defaultChecked={true}
                                        onChange={(e) =>
                                          this.shareWithAgent(e, setFieldValue)
                                        }
                                      />
                                      {
                                        this.props.dynamic_lang.form_component
                                          .display_request_agent
                                      }
                                    </label>
                                  </div>
                                </div>
                              </div>
                          )}
                          {this.state.cc_Customers &&
                            this.state.cc_Customers.length > 0 && (
                              <div className="row mt-10">
                                <div className="col-md-12">
                                  <span
                                    className="fieldset-legend"
                                    id="DisabledAutoHideExample"
                                  >
                                    {
                                      this.props.dynamic_lang.form_component
                                        .grant_access_request
                                    }
                                    <img
                                      src={infoIcon}
                                      width="15.44"
                                      height="18"
                                      id="DisabledAutoHideExample"
                                      type="button"
                                    />
                                    <Tooltip
                                      placement="right"
                                      isOpen={this.state.tooltipOpen}
                                      autohide={false}
                                      target="DisabledAutoHideExample"
                                      toggle={this.toggle}
                                    >
                                      {
                                        this.props.dynamic_lang.form_component
                                          .grant_access_request_tooltip
                                      }
                                    </Tooltip>
                                  </span>
                                  {this.state.cc_Customers &&
                                    this.state.cc_Customers.map((user, index) => {
                                      return (
                                        <div key={index} className="form-check">
                                          <label className="form-check-label">
                                            <input
                                              id={index}
                                              type="checkbox"
                                              className="form-check-input"
                                              value={index}
                                              //checked={user.status}
                                              onChange={(e) =>
                                                this.customerChecked(e)
                                              }
                                            />
                                            {htmlDecode(user.first_name)}{" "}
                                            {htmlDecode(user.last_name)} (
                                            {htmlDecode(user.company_name)})
                                          </label>
                                        </div>
                                      );
                                    })}
                                </div>
                              </div>
                            )}
                          <div className="row">
                            <div className="col text-center">
                              <button
                                className="btn btn-default"
                                style={{
                                  width: "auto",
                                }}
                              >
                                {this.props.dynamic_lang.stock_enquiry_new.check_availability}
                              </button>
                            </div>
                          </div>
                        </Form>
                      );
                    }}
                  </Formik>
                </div>
              )}

              {this.state.showView && (
                <div className="service-request-form-sec neViewmSec">
                  <div className="form-page-title-block">
                    <h2>{this.props.dynamic_lang.stock_enquiry_new.stock_enquiry}</h2>
                    <Link
                      to="#"
                      className="backLink"
                      onClick={this.showPrevious}
                      id="showHistory"
                    >
                      {this.props.dynamic_lang.stock_enquiry_new.back}
                    </Link>
                  </div>

                  <div className="row stockDisplayWrapper">
                    <div className="col-md-12">
                      <table className="table table-bordered customTable">
                        <tbody>
                          <tr>
                            <td width="150" className="tablePrimaryBg">
                              <strong>{label_lang.product}</strong>
                            </td>
                            <td>{this.state.product_id.label}</td>
                            {/* </tr>

                        <tr> */}
                            <td width="150" className="tablePrimaryBg">
                              <strong>{label_lang.product_code}</strong>
                            </td>
                            <td>{this.state.product_code.value}</td>
                          </tr>

                          <tr>
                            <td width="150" className="tablePrimaryBg">
                              <strong>{label_lang.market}</strong>
                            </td>
                            <td>{this.state.market_code.label}</td>
                            {/* </tr>
                        <tr> */}
                            <td width="150" className="tablePrimaryBg">
                              <strong>{label_lang.ship_to_party}</strong>
                            </td>
                            <td>{this.state.ship_to.label}</td>
                          </tr>
                          {this.state.specification &&
                            this.state.specification != "undefined" && (
                              <tr>
                                <td width="150" className="tablePrimaryBg">
                                  <strong>{label_lang.specs}</strong>
                                </td>
                                <td colspan="3">{this.state.specification}</td>
                              </tr>
                            )}
                        </tbody>
                      </table>

                      <h5>{this.props.dynamic_lang.stock_enquiry_new.stock_availability_confirmation}</h5>

                      {this.state.dynamic_stock_list && this.state.dynamic_stock_list.length > 0 && this.makeDynamicHtml()}


                    </div>
                  </div>
                  <div className="row">
                    <div className="col text-center mb-5">
                      <button
                        className="btn btn-default reserveBtn"
                        style={{
                          width: "auto",
                        }}
                        disabled={!this.state.buttonNext}
                        onClick={this.handleCheckoutSubmit}
                      >
                        {this.props.dynamic_lang.stock_enquiry_new.reserve_and_proceed}
                      </button>
                      <button
                        className="btn btn-default btn-cancel mt-0"
                        style={{
                          width: "auto",
                        }}
                        onClick={(e) => this.handleReserveMoreSubmit(e)}
                        disabled={!this.state.buttonNext}
                      >
                        {this.props.dynamic_lang.stock_enquiry_new.reserve_more_quantity}
                      </button>
                    </div>
                  </div>
                </div>
              )}

              {this.state.showCheckout && (
                <div className="service-request-form-sec neCheckoutmSec">
                  <div className="form-page-title-block">
                    <h2>{this.props.dynamic_lang.stock_enquiry_new.stock_enquiry}</h2>
                    <Link
                      to="#"
                      className="backLink"
                      onClick={this.showLastPrevious}
                      id="showHistory"
                    >
                      {this.props.dynamic_lang.stock_enquiry_new.back}
                    </Link>
                  </div>

                  <div className="stockCheckoutWrapper">
                    <div
                      className="messageserror"
                      style={{
                        display:
                          ((this.state.rejectedFile &&
                            this.state.rejectedFile.length) > 0 ) || this.state.po_err!=''
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIcon}
                          width="22px"
                          height="22px"
                          onClick={(e) => this.removeFile_Error()}
                        />
                      </Link>
                      {this.state.rejectedFile &&
                        this.state.rejectedFile.map((file, index) => {
                          return (
                            <div key={index}>
                              <ul className="">
                                <li className="">{file.errorText}</li>
                              </ul>
                            </div>
                          );
                        })}
                        <div>
                          <ul className="">
                            {this.state.po_err ? (
                              <li className="">{this.state.po_err}</li>
                            ) : null}
                          </ul>
                        </div>
                    </div>
                    <div className="row">
                      <div className="col-md-12"><label>{label_lang.sold_to_party_details}</label></div>
                      <div className="col-md-12">
                        <p>{this.state.sold_to_party_list.company_name}</p>
                        <p>
                          {this.state.sold_to_party_list.street != "" && this.state.sold_to_party_list.street != null ? this.state.sold_to_party_list.street + ", " : ""}
                          {this.state.sold_to_party_list.city != "" && this.state.sold_to_party_list.city != null ? this.state.sold_to_party_list.city + ", " : ""}
                          {this.state.sold_to_party_list.postalcode != "" && this.state.sold_to_party_list.postalcode != null ? this.state.sold_to_party_list.postalcode + " " : ""}
                        </p>
                        <p>{this.state.sold_to_party_list.country != "" && this.state.sold_to_party_list.country != null ? this.state.sold_to_party_list.country : ""}</p>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-12"><label>{this.props.dynamic_lang.task_details.po_no}</label></div>
                      <div className="col-md-12">
                        <input
                          name="po_no"
                          type="text"                                
                          className="form-control customInput"                                
                          placeholder={this.props.dynamic_lang.stock_enquiry_new.enter_po}
                          onChange={(e) =>{
                            if (e === null || e === "" || e== undefined) {
                              this.setState({po_no : ''})
                            }else{
                              this.setState({po_no : e.target.value})
                            }
                          }}
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-12"><label>{this.props.dynamic_lang.stock_enquiry_new.attach_po}</label></div>
                      <div className="col-md-6">
                        <Dropzone
                          multiple={false}
                          onDrop={(acceptedFiles) =>
                            this.setDropZoneFiles(acceptedFiles)
                          }
                        >
                          {({ getRootProps, getInputProps }) => (
                            <section>
                              <div {...getRootProps()} className="customFile">
                                <input
                                  {...getInputProps()}
                                  ref={this.inputRef}
                                  style={{ display: "block", float: "left" }}
                                  className="mt-0"
                                />
                              </div>
                            </section>
                          )}
                        </Dropzone>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col text-center mb-5">

                        <button
                          className="btn btn-default"
                          style={{
                            width: "auto",
                          }}
                          onClick={this.handlePOSubmit}
                        >
                           {this.props.dynamic_lang.stock_enquiry_new.upload_po_proceed}
                          </button>

                        <span
                          className="btn"
                          style={{
                            marginLeft: "1%",
                            paddingTop: "2%",
                          }}

                        >
                          {this.props.dynamic_lang.stock_enquiry_new.or}
                        </span>

                        <button
                          className="btn btn-default btn-cancel"
                          style={{
                            width: "auto",
                          }}
                          onClick={this.handlePODelaySubmit}
                        >
                          {this.props.dynamic_lang.stock_enquiry_new.reserve_stock_24hrs}
                          </button>

                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>

          <Modal
            style={{ height: 300 }}
            show={this.state.showModal}
            onHide={() => this.modalCloseHandler()}
            backdrop="static"
            className="addQntModal"
          >
            <Formik
              //initialValues={newInitialValues}
              validationSchema={validateStopFlag}
              onSubmit={this.handleSubmitModal}
            >
              {({
                values,
                errors,
                touched,
                isValid,
                isSubmitting,
                setFieldValue,
                setFieldTouched,
              }) => {
                //console.log("formik values", values);
                //console.log(errors);console.log(touched);
                return (
                  <Form>
                    <Modal.Header closeButton>
                      <Modal.Title>{this.props.dynamic_lang.stock_enquiry_new.stock_enquiry}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                      <div>
                        <div
                          className="messageserror"
                          style={{
                            display:
                              (errors.quantity_modal) || (errors.quantity_unit_modal) || (errors.deliveryDateModal)
                                ? "block"
                                : "none",
                          }}
                        >
                          <div>
                            <ul className="">
                              {errors.quantity_modal ? (
                                <li className="">{this.props.dynamic_lang.stock_enquiry_new.validation.quantity}</li>
                              ) : null}
                              {errors.quantity_unit_modal  ? (
                                <li className="">{this.props.dynamic_lang.stock_enquiry_new.validation.unit_required}</li>
                              ) : null}
                               {errors.deliveryDateModal ? (
                                <li className="">{this.props.dynamic_lang.stock_enquiry_new.validation.delivery_date}</li>
                              ) : null}

                            </ul>
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-6">
                            <Field
                              name="quantity_modal"
                              type="number"
                              step="any"
                              min="0"
                              step="1"
                              className="form-control customInput"
                              placeholder={this.props.dynamic_lang.stock_enquiry_new.quantity}
                            />
                          </div>
                          <div className="col-md-6">
                            <Select
                              //value={quantity_unit_modal}
                              options={quantity_unit_list}
                              isSearchable={true}
                              isClearable={true}
                              placeholder={this.props.dynamic_lang.stock_enquiry_new.unit}
                              className=""
                              onChange={(e) => {
                                if (e === null || e === "") {
                                  setFieldValue("quantity_unit_modal", "");
                                  this.setState({ quantity_unit_modal: "" });
                                } else {
                                  setFieldValue("quantity_unit_modal", e.value);
                                  this.setState({ quantity_unit_modal: e });
                                }
                              }}
                            />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col-md-12">
                            <DatePicker
                              className="form-control customInput"
                              selected={this.state.startDateModal}
                              name="deliveryDateModal"
                              showMonthDropdown
                              showYearDropdown
                              minDate={minDate}
                              maxDate={maxDate}
                              dropdownMode="select"
                              onChange={(e) => {
                                //console.log("evenet object", e);
                                if (e === null) {
                                  setFieldValue("deliveryDateModal", "");
                                } else {
                                  setFieldValue(
                                    "deliveryDateModal",
                                    dateFormat(e, "yyyy-mm-dd")
                                  );
                                }

                                this.setState({ startDateModal: e });
                              }}
                              // onChange={e => {
                              //   this.changeDate(e, setFieldValue);
                              // }}
                              dateFormat="dd-MM-yyyy"
                              autoComplete="off"
                              placeholderText={this.props.dynamic_lang.stock_enquiry_new.request_delivery_date}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <button
                            className={`btn btn-default reserveBtn`}
                            type="submit"
                            style={{ width: "auto" }}
                          >
                            {this.props.dynamic_lang.stock_enquiry_new.check_availability}
                          </button>
                          {/* <button
                            onClick={(e) => this.modalCloseHandler()}
                            className={`btn btn-default btn-cancel`}
                            type="button"
                          >
                            Cancel
                          </button> */}
                        </div>
                        <div className="col-md-12">&nbsp;</div>
                      </div>
                    </Modal.Body>
                  </Form>
                );
              }}
            </Formik>
          </Modal>

          <Modal
            style={{ height: 500 }}
            show={this.state.showModalInfo}
            onHide={() => this.modalInfoCloseHandler()}
            backdrop="static"
            className="stockInfoModal"
          >
            <Modal.Header closeButton>
              <Modal.Title>{this.props.dynamic_lang.stock_enquiry_new.current_stock_availability_data}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <div className="stockDisplayBox">
                <table className="table table-bordered customTable proInfo">
                  <tbody>
                    <tr>
                      <td width="25%" height="27" className="tablePrimaryBg"><strong>{this.props.dynamic_lang.stock_enquiry_new.slno}</strong></td>
                      <td width="25%" height="27" className="tablePrimaryBg"><strong>{this.props.dynamic_lang.stock_enquiry_new.fbpo_no}</strong></td>
                      <td width="25%" className="tablePrimaryBg"><strong>{this.props.dynamic_lang.stock_enquiry_new.quantity}</strong></td>
                      <td width="25%" className="tablePrimaryBg"><strong>{this.props.dynamic_lang.stock_enquiry_new.expected_delivery}</strong></td>
                    </tr>
                    {this.state.dynamic_stock_info_html}
                  </tbody>
                </table>
              </div>

            </Modal.Body>
          </Modal>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    customerList: state.user.customerList,
    dynamic_lang: state.auth.dynamic_lang,
    show_stock_enquiry: state.auth.show_stock_enquiry,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(StockEnquiry);