import React, { Component, Alert } from "react";

import * as Yup from "yup";

import FormComponent from "../../FormComponent/formComponent";
import { connect } from "react-redux";

//validation
const initialValues = {
  product_id: "",
  country_id: "",
  quantity: "",
  unit: "",
  messages: "",
  file: "",
  file_name: [],
};

const validationSchema = (refObj) =>
  Yup.object().shape({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
    country_id: Yup.string()
      .nullable()
      .required(refObj.props.dynamic_lang.display_error.form_error.country_id),
    quantity: Yup.string().trim()
      .required(refObj.props.dynamic_lang.display_error.form_error.quantity)
      .test(
        "customCheck1",
        refObj.props.dynamic_lang.display_error.form_error.quantity_check,
        function (value) {
          if (value == 0) {
            return false;
          }
          return true;
        }
      )
      .matches(
        /^(\d*\.)?\d+$/,
        refObj.props.dynamic_lang.display_error.form_error.quantity_match
      ),
    unit: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.unit
    ),
  });

class RequestPriceQuoteForm extends Component {
  state = {
    placeHolderText: this.props.dynamic_lang.form_component
      .select_market_required,
    apiPath: "tasks/price-quote",
    displayCountry: true,
    heading: this.props.dynamic_lang.form_component
      .request_for_price_quote_request,
    request_type_heading: "Request for a Price Quote",
    labelAttachment: this.props.dynamic_lang.form_component.attachment,
    initialValue: initialValues,
    disPlayPharmacopoeia: false,
    customClassName: "col-md-3",
    disPlayPolymorPhicForm: false,
    disPlatDMFNumber: false,
    displayRequested_date_response: false,
    displayQuantity: true,
    displayUnit: true,
  };

  componentDidMount() {}

  render() {
    return (
      <FormComponent
        {...this.props}
        state={this.state}
        validationSchema={() => validationSchema(this)}
      />
    );
  }
}

//export default RequestPriceQuoteForm;
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RequestPriceQuoteForm);
