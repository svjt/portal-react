import React, { Component, Alert } from "react";

import * as Yup from "yup";

import FormComponent from "../../FormComponent/formComponent";
import { connect } from "react-redux";

//validation
const initialValues = {
  product_id: "",
  country_id: "",
  Pharmacopoeia: "",
  messages: "",
  file: "",
  polymorphicForm: "",
  file_name: [],
};

const validationSchema = (refObj) =>
  Yup.object().shape({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
  });

class TypicalCoaForm extends Component {
  state = {
    placeHolderText: this.props.dynamic_lang.form_component.select_market,
    displayCountry: true,
    apiPath: "tasks/typical-coa",
    heading: this.props.dynamic_lang.form_component.typical_coa,
    request_type_heading: "Typical CoA",
    initialValue: initialValues,
    labelAttachment: this.props.dynamic_lang.form_component.attachment,
    disPlayPharmacopoeia: true,
    customClassName: "col-md-3",
    disPlayPolymorPhicForm: true,
  };

  componentDidMount() {
    //
  }

  render() {
    return (
      <FormComponent
        {...this.props}
        state={this.state}
        validationSchema={() => validationSchema(this)}
      />
    );
  }
}

//export default TypicalCoaForm;
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(TypicalCoaForm);
