import React, { Component } from 'react';
import Layout from '../../Layout/layout';
import { UncontrolledPopover, PopoverHeader, PopoverBody } from 'reactstrap';
import infoIcon from '../../../assets/images/info-circle-solid.svg';

class WorkingStandardForm extends Component {

    state = {
        display_samp: 'none',
        display_wstd: 'none',
        display_impr: 'none',
        product_arr: [
            { value: '1', label: 'product1' },
            { value: '2', label: 'product2' },
            { value: '3', label: 'product3' },
            { value: '4', label: 'product4' }
        ],
        market_arr: [
            { value: '1', label: 'India' },
            { value: '2', label: 'England' },
            { value: '3', label: 'Australia' },
            { value: '4', label: 'New Zealand' }
        ],
        cc_users_arr: [
            { value: '1', label: 'Prabhat' },
            { value: '2', label: 'Uttam' },
            { value: '3', label: 'Satyajit' },
            { value: '4', label: 'Debraj' }
        ],
        unit_arr: [
            { value: 'mh', label: 'Mg' },
            { value: 'gm', label: 'Gm' },
            { value: 'kg', label: 'Kg' }
        ],
    }
    changeSample(event) {
        console.log(event.target.checked);
        if(event.target.checked) {
            this.setState({display_samp: ''})
        }
        else {
            this.setState({display_samp: 'none'})
        }
        
    }
    changeWorkingStandard(event) {
        console.log(event.target.checked);
        if(event.target.checked) {
            this.setState({display_wstd: ''})
        }
        else {
            this.setState({display_wstd: 'none'})
        }
        
    }
    changeImpurities(event) {
        console.log(event.target.checked);
        if(event.target.checked) {
            this.setState({display_impr: ''})
        }
        else {
            this.setState({display_impr: 'none'})
        }
        
    }

    componentDidMount() {
       document.title = `Samples/Working Standards/Impurities`;
      }

    render() {
        return (
                
                <div className="container-fluid clearfix">
                    <div className="dashboard-content-sec">
                        <div className="service-request-form-sec">
                            <div className="form-page-title-block">
                                <h2>Samples/Working Standards/Impurities</h2>
                            </div>
                            <div className="row">
                                <div className="col-md-4">
                                    <select className="form-control customeSelect" >
                                        <option></option>
                                    </select>
                                </div>
                                <div className="col-md-4">
                                    <select className="form-control customeSelect" >
                                        <option></option>
                                    </select>
                                </div>
                                <div className="col-md-4">
                                    <input type="text" className="form-control customInput" />
                                </div>
                            </div>
                            <div className="row">
                                    <div className="col-md-4">
                                        <input type="checkbox" onChange={e => this.changeSample(e)}/>
                                        <label>Samples</label>
                                    </div>
                                </div>
                                <div className="row" style={{ display: this.state.display_samp }}>
                                    <div className="col-md-4">
                                    <label>Number of Batches</label>
                                    <div className="form-control">
                                        <input type="radio" name="site_name" value="1" />1
                                        <input type="radio" name="site_name" value="2" />2
                                        <input type="radio" name="site_name" value="3" />3
                                    </div>
                                    </div>
                                    <div className="col-md-4">
                                        <label>Quantity</label>
                                        <input type="text" pattern="^-?[0-9]\d*\.?\d*$" className="form-control customInput" placeholder="Quantity"/>
                                    </div>
                                    <div className="col-md-4">
                                        <label>Select Unit</label>
                                        <select className="form-control customeSelect" >
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-4">
                                        <input type="checkbox" onChange={e => this.changeWorkingStandard(e)}/>
                                        <label>Working Standard</label>
                                    </div>
                                </div>
                                <div className="row" style={{ display: this.state.display_wstd }}>
                                    <div className="col-md-4">
                                        <label>Quantity</label>
                                        <input type="text" pattern="^-?[0-9]\d*\.?\d*$" className="form-control customInput" placeholder="Quantity"/>
                                    </div>
                                    <div className="col-md-4">
                                        <label>Select Unit</label>
                                        <select className="form-control customeSelect" >
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-md-4">
                                        <input type="checkbox" onChange={e => this.changeImpurities(e)}/>
                                        <label>Impurities</label>
                                    </div>
                                </div>
                                <div className="row" style={{ display: this.state.display_impr }}>
                                    <div className="col-md-4">
                                        <label>Quantity</label>
                                        <input type="text" pattern="^-?[0-9]\d*\.?\d*$" className="form-control customInput" placeholder="Quantity"/>
                                    </div>
                                    <div className="col-md-4">
                                        <label>Select Unit</label>
                                        <select className="form-control customeSelect" >
                                            <option></option>
                                        </select>
                                    </div>
                                </div>
                            <div className="row">
                                <div className="col-md-12">
                                    <textarea name="description" rows="5" cols="60" placeholder="Enter Any Other Requirements" className="customTextarea"></textarea>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-4">
                                    <input type="file" className="customFile" />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-12"> 
                                    <span className="fieldset-legend">Grant access for this request<img src={infoIcon} width="20" height="20" id="UncontrolledPopover" type="button" />
                                        <UncontrolledPopover placement="right" target="UncontrolledPopover">
                                            <PopoverHeader>Popover Title</PopoverHeader>
                                            <PopoverBody>Select users within your organisation who can view and take action on this request.</PopoverBody>
                                        </UncontrolledPopover>
                                    </span>
                                    <div className="form-check">
                                        <label className="form-check-label">
                                            <input type="checkbox" className="form-check-input" value="" />
                                            Option 1
                                        </label>
                                    </div>
                                    <div className="form-check">
                                        <label className="form-check-label">
                                            <input type="checkbox" className="form-check-input" value="" />
                                            Option 2
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col text-center">
                                    <button className="btn btn-default">Submit</button>
                                    <button className="btn btn-default btn-cancel">Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}

export default WorkingStandardForm;