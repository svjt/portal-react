import React, { Component, Alert } from "react";

import * as Yup from "yup";
import FormComponent from "../../FormComponent/formComponent";
import { connect } from "react-redux";

//validation
const initialValues = {
  product_id: "",
  country_id: "",
  stabilityType: "",
  messages: "",
  file: "",
  file_name: [],
};

const validationSchema = (refObj) =>
  Yup.object().shape({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
    stabilityType: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.type
    ),
  });

class AuditVisitRequestForm extends Component {
  state = {
    placeHolderText: this.props.dynamic_lang.form_component.select_market,
    displayCountry: true,
    apiPath: "tasks/stability-data",
    heading: this.props.dynamic_lang.form_component.stability_data,
    request_type_heading: "Stability Data",
    labelAttachment: this.props.dynamic_lang.form_component.attachment,
    initialValue: initialValues,
    disPlayPharmacopoeia: false,
    customClassName: "col-md-4",
    disPlayPolymorPhicForm: false,
    auditDate: false,
    siteName: false,
    selectedType: true,
  };

  componentDidMount() {}

  render() {
    return (
      <FormComponent
        {...this.props}
        state={this.state}
        validationSchema={() => validationSchema(this)}
      />
    );
  }
}

//export default AuditVisitRequestForm;
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuditVisitRequestForm);
