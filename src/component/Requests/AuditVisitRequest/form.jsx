import React, { Component, Alert } from "react";

import * as Yup from "yup";

import FormComponent from "../../FormComponent/formComponent";
import { connect } from "react-redux";

//validation
const initialValues = {
  product_id: "",
  country_id: "",
  siteName: "",
  auditDate: "",

  messages: "",
  file: "",
  file_name: [],
};

const validationSchema = (refObj) =>
  Yup.object().shape({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
    siteName: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.site_name
    ),
    auditDate: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.audit_visit_date
    ),
  });

class AuditVisitRequestForm extends Component {
  state = {
    placeHolderText: this.props.dynamic_lang.form_component.select_market,
    displayCountry: true,
    apiPath: "tasks/audit-visit",
    heading: this.props.dynamic_lang.form_component.audit_visit_request,
    request_type_heading: "Audit/Visit Request",
    labelAttachment: this.props.dynamic_lang.form_component.attachment,
    initialValue: initialValues,
    disPlayPharmacopoeia: false,
    customClassName: "col-md-3",
    disPlayPolymorPhicForm: false,
    auditDate: true,
    siteName: true,
  };

  componentDidMount() {}

  render() {
    return (
      <FormComponent
        {...this.props}
        state={this.state}
        validationSchema={() => validationSchema(this)}
      />
    );
  }
}

//export default AuditVisitRequestForm;
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuditVisitRequestForm);
