import React, { Component, Alert } from "react";

import * as Yup from "yup";
import FormComponent from "../../FormComponent/formComponent";
import { connect } from "react-redux";

//validation
const initialValues = {
  product_id: "",
  country_id: "",
  messages: "",

  file: "",
  file_name: [],
};

const validationSchema = (refObj) =>
  Yup.object().shape({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
    file_name: Yup.string()
      //.required("Please upload determination file")
      .test(
        "required",
        refObj.props.dynamic_lang.display_error.form_error.file_name,
        function (value) {
          return value ? true : false;
        }
      ),
  });

class VendorQuestionnaireForm extends Component {
  state = {
    placeHolderText: this.props.dynamic_lang.form_component.select_market,
    displayCountry: true,
    apiPath: "tasks/vendor",
    heading: this.props.dynamic_lang.form_component.vendor_questionnaire,
    request_type_heading: "Vendor Questionnaire",
    labelAttachment: this.props.dynamic_lang.form_component
      .attach_your_questionnaire,
    astrixRequired: true,
    initialValue: initialValues,
    disPlayPharmacopoeia: false,
    customClassName: "col-md-6",
    disPlayPolymorPhicForm: false,
  };

  componentDidMount() {}

  render() {
    return (
      <FormComponent
        {...this.props}
        state={this.state}
        validationSchema={() => validationSchema(this)}
      />
    );
  }
}

//export default VendorQuestionnaireForm;
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VendorQuestionnaireForm);
