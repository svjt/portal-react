import React, { Component, Alert } from "react";

import * as Yup from "yup";

import FormComponent from "../../FormComponent/formComponent";
import { connect } from "react-redux";

//validation
const initialValues = {
  product_id: "",
  country_id: "",
  Pharmacopoeia: "",
  requested_date_response: "",
  messages: "",
  file: "",
  file_name: [],
};

const validationSchema = (refObj) =>
  Yup.object().shape({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
  });

class QualityOverallSummaryForm extends Component {
  state = {
    placeHolderText: this.props.dynamic_lang.form_component.select_market,
    displayCountry: true,
    apiPath: "tasks/qos",
    heading: this.props.dynamic_lang.form_component.quality_overall_summary,
    request_type_heading: "Quality Overall Summary (QOS)",
    labelAttachment: this.props.dynamic_lang.form_component.attachment,
    initialValue: initialValues,
    disPlayPharmacopoeia: true,
    customClassName: "col-md-4",
    disPlayPolymorPhicForm: false,
    disPlatDMFNumber: false,
    displayRequested_date_response: false,
  };

  componentDidMount() {}

  render() {
    return (
      <FormComponent
        {...this.props}
        state={this.state}
        validationSchema={() => validationSchema(this)}
      />
    );
  }
}

//export default QualityOverallSummaryForm;
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QualityOverallSummaryForm);
