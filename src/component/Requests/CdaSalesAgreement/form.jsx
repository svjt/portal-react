import React, { Component, Alert } from "react";

import * as Yup from "yup";

import FormComponent from "../../FormComponent/formComponent";
import { connect } from "react-redux";

//validation
const initialValues = {
  product_id: "",
  country_id: "",

  messages: "",
  file: "",
  file_name: [],
};

const validationSchema = (refObj) =>
  Yup.object().shape({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
  });

class CdaSalesAgreementForm extends Component {
  state = {
    placeHolderText: this.props.dynamic_lang.form_component.select_market,
    displayCountry: true,
    apiPath: "tasks/cda-sales-agreement",
    heading: this.props.dynamic_lang.form_component.cda_sales_sagreement,
    request_type_heading: "CDA/Sales Agreement",
    labelAttachment: this.props.dynamic_lang.form_component
      .upload_sales_agreement,
    initialValue: initialValues,
    disPlayPharmacopoeia: false,
    customClassName: "col-md-4",
    disPlayPolymorPhicForm: false,
    downloadFile: true,
  };

  componentDidMount() {}

  render() {
    return (
      <FormComponent
        {...this.props}
        state={this.state}
        validationSchema={() => validationSchema(this)}
      />
    );
  }
}

//export default CdaSalesAgreementForm;
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CdaSalesAgreementForm);
