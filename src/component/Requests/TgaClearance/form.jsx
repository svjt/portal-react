import React, { Component, Alert } from "react";

import * as Yup from "yup";

import FormComponent from "../../FormComponent/formComponent";
import { connect } from "react-redux";

//validation
const initialValues = {
  product_id: "",
  gmp_clearance_id: "",
  tga_email_id: "",
  req_deadline: "",
  applicant_name: "",
  doc_required: "",
  messages: "",
  file: "",
  file_name: [],
};

const validationSchema = (refObj) =>
  Yup.object().shape({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
    gmp_clearance_id: Yup.string().trim().required(
      refObj.props.dynamic_lang.display_error.form_error.gmp_id
    ),
    tga_email_id: Yup.string().trim()
      .required(refObj.props.dynamic_lang.display_error.form_error.email)
      .email(refObj.props.dynamic_lang.display_error.form_error.valid_email)
      .max(100, refObj.props.dynamic_lang.display_error.form_error.email_max),
    applicant_name: Yup.string().trim().required(
      refObj.props.dynamic_lang.display_error.form_error.applicant_name
    ),
    doc_required: Yup.string().trim().required(
      refObj.props.dynamic_lang.display_error.form_error.doc_required
    ),
  });

class TgaClearance extends Component {
  state = {
    placeHolderText: this.props.dynamic_lang.form_component.select_market,
    displayCountry: false,
    displayGmpClearanceId: true,
    displayTgaEmailId: true,
    requestDeadline: true,
    displayApplicantName: true,
    displayDocReq: true,
    apiPath: "tasks/tga",
    heading: this.props.dynamic_lang.form_component.tga_gmp_learance_documents,
    request_type_heading: "TGA GMP Clearance Documents",
    labelAttachment: this.props.dynamic_lang.form_component.attachment,
    initialValue: initialValues,
    disPlayPharmacopoeia: false,
    customClassName: "col-md-3",
    disPlayPolymorPhicForm: false,
    auditDate: false,
    siteName: false,
  };

  componentDidMount() {}

  render() {
    return (
      <FormComponent
        {...this.props}
        state={this.state}
        validationSchema={() => validationSchema(this)}
      />
    );
  }
}

//export default TgaClearance;
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(TgaClearance);
