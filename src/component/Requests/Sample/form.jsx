import React, { Component, Alert } from "react";
import { Link } from "react-router-dom";
import Layout from "../../Layout/layout";
import Select from "react-select";
import closeIcon from "../../../assets/images/times-solid-red.svg";

import { Tooltip } from "reactstrap";
import infoIcon from "../../../assets/images/info-circle-solid.svg";
import axios from "../../../shared/axios";
import { Formik, Field, Form, yupToFormErrors } from "formik";
import * as Yup from "yup";
import {
  htmlDecode,
  supportedFileType,
  inArray,
  BasicUserData,
} from "../../../shared/helper";
import { Editor } from "@tinymce/tinymce-react";
import Dropzone from "react-dropzone";
import Loader from "../../Loader/loader";
import { connect } from "react-redux";
import ReactHtmlParser from "react-html-parser";

var path = require("path");
var cc_Customer = [];

var errorList = [];

//validation
let initialValues = {
  product_id: "",
  country_id: "",
  PharmacopoeiaValue: "",
  messages: "",
  file: "",
  file_name: [],

  shippingAddress: "",
  share_with_agent: 1,
};

const options = [
  { value: "Mgs", label: "Mgs" },
  { value: "Gms", label: "Grams" },
  { value: "Kgs", label: "Kgs" },
];

// const validationSchema = Yup.object().shape({
//   product_id: Yup.string().required("Product Name field is required."),
//   shippingAddress: Yup.string().required("Shipping Address field is required."),
//   samples: Yup.boolean(),
//   workingStandards: Yup.boolean(),
//   impurities: Yup.boolean()

// });

const validationSchema = (refObj) =>
  Yup.object({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
    shippingAddress: Yup.string().trim().required(
      refObj.props.dynamic_lang.display_error.form_error.shipping_address
    ),
  });

class Sample extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      tooltipOpen: false,
      selectedOption: null,
      products: [],
      units: [],
      countryList: [],
      selectedCountry: null,
      files: [],
      filesHtml: "",
      rejectedFile: [],
      cc_Customers: [],
      share_with_agent: true,
      errMsg: null,
      errMsgList: [],

      showLoader: false,

      shippingAddress: "",
      isSubmited: false,
      customerList: [],
      ccAgentCustomerList: [],
      displayCCList: false,
      pre_selected: [],
      agent_customer_id: null,
      selProducts: null,
      SWIhtml: "",
    };
    this.inputRef = React.createRef();
  }

  getProductUnit() {
    axios
      .get("/product_unit")
      .then((res) => {
        var product_unit_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          product_unit_list.push({
            value: element["unit_id"],
            label: htmlDecode(element["unit_name"]),
          });
        }
        this.setState({ units: product_unit_list });
      })
      .catch((err) => {});
  }

  deleteFile = (e, setFieldValue) => {
    this.inputRef.current.value = null; // clear input value - SATYAJIT
    e.preventDefault();
    var tempArray = [];

    this.state.files.map((file, fileIndex) => {
      if (file.checked === true) {
      } else {
        tempArray.push(file);
      }
    });

    setFieldValue("file_name", tempArray);

    this.setState({ files: tempArray });
  };
  customerChecked = (event) => {
    var tempArray = [];

    if (event.target.checked === true) {
      this.state.cc_Customers.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 1;
        }
        tempArray.push(user);
      });
    } else {
      this.state.cc_Customers.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 0;
        }
        tempArray.push(user);
      });
    }
    this.setState({ cc_Customers: tempArray });
  };
  agentcustomerChecked = (event) => {
    var tempArray = [];

    if (event.target.checked === true) {
      this.state.ccAgentCustomerList.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 1;
        }
        tempArray.push(user);
      });
    } else {
      this.state.ccAgentCustomerList.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 0;
        }
        tempArray.push(user);
      });
    }
    this.setState({ ccAgentCustomerList: tempArray });
  };
  recId = (event) => {
    var tempArray = [];

    if (event.target.checked === true) {
      this.state.files.map((file, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          file.checked = event.target.checked;
        }
        tempArray.push(file);
      });
    } else {
      this.state.files.map((file, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          file.checked = event.target.checked;
        }
        tempArray.push(file);
      });
    }

    this.setState({ files: tempArray }, () => {});
  };
  getCc_CustomerList() {
    axios
      .get("tasks/get-cc-customers")
      .then((res) => {
        cc_Customer = res.data.data;
        let pre_selected = res.data.pre_selected;
        this.setState({
          cc_Customers: cc_Customer,
          pre_selected: pre_selected,
        });
      })
      .catch((err) => {});
  }

  componentDidUpdate = (prevProps) => {
    if (this.props.selCompany !== prevProps.selCompany) {
      this.setState({ agent_customer_id: null, ccAgentCustomerList: [] });
    }
  };

  componentDidMount() {
    window.scrollTo(0, 0);
    this.getProductList();
    this.getProductUnit();
    this.getCountryList();
    this.getCc_CustomerList();

    this.setState({ files: [], filesHtml: "" });
    document.title =
      this.props.dynamic_lang.left_menu.samples_working_impurities +
      ` | Dr. Reddy's API`;

    let userData = BasicUserData();
    if (userData.role === 2) {
      this.setState({ displayCCList: true });
    }

    // set product while coming from product catalouge -- SATYAJIT
    if (this.props.history.location.state !== undefined) {
      // this.setState({
      //   selectedOption: {
      //     value: this.props.history.location.state.pid,
      //     label: this.props.history.location.state.pname
      //   }
      // })

      let sel_product = [];

      sel_product.push({
        value: this.props.history.location.state.pid,
        label: this.props.history.location.state.pname,
      });

      initialValues.product_id = sel_product;

      let prev_state = [];

      prev_state.push({
        product_id: this.props.history.location.state.pid,
        product_name: this.props.history.location.state.pname,
        sample: {
          chk: false,
          batch: 0,
          quantity: "",
          unit: [],
        },
        impurities: {
          chk: false,
          specify_impurities: "",
          quantity: "",
          unit: [],
        },
        working_standards: {
          chk: false,
          quantity: "",
          unit: [],
        },
        common_err: "",
      });

      this.setState({ selProducts: prev_state }, () => {
        this.getHtmlSIW(prev_state);
      });
    }
    // ===================//
  }

  getProductList() {
    axios
      .get("/products")
      .then((res) => {
        var product_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          if (
            this.props.history.location.state !== undefined &&
            this.props.history.location.state.pid == element["product_id"]
          ) {
            //DO NOT PUSH
          } else {
            product_list.push({
              value: element["product_id"],
              //label: htmlDecode(element['medicalCondition'])
              label: htmlDecode(element["product_name"]),
            });
          }
        }
        this.setState({ products: product_list });
      })
      .catch((err) => {});
  }
  getProductUnit() {
    axios
      .get("/product_unit")
      .then((res) => {
        var product_unit_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          product_unit_list.push({
            value: element["unit_id"],
            label: htmlDecode(element["unit_name"]),
          });
        }
        this.setState({ units: product_unit_list });
      })
      .catch((err) => {});
  }
  getCountryList() {
    axios
      .get("/country")
      .then((res) => {
        var country_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          country_list.push({
            value: element["country_id"],
            //label: htmlDecode(element['medicalCondition'])
            label: htmlDecode(element["country_name"]),
          });
        }
        this.setState({ countryList: country_list });
      })
      .catch((err) => {});
  }

  fileChangedHandler = (event) => {
    this.setState({ upload_file: event.target.files[0] });
  };

  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue, errors) => {
    var rejectedFiles = [];
    var uploadFile = [];
    var totalfile = acceptedFiles.length;

    for (var index = 0; index < totalfile; index++) {
      var error = 0;
      var filename = acceptedFiles[index].name.toLowerCase();
      var extension_list = supportedFileType();
      var ext_with_dot = path.extname(filename);
      var file_extension = ext_with_dot.split(".").join("");

      var obj = {};

      var fileErrText = this.props.dynamic_lang.display_error.form_error
        .following_extensions;

      if (extension_list.indexOf(file_extension) === -1) {
        error = error + 1;

        if (totalfile > 1) {
          if (index === 0) {
            obj["errorText"] =
              this.props.dynamic_lang.display_error.form_error.error_text_1.replace(
                "[file_name]",
                filename
              ) + fileErrText;
          } else {
            obj["errorText"] =
              this.props.dynamic_lang.display_error.form_error.error_text_2.replace(
                "[file_name]",
                filename
              ) + fileErrText;
          }
        } else {
          obj["errorText"] =
            this.props.dynamic_lang.display_error.form_error.error_text_2.replace(
              "[file_name]",
              filename
            ) + fileErrText;
        }
        rejectedFiles.push(obj);
      }

      if (acceptedFiles[index].size > 50000000) {
        obj[
          "errorText"
        ] = this.props.dynamic_lang.display_error.form_error.allowed_size.replace(
          "[file_name]",
          filename
        );
        error = error + 1;
        rejectedFiles.push(obj);
      }

      if (error === 0) {
        uploadFile.push(acceptedFiles[index]);
        setErrors({ file_name: false });
      }
    }

    // //setErrors({ file_name: false });
    // errors.file_name = [];
    // setFieldValue(this.state.files);

    var prevFiles = this.state.files;
    var newFiles = [];
    if (prevFiles.length > 0) {
      for (let index = 0; index < uploadFile.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (uploadFile[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        if (remove === 0) {
          prevFiles.push(uploadFile[index]);
        }
      }

      prevFiles.map((file) => {
        file.checked = false;
        newFiles.push(file);
      });
    } else {
      uploadFile.map((file) => {
        file.checked = false;
        newFiles.push(file);
      });
    }

    this.setState({
      files: newFiles,
      // filesHtml: fileListHtml
    });

    setFieldValue("file_name", newFiles);

    this.setState({
      rejectedFile: rejectedFiles,
    });
    // }
  };

  handleSubmit = (values, actions) => {
    this.setState({ showLoader: true });
    var cc_Cust = [];
    let err = 0;

    //VALIDATE SAMPLE WORKING INPURITIES
    let swiData = this.state.selProducts;
    //console.log("swiData", swiData);
    for (let index = 0; index < swiData.length; index++) {
      const element = swiData[index];
      element.common_err = "";
      let err_arr = [];

      if (
        element.sample.chk === false &&
        element.impurities.chk === false &&
        element.working_standards.chk === false
      ) {
        err_arr.push(
          this.props.dynamic_lang.display_error.form_error.atleast_one
        );
        err++;
      } else {
        let numbers = /^(\d*\.)?\d+$/;
        if (element.sample.chk === true) {
          if (+element.sample.batch === 0) {
            err_arr.push(
              this.props.dynamic_lang.display_error.form_error.invalid_batch
            );
            err++;
          } else if (!inArray(+element.sample.batch, [1, 2, 3])) {
            err_arr.push(
              this.props.dynamic_lang.display_error.form_error.invalid_batch
            );
            err++;
          }

          if (element.sample.quantity != "" || element.sample.unit.length > 0) {
            if (element.sample.quantity === 0) {
              err_arr.push(
                this.props.dynamic_lang.display_error.form_error
                  .samp_quantity_greater
              );
              err++;
            } else if (!element.sample.quantity.match(numbers)) {
              err_arr.push(
                this.props.dynamic_lang.display_error.form_error
                  .samp_invalid_quantity
              );
              err++;
            }

            if (element.sample.unit.length > 0) {
              if (
                !inArray(element.sample.unit[0].value.toLowerCase(), [
                  "mgs",
                  "gms",
                  "kgs",
                ])
              ) {
                err_arr.push(
                  this.props.dynamic_lang.display_error.form_error
                    .samp_invalid_units
                );
                err++;
              }
            } else {
              err_arr.push(
                this.props.dynamic_lang.display_error.form_error
                  .samp_enter_units
              );
              err++;
            }
          }
        }

        if (element.working_standards.chk === true) {
          if (
            element.working_standards.quantity != "" ||
            element.working_standards.unit.length > 0
          ) {
            if (element.working_standards.quantity === 0) {
              err_arr.push(
                this.props.dynamic_lang.display_error.form_error
                  .work_quantity_greater
              );
              err++;
            } else if (!element.working_standards.quantity.match(numbers)) {
              err_arr.push(
                this.props.dynamic_lang.display_error.form_error
                  .work_invalid_quantity
              );
              err++;
            }

            if (element.working_standards.unit.length > 0) {
              if (
                !inArray(
                  element.working_standards.unit[0].value.toLowerCase(),
                  ["mgs", "gms", "kgs"]
                )
              ) {
                err_arr.push(
                  this.props.dynamic_lang.display_error.form_error
                    .work_invalid_units
                );
                err++;
              }
            } else {
              err_arr.push(
                this.props.dynamic_lang.display_error.form_error
                  .work_enter_units
              );
              err++;
            }
          } else {
            err_arr.push(
              this.props.dynamic_lang.display_error.form_error.work_quan_unit
            );
            err++;
          }
        }

        if (element.impurities.chk === true) {
          if (
            element.impurities.quantity != "" ||
            element.impurities.unit.length > 0
          ) {
            if (element.impurities.quantity === 0) {
              err_arr.push(
                this.props.dynamic_lang.display_error.form_error
                  .impu_quantity_greater
              );
              err++;
            } else if (!element.impurities.quantity.match(numbers)) {
              err_arr.push(
                this.props.dynamic_lang.display_error.form_error
                  .impu_invalid_quantity
              );
              err++;
            }
            if (element.impurities.unit.length > 0) {
              if (
                !inArray(element.impurities.unit[0].value.toLowerCase(), [
                  "mgs",
                  "gms",
                  "kgs",
                ])
              ) {
                err_arr.push(
                  this.props.dynamic_lang.display_error.form_error
                    .impu_invalid_units
                );
                err++;
              }
            } else {
              err_arr.push(
                this.props.dynamic_lang.display_error.form_error
                  .impu_enter_units
              );
              err++;
            }
          } else {
            err_arr.push(
              this.props.dynamic_lang.display_error.form_error.impu_quan_unit
            );
            err++;
          }
        }
      }

      if (err_arr.length > 0) {
        //err_arr.push(`${element.product_name}`);
        element.common_err = `<li>${err_arr.join("</li><li>")}<li>`;
      }
    }

    if (err > 0) {
      this.setState({ selProducts: swiData, showLoader: false }, () => {
        this.getHtmlSIW(swiData);
      });
    } else {
      var formData = new FormData();

      let userData = BasicUserData();
      if (userData.role === 2) {
        if (this.state.ccAgentCustomerList) {
          this.state.ccAgentCustomerList.map((user, index) => {
            if (user.checked === 1) {
              cc_Cust.push({ customer_id: user.customer_id });
            }
          });
        }
        if (
          this.state.agent_customer_id &&
          this.state.agent_customer_id !== null &&
          this.state.agent_customer_id.value > 0
        ) {
          formData.append(
            "agent_customer_id",
            this.state.agent_customer_id.value
          );
        }
      } else {
        this.state.cc_Customers.map((user, index) => {
          if (user.checked === 1) {
            cc_Cust.push({ customer_id: user.customer_id });
          }
        });
      }

      if (this.state.files && this.state.files.length > 0) {
        for (let index = 0; index < this.state.files.length; index++) {
          const element = this.state.files[index];
          formData.append("file", element);
        }
      } else {
        formData.append("file", []);
      }

      formData.append("product_id", JSON.stringify(values.product_id));

      formData.append(
        "country_id",
        values.country_id != "" && values.country_id != null
          ? JSON.stringify([values.country_id])
          : ""
      );
      formData.append("pharmacopoeia", values.PharmacopoeiaValue.trim());

      formData.append("description", values.messages.trim());
      formData.append("shipping_address", values.shippingAddress.trim());
      formData.append(
        "request_type_id",
        "Samples/Working Standards/Impurities"
      );

      formData.append("cc_customers", JSON.stringify(cc_Cust));
      formData.append("share_with_agent", values.share_with_agent);

      formData.append(
        "swi_data",
        this.state.selProducts.length > 0
          ? JSON.stringify(this.state.selProducts)
          : JSON.stringify([])
      );

      axios
        .post("tasks/samples-working-standards", formData)
        .then((res) => {
          if (res.status === 200) {
            this.setState({ showLoader: false });
            this.props.history.push({
              pathname: "/my_requests",

              state: {
                message: this.props.dynamic_lang.form_component
                  .thank_for_request,
              },
            });
          } else {
            alert("Failed to add Task");
          }
        })
        .catch((err) => {
          this.setState({ showLoader: false });

          actions.setSubmitting(false);
          //  alert(err.data.status)

          if (err.data.status === 2) {
            actions.setErrors(err.data.errors);
          } else {
            this.setState({ errMsg: err.data.message });
          }
        });
    }
  };

  removeError = (setErrors) => {
    setErrors({});
  };
  removeFile_Error = () => {
    this.setState({ rejectedFile: [] });
  };
  hideError = () => {
    this.setState({ errMsg: "" });
  };

  sampleClick = (e, setFieldValue) => {
    setFieldValue("sample_unit", "");
    setFieldValue("sample_Quantity", "");

    setFieldValue("samples", e.target.checked);

    if (!e.target.checked) {
      setFieldValue("samples_batch_no", "");
      this.setState({ batchNumber: "" });
    }

    this.setState({ sampleChecked: e.target.checked });
  };
  workingStandardClick = (e, setFieldValue) => {
    setFieldValue("workingstandardUnit", "");
    setFieldValue("workingStandardQuantity", "");

    setFieldValue("workingStandards", e.target.checked);

    setFieldValue("workingstandardUnit", "");
    setFieldValue("workingStandardQuantity", "");

    this.setState({ workingStandardsChecked: e.target.checked });
  };

  ImpuritiesClick = (e, setFieldValue) => {
    setFieldValue("ImpuritiesQuantity", "");
    setFieldValue("impuritiesRequired", "");
    setFieldValue("impuritiesUnit", "");

    setFieldValue("impurities", e.target.checked);
    this.setState({ impuritiesChecked: e.target.checked });
  };

  bachNoClick = (e, setFieldValue) => {
    setFieldValue("samples_batch_no", e.target.value);

    this.setState({ batchNumber: e.target.value });
  };

  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    });
  }

  shareWithAgent = (event, setFieldValue) => {
    if (event.target.checked === true) {
      setFieldValue("share_with_agent", 1);
    } else {
      setFieldValue("share_with_agent", 0);
    }
  };

  getAgentCCList = (event, setFieldValue) => {
    this.setState({ agent_customer_id: event });

    if (event === null || event === "") {
      setFieldValue("agent_customer_id", "");
      this.setState({ ccAgentCustomerList: [] });
    } else {
      axios
        .get(`/agents/cc_customers?custid=${event.value}`)
        .then((res) => {
          var customer_list = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            customer_list.push({
              value: element["customer_id"],
              label: htmlDecode(element["cust_name"]),
            });
          }
          this.setState({
            ccAgentCustomerList: res.data.data,
            pre_selected: res.data.pre_selected,
          });
          setFieldValue("agent_customer_id", event.value);
        })
        .catch((err) => {});
    }
  };

  setCountry = (event, setFieldValue) => {
    setFieldValue("country_id", event);
  };

  selectMultiProducts = (event, setFieldValue) => {
    if (this.props.history.location.state !== undefined) {
      if (event === null) {
        let indexOFProd = this.state.products.findIndex((prod) => {
          return prod.value == this.props.history.location.state.pid;
        });

        if (indexOFProd == "-1") {
          this.state.products.push({
            value: this.props.history.location.state.pid,
            label: this.props.history.location.state.pname,
          });

          this.state.products.sort(function (a, b) {
            var textA = a.label.toUpperCase();
            var textB = b.label.toUpperCase();
            return textA < textB ? -1 : textA > textB ? 1 : 0;
          });
        }
      } else {
        let indexOF = event.findIndex((person) => {
          return person.value == this.props.history.location.state.pid;
        });

        if (indexOF == "-1") {
          let indexOFProd = this.state.products.findIndex((prod) => {
            return prod.value == this.props.history.location.state.pid;
          });

          if (indexOFProd == "-1") {
            this.state.products.push({
              value: this.props.history.location.state.pid,
              label: this.props.history.location.state.pname,
            });
            this.state.products.sort(function (a, b) {
              var textA = a.label.toUpperCase();
              var textB = b.label.toUpperCase();
              return textA < textB ? -1 : textA > textB ? 1 : 0;
            });
          }
        }
      }
    }

    if (event === null || event.length === 0) {
      setFieldValue("product_id", []);
      this.setState({ selProducts: null }, () => {
        this.getHtmlSIW(null);
      });
    } else {
      if (event.length > 5) {
      } else {
        setFieldValue("product_id", event);

        let selProducts = [];
        let prev_state =
          this.state.selProducts != null ? this.state.selProducts : [];
        //ADD NEW PRODUCTS
        for (let index = 0; index < event.length; index++) {
          const element = event[index];

          // let indexOfEvent = event.findIndex((product) => {
          //   return product.value == element.product_id
          // });

          let indexOfPrevState = prev_state.findIndex((product) => {
            return product.product_id == element.value;
          });

          //console.log(element.value, indexOfPrevState);

          if (indexOfPrevState == "-1") {
            prev_state.push({
              product_id: element.value,
              product_name: element.label,
              sample: {
                chk: false,
                batch: 0,
                quantity: "",
                unit: [],
              },
              impurities: {
                chk: false,
                specify_impurities: "",
                quantity: "",
                unit: [],
              },
              working_standards: {
                chk: false,
                quantity: "",
                unit: [],
              },
              common_err: "",
            });
          }

          selProducts.push(element.value);
        }

        //REMOVE EXCESS PRODUCTS
        for (let index = 0; index < prev_state.length; index++) {
          const element = prev_state[index];
          if (!inArray(element.product_id, selProducts)) {
            prev_state.splice(index, 1);
          }
        }
        //console.log("prev_state", prev_state);
        this.setState({ selProducts: prev_state }, () => {
          this.getHtmlSIW(prev_state);
        });
      }
    }
  };

  handleSWI = (event, index, key) => {
    let prev_state = this.state.selProducts;
    prev_state[index].common_err = "";
    if (key === "sample_check") {
      prev_state[index].sample.chk = event.target.checked;
      if (event.target.checked === false) {
        prev_state[index].sample.batch = 0;
        prev_state[index].sample.quantity = "";
        prev_state[index].sample.unit = [];
      }
    }

    if (key === "sample_batch") {
      prev_state[index].sample.batch = event.target.value;
    }

    if (key === "sample_quantity") {
      prev_state[index].sample.quantity = event.target.value;
    }

    if (key === "sample_unit") {
      if (event === null) {
        prev_state[index].sample.unit = [];
      } else {
        prev_state[index].sample.unit = [event];
      }
    }

    if (key === "working_standards_check") {
      prev_state[index].working_standards.chk = event.target.checked;
      if (event.target.checked === false) {
        prev_state[index].working_standards.quantity = "";
        prev_state[index].working_standards.unit = [];
      }
    }

    if (key === "working_standards_quantity") {
      prev_state[index].working_standards.quantity = event.target.value;
    }

    if (key === "working_standards_unit") {
      if (event === null) {
        prev_state[index].working_standards.unit = [];
      } else {
        prev_state[index].working_standards.unit = [event];
      }
    }

    if (key === "impurities_check") {
      prev_state[index].impurities.chk = event.target.checked;
      if (event.target.checked === false) {
        prev_state[index].impurities.specify_impurities = "";
        prev_state[index].impurities.quantity = "";
        prev_state[index].impurities.unit = [];
      }
    }

    if (key === "specify_impurities") {
      prev_state[index].impurities.specify_impurities = event.target.value;
    }

    if (key === "impurities_quantity") {
      prev_state[index].impurities.quantity = event.target.value;
    }

    if (key === "impurities_unit") {
      if (event === null) {
        prev_state[index].impurities.unit = [];
      } else {
        prev_state[index].impurities.unit = [event];
      }
    }

    //console.log('prev_state',prev_state);
    this.setState({ selProducts: prev_state }, () => {
      this.getHtmlSIW(prev_state);
    });
  };

  rmError = (index) => {
    let prev_state = this.state.selProducts;
    prev_state[index].common_err = "";
    this.setState({ selProducts: prev_state }, () => {
      this.getHtmlSIW(prev_state);
    });
  };

  getHtmlSIW = (theState) => {
    //console.log("theState", theState);
    this.setState({ SWIhtml: "" });
    if (theState === null) {
    } else {
      let htmlData = [];
      for (let index = 0; index < theState.length; index++) {
        const element = theState[index];
        htmlData.push(
          <div className="sampleCheckSec" key={`sampleCheckSec${index}`}>
            <div className="row">
              <div className="col-md-12">
                <label className="form-check-label">
                  <b>{element.product_name}</b>
                </label>
              </div>
            </div>
            <div
              className="messageserror"
              style={{
                display: element.common_err !== "" ? "block" : "none",
              }}
            >
              <Link to="#" className="close">
                <img
                  src={closeIcon}
                  width="17.69px"
                  height="22px"
                  onClick={(e) => this.rmError(index)}
                />
              </Link>
              <div>
                <ul className="">
                  {ReactHtmlParser(htmlDecode(element.common_err))}
                </ul>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="form-check">
                  <label className="form-check-label">
                    <Field
                      type="checkbox"
                      className="form-check-input"
                      value="1"
                      defaultChecked={element.sample.chk}
                      onClick={(e) => {
                        this.handleSWI(e, index, "sample_check");
                      }}
                    />
                    {this.props.dynamic_lang.form_component.samples}
                  </label>
                </div>
              </div>

              {element.sample.chk === true ? (
                <>
                  <div className="clearfix" />
                  <div className="col-md-4 my-auto">
                    <span className="info">
                      {this.props.dynamic_lang.form_component.number_of_batches}
                    </span>
                    <label className="radio-inline">
                      <input
                        type="radio"
                        name={`no_batch${element.product_id}`}
                        onClick={(e) => {
                          this.handleSWI(e, index, "sample_batch");
                        }}
                        defaultChecked={
                          element.sample.batch === 1 ? true : false
                        }
                        value="1"
                      />
                      1
                    </label>
                    <label className="radio-inline">
                      <input
                        type="radio"
                        name={`no_batch${element.product_id}`}
                        onClick={(e) => {
                          this.handleSWI(e, index, "sample_batch");
                        }}
                        defaultChecked={
                          element.sample.batch === 2 ? true : false
                        }
                        value="2"
                      />
                      2
                    </label>
                    <label className="radio-inline">
                      <input
                        type="radio"
                        name={`no_batch${element.product_id}`}
                        onClick={(e) => {
                          this.handleSWI(e, index, "sample_batch");
                        }}
                        defaultChecked={
                          element.sample.batch === 3 ? true : false
                        }
                        value="3"
                      />
                      3
                    </label>
                  </div>

                  <div className="col-md-4">
                    <Field
                      type="number"
                      className="customInput"
                      placeholder={
                        this.props.dynamic_lang.form_component.no_quantity
                      }
                      onChange={(e) => {
                        this.handleSWI(e, index, "sample_quantity");
                      }}
                      value={element.sample.quantity}
                    />
                  </div>
                  <div className="col-md-4">
                    <Select
                      value={element.sample.unit}
                      isClearable={true}
                      onChange={(e) => {
                        this.handleSWI(e, index, "sample_unit");
                      }}
                      options={this.state.units}
                      placeholder={
                        this.props.dynamic_lang.form_component.no_unit
                      }
                    />
                  </div>
                </>
              ) : (
                ""
              )}

              <div className="clearfix" />
            </div>

            <div className="row">
              <div className="col-md-12">
                <div className="form-check">
                  <label className="form-check-label">
                    <input
                      type="checkbox"
                      className="form-check-input"
                      value="1"
                      defaultChecked={element.working_standards.chk}
                      onClick={(e) => {
                        this.handleSWI(e, index, "working_standards_check");
                      }}
                    />
                    {this.props.dynamic_lang.form_component.working_standards}
                  </label>
                </div>
              </div>

              {element.working_standards.chk ? (
                <>
                  <div className="clearfix" />

                  <div className="col-md-4">
                    <Field
                      name="workingStandardQuantity"
                      type="number"
                      step="any"
                      className="customInput"
                      placeholder={
                        this.props.dynamic_lang.form_component.quantity
                      }
                      onChange={(e) => {
                        this.handleSWI(e, index, "working_standards_quantity");
                      }}
                      value={element.working_standards.quantity}
                    />
                  </div>
                  <div className="col-md-4">
                    <Select
                      value={element.working_standards.unit}
                      isClearable={true}
                      onChange={(e) => {
                        this.handleSWI(e, index, "working_standards_unit");
                      }}
                      options={this.state.units}
                      placeholder={this.props.dynamic_lang.form_component.unit}
                    />
                  </div>
                </>
              ) : (
                ""
              )}
            </div>

            <div className="row">
              <div className="col-md-12">
                <div className="form-check">
                  <label className="form-check-label">
                    <input
                      type="checkbox"
                      className="form-check-input"
                      value="1"
                      defaultChecked={element.impurities.chk}
                      onClick={(e) => {
                        this.handleSWI(e, index, "impurities_check");
                      }}
                    />
                    {this.props.dynamic_lang.form_component.impurities}
                  </label>
                </div>
              </div>
              {element.impurities.chk ? (
                <>
                  <div className="clearfix" />
                  <div className="col-md-4">
                    <Field
                      name="impuritiesRequired"
                      type="text"
                      className="customInput"
                      placeholder={
                        this.props.dynamic_lang.form_component.specify_impurity
                      }
                      onChange={(e) => {
                        this.handleSWI(e, index, "specify_impurities");
                      }}
                      value={element.impurities.specify_impurities}
                    />
                  </div>
                  <div className="col-md-4">
                    <Field
                      name="ImpuritiesQuantity"
                      type="number"
                      step="any"
                      className="customInput"
                      placeholder={
                        this.props.dynamic_lang.form_component.quantity
                      }
                      onChange={(e) => {
                        this.handleSWI(e, index, "impurities_quantity");
                      }}
                      value={element.impurities.quantity}
                    />
                  </div>
                  <div className="col-md-4">
                    <Select
                      value={element.impurities.unit}
                      isClearable={true}
                      onChange={(e) => {
                        this.handleSWI(e, index, "impurities_unit");
                      }}
                      options={this.state.units}
                      placeholder={this.props.dynamic_lang.form_component.unit}
                    />
                  </div>
                </>
              ) : (
                ""
              )}

              <div className="clearfix" />
            </div>
            <div className="clearfix" />
          </div>
        );
      }
      this.setState({ SWIhtml: htmlData });
    }
  };

  render() {
    const {
      selectedOption,
      products,
      units,
      countryList,
      selectedCountry,
      sampleUnit,
      workingStandardsUnit,
      impuritiesUnit,
      customerList,
      ccAgentCustomerList,
    } = this.state;

    //console.log(this.state);

    return (
      <div className="container-fluid clearfix formSec">
        <div className="dashboard-content-sec">
          <div className="service-request-form-sec newFormSec">
            <div className="form-page-title-block">
              <h2>
                {this.props.dynamic_lang.left_menu.samples_working_impurities}
              </h2>
            </div>
            {this.state.showLoader === true ? <Loader /> : null}
            <Formik
              initialValues={initialValues}
              validationSchema={() => validationSchema(this)}
              onSubmit={this.handleSubmit}
            >
              {({
                errors,
                values,
                touched,
                setErrors,
                setFieldValue,
                setFieldTouched,
              }) => {
                //console.log("errors", values);
                //console.log("touched", touched);
                return (
                  <Form>
                    <div
                      className="messageserror"
                      style={{
                        display:
                          this.state.errMsg &&
                          this.state.errMsg !== null &&
                          this.state.errMsg !== ""
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.hideError()}
                        />
                      </Link>
                      <div>
                        <ul className="">
                          <li className="">{this.state.errMsg}</li>
                          {/* {errors.product_id && touched.product_id ? (
                              <li className="">{errors.product_id}</li>
                            ) : null}
                            {errors.siteName && touched.siteName ? (
                              <li className="">{errors.siteName}</li>
                            ) : null}
                            {errors.auditDate && touched.auditDate ? (
                              <li className="">{errors.auditDate}</li>
                            ) : null} */}
                        </ul>
                      </div>
                    </div>
                    <div
                      className="messageserror"
                      style={{
                        display:
                          (errors.product_id && touched.product_id) ||
                          (errors.shippingAddress && touched.shippingAddress) ||
                          (errors.samples_batch_no &&
                            touched.samples_batch_no) ||
                          (errors.sample_unit && touched.sample_unit) ||
                          (errors.sample_Quantity && touched.sample_Quantity) ||
                          (errors.workingstandardUnit &&
                            touched.workingstandardUnit) ||
                          (errors.workingStandardQuantity &&
                            touched.workingStandardQuantity) ||
                          (errors.impuritiesRequired &&
                            touched.impuritiesRequired) ||
                          (errors.impuritiesUnit &&
                            touched.workingStandardQuantity) ||
                          (errors.ImpuritiesQuantity &&
                            touched.ImpuritiesQuantity) ||
                          (errors.common_swi && touched.samples) ||
                          (errors.message)
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.removeError(setErrors)}
                        />
                      </Link>
                      <div>
                        <ul className="">
                          {errors.product_id && touched.product_id ? (
                            <li className="">{errors.product_id}</li>
                          ) : null}
                          {errors.common_swi ? (
                            <li className="">{errors.common_swi}</li>
                          ) : null}
                          {errors.shippingAddress && touched.shippingAddress ? (
                            <li className="">{errors.shippingAddress}</li>
                          ) : null}

                          {errors.samples_batch_no &&
                          touched.samples_batch_no ? (
                            <li className="">{errors.samples_batch_no}</li>
                          ) : null}
                          {errors.sample_unit && touched.sample_unit ? (
                            <li className="">{errors.sample_unit}</li>
                          ) : null}

                          {errors.workingstandardUnit ? (
                            <li className="">{errors.workingstandardUnit}</li>
                          ) : null}

                          {errors.workingStandardQuantity ? (
                            <li className="">
                              {errors.workingStandardQuantity}
                            </li>
                          ) : null}
                          {errors.impuritiesUnit ? (
                            <li className="">{errors.impuritiesUnit}</li>
                          ) : null}
                          {errors.impuritiesRequired ? (
                            <li className="">{errors.impuritiesRequired}</li>
                          ) : null}

                          {errors.ImpuritiesQuantity ? (
                            <li className="">{errors.ImpuritiesQuantity}</li>
                          ) : null}

                          {errors.sample_Quantity ? (
                            <li className="">{errors.sample_Quantity}</li>
                          ) : null}

                          {errors.message ? (
                            <li className="">{errors.message}</li>
                          ) : null}
                        </ul>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-4">
                        <Select
                          isMulti
                          value={values.product_id}
                          //onChange={this.handleChange}
                          options={products}
                          isSearchable={true}
                          isClearable={true}
                          placeholder={
                            this.props.dynamic_lang.form_component
                              .select_product
                          }
                          className="product_id"
                          name="product_id"
                          onChange={(e) => {
                            this.selectMultiProducts(e, setFieldValue);
                          }}
                        />
                      </div>
                      <div className="col-md-4">
                        <Select
                          
                          onChange={(e) => {
                            this.setCountry(e, setFieldValue);
                          }}
                          options={countryList}
                          isSearchable={true}
                          isClearable={true}
                          name="country_id"
                          placeholder={
                            this.props.dynamic_lang.form_component.select_market
                          }
                        />
                      </div>
                      <div className="col-md-4">
                        <Field
                          name="PharmacopoeiaValue"
                          type="text"
                          className="form-control customInput"
                          placeholder={
                            this.props.dynamic_lang.form_component.pharmacopoeia
                          }
                        />
                      </div>
                    </div>

                    {this.state.SWIhtml}

                    <div className="row">
                      <div className="col-md-12">
                        <Editor
                          name="messages"
                          content={values.messages}
                          init={{
                            menubar: false,
                            branding: false,
                            placeholder: this.props.dynamic_lang.form_component
                              .textarea_requirements,
                            plugins:
                              "link table hr visualblocks code placeholder paste lists  textcolor",
                            paste_data_images: true,
                            paste_use_dialog: true,
                            paste_auto_cleanup_on_paste: false,
                            paste_convert_headers_to_strong: true,
                            paste_strip_class_attributes: "none",
                            paste_remove_spans: false,
                            paste_remove_styles: false,
                            paste_retain_style_properties: "all",
                            toolbar:
                              "bold italic strikethrough superscript subscript | forecolor backcolor  | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | formatselect | visualblocks code | paste ",
                            content_css: ["/css/editor.css"],
                            color_map: [
                              "000000",
                              "Black",
                              "808080",
                              "Gray",
                              "FFFFFF",
                              "White",
                              "FF0000",
                              "Red",
                              "FFFF00",
                              "Yellow",
                              "008000",
                              "Green",
                              "0000FF",
                              "Blue",
                            ]
                          }}
                          onEditorChange={(value) =>
                            setFieldValue("messages", value)
                          }
                        />
                      </div>
                    </div>
                    <br />
                    <div className="row">
                      <div className="col-md-12">
                        <Editor
                          name="shippingAddress"
                          content={values.messages}
                          init={{
                            menubar: false,
                            branding: false,
                            placeholder: this.props.dynamic_lang.form_component
                              .shipping_address,
                            plugins:
                              "link table hr visualblocks code placeholder paste lists textcolor",
                            paste_data_images: true,
                            paste_use_dialog: true,
                            paste_auto_cleanup_on_paste: false,
                            paste_convert_headers_to_strong: true,
                            paste_strip_class_attributes: "none",
                            paste_remove_spans: false,
                            paste_remove_styles: false,
                            paste_retain_style_properties: "all",
                            toolbar:
                              "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | formatselect | visualblocks code | paste ",
                            content_css: ["/css/editor.css"],
                            color_map: [
                              "000000",
                              "Black",
                              "808080",
                              "Gray",
                              "FFFFFF",
                              "White",
                              "FF0000",
                              "Red",
                              "FFFF00",
                              "Yellow",
                              "008000",
                              "Green",
                              "0000FF",
                              "Blue",
                            ]
                          }}
                          onEditorChange={(value) =>
                            setFieldValue("shippingAddress", value)
                          }
                        />
                      </div>
                    </div>
                    <br />
                    <div
                      className="messageserror"
                      style={{
                        display:
                          (this.state.rejectedFile &&
                            this.state.rejectedFile.length) > 0
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.removeFile_Error()}
                        />
                      </Link>
                      {this.state.rejectedFile &&
                        this.state.rejectedFile.map((file, index) => {
                          return (
                            <div key={index}>
                              <ul className="">
                                <li className="">{file.errorText}</li>
                              </ul>
                            </div>
                          );
                        })}
                    </div>

                    {this.state.displayCCList && (
                      <div className="col-md-12">
                        <Select
                          options={
                            this.props.customerList !== null
                              ? this.props.customerList
                              : []
                          }
                          isSearchable={true}
                          isClearable={true}
                          value={this.state.agent_customer_id}
                          placeholder="Select User *"
                          onChange={(e) =>
                            this.getAgentCCList(e, setFieldValue)
                          }
                        />
                      </div>
                    )}
                    <div className="clearfix"></div>

                    {this.state.share_with_agent === true &&
                      this.state.displayCCList === false && (
                        <div className="col-md-12">
                          <div className="form-check">
                            <label className="form-check-label">
                              <input
                                type="checkbox"
                                className="form-check-input"
                                value={1}
                                defaultChecked={true}
                                onChange={(e) =>
                                  this.shareWithAgent(e, setFieldValue)
                                }
                              />
                              {
                                this.props.dynamic_lang.form_component
                                  .display_request_agent
                              }
                            </label>
                          </div>
                        </div>
                      )}

                    <div className="col-md-4">
                      <label className="mb-0">
                        {this.props.dynamic_lang.form_component.attachment}
                      </label>
                    </div>
                    <div className="clearfix" />
                    <div className="row uploadSec">
                      {/* <div className="col-md-4">
                          <input type="file" className="customFile" />
                        </div> */}

                      <div className="col-md-3">
                        <Dropzone
                          onDrop={(acceptedFiles) =>
                            this.setDropZoneFiles(
                              acceptedFiles,
                              setErrors,
                              setFieldValue
                            )
                          }
                        >
                          {({ getRootProps, getInputProps }) => (
                            <section>
                              <div {...getRootProps()} className="customFile">
                                <input
                                  {...getInputProps()}
                                  ref={this.inputRef}
                                  style={{ display: "block" }}
                                />
                                {/* <p>Upload file</p> */}
                              </div>
                              {/* <div className="form-check-input">
                                {this.state.filesHtml}
                              </div> */}
                            </section>
                          )}
                        </Dropzone>
                      </div>
                      <div className="col-md-9 my-auto">
                        {this.state.files &&
                          this.state.files.map((file, index) => {
                            return (
                              <>
                                <div key={index} className="form-check-inline">
                                  <label className="form-check-label">
                                    <input
                                      id={index}
                                      type="checkbox"
                                      className="form-check-input"
                                      value={index}
                                      checked={file.checked}
                                      onChange={(e) => this.recId(e)}
                                    />
                                    {file.name}
                                  </label>
                                </div>
                              </>
                            );
                          })}
                        {this.state.files && this.state.files.length > 0 ? (
                          <button
                            className="button"
                            onClick={(e) => this.deleteFile(e, setFieldValue)}
                          >
                            {
                              this.props.dynamic_lang.form_component
                                .remove_selected
                            }
                          </button>
                        ) : (
                          ""
                        )}
                      </div>
                    </div>

                    {((this.state.displayCCList &&
                      ccAgentCustomerList &&
                      ccAgentCustomerList.length > 0) ||
                      (this.state.pre_selected &&
                        this.state.pre_selected.length > 0) ||
                      (this.state.cc_Customers &&
                        this.state.cc_Customers.length > 0)) && (
                      <div className="row">
                        <div className="col-md-12">
                          <span
                            className="fieldset-legend"
                            id="DisabledAutoHideExample"
                          >
                            {
                              this.props.dynamic_lang.form_component
                                .grant_access_request
                            }
                            <img
                              src={infoIcon}
                              width="15.44"
                              height="18"
                              id="DisabledAutoHideExample"
                              type="button"
                            />
                            <Tooltip
                              placement="right"
                              isOpen={this.state.tooltipOpen}
                              autohide={false}
                              target="DisabledAutoHideExample"
                              toggle={this.toggle}
                            >
                              {
                                this.props.dynamic_lang.form_component
                                  .grant_access_request_tooltip
                              }
                            </Tooltip>
                          </span>

                          {this.state.pre_selected &&
                            this.state.pre_selected.map((cust, index) => {
                              return (
                                <div key={index} className="form-check">
                                  <label className="form-check-label">
                                    <input
                                      id={index}
                                      type="checkbox"
                                      className="form-check-input"
                                      defaultChecked={true}
                                      disabled={true}
                                    />
                                    {htmlDecode(cust.first_name)}{" "}
                                    {htmlDecode(cust.last_name)}
                                  </label>
                                </div>
                              );
                            })}

                          {this.state.displayCCList &&
                            ccAgentCustomerList &&
                            ccAgentCustomerList.map((user, index) => {
                              return (
                                <div key={index} className="form-check">
                                  <label className="form-check-label">
                                    <input
                                      id={index}
                                      type="checkbox"
                                      className="form-check-input"
                                      value={index}
                                      //checked={user.status}
                                      onChange={(e) =>
                                        this.agentcustomerChecked(e)
                                      }
                                    />
                                    {htmlDecode(user.first_name)}{" "}
                                    {htmlDecode(user.last_name)} (
                                    {htmlDecode(user.company_name)})
                                  </label>
                                </div>
                              );
                            })}

                          {this.state.cc_Customers &&
                            this.state.cc_Customers.map((user, index) => {
                              return (
                                <div key={index} className="form-check">
                                  <label className="form-check-label">
                                    <input
                                      id={index}
                                      type="checkbox"
                                      className="form-check-input"
                                      value={index}
                                      //checked={user.status}
                                      onChange={(e) => this.customerChecked(e)}
                                    />
                                    {htmlDecode(user.first_name)}{" "}
                                    {htmlDecode(user.last_name)} (
                                    {htmlDecode(user.company_name)})
                                  </label>
                                </div>
                              );
                            })}
                        </div>
                      </div>
                    )}

                    <div className="row">
                      <div className="col text-center">
                        <button className="btn btn-default">
                          {" "}
                          {this.props.dynamic_lang.form_component.submit}
                        </button>
                        <button
                          className="btn btn-default btn-cancel"
                          onClick={() => this.props.history.push("/dashboard")}
                        >
                          {this.props.dynamic_lang.form_component.cancel}
                        </button>
                      </div>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    customerList: state.user.customerList,
    selCompany: state.user.selCompany,
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Sample);
