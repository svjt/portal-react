import React, { Component, Alert } from "react";

import * as Yup from "yup";

import FormComponent from "../../FormComponent/formComponent";
import { connect } from "react-redux";

//validation
const initialValues = {
  product_id: "",
  country_id: "",
  dmf_number: "",
  req_deadline: "",
  messages: "",
  file: "",
  file_name: [],
};

const validationSchema = (refObj) =>
  Yup.object().shape({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
    country_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.country_id
    ),
    dmf_number: Yup.string().trim().required(
      refObj.props.dynamic_lang.display_error.form_error.dmf_number
    ),
    file_name: Yup.string().test(
      "required",
      refObj.props.dynamic_lang.display_error.form_error.upload_file,
      function (value) {
        return value ? true : false;
      }
    ),
  });

class NitroSoamine extends Component {
  state = {
    placeHolderText: this.props.dynamic_lang.form_component
      .select_market_required,
    displayCountry: true,
    apiPath: "tasks/nitrosoamine",
    heading: this.props.dynamic_lang.form_component.nitrosoamines_declarations,
    request_type_heading: "N-Nitrosoamines Declarations",
    labelAttachment: this.props.dynamic_lang.form_component.attachment,
    astrixRequired: true,
    initialValue: initialValues,
    disPlayPharmacopoeia: false,
    disPlayDMF: true,
    customClassName: "col-md-3",
    disPlayPolymorPhicForm: false,
    auditDate: false,
    siteName: false,
    requestDeadline: true,
  };

  componentDidMount() {}

  render() {
    return (
      <FormComponent
        {...this.props}
        state={this.state}
        validationSchema={() => validationSchema(this)}
      />
    );
  }
}

//export default NitroSoamine;
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(NitroSoamine);
