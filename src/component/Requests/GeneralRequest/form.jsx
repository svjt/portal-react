import React, { Component, Alert } from "react";

import * as Yup from "yup";

import FormComponent from "../../FormComponent/formComponent";
import { connect } from "react-redux";

//validation
const initialValues = {
  product_id: "",
  country_id: "",
  Pharmacopoeia: "",
  messages: "",
  file: "",
  file_name: [],
};

const validationSchema = (refObj) =>
  Yup.object().shape({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
  });

class GeneralRequestForm extends Component {
  state = {
    placeHolderText: this.props.dynamic_lang.form_component.select_market,
    displayCountry: true,
    apiPath: "tasks",
    heading: this.props.dynamic_lang.form_component.general_request,
    request_type_heading: "General Request",
    labelAttachment: this.props.dynamic_lang.form_component.attachment,
    initialValue: initialValues,
    disPlayPharmacopoeia: true,
    customClassName: "col-md-4",
    disPlayPolymorPhicForm: false,
  };

  render() {
    return (
      <FormComponent
        {...this.props}
        state={this.state}
        validationSchema={() => validationSchema(this)}
      />
    );
  }
}

//export default GeneralRequestForm;

const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
    set_lang: state.auth.set_lang
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(GeneralRequestForm);
