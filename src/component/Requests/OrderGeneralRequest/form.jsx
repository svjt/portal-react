import React, { Component } from "react";
import { Link } from "react-router-dom";
// import Layout from "../../Layout/layout";
import Select from "react-select";
import { Tooltip } from "reactstrap";
import infoIcon from "../../../assets/images/info-circle-solid.svg";
// import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { Editor } from "@tinymce/tinymce-react";
import closeIcon from "../../../assets/images/times-solid-red.svg";

// import dateFormat from "dateformat";

import axios from "../../../shared/axios";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import {
  htmlDecode,
  supportedFileType,
  BasicUserData,
} from "../../../shared/helper";
import Dropzone from "react-dropzone";
import Loader from "../../Loader/loader";
import { connect } from "react-redux";
import { isMobile } from "react-device-detect";

var path = require("path");
var cc_Customer = [];

var minDate = "";
var maxDate = "";
//validation
const initialValues = {
  product_id: "",
  country_id: "",
  quantity: "",
  unit: "",
  deliveryDate: "",
  share_with_agent: 1,
  messages: "",
  file: "",
  file_name: [],
  pharmacopoeia: "",
};

const options = [
  { value: "Mgs", label: "Mgs" },
  { value: "Gms", label: "Grams" },
  { value: "Kgs", label: "Kgs" },
];
const validationSchema = (refObj) =>
  Yup.object().shape({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
    country_id: Yup.string()
      .nullable()
      .required(refObj.props.dynamic_lang.display_error.form_error.country_id)
  });

class ProformaInvoice extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      startDate: "",
      tooltipOpen: false,
      showLoader: false,
      selectedOption: null,
      share_with_agent: true,
      products: [],
      units: [],
      countryList: [],
      selectedCountry: null,
      files: [],
      filesHtml: "",
      rejectedFile: [],
      cc_Customers: [],
      selectedUnit: "",
      selectedDate: "",
      errMsg: null,
      customerList: [],
      ccAgentCustomerList: [],
      pre_selected: [],
      selUser: null,
      displayCCList: false,
      agent_customer_id: null,
    };
    this.inputRef = React.createRef();
  }
  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    });
  }

  deleteFile = (e, setFieldValue) => {
    this.inputRef.current.value = null; // clear input value - SATYAJIT
    e.preventDefault();
    var tempArray = [];

    this.state.files.map((file, fileIndex) => {
      if (file.checked === true) {
      } else {
        tempArray.push(file);
      }
    });

    setFieldValue("file_name", tempArray);

    this.setState({ files: tempArray });
  };
  customerChecked = (event) => {
    // console.log(event.target.checked);
    var tempArray = [];

    if (event.target.checked === true) {
      this.state.cc_Customers.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 1;
        }
        tempArray.push(user);
      });
    } else {
      this.state.cc_Customers.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 0;
        }
        tempArray.push(user);
      });
    }
    // console.log("tempArray", tempArray);
    this.setState({ cc_Customers: tempArray });
  };

  agentcustomerChecked = (event) => {
    var tempArray = [];

    if (event.target.checked === true) {
      this.state.ccAgentCustomerList.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 1;
        }
        tempArray.push(user);
      });
    } else {
      this.state.ccAgentCustomerList.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 0;
        }
        tempArray.push(user);
      });
    }
    this.setState({ ccAgentCustomerList: tempArray });
  };
  recId = (event) => {
    var tempArray = [];

    if (event.target.checked === true) {
      this.state.files.map((file, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          // console.log("match");
          file.checked = event.target.checked;
        }
        tempArray.push(file);
        // console.log(tempArray);
      });
    } else {
      this.state.files.map((file, fileIndex) => {
        // console.log("fileIndex", fileIndex);
        // console.log("value", event.target.value);

        if (event.target.value.toString() === fileIndex.toString()) {
          // console.log("match");
          file.checked = event.target.checked;
        }
        tempArray.push(file);
        // console.log(tempArray);
      });
    }

    this.setState({ files: tempArray }, () => {
      console.log(this.state.files);
    });
  };
  getCc_CustomerList() {
    axios
      .get("tasks/get-cc-customers")
      .then((res) => {
        cc_Customer = res.data.data;
        let pre_selected = res.data.pre_selected;
        this.setState({
          cc_Customers: cc_Customer,
          pre_selected: pre_selected,
        });
      })
      .catch((err) => { });
  }

  componentDidUpdate = (prevProps) => {
    if (this.props.selCompany !== prevProps.selCompany) {
      this.setState({ agent_customer_id: null, ccAgentCustomerList: [] });
    }
  };

  componentDidMount() {
    if (isMobile) {
      const datePickers = document.getElementsByClassName(
        "react-datepicker__input-container"
      );
      //console.log('datePickers', datePickers)
      Array.from(datePickers).forEach((el) =>
        el.childNodes[0].setAttribute("readOnly", true)
      );
    }
    this.getProductList();
    this.getCountryList();
    this.getProductUnit();

    minDate = new Date();
    maxDate = new Date(new Date().setFullYear(new Date().getFullYear() + 10));

    this.setState({ files: [], filesHtml: "" });
    document.title =
      this.props.dynamic_lang.form_component.request_for_proforma_invoice +
      ` | Dr. Reddy's API`;

    let userData = BasicUserData();
    if (userData.role === 2) {
      this.setState({ displayCCList: true });
    } else {
      this.getCc_CustomerList();
    }
  }

  getProductList() {
    axios
      .get("/products")
      .then((res) => {
        var product_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          product_list.push({
            value: element["product_id"],
            //label: htmlDecode(element['medicalCondition'])
            label: htmlDecode(element["product_name"]),
          });
        }
        this.setState({ products: product_list });

        // this.setState({ product: res.data.data }, () =>
        //   console.log(JSON.stringify(res.data.data))
        // );
      })
      .catch((err) => { });
  }
  getProductUnit() {
    axios
      .get("/product_unit")
      .then((res) => {
        var product_unit_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          product_unit_list.push({
            value: element["unit_id"],
            label: htmlDecode(element["unit_name"]),
          });
        }
        this.setState({ units: product_unit_list });
      })
      .catch((err) => { });
  }
  getCountryList() {
    axios
      .get("/country")
      .then((res) => {
        var country_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          country_list.push({
            value: element["country_id"],
            //label: htmlDecode(element['medicalCondition'])
            label: htmlDecode(element["country_name"]),
          });
        }
        this.setState({ countryList: country_list });
      })
      .catch((err) => { });
  }
  fileChangedHandler = (event) => {
    this.setState({ upload_file: event.target.files[0] });
  };
  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue, errors) => {
    // console.log(acceptedFiles);
    var rejectedFiles = [];
    var uploadFile = [];

    var totalfile = acceptedFiles.length;

    for (var index = 0; index < totalfile; index++) {
      var error = 0;
      var filename = acceptedFiles[index].name.toLowerCase();
      var extension_list = supportedFileType();
      var ext_with_dot = path.extname(filename);
      var file_extension = ext_with_dot.split(".").join("");

      var obj = {};

      var fileErrText = this.props.dynamic_lang.display_error.form_error
        .following_extensions;

      if (extension_list.indexOf(file_extension) === -1) {
        error = error + 1;

        if (totalfile > 1) {
          if (index === 0) {
            obj["errorText"] =
              this.props.dynamic_lang.display_error.form_error.error_text_1.replace(
                "[file_name]",
                filename
              ) + fileErrText;
          } else {
            obj["errorText"] =
              this.props.dynamic_lang.display_error.form_error.error_text_2.replace(
                "[file_name]",
                filename
              ) + fileErrText;
          }
        } else {
          obj["errorText"] =
            this.props.dynamic_lang.display_error.form_error.error_text_2.replace(
              "[file_name]",
              filename
            ) + fileErrText;
        }
        rejectedFiles.push(obj);
      }

      if (acceptedFiles[index].size > 50000000) {
        obj[
          "errorText"
        ] = this.props.dynamic_lang.display_error.form_error.allowed_size.replace(
          "[file_name]",
          filename
        );
        error = error + 1;
        rejectedFiles.push(obj);
      }

      if (error === 0) {
        uploadFile.push(acceptedFiles[index]);
        setErrors({ file_name: false });
      }
    }

    // //setErrors({ file_name: false });
    // errors.file_name = [];
    //setFieldValue(this.state.files);

    var prevFiles = this.state.files;
    var newFiles = [];
    if (prevFiles.length > 0) {
      for (let index = 0; index < uploadFile.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (uploadFile[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        if (remove === 0) {
          prevFiles.push(uploadFile[index]);
        }
      }

      prevFiles.map((file) => {
        file.checked = false;
        newFiles.push(file);
      });
    } else {
      uploadFile.map((file) => {
        file.checked = false;
        newFiles.push(file);
      });

      // console.log("acceptedFiles", acceptedFiles);
      // console.log("newFiles", newFiles);
    }

    this.setState({
      files: newFiles,
      // filesHtml: fileListHtml
    });
    // console.log("newFiles", newFiles);

    setFieldValue("file_name", newFiles);

    this.setState({
      rejectedFile: rejectedFiles,
    });
    // }
  };
  handleSubmit = (values, actions) => {
    this.setState({ showLoader: true });
    var cc_Cust = [];
    this.setState({ errMsg: "" });

    var formData = new FormData();

    let userData = BasicUserData();
    if (userData.role === 2) {
      if (this.state.ccAgentCustomerList) {
        this.state.ccAgentCustomerList.map((user, index) => {
          if (user.checked === 1) {
            cc_Cust.push({ customer_id: user.customer_id });
          }
        });
      }
      if (
        this.state.agent_customer_id &&
        this.state.agent_customer_id !== null &&
        this.state.agent_customer_id.value > 0
      ) {
        formData.append(
          "agent_customer_id",
          this.state.agent_customer_id.value
        );
      }
    } else {
      this.state.cc_Customers.map((user, index) => {
        if (user.checked === 1) {
          cc_Cust.push({ customer_id: user.customer_id });
        }
      });
    }

    //alert(JSON.stringify(values))
    if (this.state.files && this.state.files.length > 0) {
      for (let index = 0; index < this.state.files.length; index++) {
        const element = this.state.files[index];
        formData.append("file", element);
      }
    } else {
      formData.append("file", []);
    }

    formData.append("product_id", JSON.stringify(values.product_id));
    formData.append(
      "country_id",
      values.country_id != "" && values.country_id != null
        ? JSON.stringify(values.country_id)
        : ""
    );

    formData.append("description", values.messages.trim());
    formData.append("quantity", values.quantity + " " + values.unit);
    formData.append("share_with_agent", values.share_with_agent);

    formData.append("request_type_id", "Order General Request");

    formData.append("cc_customers", JSON.stringify(cc_Cust));
    formData.append("pharmacopoeia", values.pharmacopoeia.trim());

    axios
      .post("tasks/order-general-request", formData)
      .then((res) => {
        if (res.status === 200) {
          this.setState({ showLoader: false });
          this.props.history.push({
            pathname: "/my_requests",

            state: {
              message: this.props.dynamic_lang.my_orders.submitted_successfully,
            },
          });
        } else {
          actions.setErrors(res.data.errors);
        }
      })
      .catch((err) => {
        this.setState({ showLoader: false });

        actions.setSubmitting(false);
        //  alert(err.data.status)

        if (err.data.status === 2) {
          actions.setErrors(err.data.errors);
        } else {
          this.setState({ errMsg: err.data.message });
        }
      });
  };
  handleChange = (selectedUnit) => {
    this.setState({ selectedUnit });
    // console.log(`Option selected:`, selectedUnit);
  };
  changeDate = (event, setFieldValue) => {
    setFieldValue("deliveryDate", event);
  };
  removeError = (setErrors) => {
    setErrors({});
  };
  removeFile_Error = () => {
    this.setState({ rejectedFile: [] });
  };
  hideError = () => {
    this.setState({ errMsg: "" });
  };

  shareWithAgent = (event, setFieldValue) => {
    if (event.target.checked === true) {
      setFieldValue("share_with_agent", 1);
    } else {
      setFieldValue("share_with_agent", 0);
    }
  };

  getAgentCCList = (event, setFieldValue) => {
    this.setState({ agent_customer_id: event });

    if (event === null || event === "") {
      setFieldValue("agent_customer_id", "");
      this.setState({ ccAgentCustomerList: [] });
    } else {
      axios
        .get(`/agents/cc_customers?custid=${event.value}`)
        .then((res) => {
          var customer_list = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            customer_list.push({
              value: element["customer_id"],
              label: htmlDecode(element["cust_name"]),
            });
          }
          this.setState({
            ccAgentCustomerList: res.data.data,
            pre_selected: res.data.pre_selected,
          });
          setFieldValue("agent_customer_id", event.value);
        })
        .catch((err) => { });
    }
  };

  setCountry = (event, setFieldValue) => {
    setFieldValue("country_id", event);
  };

  render() {
    const {
      selectedOption,
      products,
      units,
      countryList,
      selectedCountry,
      selectedUnit,
      selectedDate,
      customerList,
      ccAgentCustomerList,
    } = this.state;
    return (
      <div className="container-fluid clearfix formSec">
        <div className="dashboard-content-sec">
          <div className="service-request-form-sec newFormSec">
            <div className="form-page-title-block">
              <h2>
                {this.props.dynamic_lang.form_component.order_general_request}
              </h2>
            </div>
            {this.state.showLoader === true ? <Loader /> : null}
            <Formik
              initialValues={initialValues}
              validationSchema={() => validationSchema(this)}
              onSubmit={this.handleSubmit}
            >
              {({
                errors,
                values,
                touched,
                setErrors,
                setFieldValue,
                setFieldTouched,
              }) => {
                // console.log("formik values", values);

                return (
                  <Form>
                    <div
                      className="messageserror"
                      style={{
                        display:
                          this.state.errMsg &&
                            this.state.errMsg !== null &&
                            this.state.errMsg !== ""
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.hideError()}
                        />
                      </Link>
                      <div>
                        <ul className="">
                          <li className="">{this.state.errMsg}</li>
                        </ul>
                      </div>
                    </div>
                    <div
                      className="messageserror"
                      style={{
                        display:
                          (errors.product_id && touched.product_id) ||
                            (errors.country_id && touched.country_id) ||
                            (errors.quantity && touched.quantity) ||
                            (errors.unit && touched.unit) ||
                            (errors.file_name && touched.file_name) ||
                            (errors.message) 
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.removeError(setErrors)}
                        />
                      </Link>
                      <div>
                        <ul className="">
                          {errors.product_id && touched.product_id ? (
                            <li className="">{errors.product_id}</li>
                          ) : null}
                          {errors.country_id && touched.country_id ? (
                            <li className="">{errors.country_id}</li>
                          ) : null}
                          {errors.quantity && touched.quantity ? (
                            <li className="">{errors.quantity}</li>
                          ) : null}
                          {errors.unit && touched.unit ? (
                            <li className="">{errors.unit}</li>
                          ) : null}
                          {errors.file_name && touched.file_name ? (
                            <li className="">{errors.file_name}</li>
                          ) : null}
                          {errors.message ? (
                            <li className="">{errors.message}</li>
                          ) : null}
                        </ul>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-3">
                        <Select
                          isMulti
                          value={selectedOption}
                          //onChange={this.handleChange}
                          options={products}
                          isSearchable={true}
                          isClearable={true}
                          placeholder={
                            this.props.dynamic_lang.form_component
                              .select_product
                          }
                          className="product_id"
                          onChange={(e) => {
                            if(e === null || e === ""){
                              setFieldValue("product_id", "");
                              this.setState({ selectedOption: "" });
                            }else if(e.length > 5){
                              //DO NOTHING
                            }else{
                              setFieldValue("product_id", e);
                              this.setState({ selectedOption: e });
                            }
                          }}
                        />
                      </div>
                      <div className="col-md-3">
                        <Select
                          isMulti
                          onChange={(e) => {
                            this.setCountry(e, setFieldValue);
                          }}
                          options={countryList}
                          isSearchable={true}
                          isClearable={true}
                          name="country_id"
                          placeholder={
                            this.props.dynamic_lang.form_component
                              .select_market_required
                          }
                        />
                      </div>
                      <div className="col-md-3">
                        <Field
                          name="quantity"
                          type="number"
                          step="any"
                          className="form-control customInput"
                          placeholder={
                            this.props.dynamic_lang.form_component
                              .no_quantity
                          }
                        />
                      </div>
                      <div className="col-md-3">
                        <Select
                          value={selectedUnit}
                          isClearable={true}
                          onChange={(e) => {
                            if (e === null || e === "") {
                              this.setState({ selectedUnit: "" });

                              setFieldValue("unit", "");
                            } else {
                              this.setState({ selectedUnit: e });

                              setFieldValue("unit", e.value);
                            }
                          }}
                          options={units}
                          placeholder={
                            this.props.dynamic_lang.form_component
                              .no_unit
                          }
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-4">
                        <Field
                          name="pharmacopoeia"
                          type="text"
                          className="form-control customInput"
                          placeholder={
                            this.props.dynamic_lang.form_component.pharmacopoeia
                          }
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-12">
                        <Editor
                          name="messages"
                          content={values.messages}
                          init={{
                            menubar: false,
                            branding: false,
                            placeholder: this.props.dynamic_lang.form_component
                              .textarea_other_requirements,
                            plugins:
                              "link table hr visualblocks code placeholder paste lists textcolor",
                            paste_data_images: true,
                            paste_use_dialog: true,
                            paste_auto_cleanup_on_paste: false,
                            paste_convert_headers_to_strong: true,
                            paste_strip_class_attributes: "none",
                            paste_remove_spans: false,
                            paste_remove_styles: false,
                            paste_retain_style_properties: "all",
                            toolbar:
                              "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | formatselect | visualblocks code | paste ",
                            content_css: ["/css/editor.css"],
                            color_map: [
                              "000000",
                              "Black",
                              "808080",
                              "Gray",
                              "FFFFFF",
                              "White",
                              "FF0000",
                              "Red",
                              "FFFF00",
                              "Yellow",
                              "008000",
                              "Green",
                              "0000FF",
                              "Blue",
                            ]
                          }}
                          onEditorChange={(value) =>
                            setFieldValue("messages", value)
                          }
                        />
                        {/* <Field
                            name="messages"
                            type="textarea"
                            component="textarea"
                            className="form-control customTextarea"
                            placeholder="Enter Any Other Requirements"
                            rows="5"
                            cols="60"
                          /> */}
                      </div>
                    </div>
                    <br />

                    <div
                      className="messageserror"
                      style={{
                        display:
                          (this.state.rejectedFile &&
                            this.state.rejectedFile.length) > 0
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.removeFile_Error()}
                        />
                      </Link>
                      {this.state.rejectedFile &&
                        this.state.rejectedFile.map((file, index) => {
                          return (
                            <div key={index}>
                              <ul className="">
                                <li className="">{file.errorText}</li>
                              </ul>
                            </div>
                          );
                        })}
                    </div>

                    {this.state.displayCCList && (
                      <div className="col-md-12">
                        <Select
                          options={
                            this.props.customerList !== null
                              ? this.props.customerList
                              : []
                          }
                          isSearchable={true}
                          isClearable={true}
                          value={this.state.agent_customer_id}
                          placeholder="Select User *"
                          onChange={(e) =>
                            this.getAgentCCList(e, setFieldValue)
                          }
                        />
                      </div>
                    )}

                    {this.state.share_with_agent === true &&
                      this.state.displayCCList === false && (
                        <div className="row mt-10">
                          <div className="col-md-12">
                            <div className="form-check">
                              <label className="form-check-label">
                                <input
                                  type="checkbox"
                                  className="form-check-input"
                                  value={1}
                                  defaultChecked={true}
                                  onChange={(e) =>
                                    this.shareWithAgent(e, setFieldValue)
                                  }
                                />
                                {
                                  this.props.dynamic_lang.form_component
                                    .display_request_agent
                                }
                              </label>
                            </div>
                          </div>
                        </div>
                      )}

                    <div className="col-md-4">
                      <label className="mb-0">
                        {this.props.dynamic_lang.form_component.attachments}
                      </label>
                    </div>
                    <div className="clearfix" />
                    <div className="row uploadSec">
                      {/* <div className="col-md-4">
                          <input type="file" className="customFile" />
                        </div> */}

                      <div className="col-md-3">
                        <Dropzone
                          onDrop={(acceptedFiles) =>
                            this.setDropZoneFiles(
                              acceptedFiles,
                              setErrors,
                              setFieldValue
                            )
                          }
                        >
                          {({ getRootProps, getInputProps }) => (
                            <section>
                              <div {...getRootProps()} className="customFile">
                                <input
                                  {...getInputProps()}
                                  ref={this.inputRef}
                                  style={{ display: "block" }}
                                />
                              </div>
                            </section>
                          )}
                        </Dropzone>
                      </div>
                      <div className="col-md-9 my-auto">
                        {this.state.files &&
                          this.state.files.map((file, index) => {
                            return (
                              <>
                                <div key={index} className="form-check-inline">
                                  <label className="form-check-label">
                                    <input
                                      id={index}
                                      type="checkbox"
                                      className="form-check-input"
                                      value={index}
                                      checked={file.checked}
                                      onChange={(e) => this.recId(e)}
                                    />
                                    {file.name}
                                  </label>
                                </div>
                              </>
                            );
                          })}
                        {this.state.files && this.state.files.length > 0 ? (
                          <input
                            // type="submit"
                            className="button"
                            value={
                              this.props.dynamic_lang.form_component
                                .remove_selected
                            }
                            onClick={(e) => this.deleteFile(e, setFieldValue)}
                          />
                        ) : (
                            ""
                          )}
                      </div>
                    </div>

                    {((this.state.displayCCList &&
                      ccAgentCustomerList &&
                      ccAgentCustomerList.length > 0) ||
                      (this.state.pre_selected &&
                        this.state.pre_selected.length > 0)) && (
                        <div className="row">
                          <div className="col-md-12">
                            <span
                              className="fieldset-legend"
                              id="DisabledAutoHideExample"
                            >
                              {
                                this.props.dynamic_lang.form_component
                                  .grant_access_request
                              }
                              <img
                                src={infoIcon}
                                width="15.44"
                                height="18"
                                id="DisabledAutoHideExample"
                                type="button"
                              />
                              <Tooltip
                                placement="right"
                                isOpen={this.state.tooltipOpen}
                                autohide={false}
                                target="DisabledAutoHideExample"
                                toggle={this.toggle}
                              >
                                {
                                  this.props.dynamic_lang.form_component
                                    .grant_access_request_tooltip
                                }
                              </Tooltip>
                            </span>

                            {this.state.pre_selected &&
                              this.state.pre_selected.map((cust, index) => {
                                return (
                                  <div key={index} className="form-check">
                                    <label className="form-check-label">
                                      <input
                                        id={index}
                                        type="checkbox"
                                        className="form-check-input"
                                        defaultChecked={true}
                                        disabled={true}
                                      />
                                      {htmlDecode(cust.first_name)}{" "}
                                      {htmlDecode(cust.last_name)}
                                    </label>
                                  </div>
                                );
                              })}

                            {this.state.displayCCList &&
                              ccAgentCustomerList &&
                              ccAgentCustomerList.map((user, index) => {
                                return (
                                  <div key={index} className="form-check">
                                    <label className="form-check-label">
                                      <input
                                        id={index}
                                        type="checkbox"
                                        className="form-check-input"
                                        value={index}
                                        //checked={user.status}
                                        onChange={(e) =>
                                          this.agentcustomerChecked(e)
                                        }
                                      />
                                      {htmlDecode(user.first_name)}{" "}
                                      {htmlDecode(user.last_name)} (
                                    {htmlDecode(user.company_name)})
                                  </label>
                                  </div>
                                );
                              })}
                          </div>
                        </div>
                      )}

                    {this.state.cc_Customers &&
                      this.state.cc_Customers.length > 0 && (
                        <div className="row mt-10">
                          <div className="col-md-12">
                            <span
                              className="fieldset-legend"
                              id="DisabledAutoHideExample"
                            >
                              {
                                this.props.dynamic_lang.form_component
                                  .grant_access_request
                              }
                              <img
                                src={infoIcon}
                                width="15.44"
                                height="18"
                                id="DisabledAutoHideExample"
                                type="button"
                              />
                              <Tooltip
                                placement="right"
                                isOpen={this.state.tooltipOpen}
                                autohide={false}
                                target="DisabledAutoHideExample"
                                toggle={this.toggle}
                              >
                                {
                                  this.props.dynamic_lang.form_component
                                    .grant_access_request_tooltip
                                }
                              </Tooltip>
                            </span>
                            {this.state.cc_Customers &&
                              this.state.cc_Customers.map((user, index) => {
                                return (
                                  <div key={index} className="form-check">
                                    <label className="form-check-label">
                                      <input
                                        id={index}
                                        type="checkbox"
                                        className="form-check-input"
                                        value={index}
                                        //checked={user.status}
                                        onChange={(e) =>
                                          this.customerChecked(e)
                                        }
                                      />
                                      {htmlDecode(user.first_name)}{" "}
                                      {htmlDecode(user.last_name)} (
                                      {htmlDecode(user.company_name)})
                                    </label>
                                  </div>
                                );
                              })}
                          </div>
                        </div>
                      )}

                    <div className="row">
                      <div className="col text-center">
                        <button className="btn btn-default">
                          {this.props.dynamic_lang.form_component.submit}
                        </button>
                        <button
                          className="btn btn-default btn-cancel"
                          onClick={() => this.props.history.push("/dashboard")}
                        >
                          {this.props.dynamic_lang.form_component.cancel}
                        </button>
                      </div>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    customerList: state.user.customerList,
    selCompany: state.user.selCompany,
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(ProformaInvoice);
