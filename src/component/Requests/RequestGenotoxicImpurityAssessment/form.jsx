import React, { Component, Alert } from "react";

import * as Yup from "yup";

import FormComponent from "../../FormComponent/formComponent";
import { connect } from "react-redux";
//validation
const initialValues = {
  product_id: "",
  country_id: "",
  Pharmacopoeia: "",
  messages: "",
  file: "",
  file_name: [],
};
const validationSchema = (refObj) =>
  Yup.object().shape({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
    country_id: Yup.string()
      .nullable()
      .required(refObj.props.dynamic_lang.display_error.form_error.country_id),
  });

class RequestDmfForm extends Component {
  state = {
    placeHolderText: this.props.dynamic_lang.form_component
      .select_market_required,
    displayCountry: true,
    apiPath: "tasks/genotoxic-impurity-assessment",
    heading: this.props.dynamic_lang.form_component
      .genotoxic_impurity_assessment,
    request_type_heading: "Genotoxic Impurity Assessment",
    initialValue: initialValues,
    labelAttachment: this.props.dynamic_lang.form_component.attachment,
    disPlayPharmacopoeia: true,
    customClassName: "col-md-4",
    disPlayPolymorPhicForm: false,
  };

  componentDidMount() {}

  render() {
    return (
      <FormComponent
        {...this.props}
        state={this.state}
        validationSchema={() => validationSchema(this)}
      />
    );
  }
}

//export default RequestDmfForm;
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(RequestDmfForm);
