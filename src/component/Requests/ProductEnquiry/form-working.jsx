import React, { Component, Alert, useContext } from "react";
import { Link } from "react-router-dom";
import { Scrollbars } from 'react-custom-scrollbars';
import Layout from "../../Layout/layout";
import Select from "react-select";
import closeIcon from "../../../assets/images/times-solid-red.svg";

import { Tooltip } from "reactstrap";
import infoIcon from "../../../assets/images/info-circle-solid.svg";
import closeIconSelect from "../../../assets/images/close.svg";
import axios from "../../../shared/axios";
import { Formik, Field, Form, yupToFormErrors } from "formik";
import * as Yup from "yup";
import {
  htmlDecode,
  supportedFileType,
  inArray,
  BasicUserData,
} from "../../../shared/helper";
import { Editor } from "@tinymce/tinymce-react";
import Dropzone from "react-dropzone";
import Loader from "../../Loader/loader";
import { connect, useSelector } from "react-redux";
import ReactHtmlParser from "react-html-parser";
import Pagination from "react-js-pagination";

import {
  TabContent,
  Button,
  Row, Modal, ButtonToolbar, Tabs, Tab
} from "react-bootstrap";

import AccordionContext from 'react-bootstrap/AccordionContext';
//import { useAccordionToggle } from 'react-bootstrap/AccordionToggle'; 

import Autosuggest from "react-autosuggest";

var path = require("path");

let initialValues = {
  requested_for: '',
  product_id: ''
}

class ProductEnquiryAmit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show_confirm_modal: false,
      enquiry_counter: 0,
      product_enquiry_data: [],
      products: [],
      suggestions: [],
      product_value: '',
      selected_requested_for: 1,
      more_info: [],
      more_info_count: 0,
      selected_product: '',
      show_request_button: false,
      popup_data: '',
      open_popup: false,
      data_searched:false,
      open_err_popup:false,
      fa_loader: false,
      dynamic_html:'',
      err_enter:'',
      selected_answer:'',
      select_display_ndc:[],
      select_alternate_display_ndc:'',
      select_display_organization:[],
      no_result_found:'',
      functional_category:'',
      unni:'',
      similarity_index:'',
      requested_for_list: [
        {
          id: 1,
          question : `Want to know more Orange Book details of the particular product?`,
          title    : ` USFDA `,
          //answer   : `Retrieve details from the USFDA Orange Book Details for a product. This will provide information on the player market, NDC (National Drug Code), and the relevant application numbers. If you want more details on the dosage forms, pill details
          //(like color, shape, size), strength, whether RLD is available, please click on Request for more information.`
          answer: `The USFDA Orange Book details information on the market players, NDC (National Drug Code), and the relevant application numbers. For more information on the dosage forms, pill details (color, shape, size), strength, or whether a RLD is available, please click on Request further information`
        },
        {
          id: 3,
          question: 'Want to know the EMA details of the product?',
          title    : `EMA Product Details`,
          //answer: `Retrieve details from the European Medicenes Agency for a product. If you require information on last modified date or indications it is approved for, please click on Request for more information.`
          answer: `If you require information on the last modified date or approved indications, please click on Request further information.`
        },
        {
          id: 2,
          question: 'Want to know the TGA details of the product (Australia)?',
          title    : `TGA Product Details`,
          //answer: `Retrieve details from the Therapeutics Goods Administration (Australia) for a product.`
          answer: `Approvals by the Therapeutics Goods Administration (Australia).`          
        },
        {
          id: 4,
          question: 'What are the details of the API of the product?',
          title    : `API Details`,
          //answer: `Retrieve details from the API DrugBank for a particular API. This will provide information on the product structure & identification, experimental properties, calculated properties, and its pharmcology.`
          answer: `Get more information on the API structure and identification, experimental properties, calculated properties, and pharmacology. Source: API Drugbank`
        },
        {
          id: 5,
          question: 'What are the various patents associated with a product?',
          title    : `Patent Details`,
          //answer: `Retrieve list of all patents associated with a product along with a patent expiry date, with links to the patent offices (USPTO) for dossiers. If you require more information on claims on the patents (independent or dependent claims), please
          //click on Request for more information`
          answer: `Here you can find a list of all patents associated with a product along with a patent expiry date, with links to the patent offices (USPTO) for dossiers. If you require more information on claims on the patents (independent or dependent claims), please write to us by clicking on Request further information.`,
        },
        {
          id: 6,
          question: 'For a particular product, what are the various excipients used?',
          title    : `Excipient Information`,
          //answer: `Retrieve information on a product (please provide either the specific NDC code or the organization). You will receive all the Orange book details of the product, various excipients used with the following details for each excipient: Name, functional
          //category, daily dosage limit, regulatory status, incompatibilities, links for each to Inxight: Drugs`
          answer: `Search by the NDC code or the organization to get further information on for each excipient: Name, functional category, daily dosage limit, regulatory status, incompatibilities, links for each to Inxight: Drugs. Source: Orange Book`
        },
        {
          id: 7,
          question: 'For a particular product, what are the various alternate excipients used?',
          title    : `Alternate Excipient Information`,
          answer: `Retrieve information on a product (please provide either the specific NDC code). You will receive all the Orange book details of the product, various excipients used with the following details for each excipient: Name, functional
          category, daily dosage limit, regulatory status, incompatibilities, links for each to Inxight: Drugs`
        }
        // ,
        // {
        //   id: 7,
        //   question: 'What are alternate excipients that can be used?',
        //   answer:`Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
        // }
      ],
      ndc: [],
      organization: [],
      select_organization: [],
      select_ndc: [],
      select_alternate_ndc: [],
      excipient_name: [],
      function_category: [],
      show_enquiry_loader: false,
      show_products: false,
      activePage: 1,
      activePagePopup:1,
      totalCount: 0,
      itemPerPage: 10
    }

  }

  componentDidMount() {
    window.scrollTo(0, 0);
    document.title = `Product Enquiry | Dr.Reddys API`;

  }

  handleSubmit = (values, actions) => {
    this.setState({ show_confirm_modal: true });
  };

  handleCloseModal = () => {
    this.setState({ show_confirm_modal: false });
  };

  handleSubmitFinal = (values, actions) => {
    this.setState({fa_loader:true});
    let post_obj = { 
      enquiry_type : `${this.state.selected_requested_for}`, 
      product_name : this.state.selected_product,
      comment      : values.comment

    }
    axios
      .post("/fa/request_more_info",post_obj)
      .then((res) => {

        this.props.history.push({
            pathname: "/my_product_enquiries",
            state: {
              message: 'Enquiry submitted successfully'
            }
        });

        this.setState({fa_loader:false});
      })
      .catch((err) => {

    });
  }

  setRequestedFor = (e, setFieldValue) => {
    if (e == null) {
      setFieldValue('requested_for', '');
      this.setState({ show_products: false });
    } else {
      setFieldValue('requested_for', e);

      this.setState({ suggestions: [] }, () => {
        this.setState({ show_products: true });
      });
    }
  }

  onReferenceChange = (event, { newValue }) => {
    
    this.setState(
      {
        product_value: newValue
      }
    );
  };

  // ==== Autosuggest for Reference Query DRL ==== //
  onSuggestionsFetchRequested = ({ value }) => {
    // console.log('value',value);
    const inputValue = value.toLowerCase();
    const inputLength = inputValue.length;

    if (inputLength === 0) {
      return [];
    } else {
      if (inputLength > 2) {
        this.setState({fa_loader:true});
        axios
          .post("/fa/suggest", { id: `${this.state.selected_requested_for}`, txt: inputValue })
          .then((res) => {
            let no_result_found = '';
            if(res.data.data.length == 0){
              no_result_found = 'No Data Found';
            }


            this.setState({suggestions: res.data.data,data_searched:false,more_info:[], selected_product:'', more_info_count:0,fa_loader:false,no_result_found:no_result_found});
            return this.state.suggestions;

          })
          .catch((err) => {

          });
      }
    }
  };

  // ==== Autosuggest for Reference Query DRL ==== //
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  getSuggestionValue = (suggestion) => {
    this.setState({fa_loader:true});
    axios
      .post("/fa/search", { id: `${this.state.selected_requested_for}`, txt: suggestion.label })
      .then((res) => {
        let show_request_button = false;
        let ndc = [];
        let organization = [];
        let alternate_expients_list = [];
        if (this.state.selected_requested_for == 1 || this.state.selected_requested_for == 3 || this.state.selected_requested_for == 5) {
          show_request_button = true;
        }

        if (this.state.selected_requested_for == 6) {
          ndc = res.data.ndc_code;
          organization = res.data.organization;
        }else if(this.state.selected_requested_for == 7){
          ndc = res.data.ndc_code;
          alternate_expients_list = res.data.alternate_expients_list;
        }

        this.setState({ more_info: res.data.data, selected_product: suggestion.label, more_info_count: res.data.cnt, show_request_button: show_request_button, ndc: ndc, organization: organization,alternate_expients_list:alternate_expients_list,fa_loader:false,data_searched:true,select_display_organization:[],select_display_ndc:[],select_alternate_display_ndc:[] });

        document.getElementById('focus-ausuggest-input').blur();

      })
      .catch((err) => {

      });

    return suggestion.label;

  };

  renderSuggestion = (suggestion) => {
    
    return (
      <div style={{ cursor: 'pointer',borderBottom:'1px solid #a2a4a6' }} >
        {suggestion.label}
      </div>
    );
    
  };

  clearAutoSuggest = () => {
    this.setState({ product_value: "",err_enter:"", suggestion: [], more_info: [], more_info_count: 0, activePage: 1,activePagePopup:1, show_request_button: false, ndc: [], organization: [],functional_category:'',unni:'',similarity_index:'',data_searched: false });
  };

  setAlternateExcipientNdcCode = (ncd_event) => {
    // console.log(ncd_event);
    if (ncd_event == null || ncd_event === []) {
      this.setState({ select_alternate_ndc: [],select_alternate_display_ndc:[],functional_category:'',unni:'',similarity_index:'' }, () => this.paginationMoreInfo());
    } else {
      let arr = [ncd_event.value];
      this.setState({ select_alternate_ndc:arr,select_alternate_display_ndc:ncd_event }, () => this.paginationMoreInfo());
    }
  }

  setNdcCode = (ncd_event) => {
    // console.log(ncd_event);
    if (ncd_event == null || ncd_event === []) {
      this.setState({ select_ndc: [],select_display_ndc:[],functional_category:'',unni:'',similarity_index:'' }, () => this.paginationMoreInfo());
    } else {
      let arr = [];
      for (let index = 0; index < ncd_event.length; index++) {
        const element = ncd_event[index];
        arr.push(element.label);
        console.log('element.label', element.label);
      }
      this.setState({ select_ndc: arr,select_display_ndc:ncd_event }, () => this.paginationMoreInfo());
    }
  }

  setOrganization = (org_event) => {
    console.log(org_event);
    if (org_event == null || org_event === []) {
      this.setState({ select_organization: [],select_display_organization:[] }, () => this.paginationMoreInfo());
    } else {
      let arr = [];
      for (let index = 0; index < org_event.length; index++) {
        const element = org_event[index];
        arr.push(element.label);
      }
      this.setState({ select_organization: arr,select_display_organization:org_event }, () => this.paginationMoreInfo());
    }
  }

  dynamicTD = (label,key,value) => {
    if(key == 0){
      return label
    }else{
      return value
    }
  }

  getMoreInfo = (id) => {
    let html = [];
    if (id === 1) {
      //this.setState({show_request_button:true});
      html.push(<>
        <div className="listing-table productEnquiryWrapperShowing">
        <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}>
          <table className="" style={{ width: "100%" }}>
            <thead>
              <tr>
                <th>
                  NDC Code
                </th>
                <th>
                  Application Number
                </th>
                <th>
                  Organization
                </th>
              </tr>
            </thead>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <tr
                  key={key}
                >
                  <td>{request.ndc_code}</td>
                  <td>{request.applicationnumber}</td>
                  <td>{request.organization}</td>
                </tr>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                  </td>
                </tr>
              )}
            </tbody>
          </table>
          </Scrollbars>
        </div>
      </>);
    } else if (id === 2) {
      html.push(<>
        <div className="listing-table">
        <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}>
          <table className="" style={{ width: "100%" }}>
            <thead>
              <tr>
                <th>Product Name</th>
                <th>NDC Code</th>
                <th>NDC Name</th>
                <th>Submission Number</th>
                <th>Sponsor</th>
                <th>Submission Type</th>
                <th>Authorization Status</th>
                <th>Auspar Date</th>
                <th>Publication Date</th>
              </tr>
            </thead>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <tr
                  key={key}
                >
                  <td>{request.product_name}</td>
                  <td>{request.ndc_code}</td>
                  <td>{request.ndc_name}</td>
                  <td>{request.submission_number}</td>
                  <td>{request.sponsor}</td>
                  <td>{request.submission_type}</td>
                  <td>{request.authorization_status}</td>
                  <td>{request.auspar_date}</td>
                  <td>{request.publication_date}</td>
                </tr>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                    </td>
                </tr>
              )}
            </tbody>
          </table>
          </Scrollbars>
        </div>
      </>);
    } else if (id === 3) {
      html.push(<>
        <div className="listing-table">
        <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}>
          <table className="" style={{ width: "100%" }}>
            <thead>
              <tr>
                <th>Product Name</th>
                <th>NDC Code</th>
                <th>Product Number</th>
                <th>Authorisation Status</th>
                <th>Therapeutic Area</th>
                <th>Generic</th>
                <th>Biosimilar</th>
                <th>Additional Monitoring</th>
                <th>Orphan Drug</th>
                <th>Marketing Date</th>
                <th>Pharma Class</th>
                <th>Company Name</th>
              </tr>
            </thead>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <tr
                  key={key}
                >
                  <td>{request.productname}</td>
                  <td>{request.ndc_code}</td>
                  <td>{request.product_number}</td>
                  <td>{request.authorisation_status}</td>
                  <td>{request.therapeutic_area}</td>
                  <td>{request.generic}</td>
                  <td>{request.biosimilar}</td>
                  <td>{request.additional_monitoring}</td>
                  <td>{request.orphan_drug}</td>
                  <td>{request.marketing_date}</td>
                  <td>{request.pharma_class}</td>
                  <td>{request.company_name}</td>

                </tr>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="12" align="center">
                    No Data To Display
                    </td>
                </tr>
              )}
            </tbody>
          </table>
          </Scrollbars>
        </div>
      </>);
    } else if (id === 4) {
      // console.log('more_info_render',this.state.more_info);
      html.push(<>
        <h4>Identification</h4>
        <div className="listing-table api-details-table ">
        <div style={{ width: "100%",  maxHeight: 400, overflow: 'hidden', overflowY: 'scroll'  }}>
          <table style={{ width: "100%" }}>
              <tbody>
                {this.state.more_info.map((request, key) => (
                  <>
                    <tr><td>{this.dynamicTD('Drug Bank ID',request.drugbank_id,key)}</td></tr>
                    <tr><td>CAS Number</td><td>{request.casnum}</td></tr>
                    <tr><td>Smiles</td><td>{request.smiles}</td></tr>
                    <tr><td>IUPAC Name</td><td>{request.iupac_name}</td></tr>
                    <tr><td>Description</td><td>{request.description}</td></tr>
                    <tr><td>Indication</td><td>{request.indication}</td></tr>
                    <tr><td>Direct Parent</td><td>{request.dc_direct_parent}</td></tr>
                    <tr><td>Average Mass</td><td>{request.average_mass}</td></tr>
                    <tr><td>State</td><td>{request.state}</td></tr>
                    <tr><td>Pharm Classes</td><td>{request.pharm_classes}</td></tr>
                  </>
                ))}
                {this.state.more_info.length === 0 && (
                  <tr>
                    <td colSpan="11" align="center">
                      No Data To Display
                      </td>
                  </tr>
                )}
              </tbody>
          </table>
          </div>
         </div>
         <h4>Experimental Properties:</h4>  
         <div className="listing-table api-details-table ">
         <div style={{ width: "100%",  maxHeight: 400, overflow: 'hidden', overflowY: 'scroll'  }}>  
          <table className="" style={{ width: "100%" }}>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <>
                  <tr><td>Molecular Formula X</td><td>{request.molecular_formula_x}</td></tr>
                  <tr><td>Molecular Formula Y</td><td>{request.molecular_formula_y}</td></tr>
                  <tr><td>Logp Y</td><td>{request.logp_y}</td></tr>
                  <tr><td>Logs Y</td><td>{request.logs_y}</td></tr>
                  <tr><td>Calculated Water Solubility</td><td>{request.water_solubility_y}</td></tr>

                  <tr><td>PKA Strongest Basic</td><td>{request.pka_strongest_basic}</td></tr>
                  <tr><td>PKA Strongest Acidic</td><td>{request.pka_strongest_acidic}</td></tr>
                  <tr><td>Boiling Point</td><td>{request.boiling_point}</td></tr>
                  <tr><td>Hydrophobicity</td><td>{request.hydrophobicity}</td></tr>
                  <tr><td>Isoelectric Point</td><td>{request.isoelectric_point}</td></tr>
                  <tr><td>Melting Point</td><td>{request.melting_point}</td></tr>
                  <tr><td>Radioactivity</td><td>{request.radioactivity}</td></tr>
                  <tr><td>Caco2 Permeability</td><td>{request.caco2_permeability}</td></tr>
                </>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                    </td>
                </tr>
              )}
            </tbody>
          </table>
          </div>
          </div>
          <h4>Calculated Properties:</h4> 
          <div className="listing-table api-details-table ">
          <div style={{ width: "100%",  maxHeight: 400, overflow: 'hidden', overflowY: 'scroll'  }}>  
          <table className="" style={{ width: "100%" }}>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <>
                  <tr><td>Water Sol X</td><td>{request.water_sol_x}</td></tr>
                  <tr><td>Monoisotopic Mass</td><td>{request.monoisotopic_mass}</td></tr>
                  <tr><td>H Bond Acceptor Count</td><td>{request.h_bond_acceptor_count}</td></tr>
                  <tr><td>H Bond Donor Count</td><td>{request.h_bond_donor_ch_bond_donor_countount}</td></tr>
                  <tr><td>Molecular Formula</td><td>{request.molecular_formula_y}</td></tr>
                  <tr><td>LogP</td><td>{request.logp_x}</td></tr>
                  <tr><td>LogX</td><td>{request.logs_x}</td></tr>
                  <tr><td>PKA</td><td>{request.pka}</td></tr>
                  <tr><td>Physiological Charge</td><td>{request.physiological_charge}</td></tr>
                  <tr><td>Polar Surface Area</td><td>{request.polar_surface_area_psa}</td></tr>
                  <tr><td>Polarizability</td><td>{request.polarizability}</td></tr>
                  <tr><td>Refractivity</td><td>{request.refractivity}</td></tr>
                  
                </>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                    </td>
                </tr>
              )}
            </tbody>
          </table>
          </div>
         </div>
         <h4>Pharmacology:</h4>  
         <div className="listing-table api-details-table ">
         <div style={{ width: "100%",  maxHeight: 400, overflow: 'hidden', overflowY: 'scroll'  }}>
          <table className="" style={{ width: "100%" }}>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <>
                  <tr><td>Absorption</td><td>{request.absorption}</td></tr>
                  <tr><td>Toxicity</td><td>{request.toxicity}</td></tr>
                  <tr><td>Pharmacodynamics</td><td>{request.pharmacodynamics}</td></tr>
                  <tr><td>Mechanism Of Action</td><td>{request.mechanism_of_action}</td></tr>
                  <tr><td>Volume Of Distribution</td><td>{request.volume_of_distribution}</td></tr>
                  <tr><td>Protein Binding</td><td>{request.protein_binding}</td></tr>
                  <tr><td>Metabolism</td><td>{request.metabolism}</td></tr>
                  <tr><td>Route Of Elimination</td><td>{request.route_of_elimination}</td></tr>
                  <tr><td>Half Life</td><td>{request.half_life}</td></tr>
                  <tr><td>Clearance</td><td>{request.clearance}</td></tr>
                  
                </>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                    </td>
                </tr>
              )}
            </tbody>
          </table>
          </div>
        </div>
      </>);
    } else if (id === 5) {
      html.push(<>
        <div className="listing-table">
        <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}>
          <table className="" style={{ width: "100%" }}>
            <thead>
              <tr>
                <th>Patent Number</th>
                <th>Product Name</th>
                <th>Exclusivity Information</th>
                <th>Submission Date</th>
                <th>Patent Expiry</th>
                <th>DP</th>
                <th>DS</th>
                <th>Current Assignee</th>
                <th>Patent Title</th>
                <th>Global Dossier</th>
              </tr>
            </thead>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <tr
                  key={key}
                >
                  <td>{request.patent_number}</td>
                  <td>{request.productname}</td>
                  <td>{request.exclusivity_information}</td>
                  <td>{request.submission_date}</td>
                  <td>{request.patentexpiry}</td>
                  <td>{request.dp}</td>
                  <td>{request.ds}</td>
                  <td>{request.current_assignee}</td>
                  <td>{request.patent_title}</td>
                  <td><a href={request.global_dossier} target='__blank' >{request.global_dossier}</a></td>
                </tr>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                    </td>
                </tr>
              )}
            </tbody>
          </table>
          </Scrollbars>
        </div>
      </>);
    } else if (id === 6) {
      //this.setState({show_request_button:true});
      html.push(<div className="row">
        {this.state.ndc && this.state.ndc.length > 0 && <div className="col-md-6">
          <label>NDC Code</label>
          <Select
            isMulti
            value={this.state.select_display_ndc}
            options={this.state.ndc}
            isSearchable={true}
            isClearable={true}
            placeholder={
              `Select NDC Code`
            }
            name="ndc"
            onChange={(e) => this.setNdcCode(e)}
          />
        </div>}

        {this.state.organization && this.state.organization.length > 0 && <div className="col-md-6">
          <label>Organization</label>
          <Select
            isMulti
            value={this.state.select_display_organization}
            options={this.state.organization}
            isSearchable={true}
            isClearable={true}
            placeholder={
              `Select Organization`
            }
            name="organization"
            onChange={(e) => this.setOrganization(e)}
          />
        </div>}

      </div>)

      html.push(<>
        <div className="listing-table">
        <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}>
          <table className="" style={{ width: "100%" }}>
            <thead>
              <tr>
                <th>NDC Code</th>
                <th>Application Number</th>
                <th>Organization</th>
                <th>No Of Excepients</th>
                <th>Dosage Form</th>
                <th>Route</th>
                <th>Color</th>
                <th>Shape</th>
                <th>Sizes</th>
                <th>Strength</th>
                <th>RLD</th>
              </tr>
            </thead>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <tr
                  key={key}
                >
                  <td>{request.ndc_code}</td>
                  <td>{request.applicationnumber}</td>
                  <td>{request.organization}</td>
                  <td onClick={() => { this.openExcepientsPopup(request) }} ><span style={{ color: 'blue', cursor: 'pointer', textDecoration: 'underline' }} >{request.no_of_excepients}</span></td>
                  <td>{request.dosage_form}</td>
                  <td>{request.route}</td>
                  <td>{request.color}</td>
                  <td>{request.shape}</td>
                  <td>{request.sizes}</td>
                  <td>{request.strength}</td>
                  <td>{request.rld}</td>
                </tr>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                  </td>
                </tr>
              )}
            </tbody>
          </table>
          </Scrollbars>
        </div>
      </>);



    } else if (id === 7) {
      //this.setState({show_request_button:true});
      html.push(<div className="row">
        {this.state.ndc && this.state.ndc.length > 0 && <div className="col-md-6">
          <label>NDC Code</label>
          <Select
            value={this.state.select_alternate_display_ndc}
            options={this.state.ndc}
            isSearchable={true}
            isClearable={true}
            placeholder={
              `Select NDC Code`
            }
            name="ndc"
            onChange={(e) => this.setAlternateExcipientNdcCode(e)}
          />
        </div>}
      </div>)

      if(this.state.alternate_expients_list.length > 0){

        html.push(<>
          <div className="listing-table">
          <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}>
            <table className="" style={{ width: "100%" }}>
              <thead>
                <tr>
                {this.state.alternate_expients_list.map((request, key) => (
                  <th>{request.excipientname}</th>
                ))}
                </tr>
              </thead>
              <tbody>
                  <tr>
                  {this.state.alternate_expients_list.map((request, key) => (
                    <td>{this.getfunctionCategory(request.functional_category.split(';'),request.excipientname)}</td>
                  ))}
                </tr>
              </tbody>
            </table>
            </Scrollbars>
          </div>
        </>);

      }

      if(this.state.more_info.length > 0){
        html.push(<>
          <div className="listing-table">
          <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}>
            <table className="" style={{ width: "100%" }}>
              <thead>
                <tr>
                  <th>Unii</th>
                  <th>Excipient Name</th>
                  <th>Popularity Score</th>
                  <th>Similarity Score</th>
                  <th>Potency Details</th>
                  
                  
                </tr>
              </thead>
              <tbody>
                {this.state.more_info.map((request, key) => (
                  <tr
                    key={key}
                  >
                    <td>{request.unii}</td>
                    <td>{request.excipient_name}</td>
                    <td>{request.popularity_score}</td>
                    <td>{request.similarity_score}</td>
                    <td>{request.potencydetails}</td>
                  </tr>
                ))}
                {this.state.more_info.length === 0 && (
                  <tr>
                    <td colSpan="11" align="center">
                      No Data To Display
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
            </Scrollbars>
          </div>
        </>);
      }

    }

    if (this.state.more_info_count > this.state.itemPerPage && id !== 7) {
      html.push(
        <div className="pagination-area">
          <div className="paginationOuter text-right">
            <Pagination
              hideDisabled
              hideFirstLastPages
              prevPageText="‹‹"
              nextPageText="››"
              activePage={this.state.activePage}
              itemsCountPerPage={this.state.itemPerPage}
              totalItemsCount={this.state.more_info_count}
              itemClass="nav-item"
              linkClass="nav-link"
              activeClass="active"
              pageRangeDisplayed="1"
              onChange={(e) => this.handlePageChange(e)}
            />
          </div>
        </div>
      );
    }
    return html;

  }

  getfunctionCategory = (request,excipientname) => {
    let html_return = request.map((fc, k) => (
      <tr><td><input type="radio" onClick={()=>this.excipeintRadioButton(fc,excipientname)} />{` `}{fc}</td></tr>
    ));
    let html = [];
    html.push(<table>{html_return}</table>);
    return html;
  }

  excipeintRadioButton = (functional_category,excipient_name) => {

    this.setState({fa_loader:true});
    let obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}`, ndc_code: this.state.select_alternate_ndc,functional_category:functional_category,unni:excipient_name};


    axios
      .post(`/fa/search?page=1`, obj)
      .then((res) => {
        let show_request_button     = false;
        let ndc                     = [];
        let organization            = [];
        let alternate_expients_list = [];
        let similarity_index        = '';

        if (this.state.selected_requested_for == 6) {
          ndc = res.data.ndc_code;
          organization = res.data.organization;
        }else if(this.state.selected_requested_for == 7){
          ndc = res.data.ndc_code;
          alternate_expients_list = res.data.alternate_expients_list;
          similarity_index = res.data.similarity_index;
        }

        this.setState({ more_info: res.data.data, more_info_count: res.data.cnt, show_request_button: show_request_button, ndc: ndc,alternate_expients_list:alternate_expients_list,functional_category:functional_category,unni:excipient_name,similarity_index:similarity_index,fa_loader:false});

      })
      .catch((err) => {

      });

  }

  openExcepientsPopup = (req) => {
    //console.log(req);
    if (req.ndc_code) {
      this.setState({fa_loader:true});
      axios
        .post(`/fa/get_excipients?page=1`, { ndc_code: req.ndc_code })
        .then((res) => {
          if(res.data.data.length > 0){
              let html = [];

              for (let index = 0; index < res.data.data.length; index++) {
                const element = res.data.data[index];

                html.push(<tr key={index}>
                <td>{element.ndc_code}</td>
                <td>{element.unii}</td>
                <td>{element.product_name}</td>
                <td>{element.excipientname}</td>
                <td>{element.dailydosagelimit}</td>
                <td>{element.alternate_excipients}</td>
                <td>{element.functional_category}</td>
                <td>{element.incompatibilities}</td>
                </tr>);
              }

              let rethtml = [];

              rethtml.push(<div className="listing-table popup-excipients-reddy">
                 <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 300, minHeight: 300 }}>
                <table className="" style={{ width: "100%" }}>
                  <thead>
                    <tr>
                      <th>NDC Code</th>
                      <th>UNII</th>
                      <th>Product Name</th>
                      <th>Excipient Name</th>
                      <th>Daily Dosage Limit</th>
                      <th>Alternate Excipients</th>
                      <th>Functional Category</th>
                      <th>Incompatibilities</th>
                    </tr>
                  </thead>
                  <tbody>
                    {html}
                  </tbody>
                </table>
                </Scrollbars>
            </div>)


            if (res.data.cnt > this.state.itemPerPage) {
              
              rethtml.push(<div className="pagination-area" style={{width:'5%'}} >
                  <div className="paginationOuter text-right">
                    <Pagination
                      hideDisabled
                      hideFirstLastPages
                      prevPageText="‹‹"
                      nextPageText="››"
                      activePage={this.state.activePagePopup}
                      itemsCountPerPage={this.state.itemPerPage}
                      totalItemsCount={res.data.cnt}
                      itemClass="nav-item"
                      linkClass="nav-link"
                      activeClass="active"
                      pageRangeDisplayed="1"
                      onChange={(e) => this.handlePageChangePopup(e,req.ndc_code)}
                    />
                  </div>
                </div>)
              
            }


            this.setState({ popup_data: rethtml, open_popup: true,fa_loader:false });

          }
        })
        .catch((err) => {
          let html = [];
          html.push(<div className="listing-table popup-excipients-reddy">
             <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 300, minHeight: 300}}>
              <table className="" style={{ width: "100%" }}>
                <thead>
                  <tr>
                    <th>NDC Code</th>
                    <th>UNII</th>
                    <th>Product Name</th>
                    <th>Excipient Name</th>
                    <th>Daily Dosage Limit</th>
                    <th>Alternate Excipients</th>
                    <th>Functional Category</th>
                    <th>Incompatibilities</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colSpan="11" align="center">
                        No Data To Display
                    </td>
                  </tr>
                </tbody>
              </table>
              </Scrollbars>
          </div>);

          this.setState({ popup_data: html, open_popup: true,fa_loader:false });
        });
    }
  }

  handlePageChangePopup = (pageNumber,ndc_code) => {
    this.setState({ activePagePopup: pageNumber });
    this.paginationMoreInfoPopup(
      pageNumber > 0 ? pageNumber : 1,ndc_code
    );
  };

  paginationMoreInfoPopup = (page = 1,ndc_code = '') => {
    if (ndc_code != '') {
      this.setState({fa_loader:true});
      axios
        .post(`/fa/get_excipients?page=${page}`, { ndc_code: ndc_code })
        .then((res) => {
          if (res.data.data.length > 0) {
            let html = [];

            let dynamic_html = [];

            for (let index = 0; index < res.data.data.length; index++) {
              const element = res.data.data[index];

              dynamic_html.push(<tr key={index}>
                <td>{element.ndc_code}</td>
                <td>{element.unii}</td>
                <td>{element.product_name}</td>
                <td>{element.excipientname}</td>
                <td>{element.dailydosagelimit}</td>
                <td>{element.alternate_excipients}</td>
                <td>{element.functional_category}</td>
                <td>{element.incompatibilities}</td>
              </tr>);

            }

            html.push(<div className="listing-table popup-excipients-reddy">
              <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 300, minHeight: 300 }}>
                <table className="" style={{ width: "100%" }}>
                  <thead>
                    <tr>
                      <th>NDC Code</th>
                      <th>UNII</th>
                      <th>Product Name</th>
                      <th>Excipient Name</th>
                      <th>Daily Dosage Limit</th>
                      <th>Alternate Excipients</th>
                      <th>Functional Category</th>
                      <th>Incompatibilities</th>
                    </tr>
                  </thead>
                  <tbody>
                    {dynamic_html}
                  </tbody>
                </table>
                </Scrollbars>
            </div>);


            if (res.data.cnt > this.state.itemPerPage) {
              
              html.push( <div className="pagination-area" style={{width:'5%'}} >
                  <div className="paginationOuter text-right">
                    <Pagination
                      hideDisabled
                      hideFirstLastPages
                      prevPageText="‹‹"
                      nextPageText="››"
                      activePage={this.state.activePagePopup}
                      itemsCountPerPage={this.state.itemPerPage}
                      totalItemsCount={res.data.cnt}
                      itemClass="nav-item"
                      linkClass="nav-link"
                      activeClass="active"
                      pageRangeDisplayed="1"
                      onChange={(e) => this.handlePageChangePopup(e,ndc_code)}
                    />
                  </div>
                </div>)
              
            }


            this.setState({ popup_data: html, open_popup: true,fa_loader:false });

          }
        })
        .catch((err) => {
          let html = [];

          html.push(<div className="listing-table popup-excipients-reddy">
            <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}>
              <table className="" style={{ width: "100%" }}>
                <thead>
                  <tr>
                    <th>NDC Code</th>
                    <th>UNII</th>
                    <th>Product Name</th>
                    <th>Excipient Name</th>
                    <th>Daily Dosage Limit</th>
                    <th>Alternate Excipients</th>
                    <th>Functional Category</th>
                    <th>Incompatibilities</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colSpan="11" align="center">
                        No Data To Display
                    </td>
                  </tr>
                </tbody>
              </table>
              </Scrollbars>
          </div>);

          this.setState({ popup_data: html, open_popup: true,fa_loader:false });
        });
    }
  };

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.paginationMoreInfo(
      pageNumber > 0 ? pageNumber : 1
    );
  };

  paginationMoreInfo = (page = 1) => {
    this.setState({fa_loader:true});
    let obj = {};
    if (this.state.selected_requested_for == 7) {
      obj = { 
        id                  : `${this.state.selected_requested_for}`, 
        txt                 : `${this.state.selected_product}`, 
        ndc_code            : this.state.select_alternate_ndc,
        functional_category : `${this.state.functional_category}`,
        unni                : `${this.state.unni}`,
        similarity_index    : `${this.state.similarity_index}`
      };

    }else if (this.state.selected_requested_for == 6) {
      obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}`, ndc_code: this.state.select_ndc, organization: this.state.select_organization };
    } else {
      obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}` };
    }

    axios
      .post(`/fa/search?&page=${page}`, obj)
      .then((res) => {
        let show_request_button     = false;
        let ndc                     = [];
        let organization            = [];
        let alternate_expients_list = [];

        if (this.state.selected_requested_for == 1 || this.state.selected_requested_for == 3 || this.state.selected_requested_for == 5) {
          show_request_button = true;
        }

        if (this.state.selected_requested_for == 6) {
          ndc = res.data.ndc_code;
          organization = res.data.organization;
        }else if(this.state.selected_requested_for == 7){
          ndc = res.data.ndc_code;
          alternate_expients_list = res.data.alternate_expients_list;
        }

        this.setState({ more_info: res.data.data, more_info_count: res.data.cnt, show_request_button: show_request_button, ndc: ndc, organization: organization,alternate_expients_list:alternate_expients_list,fa_loader:false});

      })
      .catch((err) => {

      });
  };

  handlePopupClose = () => {
    this.setState({ popup_data: '', open_popup: false, activePagePopup:1 });
  }

  handlePopupCloseErr = () => {
    this.setState({ popup_data: '', open_err_popup: false });
  }

  downloadSearch = (event) => {
    this.setState({fa_loader:true});
    event.preventDefault();
    let obj = {};
    if (this.state.selected_requested_for == 6) {
      obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}`, ndc_code: this.state.select_ndc, organization: this.state.select_organization };
    } else {
      obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}` };
    }

    if (this.state.selected_requested_for == 1) {
      var file_name = "Orange Book details";
    } else if (this.state.selected_requested_for == 2) {
      var file_name = "TGA details";
    } else if (this.state.selected_requested_for == 3) {
      var file_name = "EMA details";
    } else if (this.state.selected_requested_for == 4) {
      var file_name = "API DrugBank details";
    } else if (this.state.selected_requested_for == 5) {
      var file_name = "Patents Associated details";
    } else if (this.state.selected_requested_for == 6) {
      var file_name = "Excipients details";
    }
    axios
      .post(`/fa/search_download`, obj, { responseType: "blob" })
      .then((res) => {
        this.setState({fa_loader:false});
        let url = window.URL.createObjectURL(res.data);
        let a = document.createElement("a");
        a.href = url;
        a.download = `${file_name}.xlsx`;
        a.click();
      })
      .catch((err) => {
        //showErrorMessage(err, this.props);
      });
  }

  getQuestions = () => {

    let html = this.state.requested_for_list.map((element,index)=>{
      return (
        <li><label className={(this.state.selected_requested_for == element.id)?'active':''} ><input type="radio" onClick={()=>this.showAnswer(element.id,element.answer)} />{element.question}</label></li>
      );
    })

    return html;
  } 

  showAnswer = (id,answer) => {

    if(id > 0){
      this.setState({ selected_requested_for: id,selected_answer:answer, product_value: "",err_enter:"", suggestion: [], more_info: [], more_info_count: 0, activePage: 1, activePagePopup: 1,show_request_button: false, ndc: [], organization: [],functional_category:'',unni:'',similarity_index:'',data_searched:false,no_result_found:'' });
    }

  }

  enterPressed = (event) => {
    var code = event.keyCode || event.which;
    if (code === 13) {
      if(this.state.product_value.length < 3){
        this.setState({err_enter:'Please enter atleast 3 letters'});
      }else{
        
        this.setState({fa_loader:true});
        axios
        .post("/fa/suggest", { id: `${this.state.selected_requested_for}`, txt: this.state.product_value })
        .then((res) => {
          this.setState({fa_loader:false});
          this.setState({ suggestions: res.data.data,data_searched:false,more_info:[], selected_product:'', more_info_count:0 });
          return this.state.suggestions;

        })
        .catch((err) => {

        });

      }
    }
  };

  hideError = () => {
    this.setState({ err_enter: "" });
  };

  getTabContent = () => {
    return this.state.requested_for_list.map((element,index)=>{
      return (
        <Tab eventKey={element.id} title={element.title}>
          
          <p>{element.answer}</p>
          {this.state.more_info.length > 0 && <span className="downloadBtn" onClick={(e) => { this.downloadSearch(e) }} style={{cursor:'pointer'}} >Download Excel</span>}
          {this.state.fa_loader === false && this.state.more_info && this.getMoreInfo(element.id)}
        </Tab>
      );
    })
  }

  changeTabs = (id) => {
    if(id > 0){
      this.setState({ selected_requested_for: id,err_enter:"", more_info: [], more_info_count: 0, activePage: 1, activePagePopup: 1,show_request_button: false, ndc: [], organization: [],functional_category:'',unni:'',similarity_index:'',no_result_found:'',fa_loader:true,dynamic_html:''},()=>{
        axios
        .post("/fa/search", { id: `${id}`, txt: this.state.selected_product })
        .then((res) => {
          let show_request_button = false;
          let ndc = [];
          let organization = [];
          let alternate_expients_list = [];
          if (this.state.selected_requested_for == 1 || this.state.selected_requested_for == 3 || this.state.selected_requested_for == 5) {
            show_request_button = true;
          }

          if (this.state.selected_requested_for == 6) {
            ndc = res.data.ndc_code;
            organization = res.data.organization;
          }else if(this.state.selected_requested_for == 7){
            ndc = res.data.ndc_code;
            alternate_expients_list = res.data.alternate_expients_list;
          }

          this.setState({ more_info: res.data.data, more_info_count: res.data.cnt, show_request_button: show_request_button, ndc: ndc, organization: organization,alternate_expients_list:alternate_expients_list,select_display_organization:[],select_display_ndc:[],select_alternate_display_ndc:[],fa_loader:false});

        })
        .catch((err) => {

        });
      });
    }

  }

  render() {

    if(this.props.faAcess === false){
      window.location.href='dashboard'
      return null;
    }

    const inputProps = {
      placeholder: `Enter Product Name`,
      value: this.state.product_value,
      type: "search",
      onChange: this.onReferenceChange,
      onKeyPress: this.enterPressed.bind(this),
      id:'focus-ausuggest-input',
      className:
        this.state.product_value != '' ? "form-control customInput" : "form-control customInput"
    };

    if (this.state.show_enquiry_loader) {
      return <Loader />;
    } else {
      return (
        <>
          {this.state.fa_loader && <Loader />}
          <div className="container-fluid clearfix formSec">

            <div className="dashboard-content-sec productEnquirySection">
              <div className="service-request-form-sec newFormSec productEnquirySection">
                <div className="form-page-title-block fa-button-class">
                  <h2>
                    Xceed Formulation Guide
                  </h2>
                  <Link
                    to={{ pathname: `/my_product_enquiries` }}
                    className=""
                  >
                    My Enquiries
                  </Link>
                </div>

              
              <div className="productEnquiryWrapperShowing">
              
                <div className="row">
                  {this.state.err_enter !== '' && <div className="messageserror" style={{display:"block"}}>
                    <Link to="#" className="close">
                      <img
                        src={closeIcon}
                        width="17.69px"
                        height="22px"
                        onClick={(e) => this.hideError()}
                      />
                    </Link>
                    <div>
                      <ul className="">
                        <li className="">{this.state.err_enter}</li>
                      </ul>
                    </div>
                  </div>}

                  <div className="col-md-12">
                    <Autosuggest
                      autoComplete="off"
                      suggestions={this.state.suggestions}
                      onSuggestionsFetchRequested={
                        this.onSuggestionsFetchRequested
                      }
                      onSuggestionsClearRequested={
                        this.onSuggestionsClearRequested
                      }
                      getSuggestionValue={(suggestion) =>
                        this.getSuggestionValue(suggestion)
                      }
                      renderSuggestion={this.renderSuggestion}
                      defaultShouldRenderSuggestions={true}
                      inputProps={inputProps}
                    />
                    {this.state.product_value !== "" && (
                      <div className="close-icon-container">
                        <a
                          className="close-icon"
                          onClick={this.clearAutoSuggest}
                        >
                          <img
                            src={closeIconSelect}
                            width="10"
                            height="10"
                          />
                        </a>
                      </div>
                    )}

                  </div>
                </div>
                {this.state.no_result_found != '' && <><div className="clearfix"></div><br/>{this.state.no_result_found}</>}
              </div>

              {this.state.data_searched && <div className="productEnquiryWrapperShowing">
                <Tabs defaultActiveKey={this.state.selected_requested_for} id="uncontrolled-tab-example" onSelect={(k) =>  this.changeTabs(k)} >
                  {this.getTabContent()}
                </Tabs>
              </div>}


              <Formik
                initialValues={initialValues}
                validationSchema={false}
                onSubmit={this.handleSubmit}
              >
                {({
                  errors,
                  values,
                  touched,
                  setErrors,
                  setFieldValue,
                  setFieldTouched,
                }) => {

                  return (
                    <Form>
                      <div className="row">
                        <div className="col-md-12">
                          
                        </div>

                      </div>

                      <div className="row">
                        <div className="col text-right">
                          <br />
                          {this.state.show_request_button && <button style={{ width: '230px' }} className="btn btn-default" type="submit" >
                            {" "}
                            Request Further Info
                          </button>}
                          {this.state.show_request_button && <button
                            className="btn btn-default btn-cancel"
                            onClick={() => this.props.history.push("/my_product_enquiries")}
                          >
                            {this.props.dynamic_lang.form_component.cancel}
                          </button>}
                        </div>
                      </div>
                    </Form>
                  );
                }}
              </Formik>


                {this.state.show_confirm_modal === true && (
                  <Modal
                    show={this.state.show_confirm_modal}
                    backdrop="static"
                    className="grantmodal"
                  >
                    <Formik
                      initialValues={false}
                      validationSchema={false}
                      onSubmit={this.handleSubmitFinal}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        handleChange,
                        setFieldValue,
                        setFieldTouched,
                        setErrors,
                      }) => {
                        return (
                          <Form>
                            <Modal.Header>
                              <Modal.Title>
                                Add Additional Details
                            </Modal.Title>
                              <button className="close" onClick={this.handleCloseModal} />
                            </Modal.Header>
                            <Modal.Body>
                              <div>
                                <Row>

                                  <Editor
                                    name="comment"
                                    content={values.comment}
                                    init={{
                                      menubar: false,
                                      branding: false,
                                      placeholder: `Enter Addition Details (Optional)`,
                                      plugins:
                                        "link table hr visualblocks code placeholder lists",
                                      toolbar:
                                        "bold italic strikethrough superscript subscript | alignleft aligncenter alignright alignjustify | numlist bullist | removeformat underline | link unlink | blockquote table  hr | visualblocks code ",
                                      content_css: ["/css/editor.css"],
                                    }}
                                    onEditorChange={(value) => {
                                      setFieldValue("comment", value);
                                      this.setState({
                                        comment: value
                                      });
                                    }}
                                  />

                                </Row>
                                {this.state.escalation_error != '' && <p style={{ color: 'red' }} >{this.state.escalation_error}</p>}
                              </div>

                              <ButtonToolbar>

                                <Button
                                  className={`btn btn-default`}
                                  type="submit"
                                //disabled={isValid ? false : true}
                                >
                                  Submit
                              </Button>

                                <Button
                                  className={`btn btn-default btn-cancel`}
                                  type="button"
                                  onClick={this.handleCloseModal}
                                >
                                  Cancel
                              </Button>

                              </ButtonToolbar>
                            </Modal.Body>
                            {/* <Modal.Footer>
                            
                          </Modal.Footer> */}
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>
                )}

                {this.state.open_popup && <Modal
                  show={this.state.open_popup}
                  backdrop="static"
                  className="grantmodal excipient_popup_new"

                >
                  <Modal.Header>
                    <Modal.Title>
                      Excipient Details
                    </Modal.Title>
                    <button className="close" onClick={this.handlePopupClose} />
                  </Modal.Header>
                  <Modal.Body style={{ width: '100%' }}>
                    {this.state.popup_data}
                  </Modal.Body>
                </Modal>}

              </div>
            </div>
          </div>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    customerList: state.user.customerList,
    selCompany: state.user.selCompany,
    dynamic_lang: state.auth.dynamic_lang,
    faAcess:state.auth.faAcess
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductEnquiryAmit);
