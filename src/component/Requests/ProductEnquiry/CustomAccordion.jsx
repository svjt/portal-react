import React, {useContext} from 'react';
import AccordionContext from 'react-bootstrap/AccordionContext';
import {useAccordionToggle} from 'react-bootstrap/AccordionToggle';
import classNames from 'classnames';

function MyCustomToggle({eventKey, callback}) {
  const currentEventKey = useContext(AccordionContext); // <-- Will update every time the eventKey changes.
  const toggleOnClick = useAccordionToggle(eventKey, () => {
    callback(eventKey);
  });
  const isCurrentEventKey = currentEventKey === eventKey;

  return (
    <button
      type="button"
      className={classNames('myDefaultStyling', {'myCollapsedStyling': isCurrentEventKey})}
      onClick={toggleOnClick}
    >
      {isCurrentEventKey ? 'Collapse me' : 'Expand me'}
    </button>
  );
}

export default MyCustomToggle;