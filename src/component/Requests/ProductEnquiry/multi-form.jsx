import React, { Component, Alert } from "react";
import { Link } from "react-router-dom";
import Layout from "../../Layout/layout";
import Select from "react-select";
import closeIcon from "../../../assets/images/times-solid-red.svg";

import { Tooltip } from "reactstrap";
import infoIcon from "../../../assets/images/info-circle-solid.svg";
import axios from "../../../shared/axios";
import { Formik, Field, Form, yupToFormErrors } from "formik";
import * as Yup from "yup";
import {
  htmlDecode,
  supportedFileType,
  inArray,
  BasicUserData,
} from "../../../shared/helper";
import { Editor } from "@tinymce/tinymce-react";
import Dropzone from "react-dropzone";
import Loader from "../../Loader/loader";
import { connect } from "react-redux";
import ReactHtmlParser from "react-html-parser";

var path = require("path");

class Sample extends Component {
  constructor(props) {
    super(props);

    this.state = {
      enquiry_counter:0,
      product_enquiry_data:[],
      products:[],
      requested_for_list:[
        {
          value: 1,
          label: 'Want to know more Orange Book details of the particular product?'
        },
        {
          value: 2,
          label: 'What are the details of the Api Of The Products'
        },
        {
          value: 3,
          label: 'What are the carious patents associated with a product?'
        },
        {
          value: 4,
          label: 'For a particular product, what are the carious excipients used?'
        },
        {
          value: 5,
          label: 'Want are the alternate excipients that can be used?'
        }
      ],
      ndc:[
        {
          value: 1,
          label: 'Lorem Ipsum'
        },
        {
          value: 2,
          label: 'Lorem Ipsum'
        },
        {
          value: 3,
          label: 'Lorem Ipsum'
        },
        {
          value: 4,
          label: 'Lorem Ipsum'
        }
      ],
      excipient_name:[
        {
          value: 1,
          label: 'Lorem Ipsum'
        },
        {
          value: 2,
          label: 'Lorem Ipsum'
        },
        {
          value: 3,
          label: 'Lorem Ipsum'
        },
        {
          value: 4,
          label: 'Lorem Ipsum'
        }
      ],
      function_category:[
        {
          value: 1,
          label: 'Lorem Ipsum'
        },
        {
          value: 2,
          label: 'Lorem Ipsum'
        },
        {
          value: 3,
          label: 'Lorem Ipsum'
        },
        {
          value: 4,
          label: 'Lorem Ipsum'
        }
      ],
      show_enquiry_loader:true
    }

  }

  componentDidMount() {
    window.scrollTo(0, 0);
    this.getProductList();
    this.getNextHtml(false);
    
    document.title = `Product Enquiry | Dr.Reddys API`;

  }

  getProductList() {
    axios
      .get("/products")
      .then((res) => {
        var product_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          if (
            this.props.history.location.state !== undefined &&
            this.props.history.location.state.pid == element["product_id"]
          ) {
            //DO NOT PUSH
          } else {
            product_list.push({
              value: element["product_id"],
              //label: htmlDecode(element['medicalCondition'])
              label: htmlDecode(element["product_name"]),
            });
          }
        }
        this.setState({ products: product_list });
      })
      .catch((err) => {});
  }

  getNextHtml = (validate = true) => {
    this.setState({show_enquiry_loader:true});
    if(validate === true){
      //DO VALIDATION BEFORE BUILDING HTML
    }

    let prev_state = this.state.product_enquiry_data;

    prev_state.push({
      product_id:'',
      requested_for:[],
      show_requirements:false,
      requirements:'',
      show_ndc:false,
      ndc:[],
      excipient_name:'',
      function_category:''
    });

    this.setState({product_enquiry_data:prev_state,show_enquiry_loader:false});

  }

  handleSubmit = (values, actions) => {
    this.setState({ show_enquiry_loader: true });
    let err = 1;

    if (err > 0) {
      
    } else {
      var formData = new FormData();

      formData.append(
        "enqiry_data",
        this.state.product_enquiry_data.length > 0
          ? JSON.stringify(this.state.product_enquiry_data)
          : JSON.stringify([])
      );

      axios
        .post("tasks/samples-working-standards", formData)
        .then((res) => {
          if (res.status === 200) {
            this.setState({ show_enquiry_loader: false });
            this.props.history.push({
              pathname: "/my_requests",

              state: {
                message: this.props.dynamic_lang.form_component
                  .thank_for_request,
              },
            });
          } else {
            alert("Failed to add Task");
          }
        })
        .catch((err) => {
          this.setState({ show_enquiry_loader: false });

          actions.setSubmitting(false);
          //  alert(err.data.status)

          if (err.data.status === 2) {
            actions.setErrors(err.data.errors);
          } else {
            this.setState({ errMsg: err.data.message });
          }
        });
    }
  };

  setEnquiryDataSelect = (event,req_index,req_value) => {
    let enquiry_counter = 0;
    let prev_product_enquiry_data = this.state.product_enquiry_data;
    for (let index = 0; index < prev_product_enquiry_data.length; index++) {
      const element = prev_product_enquiry_data[index];

      if(req_index === index && req_value === 'product_id'){
        if(event == null){
          prev_product_enquiry_data[index].product_id = '';
        }else{
          prev_product_enquiry_data[index].product_id = event;
        }
      }

      if(req_index === index && req_value === 'requested_for'){
        if(event.length == 0){
          prev_product_enquiry_data[index].requested_for = [];
          prev_product_enquiry_data[index].show_ndc = false;
          prev_product_enquiry_data[index].show_requirements = false;
        }else{
          prev_product_enquiry_data[index].requested_for = event;
          prev_product_enquiry_data[index].show_requirements = true;
        
          for (let index_req = 0; index_req < event.length; index_req++) {
            const element = event[index_req];

            if(element.value == 4 || element.value == 5){
              prev_product_enquiry_data[index].show_ndc = true;
            }else{
              prev_product_enquiry_data[index].show_ndc = false;
            }
            
          }
        }
      }

      if(req_index === index && req_value === 'ndc'){
        if(event.length == 0){
          prev_product_enquiry_data[index].ndc = [];
        }else{
          prev_product_enquiry_data[index].ndc = event;
        }
      }

      if(req_index === index && req_value === 'excipient_name'){
        if(event == null){
          prev_product_enquiry_data[index].excipient_name = '';
        }else{
          prev_product_enquiry_data[index].excipient_name = event;
        }

      }

      if(req_index === index && req_value === 'function_category'){
        if(event == null){
          prev_product_enquiry_data[index].function_category = '';
        }else{
          prev_product_enquiry_data[index].function_category = event;
        }

      }

      if(prev_product_enquiry_data[index].show_requirements){
        enquiry_counter++;
      }

    }
    this.setState({
      product_enquiry_data:prev_product_enquiry_data,enquiry_counter:enquiry_counter
    });
  }

  setEnquiryDataEditor = (event,req_index,req_value) => {

    let prev_product_enquiry_data = this.state.product_enquiry_data;
    for (let index = 0; index < prev_product_enquiry_data.length; index++) {
      const element = prev_product_enquiry_data[index];

      if(req_index === index && req_value === 'requirements'){
        if(event == null){
          prev_product_enquiry_data[index].requirements = '';
        }else{
          prev_product_enquiry_data[index].requirements = event;
        }

      }

    }
    this.setState({
      product_enquiry_data:prev_product_enquiry_data
    });
  }

  getEnquiryHtml = () => {
    let html = [];
    let product_enquiry_data = this.state.product_enquiry_data;
    for (let index = 0; index < product_enquiry_data.length; index++) {
      const element = product_enquiry_data[index];
            
      html.push(
        <>
          {index > 0 && <br/>}
          <div style={{border:'2px solid #D1D1D1',borderRadius:'20px'}} key={index} >
          <br/>
          <div className="row">
            <div className="col-md-12">
              <label>Product</label>
              <Select
                value={element.product_id}
                options={this.state.products}
                isSearchable={true}
                isClearable={true}
                placeholder={
                  `Select the product`
                }
                className="product_id"
                name="product_id"
                onChange={(e) => {
                  this.setEnquiryDataSelect(e,index,'product_id');
                }}
              />
            </div>
          </div>

          <div className="row">
            <div className="col-md-12">
              <label>What do you want to request for?</label>
              <Select
                isMulti
                value={element.requested_for}
                options={this.state.requested_for_list}
                isSearchable={true}
                isClearable={true}
                placeholder={
                  `Select the query`
                }
                className="requested_for"
                name="requested_for"
                onChange={(e) => {
                  this.setEnquiryDataSelect(e,index,'requested_for');
                }}
              />
            </div>
          </div>



          {element.show_ndc && <><div className="row">
            <div className="col-md-4">
              <label>NDC</label>
              <Select
                isMulti
                value={element.ndc}
                options={this.state.ndc}
                isSearchable={true}
                isClearable={true}
                placeholder={
                  `Select the NDC`
                }
                className="ndc"
                name="ndc"
                onChange={(e) => {
                  this.setEnquiryDataSelect(e,index,'ndc');
                }}
              />
            </div>
          
            <div className="col-md-4">
              <label>Excipient Name</label>
              <Select
                value={element.excipient_name}
                options={this.state.excipient_name}
                isSearchable={true}
                isClearable={true}
                placeholder={
                  `Select the Excipient`
                }
                className="excipient_name"
                name="excipient_name"
                onChange={(e) => {
                  this.setEnquiryDataSelect(e,index,'excipient_name');
                }}
              />
            </div>
          
            <div className="col-md-4">
              <label>Function Category</label>
              <Select
                value={element.function_category}
                options={this.state.function_category}
                isSearchable={true}
                isClearable={true}
                placeholder={
                  `Select the Excipient`
                }
                className="function_category"
                name="function_category"
                onChange={(e) => {
                  this.setEnquiryDataSelect(e,index,'function_category');
                }}
              />
            </div>
          </div></>}

          {element.show_requirements && <div className="row">
            <div className="col-md-12">
              <Editor
                name="requirements"
                content={element.requirements}
                init={{
                  menubar: false,
                  branding: false,
                  placeholder: 'Add more details if required',
                  plugins:
                    "link table hr visualblocks code placeholder paste lists",
                  paste_data_images: true,
                  paste_use_dialog: true,
                  paste_auto_cleanup_on_paste: false,
                  paste_convert_headers_to_strong: true,
                  paste_strip_class_attributes: "none",
                  paste_remove_spans: false,
                  paste_remove_styles: false,
                  paste_retain_style_properties: "all",
                  toolbar:
                    "bold italic strikethrough superscript subscript | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | formatselect | visualblocks code | paste ",
                  content_css: ["/css/editor.css"],
                }}
                onEditorChange={(value) => {
                  this.setEnquiryDataEditor(value,index,'requirements');
                }
                }
              />
            </div>
          </div>}
          <br/>
          </div>
        </>
      );

    }

    return html;
  }

  close = () => {
    
  }

  render() {

    if(this.state.show_enquiry_loader){
      return <Loader />;
    }else{
      return (
        <div className="container-fluid clearfix formSec">
          <div className="dashboard-content-sec">
            <div className="service-request-form-sec newFormSec">
              <div className="form-page-title-block">
                <h2>
                  Product Enquiry
                </h2>
              </div>
              <Formik
                initialValues={false}
                validationSchema={false}
                onSubmit={this.handleSubmit}
              >
                {({
                  errors,
                  values,
                  touched,
                  setErrors,
                  setFieldValue,
                  setFieldTouched,
                }) => {
                  //console.log("errors", values);
                  //console.log("touched", touched);
                  return (
                    <Form>
                      
                      {this.state.product_enquiry_data.length > 0 && this.getEnquiryHtml()}
                      <div className="clearfix"></div>
  
                      
                      <div className="row">
                        <div className="col text-right">
                          {this.state.enquiry_counter} Product{this.state.enquiry_counter > 1 && `s`} Enquiry Created
                          <br/>
                          <button className="btn btn-default" type="submit" >
                            {" "}
                            Request
                          </button>
                          <br/>
                          <button 
                            className="btn btn-default"
                            onClick={(e) => {
                              e.preventDefault();
                              this.getNextHtml(false)
                            }}
                          >
                            {" "}
                            Add Product
                          </button>
                          <br/>
                          <button
                            className="btn btn-default btn-cancel"
                            onClick={() => this.props.history.push("/dashboard")}
                          >
                            {this.props.dynamic_lang.form_component.cancel}
                          </button>
                        </div>
                      </div>
                    </Form>
                  );
                }}
              </Formik>
            </div>
          </div>
        </div>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    customerList: state.user.customerList,
    selCompany: state.user.selCompany,
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Sample);
