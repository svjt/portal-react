import React, { Component, Alert, useContext } from "react";
// import { Link } from "react-router-dom";
// import Layout from "../../Layout/layout";
import Select from "react-select";
// import closeIcon from "../../../assets/images/times-solid-red.svg";

// import { Tooltip } from "reactstrap";
// import infoIcon from "../../../assets/images/info-circle-solid.svg";
import closeIconSelect from "../../../assets/images/close.svg";
import axios from "../../../shared/axios";
import { Formik, Form } from "formik";
// import * as Yup from "yup";
// import {
//   htmlDecode,
//   supportedFileType,
//   inArray,
//   BasicUserData,
// } from "../../../shared/helper";
import { Editor } from "@tinymce/tinymce-react";
// import Dropzone from "react-dropzone";
import Loader from "../../Loader/loader";
import { connect, useSelector } from "react-redux";
import ReactHtmlParser from "react-html-parser";
import Pagination from "react-js-pagination";


import {
  Button,
  Row, Modal, ButtonToolbar, Card, Accordion, useAccordionToggle
} from "react-bootstrap";

// import AccordionContext from 'react-bootstrap/AccordionContext';
//import { useAccordionToggle } from 'react-bootstrap/AccordionToggle'; 

import Autosuggest from "react-autosuggest";

var path = require("path");

let initialValues = {
  requested_for: '',
  product_id: ''
}

function ContextAwareToggle({ children, eventKey, objRef }) {
  const toggleOnClick = useAccordionToggle(eventKey, () => {
    let selectedAccordion = objRef.state.selected_requested_for;
    let incremented_eventKey = eventKey + 1;
    console.log(incremented_eventKey, selectedAccordion);
    if (selectedAccordion == incremented_eventKey) {
      objRef.setState({ selected_requested_for: 0, product_value: "", suggestion: [], more_info: [], more_info_count: 0, activePage: 1, show_request_button: false, ndc: [], organization: [] });
    } else {
      objRef.setState({ selected_requested_for: incremented_eventKey, product_value: "", suggestion: [], more_info: [], more_info_count: 0, activePage: 1, show_request_button: false, ndc: [], organization: [] });
    }
  });

  return (
    <div
      onClick={toggleOnClick}
    >
      {children}
    </div>
  );
}

class Sample extends Component {
  constructor(props) {
    super(props);

    this.state = {
      show_confirm_modal: false,
      enquiry_counter: 0,
      product_enquiry_data: [],
      products: [],
      suggestions: [],
      product_value: '',
      selected_requested_for: 0,
      more_info: [],
      more_info_count: 0,
      selected_product: '',
      show_request_button: false,
      popup_data: '',
      open_popup: false,
      fa_loader: false,
      requested_for_list: [
        {
          id: 1,
          question: 'Want to know more Orange Book details of the particular product?',
          answer: `Retrieve details from the USFDA Orange Book Details for a product. This will provide information on the player market, NDC (National Drug Code), and the relevant application numbers. If you want more details on the dosage forms, pill details
          (like color, shape, size), strength, whether RLD is available, please click on Request for more information.`
        },
        {
          id: 2,
          question: 'Want to know the TGA details of the product (Australia)?',
          answer: `Retrieve details from the Therapeutics Goods Administration (Australia) for a product.`
        },
        {
          id: 3,
          question: 'Want to know the EMA details of the product?',
          answer: `Retrieve details from the European Medicenes Agency for a product. If you require information on last modified date or indications it is approved for, please click on Request for more information.`
        },
        {
          id: 4,
          question: 'What are the details of the API of the product?',
          answer: `Retrieve details from the API DrugBank for a particular API. This will provide information on the product structure & identification, experimental properties, calculated properties, and its pharmcology.`
        },
        {
          id: 5,
          question: 'What are the various patents associated with a product?',
          answer: `Retrieve list of all patents associated with a product along with a patent expiry date, with links to the patent offices (USPTO) for dossiers. If you require more information on claims on the patents (independent or dependent claims), please
          click on Request for more information`
        },
        {
          id: 6,
          question: 'For a particular product, what are the various excipients used?',
          answer: `Retrieve information on a product (please provide either the specific NDC code or the organization). You will receive all the Orange book details of the product, various excipients used with the following details for each excipient: Name, functional
          category, daily dosage limit, regulatory status, incompatibilities, links for each to Inxight: Drugs`
        }
        // ,
        // {
        //   id: 7,
        //   question: 'What are alternate excipients that can be used?',
        //   answer:`Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
        // }
      ],
      ndc: [],
      organization: [],
      select_organization: [],
      select_ndc: [],
      excipient_name: [],
      function_category: [],
      show_enquiry_loader: false,
      show_products: false,
      activePage: 1,
      totalCount: 0,
      itemPerPage: 10
    }

  }

  componentDidMount() {
    window.scrollTo(0, 0);
    document.title = `Product Enquiry | Dr.Reddys API`;

  }

  handleSubmit = (values, actions) => {
    this.setState({ show_confirm_modal: true });
  };

  handleCloseModal = () => {
    this.setState({ show_confirm_modal: false });
  };

  handleSubmitFinal = (values, actions) => {
    this.setState({fa_loader:true});
    let post_obj = { 
      enquiry_type : `${this.state.selected_requested_for}`, 
      product_name : this.state.selected_product,
      comment      : values.comment

    }
    axios
      .post("/fa/request_more_info",post_obj)
      .then((res) => {

        this.props.history.push({
            pathname: "/my_product_enquiries",
            state: {
              message: 'Enquiry submitted successfully'
            }
        });

        this.setState({fa_loader:false});
      })
      .catch((err) => {

    });
  }

  setRequestedFor = (e, setFieldValue) => {
    if (e == null) {
      setFieldValue('requested_for', '');
      this.setState({ show_products: false });
    } else {
      setFieldValue('requested_for', e);

      this.setState({ suggestions: [] }, () => {
        this.setState({ show_products: true });
      });
    }
  }

  onReferenceChange = (event, { newValue }) => {
    console.log(newValue);
    this.setState(
      {
        product_value: newValue
      }
    );
  };

  // ==== Autosuggest for Reference Query DRL ==== //
  onSuggestionsFetchRequested = ({ value }) => {
    const inputValue = value.toLowerCase();
    const inputLength = inputValue.length;

    if (inputLength === 0) {
      return [];
    } else {
      if (inputLength > 2) {
        this.setState({fa_loader:true});
        axios
          .post("/fa/suggest", { id: `${this.state.selected_requested_for}`, txt: inputValue })
          .then((res) => {
            this.setState({fa_loader:false});
            this.setState({ suggestions: res.data.data });
            return this.state.suggestions;

          })
          .catch((err) => {

          });
      }
    }
  };

  // ==== Autosuggest for Reference Query DRL ==== //
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  getSuggestionValue = (suggestion) => {
    this.setState({fa_loader:true});
    axios
      .post("/fa/search", { id: `${this.state.selected_requested_for}`, txt: suggestion.label })
      .then((res) => {
        let show_request_button = false;
        let ndc = [];
        let organization = [];
        if (this.state.selected_requested_for == 1 || this.state.selected_requested_for == 3 || this.state.selected_requested_for == 5) {
          show_request_button = true;
        }

        if (this.state.selected_requested_for == 6) {
          ndc = res.data.ndc_code;
          organization = res.data.organization;
        }

        this.setState({ more_info: res.data.data, selected_product: suggestion.label, more_info_count: res.data.cnt, show_request_button: show_request_button, ndc: ndc, organization: organization,fa_loader:false });

      })
      .catch((err) => {

      });

    return suggestion.label;

  };

  renderSuggestion = (suggestion) => {
    return (
      <div style={{ cursor: 'pointer' }} >
        {suggestion.label}
      </div>
    );
  };

  clearAutoSuggest = () => {
    this.setState({ product_value: "", suggestion: [], more_info: [], more_info_count: 0, activePage: 1, show_request_button: false, ndc: [], organization: [] });
  };

  setNdcCode = (ncd_event) => {
    if (ncd_event == null) {
      this.setState({ select_ndc: [] });
    } else {
      let arr = [];
      for (let index = 0; index < ncd_event.length; index++) {
        const element = ncd_event[index];
        arr.push(element.label);
        console.log('element.label', element.label);
      }
      this.setState({ select_ndc: arr }, () => this.paginationMoreInfo());
    }
  }

  setOrganization = (org_event) => {
    if (org_event == null) {
      this.setState({ select_organization: [] });
    } else {
      let arr = [];
      for (let index = 0; index < org_event.length; index++) {
        const element = org_event[index];
        arr.push(element.label);
      }
      this.setState({ select_organization: arr }, () => this.paginationMoreInfo());
    }
  }

  getMoreInfo = () => {
    let html = [];
    if (this.state.selected_requested_for === 1) {
      //this.setState({show_request_button:true});
      html.push(<>
        <div className="listing-table table-responsive">
          <table className="" style={{ width: "100%" }}>
            <thead>
              <tr>
                <th>
                  NDC Code
                </th>
                <th>
                  Application Number
                </th>
                <th>
                  Organization
                </th>
              </tr>
            </thead>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <tr
                  key={key}
                >
                  <td>{request.ndc_code}</td>
                  <td>{request.applicationnumber}</td>
                  <td>{request.organization}</td>
                </tr>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </>);
    } else if (this.state.selected_requested_for === 2) {
      html.push(<>
        <div className="listing-table table-responsive">
          <table className="" style={{ width: "100%" }}>
            <thead>
              <tr>
                <th>Product Name</th>
                <th>NDC Code</th>
                <th>NDC Name</th>
                <th>Submission Number</th>
                <th>Sponsor</th>
                <th>Submission Type</th>
                <th>Authorization Status</th>
                <th>Auspar Date</th>
                <th>Publication Date</th>
              </tr>
            </thead>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <tr
                  key={key}
                >
                  <td>{request.product_name}</td>
                  <td>{request.ndc_code}</td>
                  <td>{request.ndc_name}</td>
                  <td>{request.submission_number}</td>
                  <td>{request.sponsor}</td>
                  <td>{request.submission_type}</td>
                  <td>{request.authorization_status}</td>
                  <td>{request.auspar_date}</td>
                  <td>{request.publication_date}</td>
                </tr>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                    </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </>);
    } else if (this.state.selected_requested_for === 3) {
      html.push(<>
        <div className="listing-table table-responsive">
          <table className="" style={{ width: "100%" }}>
            <thead>
              <tr>
                <th>Product Name</th>
                <th>NDC Code</th>
                <th>Product Number</th>
                <th>Authorisation Status</th>
                <th>Therapeutic Area</th>
                <th>Generic</th>
                <th>Biosimilar</th>
                <th>Additional Monitoring</th>
                <th>Orphan Drug</th>
                <th>Marketing Date</th>
                <th>Pharma Class</th>
                <th>Company Name</th>
              </tr>
            </thead>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <tr
                  key={key}
                >
                  <td>{request.productname}</td>
                  <td>{request.ndc_code}</td>
                  <td>{request.product_number}</td>
                  <td>{request.authorisation_status}</td>
                  <td>{request.therapeutic_area}</td>
                  <td>{request.generic}</td>
                  <td>{request.biosimilar}</td>
                  <td>{request.additional_monitoring}</td>
                  <td>{request.orphan_drug}</td>
                  <td>{request.marketing_date}</td>
                  <td>{request.pharma_class}</td>
                  <td>{request.company_name}</td>

                </tr>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                    </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </>);
    } else if (this.state.selected_requested_for === 4) {
      html.push(<>
        <div className="listing-table table-responsive">
          <table className="" style={{ width: "100%" }}>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Dc Name</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.dcname}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >NDC Code</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.ndccode}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Drugbank Id</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.drugbank_id}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Product Name</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.productname}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Water Sol</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.water_sol_x}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Log</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.logp_x}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >PKA</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.pka}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Smiles</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.smiles}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Casnum</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.casnum}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Boiling Point</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.boiling_point}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Hydrophobicity</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.hydrophobicity}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Isoelectric_point</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.isoelectric_point}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Melting Point</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.melting_point}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Molecular Formula</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.molecular_formula_x}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Molecular Weight</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.molecular_weight_x}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Radioactivity</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.radioactivity}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Caco2 Permeability</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.caco2_permeability}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Logs</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.logs_x}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Description</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.description}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Indication</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.indication}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Average Mass</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.average_mass}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Monoisotopic Mass</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.monoisotopic_mass}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >State</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.state}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Pharmacodynamics</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.pharmacodynamics}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Clearance</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.clearance}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Mechanism Of Action</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.mechanism_of_action}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Toxicity</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.toxicity}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Metabolism</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.metabolism}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Absorption</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.absorption}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Half Life</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.half_life}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Protein Binding</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.protein_binding}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Route Of Elimination</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.route_of_elimination}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Volume Of Distribution</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.volume_of_distribution}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Bond Acceptor Count</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.h_bond_acceptor_count}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >H Bond Donor CH Bond Donor Countount</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.h_bond_donor_ch_bond_donor_countount}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >IUPAC Name</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.iupac_name}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Molecular Formula</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.molecular_formula_y}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Molecular Weight</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.molecular_weight_y}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >physiological Charge</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.physiological_charge}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Polar Surface Area</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.polar_surface_area_psa}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Polarizability</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.polarizability}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Refractivity</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.refractivity}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Rotatable Bond Count</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.rotatable_bond_count}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Water Solubility </td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.water_solubility_y}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Log P Y</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.logp_y}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Log S Y</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.logs_y}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >PKA Strongest Acidic</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.pka_strongest_acidic}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >PKA Strongest Basic</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.pka_strongest_basic}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >DC Direct Parent</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.dc_direct_parent}</td></tr>
                  <tr><td bgcolor="#8a4fee" style={{ textAlign: 'left', color: 'white', fontSize: '18px' }} >Pharm Classes</td><td bgcolor="#e6e2e2" style={{ textAlign: 'left', color: 'black', fontSize: '15px' }} >{request.pharm_classes}</td></tr>
                </>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                    </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </>);
    } else if (this.state.selected_requested_for === 5) {
      html.push(<>
        <div className="listing-table table-responsive">
          <table className="" style={{ width: "100%" }}>
            <thead>
              <tr>
                <th>Patent Number</th>
                <th>Product Name</th>
                <th>Exclusivity Information</th>
                <th>Submission Date</th>
                <th>Patent Expiry</th>
                <th>DP</th>
                <th>DS</th>
                <th>Current Assignee</th>
                <th>Patent Title</th>
                <th>Key Words</th>
                <th>Global Dossier</th>
              </tr>
            </thead>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <tr
                  key={key}
                >
                  <td>{request.patent_number}</td>
                  <td>{request.productname}</td>
                  <td>{request.exclusivity_information}</td>
                  <td>{request.submission_date}</td>
                  <td>{request.patentexpiry}</td>
                  <td>{request.dp}</td>
                  <td>{request.ds}</td>
                  <td>{request.current_assignee}</td>
                  <td>{request.patent_title}</td>
                  <td>{request.key_words}</td>
                  <td><a href={request.global_dossier} target='__blank' >{request.global_dossier}</a></td>
                </tr>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                    </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </>);
    } else if (this.state.selected_requested_for === 6) {
      //this.setState({show_request_button:true});

      html.push(<div className="row">
        {this.state.ndc && this.state.ndc.length > 0 && <div className="col-md-6">
          <label>NDC Code</label>
          <Select
            isMulti
            options={this.state.ndc}
            isSearchable={true}
            isClearable={true}
            placeholder={
              `Select NDC Code`
            }
            name="ndc"
            onChange={(e) => this.setNdcCode(e)}
          />
        </div>}

        {this.state.organization && this.state.organization.length > 0 && <div className="col-md-6">
          <label>Organization</label>
          <Select
            isMulti
            options={this.state.organization}
            isSearchable={true}
            isClearable={true}
            placeholder={
              `Select Organization`
            }
            name="organization"
            onChange={(e) => this.setOrganization(e)}
          />
        </div>}

      </div>)

      html.push(<>
        <div className="listing-table table-responsive">
          <table className="" style={{ width: "100%" }}>
            <thead>
              <tr>
                <th>NDC Code</th>
                <th>Application Number</th>
                <th>Organization</th>
                <th>No Of Excepients</th>
                <th>Dosage Form</th>
                <th>Route</th>
                <th>Color</th>
                <th>Shape</th>
                <th>Sizes</th>
                <th>Strength</th>
                <th>RLD</th>
              </tr>
            </thead>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <tr
                  key={key}
                >
                  <td>{request.ndc_code}</td>
                  <td>{request.applicationnumber}</td>
                  <td>{request.organization}</td>
                  <td onClick={() => { this.openExcepientsPopup(request) }} ><span style={{ color: 'blue', cursor: 'pointer', textDecoration: 'underline' }} >{request.no_of_excepients}</span></td>
                  <td>{request.dosage_form}</td>
                  <td>{request.route}</td>
                  <td>{request.color}</td>
                  <td>{request.shape}</td>
                  <td>{request.sizes}</td>
                  <td>{request.strength}</td>
                  <td>{request.rld}</td>
                </tr>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </>);



    }

    if (this.state.more_info_count > this.state.itemPerPage) {
      html.push(
        <div className="pagination-area">
          <div className="paginationOuter text-right">
            <Pagination
              hideDisabled
              hideFirstLastPages
              prevPageText="‹‹"
              nextPageText="››"
              activePage={this.state.activePage}
              itemsCountPerPage={this.state.itemPerPage}
              totalItemsCount={this.state.more_info_count}
              itemClass="nav-item"
              linkClass="nav-link"
              activeClass="active"
              pageRangeDisplayed="1"
              onChange={this.handlePageChange}
            />
          </div>
        </div>
      );
    }

    return html;

  }

  openExcepientsPopup = (req) => {
    //console.log(req);
    if (req.ndc_code) {
      this.setState({fa_loader:true});
      axios
        .post(`/fa/get_excipients`, { ndc_code: req.ndc_code })
        .then((res) => {
          if (res.data.data.length > 0) {
            let html = '';

            let dynamic_html = '';

            if (res.data.data.length === 0) {
              dynamic_html += `<tr>
              <td colSpan="11" align="center">
                  No Data To Display
              </td>
            </tr>`;
            } else {
              for (let index = 0; index < res.data.data.length; index++) {
                const element = res.data.data[index];

                dynamic_html += `<tr key=${index}>
              <td>${element.ndc_code}</td>
              <td>${element.unii}</td>
              <td>${element.product_name}</td>
              <td>${element.excipientname}</td>
              <td>${element.dailydosagelimit}</td>
              <td>${element.alternate_excipients}</td>
              <td>${element.functional_category}</td>
              <td>${element.incompatibilities}</td>
            </tr>`;

                if (index == 10) {
                  break;
                }

              }
            }

            html += `<div className="listing-table table-responsive">
              <table className="" style={{ width: "100%" }}>
                <thead>
                  <tr>
                    <th>NDC Code</th>
                    <th>UNII</th>
                    <th>Product Name</th>
                    <th>Excipient Name</th>
                    <th>Daily Dosage Limit</th>
                    <th>Alternate Excipients</th>
                    <th>Functional Category</th>
                    <th>Incompatibilities</th>
                  </tr>
                </thead>
                <tbody>
                  ${dynamic_html}
                </tbody>
              </table>
          </div>`;

            this.setState({ popup_data: html, open_popup: true,fa_loader:false });

          }
        })
        .catch((err) => {

        });
    }
  }

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.paginationMoreInfo(
      pageNumber > 0 ? pageNumber : 1
    );
  };

  paginationMoreInfo = (page = 1) => {
    this.setState({fa_loader:true});
    let obj = {};
    if (this.state.selected_requested_for == 6) {
      obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}`, ndc_code: this.state.select_ndc, organization: this.state.select_organization };
    } else {
      obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}` };
    }

    axios
      .post(`/fa/search?&page=${page}`, obj)
      .then((res) => {
        console.log('res.data.data', res.data.data);
        this.setState({ more_info: res.data.data,fa_loader:false });
      })
      .catch((err) => {

      });
  };

  handlePopupClose = () => {
    this.setState({ popup_data: '', open_popup: false });
  }

  downloadSearch = (event) => {
    this.setState({fa_loader:true});
    event.preventDefault();
    let obj = {};
    if (this.state.selected_requested_for == 6) {
      obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}`, ndc_code: this.state.select_ndc, organization: this.state.select_organization };
    } else {
      obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}` };
    }

    if (this.state.selected_requested_for == 1) {
      var file_name = "Orange Book details";
    } else if (this.state.selected_requested_for == 2) {
      var file_name = "TGA details";
    } else if (this.state.selected_requested_for == 3) {
      var file_name = "EMA details";
    } else if (this.state.selected_requested_for == 4) {
      var file_name = "API DrugBank details";
    } else if (this.state.selected_requested_for == 5) {
      var file_name = "Patents Associated details";
    } else if (this.state.selected_requested_for == 6) {
      var file_name = "Excipients details";
    }
    axios
      .post(`/fa/search_download`, obj, { responseType: "blob" })
      .then((res) => {
        this.setState({fa_loader:false});
        let url = window.URL.createObjectURL(res.data);
        let a = document.createElement("a");
        a.href = url;
        a.download = `${file_name}.xlsx`;
        a.click();
      })
      .catch((err) => {
        //showErrorMessage(err, this.props);
      });
  }

  render() {
    const inputProps = {
      placeholder: `Enter Product Name`,
      value: this.state.product_value,
      type: "search",
      onChange: this.onReferenceChange,
      className:
        this.state.product_value != '' ? "form-control customInput" : "form-control customInput"
    };

    if (this.state.show_enquiry_loader) {
      return <Loader />;
    } else {
      return (
        <>
          {this.state.fa_loader && <Loader />}
          <div className="container-fluid clearfix formSec">

            <div className="dashboard-content-sec">
              <div className="service-request-form-sec newFormSec">
                <div className="form-page-title-block">
                  <h2>
                    Product Enquiry
                  </h2>
                </div>
                <Formik
                  initialValues={initialValues}
                  validationSchema={false}
                  onSubmit={this.handleSubmit}
                >
                  {({
                    errors,
                    values,
                    touched,
                    setErrors,
                    setFieldValue,
                    setFieldTouched,
                  }) => {

                    return (
                      <Form>
                        <div className="row">
                          <div className="col-md-12">
                            <Accordion>
                              {this.state.requested_for_list.map((value, index) => {
                                return (
                                  <Card key={index}>
                                    <Card.Header style={{ cursor: 'pointer' }} >
                                      <ContextAwareToggle eventKey={index} objRef={this} >{value.question}</ContextAwareToggle>
                                    </Card.Header>
                                    <Accordion.Collapse eventKey={index}>
                                      <Card.Body>
                                        {value.answer}
                                        {this.state.selected_requested_for > 0 && <div className="row">
                                          <div className="col-md-12">
                                            <label></label>

                                            <Autosuggest
                                              autoComplete="off"
                                              suggestions={this.state.suggestions}
                                              onSuggestionsFetchRequested={
                                                this.onSuggestionsFetchRequested
                                              }
                                              onSuggestionsClearRequested={
                                                this.onSuggestionsClearRequested
                                              }
                                              getSuggestionValue={(suggestion) =>
                                                this.getSuggestionValue(suggestion)
                                              }
                                              renderSuggestion={this.renderSuggestion}
                                              defaultShouldRenderSuggestions={true}
                                              inputProps={inputProps}
                                            />
                                            {this.state.product_value !== "" && (
                                              <div className="close-icon-container">
                                                <a
                                                  className="close-icon"
                                                  onClick={this.clearAutoSuggest}
                                                >
                                                  <img
                                                    src={closeIconSelect}
                                                    width="10"
                                                    height="10"
                                                  />
                                                </a>
                                              </div>
                                            )}
                                          </div>
                                        </div>}

                                        {this.state.more_info.length > 0 &&
                                          <><div className="clearfix"></div>
                                            <br />
                                            <span style={{ color: 'blue', cursor: 'pointer', textDecoration: 'underline' }} onClick={(e) => { this.downloadSearch(e) }} >Download Excel</span>
                                            {this.getMoreInfo()}
                                          </>}

                                      </Card.Body>
                                    </Accordion.Collapse>
                                  </Card>

                                );
                              })}
                            </Accordion>
                          </div>

                        </div>

                        <div className="row">
                          <div className="col text-right">
                            <br />
                            {this.state.show_request_button && <button style={{ width: '230px' }} className="btn btn-default" type="submit" >
                              {" "}
                              Request Further Info
                            </button>}
                            <button
                              className="btn btn-default btn-cancel"
                              onClick={() => this.props.history.push("/dashboard")}
                            >
                              {this.props.dynamic_lang.form_component.cancel}
                            </button>
                          </div>
                        </div>
                      </Form>
                    );
                  }}
                </Formik>


                {this.state.show_confirm_modal === true && (
                  <Modal
                    show={this.state.show_confirm_modal}
                    backdrop="static"
                    className="grantmodal"
                  >
                    <Formik
                      initialValues={false}
                      validationSchema={false}
                      onSubmit={this.handleSubmitFinal}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        handleChange,
                        setFieldValue,
                        setFieldTouched,
                        setErrors,
                      }) => {
                        return (
                          <Form>
                            <Modal.Header>
                              <Modal.Title>
                                Add Additional Details
                            </Modal.Title>
                              <button className="close" onClick={this.handleSendMailEscalationClose} />
                            </Modal.Header>
                            <Modal.Body>
                              <div>
                                <Row>

                                  <Editor
                                    name="comment"
                                    content={values.comment}
                                    init={{
                                      menubar: false,
                                      branding: false,
                                      placeholder: `Enter Addition Details (Optional)`,
                                      plugins:
                                        "link table hr visualblocks code placeholder lists",
                                      toolbar:
                                        "bold italic strikethrough superscript subscript | alignleft aligncenter alignright alignjustify | numlist bullist | removeformat underline | link unlink | blockquote table  hr | visualblocks code ",
                                      content_css: ["/css/editor.css"],
                                    }}
                                    onEditorChange={(value) => {
                                      setFieldValue("comment", value);
                                      this.setState({
                                        comment: value
                                      });
                                    }}
                                  />

                                </Row>
                                {this.state.escalation_error != '' && <p style={{ color: 'red' }} >{this.state.escalation_error}</p>}
                              </div>

                              <ButtonToolbar>

                                <Button
                                  className={`btn btn-default`}
                                  type="submit"
                                //disabled={isValid ? false : true}
                                >
                                  Submit
                              </Button>

                                <Button
                                  className={`btn btn-default btn-cancel`}
                                  type="button"
                                  onClick={this.handleCloseModal}
                                >
                                  Cancel
                              </Button>

                              </ButtonToolbar>
                            </Modal.Body>
                            {/* <Modal.Footer>
                            
                          </Modal.Footer> */}
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>
                )}

                {this.state.open_popup && <Modal
                  show={this.state.open_popup}
                  backdrop="static"
                  className="grantmodal excipient_popup"

                >
                  <Modal.Header>
                    <Modal.Title>
                      Excipient Details
                    </Modal.Title>
                    <button className="close" onClick={this.handlePopupClose} />
                  </Modal.Header>
                  <Modal.Body style={{ width: '100%' }}>
                    {ReactHtmlParser(this.state.popup_data)}
                  </Modal.Body>
                </Modal>}

              </div>
            </div>
          </div>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    customerList: state.user.customerList,
    selCompany: state.user.selCompany,
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(Sample);
