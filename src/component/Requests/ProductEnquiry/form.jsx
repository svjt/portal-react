import React, { Component, Alert, useContext } from "react";
import { Link } from "react-router-dom";
// import { Scrollbars } from 'react-custom-scrollbars';
// import Layout from "../../Layout/layout";
import Select from "react-select";
import closeIcon from "../../../assets/images/times-solid-red.svg";

import { Tooltip } from "reactstrap";
import infoIcon from "../../../assets/images/info-circle-solid.svg";
import formulationImg from "../../../assets/images/XCEED Formulation Handbook.jpg";
import closeIconSelect from "../../../assets/images/close.svg";
import axios from "../../../shared/axios";
import { Formik, Form } from "formik";
// import * as Yup from "yup";
import {
  htmlDecode,
} from "../../../shared/helper";
// import { Editor } from "@tinymce/tinymce-react";
// import Dropzone from "react-dropzone";
import Loader from "../../Loader/loader";
import { connect, useSelector } from "react-redux";
import { faHideLeftMenu} from "../../../store/actions/auth";
// import ReactHtmlParser from "react-html-parser";
import Pagination from "react-js-pagination";

import {
  TabContent,
  Button,
  Row,Col, Modal, ButtonToolbar, Tabs, Tab
} from "react-bootstrap";

// import AccordionContext from 'react-bootstrap/AccordionContext';
//import { useAccordionToggle } from 'react-bootstrap/AccordionToggle'; 
import Rating from "react-rating";
import Autosuggest from "react-autosuggest";

var path = require("path");



let initialValues = {
  requested_for: '',
  product_id: ''
}

class ProductEnquiryAmit extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tooltipOpen: false,
      show_confirm_modal: false,
      enquiry_counter: 0,
      product_enquiry_data: [],
      selOptions: [],
      products: [],
      suggestions: [],
      product_value: '',
      selected_requested_for: 1,
      more_info: [],
      more_info_count: 0,
      selected_product: '',
      show_request_button: false,
      popup_data: '',
      open_popup: false,
      data_searched:false,
      open_err_popup:false,
      fa_loader: false,
      dynamic_html:'',
      err_enter:'',
      selected_answer:'',
      select_display_ndc:[],
      select_alternate_display_ndc:'',
      select_display_organization:[],
      no_result_found:'',
      functional_category:'',
      unni:'',
      selected_drug_bank:'',
      drug_bank_index:0,
      similarity_index:'',
      requested_for_list: [
        {
          id: 1,
          question : `Want to know more Orange Book details of the particular product?`,
          title    : ` USFDA `,
          //answer   : `Retrieve details from the USFDA Orange Book Details for a product. This will provide information on the player market, NDC (National Drug Code), and the relevant application numbers. If you want more details on the dosage forms, pill details
          //(like color, shape, size), strength, whether RLD is available, please click on Request for more information.`
          answer: `The USFDA Orange Book details information on the market players, NDC (National Drug Code), and the relevant application numbers. For more information on the dosage forms, pill details (color, shape, size), strength, or whether a RLD is available, please click on Request further information`
        },
        {
          id: 3,
          question: 'Want to know the EMA details of the product?',
          title    : `EMA Product Details`,
          //answer: `Retrieve details from the European Medicenes Agency for a product. If you require information on last modified date or indications it is approved for, please click on Request for more information.`
          answer: `If you require information on the last modified date or approved indications, please click on Request further information.`
        },
        {
          id: 2,
          question: 'Want to know the TGA details of the product (Australia)?',
          title    : `TGA Product Details`,
          //answer: `Retrieve details from the Therapeutics Goods Administration (Australia) for a product.`
          answer: `Approvals by the Therapeutics Goods Administration (Australia).`          
        },
        {
          id: 4,
          question: 'What are the details of the API of the product?',
          title    : `API Details`,
          //answer: `Retrieve details from the API DrugBank for a particular API. This will provide information on the product structure & identification, experimental properties, calculated properties, and its pharmcology.`
          answer: `Get more information on the API structure and identification, experimental properties, calculated properties, and pharmacology. Source: API Drugbank`
        },
        {
          id: 5,
          question: 'What are the various patents associated with a product?',
          title    : `Patent Details`,
          //answer: `Retrieve list of all patents associated with a product along with a patent expiry date, with links to the patent offices (USPTO) for dossiers. If you require more information on claims on the patents (independent or dependent claims), please
          //click on Request for more information`
          answer: `Here you can find a list of all patents associated with a product along with a patent expiry date, with links to the patent offices (USPTO) for dossiers. If you require more information on claims on the patents (independent or dependent claims), please write to us by clicking on Request further information.`,
        },
        {
          id: 6,
          question: 'For a particular product, what are the various excipients used?',
          title    : `Excipient Information`,
          //answer: `Retrieve information on a product (please provide either the specific NDC code or the organization). You will receive all the Orange book details of the product, various excipients used with the following details for each excipient: Name, functional
          //category, daily dosage limit, regulatory status, incompatibilities, links for each to Inxight: Drugs`
          answer: `Search by the NDC code or the organization to get further information on for each excipient: Name, functional category, daily dosage limit, regulatory status, incompatibilities, links for each to Inxight: Drugs. Source: Orange Book`
        },
        {
          id: 7,
          question : 'For a particular product, what are the various alternate excipients used?',
          title    : `Alternate Excipient Information`,
          answer   : `Find an alternate excipient for the required formulation based on a proprietary AI / ML algorithm based on the functional use of the excipient.`
        }
        // ,
        // {
        //   id: 7,
        //   question: 'What are alternate excipients that can be used?',
        //   answer:`Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`
        // }
      ],
      ndc: [],
      organization: [],
      select_organization: [],
      select_ndc: [],
      select_alternate_ndc: [],
      excipient_name: [],
      function_category: [],
      show_enquiry_loader: false,
      show_products: false,
      activePage: 1,
      activePagePopup:1,
      totalCount: 0,
      itemPerPage: 10,
      prev_rated:0
    }

  }

  componentDidMount() {
    window.scrollTo(0, 0);
    document.title = `Product Enquiry | Dr.Reddys API`;
    this.props.faLeftMenu();

    

    axios
      .get("/fa/check-rating")
      .then((res) => {
        this.setState({prev_rated:res.data.rated});
      })
      .catch((err) => {

    });

  }

  handleSubmit = (values, actions) => {
    this.setState({ show_confirm_modal: true });
  };

  handleCloseModal = () => {
    this.setState({ show_confirm_modal: false });
  };

  handleSubmitFinal = (values, actions) => {
    this.setState({fa_loader:true});
    let post_obj = { 
      enquiry_type : `${this.state.selected_requested_for}`, 
      product_name : this.state.selected_product,
      comment      : values.comment

    }
    axios
      .post("/fa/request_more_info",post_obj)
      .then((res) => {

        this.props.history.push({
            pathname: "/my_product_enquiries",
            state: {
              message: 'Enquiry submitted successfully'
            }
        });

        this.setState({fa_loader:false});
      })
      .catch((err) => {

    });
  }

  setRequestedFor = (e, setFieldValue) => {
    if (e == null) {
      setFieldValue('requested_for', '');
      this.setState({ show_products: false });
    } else {
      setFieldValue('requested_for', e);

      this.setState({ suggestions: [] }, () => {
        this.setState({ show_products: true });
      });
    }
  }

  onReferenceChange = (event, { newValue }) => {
    
    this.setState(
      {
        product_value: newValue
      }
    );
  };

  // ==== Autosuggest for Reference Query DRL ==== //
  onSuggestionsFetchRequested = ({ value }) => {
    // console.log('value',value);
    const inputValue = value.toLowerCase();
    const inputLength = inputValue.length;

    if (inputLength === 0) {
      return [];
    } else {
      if (inputLength > 2) {
        this.setState({fa_loader:true});
        axios
          .post("/fa/suggest", { id: `${this.state.selected_requested_for}`, txt: inputValue })
          .then((res) => {
            let no_result_found = '';
            if(res.data.data.length == 0){
              no_result_found = 'No Data Found';
            }


            this.setState({suggestions: res.data.data,data_searched:false,more_info:[], selected_product:'', more_info_count:0,fa_loader:false,no_result_found:no_result_found});
            return this.state.suggestions;

          })
          .catch((err) => {

          });
      }
    }
  };

  // ==== Autosuggest for Reference Query DRL ==== //
  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  getSuggestionValue = (suggestion) => {
    this.setState({fa_loader:true});
    axios
      .post("/fa/search", { id: `${this.state.selected_requested_for}`, txt: suggestion.label })
      .then((res) => {
        let show_request_button = false;
        let ndc = [];
        let organization = [];
        let alternate_expients_list = [];
        if (this.state.selected_requested_for == 1 || this.state.selected_requested_for == 3 || this.state.selected_requested_for == 5) {
          show_request_button = true;
        }

        if (this.state.selected_requested_for == 6) {
          ndc = res.data.ndc_code;
          organization = res.data.organization;
        }else if(this.state.selected_requested_for == 7){
          ndc = res.data.ndc_code;
          alternate_expients_list = res.data.alternate_expients_list;
        }

        this.setState({ more_info: res.data.data, selected_product: suggestion.label, more_info_count: res.data.cnt, show_request_button: show_request_button, ndc: ndc, organization: organization,alternate_expients_list:alternate_expients_list,fa_loader:false,data_searched:true,select_display_organization:[],select_display_ndc:[],select_alternate_display_ndc:[] });

        document.getElementById('focus-ausuggest-input').blur();

        axios
        .post("/fa/search_tracking", { enquiry_type: `${this.state.selected_requested_for}`, product_name: suggestion.label })
        .then((res) => {
  
        })
        .catch((err) => {
  
        });

      })
      .catch((err) => {

      });

    return suggestion.label;

  };

  renderSuggestion = (suggestion) => {
    
    return (
      <div style={{ cursor: 'pointer',borderBottom:'1px solid #a2a4a6' }} >
        {suggestion.label}
      </div>
    );
    
  };

  clearAutoSuggest = () => {
    this.setState({ product_value: "",err_enter:"", suggestion: [], more_info: [], more_info_count: 0, activePage: 1,activePagePopup:1, show_request_button: false, ndc: [], organization: [],functional_category:'',unni:'',similarity_index:'',data_searched: false });
  };

  setAlternateExcipientNdcCode = (ncd_event) => {
    // console.log(ncd_event);
    if (ncd_event == null || ncd_event === []) {
      this.setState({ select_alternate_ndc: [],select_alternate_display_ndc:[],functional_category:'',unni:'',similarity_index:'' }, () => this.paginationMoreInfo());
    } else {
      let arr = [ncd_event.value];
      this.setState({ select_alternate_ndc:arr,select_alternate_display_ndc:ncd_event,functional_category:'',unni:'',similarity_index:'' }, () => this.paginationMoreInfo());
    }
  }

  setNdcCode = (ncd_event) => {
    // console.log(ncd_event);
    if (ncd_event == null || ncd_event === []) {
      this.setState({ select_ndc: [],select_display_ndc:[],functional_category:'',unni:'',similarity_index:'' }, () => this.paginationMoreInfo());
    } else {
      let arr = [];
      for (let index = 0; index < ncd_event.length; index++) {
        const element = ncd_event[index];
        arr.push(element.label);
        // console.log('element.label', element.label);
      }
      this.setState({ select_ndc: arr,select_display_ndc:ncd_event }, () => this.paginationMoreInfo());
    }
  }

  setOrganization = (org_event) => {
    // console.log(org_event);
    if (org_event == null || org_event === []) {
      this.setState({ select_organization: [],select_display_organization:[] }, () => this.paginationMoreInfo());
    } else {
      let arr = [];
      for (let index = 0; index < org_event.length; index++) {
        const element = org_event[index];
        arr.push(element.label);
      }
      this.setState({ select_organization: arr,select_display_organization:org_event }, () => this.paginationMoreInfo());
    }
  }

  setDropdown = () => {

    let dbArr = [];
    let selDB;

    for (let index = 0; index < this.state.more_info.length; index++) {
      const element = this.state.more_info[index];

      dbArr.push(
        {
          value:element.drugbank_id,
          label:`${element.drugbank_id}/${element.casnum}`
          
        }
      );

      if(index == 0 && this.state.selected_drug_bank == ''){
        selDB = {
          value:element.drugbank_id,
          label:`${element.drugbank_id}/${element.casnum}`
        }
      }
    }

    if(this.state.selected_drug_bank != ''){
      selDB = this.state.selected_drug_bank;
    }

    return (
      <Select
        value={selDB}
        options={dbArr}
        placeholder={
            `Select Drug Bank Id / CAS Number`
        }
        onChange={(e) => this.setDrugBankDetails(e)}
      />
    );
  }

  setDrugBankDetails = (e) => {
    let sel_index = 0;
    for (let index = 0; index < this.state.more_info.length; index++) {
      const element = this.state.more_info[index];

      if(e.value === element.drugbank_id ){
        sel_index = index;
      }
      
    }

    this.setState({selected_drug_bank:e,drug_bank_index:sel_index});
  }

  getMoreInfo = (id) => {
    let html = [];
    if (id === 1) {
      //this.setState({show_request_button:true});
      html.push(<>
        <div className="listing-table">
        <div className="tableFixHead">
        {/* <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}> */}
          <table className="" style={{ width: "100%" }}>       
            <thead>
              <tr>
                <th width="33.3%">
                  NDC Code
                </th>
                <th width="33.3%">
                  Application Number
                </th>
                <th width="33.3%">
                  Organization
                </th>
              </tr>
            </thead>     
            <tbody>
              {this.state.more_info.map((request, key) => (
                <tr
                  key={key}
                >
                  <td width="33.3%">{request.ndc_code}</td>
                  <td width="33.3%">{request.applicationnumber}</td>
                  <td width="33.3%">{request.organization}</td>
                </tr>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        {/* </Scrollbars> */}
        </div>
        </div>
      </>);
    } else if (id === 2) {
      html.push(<>
        <div className="listing-table">
        <div className="tableFixHead">
          {/* <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}> */}
            <table className="" style={{ width: "100%" }}>
              <thead>
                <tr>
                  <th width="160px" >Product Name</th>
                  <th width="160px" >NDC Code</th>
                  <th width="160px" >NDC Name</th>
                  <th width="160px" >Submission Number</th>
                  <th width="160px" >Sponsor</th>
                  <th width="160px" >Submission Type</th>
                  <th width="160px" >Authorization Status</th>
                  <th width="160px" >Auspar Date</th>
                  <th width="160px" >Publication Date</th>
                </tr>
              </thead>
              <tbody>
                {this.state.more_info.map((request, key) => (
                  <tr
                    key={key}
                  >
                    <td width="160px" >{request.product_name}</td>
                    <td width="160px" >{request.ndc_code}</td>
                    <td width="160px" >{request.ndc_name}</td>
                    <td width="160px" >{request.submission_number}</td>
                    <td width="160px" >{request.sponsor}</td>
                    <td width="160px" >{request.submission_type}</td>
                    <td width="160px" >{request.authorization_status}</td>
                    <td width="160px" >{request.auspar_date}</td>
                    <td width="160px" >{request.publication_date}</td>
                  </tr>
                ))}
                {this.state.more_info.length === 0 && (
                  <tr>
                    <td colSpan="11" align="center">
                      No Data To Display
                      </td>
                  </tr>
                )}
              </tbody>
            </table>
          {/* </Scrollbars> */}
        </div>
        </div>
      </>);
    } else if (id === 3) {
      html.push(<>
        <div className="listing-table">


        <div className="tableFixHead">

        {/* <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}> */}
          <table className="" style={{ width: "100%" }}>
            <thead>
              <tr>
                <th width="160px" >Product Name</th>
                <th width="160px" >NDC Code</th>
                <th width="160px" >Product Number</th>
                <th width="160px" >Authorisation Status</th>
                <th width="160px" >Therapeutic Area</th>
                <th width="160px" >Generic</th>
                <th width="160px" >Biosimilar</th>
                <th width="160px" >Additional Monitoring</th>
                <th width="160px" >Orphan Drug</th>
                <th width="160px" >Marketing Date</th>
                <th width="160px" >Pharma Class</th>
                <th width="160px" >Company Name</th>
              </tr>
            </thead>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <tr
                  key={key}
                >
                  <td width="160px" >{request.productname}</td>
                  <td width="160px" >{request.ndc_code}</td>
                  <td width="160px" >{request.product_number}</td>
                  <td width="160px" >{request.authorisation_status}</td>
                  <td width="160px" >{request.therapeutic_area}</td>
                  <td width="160px" >{request.generic}</td>
                  <td width="160px" >{request.biosimilar}</td>
                  <td width="160px" >{request.additional_monitoring}</td>
                  <td width="160px" >{request.orphan_drug}</td>
                  <td width="160px" >{request.marketing_date}</td>
                  <td width="160px" >{request.pharma_class}</td>
                  <td width="160px" >{request.company_name}</td>

                </tr>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="12" align="center">
                    No Data To Display
                    </td>
                </tr>
              )}
            </tbody>
          </table>
        {/* </Scrollbars> */}
        </div>
        </div>
      </>);
    } else if (id === 4) {

      if(this.state.more_info.length > 0){

        html.push(<div className="row">
          <div className="col-md-6">
            <label>Drug Bank Id / CAS Number</label>
            {this.setDropdown()}
          </div>
        </div>)

        let filtered_arr = [this.state.more_info[this.state.drug_bank_index]];
        html.push(<>
          <div className="clearfix"></div>
          <div className="row">
            <div className="col-md-6">
            <div className="apiBox">
            <h4>Identification</h4>
            <div className="listing-table api-details-table ">
            <div style={{ width: "100%",  maxHeight: 400, overflow: 'hidden', overflowY: 'scroll'  }}>
              <table style={{ width: "100%" }}>
                  <tbody>
                    {filtered_arr.map((request, key) => (
                      <>
                        <tr><td>Drug Bank ID</td><td>{request.drugbank_id}</td></tr>
                        <tr><td>CAS Number</td><td>{request.casnum}</td></tr>
                        <tr><td>Smiles</td><td>{request.smiles}</td></tr>
                        <tr><td>IUPAC Name</td><td>{request.iupac_name}</td></tr>
                        <tr><td>Description</td><td>{request.description}</td></tr>
                        <tr><td>Indication</td><td>{request.indication}</td></tr>
                        <tr><td>Direct Parent</td><td>{request.dc_direct_parent}</td></tr>
                        <tr><td>Average Mass</td><td>{request.average_mass}</td></tr>
                        <tr><td>State</td><td>{request.state}</td></tr>
                        <tr><td>Pharm Classes</td><td>{request.pharm_classes}</td></tr>
                      </>
                    ))}
                    {this.state.more_info.length === 0 && (
                      <tr>
                        <td colSpan="11" align="center">
                          No Data To Display
                          </td>
                      </tr>
                    )}
                  </tbody>
              </table>
              </div>
             </div>
            </div>
            </div>
            <div className="col-md-6">
            <div className="apiBox">
             <h4>Experimental Properties:</h4>  
             <div className="listing-table api-details-table ">
             <div style={{ width: "100%",  maxHeight: 400, overflow: 'hidden', overflowY: 'scroll'  }}>  
              <table className="" style={{ width: "100%" }}>
                <tbody>
                  {filtered_arr.map((request, key) => (
                    <>
                      <tr><td>Molecular Formula X</td><td>{request.molecular_formula_x}</td></tr>
                      <tr><td>Molecular Formula Y</td><td>{request.molecular_formula_y}</td></tr>
                      <tr><td>Logp Y</td><td>{request.logp_y}</td></tr>
                      <tr><td>Logs Y</td><td>{request.logs_y}</td></tr>
                      <tr><td>Calculated Water Solubility</td><td>{request.water_solubility_y}</td></tr>
    
                      <tr><td>PKA Strongest Basic</td><td>{request.pka_strongest_basic}</td></tr>
                      <tr><td>PKA Strongest Acidic</td><td>{request.pka_strongest_acidic}</td></tr>
                      <tr><td>Boiling Point</td><td>{request.boiling_point}</td></tr>
                      <tr><td>Hydrophobicity</td><td>{request.hydrophobicity}</td></tr>
                      <tr><td>Isoelectric Point</td><td>{request.isoelectric_point}</td></tr>
                      <tr><td>Melting Point</td><td>{request.melting_point}</td></tr>
                      <tr><td>Radioactivity</td><td>{request.radioactivity}</td></tr>
                      <tr><td>Caco2 Permeability</td><td>{request.caco2_permeability}</td></tr>
                    </>
                  ))}
                  {this.state.more_info.length === 0 && (
                    <tr>
                      <td colSpan="11" align="center">
                        No Data To Display
                        </td>
                    </tr>
                  )}
                </tbody>
              </table>
              </div>
              </div>
             </div>
            </div>  
            <div className="clearfix"></div>
            <div className="col-md-6"> 
            <div className="apiBox">
              <h4>Calculated Properties:</h4> 
              <div className="listing-table api-details-table ">
              <div style={{ width: "100%",  maxHeight: 400, overflow: 'hidden', overflowY: 'scroll'  }}>  
              <table className="" style={{ width: "100%" }}>
                <tbody>
                  {filtered_arr.map((request, key) => (
                    <>
                      <tr><td>Water Sol X</td><td>{request.water_sol_x}</td></tr>
                      <tr><td>Monoisotopic Mass</td><td>{request.monoisotopic_mass}</td></tr>
                      <tr><td>H Bond Acceptor Count</td><td>{request.h_bond_acceptor_count}</td></tr>
                      <tr><td>H Bond Donor Count</td><td>{request.h_bond_donor_ch_bond_donor_countount}</td></tr>
                      <tr><td>Molecular Formula</td><td>{request.molecular_formula_y}</td></tr>
                      <tr><td>LogP</td><td>{request.logp_x}</td></tr>
                      <tr><td>LogX</td><td>{request.logs_x}</td></tr>
                      <tr><td>PKA</td><td>{request.pka}</td></tr>
                      <tr><td>Physiological Charge</td><td>{request.physiological_charge}</td></tr>
                      <tr><td>Polar Surface Area</td><td>{request.polar_surface_area_psa}</td></tr>
                      <tr><td>Polarizability</td><td>{request.polarizability}</td></tr>
                      <tr><td>Refractivity</td><td>{request.refractivity}</td></tr>
                      
                    </>
                  ))}
                  {this.state.more_info.length === 0 && (
                    <tr>
                      <td colSpan="11" align="center">
                        No Data To Display
                        </td>
                    </tr>
                  )}
                </tbody>
              </table>
              </div>
             </div>
             </div>
            </div>
            <div className="col-md-6">
            <div className="apiBox">
             <h4>Pharmacology:</h4>  
             <div className="listing-table api-details-table ">
             <div style={{ width: "100%",  maxHeight: 400, overflow: 'hidden', overflowY: 'scroll'  }}>
              <table className="" style={{ width: "100%" }}>
                <tbody>
                  {filtered_arr.map((request, key) => (
                    <>
                      <tr><td>Absorption</td><td>{request.absorption}</td></tr>
                      <tr><td>Toxicity</td><td>{request.toxicity}</td></tr>
                      <tr><td>Pharmacodynamics</td><td>{request.pharmacodynamics}</td></tr>
                      <tr><td>Mechanism Of Action</td><td>{request.mechanism_of_action}</td></tr>
                      <tr><td>Volume Of Distribution</td><td>{request.volume_of_distribution}</td></tr>
                      <tr><td>Protein Binding</td><td>{request.protein_binding}</td></tr>
                      <tr><td>Metabolism</td><td>{request.metabolism}</td></tr>
                      <tr><td>Route Of Elimination</td><td>{request.route_of_elimination}</td></tr>
                      <tr><td>Half Life</td><td>{request.half_life}</td></tr>
                      <tr><td>Clearance</td><td>{request.clearance}</td></tr>
                      
                    </>
                  ))}
                  {this.state.more_info.length === 0 && (
                    <tr>
                      <td colSpan="11" align="center">
                        No Data To Display
                        </td>
                    </tr>
                  )}
                </tbody>
              </table>
              </div>
            </div>
            </div>
            </div>
            <div className="clearfix"></div>
          </div>
          
          </>);
      }

      
    } else if (id === 5) {
      html.push(<>
        <div className="listing-table">

        {/* <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}> */}
          <div className="tableFixHead">
          <table className="" style={{ width: "100%" }}>
          <thead>
              <tr>
                <th width="160px" >Patent Number</th>
                <th width="160px" >Product Name</th>
                <th width="160px" >Exclusivity Information</th>
                <th width="160px" >Submission Date</th>
                <th width="160px" >Patent Expiry</th>
                <th width="160px" >DP</th>
                <th width="160px" >DS</th>
                <th width="160px" >Current Assignee</th>
                <th width="160px" >Patent Title</th>
                <th width="160px" >Global Dossier</th>
              </tr>
            </thead>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <tr
                  key={key}
                >
                  <td width="160px" ><a href={request.patent_number_url} target="blank" >{request.patent_number}</a></td>
                  <td width="160px" >{request.productname}</td>
                  <td width="160px" >{request.exclusivity_information}</td>
                  <td width="160px" >{request.submission_date}</td>
                  <td width="160px" >{request.patentexpiry}</td>
                  <td width="160px" >{request.dp}</td>
                  <td width="160px" >{request.ds}</td>
                  <td width="160px" >{request.current_assignee}</td>
                  <td width="160px" >{request.patent_title}</td>
                  <td width="160px" ><a href={request.global_dossier} target='__blank' >{request.global_dossier}</a></td>
                </tr>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                    </td>
                </tr>
              )}
            </tbody>
          </table>
          </div>
          {/* </Scrollbars> */}
        </div>
      </>);
    } else if (id === 6) {
      //this.setState({show_request_button:true});
      html.push(<div className="row">
        {this.state.ndc && this.state.ndc.length > 0 && <div className="col-md-6">
          <label>NDC Code</label>
          <Select
            isMulti
            value={this.state.select_display_ndc}
            options={this.state.ndc}
            isSearchable={true}
            isClearable={true}
            placeholder={
              `Select NDC Code`
            }
            name="ndc"
            onChange={(e) => this.setNdcCode(e)}
          />
        </div>}

        {this.state.organization && this.state.organization.length > 0 && <div className="col-md-6">
          <label>Organization</label>
          <Select
            isMulti
            value={this.state.select_display_organization}
            options={this.state.organization}
            isSearchable={true}
            isClearable={true}
            placeholder={
              `Select Organization`
            }
            name="organization"
            onChange={(e) => this.setOrganization(e)}
          />
        </div>}

      </div>)

      html.push(<>
        <div className="listing-table">

        {/* <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}> */}
        <div className="tableFixHead">
          <table className="" style={{ width: "100%" }}>
          <thead>
            <tr>
              <th width="160px" >NDC Code</th>
              <th width="160px" >Application Number</th>
              <th width="160px" >Organization</th>
              <th width="160px" >No Of Excipients</th>
              <th width="160px" >Dosage Form</th>
              <th width="160px" >Route</th>
              <th width="160px" >Color</th>
              <th width="160px" >Shape</th>
              <th width="160px" >Sizes</th>
              <th width="160px" >Strength</th>
              <th width="160px" >RLD</th>
            </tr>
          </thead>
            <tbody>
              {this.state.more_info.map((request, key) => (
                <tr
                  key={key}
                >
                  <td width="160px" >{request.ndc_code}</td>
                  <td width="160px" >{request.applicationnumber}</td>
                  <td width="160px" >{request.organization}</td>
                  <td width="160px" onClick={() => { this.openExcepientsPopup(request) }} ><span style={{ color: 'blue', cursor: 'pointer', textDecoration: 'underline' }} >{request.no_of_excepients}</span></td>
                  <td width="160px" >{request.dosage_form}</td>
                  <td width="160px" >{request.route}</td>
                  <td width="160px" >{request.color}</td>
                  <td width="160px" >{request.shape}</td>
                  <td width="160px" >{request.sizes}</td>
                  <td width="160px" >{request.strength}</td>
                  <td width="160px" >{request.rld}</td>
                </tr>
              ))}
              {this.state.more_info.length === 0 && (
                <tr>
                  <td colSpan="11" align="center">
                    No Data To Display
                  </td>
                </tr>
              )}
            </tbody>
          </table>
          </div>
          {/* </Scrollbars> */}
        </div>
      </>);



    } else if (id === 7) {
      //this.setState({show_request_button:true});
      html.push(<div className="row">
        {this.state.ndc && this.state.ndc.length > 0 && <div className="col-md-6">
          <label>NDC Code</label>
          <Select
            value={this.state.select_alternate_display_ndc}
            options={this.state.ndc}
            isSearchable={true}
            isClearable={true}
            placeholder={
              `Select NDC Code`
            }
            name="ndc"
            onChange={(e) => this.setAlternateExcipientNdcCode(e)}
          />
        </div>}
      </div>)

      if(this.state.alternate_expients_list.length > 0){

        html.push(<>
          <div className="listing-table">
          <div className="tableFixHead">
          {/* <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}> */}
            <table className="" style={{ width: `${this.state.alternate_expients_list.length*200}px` }}>
              <thead>
                <tr>
                {this.state.alternate_expients_list.map((request, key) => (
                  <th  width="200px">{request.excipientname}</th>
                ))}
                </tr>
              </thead>
              <tbody>
                  <tr>
                  {this.state.alternate_expients_list.map((request, key) => (
                    <td  width="200px">{this.getfunctionCategory(request.functional_category.split(';'),request.excipientname)}</td>
                  ))}
                </tr>
              </tbody>
            </table>
            {/* </Scrollbars> */}
            </div>
          </div>
        </>);

      }

      if(this.state.more_info.length > 0){
        html.push(<>
          <br/>
          <div className="listing-table">
          <div className="tableFixHead">
          {/* <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}> */}
            <table className="" style={{ width: "100%" }}>
              <thead>
                <tr>
                  <th width="160px">
                    UNII
                    <img
                      src={infoIcon}
                      width="15.44"
                      height="18"
                      id="DisabledAutoHideExample1"
                      type="button"
                    />
                    <Tooltip
                      placement="right"
                      isOpen={this.state.tooltipOpen}
                      autohide={false}
                      target="DisabledAutoHideExample1"
                      toggle={()=>{this.setState({tooltipOpen: !this.state.tooltipOpen})}}
                    >
                      Unique Ingredient Identifier
                    </Tooltip>
                  </th>
                  <th width="160px">Excipient Name</th>
                  <th width="160px">Popularity Score</th>
                  <th width="160px">Similarity Score</th>
                  <th width="160px">Potency Details</th>
                  
                  
                </tr>
              </thead>
              <tbody>
                {this.state.more_info.map((request, key) => (
                  <tr
                    key={key}
                  >
                    <td width="160px">{request.unii}</td>
                    <td width="160px">{request.excipient_name}</td>
                    <td width="160px">
                      <a href="javascript:void(0)" id={`DisabledAutoHideExample${request.unii}`} >{request.popularity_score}</a>
                      <Tooltip
                        placement="right"
                        isOpen={this.state[request.unii]}
                        autohide={false}
                        target={`DisabledAutoHideExample${request.unii}`}
                        toggle={()=>{this.setState({[request.unii]: !this.state[request.unii]})}}
                      >
                        {request.popularity_score_original}
                      </Tooltip>

                    </td>
                    <td width="160px">
                      

                      <a href="javascript:void(0)" id={`DisabledAutoHideExampleSim${request.unii}`} >{request.similarity_score}</a>
                      <Tooltip
                        placement="right"
                        isOpen={this.state[`Sim${request.unii}`]}
                        autohide={false}
                        target={`DisabledAutoHideExampleSim${request.unii}`}
                        toggle={()=>{this.setState({[`Sim${request.unii}`]: !this.state[`Sim${request.unii}`]})}}
                      >
                        {request.similarity_score_original}
                      </Tooltip>

                    </td>
                    <td width="160px">{request.potencydetails}</td>
                  </tr>
                ))}
                {this.state.more_info.length === 0 && (
                  <tr>
                    <td colSpan="11" align="center">
                      No Data To Display
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
            {/* </Scrollbars> */}
            </div>
          </div>
        </>);
      }else{
        if(this.state.functional_category != ''){
          html.push(<>
            <br/>
            <div className="listing-table">
            <div className="tableFixHead">
            {/* <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}> */}
              <table className="" style={{ width: "100%" }}>
                <thead>
                  <tr>
                    <th width="160px">
                      UNII
                      <img
                        src={infoIcon}
                        width="15.44"
                        height="18"
                        id="DisabledAutoHideExample1"
                        type="button"
                      />
                      <Tooltip
                        placement="right"
                        isOpen={this.state.tooltipOpen}
                        autohide={false}
                        target="DisabledAutoHideExample1"
                        toggle={()=>{this.setState({tooltipOpen: !this.state.tooltipOpen})}}
                      >
                        Unique Ingredient Identifier
                      </Tooltip>
                    </th>
                    <th width="160px">Excipient Name</th>
                    <th width="160px">Popularity Score</th>
                    <th width="160px">Similarity Score</th>
                    <th width="160px">Potency Details</th>
                    
                    
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colSpan="5" align="center">
                      No Data To Display
                    </td>
                  </tr>
                </tbody>
              </table>
              {/* </Scrollbars> */}
              </div>
            </div>
          </>);
        }
      }

    }

    if (this.state.more_info_count > this.state.itemPerPage && id !== 7) {
      html.push(
        <div className="pagination-area">
          <div className="paginationOuter text-right">
            <Pagination
              hideDisabled
              hideFirstLastPages
              prevPageText="‹‹"
              nextPageText="››"
              activePage={this.state.activePage}
              itemsCountPerPage={this.state.itemPerPage}
              totalItemsCount={this.state.more_info_count}
              itemClass="nav-item"
              linkClass="nav-link"
              activeClass="active"
              pageRangeDisplayed="1"
              onChange={(e) => this.handlePageChange(e)}
            />
          </div>
        </div>
      );
    }
    return html;

  }

  getfunctionCategory = (request,excipientname) => {
    let html_return = request.map((fc, k) => (
      <tr><td><label class="radio-inline"><input type="radio" onClick={()=>this.excipeintRadioButton(fc,excipientname)} checked={this.state.functional_category.toLowerCase() === fc.toLowerCase() && excipientname.toLowerCase() === this.state.unni.toLowerCase() ?true:false} />{` `}{fc}</label></td></tr>
    ));
    let html = [];
    html.push(<table>{html_return}</table>);
    return html;
  }

  excipeintRadioButton = (functional_category,excipient_name) => {

    this.setState({fa_loader:true});
    let obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}`, ndc_code: this.state.select_alternate_ndc,functional_category:functional_category,unni:excipient_name};


    axios
      .post(`/fa/search?page=1`, obj)
      .then((res) => {
        let show_request_button     = false;
        let ndc                     = [];
        let organization            = [];
        let alternate_expients_list = [];
        let similarity_index        = '';

        if (this.state.selected_requested_for == 6) {
          ndc = res.data.ndc_code;
          organization = res.data.organization;
        }else if(this.state.selected_requested_for == 7){
          ndc = res.data.ndc_code;
          alternate_expients_list = res.data.alternate_expients_list;
          similarity_index = res.data.similarity_index;
        }

        this.setState({ more_info: res.data.data, more_info_count: res.data.cnt, show_request_button: show_request_button, ndc: ndc,alternate_expients_list:alternate_expients_list,functional_category:functional_category,unni:excipient_name,similarity_index:similarity_index,fa_loader:false});

      })
      .catch((err) => {

      });

  }

  openExcepientsPopup = (req) => {
    //console.log(req);
    if (req.ndc_code) {
      this.setState({fa_loader:true});
      axios
        .post(`/fa/get_excipients?page=1`, { ndc_code: req.ndc_code })
        .then((res) => {
          if(res.data.data.length > 0){
              let html = [];

              for (let index = 0; index < res.data.data.length; index++) {
                const element = res.data.data[index];

                html.push(<tr key={index}>
                <td width="120px" >{element.ndc_code}</td>
                <td width="120px">{element.unii}</td>
                <td width="120px" >{element.product_name}</td>
                <td width="160px" ><a target="blank" href={element.unii_url} >{element.excipientname}</a></td>
                <td width="160px" >{element.dailydosagelimit}</td>
                <td width="160px" >{element.alternate_excipients}</td>
                <td width="300px" >{element.functional_category}</td>
                <td width="300px" >{element.incompatibilities}</td>
                </tr>);
              }

              let rethtml = [];

              rethtml.push(<div className="listing-table popup-excipients-reddy">
                 {/* <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 300, minHeight: 300 }}> */}
                 <div className="tableFixHead">
                <table className="" style={{ width: "1440px" }}>
                  <thead>
                    <tr>
                      <th width="120px" >NDC Code</th>
                      <th width="120px" >UNII</th>
                      <th width="120px" >Product Name</th>
                      <th width="160px" >Excipient Name</th>
                      <th width="160px" >Daily Dosage Limit</th>
                      <th width="160px" >Alternate Excipients</th>
                      <th width="300px" >Functional Category</th>
                      <th width="300px" >Incompatibilities</th>
                    </tr>
                  </thead>
                  <tbody>
                    {html}
                  </tbody>
                </table>
                </div>
                {/* </Scrollbars> */}
            </div>)


            if (res.data.cnt > this.state.itemPerPage) {
              
              rethtml.push(<div className="pagination-area" style={{width:'5%'}} >
                  <div className="paginationOuter text-right">
                    <Pagination
                      hideDisabled
                      hideFirstLastPages
                      prevPageText="‹‹"
                      nextPageText="››"
                      activePage={this.state.activePagePopup}
                      itemsCountPerPage={this.state.itemPerPage}
                      totalItemsCount={res.data.cnt}
                      itemClass="nav-item"
                      linkClass="nav-link"
                      activeClass="active"
                      pageRangeDisplayed="1"
                      onChange={(e) => this.handlePageChangePopup(e,req.ndc_code)}
                    />
                  </div>
                </div>)
              
            }


            this.setState({ popup_data: rethtml, open_popup: true,fa_loader:false });

          }
        })
        .catch((err) => {
          let html = [];
          html.push(<div className="listing-table popup-excipients-reddy">
             {/* <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 300, minHeight: 300}}> */}
             <div className="tableFixHead">
              <table className="" style={{ width: "100%" }}>
                <thead>
                  <tr>
                    <th>NDC Code</th>
                    <th>
                      UNII
                    </th>
                    <th>Product Name</th>
                    <th>Excipient Name</th>
                    <th>Daily Dosage Limit</th>
                    <th>Alternate Excipients</th>
                    <th>Functional Category</th>
                    <th>Incompatibilities</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colSpan="11" align="center">
                        No Data To Display
                    </td>
                  </tr>
                </tbody>
              </table>
              </div>
              {/* </Scrollbars> */}
          </div>);

          this.setState({ popup_data: html, open_popup: true,fa_loader:false });
        });
    }
  }

  handlePageChangePopup = (pageNumber,ndc_code) => {
    this.setState({ activePagePopup: pageNumber });
    this.paginationMoreInfoPopup(
      pageNumber > 0 ? pageNumber : 1,ndc_code
    );
  };

  paginationMoreInfoPopup = (page = 1,ndc_code = '') => {
    if (ndc_code != '') {
      this.setState({fa_loader:true});
      axios
        .post(`/fa/get_excipients?page=${page}`, { ndc_code: ndc_code })
        .then((res) => {
          if (res.data.data.length > 0) {
            let html = [];

            let dynamic_html = [];

            for (let index = 0; index < res.data.data.length; index++) {
              const element = res.data.data[index];

              dynamic_html.push(<tr key={index}>

                <td width="120px" >{element.ndc_code}</td>
                <td width="120px">{element.unii}</td>
                <td width="120px" >{element.product_name}</td>
                <td width="160px" ><a target="blank" href={element.unii_url} >{element.excipientname}</a></td>
                <td width="160px" >{element.dailydosagelimit}</td>
                <td width="160px" >{element.alternate_excipients}</td>
                <td width="300px" >{element.functional_category}</td>
                <td width="300px" >{element.incompatibilities}</td>

              </tr>);

            }

            html.push(<div className="listing-table popup-excipients-reddy">
              {/* <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 300, minHeight: 300 }}> */}
              <div className="tableFixHead">
                <table className="" style={{ width: "1440px" }}>
                  <thead>
                    <tr>
                      <th width="120px" >NDC Code</th>
                      <th width="120px" >UNII</th>
                      <th width="120px" >Product Name</th>
                      <th width="160px" >Excipient Name</th>
                      <th width="160px" >Daily Dosage Limit</th>
                      <th width="160px" >Alternate Excipients</th>
                      <th width="300px" >Functional Category</th>
                      <th width="300px" >Incompatibilities</th>
                    </tr>
                  </thead>
                  <tbody>
                    {dynamic_html}
                  </tbody>
                </table>
              </div>
              {/* </Scrollbars> */}
            </div>);


            if (res.data.cnt > this.state.itemPerPage) {
              
              html.push( <div className="pagination-area" style={{width:'5%'}} >
                  <div className="paginationOuter text-right">
                    <Pagination
                      hideDisabled
                      hideFirstLastPages
                      prevPageText="‹‹"
                      nextPageText="››"
                      activePage={this.state.activePagePopup}
                      itemsCountPerPage={this.state.itemPerPage}
                      totalItemsCount={res.data.cnt}
                      itemClass="nav-item"
                      linkClass="nav-link"
                      activeClass="active"
                      pageRangeDisplayed="1"
                      onChange={(e) => this.handlePageChangePopup(e,ndc_code)}
                    />
                  </div>
                </div>)
              
            }


            this.setState({ popup_data: html, open_popup: true,fa_loader:false });

          }
        })
        .catch((err) => {
          let html = [];

          html.push(<div className="listing-table popup-excipients-reddy">
            {/* <Scrollbars style={{ width: "100%", height: 'auto', maxHeight: 400, minHeight: 300 }}> */}
            <div className="tableFixHead">
              <table className="" style={{ width: "100%" }}>
                <thead>
                  <tr>
                    <th>NDC Code</th>
                    <th>
                      UNII 
                    </th>
                    <th>Product Name</th>
                    <th>Excipient Name</th>
                    <th>Daily Dosage Limit</th>
                    <th>Alternate Excipients</th>
                    <th>Functional Category</th>
                    <th>Incompatibilities</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colSpan="11" align="center">
                        No Data To Display
                    </td>
                  </tr>
                </tbody>
              </table>
              </div>
              {/* </Scrollbars> */}
          </div>);

          this.setState({ popup_data: html, open_popup: true,fa_loader:false });
        });
    }
  };

  handlePageChange = (pageNumber) => {
    this.setState({ activePage: pageNumber });
    this.paginationMoreInfo(
      pageNumber > 0 ? pageNumber : 1
    );
  };

  paginationMoreInfo = (page = 1) => {
    this.setState({fa_loader:true});
    let obj = {};
    if (this.state.selected_requested_for == 7) {
      obj = { 
        id                  : `${this.state.selected_requested_for}`, 
        txt                 : `${this.state.selected_product}`, 
        ndc_code            : this.state.select_alternate_ndc,
        functional_category : `${this.state.functional_category}`,
        unni                : `${this.state.unni}`,
        similarity_index    : `${this.state.similarity_index}`
      };

    }else if (this.state.selected_requested_for == 6) {
      obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}`, ndc_code: this.state.select_ndc, organization: this.state.select_organization };
    } else {
      obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}` };
    }

    axios
      .post(`/fa/search?&page=${page}`, obj)
      .then((res) => {
        let show_request_button     = false;
        let ndc                     = [];
        let organization            = [];
        let alternate_expients_list = [];

        if (this.state.selected_requested_for == 1 || this.state.selected_requested_for == 3 || this.state.selected_requested_for == 5) {
          show_request_button = true;
        }

        if (this.state.selected_requested_for == 6) {
          ndc = res.data.ndc_code;
          organization = res.data.organization;
        }else if(this.state.selected_requested_for == 7){
          ndc = res.data.ndc_code;
          alternate_expients_list = res.data.alternate_expients_list;
        }

        this.setState({ more_info: res.data.data, more_info_count: res.data.cnt, show_request_button: show_request_button, ndc: ndc, organization: organization,alternate_expients_list:alternate_expients_list,fa_loader:false});

      })
      .catch((err) => {

      });
  };

  handlePopupClose = () => {
    this.setState({ popup_data: '', open_popup: false, activePagePopup:1 });
  }

  handlePopupCloseErr = () => {
    this.setState({ popup_data: '', open_err_popup: false });
  }

  downloadSearch = (event) => {
    this.setState({fa_loader:true});
    event.preventDefault();
    let obj = {};
    if (this.state.selected_requested_for == 6) {
      obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}`, ndc_code: this.state.select_ndc, organization: this.state.select_organization };
    }else if(this.state.selected_requested_for == 7){
      obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}`, ndc_code: this.state.select_alternate_ndc,functional_category:this.state.functional_category,unni:this.state.unni}
    } else {
      obj = { id: `${this.state.selected_requested_for}`, txt: `${this.state.selected_product}` };
    }

    if (this.state.selected_requested_for == 1) {
      var file_name = "Orange Book details";
    } else if (this.state.selected_requested_for == 2) {
      var file_name = "TGA details";
    } else if (this.state.selected_requested_for == 3) {
      var file_name = "EMA details";
    } else if (this.state.selected_requested_for == 4) {
      var file_name = "API DrugBank details";
    } else if (this.state.selected_requested_for == 5) {
      var file_name = "Patents Associated details";
    } else if (this.state.selected_requested_for == 6) {
      var file_name = "Excipients details";
    } else if (this.state.selected_requested_for == 7) {
      var file_name = "Alternate Excipients details";
    }
    axios
      .post(`/fa/search_download`, obj, { responseType: "blob" })
      .then((res) => {
        this.setState({fa_loader:false});
        let url = window.URL.createObjectURL(res.data);
        let a = document.createElement("a");
        a.href = url;
        a.download = `${file_name}.xlsx`;
        a.click();
      })
      .catch((err) => {
        //showErrorMessage(err, this.props);
      });
  }

  getQuestions = () => {

    let html = this.state.requested_for_list.map((element,index)=>{
      return (
        <li><label className={(this.state.selected_requested_for == element.id)?'active':''} ><input type="radio" onClick={()=>this.showAnswer(element.id,element.answer)} />{element.question}</label></li>
      );
    })

    return html;
  } 

  showAnswer = (id,answer) => {

    if(id > 0){
      this.setState({ selected_requested_for: id,selected_answer:answer, product_value: "",err_enter:"", suggestion: [], more_info: [], more_info_count: 0, activePage: 1, activePagePopup: 1,show_request_button: false, ndc: [], organization: [],functional_category:'',unni:'',similarity_index:'',data_searched:false,no_result_found:'' });
    }

  }

  enterPressed = (event) => {
    var code = event.keyCode || event.which;
    if (code === 13) {
      if(this.state.product_value.length < 3){
        this.setState({err_enter:'Please enter atleast 3 letters'});
      }else{
        
        this.setState({fa_loader:true});
        axios
        .post("/fa/suggest", { id: `${this.state.selected_requested_for}`, txt: this.state.product_value })
        .then((res) => {
          this.setState({fa_loader:false});
          this.setState({ suggestions: res.data.data,data_searched:false,more_info:[], selected_product:'', more_info_count:0 });
          return this.state.suggestions;

        })
        .catch((err) => {

        });

      }
    }
  };

  hideError = () => {
    this.setState({ err_enter: "" });
  };

  getTabContent = () => {
    return this.state.requested_for_list.map((element,index)=>{
      return (
        <Tab eventKey={element.id} title={element.title}>
          <div className="clearfix"></div>
          <p>{element.answer}</p>
          {/* {this.state.more_info.length > 0 && <span className="downloadBtn" onClick={(e) => { this.downloadSearch(e) }} style={{cursor:'pointer'}} >Download Excel</span>} */}
          {this.state.fa_loader === false && this.state.more_info && this.getMoreInfo(element.id)}
          <div className="clearfix"></div>
        </Tab>
      );
    })
  }

  changeTabs = (id) => {
    if(id > 0){
      this.setState({ selected_requested_for: id,err_enter:"", more_info: [], more_info_count: 0, activePage: 1, activePagePopup: 1,show_request_button: false, ndc: [], organization: [],functional_category:'',unni:'',similarity_index:'',no_result_found:'',fa_loader:true,dynamic_html:''},()=>{
        axios
        .post("/fa/search", { id: `${id}`, txt: this.state.selected_product })
        .then((res) => {
          let show_request_button = false;
          let ndc = [];
          let organization = [];
          let alternate_expients_list = [];
          if (this.state.selected_requested_for == 1 || this.state.selected_requested_for == 3 || this.state.selected_requested_for == 5) {
            show_request_button = true;
          }

          if (this.state.selected_requested_for == 6) {
            ndc = res.data.ndc_code;
            organization = res.data.organization;
          }else if(this.state.selected_requested_for == 7){
            ndc = res.data.ndc_code;
            alternate_expients_list = res.data.alternate_expients_list;
          }

          this.setState({ more_info: res.data.data, more_info_count: res.data.cnt, show_request_button: show_request_button, ndc: ndc, organization: organization,alternate_expients_list:alternate_expients_list,select_display_organization:[],select_display_ndc:[],select_alternate_display_ndc:[],fa_loader:false});

          axios
          .post("/fa/search_tracking", { enquiry_type: `${id}`, product_name: `${this.state.selected_product}` })
          .then((res) => {

          })
          .catch((err) => {

          });

        })
        .catch((err) => {

        });
      });
    }

  }

  showRatingPopup = (e) => {
    e.preventDefault();
    let lang_data = require(`../../../assets/lang/en.json`);
    this.setState({ popup_lang: lang_data }, () => {
      this.setState({
        optionsArr: [],
        questionsArr: [],
        showPendingPopup: false,
        showThankyouPopup: false,
        pendingRatings: false,
      });
      axios
        .get(`fa/rating-options`)
        .then((res) => {
          this.setState(
            {
              optionsArr: res.data.options,
              questionsArr: res.data.questions,
              task_rating: 0,
              task_rating_application:0
              //task_rating_application:res.data.previously_rated
            },
            () => {
              this.setQuestion();
            }
          );
        })
        .catch((err) => {});
    });
  };

  setQuestion = () => {
    let question = this.state.popup_lang.my_requests.initial_review_text;
    let options = "";
    let html2 = "";
    for (let index = 0; index < this.state.questionsArr.length; index++) {
      const element = this.state.questionsArr[index];

      let rate = element.rating.split(",");
      if (rate.length == 1) {
        if (rate[0] == this.state.task_rating) {
          question = element.name;
          break;
        }
      } else {
        for (let index = 0; index < rate.length; index++) {
          if (rate[index] == this.state.task_rating) {
            question = element.name;
            break;
          }
        }
        break;
      }
    }

    let html = question;

    this.setState({
      ratingHtml: html,
      ratingHtml2: html2,
      showRRPopup: true,
      rating_err: "",
      rating_err_prod:""
    });
  };

  changeQuestion = (submitted_rating) => {
    let question = "";
    let options = [];
    let html2 = "";

    for (let index = 0; index < this.state.questionsArr.length; index++) {
      const element = this.state.questionsArr[index];

      let rate = element.rating.split(",");
      if (rate.length == 1) {
        if (rate[0] == submitted_rating) {
          question = element.name;
          break;
        }
      } else {
        for (let index = 0; index < rate.length; index++) {
          if (rate[index] == submitted_rating) {
            question = element.name;
            break;
          }
        }
        //break;
      }
    }

    for (let index = 0; index < this.state.optionsArr.length; index++) {
      const element = this.state.optionsArr[index];

      let rate = element.rating.split(",");
      if (rate.length == 1) {
        if (rate[0] == submitted_rating) {
          options.push(
            <>
              <Col sm={6}>
                <label className="customCheckBoxbtn">
                  <input
                    key={index}
                    type="checkbox"
                    onClick={(e) => {
                      let prev_state = this.state.selOptions;
                      if (e.target.checked) {
                        prev_state.push(element.o_id);
                      } else {
                        const index = prev_state.indexOf(element.o_id);
                        if (index > -1) {
                          prev_state.splice(index, 1);
                        }
                      }
                      this.setState({ selOptions: prev_state });
                    }}
                  />
                  <span className="checkmark ">{htmlDecode(element.name)}</span>
                </label>
              </Col>
            </>
          );
        }
      } else {
        for (let index = 0; index < rate.length; index++) {
          if (rate[index] == submitted_rating) {
            options.push(
              <>
                <Col sm={6}>
                  <label className="customCheckBoxbtn">
                    <input
                      key={index}
                      type="checkbox"
                      onClick={(e) => {
                        let prev_state = this.state.selOptions;
                        if (e.target.checked) {
                          prev_state.push(element.o_id);
                        } else {
                          const index = prev_state.indexOf(element.o_id);
                          if (index > -1) {
                            prev_state.splice(index, 1);
                          }
                        }
                        this.setState({ selOptions: prev_state });
                      }}
                    />
                    <span className="checkmark ">
                      {htmlDecode(element.name)}
                    </span>
                  </label>
                </Col>
              </>
            );
            break;
          }
        }
      }
    }
    html2 = options;

    let html = question;

    this.setState({
      ratingHtml: html,
      ratingHtml2: html2,
      task_rating: submitted_rating,
      rating_err: "",
      rating_err_prod:""
    });
  };

  changeApplicationRating = (submitted_rating) => {
    this.setState({
      task_rating_application: submitted_rating
    });
  };

  handleRatings = (values, actions) => {
    if (this.state.task_rating === 0) {
      this.setState({
        rating_err: this.state.popup_lang.display_error.form_error
          .ratings_before_submitting,
      });
      return false;
    } else if (this.state.task_rating_application === 0) {
      this.setState({
        rating_err_prod: this.state.popup_lang.display_error.form_error
          .ratings_before_submitting,
      });
      return false;
    }else {
      this.setState({ showLoader: true });
      axios
        .post("fa/post-ratings", {
          rating: this.state.task_rating,
          task_rating_application: this.state.task_rating_application,
          comment: this.state.ratingComment,
          options: this.state.selOptions,
          product:this.state.product_value
        })
        .then((res) => {
          this.setState({
            optionsArr: [],
            selOptions: [],
            questionsArr: [],
            showRRPopup: false,
            rating_err: "",
            rating_err_prod:"",
            ratingComment: "",
            ratingHtml: "",
            ratingHtml2: "",
            task_rating: 5,
            showLoader: false,
          });
          this.openThankyouPopup();
        })
        .catch((err) => {});
    }
  };

  handleRRClose = (e) => {
    e.preventDefault();
    this.setState({
      optionsArr: [],
      selOptions: [],
      questionsArr: [],
      showRRPopup: false,
      ratingComment: "",
      ratingHtml: "",
      ratingHtml2: "",
      task_rating: 5,
      rating_err: "",
      rating_err_prod: ""
    });
  };

  openThankyouPopup = () => {
    this.setState({ showThankyouPopup: true});
  };

  handleThankyouClose = (e) => {
    //e.preventDefault();
    this.setState({ showThankyouPopup: false,prev_rated:1});
  };

  render() {

    if(this.props.faAcess === false){
      window.location.href='dashboard'
      return null;
    }

    const inputProps = {
      placeholder: `Enter Product Name`,
      value: this.state.product_value,
      type: "search",
      onChange: this.onReferenceChange,
      onKeyPress: this.enterPressed.bind(this),
      id:'focus-ausuggest-input',
      className:
        this.state.product_value != '' ? "form-control customInput" : "form-control customInput"
    };

    if (this.state.show_enquiry_loader) {
      return <Loader />;
    } else {
      return (
        <>
          {this.state.fa_loader && <Loader />}
          <div className="container-fluid clearfix formSec">

            <div className="dashboard-content-sec productEnquirySectionNew">
              <div className="service-request-form-sec newFormSec">
                <div className="form-page-title-block">
                  <h2>
                    Formulation Handbook <img src={formulationImg} height={`30px`}/>
                  </h2>
                  <div className="rightHeadBtnSec">
                  <Link
                    to={'#'}
                    className="enquiries_btn"
                    onClick={()=>{
                      this.props.history.push("/my_product_enquiries");
                    }}
                  >
                    My Enquiries
                  </Link>
                  <Link
                    to={'#'}
                    className="samples_btn"
                    onClick={()=>{
                      this.props.history.push("/sample");
                    }}
                  >
                    Request For Samples
                  </Link>
                  {this.state.data_searched && <Link
                    to={'#'}
                    className="feedback_btn"
                    onClick={(e) =>
                      this.showRatingPopup(e)
                    }
                  >
                    Rate Us
                  </Link> }
                  </div>
                </div>

              
              <div className="productEnquiryWrapperShowing">
              
                <div className="row">

                  {this.state.err_enter !== '' && <div className="messageserror" style={{display:"block"}}>
                    <Link to="#" className="close">
                      <img
                        src={closeIcon}
                        width="17.69px"
                        height="22px"
                        onClick={(e) => this.hideError()}
                      />
                    </Link>
                    <div>
                      <ul className="">
                        <li className="">{this.state.err_enter}</li>
                      </ul>
                    </div>
                  </div>}

                  <div className="col-md-12">
                    <Autosuggest
                      autoComplete="off"
                      suggestions={this.state.suggestions}
                      onSuggestionsFetchRequested={
                        this.onSuggestionsFetchRequested
                      }
                      onSuggestionsClearRequested={
                        this.onSuggestionsClearRequested
                      }
                      getSuggestionValue={(suggestion) =>
                        this.getSuggestionValue(suggestion)
                      }
                      renderSuggestion={this.renderSuggestion}
                      defaultShouldRenderSuggestions={true}
                      inputProps={inputProps}
                    />
                    {this.state.product_value !== "" && (
                      <div className="close-icon-container">
                        <a
                          className="close-icon"
                          onClick={this.clearAutoSuggest}
                        >
                          <img
                            src={closeIconSelect}
                            width="10"
                            height="10"
                          />
                        </a>
                      </div>
                    )}

                  </div>
                </div>
                {this.state.no_result_found != '' && <><div className="clearfix"></div><br/>{this.state.no_result_found}</>}
              </div>

              {this.state.data_searched && <div className="productEnquiryWrapperShowing-white">
                <Tabs defaultActiveKey={this.state.selected_requested_for} id="uncontrolled-tab-example" onSelect={(k) =>  this.changeTabs(k)} >
                  {this.getTabContent()}
                </Tabs>

                 <div className="clearfix"></div>

                <Formik
                initialValues={initialValues}
                validationSchema={false}
                onSubmit={this.handleSubmit}
              >
                {({
                  errors,
                  values,
                  touched,
                  setErrors,
                  setFieldValue,
                  setFieldTouched,
                }) => {

                  return (
                    <Form>
                      <div className="row">
                        <div className="col-md-12">
                          
                        </div>

                      </div>

                      <div className="row">
                        <div className="col btnSecEnquiry">
                          <br />

                          {this.state.show_request_button && this.state.selected_requested_for == 5 && 
                          <Link
                            to={'#'}
                            className="btn btn-ip-enquiry"
                            onClick={()=>{
                              this.props.history.push("/ip_related_enquiries");
                            }}
                          >
                            IP Related Enquiries
                          </Link>
                          }

                          {this.state.show_request_button && <button style={{ width: '230px' }} className="btn btn-default" type="submit" >
                            {" "}
                            Request Further Info
                          </button>}
                          {this.state.show_request_button && <button
                            className="btn btn-default btn-cancel"
                            onClick={() => {
                              this.props.history.push("/my_product_enquiries")
                            }}
                          >
                            {this.props.dynamic_lang.form_component.cancel}
                          </button>}
                        </div>
                      </div>
                    </Form>
                  );
                }}
              </Formik>
              </div>}


              


                {this.state.show_confirm_modal === true && (
                  <Modal
                    show={this.state.show_confirm_modal}
                    backdrop="static"
                    className="grantmodal"
                  >
                    <Formik
                      initialValues={false}
                      validationSchema={false}
                      onSubmit={this.handleSubmitFinal}
                    >
                      {({
                        values,
                        errors,
                        touched,
                        isValid,
                        isSubmitting,
                        handleChange,
                        setFieldValue,
                        setFieldTouched,
                        setErrors,
                      }) => {
                        return (
                          <Form>
                            <Modal.Header>
                              <Modal.Title>
                                <center>
                                  Request Additional Details
                                  
                                </center>
                              </Modal.Title>
                              <button className="close" onClick={this.handleCloseModal} />
                            </Modal.Header>
                            <Modal.Body>
                              {/* <div>
                                <Row>

                                  <Editor
                                    name="comment"
                                    content={values.comment}
                                    init={{
                                      menubar: false,
                                      branding: false,
                                      placeholder: `Enter Addition Details (Optional)`,
                                      plugins:
                                        "link table hr visualblocks code placeholder lists",
                                      toolbar:
                                        "bold italic strikethrough superscript subscript | alignleft aligncenter alignright alignjustify | numlist bullist | removeformat underline | link unlink | blockquote table  hr | visualblocks code ",
                                      content_css: ["/css/editor.css"],
                                    }}
                                    onEditorChange={(value) => {
                                      setFieldValue("comment", value);
                                      this.setState({
                                        comment: value
                                      });
                                    }}
                                  />

                                </Row>
                                {this.state.escalation_error != '' && <p style={{ color: 'red' }} >{this.state.escalation_error}</p>}
                              </div> */}
                              <center>
                                <div className="innerInfo">
                                  <Row>
                                    {this.state.selected_requested_for == 1 && `For more information on the dosage forms, pill details (color, shape, size), strength, or whether a RLD is available, please click on Submit`}

                                    {this.state.selected_requested_for == 3 && `For information on the last modified date or approved indications, please click on Submit`}

                                    {this.state.selected_requested_for == 5 && `For information on dependent / independent claims on the patents (independent or dependent claims), please click on Submit`}
                                  </Row>
                                </div>
                              </center>
                              <ButtonToolbar>

                                <Button
                                  className={`btn btn-default`}
                                  type="submit"
                                //disabled={isValid ? false : true}
                                >
                                  Submit
                              </Button>

                                <Button
                                  className={`btn btn-default btn-cancel`}
                                  type="button"
                                  onClick={this.handleCloseModal}
                                >
                                  Cancel
                              </Button>

                              </ButtonToolbar>
                            </Modal.Body>
                            {/* <Modal.Footer>
                            
                          </Modal.Footer> */}
                          </Form>
                        );
                      }}
                    </Formik>
                  </Modal>
                )}

                {this.state.open_popup && <Modal
                  show={this.state.open_popup}
                  backdrop="static"
                  className="grantmodal excipient_popup_new"

                >
                  <Modal.Header>
                    <Modal.Title>
                      Excipient Details
                    </Modal.Title>
                    <button className="close" onClick={this.handlePopupClose} />
                  </Modal.Header>
                  <Modal.Body style={{ width: '100%' }}>
                    {this.state.popup_data}
                  </Modal.Body>
                </Modal>}

              </div>
            </div>
          </div>

                  {/* RATING POPUP */}
        {this.state.showRRPopup === true && (
          <Modal
            show={this.state.showRRPopup}
            onHide={this.handleRRClose}
            backdrop="static"
            className="modalRating"
          >
            <Modal.Header>
              <div className="cntrimg">
                <img src={require("../../../assets/images/rat-2.svg")} alt="" />
              </div>

              {/* <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Empty </Tooltip>}>
                <ButtonToolbar>
               
                </ButtonToolbar>
              </OverlayTrigger> */}

              <img
                src={closeIconSelect}
                width="15.44"
                height="18"
                type="button"
                onClick={(e) => this.handleRRClose(e)}
                className="close"
              />

              {/* <Button
                className="close"
                onClick={(e) => this.handleRRClose(e)}
              /> */}
            </Modal.Header>
            <Formik
              initialValues={false}
              validationSchema={false}
              onSubmit={this.handleRatings}
            >
              {({
                values,
                errors,
                touched,
                isValid,
                isSubmitting,
                handleChange,
                setFieldValue,
                setFieldTouched,
                setErrors,
              }) => {
                return (
                  <Form>
                    <Modal.Body>
                      <Modal.Title>
                        <div className="ratinfo">
                          <h4>
                            Rate Your Overall Experience
                          </h4>
                          <h3></h3>
                        </div>
                      </Modal.Title>
                      
                      <div className="ratinfo starimg">
                        <span>Rate Formulation Handbook</span>
                        <br/>
                        <Rating
                          initialRating={this.state.task_rating_application}
                          onChange={(rate) => this.changeApplicationRating(rate)}
                          emptySymbol={
                            <img
                              src={require("../../../assets/images/uncheck-star.svg")}
                              alt=""
                              className="icon"
                            />
                          }
                          placeholderSymbol={
                            <img
                              src={require("../../../assets/images/uncheck-stars.svg")}
                              alt=""
                              className="icon"
                            />
                          }
                          fullSymbol={
                            <img
                              src={require("../../../assets/images/uncheck-stars.svg")}
                              alt=""
                              className="icon"
                            />
                          }
                        />
                        {this.state.rating_err && (
                          <p
                            style={{
                              color: "red",
                              fontSize: "14px",
                              marginBottom: "0px",
                              marginTop: "4px",
                            }}
                          >
                            {this.state.popup_lang.my_requests.forgot_to_rate}
                          </p>
                        )}
                        <br/>
                        <span>Rate Search Results For <b>{this.state.product_value}</b></span>
                        <br/>  
                        <Rating
                          initialRating={this.state.task_rating}
                          onChange={(rate) => this.changeQuestion(rate)}
                          emptySymbol={
                            <img
                              src={require("../../../assets/images/uncheck-star.svg")}
                              alt=""
                              className="icon"
                            />
                          }
                          placeholderSymbol={
                            <img
                              src={require("../../../assets/images/uncheck-stars.svg")}
                              alt=""
                              className="icon"
                            />
                          }
                          fullSymbol={
                            <img
                              src={require("../../../assets/images/uncheck-stars.svg")}
                              alt=""
                              className="icon"
                            />
                          }
                        />

                        {this.state.rating_err_prod && (
                          <p
                            style={{
                              color: "red",
                              fontSize: "14px",
                              marginBottom: "0px",
                              marginTop: "4px",
                            }}
                          >
                            {this.state.popup_lang.my_requests.forgot_to_rate}
                          </p>
                        )}
                        <textarea
                          className="inputratng"
                          placeholder={htmlDecode(this.state.ratingHtml)}
                          onChange={(e) => {
                            this.setState({ ratingComment: e.target.value });
                          }}
                        ></textarea>
                        <br />
                        <center>
                          <Row className="checkmargin">
                            {this.state.ratingHtml2}
                          </Row>
                        </center>

                        <ButtonToolbar className="txtcntr">
                          <Button
                            className={` submit m-b-0 btn-fill ${
                              isValid ? "btn-custom-green" : "btn-disable"
                            } m-r-10`}
                            type="submit"
                            disabled={this.state.task_rating > 0 ? false : true}
                          >
                            {this.state.popup_lang.my_requests.submit}
                          </Button>
                        </ButtonToolbar>
                      </div>
                    </Modal.Body>
                  </Form>
                );
              }}
            </Formik>
          </Modal>
        )}

        {/* THANK YOU POPUP */}
        {this.state.showThankyouPopup === true && (
          <Modal
            show={this.state.showThankyouPopup}
            onHide={this.handleThankyouClose}
            backdrop="static"
            className="modalthnx"
          >
            <Modal.Header>
              <div className="thankuhead">
                <img
                  src={require("../../../assets/images/thank-You-1.svg")}
                  alt=""
                />
                <button
                  className="close"
                  onClick={(e) => this.handleThankyouClose(e)}
                />
                <p className="thankutxt">
                  {this.state.popup_lang.my_requests.thank_you}
                </p>
              </div>
            </Modal.Header>

            <Modal.Body>
              <div className="infopara">
                <p>{this.state.popup_lang.my_requests.sharing_feedback}</p>
                <p>{this.state.popup_lang.my_requests.overall_process}</p>
                {this.state.pendingRatings && (
                  <>
                    <p className="txtcntr">
                      {this.state.popup_lang.my_requests.pending_reviews}
                    </p>
                    <div className="d-flex justify-content-center infopara">
                      <Link to="#" onClick={(e) => this.openPendingPopup(e)}>
                        {this.state.popup_lang.my_requests.share_your_feedback}
                      </Link>
                    </div>
                  </>
                )}
              </div>
            </Modal.Body>
          </Modal>
        )}
        {/* DISCLAIMER */}
        <div className="container-fluid">
        <div className="desclaimerInfo">
        <span>
          <span>Disclaimer:</span>
          <span>The information and  recommendations provided in the “Formulation Handbook”  are only for general guidance only.  The content herein has been taken from publicly available databases (like Orange Book, EMA, TGA etc.).” Although greatest possible care has been taken in compiling, checking and developing the content to ensure that it is accurate and complete, the authors, Dr.Reddy’s Laboratories Limited are not responsible or in any way liable for reliance placed on or action taken basis of the information in this "Formulation Handbook" or any errors, omissions or inaccuracies and/or incompleteness of the information in this tool, whether arising from negligence or otherwise.</span>
        </span>
        </div>
        </div>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    customerList: state.user.customerList,
    selCompany: state.user.selCompany,
    dynamic_lang: state.auth.dynamic_lang,
    faAcess:state.auth.faAcess
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    faLeftMenu: () => dispatch(faHideLeftMenu())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductEnquiryAmit);
