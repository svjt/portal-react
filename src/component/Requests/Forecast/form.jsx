import React, { Component, Alert } from "react";
import { Link } from "react-router-dom";
// import Layout from "../../Layout/layout";
import Select from "react-select";

import { Tooltip } from "reactstrap";
import infoIcon from "../../../assets/images/info-circle-solid.svg";

import "react-datepicker/dist/react-datepicker.css";
import axios from "../../../shared/axios";
import { Formik, Form } from "formik";

import * as Yup from "yup";

import {
  htmlDecode,
  supportedFileType,
  BasicUserData,
} from "../../../shared/helper";
import { Editor } from "@tinymce/tinymce-react";
import Dropzone from "react-dropzone";
import closeIcon from "../../../assets/images/times-solid-red.svg";
import Loader from "../../Loader/loader";
import { connect } from "react-redux";

var path = require("path");

var cc_Customer = [];

//validation
const initialValues = {
  messages: "",
  file_name: [],
  share_with_agent: 1,
};
const validationSchema = Yup.object().shape({
  messages: Yup.string().test(
    "message-Isvalid",
    "Passwords must match ya fool",
    function (value) {
      return this.parent.file_name === "";
    }
  ),
  file_name: Yup.string().test(
    "message-Isvalid",
    "Passwords must match ya fool",
    function (value) {
      return this.parent.messages === "";
    }
  ),
});

class ForecastForm extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.state = {
      startDate: new Date(),
      tooltipOpen: false,
      showLoader: false,
      share_with_agent: true,
      selectedOption: null,
      products: [],
      countryList: [],
      selectedDate: null,
      selectedCountry: null,
      cc_Customers: [],
      files: [],
      filesHtml: "",
      rejectedFile: [],
      error: false,
      customerList: [],
      ccAgentCustomerList: [],
      showCustomerList: false,
      pre_selected: [],
      errMsg: null,
      agent_customer_id: null,
      displayCCList: false,
    };
  }

  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    });
  }

  deleteFile = () => {
    this.setState({ files: [] });
  };

  customerChecked = (event) => {
    var tempArray = [];

    if (event.target.checked === true) {
      this.state.cc_Customers.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 1;
        }
        tempArray.push(user);
      });
    } else {
      this.state.cc_Customers.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 0;
        }
        tempArray.push(user);
      });
    }
    this.setState({ cc_Customers: tempArray });
  };

  agentcustomerChecked = (event) => {
    var tempArray = [];

    if (event.target.checked === true) {
      this.state.ccAgentCustomerList.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 1;
        }
        tempArray.push(user);
      });
    } else {
      this.state.ccAgentCustomerList.map((user, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          user.checked = 0;
        }
        tempArray.push(user);
      });
    }
    this.setState({ ccAgentCustomerList: tempArray });
  };

  recId = (event) => {
    var tempArray = [];

    if (event.target.checked === true) {
      this.state.files.map((file, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          file.checked = event.target.checked;
        }
        tempArray.push(file);
      });
    } else {
      this.state.files.map((file, fileIndex) => {
        if (event.target.value.toString() === fileIndex.toString()) {
          file.checked = event.target.checked;
        }
        tempArray.push(file);
      });
    }

    this.setState({ files: tempArray });
  };
  getCc_CustomerList() {
    axios
      .get("tasks/get-cc-customers")
      .then((res) => {
        cc_Customer = res.data.data;
        let pre_selected = res.data.pre_selected;
        this.setState({
          cc_Customers: cc_Customer,
          pre_selected: pre_selected,
        });
      })
      .catch((err) => {});
  }

  componentDidUpdate = (prevProps) => {
    if (this.props.selCompany !== prevProps.selCompany) {
      this.setState({ agent_customer_id: null, ccAgentCustomerList: [] });
    }
  };

  componentDidMount() {
    this.getProductList();
    this.getCountryList();

    this.setState({ files: [], filesHtml: "" });
    document.title =
      this.props.dynamic_lang.forecast.title + ` | Dr. Reddy's API`;

    let userData = BasicUserData();
    if (userData.role === 2) {
      this.setState({ displayCCList: true });
    } else {
      this.getCc_CustomerList();
    }
  }
  getCustomerList() {
    let company_id = 0;
    if (company_id > 0) {
      axios
        .get(`/agents/customers?cid=${company_id}`)
        .then((res) => {
          var customer_list = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            customer_list.push({
              value: element["customer_id"],
              label: htmlDecode(element["cust_name"]),
            });
          }
          this.setState({ customerList: customer_list });
        })
        .catch((err) => {});
    }
  }
  getProductList() {
    axios
      .get("/products")
      .then((res) => {
        var product_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          product_list.push({
            value: element["product_id"],
            //label: htmlDecode(element['medicalCondition'])
            label: htmlDecode(element["product_name"]),
          });
        }
        this.setState({ products: product_list });
      })
      .catch((err) => {});
  }
  getCountryList() {
    axios
      .get("/country")
      .then((res) => {
        var country_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          country_list.push({
            value: element["country_id"],
            //label: htmlDecode(element['medicalCondition'])
            label: htmlDecode(element["country_name"]),
          });
        }
        this.setState({ countryList: country_list });
      })
      .catch((err) => {});
  }
  fileChangedHandler = (event) => {
    this.setState({ upload_file: event.target.files[0] });
  };

  changeDate = (event, setFieldValue) => {
    setFieldValue("auditDate", event);
  };
  setDropZoneFiles = (acceptedFiles) => {
    var rejectedFiles = [];
    var uploadFile = [];

    var totalfile = acceptedFiles.length;

    for (var index = 0; index < totalfile; index++) {
      var error = 0;
      var filename = acceptedFiles[index].name.toLowerCase();
      var extension_list = supportedFileType();
      var ext_with_dot = path.extname(filename);
      var file_extension = ext_with_dot.split(".").join("");

      var obj = {};

      var fileErrText = this.props.dynamic_lang.display_error.form_error
        .following_extensions;

      if (extension_list.indexOf(file_extension) === -1) {
        error = error + 1;

        if (totalfile > 1) {
          if (index === 0) {
            obj["errorText"] =
              this.props.dynamic_lang.display_error.form_error.error_text_1.replace(
                "[file_name]",
                filename
              ) + fileErrText;
          } else {
            obj["errorText"] =
              this.props.dynamic_lang.display_error.form_error.error_text_2.replace(
                "[file_name]",
                filename
              ) + fileErrText;
          }
        } else {
          obj["errorText"] =
            this.props.dynamic_lang.display_error.form_error.error_text_2.replace(
              "[file_name]",
              filename
            ) + fileErrText;
        }
        rejectedFiles.push(obj);
      }

      if (acceptedFiles[index].size > 50000000) {
        obj[
          "errorText"
        ] = this.props.dynamic_lang.display_error.form_error.allowed_size.replace(
          "[file_name]",
          filename
        );
        error = error + 1;
        rejectedFiles.push(obj);
      }

      if (error === 0) {
        uploadFile.push(acceptedFiles[index]);
      }
    }

    this.setState({
      files: uploadFile,
    });

    this.setState({
      rejectedFile: rejectedFiles,
    });
  };

  handleSubmit = (values, actions) => {
    //alert(JSON.stringify(values))

    if (
      this.state.files.length === 0 &&
      values.messages.replace(/\s/g, "") === ""
    ) {
      this.setState({ error: true });
    } else {
      this.setState({ showLoader: true });
      var cc_Cust = [];
      this.setState({ errMsg: "" });

      var formData = new FormData();

      let userData = BasicUserData();
      if (userData.role === 2) {
        if (this.state.ccAgentCustomerList) {
          this.state.ccAgentCustomerList.map((user, index) => {
            if (user.checked === 1) {
              cc_Cust.push({ customer_id: user.customer_id });
            }
          });
        }
        if (
          this.state.agent_customer_id &&
          this.state.agent_customer_id !== null &&
          this.state.agent_customer_id.value > 0
        ) {
          formData.append(
            "agent_customer_id",
            this.state.agent_customer_id.value
          );
        }
      } else {
        this.state.cc_Customers.map((user, index) => {
          if (user.checked === 1) {
            cc_Cust.push({ customer_id: user.customer_id });
          }
        });
      }

      if (this.state.files && this.state.files.length > 0) {
        for (let index = 0; index < this.state.files.length; index++) {
          const element = this.state.files[index];
          formData.append("file", element);
        }
      } else {
        formData.append("file", []);
      }

      formData.append("request_category_type", 4);

      formData.append("description", values.messages);

      formData.append("cc_customers", JSON.stringify(cc_Cust));
      formData.append("share_with_agent", values.share_with_agent);

      axios
        .post("tasks/forecast", formData)
        .then((res) => {
          if (res.status === 200) {
            this.setState({ showLoader: false });
            // alert("Task added");
            this.props.history.push({
              pathname: "/my_forecast",

              state: {
                message: this.props.dynamic_lang.forecast.thank_you,
              },
            });
          } else {
            alert("Failed to add Task");
          }
        })
        .catch((err) => {
          this.setState({ showLoader: false });

          actions.setSubmitting(false);
          //  alert(err.data.status)

          if (err.data.status === 2) {
            //alert(err.data.errors.customer_name);
            if (
              err.data.errors.customer_name &&
              err.data.errors.customer_name != ""
            ) {
              //alert(err.data.errors.customer_name);
              this.setState({ errMsg: err.data.errors.customer_name }, () =>
                console.log("kkk", this.state.errMsg)
              );
            }
            actions.setErrors(err.data.errors);
          } else {
            this.setState({ errMsg: err.data.message });
          }
        });
    }
  };

  removeError = (e) => {
    this.setState({ error: false });
  };

  removeFile_error = (e) => {
    this.setState({ rejectedFile: [], files: [] });
  };

  removeError_new = () => {
    this.setState({ errMsg: "" });
  };

  shareWithAgent = (event, setFieldValue) => {
    if (event.target.checked === true) {
      setFieldValue("share_with_agent", 1);
    } else {
      setFieldValue("share_with_agent", 0);
    }
  };

  getAgentCCList = (event, setFieldValue) => {
    this.setState({ agent_customer_id: event });

    if (event === null || event === "") {
      setFieldValue("agent_customer_id", "");
      this.setState({ ccAgentCustomerList: [] });
    } else {
      axios
        .get(`/agents/cc_customers?custid=${event.value}`)
        .then((res) => {
          var customer_list = [];
          for (let index = 0; index < res.data.data.length; index++) {
            const element = res.data.data[index];
            customer_list.push({
              value: element["customer_id"],
              label: htmlDecode(element["cust_name"]),
            });
          }
          this.setState({
            ccAgentCustomerList: res.data.data,
            pre_selected: res.data.pre_selected,
          });
          setFieldValue("agent_customer_id", event.value);
        })
        .catch((err) => {});
    }
  };

  render() {
    const {
      selectedOption,
      products,
      countryList,
      selectedCountry,
      customerList,
      ccAgentCustomerList,
      showCustomerList,
    } = this.state;

    return (
      <div className="container-fluid clearfix formSec">
        <div className="dashboard-content-sec">
          <div className="service-request-form-sec">
            <div className="form-page-title-block">
              <h2>{this.props.dynamic_lang.forecast.title}</h2>
            </div>
            {this.state.showLoader === true ? <Loader /> : null}
            <Formik initialValues={initialValues} onSubmit={this.handleSubmit}>
              {({
                errors,
                values,
                touched,
                setErrors,
                setFieldValue,
                setFieldTouched,
              }) => {
                return (
                  <Form style={{ paddingTop: "0px" }}>
                    <div
                      className="messageserror"
                      style={{
                        display: this.state.error === true ? "block" : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.removeError(e)}
                        />
                      </Link>
                      <div>
                        <ul className="">
                          {this.state.error === true ? (
                            <li className="">
                              {this.props.dynamic_lang.forecast.field_required}
                            </li>
                          ) : null}
                        </ul>
                      </div>
                    </div>
                    <div className="clearfix" />
                    <div className="col-md-12">
                      <p className="text-center" style={{ paddingTop: "10px" }}>
                        {this.props.dynamic_lang.forecast.upload_description}
                      </p>
                    </div>
                    <div
                      className="messageserror"
                      style={{
                        display:
                          this.state.errMsg &&
                          this.state.errMsg !== null &&
                          this.state.errMsg !== ""
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.removeError_new()}
                        />
                      </Link>
                      <div>
                        <ul className="">
                          <li className="">{this.state.errMsg}</li>
                        </ul>
                      </div>
                    </div>
                    <div
                      className="messageserror"
                      style={{
                        display:
                          (this.state.rejectedFile &&
                            this.state.rejectedFile.length) > 0
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.removeFile_error(e)}
                        />
                      </Link>

                      {this.state.rejectedFile &&
                        this.state.rejectedFile.map((file, index) => {
                          return (
                            <div key={index}>
                              <ul className="">
                                <li className="">{file.errorText}</li>
                              </ul>
                            </div>
                          );
                        })}
                    </div>

                    {this.state.files && this.state.files.length > 0 ? (
                      <div>
                        <center>
                          <label className="file file-image">
                            {this.state.files[0].name}
                          </label>
                          <a
                            href="#"
                            onClick={(e) => this.deleteFile()}
                            className="removeBtn2"
                          >
                            {this.props.dynamic_lang.forecast.remove}{" "}
                          </a>
                        </center>
                      </div>
                    ) : (
                      <div className="row">
                        <div className="col-md-12 forecastSec">
                          <label className="">
                            {this.props.dynamic_lang.forecast.attachment}
                          </label>
                          <div className="clearfix" />

                          <Dropzone
                            multiple={false}
                            onDrop={(acceptedFiles) => {
                              this.setDropZoneFiles(acceptedFiles);
                            }}
                          >
                            {({ getRootProps, getInputProps }) => (
                              <section>
                                <div {...getRootProps()} className="customFile">
                                  <input
                                    {...getInputProps()}
                                    style={{ display: "block" }}
                                  />
                                  {/* <p>Upload file</p> */}
                                </div>
                                {/* <div className="form-check-input">
                                {this.state.filesHtml}
                              </div> */}
                              </section>
                            )}
                          </Dropzone>
                          <div className="clearfix" />
                          <p>{this.props.dynamic_lang.forecast.or}</p>
                        </div>
                      </div>
                    )}

                    <div className="row">
                      <div className="col-md-12">
                        <Editor
                          name="messages"
                          content={values.messages}
                          init={{
                            menubar: false,
                            branding: false,
                            placeholder: this.props.dynamic_lang.forecast
                              .textarea_requirements,
                            plugins:
                              "link table hr visualblocks code placeholder paste lists textcolor",
                            paste_data_images: true,
                            paste_use_dialog: true,
                            paste_auto_cleanup_on_paste: false,
                            paste_convert_headers_to_strong: true,
                            paste_strip_class_attributes: "none",
                            paste_remove_spans: false,
                            paste_remove_styles: false,
                            paste_retain_style_properties: "all",
                            toolbar:
                              "bold italic strikethrough superscript subscript | forecolor backcolor | removeformat underline | link unlink | alignleft aligncenter alignright alignjustify | numlist bullist | blockquote table  hr | formatselect | visualblocks code | paste ",
                            content_css: ["/css/editor.css"],
                            color_map: [
                              "000000",
                              "Black",
                              "808080",
                              "Gray",
                              "FFFFFF",
                              "White",
                              "FF0000",
                              "Red",
                              "FFFF00",
                              "Yellow",
                              "008000",
                              "Green",
                              "0000FF",
                              "Blue",
                            ]
                          }}
                          onEditorChange={(value) =>
                            setFieldValue("messages", value)
                          }
                        />
                        {/* <Field
                            name="messages"
                            type="textarea"
                            component="textarea"
                            className="form-control customTextarea"
                            placeholder="Enter Any Other Requirements"
                            rows="5"
                            cols="60"
                          />  */}
                      </div>
                    </div>

                    {this.state.displayCCList && (
                      <div className="col-md-12 mt-20 forcastSelect">
                        <Select
                          options={
                            this.props.customerList !== null
                              ? this.props.customerList
                              : []
                          }
                          isSearchable={true}
                          isClearable={true}
                          value={this.state.agent_customer_id}
                          placeholder="Select User *"
                          onChange={(e) =>
                            this.getAgentCCList(e, setFieldValue)
                          }
                        />
                      </div>
                    )}

                    {this.state.share_with_agent === true &&
                      this.state.displayCCList === false && (
                        <div className="row mt-10">
                          <div className="col-md-12">
                            <div className="form-check">
                              <label className="form-check-label">
                                <input
                                  type="checkbox"
                                  className="form-check-input"
                                  value={1}
                                  defaultChecked={true}
                                  onChange={(e) =>
                                    this.shareWithAgent(e, setFieldValue)
                                  }
                                />
                                {
                                  this.props.dynamic_lang.forecast
                                    .display_request_agent
                                }
                              </label>
                            </div>
                          </div>
                        </div>
                      )}

                    {((this.state.displayCCList &&
                      ccAgentCustomerList &&
                      ccAgentCustomerList.length > 0) ||
                      (this.state.pre_selected &&
                        this.state.pre_selected.length > 0) ||
                      (this.state.cc_Customers &&
                        this.state.cc_Customers.length > 0)) && (
                      <div className="row  mt-10">
                        <div className="col-md-12">
                          <span
                            className="fieldset-legend"
                            id="DisabledAutoHideExample"
                          >
                            {
                              this.props.dynamic_lang.forecast
                                .grant_access_request
                            }
                            <img
                              src={infoIcon}
                              width="20"
                              height="20"
                              id="DisabledAutoHideExample"
                              type="button"
                            />
                            <Tooltip
                              placement="right"
                              isOpen={this.state.tooltipOpen}
                              autohide={false}
                              target="DisabledAutoHideExample"
                              toggle={this.toggle}
                            >
                              {
                                this.props.dynamic_lang.forecast
                                  .grant_access_request_tooltip
                              }
                            </Tooltip>
                          </span>

                          {this.state.pre_selected &&
                            this.state.pre_selected.map((cust, index) => {
                              return (
                                <div key={index} className="form-check">
                                  <label className="form-check-label">
                                    <input
                                      id={index}
                                      type="checkbox"
                                      className="form-check-input"
                                      defaultChecked={true}
                                      disabled={true}
                                    />
                                    {cust.first_name} {cust.last_name}
                                  </label>
                                </div>
                              );
                            })}

                          {this.state.displayCCList &&
                            ccAgentCustomerList &&
                            ccAgentCustomerList.map((user, index) => {
                              return (
                                <div key={index} className="form-check">
                                  <label className="form-check-label">
                                    <input
                                      id={index}
                                      type="checkbox"
                                      className="form-check-input"
                                      value={index}
                                      //checked={user.status}
                                      onChange={(e) =>
                                        this.agentcustomerChecked(e)
                                      }
                                    />
                                    {user.first_name} {user.last_name} (
                                    {user.company_name})
                                  </label>
                                </div>
                              );
                            })}

                          {this.state.cc_Customers &&
                            this.state.cc_Customers.map((user, index) => {
                              return (
                                <div key={index} className="form-check">
                                  <label className="form-check-label">
                                    <input
                                      id={index}
                                      type="checkbox"
                                      className="form-check-input"
                                      value={index}
                                      //checked={user.status}
                                      onChange={(e) => this.customerChecked(e)}
                                    />
                                    {user.first_name} {user.last_name} (
                                    {user.company_name})
                                  </label>
                                </div>
                              );
                            })}
                          {/* <div className="form-check">
                            <label className="form-check-label">
                              <input
                                type="checkbox"
                                className="form-check-input"
                                value=""
                              />
                              Option 1
                            </label>
                          </div>  */}
                          {/* <div className="form-check">
                            <label className="form-check-label">
                              <input
                                type="checkbox"
                                className="form-check-input"
                                value=""
                              />
                              Option 2
                            </label>
                          </div>  */}
                        </div>
                      </div>
                    )}
                    <div className="row">
                      <div className="col text-center">
                        <button className="btn btn-default">
                          {this.props.dynamic_lang.forecast.submit}
                        </button>
                        <button
                          className="btn btn-default btn-cancel"
                          onClick={() => this.props.history.push("/dashboard")}
                        >
                          {this.props.dynamic_lang.forecast.cancel}
                        </button>
                      </div>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    customerList: state.user.customerList,
    selCompany: state.user.selCompany,
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(ForecastForm);
