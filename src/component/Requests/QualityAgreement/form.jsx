import React, { Component, Alert } from "react";

import * as Yup from "yup";

import FormComponent from "../../FormComponent/formComponent";
import { connect } from "react-redux";

//validation
const initialValues = {
  product_id: "",
  country_id: "",
  messages: "",
  file: "",
  file_name: [],
};

const validationSchema = (refObj) =>
  Yup.object().shape({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
    file_name: Yup.string().test(
      "required",
      refObj.props.dynamic_lang.display_error.form_error.upload_agreement,
      function (value) {
        return value ? true : false;
      }
    ),
  });

class QualityAgreementForm extends Component {
  state = {
    placeHolderText: this.props.dynamic_lang.form_component.select_market,
    displayCountry: true,
    apiPath: "tasks/quality-agreement",
    heading: this.props.dynamic_lang.form_component.quality_agreement,
    request_type_heading: "Quality Agreement",
    /* labelAttachment: "Please Upload Agreement (Either download Dr Reddy's format or upload agreement in your format).", */
    initialValue: initialValues,
    disPlayPharmacopoeia: false,
    customClassName: "col-md-6",
    disPlayPolymorPhicForm: false,
    downloadFileBelow: true,
  };

  componentDidMount() {}

  render() {
    return (
      <FormComponent
        {...this.props}
        state={this.state}
        validationSchema={() => validationSchema(this)}
      />
    );
  }
}

//export default QualityAgreementForm;
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(QualityAgreementForm);
