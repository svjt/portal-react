import React, { Component, Alert } from "react";

import * as Yup from "yup";

import FormComponent from "../../FormComponent/formComponent";
import { connect } from "react-redux";

//validation
const initialValues = {
  product_id: "",
  country_id: "",
  messages: "",
  file: "",
  file_name: [],
};

const validationSchema = (refObj) =>
  Yup.object().shape({
    product_id: Yup.string().required(
      refObj.props.dynamic_lang.display_error.form_error.product_id
    ),
    country_id: Yup.string()
      .nullable()
      .required(refObj.props.dynamic_lang.display_error.form_error.country_id),
  });

class RequestLOCLOEForm extends Component {
  state = {
    placeHolderText: this.props.dynamic_lang.form_component
      .select_market_required,
    displayCountry: true,
    apiPath: "tasks/request-for-loc-loe",
    heading: this.props.dynamic_lang.form_component.request_for_loc_loe,
    request_type_heading: "Request for LOC/LOE",
    labelAttachment: this.props.dynamic_lang.form_component.attachment,
    initialValue: initialValues,
    disPlayPharmacopoeia: false,
    customClassName: "col-md-6",
    disPlayPolymorPhicForm: false,
  };

  componentDidMount() {}

  render() {
    return (
      <FormComponent
        {...this.props}
        state={this.state}
        validationSchema={() => validationSchema(this)}
      />
    );
  }
}

//export default RequestLOCLOEForm;
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(RequestLOCLOEForm);
