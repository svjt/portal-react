import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "../../shared/axios";
import dateFormat from "dateformat";
import API from "../../shared/axios";
import { supportedFileType } from "../../shared/helper";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import Dropzone from "react-dropzone";
import { Row, Col } from "reactstrap";
import infoIcon from "../../assets/images/download-new.svg";
import Tour from 'reactour';
import infoIconNew from "../../assets/images/info-circle-solid.svg";
import closeIconSelect from "../../assets/images/close.svg";

import { htmlDecode, BasicUserData } from "../../shared/helper";
import ReactHtmlParser from "react-html-parser";
import {
  ButtonToolbar,
  Button,
  Modal,
  OverlayTrigger
} from "react-bootstrap";
import { Tooltip } from "reactstrap";

// SATYAJIT
import { connect } from "react-redux";
import { submitDiscussion } from "../../store/actions/discussion";
import { updateToken } from "../../store/actions/auth";
import closeIconSuccess from "../../assets/images/times-solid-green.svg";
import closeIcon from "../../assets/images/times-solid-red.svg";
import { Editor } from "@tinymce/tinymce-react";
import Loader from "../Loader/loader";
import { stat } from "fs";
var path = require("path");

const discussion_download_all = `${process.env.REACT_APP_API_URL}/tasks/download_all_discussions/`; // SVJT

//validation
const initialValues = {
  comment: "",
  selected_option: ""

};

class Discussion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      discussionData: [],
      discussionErr: null,
      files: [],
      comments: [],
      discussionFile: [],
      commentText: "",
      isSuccess: false,
      comment: "",
      loadDiscussionForm: false,
      showLoader: false,
      toggleFile: false,
      profile_pic: "",
      user_type: "",
      owner: 0,
      task_id: 0,
      is_report: this.props.is_report, // SATYAJIT [17.12.2019]
      cancelSap: (this.props.cancelSap) ? this.props.cancelSap : false,
      isLoading: true,
      showClosePopup: false,
      sendComment: '',
      task_language_explicit: '',
      closed_discussion_err: false,
      closed_discussion_err_txt: '',
      open_tour_counter_closed_task: true,
      stop_feature:true
    };
    this.inputRef = React.createRef();
  }
  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue, errors) => {
    var rejectedFiles = [];
    var uploadFile = [];

    for (var index = 0; index < acceptedFiles.length; index++) {
      var error = 0;
      var filename = acceptedFiles[index].name.toLowerCase();
      var extension_list = supportedFileType();
      var ext_with_dot = path.extname(filename);
      var file_extension = ext_with_dot.split(".").join("");

      if (extension_list.indexOf(file_extension) === -1) {
        error = error + 1;
        rejectedFiles.push(acceptedFiles[index]);
      }
      if (acceptedFiles[index].size > 50000000) {
        error = error + 1;
        rejectedFiles.push(acceptedFiles[index]);
      }

      if (error === 0) {
        uploadFile.push(acceptedFiles[index]);
        setErrors({ file_name: false });
      }
    }

    // //setErrors({ file_name: false });
    // errors.file_name = [];
    //setFieldValue(this.state.files);

    var prevFiles = this.state.files;
    var newFiles = [];
    if (prevFiles.length > 0) {
      for (let index = 0; index < uploadFile.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (uploadFile[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        if (remove === 0) {
          prevFiles.push(uploadFile[index]);
        }
      }

      prevFiles.map((file) => {
        file.checked = false;
        newFiles.push(file);
      });
    } else {
      uploadFile.map((file) => {
        file.checked = false;
        newFiles.push(file);
      });
    }

    this.setState({
      files: newFiles,
    });

    setFieldValue("file_name", newFiles);

    this.setState({
      rejectedFile: rejectedFiles,
    });

    this.inputRef.current.value = null; // clear input value - SATYAJIT
  };

  validationSchema = () => {
    if (this.props.discussion.taskDetails.close_status === 1 && this.state.stop_feature === false) {
      return Yup.object().shape({
        comment: Yup.string()
          .required(this.state.task_language_explicit.display_error.form_error.comment)
          .test((values) => {
            // console.log("discussion values ===>", values);
            return values === "<p><br></p>" ? false : true;
          }),
        selected_option: Yup.number()
          .required(this.state.task_language_explicit.display_error.form_error.select_an_option)
          .test((values) => {
            return values === "<p><br></p>" ? false : true;
          }),
      });
    } else {
      return Yup.object().shape({
        comment: Yup.string()
          .required(this.state.task_language_explicit.display_error.form_error.comment)
          .test((values) => {
            // console.log("discussion values ===>", values);
            return values === "<p><br></p>" ? false : true;
          }),
      });
    }
  }

  deleteFile = (e) => {
    e.preventDefault();
    var uploadFile = [];
    this.state.files.map((file, index) => {
      if (index.toString() === e.target.name.toString()) {
      } else {
        uploadFile.push(file);
      }
    });
    this.setState({ files: uploadFile });
  };

  handleSubmit = (values, actions) => {
    this.setState({ showLoader: true, isSuccess: false });

    if (this.props.discussion.taskDetails.close_status == 1 && this.state.stop_feature === false) {

      if (values.selected_option == 3) {

        var request_body = {
          files: this.state.files,
          task_id: this.state.task_id,
          comment: values.comment,
          owner: this.state.owner,
          is_report: this.state.is_report,
          cancelSap: this.state.cancelSap
        };

        this.props.submitUserDiscussion(request_body, () => {
          if (this.props.discussionErr) {
            actions.setErrors(this.props.discussionErr.data.errors);
            this.setState({ showLoader: false });
          } else {
            this.setState({
              discussionData: [],
              files: [],
              comments: [],
              discussionFile: [],
              comment: "",
              loadDiscussionForm: true,
              showLoader: false,
            });

            // if report popup is opened then close it else load discussion panel -- SATYAJIT [17.12.2019]
            if (this.props.is_report) {
              this.props.closeDiscussPopup();
            } else {
              this.fetchDiscussion();
            }
            this.setState({
              closed_discussion_err: true,
              closed_discussion_err_txt: this.state.task_language_explicit.task_details.your_comment
            });
            this.handleRRClose();
          }
        });
      } else {
        var formData = new FormData();
        if (this.state.files && this.state.files.length > 0) {
          for (let index = 0; index < this.state.files.length; index++) {
            formData.append("file", this.state.files[index]);
          }
        } else {
          formData.append("file", []);
        }
        formData.append("task_id", this.state.task_id);
        formData.append("comment", values.comment);
        formData.append("selected_options", values.selected_option);

        axios
          .post("tasks/closed-discussion", formData)
          .then((res) => {

            this.setState({
              discussionData: [],
              files: [],
              comments: [],
              discussionFile: [],
              comment: "",
              loadDiscussionForm: true,
              showLoader: false,
            });

            if (this.props.is_report) {
              this.props.closeDiscussPopup();
            } else {
              this.fetchDiscussion();
            }

            this.setState({
              closed_discussion_err: true,
              closed_discussion_err_txt: `${values.selected_option === 1 ? this.state.task_language_explicit.task_details
                .new_request_created : this.state.task_language_explicit.task_details
                  .new_request_additional_created}`
            });

            this.handleRRClose();
          })
          .catch((err) => {
            this.setState({
              sendComment: '',
              showClosePopup: false,
              showLoader: false,
              closed_discussion_err: true,
              closed_discussion_err_txt: err.data.message
            });
          });
      }

    } else {
      var request_body = {
        files: this.state.files,
        task_id: this.state.task_id,
        comment: values.comment,
        owner: this.state.owner,
        is_report: this.state.is_report,
        cancelSap: this.state.cancelSap
      };

      this.props.submitUserDiscussion(request_body, () => {
        if (this.props.discussionErr) {
          actions.setErrors(this.props.discussionErr.data.errors);
          this.setState({ isSuccess: false, showLoader: false });
        } else {
          this.setState({
            discussionData: [],
            files: [],
            comments: [],
            discussionFile: [],
            comment: "",
            loadDiscussionForm: true,
            isSuccess: true,
            showLoader: false,
          });

          // if report popup is opened then close it else load discussion panel -- SATYAJIT [17.12.2019]
          if (this.props.is_report) {
            this.props.closeDiscussPopup();
          } else {
            this.fetchDiscussion();
          }
        }
      });
    }
  };

  closeTourCounterClosedTask = () => {
    this.setState({ open_tour_counter_closed_task: false });
  };

  handleRRClose = (e) => {
    this.setState({
      sendComment: '',
      showClosePopup: false
    });
  };

  handleClosedDiss = (e) => {
    this.setState({
      closed_discussion_err: false,
      closed_discussion_err_txt: ''
    });
  };

  fetchDiscussion() {
    this.setState({
      discussionData: this.props.discussion,
      task_id: this.props.discussion.taskDetails.task_id,
      comments: !this.state.is_report ? this.props.discussion.comments : [], // stop loading the discussion-SATYAJIT [17.12.2019]
      loadDiscussionForm:
        this.props.discussion.taskDetails.duplicate_task === 1 ? true : false,
    });
  }

  componentDidUpdate = () => {
    if (
      this.props.discussion.comments.length > 0 &&
      this.props.is_report === false &&
      this.state.comments != this.props.discussion.comments
    ) {
      this.setState({ comments: this.props.discussion.comments });
    }
  };

  componentWillReceiveProps = (nextProps) => {
    if(nextProps.activeTab == 2){
      this.updateCounter();
    }
  }

  componentDidMount = () => {
    let userTypeData = BasicUserData();
    this.setState({ user_type: userTypeData.role });

    let lang_data = require(`../../assets/lang/${this.props.discussion.taskDetails.language}.json`);
    this.setState({ task_language_explicit: lang_data });

    if (this.props.profile_pic !== this.state.profile_pic) {
      this.setState({
        profile_pic: this.props.profile_pic,
      });
    }
    this.setState({ owner: this.props.discussion.taskDetails.owner });
    this.fetchDiscussion();
    
    let userData = BasicUserData();
    if (userData) {
      this.setState({
        isLoading: false,
      });
    }
  };

  hideFileError = () => {
    this.setState({ rejectedFile: [] });
  };

  hideError = (setErrors) => {
    setErrors({});
  };

  // handleEditorChange(e) {
  //   console.log(e.target.getContent());
  // }

  hideSuccessMsg = () => {
    this.setState({ isSuccess: false });
  };

  handleAttachment = () => {
    this.setState({ toggleFile: !this.state.toggleFile });
  };

  changeDateFormat = (input_date) => {
    if (input_date) {
      //var dateFormat = require('dateformat');
      //dateFormat(input_date.replace("-", "/"), "d mmm yyyy")
      var date_time = input_date.split(" ");
      var date = date_time[0].split("-");
      var date_format = new Date(date[0], date[1] - 1, date[2]);
      return dateFormat(date_format, "dddd, mmmm d, yyyy - h:MM");
    }
  };

  closePopup = () => {
    this.props.closeDiscussPopup();
  };

  disableFeature = (event) => {
    event.preventDefault();
    axios
      .get(`tasks/tour_done_closed_task/${this.state.task_id}`, {})
      .then((res) => {
        axios
          .get("/generate_token")
          .then(response => {
            // console.log(response.data.token);
            this.props.updateTourToken(response.data.token);
          })
          .catch(error => {
          })
      })
      .catch((err) => { });
  }

  updateCounter = () => {
    if(this.props.discussion.taskDetails.close_status == 1 && this.state.stop_feature === false){
      if(this.props.tour_counter_closed_task == true){
        axios.get(`/tasks/update_tour_counter_closed_task/${this.state.task_id}`)
        .then(res => {
          axios
            .get("/generate_token")
            .then(response => {
              this.props.updateTourToken(response.data.token)
            })
            .catch(error => {
            })
        })
        .catch(err => {
        });
      }
    }
  }

  render() {
    var tour_steps = [];
    //console.log("---------props-----------",this.props);
    if(this.props.discussion.taskDetails.close_status == 1 && this.state.stop_feature === false){
      if(this.props.tour_counter_closed_task == true){
        tour_steps = [
          {
            selector: '#first-step-new',
            content: ({ goTo, inDOM }) => (
              <div>
                
                {this.state.task_language_explicit.task_details.tooltip1}
                <br />
                <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={() => goTo(1)} >{this.state.task_language_explicit.task_details.ok_got}</button>
  
              </div>
            )
          },
          {
            selector: '#second-step-new',
            content: ({ goTo, inDOM }) => (
              <div>
                
                {this.state.task_language_explicit.task_details.tooltip2}
                <br />
                <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={() => goTo(2)} >{this.state.task_language_explicit.task_details.ok_got}</button>
  
              </div>
            )
          },
          {
            selector: '#third-step-new',
            content: ({ goTo, inDOM }) => (
              <div>
                {this.state.task_language_explicit.task_details.tooltip3}
                <br />
                <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => this.disableFeature(e)}>{this.state.task_language_explicit.task_details.ok_got}</button>
  
              </div>
            )
          }
        ]
      }
    }

    if (this.state.isLoading === true || this.state.task_language_explicit === '') {
      return (
        <>
          <div className="loginLoader">
            <div className="lds-spinner">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <span>Please Wait…</span>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Row>
          {this.props.activeTab == 2 && tour_steps.length > 0 && <Tour
              steps={tour_steps}
              isOpen={this.state.open_tour_counter_closed_task}
              showNavigation={false}
              showButtons={false}
              onRequestClose={this.closeTourCounterClosedTask}
              onBeforeClose={target => (document.body.style.overflowY = 'auto')}
              onAfterOpen={target => (document.body.style.overflowY = 'auto')}
          />}

          <Col sm="12">
            {this.state.comments && this.state.comments.length > 0 ? (
              <div className="commentArea">
                {this.state.comments.map((comment, c) => {
                  return (
                    <article key={c} className="comment">
                      <div className="comment-meta">
                        <article className="">
                          <div className="field-name-user-picture">
                            {comment.added_by_type == "C" &&
                              this.state.user_type == 1 &&
                              this.state.profile_pic != "" &&
                              this.state.profile_pic != null && (
                                <img
                                  src={this.state.profile_pic}
                                  //style = {{width:'100%',height:'100%'}}
                                  className="image-style-thumbnail "
                                />
                              )}

                            {comment.added_by_type == "C" &&
                              this.state.user_type == 1 &&
                              (this.state.profile_pic == "" ||
                                this.state.profile_pic == null) && (
                                <img
                                  src={require("../../assets/images/default-user.png")}
                                  width="100"
                                  height="75"
                                  style={{ width: "100%", height: "100%" }}
                                  className="image-style-thumbnail "
                                />
                              )}

                            {comment.added_by_type == "A" &&
                              this.state.user_type == 2 &&
                              this.state.profile_pic != "" &&
                              this.state.profile_pic != null && (
                                <img
                                  src={this.state.profile_pic}
                                  // style = {{width:'100%',height:'100%'}}
                                  className="image-style-thumbnail "
                                />
                              )}

                            {comment.added_by_type == "A" &&
                              this.state.user_type == 2 &&
                              (this.state.profile_pic == "" ||
                                this.state.profile_pic == null) && (
                                <img
                                  src={require("../../assets/images/default-user.png")}
                                  width="100"
                                  height="75"
                                  style={{ width: "100%", height: "100%" }}
                                  className="image-style-thumbnail "
                                />
                              )}

                            {comment.added_by_type == "E" &&
                              comment.employee_profile_pic != "" &&
                              comment.employee_profile_pic != null && (
                                <img
                                  src={comment.employee_profile_pic}
                                  width="100"
                                  height="75"
                                  //style = {{width:'100%',height:'100%'}}
                                  className="image-style-thumbnail "
                                />
                              )}

                            {comment.added_by_type == "E" &&
                              (comment.employee_profile_pic == "" ||
                                comment.employee_profile_pic == null) && (
                                <img
                                  src={require("../../assets/images/default-user.png")}
                                  width="100"
                                  height="75"
                                  style={{ width: "100%", height: "100%" }}
                                  className="image-style-thumbnail "
                                />
                              )}
                          </div>
                        </article>
                      </div>
                      <div className="comment-content">
                        <p className="comment-author">
                          <span>
                            {comment.added_by_type == "C" ? (
                              <a>
                                {comment.customer_first_name}{" "}
                                {comment.customer_last_name}{" "}
                              </a>
                            ) : (
                                ""
                              )}
                            {comment.added_by_type == "A" ? (
                              <a>
                                {comment.agents_first_name}{" "}
                                {comment.agents_last_name}{" "}
                              </a>
                            ) : (
                                ""
                              )}
                            {comment.added_by_type == "E" ? (
                              <a>
                                {comment.employee_first_name}{" "}
                                {comment.employee_last_name}{" "}
                              </a>
                            ) : (
                                ""
                              )}
                          </span>{" "}
                        </p>
                        <p className="comment-time">{comment.date_added}</p>
                        <div className="text-formatted">
                          {ReactHtmlParser(htmlDecode(comment.comment))}
                        </div>

                        {comment.uploads.map((file, p) => {
                          return (
                            <div key={p} className="field field-attachment">
                              <div className="field-item">
                                <span className="file file-image">
                                  <a
                                    href={file.url}
                                    // target="_blank"
                                    style={{ color: "black" }}
                                  >
                                    {file.actual_file_name}
                                  </a>
                                </span>
                              </div>
                            </div>
                          );
                        })}

                        {comment.uploads.length > 1 && (
                          <div className="field field-attachment">
                            <div className="field-item">
                              <img
                                src={infoIcon}
                                width="28"
                                height="28"
                                type="button"
                              />
                              &nbsp;
                              <a
                                href={`${comment.multi}`}
                              // target="_blank"
                              >
                                {
                                  this.state.task_language_explicit.task_details
                                    .click_download_all_files
                                }
                              </a>
                            </div>
                          </div>
                        )}
                      </div>
                    </article>
                  );
                })}
              </div>
            ) : (
                ""
              )}

            {this.state.loadDiscussionForm === false && (
              <div>
                {this.state.showLoader === true ? <Loader /> : null}
                <Formik
                  initialValues={initialValues}
                  validationSchema={() => this.validationSchema()}
                  onSubmit={this.handleSubmit}
                >
                  {({
                    errors,
                    values,
                    touched,
                    setErrors,
                    setFieldValue,
                    setFieldTouched,
                  }) => {
                    //console.log("action seterror", errors);

                    return (
                      <Form>
                        <div>
                          <div
                            className="messageserror"
                            style={{
                              display:
                                (errors.comment && touched.comment ||
                                  errors.selected_option && touched.selected_option)

                                  ? "block"
                                  : "none",
                            }}
                          >
                            <Link to="#" className="close">
                              <img
                                src={closeIcon}
                                width="17.69px"
                                height="22px"
                                onClick={(e) => this.hideError(setErrors)}
                              />
                            </Link>
                            <div>
                              <ul className="">
                                {errors.comment && touched.comment ? (
                                  <li className="">{errors.comment}</li>
                                ) : null}
                                {errors.selected_option && touched.selected_option ? (
                                  <li className="">{errors.selected_option}</li>
                                ) : null}
                              </ul>
                            </div>
                          </div>

                          <div
                            className="messagessuccess"
                            style={{
                              display: this.state.isSuccess ? "block" : "none",
                            }}
                          >
                            <Link to="#" className="close">
                              <img
                                src={closeIconSuccess}
                                width="17.69px"
                                height="22px"
                                onClick={(e) => this.hideSuccessMsg()}
                              />
                            </Link>
                            {this.state.isSuccess ? (
                              <div>
                                {
                                  this.state.task_language_explicit.task_details
                                    .your_comment_posted
                                }
                              </div>
                            ) : null}
                          </div>
                          <Editor
                            name="comment"
                            content={this.state.comment}
                            init={{
                              menubar: false,
                              branding: false,
                              placeholder: this.state.is_report
                                ? this.state.task_language_explicit.task_details
                                  .reason_for_cancellation
                                : this.state.task_language_explicit.task_details
                                  .enter_comments,
                              plugins:
                                "link table hr visualblocks code placeholder lists textcolor",
                              toolbar:
                                "bold italic strikethrough superscript subscript | forecolor backcolor | alignleft aligncenter alignright alignjustify | numlist bullist | removeformat underline | link unlink | blockquote table  hr | visualblocks code ",
                              content_css: ["/css/editor.css"],
                              color_map: [
                                "000000",
                                "Black",
                                "808080",
                                "Gray",
                                "FFFFFF",
                                "White",
                                "FF0000",
                                "Red",
                                "FFFF00",
                                "Yellow",
                                "008000",
                                "Green",
                                "0000FF",
                                "Blue",
                              ],
                            }}
                            onEditorChange={(value) => {
                              setFieldValue("comment", value);
                              this.setState({
                                comment: value,
                                isSuccess: false,
                              });
                            }}
                          />

                          {/* <textarea
                            placeholder={
                              this.state.is_report
                                ? this.state.task_language_explicit.task_details
                                    .reason_for_cancellation
                                : this.state.task_language_explicit.task_details
                                    .enter_comments
                            }
                            name="comment"
                            value={this.state.comment}
                            onChange={(e) => {
                              setFieldValue("comment", e.target.value);
                              this.setState({
                                comment: e.target.value,
                                isSuccess: false,
                              });
                            }}
                          ></textarea> */}

                          {this.props.discussion.taskDetails.close_status == 1 && this.state.stop_feature === false && <Row className="checkmargin">

                            <div role="group" aria-labelledby="my-radio-group">

                              <div className="radioInputSec" id={`first-step-new`} >
                                <label>
                                  <Field
                                    type="radio"
                                    name="selected_option"
                                    value="1"
                                    onChange={() => {
                                      setFieldValue("selected_option", 1);
                                    }}
                                  />
                                  {this.state.task_language_explicit.task_details.new_request}

                                </label>
                                <img
                                  src={infoIconNew}
                                  width="15.44"
                                  height="18"
                                  id={`DisabledAutoHideSELOPTN_1`}
                                  type="button"
                                  className={`itooltip`}
                                />
                                <Tooltip
                                  placement="top"
                                  isOpen={this.state[`${this.state.task_id}_1`]}
                                  autohide={true}
                                  target={`DisabledAutoHideSELOPTN_1`}
                                  toggle={() => {
                                    this.setState({
                                      [`${this.state.task_id}_1`]: !this.state[`${this.state.task_id}_1`]
                                    });
                                  }}
                                >
                                  {/* By clicking on “New request” button, system will post your comment as new requirement with new time line */}
                                  {this.state.task_language_explicit.task_details.tooltip1}
                                </Tooltip>
                              </div>



                              <div className="radioInputSec" id={`second-step-new`} >
                                <label>
                                  <Field
                                    type="radio"
                                    name="selected_option"
                                    value="2"
                                    onChange={() => {
                                      setFieldValue("selected_option", 2);
                                    }}
                                  />
                                  {this.state.task_language_explicit.task_details.additional_info}

                                </label>
                                <img
                                  src={infoIconNew}
                                  width="15.44"
                                  height="18"
                                  id={`DisabledAutoHideSELOPTN_2`}
                                  type="button"
                                  className={`itooltip`}
                                />
                                <Tooltip
                                  placement="top"
                                  isOpen={this.state[`${this.state.task_id}_2`]}
                                  autohide={false}
                                  target={`DisabledAutoHideSELOPTN_2`}
                                  toggle={() => {
                                    this.setState({
                                      [`${this.state.task_id}_2`]: !this.state[`${this.state.task_id}_2`]
                                    });
                                  }}
                                >
                                  {/* By clicking on “Additional requirement, system will post your comment  as new request in continuation to previous requirement with new time line */}
                                  {this.state.task_language_explicit.task_details.tooltip2}
                                </Tooltip>
                              </div>



                              <div className="radioInputSec" id={`third-step-new`} >
                                <label>
                                  <Field
                                    type="radio"
                                    name="selected_option"
                                    value="3"
                                    onChange={() => {
                                      setFieldValue("selected_option", 3);
                                    }}
                                  />
                                  {this.state.task_language_explicit.task_details.old_request}

                                </label>
                                <img
                                  src={infoIconNew}
                                  width="15.44"
                                  height="18"
                                  id={`DisabledAutoHideSELOPTN_3`}
                                  type="button"
                                  className={`itooltip`}
                                />
                                <Tooltip
                                  placement="top"
                                  isOpen={this.state[`${this.state.task_id}_3`]}
                                  autohide={false}
                                  target={`DisabledAutoHideSELOPTN_3`}
                                  toggle={() => {
                                    this.setState({
                                      [`${this.state.task_id}_3`]: !this.state[`${this.state.task_id}_3`]
                                    });
                                  }}
                                >
                                  {/* By clicking on “continue to old request”, system will post your comment on previous request in continuation with old timeline */}
                                  {this.state.task_language_explicit.task_details.tooltip3}
                                </Tooltip>
                              </div>


                            </div>
                          </Row>}

                          <div className="field-attachment">
                            <p
                              className={
                                this.state.toggleFile === true
                                  ? "attahRight"
                                  : "attahDown"
                              }
                              onClick={() => this.handleAttachment()}
                            >
                              {this.state.task_language_explicit.task_details.attachment}
                            </p>
                          </div>

                          {this.state.toggleFile === false &&
                            this.state.files.length > 0 && (
                              <table
                                className="responsive-enabled"
                                data-striping="1"
                              >
                                <thead>
                                  <tr>
                                    <th>
                                      {
                                        this.state.task_language_explicit.task_details
                                          .file_information
                                      }
                                    </th>
                                    <th>
                                      {
                                        this.state.task_language_explicit.task_details
                                          .operations
                                      }
                                    </th>
                                  </tr>
                                </thead>

                                {this.state.files &&
                                  this.state.files.map((file, index) => {
                                    return (
                                      <tbody key={index}>
                                        <tr className="draggable">
                                          <td>
                                            <a
                                              href="#"
                                              className="tabledrag-handle"
                                              title={
                                                this.state.task_language_explicit
                                                  .task_details.drag_to_re_order
                                              }
                                            />
                                            <div className=" form-managed-file">
                                              <span className="file file-image">
                                                <Link
                                                  to="#"
                                                  // type="image/jpeg;"
                                                  className="menu-item-link"
                                                >
                                                  {file.name}
                                                </Link>
                                              </span>
                                            </div>
                                          </td>
                                          <td>
                                            <input
                                              name={index}
                                              value={
                                                this.state.task_language_explicit
                                                  .task_details.remove
                                              }
                                              className="button form-submit"
                                              readOnly={true}
                                              onClick={(e) =>
                                                this.deleteFile(e)
                                              }
                                            />
                                          </td>
                                        </tr>
                                      </tbody>
                                    );
                                  })}
                              </table>
                            )}

                          <div
                            className="messageserror"
                            style={{
                              display:
                                (this.state.rejectedFile &&
                                  this.state.rejectedFile.length) > 0
                                  ? "block"
                                  : "none",
                            }}
                          >
                            <Link to="#" className="close">
                              <img
                                src={closeIcon}
                                width="17.69px"
                                height="22px"
                                onClick={(e) => this.hideFileError()}
                              />
                            </Link>
                            {this.state.rejectedFile &&
                              this.state.rejectedFile.map((file, index) => {
                                return (
                                  <div key={index}>
                                    <ul className="">
                                      <li className="">
                                        {this.state.task_language_explicit.display_error.form_error.error_text_2.replace(
                                          "[file_name]",
                                          file.name
                                        )}
                                        <br />
                                        {
                                          this.state.task_language_explicit.display_error
                                            .form_error.following_extensions
                                        }
                                      </li>
                                    </ul>
                                  </div>
                                );
                              })}
                          </div>

                          <div className="row discussAttachSec">
                            <div className="col-md-12">
                              {this.state.toggleFile === false && (
                                <Dropzone
                                  onDrop={(acceptedFiles) =>
                                    this.setDropZoneFiles(
                                      acceptedFiles,
                                      setErrors,
                                      setFieldValue
                                    )
                                  }
                                >
                                  {({ getRootProps, getInputProps }) => (
                                    <section>
                                      <div
                                        {...getRootProps()}
                                        className="customFile"
                                      >
                                        <input
                                          {...getInputProps()}
                                          ref={this.inputRef}
                                          style={{ display: "block" }}
                                        />
                                        {/* <p>Upload file</p> */}
                                      </div>
                                    </section>
                                  )}
                                </Dropzone>
                              )}
                            </div>
                            <div className="col-md-12 my-auto">
                              <button className="btn btn-default">
                                {(this.props.discussion.taskDetails.close_status == 1 && this.state.stop_feature === false) ? this.state.task_language_explicit.task_details.submit : this.state.task_language_explicit.task_details.post}
                              </button>
                              {this.props.is_report === true ? (
                                <button
                                  onClick={(e) => this.closePopup()}
                                  className={`btn btn-default btn-cancel`}
                                  type="button"
                                >
                                  {this.state.task_language_explicit.task_details.cancel}
                                </button>
                              ) : null}
                            </div>
                          </div>
                        </div>
                      </Form>
                    );
                  }}
                </Formik>
              </div>
            )}
          </Col>

          {this.state.closed_discussion_err === true && (
            <Modal
              show={this.state.closed_discussion_err}
              onHide={this.handleClosedDiss}
              backdrop="static"
              className="modalRating"
            >
              <Modal.Header>
                <img
                  src={closeIconSelect}
                  width="15.44"
                  height="18"
                  id="DisabledAutoHideExample"
                  type="button"
                  onClick={(e) => this.handleClosedDiss(e)}
                  className="close"
                />
              </Modal.Header>
              <Modal.Body>
                <Modal.Title>
                  <div className="ratinfo">

                  </div>
                </Modal.Title>
                <p style={{ textAlign: 'center' }} >{this.state.closed_discussion_err_txt}</p>
              </Modal.Body>
            </Modal>
          )}

        </Row>

      );
    }
  }
}

const mapStateToProps = (state) => {
  //console.log("disucsssion page redux ====>", state)
  return {
    profile_pic: state.user.url,
    discussion: state.discussion.discussion,
    discussionErr: state.discussion.errors,
    dynamic_lang: state.auth.dynamic_lang,
    tour_counter_closed_task: state.auth.tour_counter_closed_task
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    submitUserDiscussion: (data, onSuccess) =>
      dispatch(submitDiscussion(data, onSuccess)),
    updateTourToken: (data) => dispatch(updateToken(data))  
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Discussion);
