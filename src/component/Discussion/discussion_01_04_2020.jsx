import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "../../shared/axios";
import dateFormat from "dateformat";
import API from "../../shared/axios";
import { supportedFileType } from "../../shared/helper";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import Dropzone from "react-dropzone";
import { Row, Col } from "reactstrap";
import infoIcon from "../../assets/images/download-new.svg";

import { htmlDecode,BasicUserData } from "../../shared/helper";
import ReactHtmlParser from "react-html-parser";

// SATYAJIT
import { connect } from "react-redux";
import { submitDiscussion } from "../../store/actions/discussion";
import closeIconSuccess from "../../assets/images/times-solid-green.svg";
import closeIcon from "../../assets/images/times-solid-red.svg";
import { Editor } from "@tinymce/tinymce-react";
import Loader from "../Loader/loader";
import { stat } from "fs";
var path = require("path");

const discussion_download_all = `${process.env.REACT_APP_API_URL}/tasks/download_all_discussions/`; // SVJT

//validation
const initialValues = {
  comment: ""
};

const validationSchema = Yup.object().shape({
  comment: Yup.string()
    .required("Enter Comments * field is required.")
    .test(values => {
      console.log('values ===>',values)
      return values === "<p><br></p>" ? false : true;
    })
});

class Discussion extends Component {
  constructor(props) {
    super(props);
    this.state = {
      discussionData: [],
      discussionErr: null,
      files: [],
      comments: [],
      discussionFile: [],
      commentText: "",
      isSuccess: false,
      comment: "",
      loadDiscussionForm: false,
      showLoader: false,
      toggleFile: false,
      profile_pic : '',
      user_type : '',
      owner: 0,
      task_id: 0,
      is_report: this.props.is_report // SATYAJIT [17.12.2019]
    }
    this.inputRef = React.createRef();
  }
  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue, errors) => {
    var rejectedFiles = [];
    var uploadFile = [];

    for (var index = 0; index < acceptedFiles.length; index++) {
      var error = 0;
      var filename = acceptedFiles[index].name.toLowerCase();      
      var extension_list = supportedFileType();
      var ext_with_dot = path.extname(filename)
      var file_extension = ext_with_dot.split('.').join("");

      if (extension_list.indexOf(file_extension) === -1) {
        error = error + 1;
        rejectedFiles.push(acceptedFiles[index]);
      }
      if (acceptedFiles[index].size > 50000000) {
        error = error + 1;
        rejectedFiles.push(acceptedFiles[index]);
      }

      if (error === 0) {
        uploadFile.push(acceptedFiles[index]);
        setErrors({ file_name: false });
      }
    }

    // //setErrors({ file_name: false });
    // errors.file_name = [];
    //setFieldValue(this.state.files);

    var prevFiles = this.state.files;
    var newFiles = [];
    if (prevFiles.length > 0) {
      for (let index = 0; index < uploadFile.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (uploadFile[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        if (remove === 0) {
          prevFiles.push(uploadFile[index]);
        }
      }

      prevFiles.map(file => {
        file.checked = false;
        newFiles.push(file);
      });
    } else {
      uploadFile.map(file => {
        file.checked = false;
        newFiles.push(file);
      });
    }

    this.setState({
      files: newFiles
    });

    setFieldValue("file_name", newFiles);

    this.setState({
      rejectedFile: rejectedFiles
    });

    this.inputRef.current.value = null; // clear input value - SATYAJIT
  };

  deleteFile = (e) => {   
    e.preventDefault();
    var uploadFile = [];
    this.state.files.map((file, index) => {
      if (index.toString() === e.target.name.toString()) {
      } else {
        uploadFile.push(file);
      }
    });    
    this.setState({ files: uploadFile });
  };

  handleSubmit = (values, actions) => {
    this.setState({ showLoader: true, isSuccess: false });

    var request_body = {
      files     : this.state.files,
      task_id   : this.state.task_id,
      comment   : values.comment,
      owner     : this.state.owner,
      is_report : this.state.is_report
    }

    this.props.submitUserDiscussion( request_body, () => {

          if(this.props.discussionErr){
            actions.setErrors(this.props.discussionErr.data.errors)
            this.setState({ isSuccess : false, showLoader : false})
          }else{
            this.setState({
              discussionData    : [],
              files             : [],
              comments          : [],
              discussionFile    : [],              
              comment           : "",
              loadDiscussionForm: true,
              isSuccess         : true,
              showLoader        : false
            });

            // if report popup is opened then close it else load discussion panel -- SATYAJIT [17.12.2019]
            if(this.props.is_report){
              this.props.closeDiscussPopup()
            }else{
              this.fetchDiscussion();
            }            
          }
        
    })
  }

  fetchDiscussion() {
    this.setState({
        discussionData    : this.props.discussion,
        task_id           : this.props.discussion.taskDetails.task_id,
        comments          : !this.state.is_report ? this.props.discussion.comments : [], // stop loading the discussion-SATYAJIT [17.12.2019]
        loadDiscussionForm: this.props.discussion.taskDetails.duplicate_task === 1 ? true : false
    })   
  }

  componentDidUpdate = () => {
    if(this.props.discussion.comments.length > 0 && this.props.is_report === false && this.state.comments != this.props.discussion.comments){
      this.setState({ comments: this.props.discussion.comments});
    }
  }

  componentDidMount = () => {
    let userTypeData = BasicUserData();
    this.setState({user_type:userTypeData.role});
    
    if(this.props.profile_pic !== this.state.profile_pic) {
      this.setState({
          profile_pic: this.props.profile_pic
      })
    }
    this.setState({ owner: this.props.discussion.taskDetails.owner});    
    this.fetchDiscussion();
  }

  hideFileError = () => {
    this.setState({ rejectedFile: [] });
  };

  hideError = setErrors => {
    setErrors({});
  };

  // handleEditorChange(e) {
  //   console.log(e.target.getContent());
  // }

  hideSuccessMsg = () => {
    this.setState({ isSuccess: false });
  };

  handleAttachment = () => {
    this.setState({ toggleFile: !this.state.toggleFile });
  };

  changeDateFormat = ( input_date ) => {
    if (input_date){
      //var dateFormat = require('dateformat');
      //dateFormat(input_date.replace("-", "/"), "d mmm yyyy")
      var date_time = input_date.split(' ');
      var date = date_time[0].split('-');
      var date_format = new Date(date[0],(date[1]-1),date[2]);
      return dateFormat(date_format, "dddd, mmmm d, yyyy - h:MM");
    }     
  }

  render() {
    return (
      <Row>
        <Col sm="12">
          {this.state.comments && this.state.comments.length > 0 ? (
            <div className="commentArea">
              {this.state.comments.map((comment, c) => {
                return (
                  <article key={c} className="comment">
                    <div className="comment-meta">
                      <article className="">
                        <div className="field-name-user-picture">
                          { comment.added_by_type == 'C' && this.state.user_type == 1 &&
                          
                            <img
                              src={this.state.profile_pic}
                              //style = {{width:'100%',height:'100%'}}
                              className="image-style-thumbnail "
                            />
                          }

                          { comment.added_by_type == 'C' &&
                          
                            <img
                              src={require("../../assets/images/default-user.png")}
                              width="100"
                              height="75"
                              style = {{width:'100%',height:'100%'}}
                              className="image-style-thumbnail "
                            />
                          }

                          { comment.added_by_type == 'A' && this.state.user_type == 2 &&
                          
                            <img
                              src={this.state.profile_pic}
                             // style = {{width:'100%',height:'100%'}}
                              className="image-style-thumbnail "
                            />
                          }

                          { comment.added_by_type == 'A' && 
                          
                            <img
                              src={require("../../assets/images/default-user.png")}
                              width="100"
                              height="75"
                              style = {{width:'100%',height:'100%'}}
                              className="image-style-thumbnail "
                            />
                          }

                          { comment.added_by_type == 'E' && comment.employee_profile_pic != '' && comment.employee_profile_pic != null && 
                          
                            <img
                              src={comment.employee_profile_pic}
                              width="100"
                              height="75"
                              //style = {{width:'100%',height:'100%'}}
                              className="image-style-thumbnail "
                            />
                          }

                          { comment.added_by_type == 'E' &&
                          
                            <img
                              src={require("../../assets/images/default-user.png")}
                              width="100"
                              height="75"
                              style = {{width:'100%',height:'100%'}}
                              className="image-style-thumbnail "
                            />
                          }
                          
                        </div>
                      </article>
                    </div>
                    <div className="comment-content">
                      <p className="comment-author">
                        <span>
                        { comment.added_by_type == 'C' ?
                          <a>
                            {comment.customer_first_name} {comment.customer_last_name}{" "}
                          </a>
                          :
                          ''}
                        { comment.added_by_type == 'A' ?
                          <a>
                            {comment.agents_first_name} {comment.agents_last_name}{" "}
                          </a>
                          :
                          ''}
                        { comment.added_by_type == 'E' ?
                          <a>
                            {comment.employee_first_name} {comment.employee_last_name}{" "}
                          </a>
                          :
                          ''}    
                        </span>{" "}
                      </p>
                      <p className="comment-time">
                        {comment.date_added}
                      </p>
                      <div className="text-formatted">
                        {ReactHtmlParser(htmlDecode(comment.comment))}
                      </div>

                      {comment.uploads.map((file, p) => {
                        return (
                          <div key={p} className="field field-attachment">
                            <div className="field-item">
                              <span className="file file-image">
                                <a
                                  href={file.url}
                                  // target="_blank"
                                  style={{color:'black'}}
                                >
                                  {file.actual_file_name}
                                </a>
                              </span>
                            </div>
                          </div>
                        );
                      })}

                      {comment.uploads.length > 1 && <div className="field field-attachment">
                            <div className="field-item">
                                <img
                                    src={infoIcon}
                                    width="28"
                                    height="28"
                                    type="button"
                                  />
                                  &nbsp;
                                <a
                                  href={`${comment.multi}`}
                                  // target="_blank"
                                >
                                  Click to download all files
                                </a>
                            </div>
                          </div>}

                    </div>
                  </article>
                );
              })}
            </div>
          ) : (
            ""
          )}

          {this.state.loadDiscussionForm === false && (
            <div>
              {this.state.showLoader === true ? <Loader /> : null}
              <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={this.handleSubmit}
              >
                {({
                  errors,
                  values,
                  touched,
                  setErrors,
                  setFieldValue,
                  setFieldTouched
                }) => {

                  //console.log("action seterror", errors);
                  
                  return (
                    <Form>
                      <div>
                        <div
                          className="messageserror"
                          style={{
                            display:
                              errors.comment && touched.comment
                                ? "block"
                                : "none"
                          }}
                        >
                          <Link to="#" className="close">
                            <img
                              src={closeIcon}
                              width="17.69px"
                              height="22px"
                              onClick={e => this.hideError(setErrors)}
                            />
                          </Link>
                          <div>
                            <ul className="">
                              {errors.comment && touched.comment ? (
                                <li className="">{errors.comment}</li>
                              ) : null}
                            </ul>
                          </div>
                        </div>

                        <div
                          className="messagessuccess"
                          style={{
                            display: this.state.isSuccess ? "block" : "none"
                          }}
                        >
                          <Link to="#" className="close">
                            <img
                              src={closeIconSuccess}
                              width="17.69px"
                              height="22px"
                              onClick={e => this.hideSuccessMsg()}
                            />
                          </Link>
                          {this.state.isSuccess ? (
                            <div>Your comment has been posted.</div>
                          ) : null}
                        </div>
                        <Editor
                          name="comment"
                          content={this.state.comment}
                          init={{
                            menubar: false,
                            branding: false,
                            placeholder: this.state.is_report ? "Please enter your reason for cancellation" : "Enter Comments *",
                            plugins:
                              "link table hr visualblocks code placeholder lists",
                            toolbar:
                              "bold italic strikethrough superscript subscript | alignleft aligncenter alignright alignjustify | numlist bullist | removeformat underline | link unlink | blockquote table  hr | visualblocks code ",
                            content_css: [ '/css/editor.css']
                          }}
                          onEditorChange={value => {
                            setFieldValue("comment", value);
                            this.setState({ comment: value, isSuccess: false });
                          }}
                        />

                        <div className="field-attachment">
                          <p
                            className={
                              this.state.toggleFile === true
                                ? "attahRight"
                                : "attahDown"
                            }
                            onClick={() => this.handleAttachment()}
                          >
                            Attachment
                          </p>
                        </div>

                        {this.state.toggleFile === false &&
                          this.state.files.length > 0 && (
                            <table
                              className="responsive-enabled"
                              data-striping="1"
                            >
                              <thead>
                                <tr>
                                  <th>File information</th>
                                  <th>Operations</th>
                                </tr>
                              </thead>

                              {this.state.files &&
                                this.state.files.map((file, index) => {
                                  return (
                                    <tbody key={index}>
                                      <tr className="draggable">
                                        <td>
                                          <a
                                            href="#"
                                            className="tabledrag-handle"
                                            title="Drag to re-order"
                                          />
                                          <div className=" form-managed-file">
                                            <span className="file file-image">
                                              <Link
                                                to="#"
                                                // type="image/jpeg;"
                                                className="menu-item-link"
                                              >
                                                {file.name}
                                              </Link>
                                            </span>
                                          </div>
                                        </td>
                                        <td>
                                          <input
                                            name={index}
                                            value="Remove"
                                            className="button form-submit"
                                            readOnly={true}
                                            onClick={e => this.deleteFile(e)}
                                          />
                                        </td>
                                      </tr>
                                    </tbody>
                                  );
                                })}
                            </table>
                          )}
                        <br />
                        <div
                          className="messageserror"
                          style={{
                            display:
                              (this.state.rejectedFile &&
                                this.state.rejectedFile.length) > 0
                                ? "block"
                                : "none"
                          }}
                        >
                          <Link to="#" className="close">
                            <img
                              src={closeIcon}
                              width="17.69px"
                              height="22px"
                              onClick={e => this.hideFileError()}
                            />
                          </Link>
                          {this.state.rejectedFile &&
                            this.state.rejectedFile.map((file, index) => {
                              return (
                                <div key={index}>
                                  <ul className="">
                                    <li className="">
                                      {"The specified file" +
                                        " " +
                                        file.name +
                                        " " +
                                        "could not be uploaded."}
                                      <br />
                                      {" " +
                                        "Only files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff."}
                                    </li>
                                  </ul>
                                </div>
                              );
                            })}
                        </div>

                        <div className="row discussAttachSec">
                          <div className="col-md-4">
                            {this.state.toggleFile === false && (
                              <Dropzone
                                onDrop={acceptedFiles =>
                                  this.setDropZoneFiles(
                                    acceptedFiles,
                                    setErrors,
                                    setFieldValue
                                  )
                                }
                              >
                                {({ getRootProps, getInputProps }) => (
                                  <section>
                                    <div
                                      {...getRootProps()}
                                      className="customFile"
                                    >
                                      <input
                                        {...getInputProps()}
                                        ref={this.inputRef}
                                        style={{ display: "block" }}
                                      />
                                      {/* <p>Upload file</p> */}
                                    </div>
                                  </section>
                                )}
                              </Dropzone>
                            )}
                          </div>
                          <div className="col-md-8 my-auto">
                            <button className="btn btn-default">Post</button>
                          </div>
                        </div>

                      </div>
                    </Form>
                  );
                }}
              </Formik>
            </div>
          )}
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = state => {
  //console.log("disucsssion page redux ====>", state)
  return {
      profile_pic: state.user.url,
      discussion : state.discussion.discussion,
      discussionErr : state.discussion.errors
  }
}

const mapDispatchToProps = dispatch => {
  return {
      submitUserDiscussion: (data, onSuccess) => dispatch(submitDiscussion(data, onSuccess))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Discussion);
