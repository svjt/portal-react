import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Row, Col } from "reactstrap";
import { Tooltip } from "reactstrap";
import infoIcon from "../../assets/images/download-new.svg";

import { BasicUserData } from "../../shared/helper";

// SATYAJIT
import { connect } from "react-redux";
// SATYAJIT
const discussion_download_all = `${process.env.REACT_APP_API_URL}/tasks/download_all_discussions/`; // SVJT

class DiscussionFile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tooltipOpen: false,
      discussionFiles: [],
      isLoading: true,
      task_language_explicit:''
    };
  }

  toggleTooltip = () => {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    });
  };

  changeDateFormat = (input_date) => {
    var dateFormat = require("dateformat");
    return dateFormat(input_date, "d mmm yyyy");
  };

  componentDidMount = () => {

    let lang_data = require(`../../assets/lang/${this.props.discussion.taskDetails.language}.json`);
    this.setState({task_language_explicit:lang_data});

    if (this.props.discussion.discussionFiles !== this.state.discussionFiles) {
      this.setState({ discussionFiles: this.props.discussion.discussionFiles });
    }
    let userData = BasicUserData();
    if (userData) {
      this.setState({
        isLoading: false,
      });
    }
  };

  componentDidUpdate = (prevProps) => {
    if (prevProps.discussion.discussionFiles) {

      if (
        this.props.discussion.discussionFiles !==
        prevProps.discussion.discussionFiles
      ) {
        this.setState({
          discussionFiles: this.props.discussion.discussionFiles,
        });
      }
    }
  };

  render() {

    if (this.state.task_language_explicit === '') {
      return (
        <>
          <div className="loginLoader">
            <div className="lds-spinner">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <span>Please Wait…</span>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <Row>
          <Col sm="12">
            <div className="responsive-table">
              <div className="views-infinite-scroll-content-wrapper clearfix">
                <table className="requestTable">
                  <thead>
                    <tr>
                      <th
                        className="views-field-counter"
                        style={{ textAlign: "left" }}
                      >
                        {this.state.task_language_explicit.task_details.sl_no}
                      </th>
                      <th className="views-field-field-attachment">
                        {this.state.task_language_explicit.task_details.attachment}
                      </th>
                      <th className="views-field-download">
                        {this.state.task_language_explicit.task_details.download_all}
                      </th>
                      <th
                        className="views-field-uid"
                        style={{ textAlign: "left" }}
                      >
                        {this.state.task_language_explicit.task_details.submitted}
                      </th>
                      <th
                        className="views-field-created"
                        style={{ textAlign: "left" }}
                      >
                        {this.state.task_language_explicit.task_details.post_date}
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.discussionFiles &&
                      this.state.discussionFiles.length > 0 &&
                      this.state.discussionFiles.map((innerLoop, k) => {
                        return (
                          <tr key={k}>
                            <td
                              headers="view-counter-table-column"
                              className="views-field-counter"
                            >
                              {k + 1}
                            </td>
                            <td
                              headers="view-field-attachment-table-column"
                              className="views-field-field-attachment"
                            >
                              {
                                innerLoop.files.map((download, index) => {
                                  return (
                                    <span
                                      key={index}
                                      className="file file-image"
                                    >
                                      <a href={download.url}>
                                        {download.actual_file_name}
                                      </a>
                                      <br />
                                    </span>
                                  );
                                })
                                /*.reduce((prev, curr) => [prev, ', ', curr])}*/
                              }
                            </td>
                            <td
                              headers="views-field-download-table-column"
                              className="views-field-download text-center"
                            >
                              {innerLoop.files.length > 1 && (
                                <>
                                  <a href={`${innerLoop.multi}`}>
                                    <img
                                      src={infoIcon}
                                      width="28"
                                      height="28"
                                      id="DisabledAutoHideExampleDiss"
                                      type="button"
                                    />
                                    <Tooltip
                                      placement="right"
                                      isOpen={this.state.tooltipOpen}
                                      autohide={false}
                                      target="DisabledAutoHideExampleDiss"
                                      toggle={this.toggleTooltip}
                                    >
                                      {
                                        this.state.task_language_explicit.task_details
                                          .click_download_all_files
                                      }
                                    </Tooltip>
                                  </a>
                                </>
                              )}
                            </td>
                            {innerLoop.files[0].added_by_type == "C" && (
                              <td
                                headers="view-uid-table-column"
                                className="views-field-uid"
                              >
                                {innerLoop.files[0].customer_first_name}{" "}
                                {innerLoop.files[0].customer_last_name}
                              </td>
                            )}

                            {innerLoop.files[0].added_by_type == "A" && (
                              <td
                                headers="view-uid-table-column"
                                className="views-field-uid"
                              >
                                {innerLoop.files[0].agents_first_name}{" "}
                                {innerLoop.files[0].agents_last_name}
                              </td>
                            )}

                            {innerLoop.files[0].added_by_type == "E" && (
                              <td
                                headers="view-uid-table-column"
                                className="views-field-uid"
                              >
                                {innerLoop.files[0].employee_first_name}{" "}
                                {innerLoop.files[0].employee_last_name}
                              </td>
                            )}
                            <td
                              headers="view-created-table-column"
                              className="views-field-created"
                            >
                              {innerLoop.date_added}
                            </td>
                          </tr>
                        );
                      })}

                    {this.state.discussionFiles &&
                      this.state.discussionFiles.length === 0 && (
                        <tr>
                          <td
                            headers="view-counter-table-column"
                            className="views-field-counter"
                            colSpan="4"
                          >
                            {this.state.task_language_explicit.task_details.no_files}
                          </td>
                        </tr>
                      )}
                  </tbody>
                </table>
              </div>
            </div>
          </Col>
        </Row>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    discussion: state.discussion.discussion,
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DiscussionFile);
