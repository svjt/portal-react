import React, { Component } from "react";
import dateFormat from "dateformat";
//import API from "../../shared/axios";

import axios from "../../shared/axios";
// import Select from "react-select";
import { Link } from "react-router-dom";
// import Layout from "../Layout/layout";
import { htmlDecode, BasicUserData, supportedFileType } from "../../shared/helper";

import { refreshFeatureToken, updateToken, authLogoutReload } from "../../store/actions/auth";
import ReactHtmlParser from "react-html-parser";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  CardTitle,
  CardText,
  Alert,
  Row,
  Col,
} from "reactstrap";
import Tour from 'reactour'
import { Modal, ButtonToolbar } from "react-bootstrap";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel,
} from 'react-accessible-accordion';
import 'react-accessible-accordion/dist/fancy-example.css';
import swal from "sweetalert";
import classnames from "classnames";
import Discussion from "./../Discussion/discussion";
import DiscussionFile from "./../Discussion/discussionFile";
import { showErrorMessage } from "../../shared/handle_error";
import { Tooltip } from "reactstrap";
import infoIcon from "../../assets/images/download-new.svg";
import Loader from "../Loader/loader";
import { Formik, Field, Form } from "formik";
import { Editor } from "@tinymce/tinymce-react";
import Dropzone from "react-dropzone";

import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

//SATYAJIT
import { connect } from "react-redux";
import { fetchUserDiscussion } from "../../store/actions/discussion";

import { getTStatus } from "../../store/actions/user";
import closeIconSuccess from "../../assets/images/times-solid-green.svg";
import closeIcon from "../../assets/images/times-solid-red.svg";
import acceptIcon from "../../assets/images/approve.png";
import rejectIcon from "../../assets/images/reject.png";

//DG
// import Accordion from 'react-bootstrap/Accordion';

var path = require("path");

//SATYAJIT

const task_download_all = `${process.env.REACT_APP_API_URL}tasks/download_all/`; // SVJT
const ship_download = `${process.env.REACT_APP_API_URL}tasks/download_ship_doc/`;
const pourl = `${process.env.REACT_APP_API_URL}tasks/download_po_doc/`;

const initialValues = {
  comment: ""
};

class TaskDetails extends Component {
  constructor(props) {
    super(props);
    this.toggle = this.toggle.bind(this);
    this.toggleTooltip = this.toggleTooltip.bind(this);
    this.state = {
      taskDetails: [],
      taskSAPDetails: [],
      files: [],
      show: false,
      taskDetailsFiles: [],
      discussionFiles: [],
      invalid_access: false,
      status_active: [],
      task_id: this.props.match.params.id,
      activeTab: this.getActiveTabs(),
      showBehalf: false,
      submittedBy: false,
      order_status: [],
      other_status: [],
      complaint_status: [],
      detailsErr: null,
      showLoader: false,
      showReportIssue: false,
      tooltipOpen: false,
      is_report: false,
      isLoading: true,
      showSendMailPopup: false,
      showSendMailEscalationPopup: false,
      showRDDPopup: false,
      RDDShow: false,
      edit_temp_id: 0,
      edit_rdd: '',
      task_language_explicit: '',
      discussion_attachments: [],
      task_attachments: [],
      email_list: [],
      attachment_error: '',
      escalation_error: '',
      rdd_error: '',
      email_error: '',
      toggleFile: false,
      entered_email: '',
      isSuccess: false,
      dynamic_success_message: '',
      exclude_task_ref: true,
      send_mail_subject: '',
      send_mail_notes: '',
      new_rdd: '',
      open_tour: true,
      open_tour_new: true,
      dynamic_html: [],
      not_approved_tasks: [],
      rdd_pending: false,
      showRejectComment: false,
      reject_id: '',
      reject_type: '',
      preship_arr: [],
    };
    this.inputRef = React.createRef();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (prevState.task_id !== nextProps.match.params.id) {
      return {
        task_id: nextProps.match.params.id,
        activeTab: nextProps.location.state.fromDashboard === true ? "2" : "1",
      };
    }

    // Return null to indicate no change to state.
    return null;
  }

  toggle(tab) {

    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  toggleTooltip = () => {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    });
  };

  handleSendMailClose = () => {
    this.setState({
      attachmentList: [],
      rejectedFile: [],
      files: [],
      email_list: [],
      showSendMailPopup: false,
      showMobileAlert: false,
      displayMobileText: "",
    });
  };

  handleSendMailEscalationClose = () => {
    this.setState({
      showSendMailEscalationPopup: false,
      files: [],
      escalation_error: ''
    });
  };

  handleRDDPopupClose = () => {
    this.setState({
      showRDDPopup: false,
      RDDShow: false,
      files: [],
      escalation_error: ''
    });
  };




  getActiveTabs = () => {
    if (this.props.location.state) {
      if (this.props.location.state.fromDashboard === true) {
        return "2";
      } else if (this.props.location.state.fromDashboardpayment === true) {
        return "4";
      } else if (this.props.location.state.fromDashboardtrack === true) {
        return "5";
      } else {
        return "1";
      }
    } else {
      return "1";
    }
  }

  getAttchmentsSendMail = () => {
    this.setState({ attachmentList: [], email_list: [] });
    axios
      .get(`tasks/get-task-attachments/${this.state.task_id}`)
      .then((res) => {
        this.setState({ attachmentList: res.data.data, showSendMailPopup: true });
      })
      .catch((err) => { });
  };

  approvePreShip = (invoice_number, type, comment = '') => {
    let popupText = this.state.task_language_explicit.task_details.approve_query;
    if (invoice_number != '' && invoice_number != undefined) {

      if (type == 'approve') {
        swal({
          closeOnClickOutside: false,
          title: this.state.task_language_explicit.task_details.invoice_approval,
          text: popupText,
          icon: "warning",
          buttons: true,
          dangerMode: true,
          customClass: 'customAlert',
        }).then((willDelete) => {
          if (willDelete) {
            this.setState({ showLoader: true })
            axios
              .post(`tasks/approve_invoice`, {
                invoice_number: invoice_number,
                type: 'approve',
                task_id: this.state.task_id
              })
              .then((res) => {
                this.setState({ dynamic_success_message: this.state.task_language_explicit.task_details.invoice_success, isSuccess: true, preship_arr: res.data.data, showLoader: false });
              })
              .catch((err) => {
                if (err && err.data && err.data.status && err.data.status == 5) {
                  this.setState({ attachment_error: err.data.errors, showLoader: false });
                }
              });
          }
        });
      } else {
        this.setState({ showLoader: true })
        axios
          .post(`tasks/approve_invoice`, {
            invoice_number: invoice_number,
            type: 'reject',
            task_id: this.state.task_id,
            reject_comment: comment
          })
          .then((res) => {
            this.handleCloseReject();
            this.setState({ dynamic_success_message: this.state.task_language_explicit.task_details.invoice_reject, isSuccess: true, preship_arr: res.data.data, showLoader: false });
          })
          .catch((err) => {
            if (err && err.data && err.data.status && err.data.status == 5) {
              this.setState({ attachment_error: err.data.errors, showLoader: false });
            }
          });
      }

    }
  }

  rejectPrforma = (reject_id, reject_type) => {

    this.setState({ showRejectComment: true, reject_id: reject_id, reject_type: reject_type })
  }

  approvePreShipCOA = (coa, type, comment = '') => {
    let popupText = this.state.task_language_explicit.task_details.approve_query;
    if (coa != '' && coa != undefined) {
      if (type == 'approve') {
        swal({
          closeOnClickOutside: false,
          title: this.state.task_language_explicit.task_details.coa_approval,
          text: popupText,
          icon: "warning",
          buttons: true,
          dangerMode: true,
          customClass: 'customAlert',
        }).then((willDelete) => {
          if (willDelete) {
            this.setState({ showLoader: true })
            axios
              .post(`tasks/approve_coa`, {
                coa: coa,
                type: 'approve',
                task_id: this.state.task_id
              })
              .then((res) => {
                this.setState({ dynamic_success_message: this.state.task_language_explicit.task_details.coa_success, isSuccess: true, preship_arr: res.data.data, showLoader: false });
              })
              .catch((err) => {
                if (err && err.data && err.data.status && err.data.status == 5) {
                  this.setState({ attachment_error: err.data.errors, showLoader: false });
                }
              });
          }
        });
      } else {
        this.setState({ showLoader: true })
        axios
          .post(`tasks/approve_coa`, {
            coa: coa,
            type: 'reject',
            task_id: this.state.task_id,
            reject_comment: comment
          })
          .then((res) => {
            this.handleCloseReject();
            this.setState({ dynamic_success_message: this.state.task_language_explicit.task_details.coa_reject, isSuccess: true, preship_arr: res.data.data, showLoader: false });
          })
          .catch((err) => {
            if (err && err.data && err.data.status && err.data.status == 5) {
              this.setState({ attachment_error: err.data.errors, showLoader: false });
            }
          });
      }
    }
  }

  showEscalationPopup = () => {
    this.setState({ escalation_error: '', showSendMailEscalationPopup: true });
  };

  handleDiscussionAttchments = (event, ret_index) => {
    let prev_state = this.state.attachmentList[0].task_discussion_files;

    for (let index = 0; index < prev_state.length; index++) {
      const element = prev_state[index];

      if (index === ret_index) {
        prev_state[index].chk = event.target.checked
      }

    }

    this.setState({ attachment_error: '' });

  }

  handleTaskAttchments = (event, ret_index) => {
    let prev_state = this.state.attachmentList[0].task_files;

    for (let index = 0; index < prev_state.length; index++) {
      const element = prev_state[index];

      if (index === ret_index) {
        prev_state[index].chk = event.target.checked
      }
    }

    this.setState({ attachment_error: '' });

  }

  getTranslatedTextShipping = (text, language) => {
    if (language == 'en') {
      return text;
    } else {
      let ret = '';
      let ship_arr = [{
        'Delivered': {
          'zh': '发表',
          'ja': '配信済み',
          'es': 'Entregado',
          'pt': 'Entregue'
        },
        'Shipment Arrived': {
          'zh': '货已到',
          'ja': '発送が到着しました',
          'es': 'El Envío Llegó',
          'pt': 'Remessa Chegou'
        },
        'Shipment Departed': {
          'zh': '发货已出发',
          'ja': '出荷出発',
          'es': 'Envío Salido',
          'pt': 'Remessa Partida'
        },
        'AWB Doc': {
          'zh': '空运单文件',
          'ja': 'AWB Doc',
          'es': 'AWB Doc',
          'pt': 'AWB Doc'
        },
        'Shipping Progress': {
          'zh': '发货进度',
          'ja': '出荷の進捗状況',
          'es': 'Progreso de Envío',
          'pt': 'Progresso de Envio'
        },
        'Shipment Picked': {
          'zh': '提货',
          'ja': '選択された出荷',
          'es': 'Envío Elegido',
          'pt': 'Remessa Escolhida'
        },
        'Packing List': {
          'zh': '包装清单',
          'ja': '包装内容明細書',
          'es': 'Lista de Empaque',
          'pt': 'Lista de Embalagem'
        },
        'Invoice Doc': {
          'zh': '发票单据',
          'ja': '請求書ドキュメント',
          'es': 'Documento de Factura',
          'pt': 'Documento de Fatura'
        },
        'Approval Document': {
          'zh': '批准文件',
          'ja': '承認文書',
          'es': 'Documento de Aprobación',
          'pt': 'Documento de Aprovação'
        },
        'Approval Haz Clearance': {
          'zh': '批准危害清除',
          'ja': '承認ハズクリアランス',
          'es': 'Aprobación Despeje de Peligros',
          'pt': 'Liberação de Risco de Aprovação'
        },
        'FDA Approval': {
          'zh': 'FDA 批准',
          'ja': 'FDAの承認',
          'es': 'Aprobación de la FDA',
          'pt': 'Aprovação FDA'
        },
        'Customs Clearance': {
          'zh': '报关',
          'ja': '通関',
          'es': 'Despacho de Aduana',
          'pt': 'Desembaraço Alfandegário'
        },
        'Completed': {
          'zh': '完全的',
          'ja': '完了',
          'es': 'Terminado',
          'pt': 'Concluído'
        },
        'Invoice': {
          'zh': '发票',
          'ja': '請求書',
          'es': 'Factura',
          'pt': 'Fatura'
        },
      },
      ];

      for (let index = 0; index < ship_arr.length; index++) {
        const element = ship_arr[index];

        if (text in element) {
          ret = element[text][language];
          break;
        }

      }
      //console.log(ret);
      return ret;
    }
  }

  toggleShow = (show_no) => {
    if (this.state.show != false && this.state.show == show_no) {
      this.setState({ show: false })
    } else {
      this.setState({ show: show_no })
    }
  }
  // Show Hide Reason Reject
  RejectReason = (reject_comments, show_no = 0) => {
    let { show } = this.state;

    return (
      <>
        <button className="rejectBtn" onClick={() => this.toggleShow(show_no, show)}>{show == show_no ? this.state.task_language_explicit.task_details.hide : this.state.task_language_explicit.task_details.show} {this.state.task_language_explicit.task_details.reason}</button>
        {show == show_no && <div className="resaonWrapper">
          <div className="row">
            <div className="col-md-12"><p><strong>{this.state.task_language_explicit.task_details.reject_reason}</strong></p><p>{ReactHtmlParser(htmlDecode(reject_comments).replace(/(?:\r\n|\r|\n)/g, '<br />'))}</p></div>
          </div>
        </div>}
      </>
    )
  };

  getConverTime = (time) => {
    let timeString = '';
    let H = time.substr(0, 2);
    let h = H % 12 || 12;
    let ampm = (H < 12 || H === 24) ? "AM" : "PM";
    timeString = h.toLocaleString('en-US', { minimumIntegerDigits: 2, useGrouping: false }) + ":" + time.substr(2, 3) + " " + ampm;
    return timeString;
  };

  getBlueDartDynamicContent = (sel_index) => {

    let dynamic_html = this.props.discussion.shippingInfo.map((value, index) => {
      if (sel_index == index) {
        let obj = this.props.discussion.shippingInfo[sel_index][Object.keys(this.props.discussion.shippingInfo[sel_index])];

        let show_by_role = true;
        if (this.props.role_finance == false && obj.organization == 1007) {
          show_by_role = false;
        }

        let html_bottom = [];
        for (let props in obj.shipping_steps) {
          html_bottom.push(
            <tr>
              <td>{obj.shipping_steps[props].scan_date}</td>
              <td>{this.getConverTime(obj.shipping_steps[props].scan_time)}</td>
              <td>{obj.shipping_steps[props].scanned_location}</td>
              <td>
                {(obj.shipping_steps[props].cstat_desc) ? obj.shipping_steps[props].cstat_desc : obj.shipping_steps[props].scan}
                {(obj.shipping_steps[props].scan_code == 'POD' && obj.shipping_steps[props].file_path != '') && (<a href={`${ship_download + obj.shipping_steps[props].file_path}/bluedart?token=${this.props.login_token}`} target="_blank">
                  <img src={infoIcon} width="28" height="28" id="bluedartdownload" type="button" />
                </a>)}
              </td>
            </tr>
          );
        }

        return (
          <Row>
            <Col sm="12">
              <div className="views-row">
                {show_by_role == true && (
                  <>
                    <div className="views-field views-field-field-country">
                      <strong className="views-label views-label-field-country">
                        Invoice Date
                      </strong>
                      <div className="field-content">
                        {obj.invoice_display_date}
                      </div>
                    </div>
                    <div className="views-field views-field-field-country">
                      <strong className="views-label views-label-field-country">
                        Invoice File
                      </strong>
                      <div className="field-content">
                        {obj.invoice_file_name}
                        {(obj.invoice_file_name != '') && (<a href={`${ship_download + obj.invoice_file_hash}/invoice?token=${this.props.login_token}`} target="_blank">
                          <img src={infoIcon} width="28" height="28" id="invoicedownload" type="button" />
                        </a>)}
                      </div>
                    </div>
                    <div className="views-field views-field-field-country">
                      <strong className="views-label views-label-field-country">
                        Invoice Location
                      </strong>
                      <div className="field-content">
                        {obj.invoice_location}
                      </div>
                    </div>
                    <div className="views-field views-field-field-country">
                      <strong className="views-label views-label-field-country">
                        Packing Date
                      </strong>
                      <div className="field-content">
                        {obj.packing_display_date}
                      </div>
                    </div>
                    <div className="views-field views-field-field-country">
                      <strong className="views-label views-label-field-country">
                        Packing File
                      </strong>
                      <div className="field-content">
                        {obj.packing_file_name}
                        {(obj.packing_file_name != '') && (<a href={`${ship_download + obj.packing_file_hash}/package?token=${this.props.login_token}`} target="_blank">
                          <img src={infoIcon} width="28" height="28" id="invoicedownload" type="button" />
                        </a>)}
                      </div>
                    </div>
                    <div className="views-field views-field-field-country">
                      <strong className="views-label views-label-field-country">
                        Packing Location
                      </strong>
                      <div className="field-content">
                        {obj.packing_location}
                      </div>
                    </div>
                  </>
                )}
                <div className="views-field views-field-field-country">
                  <strong className="views-label views-label-field-country">
                    Ref. No.
                  </strong>
                  <div className="field-content">
                    {obj.ref_no}
                  </div>
                </div>
                <div className="views-field views-field-field-country">
                  <strong className="views-label views-label-field-country">
                    Origin Location
                  </strong>
                  <div className="field-content">
                    {obj.origin}
                  </div>
                </div>
                <div className="views-field views-field-field-country">
                  <strong className="views-label views-label-field-country">
                    Destination Location
                  </strong>
                  <div className="field-content">
                    {obj.destination}
                  </div>
                </div>
                <div className="views-field views-field-field-country">
                  <strong className="views-label views-label-field-country">
                    Pick Up Date
                  </strong>
                  <div className="field-content">
                    {obj.pick_up_date}
                  </div>
                </div>
                <div className="views-field views-field-field-country">
                  <strong className="views-label views-label-field-country">
                    Pick Up Time
                  </strong>
                  <div className="field-content">
                    {(obj.pick_up_time != null && obj.pick_up_time != '') ? this.getConverTime(obj.pick_up_time) : null}
                  </div>
                </div>
                <div className="views-field views-field-field-country">
                  <strong className="views-label views-label-field-country">
                    Expected Delivery Date
                  </strong>
                  <div className="field-content">
                    {obj.expected_delivery_date}
                  </div>
                </div>
                <div className="views-field views-field-field-country">
                  <strong className="views-label views-label-field-country">
                    Original Delivery Date
                  </strong>
                  <div className="field-content">
                    {obj.dynamic_expected_delivery_date}
                  </div>
                </div>
              </div>
              <div className="table-responsive-lg">
                <table className="orderDetailsTable table table-striped">
                  <thead>
                    <tr>
                      <th>{this.state.task_language_explicit.task_details.date}</th>
                      <th>{this.state.task_language_explicit.task_details.time}</th>
                      <th>{this.state.task_language_explicit.task_details.location}</th>
                      <th>{this.state.task_language_explicit.task_details.track_status}</th>
                    </tr>
                  </thead>
                  <tbody>
                    {html_bottom}

                  </tbody>
                </table>
              </div>
            </Col>
          </Row>
        );
      }
    });
    return dynamic_html;
  };

  getDynamicContent = (sel_index = 0) => {

    let dynamic_html = this.props.discussion.shippingInfo.map((value, index) => {
      if (sel_index == index) {
        let obj = this.props.discussion.shippingInfo[sel_index][Object.keys(this.props.discussion.shippingInfo[sel_index])];
        let html_top = [];

        for (let property in obj) {
          /*let show_by_role = true;               
          if(this.props.role_finance == false && obj['organization'] == 1007){
            show_by_role = false;
          }
          if((property == 'Invoice Doc' || property == 'Packing List') && show_by_role == false){
            continue;
          }*/
          if (obj[property] && obj[property].display_date != '' && obj[property].display_date != undefined && property != 'partner' && property != 'organization') {
            html_top.push(<li className={`active ${obj[property].extra_class}`}><span>{this.getTranslatedTextShipping(property, this.state.taskDetails.language)}</span><small>{obj[property].display_date}</small></li>);
          } else if (property != 'partner' && property != 'organization') {
            html_top.push(<li className=""><span>{this.getTranslatedTextShipping(property, this.state.taskDetails.language)}</span></li>);
          }
        }

        let html_bottom = [];
        let shipping_step = ['Approval Document', 'Approval Haz Clearance', 'FDA Approval', 'Customs Clearance'];
        for (let property in obj) {

          if (property == 'Shipping Progress') {
            for (let props in obj[property]) {
              if (obj[property][props].date != '' && obj[property][props].date != undefined) {
                html_bottom.push(
                  <tr>
                    <td>{obj[property][props].date}</td>
                    <td>{obj[property][props].time}</td>
                    <td>{obj[property][props].location}</td>
                    <td>{this.getTranslatedTextShipping(shipping_step[props], this.state.taskDetails.language)} {this.getTranslatedTextShipping(obj[property][props].comment, this.state.taskDetails.language)}</td>
                  </tr>
                );
              }
            }
          } else {
            if (obj[property] && obj[property].date != '' && property != 'partner' && property != 'organization') {
              let show_by_role = true;
              if (this.props.role_finance == false && obj['organization'] == 1007) {
                show_by_role = false;
              }
              if ((property == 'Invoice Doc' || property == 'Packing List') && show_by_role == false) {
                continue;
              }
              html_bottom.push(
                <tr>
                  <td>{obj[property].date}</td>
                  <td>{obj[property].time}</td>
                  <td>{obj[property].location}</td>
                  <td>
                    {this.getTranslatedTextShipping(property, this.state.taskDetails.language) + ".  "}
                    {/*((property == 'Invoice Doc' || property == 'Packing List' || property == 'AWB Doc' || property == 'HAWB Doc') && obj[property].comment != '') ? obj[property].comment : ''*/}
                    {(property == 'Invoice Doc' && obj[property].comment != '') && (<a href={`${ship_download + obj[property].file_hash}/invoice?token=${this.props.login_token}`} target="_blank">
                      <img src={infoIcon} width="28" height="28" id="invoicedownload" type="button" />
                    </a>)}
                    {(property == 'Packing List' && obj[property].comment != '') && (<a href={`${ship_download + obj[property].file_hash}/package?token=${this.props.login_token}`} target="_blank">
                      <img src={infoIcon} width="28" height="28" id="invoicedownload" type="button" />
                    </a>)}
                    {(property == 'AWB Doc' && obj[property].comment != '') && (<a href={`${ship_download + obj[property].file_hash}/${obj[property].file_type}?token=${this.props.login_token}`} target="_blank">
                      <img src={infoIcon} width="28" height="28" id="invoicedownload" type="button" />
                    </a>)}
                    {(property == 'HAWB Doc' && obj[property].comment != '') && (<a href={`${ship_download + obj[property].file_hash}/${obj[property].file_type}?token=${this.props.login_token}`} target="_blank">
                      <img src={infoIcon} width="28" height="28" id="invoicedownload" type="button" />
                    </a>)}
                  </td>
                </tr>
              );
            }
          }
        }

        return (
          <Row>
            <Col sm="12">
              <ul className="stepProgressbar">
                {html_top}
              </ul>
              <div className="table-responsive-lg">
                <table className="orderDetailsTable table table-striped">
                  <thead>
                    <tr>
                      <th>{this.state.task_language_explicit.task_details.date}</th>
                      <th>{this.state.task_language_explicit.task_details.time}</th>
                      <th>{this.state.task_language_explicit.task_details.location}</th>
                      <th>{this.state.task_language_explicit.task_details.track_status}</th>
                    </tr>
                  </thead>
                  <tbody>
                    {html_bottom}
                  </tbody>
                </table>
              </div>
            </Col>
          </Row>);

      }
    });
    return dynamic_html;
    //this.setState({dynamic_html:dynamic_html});
  }

  makeDynamicHtml = () => {
    if (this.state.attachmentList !== "" && this.state.attachmentList.length > 0) {

      let html = [];

      //console.log('attachmentList',this.state.attachmentList);

      for (let index = 0; index < this.state.attachmentList[0].task_discussion_files.length; index++) {
        const element = this.state.attachmentList[0].task_discussion_files[index];
        html.push(<div className="col-md-12">
          <div className="form-check">
            <label className="form-check-label">
              <Field
                type="checkbox"
                className="form-check-input"
                value="1"
                defaultChecked={element.chk}
                onClick={(e) => {
                  this.handleDiscussionAttchments(e, index);
                }}
              />
              {element.actual_file_name}
            </label>
          </div>
        </div>);
      }

      for (let index = 0; index < this.state.attachmentList[0].task_files.length; index++) {
        const element = this.state.attachmentList[0].task_files[index];
        html.push(<div className="col-md-12">
          <div className="form-check">
            <label className="form-check-label">
              <Field
                type="checkbox"
                className="form-check-input"
                value="1"
                defaultChecked={element.chk}
                onClick={(e) => {
                  this.handleTaskAttchments(e, index);
                }}
              />
              {element.actual_file_name}
            </label>
          </div>
        </div>);
      }

      return html;
    } else {
      return this.state.task_language_explicit.task_details.no_attachment;
    }
  };

  componentDidMount() { //console.log(this.props.location)
    let param_query = this.props.location.search;

    if (param_query === "?display=files") {
      this.toggle("3");
    }

    if (param_query === "?display=discussions") {
      this.toggle("2");
    }

    this.setState({ showLoader: true });

    axios.get(`/tasks/temp_task_orders/${this.state.task_id}`).then((res) => {
      this.setState({ not_approved_tasks: res.data.data });
    })
      .catch(err => {

        console.log(err);
      });

    this.props.getDiscussion(`${this.state.task_id}`, (result) => {
      if (this.props.detailsErr) {
        document.title = `Not Found | Dr. Reddy's API`;
        var errText = "Invalid Access To This Page.";
        this.setState({ invalid_access: true, showLoader: false });
        if (this.props.detailsErr.status === 5) {
          showErrorMessage(this.props.detailsErr.errors);
        }
      } else {

        // console.log(this.props.discussion);



        let lang_data = require(`../../assets/lang/${this.props.discussion.taskDetails.language}.json`);

        this.setState({ task_language_explicit: lang_data });


        let post_arr = {
          type: "G",
          lang: this.props.discussion.taskDetails.language,
        };
        if (
          this.props.discussion.taskDetails.request_type == 23 ||
          this.props.discussion.taskDetails.request_type == 41 ||
          this.props.discussion.taskDetails.request_type == 43
        ) {
          post_arr = {
            type: "N",
            lang: this.props.discussion.taskDetails.language,
          };
        } else if (
          this.props.discussion.taskDetails.request_type == 7 ||
          this.props.discussion.taskDetails.request_type == 34
        ) {
          post_arr = {
            type: "C",
            lang: this.props.discussion.taskDetails.language,
          };
        }

        this.props.getTaskStatus(post_arr, (res_arr) => {
          if (
            this.props.discussion.taskDetails.request_type == 23 ||
            this.props.discussion.taskDetails.request_type == 41 ||
            this.props.discussion.taskDetails.request_type === 43
          ) {
            this.setState({ order_status: this.props.task_status_arr });
          } else if (
            this.props.discussion.taskDetails.request_type == 7 ||
            this.props.discussion.taskDetails.request_type == 34
          ) {
            this.setState({ complaint_status: this.props.task_status_arr });
          } else {
            this.setState({ other_status: this.props.task_status_arr });
          }

          let userData = BasicUserData();

          document.title = `${this.props.discussion.taskDetails.task_ref} | Dr. Reddy's API`;

          this.setState({ taskDetails: this.props.discussion.taskDetails });
          this.setState({
            taskDetailsFiles: this.props.discussion.taskDetailsFiles,
          });
          this.setState({
            discussionFiles: this.props.discussion.discussionFiles,
          });
          this.setState({
            downloadAllLink: this.props.discussion.downloadAllLink,
          });
          this.setState({ preship_arr: this.props.discussion.preShipmentInfo });
          //console.log(this.props.discussion);
          if (
            userData.role === 2 &&
            this.props.discussion.taskDetails.agent_id > 0
          )
            this.setState({ showBehalf: true });

          if (
            this.state.taskDetails.submitted_by &&
            parseInt(this.state.taskDetails.submitted_by) > 0
          )
            this.setState({ showBehalf: true });

          let active_stat = null;
          let push_stat = [];
          if (
            this.props.discussion.taskDetails.status_details !== null &&
            (this.props.discussion.taskDetails.request_type === 23 ||
              this.props.discussion.taskDetails.request_type === 41 ||
              this.props.discussion.taskDetails.request_type === 43)
          ) {
            active_stat = this.props.task_status_arr.indexOf(
              this.props.discussion.taskDetails.status_details
            );
            for (let index = 0; index <= active_stat; index++) {
              push_stat.push(this.props.task_status_arr[index]);
            }
          } else if (
            this.props.discussion.taskDetails.status_details != null &&
            (this.props.discussion.taskDetails.request_type == 7 ||
              this.props.discussion.taskDetails.request_type == 34)
          ) {
            active_stat = this.props.task_status_arr.indexOf(
              this.props.discussion.taskDetails.status_details
            );
            for (let index = 0; index <= active_stat; index++) {
              push_stat.push(this.props.task_status_arr[index]);
            }
          } else {
            if (this.props.discussion.taskDetails.request_type !== 24) {
              active_stat = this.props.task_status_arr.indexOf(
                this.props.discussion.taskDetails.status_details
              );
              for (let index = 0; index <= active_stat; index++) {
                push_stat.push(this.props.task_status_arr[index]);
              }
            }
          }
          this.setState({ status_active: push_stat, showLoader: false });
        });
      }
      if (this.props.tour_counter == true) {
        axios.get(`/tasks/update_tour_counter/${this.state.task_id}`)
          .then(res => {
            axios
              .get("/generate_token")
              .then(response => {
                this.props.updateTourToken(response.data.token)
              })
              .catch(error => {
              })
          })
          .catch(err => {
          });
      }

      if (this.props.discussion.taskDetails.close_status == 1) {
        if (this.props.tour_counter_new == true) {
          axios.get(`/tasks/update_tour_counter_new/${this.state.task_id}`)
            .then(res => {
              axios
                .get("/generate_token")
                .then(response => {
                  this.props.updateTourToken(response.data.token)
                })
                .catch(error => {
                })
            })
            .catch(err => {
            });
        }
      }

      this.setState({ rdd_pending: this.props.discussion.taskDetails.rdd_pending })

      // if (this.props.tour_counter_11 == true && (this.props.discussion.shippingTab && this.props.discussion.shippingInfo.length>0) ) {
      //   axios.get(`/tasks/update_tour_counter_11/${this.state.task_id}`)
      //     .then(res => {
      //       axios
      //         .get("/generate_token")
      //         .then(response => {
      //           this.props.updateTourToken(response.data.token)
      //         })
      //         .catch(error => {
      //         })
      //     })
      //     .catch(err => {
      //     });
      // }

    });

    let userData = BasicUserData();
    if (userData) {
      this.setState({
        isLoading: false,
      });
    }
  }

  showProduct = () => {
    if (this.state.taskDetails.request_type != 24) {

      if (this.state.taskDetails.request_type != 23) {
        return <div className="views-field views-field-field-product-name">
          <strong className="views-label views-label-field-product-name">
            {this.state.task_language_explicit.task_details.product}{" "}
          </strong>
          <div className="field-content">
            {ReactHtmlParser(
              htmlDecode(this.state.taskDetails.product_name)
            )}
          </div>
        </div>;
      }

      if (this.state.taskDetails.request_type == 23 && this.state.taskDetails.order_verified_status === 1) {
        return <div className="views-field views-field-field-product-name">
          <strong className="views-label views-label-field-product-name">
            {this.state.task_language_explicit.task_details.product}{" "}
          </strong>
          <div className="field-content">
            {ReactHtmlParser(
              htmlDecode(this.state.taskDetails.product_name)
            )}
          </div>
        </div>;
      }

    }
  }

  showTitle = () => {
    if (this.state.taskDetails.request_type != 24) {

      if (this.state.taskDetails.request_type != 23) {
        return ` | ${htmlDecode(this.state.taskDetails.product_name)}`;
      }

      if (this.state.taskDetails.request_type == 23 && this.state.taskDetails.order_verified_status === 1) {
        return ` | ${htmlDecode(this.state.taskDetails.product_name)}`;
      }

    }
  }

  showUnApprovedTasks = () => {
    if (this.state.taskDetails.request_type == 23 && this.state.taskDetails.order_verified_status === 0) {
      let ret_data = [];

      ret_data = this.state.not_approved_tasks.map((val) => {
        return (
          <div className="col-md-12 multiProduct">
            <div className="views-field views-field-field-product-name pl-0">
              <strong className="views-label views-label-field-product-name">
                {this.state.task_language_explicit.task_details.product}{" "}
              </strong>
              <div className="field-content">
                {ReactHtmlParser(
                  htmlDecode(val.product_name)
                )}
              </div>
            </div>

            <div className="views-field views-field-field-product-name pl-0">
              <strong className="views-label views-label-field-country">
                {" "}
                {this.state.task_language_explicit.task_details.quantity}{" "}
              </strong>
              <div className="field-content">
                {" "}
                {(val.quantity !== "" && val.quantity !== null) ? val.quantity : "-"}{" "}
              </div>
            </div>

            <div className="views-field views-field-field-product-name pl-0">
              <strong className="views-label views-label-field-country">
                {this.state.task_language_explicit.task_details.requested_date_delivery}
              </strong>
              <div className="field-content">
                {(val.rdd !== null && val.rdd !== "") ? val.rdd : "-"}{" "}
                <button onClick={() => this.editRDDDetails(val.temp_id, val.editable_rdd)} >{this.state.task_language_explicit.my_orders.edit}</button>
              </div>

            </div>

          </div>
        )
      })

      // console.log(ret_data);

      return ret_data;
    }
  }

  editRDDDetails = (temp_id, editable_rdd) => {
    // console.log(temp_id, editable_rdd);
    let fromMaxDate = new Date();
    let toMaxDate = new Date(
      new Date().setFullYear(new Date().getFullYear() + 10)
    );

    this.setState({ showRDDPopup: true, RDDShow: false, edit_temp_id: temp_id, edit_rdd: new Date(editable_rdd), new_rdd: new Date(editable_rdd), fromMaxDate: fromMaxDate, toMaxDate, toMaxDate });
  }

  displayRequestDetails = () => {
    return (
      <div>
        <div className="navSec">
          <h1 className="field-content">
            {this.state.taskDetails.request_type === 23 ||
              this.state.taskDetails.request_type === 41
              ? this.state.task_language_explicit.task_details.order_details
              : this.state.taskDetails.request_type === 7 ||
                this.state.taskDetails.request_type === 34
                ? this.state.task_language_explicit.task_details.complaints_details
                : this.state.taskDetails.request_type === 24
                  ? this.state.task_language_explicit.task_details.forecast_details
                  : this.state.taskDetails.req_name}

            {this.showTitle()}
            | {this.state.taskDetails.task_ref}
          </h1>
          <Nav tabs>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === "1" })}
                onClick={() => {
                  this.toggle("1");
                }}
              >
                {this.state.task_language_explicit.task_details.details}
              </NavLink>
            </NavItem>
            <NavItem id={`new-step`} >
              <NavLink
                className={classnames({ active: this.state.activeTab === "2" })}
                onClick={() => {
                  this.toggle("2");
                }}
              >
                {this.state.task_language_explicit.task_details.discussion}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === "3" })}
                onClick={() => {
                  this.toggle("3");
                }}
              >
                {this.state.task_language_explicit.task_details.files}
              </NavLink>
            </NavItem>
            {(this.state.taskDetails.request_type && this.props.discussion.shippingTab && (this.state.taskDetails.request_type == 23 || this.state.taskDetails.request_type == 43)) ? (
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === "4" })}
                  onClick={() => {
                    this.toggle("4");
                  }}
                >
                  {this.state.task_language_explicit.task_details.payment}
                </NavLink>
              </NavItem>
            ) : ''}
            {(this.state.taskDetails.request_type && this.props.discussion.preShipmentTab && (this.state.taskDetails.request_type == 23 || this.state.taskDetails.request_type == 43)) ? (
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === "6" })}
                  onClick={() => {
                    this.toggle("6");
                  }}
                >
                  {this.state.task_language_explicit.task_details.pre_shipment}
                </NavLink>
              </NavItem>
            ) : ''}
            {(this.props.discussion.shippingTab && this.props.discussion.shippingInfo.length > 0 && this.state.taskDetails.show_tracking == 1) ? (
              <NavItem id={`feature-step-11`} >
                <NavLink
                  className={classnames({ active: this.state.activeTab === "5" })}
                  onClick={() => {
                    this.toggle("5");
                  }}
                >
                  {this.state.task_language_explicit.task_details.track_shipment}
                </NavLink>
              </NavItem>
            ) : ''}
          </Nav>
          <div className="clearfix"></div>
        </div>
        <div className="clearfix"></div>
        <div className="requestDetails">
          <TabContent activeTab={this.state.activeTab}>
            <TabPane tabId="1">
              <Row>
                <Col sm="12">
                  <div className="views-row">
                    <div className="views-field views-field-title">
                      <strong className="views-label views-label-title">
                        {this.state.taskDetails.request_type === 7 ||
                          this.state.taskDetails.request_type === 34
                          ? this.state.task_language_explicit.task_details
                            .complaint_number
                          : this.state.taskDetails.request_type === 23 ||
                            this.state.taskDetails.request_type === 41
                            ? this.state.task_language_explicit.task_details.order_number
                            : this.state.taskDetails.request_type === 24
                              ? this.state.task_language_explicit.task_details.forecast_ref_no
                              : this.state.task_language_explicit.task_details.reques_number}
                      </strong>
                      <div className="field-content">
                        {this.state.taskDetails.task_ref}
                      </div>
                    </div>

                    {this.state.taskDetails.request_type !== 23 &&
                      this.state.taskDetails.request_type !== 24 &&
                      this.state.taskDetails.request_type !== 43 && (
                        <div className="views-field views-field-field-request-title">
                          <strong className="views-label views-label-field-request-title">
                            {this.state.taskDetails.request_type === 7 ||
                              this.state.taskDetails.request_type === 34
                              ? this.state.task_language_explicit.task_details
                                .complaint_title
                              : this.state.task_language_explicit.task_details
                                .request_title}
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.req_name}
                          </div>
                        </div>
                      )}

                    {this.showProduct()}

                    {this.state.taskDetails.request_type === 24 && (
                      <div className="views-field views-field-field-product-name">
                        <strong className="views-label views-label-field-product-name">
                          {
                            this.state.task_language_explicit.task_details
                              .notification_date
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {" "}
                          {this.state.taskDetails.date_added}{" "}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type !== 24 &&
                      this.state.taskDetails.request_type !== 40 && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {this.state.task_language_explicit.task_details.market}{" "}
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.country_name !== "" &&
                              this.state.taskDetails.country_name !== null
                              ? ReactHtmlParser(
                                htmlDecode(
                                  this.state.taskDetails.country_name
                                )
                              )
                              : "-"}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetails.request_type === 40 && (
                      <div className="views-field views-field-field-product-name">
                        <strong className="views-label views-label-field-product-name">
                          {
                            this.state.task_language_explicit.task_details
                              .gmp_clearance_id
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {" "}
                          {this.state.taskDetails.gmp_clearance_id}{" "}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 40 && (
                      <div className="views-field views-field-field-product-name">
                        <strong className="views-label views-label-field-product-name">
                          {this.state.task_language_explicit.task_details.email_id}{" "}
                        </strong>
                        <div className="field-content">
                          {" "}
                          {this.state.taskDetails.tga_email_id}{" "}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 40 && (
                      <div className="views-field views-field-field-product-name">
                        <strong className="views-label views-label-field-product-name">
                          {
                            this.state.task_language_explicit.task_details
                              .applicants_address
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {" "}
                          {this.state.taskDetails.applicant_name}{" "}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 40 && (
                      <div className="views-field views-field-field-product-name">
                        <strong className="views-label views-label-field-product-name">
                          {
                            this.state.task_language_explicit.task_details
                              .documents_required
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {" "}
                          {this.state.taskDetails.doc_required}{" "}
                        </div>
                      </div>
                    )}

                    {(this.state.taskDetails.request_type === 7 ||
                      this.state.taskDetails.request_type === 34) && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {this.state.task_language_explicit.task_details.batch_number}{" "}
                          </strong>
                          <div className="field-content">
                            {" "}
                            {this.state.taskDetails.batch_number}{" "}
                          </div>
                        </div>
                      )}

                    {(this.state.taskDetails.request_type === 7 ||
                      this.state.taskDetails.request_type === 34) && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {this.state.task_language_explicit.task_details.nature_of_issue}{" "}
                          </strong>
                          <div className="field-content">
                            {" "}
                            {this.state.taskDetails.nature_of_issue !== null &&
                              this.state.taskDetails.nature_of_issue !== ""
                              ? this.state.taskDetails.nature_of_issue
                              : "-"}{" "}
                          </div>
                        </div>
                      )}

                    {(this.state.taskDetails.request_type === 7 ||
                      (this.state.taskDetails.request_type === 23 && this.state.taskDetails.order_verified_status === 1) ||
                      this.state.taskDetails.request_type === 41 ||
                      this.state.taskDetails.request_type === 43 ||
                      this.state.taskDetails.request_type === 44 ||
                      this.state.taskDetails.request_type === 31 ||
                      this.state.taskDetails.request_type === 34) && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {" "}
                            {this.state.task_language_explicit.task_details.quantity}{" "}
                          </strong>
                          <div className="field-content">
                            {" "}
                            {this.state.taskDetails.quantity !== "" &&
                              this.state.taskDetails.quantity !== null
                              ? this.state.taskDetails.quantity
                              : "-"}{" "}
                          </div>
                        </div>
                      )}

                    {((this.state.taskDetails.request_type === 23 && this.state.taskDetails.order_verified_status === 1) ||
                      this.state.taskDetails.request_type === 41 ||
                      this.state.taskDetails.request_type === 39 ||
                      this.state.taskDetails.request_type === 40) && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {(this.state.taskDetails.request_type === 23 && this.state.taskDetails.order_verified_status === 1) ||
                              this.state.taskDetails.request_type === 41
                              ? this.state.task_language_explicit.task_details
                                .requested_date_delivery
                              : this.state.task_language_explicit.task_details
                                .requested_deadline}
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.rdd !== null &&
                              this.state.taskDetails.rdd !== ""
                              ? this.state.taskDetails.rdd
                              : "-"}{" "}{this.state.taskDetails.request_type === 23 && this.state.taskDetails.order_verified_status === 1 && this.state.taskDetails.close_status !== 1 && this.state.rdd_pending === false && <button onClick={() => this.editRDDDetails(this.state.taskDetails.order_temp_id, this.state.taskDetails.rdd)} >{this.state.task_language_explicit.my_orders.edit}</button>}
                            {this.state.taskDetails.request_type === 23 && this.state.taskDetails.order_verified_status === 1 && this.state.taskDetails.close_status !== 1 && this.state.rdd_pending === true && <span style={{ color: 'red' }} >{this.state.task_language_explicit.task_details.pending_rdd}</span>}
                          </div>
                        </div>
                      )}

                    {((this.state.taskDetails.request_type !== 23 && this.state.taskDetails.request_type !== 24 && this.state.taskDetails.request_type !== 43) || (this.state.taskDetails.request_type === 43 && this.state.taskDetails.sales_order_date != null && this.state.taskDetails.sales_order_date != '')) && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {this.state.task_language_explicit.task_details.logged_date}{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.request_type === 43 ? this.state.taskDetails.sales_order_date : this.state.taskDetails.date_added}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 1 &&
                      this.state.taskDetails.service_request_type.indexOf(
                        "Samples"
                      ) !== -1 && (
                        <div className="views-field views-field-created">
                          <strong className="views-label views-label-created">
                            {this.state.task_language_explicit.task_details.samples}{" "}
                          </strong>
                          {this.state.taskDetails.number_of_batches !== null &&
                            this.state.taskDetails.number_of_batches !== "" && (
                              <div className="field-content">
                                {
                                  this.state.task_language_explicit.task_details
                                    .number_of_batches
                                }{" "}
                                {this.state.taskDetails.number_of_batches}
                              </div>
                            )}
                          {this.state.taskDetails.quantity !== null &&
                            this.state.taskDetails.quantity !== "" && (
                              <div className="field-content">
                                {this.state.task_language_explicit.task_details.quantity}{" "}
                                {this.state.taskDetails.quantity}
                              </div>
                            )}
                        </div>
                      )}

                    {this.state.taskDetails.request_type === 1 &&
                      this.state.taskDetails.service_request_type.indexOf(
                        "Working Standards"
                      ) !== -1 && (
                        <div className="views-field views-field-created">
                          <strong className="views-label views-label-created">
                            {
                              this.state.task_language_explicit.task_details
                                .working_standards
                            }{" "}
                          </strong>
                          <div className="field-content">
                            {this.state.task_language_explicit.task_details.impurities}{" "}
                            {this.state.taskDetails.working_quantity}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetails.request_type === 1 &&
                      this.state.taskDetails.service_request_type.indexOf(
                        "Impurities"
                      ) !== -1 && (
                        <div className="views-field views-field-created">
                          <strong className="views-label views-label-created">
                            {this.state.task_language_explicit.task_details.impurities}{" "}
                          </strong>
                          {this.state.taskDetails.impurities_quantity !==
                            null &&
                            this.state.taskDetails.impurities_quantity !==
                            "" && (
                              <div className="field-content">
                                {
                                  this.state.task_language_explicit.task_details
                                    .impurities_quantity
                                }{" "}
                                {this.state.taskDetails.impurities_quantity}
                              </div>
                            )}
                          {this.state.taskDetails.specify_impurity_required !==
                            null &&
                            this.state.taskDetails.specify_impurity_required !==
                            "" && (
                              <div className="field-content">
                                {
                                  this.state.task_language_explicit.task_details
                                    .impurities_required
                                }{" "}
                                {htmlDecode(
                                  this.state.taskDetails
                                    .specify_impurity_required
                                )}
                              </div>
                            )}
                        </div>
                      )}

                    {this.state.taskDetails.request_type === 1 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {
                            this.state.task_language_explicit.task_details
                              .service_request_type
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.service_request_type}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 1 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {
                            this.state.task_language_explicit.task_details
                              .shipping_address
                          }
                        </strong>
                        <div className="field-content">
                          {ReactHtmlParser(
                            htmlDecode(this.state.taskDetails.shipping_address)
                          )}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 9 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {
                            this.state.task_language_explicit.task_details
                              .audit_visit_date
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.request_audit_visit_date}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 9 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {this.state.task_language_explicit.task_details.site_name}{" "}
                        </strong>
                        <div className="field-content">
                          {htmlDecode(
                            this.state.taskDetails.audit_visit_site_name
                          )}
                        </div>
                      </div>
                    )}

                    {(this.state.taskDetails.request_type === 22 ||
                      this.state.taskDetails.request_type === 3 ||
                      this.state.taskDetails.request_type === 1 ||
                      this.state.taskDetails.request_type === 17 ||
                      this.state.taskDetails.request_type === 2 ||
                      this.state.taskDetails.request_type === 10 ||
                      this.state.taskDetails.request_type === 12 ||
                      this.state.taskDetails.request_type === 21 ||
                      this.state.taskDetails.request_type === 18 ||
                      this.state.taskDetails.request_type === 19 ||
                      this.state.taskDetails.request_type === 4 ||
                      this.state.taskDetails.request_type === 30 ||
                      this.state.taskDetails.request_type === 32 ||
                      this.state.taskDetails.request_type === 33 ||
                      this.state.taskDetails.request_type === 35 ||
                      this.state.taskDetails.request_type === 36 ||
                      this.state.taskDetails.request_type === 37 ||
                      this.state.taskDetails.request_type === 41 ||
                      this.state.taskDetails.request_type === 42 ||
                      this.state.taskDetails.request_type === 44) && (
                        <div className="views-field views-field-field-pharmacopoeia">
                          <strong className="views-label views-label-field-pharmacopoeia">
                            {this.state.task_language_explicit.task_details.pharmacopoeia}{" "}
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.pharmacopoeia !== "" &&
                              this.state.taskDetails.pharmacopoeia !== null
                              ? htmlDecode(this.state.taskDetails.pharmacopoeia)
                              : "-"}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetails.request_type === 39 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {this.state.task_language_explicit.task_details.dmf_cmp}{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.dmf_number &&
                            this.state.taskDetails.dmf_number !== null &&
                            htmlDecode(this.state.taskDetails.dmf_number)}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 27 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {this.state.task_language_explicit.task_details.dmf_number}{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.dmf_number &&
                            this.state.taskDetails.dmf_number !== null &&
                            htmlDecode(this.state.taskDetails.dmf_number)}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 28 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {
                            this.state.task_language_explicit.task_details
                              .requested_date_response_closure
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.rdfrc &&
                            this.state.taskDetails.rdfrc !== null &&
                            this.state.taskDetails.rdfrc}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 29 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {
                            this.state.task_language_explicit.task_details
                              .notification_number
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.notification_number &&
                            this.state.taskDetails.notification_number !==
                            null &&
                            htmlDecode(
                              this.state.taskDetails.notification_number
                            )}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type === 38 && (
                      <div className="views-field views-field-created">
                        <strong className="views-label views-label-created">
                          {this.state.task_language_explicit.task_details.document_type}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.apos_document_type &&
                            this.state.taskDetails.apos_document_type !==
                            null &&
                            this.state.taskDetails.apos_document_type}
                        </div>
                      </div>
                    )}

                    {(this.state.taskDetails.request_type === 2 ||
                      this.state.taskDetails.request_type === 3) && (
                        <div className="views-field views-field-field-pharmacopoeia">
                          <strong className="views-label views-label-field-pharmacopoeia">
                            {
                              this.state.task_language_explicit.task_details
                                .polymorphic_form
                            }{" "}
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.polymorphic_form !== "" &&
                              this.state.taskDetails.polymorphic_form !== null
                              ? htmlDecode(
                                this.state.taskDetails.polymorphic_form
                              )
                              : "-"}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetails.request_type === 13 && (
                      <div className="views-field views-field-field-pharmacopoeia">
                        <strong className="views-label views-label-field-pharmacopoeia">
                          {this.state.task_language_explicit.task_details.stability_type}{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.stability_data_type}
                        </div>
                      </div>
                    )}

                    {(this.state.taskDetails.request_type === 23 ||
                      this.state.taskDetails.request_type === 41) &&
                      this.state.taskDetails.status_details != null && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {" "}
                            {
                              this.state.task_language_explicit.task_details
                                .committed_date_delivery
                            }{" "}
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.close_status == 1
                              ? this.state.taskDetails.closed_date
                              : this.state.taskDetails.expected_closure_date}
                          </div>
                        </div>
                      )}

                    {(this.state.taskDetails.request_type == 7 ||
                      this.state.taskDetails.request_type === 34) &&
                      this.state.taskDetails.status_details != null && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {" "}
                            {
                              this.state.task_language_explicit.task_details.closure_date
                            }{" "}
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.close_status == 1
                              ? this.state.taskDetails.closed_date
                              : this.state.taskDetails.expected_closure_date}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetails.request_type !== 23 &&
                      this.state.taskDetails.request_type !== 24 &&
                      this.state.taskDetails.request_type !== 7 &&
                      this.state.taskDetails.request_type !== 34 &&
                      this.state.taskDetails.request_type !== 43 &&
                      this.state.taskDetails.status_details !== null && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {" "}
                            {
                              this.state.task_language_explicit.task_details
                                .expected_closure_date
                            }{" "}
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.expected_closure_date
                              ? this.state.taskDetails.expected_closure_date
                              : "-"}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetails.request_type !== 23 &&
                      this.state.taskDetails.request_type !== 24 &&
                      this.state.taskDetails.request_type !== 7 &&
                      this.state.taskDetails.request_type !== 34 &&
                      this.state.taskDetails.status_details !== null &&
                      this.state.taskDetails.close_status == 1 && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {" "}
                            {
                              this.state.task_language_explicit.task_details
                                .actual_closure_date
                            }{" "}
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.closed_date}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetails.request_type != 24 &&
                      this.state.taskDetails.status_details != null && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {" "}
                            {this.state.task_language_explicit.task_details.status}{" "}
                          </strong>
                          <div className="field-content">
                            {" "}
                            {this.state.taskDetails.status_details}{" "}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetails.request_type != 24 &&
                      this.state.taskDetails.status_details != null && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {
                              this.state.task_language_explicit.task_details
                                .action_requested
                            }
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.required_action
                              ? this.state.taskDetails.required_action
                              : "-"}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetails.request_type == 43 &&
                      this.state.taskDetails.SAPDetails.city != null && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {
                              this.state.task_language_explicit.task_details
                                .shipto_address
                            }
                          </strong>
                          <div className="field-content">
                            <div>{this.state.taskDetails.SAPDetails.shipto_name
                              ? this.state.taskDetails.SAPDetails.shipto_name
                              : ""}</div>
                            {this.state.taskDetails.SAPDetails.street
                              ? this.state.taskDetails.SAPDetails.street + ', '
                              : ""}
                            {this.state.taskDetails.SAPDetails.city
                              ? this.state.taskDetails.SAPDetails.city + ', '
                              : ""}
                            {this.state.taskDetails.SAPDetails.post_code
                              ? this.state.taskDetails.SAPDetails.post_code + ', '
                              : ""}
                            {this.state.taskDetails.SAPDetails.country
                              ? this.state.taskDetails.SAPDetails.country + ', '
                              : ""}
                          </div>
                        </div>
                      )}

                    {this.state.taskDetailsFiles.length > 0 && (
                      <div className="views-field views-field-field-attachment">
                        <strong className="views-label views-label-field-attachment">
                          {this.state.task_language_explicit.task_details.attachment}
                          {this.state.taskDetailsFiles.length > 1 ? `s ` : ``}
                          {this.state.taskDetailsFiles.length > 1 &&
                            this.state.downloadAllLink != "" && (
                              <>
                                <a href={`${this.state.downloadAllLink}`}>
                                  <img
                                    src={infoIcon}
                                    width="28"
                                    height="28"
                                    id="DisabledAutoHideExample"
                                    type="button"
                                  />
                                  <Tooltip
                                    placement="right"
                                    isOpen={this.state.tooltipOpen}
                                    autohide={false}
                                    target="DisabledAutoHideExample"
                                    toggle={this.toggleTooltip}
                                  >
                                    {
                                      this.state.task_language_explicit.task_details
                                        .click_download_all_files
                                    }
                                  </Tooltip>
                                </a>
                              </>
                            )}{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetailsFiles
                            .map((file, p) => {
                              return (
                                <span className="file  file-image" key={p}>
                                  <a href={file.url}>{file.actual_file_name}</a>
                                </span>
                              );
                            })
                            .reduce((prev, curr) => [prev, ", ", curr])}
                        </div>
                      </div>
                    )}

                    {this.state.taskDetails.request_type == 43 &&
                      this.state.taskDetails.SAPDetails.date_added != null && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {
                              this.state.task_language_explicit.task_details
                                .shipto
                            }
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.SAPDetails.date_added
                              ? this.state.taskDetails.SAPDetails.date_added
                              : "-"}
                          </div>
                        </div>
                      )}
                    {this.state.taskDetails.request_type == 43 && this.state.taskDetails.po_no != null &&
                      this.state.taskDetails.SAPDetails.po_file_date != null && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {
                              this.state.task_language_explicit.task_details
                                .po_file_date
                            }
                          </strong>
                          <div className="field-content">
                            {this.state.taskDetails.SAPDetails.po_file_date
                              ? this.state.taskDetails.SAPDetails.po_file_date
                              : "-"}
                          </div>
                        </div>
                      )}
                    {(this.state.taskDetails.request_type == 23 || this.state.taskDetails.request_type == 43) && this.state.taskDetails.po_no != null && (
                      <div className="views-field views-field-field-country">
                        <strong className="views-label views-label-field-country">
                          {
                            this.state.task_language_explicit.task_details.po_no
                          }
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.po_no
                            ? this.state.taskDetails.po_no
                            : ""}
                        </div>
                      </div>
                    )}

                    {(this.state.taskDetails.request_type == 23 || this.state.taskDetails.request_type == 43) && this.state.taskDetails.po_delivery_date != null && (
                      <div className="views-field views-field-field-country">
                        <strong className="views-label views-label-field-country">
                          {
                            `PO Delivery Date`
                          }
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.po_delivery_date
                            ? this.state.taskDetails.po_delivery_date
                            : ""}
                        </div>
                      </div>
                    )}
                    {this.state.taskDetails.request_type == 43 && this.state.taskDetails.po_no != null &&
                      this.state.taskDetails.SAPDetails.po_file_name != null && (
                        <div className="views-field views-field-field-country">
                          <strong className="views-label views-label-field-country">
                            {
                              this.state.task_language_explicit.task_details
                                .po_file
                            }
                          </strong>
                          <div className="field-content">
                            <span className="file  file-image">
                              <a href={`${pourl}${this.state.taskDetails.sap_request_id}`} target="_blank">{this.state.taskDetails.SAPDetails.po_file_name
                                ? this.state.taskDetails.SAPDetails.po_file_name
                                : ""}</a>
                            </span>
                          </div>
                        </div>
                      )}

                    {this.state.showBehalf == true ? (
                      <div className="views-field views-field-body">
                        <strong className="views-label views-label-body">
                          {
                            this.state.task_language_explicit.task_details
                              .submitted_behalf
                          }{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.first_name +
                            " " +
                            this.state.taskDetails.last_name}
                        </div>
                      </div>
                    ) : (
                      ""
                    )}

                    {this.state.submittedBy == true ? (
                      <div className="views-field views-field-body">
                        <strong className="views-label views-label-body">
                          {this.state.task_language_explicit.task_details.submitted_by}{" "}
                        </strong>
                        <div className="field-content">
                          {this.state.taskDetails.posted_by_first_name +
                            " " +
                            this.state.taskDetails.posted_by_last_name}
                        </div>
                      </div>
                    ) : (
                      ""
                    )}
                    <div className="clearfix"></div>

                    <div className="views-field views-field-body">
                      <strong className="views-label views-label-body">
                        {this.state.task_language_explicit.task_details.description}{" "}
                      </strong>
                      <div className="field-content">
                        {this.state.taskDetails.content !== null &&
                          this.state.taskDetails.content !== ""
                          ? ReactHtmlParser(
                            htmlDecode(this.state.taskDetails.content)
                          )
                          : "-"}
                      </div>
                    </div>
                  </div>
                  <div className="row">{this.state.not_approved_tasks && this.state.not_approved_tasks.length > 0 && this.showUnApprovedTasks()}</div>
                </Col>
              </Row>
            </TabPane>
            <TabPane tabId="2">
              <Discussion is_report={this.state.is_report} activeTab={this.state.activeTab} />
            </TabPane>
            <TabPane tabId="3">
              <DiscussionFile />
            </TabPane>
            {(this.state.taskDetails.request_type && this.props.discussion.shippingTab && (this.state.taskDetails.request_type == 23 || this.state.taskDetails.request_type == 43)) ? (
              <>
                <TabPane tabId="4">
                  {this.props.discussion.paymentInfo.length > 0 && this.state.activeTab == 4 ?
                    <>
                      <Accordion preExpanded={['p_0']}>
                        {this.props.discussion.paymentInfo.map((valuep, indexp) => {
                          let uuid = `p_${indexp}`;
                          let total_pay = 0;
                          return (
                            <AccordionItem uuid={uuid}>
                              <AccordionItemHeading>
                                <AccordionItemButton>
                                  {this.getTranslatedTextShipping('Invoice', this.state.taskDetails.language)} #{valuep.invoice_number}
                                </AccordionItemButton>
                              </AccordionItemHeading>
                              <AccordionItemPanel>
                                <Row>
                                  <Col sm="12 ">
                                    <div className="views-row">

                                      <div className="views-field views-field-field-country">
                                        <strong className="views-label views-label-field-country">
                                          {this.state.task_language_explicit.task_details.invoice_date}
                                        </strong>
                                        <div className="field-content">
                                          {dateFormat(valuep.billing_date, 'dd-mm-yyyy')}
                                        </div>
                                      </div>
                                      <div className="views-field views-field-field-country">
                                        <strong className="views-label views-label-field-country">
                                          {this.state.task_language_explicit.task_details.customer_name}
                                        </strong>
                                        <div className="field-content">
                                          {valuep.ref_name}
                                        </div>
                                      </div>
                                      <div className="views-field views-field-field-country">
                                        <strong className="views-label views-label-field-country">
                                          {this.state.task_language_explicit.task_details.product}
                                        </strong>
                                        <div className="field-content">
                                          {valuep.product}
                                        </div>
                                      </div>
                                      <div className="views-field views-field-field-country">
                                        <strong className="views-label views-label-field-country">
                                          {this.state.task_language_explicit.task_details.currency}
                                        </strong>
                                        <div className="field-content">
                                          {valuep.currency}
                                        </div>
                                      </div>
                                      <div className="views-field views-field-field-country">
                                        <strong className="views-label views-label-field-country">
                                          {this.state.task_language_explicit.task_details.payment_status}
                                        </strong>
                                        <div className="field-content">
                                          {valuep.payment_status}
                                        </div>
                                      </div>
                                      <div className="views-field views-field-field-country">
                                        <strong className="views-label views-label-field-country">
                                          {this.state.task_language_explicit.task_details.incoterms}
                                        </strong>
                                        <div className="field-content">
                                          {valuep.incoterms}
                                        </div>
                                      </div>
                                      <div className="views-field views-field-field-country">
                                        <strong className="views-label views-label-field-country">
                                          {this.state.task_language_explicit.task_details.payment_terms}
                                        </strong>
                                        <div className="field-content">
                                          {valuep.payment_terms}
                                        </div>
                                      </div>
                                      <div className="views-field views-field-field-country">
                                        <strong className="views-label views-label-field-country">
                                        </strong>
                                        <div className="field-content">
                                        </div>
                                      </div>
                                    </div>
                                    {valuep.payment_items.length > 0 &&
                                      <div className="table-responsive-lg">
                                        <table className="orderDetailsTable table table-striped">
                                          <thead>
                                            <tr>
                                              <th>{this.state.task_language_explicit.task_details.batch_number}</th>
                                              <th>{this.state.task_language_explicit.task_details.billed_quantity}</th>
                                              <th>{this.state.task_language_explicit.task_details.net_value}</th>
                                              <th>{this.state.task_language_explicit.task_details.tax}</th>
                                              <th>{this.state.task_language_explicit.task_details.amount}</th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                            {valuep.payment_items.map((valpi, indpi) => {
                                              let amount = parseInt(valpi.tax) + parseInt(valpi.net_value);
                                              total_pay += parseInt(amount);
                                              return (
                                                <>
                                                  <tr>
                                                    <td>{valpi.batch}</td>
                                                    <td>{valpi.billed_quantity} {valpi.uom}</td>
                                                    <td>{valuep.currency} {valpi.net_value}</td>
                                                    <td>{valuep.currency} {valpi.tax}</td>
                                                    <td>{valuep.currency} {amount}</td>
                                                  </tr>
                                                </>
                                              )
                                            })}
                                            {total_pay > 0 &&
                                              <tr>
                                                <td colspan="4" style={{ textAlign: 'right' }}>{this.state.task_language_explicit.task_details.total_payable_amount}</td>
                                                <td>{valuep.currency} {total_pay}</td>
                                              </tr>
                                            }
                                          </tbody>
                                        </table>
                                      </div>
                                    }
                                  </Col>

                                </Row>
                              </AccordionItemPanel>
                            </AccordionItem>
                          )
                        })}
                      </Accordion>
                    </>
                    : ""}
                </TabPane>
              </>
            ) : ""}
            {(this.props.discussion.shippingTab && this.state.taskDetails.show_tracking == 1) ? (
              <>
                <TabPane tabId="5">

                  {/* Accordion */}
                  {this.props.discussion.shippingInfo.length > 0 && this.state.activeTab == 5 ?
                    <>
                      <Accordion preExpanded={[1]}>
                        {this.props.discussion.shippingInfo.map((value, index) => {
                          return (
                            <AccordionItem uuid={index + 1}>
                              <AccordionItemHeading>
                                <AccordionItemButton>
                                  {this.getTranslatedTextShipping('Invoice', this.state.taskDetails.language)} #{Object.keys(this.props.discussion.shippingInfo[index])}
                                </AccordionItemButton>
                              </AccordionItemHeading>
                              <AccordionItemPanel>
                                <Row>
                                  <Col sm="12">
                                    {this.props.discussion.taskDetailsFilesCoa.length > 0 &&
                                      <label className="custom-label"><strong>{this.state.task_language_explicit.task_details.coa_attachments} : </strong>
                                        {this.props.discussion.taskDetailsFilesCoa.map((valuecoa, indexcoa) => {
                                          let show_by_role = true;
                                          if (this.props.role_finance == false && valuecoa.sales_org == 1007) {
                                            show_by_role = false;
                                          }
                                          if (valuecoa.invoice_number == Object.keys(this.props.discussion.shippingInfo[index]) && show_by_role) {
                                            return (<span className="">
                                              <a href={`${valuecoa.url}?token=${this.props.login_token}`} target={'_blank'}> {valuecoa.actual_file_name}  <img src={infoIcon} width="28" height="28" /></a>
                                            </span>)
                                          }
                                        })
                                        }
                                      </label>
                                    }

                                    {this.props.discussion.shippingInfo[index][Object.keys(this.props.discussion.shippingInfo[index])].partner == 'bluedart' ? this.getBlueDartDynamicContent(index) : this.getDynamicContent(index)}
                                  </Col>
                                </Row>
                              </AccordionItemPanel>
                            </AccordionItem>
                          )
                        })}
                      </Accordion>
                    </> : ""
                  }
                  {/* /Accordion */}



                </TabPane>
              </>
            ) : ''}
            {(this.state.taskDetails.request_type && this.props.discussion.preShipmentTab && (this.state.taskDetails.request_type == 23 || this.state.taskDetails.request_type == 43)) ? (
              <>
                <TabPane tabId="6">
                  {this.state.preship_arr.length > 0 && this.state.activeTab == 6 ?
                    <>
                      <div className="table-responsive-lg">
                        <table className="table table-striped orderDetailsTable preshipTable" border="1">
                          <tbody>

                            {this.state.preship_arr.map((valpsp, indpsp) => {
                              //console.log(valpsp.approve_status);
                              return (
                                <>
                                  {(valpsp.inv_block != '' && valpsp.inv_block != null) &&
                                    <>
                                      <tr>
                                        <td colspan="2">
                                          <div className="approval_section">
                                            <span>
                                              {valpsp.invoice_file_name != '' && valpsp.invoice_file_name != null ?
                                                <>
                                                  <strong>{this.state.task_language_explicit.task_details.invoice_filename} </strong>{valpsp.invoice_file_name}
                                                </> : null
                                              }
                                            </span>
                                            {(valpsp.invoice_file_name != '' && valpsp.invoice_file_name != null) &&
                                              <span>
                                                <a href={`${ship_download}${valpsp.file_hash}/invoice?token=${this.props.login_token}&extra=trackless`} target="_blank">
                                                  <img src={infoIcon} width="28" height="28" id="invoicedownload" type="button" />
                                                </a>
                                              </span>
                                            }
                                            <span style={{ marginLeft: "15px" }}>
                                              {valpsp.packing_file_name != '' && valpsp.packing_file_name != null ?
                                                <>
                                                  <strong>{this.state.task_language_explicit.task_details.packing_filename} </strong>{valpsp.packing_file_name}
                                                </> : null
                                              }
                                            </span>
                                            {valpsp.packing_file_name != '' && valpsp.packing_file_name != null &&
                                              <span>
                                                <a href={`${ship_download}${valpsp.file_hash}/package?token=${this.props.login_token}&extra=trackless`} target="_blank">
                                                  <img src={infoIcon} width="28" height="28" id="invoicedownload" type="button" />
                                                </a>
                                              </span>
                                            }


                                            <>

                                              {valpsp.approve_status == 0 && (
                                                <>
                                                  <span>
                                                    <button className={`btn btn-success btn-approved`} onClick={() => this.approvePreShip(valpsp.invoice_number, 'approve')}>
                                                      {this.state.task_language_explicit.task_details.approve}
                                                    </button>
                                                    <button className={`btn btn-cancel btn-reject`} onClick={() => this.rejectPrforma(valpsp.invoice_number, 'invoice')}>
                                                      {this.state.task_language_explicit.task_details.reject}
                                                    </button>
                                                  </span>
                                                </>
                                              )}


                                              {valpsp.approve_status == 1 && <span className="approvedText"><img src={acceptIcon} height={25} /> {this.state.task_language_explicit.task_details.approved}</span>}
                                              {valpsp.approve_status == 2 && <span className="rejectText"><img src={rejectIcon} height={25} /> {this.state.task_language_explicit.task_details.rejected} {this.RejectReason(valpsp.reject_reason, (indpsp + 1))}</span>}
                                            </>



                                          </div>
                                        </td>
                                      </tr>
                                    </>
                                  }

                                  {/* {valpsp.coa_block != '' && valpsp.coa_block != null &&
                                    <>
                                      <tr>
                                        <td width="20%"><strong>{this.state.task_language_explicit.task_details.coa_attachments} </strong></td>
                                        <td>

                                          {valpsp.coa_files.length == 0 && <span className="approval_section"> No COA Document Found</span>}
                                          {valpsp.coa_files.length > 0 &&
                                            (valpsp.coa_files.map((valcoa, indcoa) => {
                                              console.log(valcoa.approve_status);
                                              return (
                                                <>
                                                  <span className="approval_section">
                                                    <a href={`${valcoa.url}?token=${this.props.login_token}&extra=trackless`} target={'_blank'}> {valcoa.actual_file_name}  <img src={infoIcon} width="28" height="28" /></a>
                                                    {valcoa.approve_status == 0 && (
                                                      <>
                                                        <button className={`btn btn-success btn-approved`} onClick={() => this.approvePreShipCOA(valcoa.coa_id, 'approve')}>
                                                          Approve
                                                        </button>
                                                        <button className={`btn btn-cancel btn-reject`} onClick={() => this.rejectPrforma(valcoa.coa_id, 'coa')}>
                                                          Reject
                                                        </button>
                                                      </>
                                                    )}
                                                    {valcoa.approve_status == 1 && <span className="approvedText"><img src={acceptIcon} height={25} /> Approved</span>}
                                                    {valcoa.approve_status == 2 && <span className="rejectText"><img src={rejectIcon} height={25} /> Rejected</span>}
                                                  </span>
                                                </>
                                              )
                                            }))
                                          }
                                        </td>
                                      </tr>
                                    </>
                                  } */}
                                </>
                              )
                            }
                            )}
                          </tbody>
                        </table>
                      </div>
                    </>
                    : ''}
                </TabPane>
              </>
            ) : ""}

          </TabContent>
          <div className="clearfix"></div>
        </div>
      </div>
    );
  };

  displayPayments = () => {
    return (
      <div>
        <p> {this.state.task_language_explicit.task_details.not_done} </p>
      </div>
    );
  };

  displayNotification = () => {
    return (
      <div>
        <p> {this.state.task_language_explicit.task_details.not_done} </p>
      </div>
    );
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (this.props.selCompany !== prevProps.selCompany) {
      this.props.history.push("/dashboard");
    }

    let update_comp = false;
    // ============================== //
    if (this.props.match.params.id !== prevState.task_id) {
      update_comp = true;
    } else if (this.props.match.params.id === prevState.task_id) {
      if (this.props.location.state) {
        // let currentActiveTab = this.getActiveTabs(); // use currentActiveTab variable instead of activeTab if there is any issue
        //this.props.location.state.fromDashboard === true ? "2" : "1"; // value can be true | false
        let previousActiveTab = prevState.activeTab; // value can be 1 | 2
        if (this.state.activeTab !== previousActiveTab) {
          update_comp = true;
        }
      }
    }

    if (update_comp) {
      this.props.getDiscussion(`${this.props.match.params.id}`, () => {
        if (this.props.detailsErr) {
          document.title = `Not Found | Dr. Reddy's API`;
          var errText = "Invalid Access To This Page.";
          this.setState({ invalid_access: true, showLoader: false });
          if (this.props.detailsErr.status === 5) {
            showErrorMessage(this.props.detailsErr.errors);
          }
        } else {
          let userData = BasicUserData();

          this.setState({ taskDetails: this.props.discussion.taskDetails });
          this.setState({
            taskDetailsFiles: this.props.discussion.taskDetailsFiles,
          });
          this.setState({
            discussionFiles: this.props.discussion.discussionFiles,
          });

          if (
            userData.role === 2 &&
            this.props.discussion.taskDetails.agent_id > 0
          )
            this.setState({ showBehalf: true });

          let active_stat = null;
          let push_stat = [];
          if (
            this.props.discussion.taskDetails.status_details !== null &&
            (this.props.discussion.taskDetails.request_type === 23 ||
              this.props.discussion.taskDetails.request_type === 41 ||
              this.state.taskDetails.request_type === 43)
          ) {
            active_stat = this.state.order_status.indexOf(
              this.props.discussion.taskDetails.status_details
            );
            for (let index = 0; index <= active_stat; index++) {
              push_stat.push(this.state.order_status[index]);
            }
          } else if (
            this.props.discussion.taskDetails.status_details != null &&
            (this.props.discussion.taskDetails.request_type == 7 ||
              this.state.taskDetails.request_type === 34)
          ) {
            active_stat = this.state.complaint_status.indexOf(
              this.props.discussion.taskDetails.status_details
            );
            for (let index = 0; index <= active_stat; index++) {
              push_stat.push(this.state.complaint_status[index]);
            }
          } else {
            if (this.props.discussion.taskDetails.request_type !== 24) {
              active_stat = this.state.other_status.indexOf(
                this.props.discussion.taskDetails.status_details
              );
              for (let index = 0; index <= active_stat; index++) {
                push_stat.push(this.state.other_status[index]);
              }
            }
          }
          this.setState({
            status_active: push_stat,
            // activeTab: this.getActiveTabs()
            //this.props.location.state && this.props.location.state.fromDashboard === true ? "2" : "1",
          });

        }
      });
    }
  };

  openReportPopup = () => {
    this.setState({ showReportIssue: true, is_report: true });
  };

  handleClose = () => {
    this.setState({ showReportIssue: false, is_report: false });
  };
  handleCloseReject = () => {
    this.setState({ showRejectComment: false, reject_id: '', reject_type: '' });
  }

  closeDiscussPopup = () => {
    this.setState({ showReportIssue: false, is_report: false });
  };

  handleAttachment = () => {
    this.setState({ toggleFile: !this.state.toggleFile });
  };

  setDropZoneFiles = (acceptedFiles, setErrors, setFieldValue, errors) => {
    var rejectedFiles = [];
    var uploadFile = [];

    for (var index = 0; index < acceptedFiles.length; index++) {
      var error = 0;
      var totalfile = acceptedFiles.length;
      var filename = acceptedFiles[index].name.toLowerCase();
      var extension_list = supportedFileType();
      var ext_with_dot = path.extname(filename);
      var file_extension = ext_with_dot.split(".").join("");

      var obj = {};

      var fileErrText = this.props.dynamic_lang.display_error.form_error
        .following_extensions;

      if (extension_list.indexOf(file_extension) === -1) {
        // rejectedFiles.push(acceptedFiles[index]);
        error = error + 1;

        if (totalfile > 1) {
          if (index === 0) {
            obj["errorText"] =
              this.props.dynamic_lang.display_error.form_error.error_text_1.replace(
                "[file_name]",
                filename
              ) + fileErrText;
          } else {
            obj["errorText"] =
              this.props.dynamic_lang.display_error.form_error.error_text_2.replace(
                "[file_name]",
                filename
              ) + fileErrText;
          }
        } else {
          obj["errorText"] =
            this.props.dynamic_lang.display_error.form_error.error_text_2.replace(
              "[file_name]",
              filename
            ) + fileErrText;
        }
        rejectedFiles.push(obj);
        var obj = {};
      }

      if (acceptedFiles[index].size > 50000000) {
        // rejectedFiles.push(acceptedFiles[index]);
        obj[
          "errorText"
        ] = this.props.dynamic_lang.display_error.form_error.allowed_size.replace(
          "[file_name]",
          filename
        );
        rejectedFiles.push(obj);
        error = error + 1;
      }

      if (error === 0) {
        uploadFile.push(acceptedFiles[index]);
        setErrors({ file_name: false });
      }
    }

    // //setErrors({ file_name: false });
    // errors.file_name = [];
    //setFieldValue(this.state.files);

    var prevFiles = this.state.files;
    var newFiles = [];
    if (prevFiles.length > 0) {
      for (let index = 0; index < uploadFile.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (uploadFile[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        if (remove === 0) {
          prevFiles.push(uploadFile[index]);
        }
      }

      prevFiles.map((file) => {
        file.checked = false;
        newFiles.push(file);
      });
    } else {
      uploadFile.map((file) => {
        file.checked = false;
        newFiles.push(file);
      });
    }

    this.setState({
      files: newFiles,
    });

    setFieldValue("file_name", newFiles);

    this.setState({
      rejectedFile: rejectedFiles,
    });

    this.inputRef.current.value = null; // clear input value - SATYAJIT
  };

  deleteFile = (e) => {
    e.preventDefault();
    var uploadFile = [];
    this.state.files.map((file, index) => {
      if (index.toString() === e.target.name.toString()) {
      } else {
        uploadFile.push(file);
      }
    });
    this.setState({ files: uploadFile });
  };

  //MULTER ATTACHMENT AMAN
  handleSubmitAttachments = () => {
    //e.preventDefault();

    if ((this.state.email_list && this.state.email_list.length > 0 && this.state.send_mail_subject != '') || (this.state.entered_email != '' && this.validateEmail(this.state.entered_email) && this.state.send_mail_subject != '')) {

      let email_list;

      if (this.state.email_list && this.state.email_list.length > 0) {
        email_list = this.state.email_list;
      } else {
        email_list = [this.state.entered_email];
      }
      var formData = new FormData();
      formData.append("task_attachments", JSON.stringify(this.state.attachmentList));
      formData.append("email_list", JSON.stringify(email_list));
      formData.append("subject", this.state.send_mail_subject);
      formData.append("notes", this.state.send_mail_notes);
      formData.append("exclude_task_ref", this.state.exclude_task_ref);
      if (this.state.files && this.state.files.length > 0) {
        for (let index = 0; index < this.state.files.length; index++) {
          formData.append("file", this.state.files[index]);
        }
      } else {
        formData.append("file", []);
      }

      // axios
      //   .post(`tasks/send-mail-task-details-attachments/${this.state.task_id}`, {
      //     task_attachments: this.state.attachmentList,
      //     email_list: email_list,
      //     subject: this.state.send_mail_subject,
      //     notes: this.state.send_mail_notes,
      //     exclude_task_ref: this.state.exclude_task_ref
      //   })
      axios
        .post(`tasks/send-mail-task-details-attachments/${this.state.task_id}`, formData)
        .then((res) => {
          this.handleSendMailClose();
          this.setState({ dynamic_success_message: this.state.task_language_explicit.task_details.mail_sent, isSuccess: true });
        })
        .catch((err) => {
          if (err && err.data && err.data.status && err.data.status == 5) {
            this.setState({ attachment_error: err.data.errors });
          }
        });
    } else {
      if ((this.state.email_list && this.state.email_list.length == 0)) {
        this.setState({ email_error: this.state.task_language_explicit.task_details.email });
      } else if (this.state.entered_email == '' && !this.validateEmail(this.state.entered_email)) {
        this.setState({ email_error: this.state.task_language_explicit.task_details.valid_email });
      }

      if (this.state.send_mail_subject == '') {
        this.setState({ subject_error: this.state.task_language_explicit.task_details.subject_err });
      }

    }
  };

  handleSubmitEscalation = (values, actions) => {

    var formData = new FormData();
    if (this.state.files && this.state.files.length > 0) {
      for (let index = 0; index < this.state.files.length; index++) {
        formData.append("file", this.state.files[index]);
      }
    } else {
      formData.append("file", []);
    }
    formData.append("content", values.comment);

    axios
      .post(`tasks/send-mail-escalation/${this.state.task_id}`, formData)
      .then((res) => {
        this.handleSendMailEscalationClose();
        this.setState({ dynamic_success_message: this.state.task_language_explicit.task_details.mail_sent, isSuccess: true });
      })
      .catch((err) => {
        if (err && err.data && err.data.status && err.data.status == 5) {
          this.setState({ escalation_error: err.data.errors });
        }
        if (err && err.data && err.data.status && err.data.status == 2) {

          if (this.props.discussion.taskDetails.language === 'en') {
            this.setState({ escalation_error: 'Please enter comment.' });
          } else if (this.props.discussion.taskDetails.language === 'zh') {
            this.setState({ escalation_error: '请输入评论。' });
          } else if (this.props.discussion.taskDetails.language === 'ja') {
            this.setState({ escalation_error: 'コメントを入力してください。' });
          } else if (this.props.discussion.taskDetails.language === 'pt') {
            this.setState({ escalation_error: 'Por favor, insira um comentário.' });
          } else if (this.props.discussion.taskDetails.language === 'es') {
            this.setState({ escalation_error: 'Introduzca un comentario.' });
          }
          //this.setState({ escalation_error: err.data.errors.content });
        }
      });
  };

  handleSubmitRDD = (values, actions) => {
    this.setState({ RDDShow: true });
    if (dateFormat(this.state.edit_rdd, "yyyy-mm-dd") == dateFormat(this.state.new_rdd, "yyyy-mm-dd")) {
      this.setState({ rdd_error: this.state.task_language_explicit.task_details.rdd_same, RDDShow: false });
    } else if (this.state.new_rdd < new Date()) {
      this.setState({ rdd_error: this.state.task_language_explicit.task_details.rdd_less, RDDShow: false });
    } else {
      let formData = { rdd: dateFormat(this.state.new_rdd, "yyyy-mm-dd"), temp_id: this.state.edit_temp_id };

      axios
        .post(`tasks/edit-rdd/${this.state.task_id}`, formData)
        .then((res) => {
          this.handleRDDPopupClose();

          if (this.state.taskDetails.request_type === 23 && this.state.taskDetails.order_verified_status === 1) {
            this.setState({ dynamic_success_message: this.state.task_language_explicit.task_details.rdd_update_approve_pending, isSuccess: true, rdd_pending: true });

          } else {
            this.setState({ dynamic_success_message: this.state.task_language_explicit.task_details.rdd_update, isSuccess: true, not_approved_tasks: res.data.data });
          }

        })
        .catch((err) => {
          if (err && err.data && err.data.status && err.data.status == 5) {
            this.setState({ escalation_error: err.data.errors, RDDShow: false });
          }
        });
    }
  };

  enterEmailAddress = (event) => {
    event.preventDefault();
    this.setState({ entered_email: event.target.value, email_error: '' });
  }

  validateEmail = (email) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  addEmail = (e) => {
    if (e.key === 'Enter' || e.key === ',' || e.key === ';') {
      //console.log('key pressed',e.key);
      e.preventDefault();
      if (this.state.entered_email != '' && this.validateEmail(this.state.entered_email)) {
        e.target.value = '';
        let prev_emails = this.state.email_list;
        prev_emails.push(this.state.entered_email);
        this.setState({ email_list: prev_emails, entered_email: '' });
      } else {
        this.setState({ email_error: this.state.task_language_explicit.task_details.valid_email });
      }

    }
  }

  removeEmails = (email, objRef) => {
    let prev_arr = objRef.state.email_list;
    let filetered = prev_arr.filter((value, index, arr) => { return value != email; });
    objRef.setState({ email_list: filetered });
  }

  getEmailList = () => {
    var html = this.state.email_list.map((email, index) => (
      <Alert color="secondary" key={email} style={(index == 0) ? { marginTop: '5px', marginBottom: '5px' } : { marginBottom: '5px' }} >
        <img src={closeIcon} onClick={() => this.removeEmails(email, this)} height={20} style={{ pointer: 'cursor' }} />
        {`     `}
        {/* <button className="close" onClick={() => this.removeEmails(email, this)} /> */}
        {email}
      </Alert>
    ));

    return html;
  }

  hideSuccessMsg = () => {
    this.setState({ isSuccess: false, dynamic_success_message: '' });
  };

  removeFile_Error = () => {
    this.setState({ rejectedFile: [] });
  };

  closeTour = () => {
    this.setState({ open_tour: false });
  };

  closeTourNew = () => {
    this.setState({ open_tour_new: false });
  };

  disableSendMailNewFeature = (event) => {
    event.preventDefault();
    axios
      .get(`tasks/tour_done/${this.state.task_id}`, {})
      .then((res) => {
        axios
          .get(`tasks/tour_done_11/${this.state.task_id}`, {})
          .then((res2) => {
            axios
              .get("/generate_token")
              .then(response => {
                this.props.updateTourToken(response.data.token)
              })
              .catch(error => { });
          })
          .catch((err) => { });
      })
      .catch((err) => { });
  }

  disableNewTour = (event) => {
    event.preventDefault();
    axios
      .get(`tasks/tour_done_new/${this.state.task_id}`, {})
      .then((res) => {

        if (this.props.tour_counter == true
          // || (this.props.tour_counter_11 == true && (this.props.discussion.shippingTab && this.props.discussion.shippingInfo.length>0))
        ) {
          axios
            .get(`tasks/tour_done/${this.state.task_id}`, {})
            .then((res) => {
              // axios
              // .get(`tasks/tour_done_11/${this.state.task_id}`, {})
              // .then((res2) => {
              axios
                .get("/generate_token")
                .then(response => {
                  this.props.updateTourToken(response.data.token)
                })
                .catch(error => { });
              // })
              // .catch((err) => { });
            })
            .catch((err) => { });
        } else {
          axios
            .get("/generate_token")
            .then(response => {
              this.props.updateTourToken(response.data.token)
            })
            .catch(error => { });
        }
      })
      .catch((err) => { });

  }

  render() {
    if (this.state.isLoading === true || this.state.task_language_explicit === '') {
      return (
        <>
          <div className="loginLoader">
            <div className="lds-spinner">
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <div></div>
              <span>Please Wait…</span>
            </div>
          </div>
        </>
      );
    } else {
      if (this.state.showLoader) {
        return <Loader />;
        // return <h2>Loading....</h2>
      } else {
        var tour_steps = [];

        if (this.state.taskDetails.close_status == 1) {

          if (this.props.tour_counter == true && this.props.tour_counter_new == true) {

            // if(this.props.tour_counter_11 == true && (this.props.discussion.shippingTab && this.props.discussion.shippingInfo.length>0)){
            //   tour_steps.push({
            //     selector: '#feature-step-11',
            //     content: ({ goTo, inDOM }) => (
            //       <div>
            //        {this.state.task_language_explicit.task_details.ship_block}
            //         <br />
            //         <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => goTo(1)} >{this.state.task_language_explicit.task_details.ok_got}</button>

            //       </div>
            //     )
            //   });

            //   tour_steps.push({
            //     selector: '#first-step',
            //     content: ({ goTo, inDOM }) => (
            //       <div>
            //         {this.state.task_language_explicit.task_details.send_drl}
            //         <br />
            //         <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={() => goTo(2)} >{this.state.task_language_explicit.task_details.ok_got}</button>
            //       </div>
            //     )
            //   });

            //   tour_steps.push({
            //     selector: '#second-step',
            //     content: ({ goTo, inDOM }) => (
            //       <div>
            //         {this.state.task_language_explicit.task_details.escalate_drl}
            //         <br />
            //         <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => goTo(3)}>{this.state.task_language_explicit.task_details.ok_got}</button>

            //       </div>
            //     )
            //   });

            // }else{
            tour_steps.push({
              selector: '#first-step',
              content: ({ goTo, inDOM }) => (
                <div>
                  {this.state.task_language_explicit.task_details.send_drl}
                  <br />
                  <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={() => goTo(1)} >{this.state.task_language_explicit.task_details.ok_got}</button>
                </div>
              )
            });
            tour_steps.push({
              selector: '#second-step',
              content: ({ goTo, inDOM }) => (
                <div>
                  {this.state.task_language_explicit.task_details.escalate_drl}
                  <br />
                  <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => goTo(2)}>{this.state.task_language_explicit.task_details.ok_got}</button>

                </div>
              )
            });
            //}

            tour_steps.push({
              selector: '#new-step',
              content: ({ goTo, inDOM }) => (
                <div>
                  {this.state.task_language_explicit.task_details.shipblock}
                  <br />
                  <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => this.disableNewTour(e)} >{this.state.task_language_explicit.task_details.ok_got}</button>

                </div>
              )
            });

          } else if (this.props.tour_counter == true && this.props.tour_counter_new == false) {

            // if(this.props.tour_counter_11 == true && (this.props.discussion.shippingTab && this.props.discussion.shippingInfo.length>0)){
            //   tour_steps.push({
            //     selector: '#feature-step-11',
            //     content: ({ goTo, inDOM }) => (
            //       <div>
            //        {this.state.task_language_explicit.task_details.ship_block}
            //         <br />
            //         <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => goTo(1)} >{this.state.task_language_explicit.task_details.ok_got}</button>

            //       </div>
            //     )
            //   });

            //   tour_steps.push({
            //     selector: '#first-step',
            //     content: ({ goTo, inDOM }) => (
            //       <div>
            //         {this.state.task_language_explicit.task_details.send_drl}
            //         <br />
            //         <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={() => goTo(2)} >{this.state.task_language_explicit.task_details.ok_got}</button>
            //       </div>
            //     )
            //   });

            // }else{
            tour_steps.push({
              selector: '#first-step',
              content: ({ goTo, inDOM }) => (
                <div>
                  {this.state.task_language_explicit.task_details.send_drl}
                  <br />
                  <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={() => goTo(1)} >{this.state.task_language_explicit.task_details.ok_got}</button>
                </div>
              )
            });
            //}

            tour_steps.push({
              selector: '#second-step',
              content: ({ goTo, inDOM }) => (
                <div>
                  {this.state.task_language_explicit.task_details.escalate_drl}
                  <br />
                  <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => this.disableSendMailNewFeature(e)}>{this.state.task_language_explicit.task_details.ok_got}</button>

                </div>
              )
            });
          } else if (this.props.tour_counter == false && this.props.tour_counter_new == true) {

            // if(this.props.tour_counter_11 == true && (this.props.discussion.shippingTab && this.props.discussion.shippingInfo.length>0)){
            //   tour_steps.push({
            //     selector: '#feature-step-11',
            //     content: ({ goTo, inDOM }) => (
            //       <div>
            //        {this.state.task_language_explicit.task_details.ship_block}
            //         <br />
            //         <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => goTo(1)} >{this.state.task_language_explicit.task_details.ok_got}</button>

            //       </div>
            //     )
            //   });
            // }

            tour_steps.push({
              selector: '#new-step',
              content: ({ goTo, inDOM }) => (
                <div>
                  You can post follow-up comments on closed requests here.
                  <br />
                  <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => this.disableNewTour(e)} >{this.state.task_language_explicit.task_details.ok_got}</button>

                </div>
              )
            });
          } else {
            // if(this.props.tour_counter_11 == true && (this.props.discussion.shippingTab && this.props.discussion.shippingInfo.length>0)){
            //   tour_steps.push({
            //     selector: '#feature-step-11',
            //     content: ({ goTo, inDOM }) => (
            //       <div>
            //        {this.state.task_language_explicit.task_details.ship_block}
            //         <br />
            //         <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => this.disableNewTour(e)} >{this.state.task_language_explicit.task_details.ok_got}</button>

            //       </div>
            //     )
            //   });
            // }
          }
        } else {
          if (this.props.tour_counter == true) {

            // if(this.props.tour_counter_11 == true && (this.props.discussion.shippingTab && this.props.discussion.shippingInfo.length>0)){
            //   tour_steps.push({
            //     selector: '#feature-step-11',
            //     content: ({ goTo, inDOM }) => (
            //       <div>
            //        {this.state.task_language_explicit.task_details.ship_block}
            //         <br />
            //         <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => goTo(1)} >{this.state.task_language_explicit.task_details.ok_got}</button>

            //       </div>
            //     )
            //   });

            //   tour_steps.push({
            //     selector: '#first-step',
            //     content: ({ goTo, inDOM }) => (
            //       <div>

            //         {this.state.task_language_explicit.task_details.send_drl}
            //         <br />
            //         <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={() => goTo(2)} >{this.state.task_language_explicit.task_details.ok_got}</button>

            //       </div>
            //     )
            //   });

            // }else{
            tour_steps.push({
              selector: '#first-step',
              content: ({ goTo, inDOM }) => (
                <div>

                  {this.state.task_language_explicit.task_details.send_drl}
                  <br />
                  <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={() => goTo(1)} >{this.state.task_language_explicit.task_details.ok_got}</button>

                </div>
              )
            });
            //}

            tour_steps.push({
              selector: '#second-step',
              content: ({ goTo, inDOM }) => (
                <div>
                  {this.state.task_language_explicit.task_details.escalate_drl}
                  <br />
                  <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => this.disableSendMailNewFeature(e)}>{this.state.task_language_explicit.task_details.ok_got}</button>

                </div>
              )
            });
          } else {

            // if(this.props.tour_counter_11 == true && (this.props.discussion.shippingTab && this.props.discussion.shippingInfo.length>0)){
            //   tour_steps.push({
            //     selector: '#feature-step-11',
            //     content: ({ goTo, inDOM }) => (
            //       <div>
            //        {this.state.task_language_explicit.task_details.ship_block}
            //         <br />
            //         <button className="btn btn-default" style={{ margin: '0px 0px 0px 0px', height: '36px', width: '135px' }} onClick={(e) => this.disableSendMailNewFeature(e)} >{this.state.task_language_explicit.task_details.ok_got}</button>

            //       </div>
            //     )
            //   });
            // }
          }
        }


        return (
          <>
            {(typeof this.props.new_feature_send_mail == 'undefined') && this.props.disconnectUserReload('', () => {
              window.location.reload();
            })}
            {tour_steps.length > 0 && <Tour
              steps={tour_steps}
              isOpen={this.state.open_tour}
              showNavigation={false}
              showButtons={false}
              onRequestClose={this.closeTour}
              onBeforeClose={target => (document.body.style.overflowY = 'auto')}
              onAfterOpen={target => (document.body.style.overflowY = 'auto')}
            />}

            {/* {this.props.tour_counter_new == true && this.state.taskDetails.close_status == 1 && <Tour
                steps={tour_steps_new}
                isOpen={this.state.open_tour_new}
                showNavigation={false}
                showButtons={false}
                onRequestClose={this.closeTourNew}
                onBeforeClose={target => (document.body.style.overflowY = 'auto')}
                onAfterOpen={target => (document.body.style.overflowY = 'auto')}
            />} */}

            <div className="container-fluid clearfix taskDetailsSec">

              <div className="dashboard-content-sec">
                <div className="product-content-sec">
                  <div className="row breadcrumb-area">
                    <div className="col-12 col-sm-6">
                      <div className="breadcrumb">
                        <ul>
                          <li>
                            {" "}
                            <Link to="/">
                              {this.state.task_language_explicit.task_details.home}
                            </Link>{" "}
                          </li>
                          <li>
                            {" "}
                            {this.state.task_language_explicit.task_details.dashboard}{" "}
                          </li>
                          {this.state.taskDetails.request_type === 7 ||
                            this.state.taskDetails.request_type === 34 ? (
                            <li>
                              <Link to="/my_complaints">
                                {this.state.task_language_explicit.task_details.complaints}
                              </Link>{" "}
                            </li>
                          ) : this.state.taskDetails.request_type === 23 ||
                            this.state.taskDetails.request_type === 41 ||
                            this.state.taskDetails.request_type === 43 ? (
                            <li>
                              <Link to="/my_orders">
                                {this.state.task_language_explicit.task_details.orders}
                              </Link>{" "}
                            </li>
                          ) : this.state.taskDetails.request_type === 24 ? (
                            <li>
                              <Link to="/my_forecast">
                                {this.state.task_language_explicit.task_details.forecast}
                              </Link>{" "}
                            </li>
                          ) : (
                            <li>
                              <Link to="/my_requests">
                                {this.state.task_language_explicit.task_details.request}
                              </Link>{" "}
                            </li>
                          )}
                          <li> {this.state.taskDetails.task_ref} </li>
                        </ul>
                      </div>
                    </div>
                    <div className="col-12 col-sm-6 float-right">
                      <p className="text-right">

                        <Link
                          to="#"
                          id="first-step"
                          className="back-btn"
                          onClick={() => this.getAttchmentsSendMail()}
                        >
                          {/* Send Mail */}{this.state.task_language_explicit.task_details.send_mail}
                        </Link>

                        <Link
                          to="#"
                          id="second-step"
                          className="back-btn"
                          onClick={() => this.showEscalationPopup()}
                        >
                          {/* Escalation */}{this.state.task_language_explicit.task_details.escalation}
                        </Link>

                        {this.state.taskDetails.duplicate_task !== 1 && (
                          <Link
                            to="#"
                            className="back-btn"
                            onClick={() => this.openReportPopup()}
                          >
                            {this.state.task_language_explicit.task_details.cancel_request}
                          </Link>
                        )}

                        {this.state.taskDetails.request_type === 7 ||
                          this.state.taskDetails.request_type === 34 ? (
                          <Link
                            className="back-btn"
                            to={{ pathname: `/my_complaints` }}
                            style={{ cursor: "pointer" }}
                            title={
                              this.state.task_language_explicit.task_details
                                .previous_page_viewed
                            }
                          >
                            {" "}
                            {this.state.task_language_explicit.task_details.back}{" "}
                          </Link>
                        ) : this.state.taskDetails.request_type === 23 ||
                          this.state.taskDetails.request_type === 41 ||
                          this.state.taskDetails.request_type === 43 ? (
                          <Link
                            className="back-btn"
                            to={{ pathname: `/my_orders` }}
                            style={{ cursor: "pointer" }}
                            title={
                              this.state.task_language_explicit.task_details
                                .previous_page_viewed
                            }
                          >
                            {" "}
                            {this.state.task_language_explicit.task_details.back}{" "}
                          </Link>
                        ) : this.state.taskDetails.request_type === 24 ? (
                          <Link
                            className="back-btn"
                            to={{ pathname: `/my_forecast` }}
                            style={{ cursor: "pointer" }}
                            title={
                              this.state.task_language_explicit.task_details
                                .previous_page_viewed
                            }
                          >
                            {" "}
                            {this.state.task_language_explicit.task_details.back}{" "}
                          </Link>
                        ) : (
                          <Link
                            className="back-btn"
                            to={{ pathname: `/my_requests` }}
                            style={{ cursor: "pointer" }}
                            title={
                              this.state.task_language_explicit.task_details
                                .previous_page_viewed
                            }
                          >
                            {" "}
                            {this.state.task_language_explicit.task_details.back}{" "}
                          </Link>
                        )}
                      </p>
                    </div>
                    <div className="clearfix"></div>

                    <div
                      className="messagessuccess"
                      style={{
                        display: this.state.isSuccess ? "block" : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIconSuccess}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.hideSuccessMsg()}
                        />
                      </Link>
                      {this.state.isSuccess ? (
                        <div>
                          {
                            this.state.dynamic_success_message
                          }
                        </div>
                      ) : null}
                    </div>

                    {/* status_active */}

                    {(this.state.taskDetails.request_type === 23 ||
                      this.state.taskDetails.request_type === 41) &&
                      this.state.taskDetails.status_details != null && (
                        <ul className="progressbar">
                          {this.state.order_status
                            ? this.state.order_status.map((player, i) => {
                              return (
                                <li
                                  key={i}
                                  className={
                                    this.state.status_active &&
                                      this.state.status_active.length > 0 &&
                                      this.state.status_active.includes(player)
                                      ? "active"
                                      : ""
                                  }
                                >
                                  {player}
                                </li>
                              );
                              // -------------------^^^^^^^^^^^---------^^^^^^^^^^^^^^
                            })
                            : ""}
                        </ul>
                      )}

                    {/* {(this.state.taskDetails.request_type === 41) && this.state.taskDetails.request_type !== 24 && this.state.taskDetails.status_details != null &&
                          <ul className="progressbar">
                          {this.state.other_status ? this.state.other_status.map((player,i) => {
                            
                            return <li key={i} className={(this.state.status_active && this.state.status_active.length > 0 && this.state.status_active.includes(player)) ? 'active' : '' }>{player}</li>
                            // -------------------^^^^^^^^^^^---------^^^^^^^^^^^^^^
                          }) : ''}
                          </ul>
                        } */}
                    {(this.state.taskDetails.request_type === 7 ||
                      this.state.taskDetails.request_type === 34) &&
                      this.state.taskDetails.status_details != null && (
                        <ul className="progressbar">
                          {this.state.complaint_status
                            ? this.state.complaint_status.map((player, i) => {
                              return (
                                <li
                                  key={i}
                                  className={
                                    this.state.status_active &&
                                      this.state.status_active.length > 0 &&
                                      this.state.status_active.includes(player)
                                      ? "active"
                                      : ""
                                  }
                                >
                                  {player}
                                </li>
                              );
                              // -------------------^^^^^^^^^^^---------^^^^^^^^^^^^^^
                            })
                            : ""}
                        </ul>
                      )}

                    {this.state.taskDetails.request_type !== 7 &&
                      this.state.taskDetails.request_type !== 34 &&
                      this.state.taskDetails.request_type !== 23 &&
                      this.state.taskDetails.request_type !== 43 &&
                      this.state.taskDetails.request_type !== 41 &&
                      this.state.taskDetails.request_type !== 24 &&
                      this.state.taskDetails.status_details != null && (
                        <ul className="progressbar">
                          {this.state.other_status
                            ? this.state.other_status.map((player, i) => {
                              return (
                                <li
                                  key={i}
                                  className={
                                    this.state.status_active &&
                                      this.state.status_active.length > 0 &&
                                      this.state.status_active.includes(player)
                                      ? "active"
                                      : ""
                                  }
                                >
                                  {player}
                                </li>
                              );
                              // -------------------^^^^^^^^^^^---------^^^^^^^^^^^^^^
                            })
                            : ""}
                        </ul>
                      )}

                    {/* player ==='Acknowledge' ? 'Acknowledged' : player */}
                  </div>

                  {(this.state.taskDetails.request_type === 2 ||
                    this.state.taskDetails.request_type === 3 ||
                    this.state.taskDetails.request_type === 4 ||
                    this.state.taskDetails.request_type === 5 ||
                    this.state.taskDetails.request_type === 6 ||
                    this.state.taskDetails.request_type === 7 ||
                    this.state.taskDetails.request_type === 8 ||
                    this.state.taskDetails.request_type === 9 ||
                    this.state.taskDetails.request_type === 10 ||
                    this.state.taskDetails.request_type === 11 ||
                    this.state.taskDetails.request_type === 12 ||
                    this.state.taskDetails.request_type === 13 ||
                    this.state.taskDetails.request_type === 14 ||
                    this.state.taskDetails.request_type === 15 ||
                    this.state.taskDetails.request_type === 16 ||
                    this.state.taskDetails.request_type === 17 ||
                    this.state.taskDetails.request_type === 18 ||
                    this.state.taskDetails.request_type === 19 ||
                    this.state.taskDetails.request_type === 20 ||
                    this.state.taskDetails.request_type === 21 ||
                    this.state.taskDetails.request_type === 22 ||
                    this.state.taskDetails.request_type === 23 ||
                    this.state.taskDetails.request_type === 24 ||
                    this.state.taskDetails.request_type === 1 ||
                    this.state.taskDetails.request_type === 27 ||
                    this.state.taskDetails.request_type === 28 ||
                    this.state.taskDetails.request_type === 29 ||
                    this.state.taskDetails.request_type === 30 ||
                    this.state.taskDetails.request_type === 31 ||
                    this.state.taskDetails.request_type === 32 ||
                    this.state.taskDetails.request_type === 33 ||
                    this.state.taskDetails.request_type === 34 ||
                    this.state.taskDetails.request_type === 35 ||
                    this.state.taskDetails.request_type === 36 ||
                    this.state.taskDetails.request_type === 37 ||
                    this.state.taskDetails.request_type === 38 ||
                    this.state.taskDetails.request_type === 39 ||
                    this.state.taskDetails.request_type === 40 ||
                    this.state.taskDetails.request_type === 41 ||
                    this.state.taskDetails.request_type === 44 ||
                    this.state.taskDetails.request_type === 42 ||
                    this.state.taskDetails.request_type === 43) &&
                    this.displayRequestDetails()}

                  {this.state.taskDetails.request_type === 25
                    ? this.displayPayments()
                    : ""}
                  {this.state.taskDetails.request_type === 26
                    ? this.displayNotification()
                    : ""}
                </div>
              </div>

              {/* report an issue */}
              {this.state.showReportIssue === true && (
                <Modal
                  show={this.state.showReportIssue}
                  onHide={this.handleClose}
                  backdrop="static"
                  className="grantmodal canclpopup"
                >
                  <Formik
                    initialValues={false}
                    validationSchema={false}
                    onSubmit={this.handleSubmitAssignTask}
                  >
                    {({
                      values,
                      errors,
                      touched,
                      isValid,
                      isSubmitting,
                      handleChange,
                      setFieldValue,
                      setFieldTouched,
                      setErrors,
                    }) => {
                      return (
                        <>
                          <Modal.Header closeButton>
                            <Modal.Title>
                              <span className="fieldset-legend">
                                {
                                  this.state.task_language_explicit.task_details
                                    .cancel_request
                                }
                              </span>
                            </Modal.Title>
                          </Modal.Header>
                          <Modal.Body>
                            <Discussion
                              is_report={this.state.is_report}
                              closeDiscussPopup={this.closeDiscussPopup}
                            />
                          </Modal.Body>
                          {/* <Modal.Footer></Modal.Footer> */}
                        </>
                      );
                    }}
                  </Formik>
                </Modal>
              )}
              {/* report an issue */}

              {this.state.showSendMailPopup === true && (
                <Modal
                  show={this.state.showSendMailPopup}
                  onHide={this.handleSendMailClose}
                  backdrop="static"
                  className="grantmodal sendMailModal"
                >
                  <Formik
                    initialValues={false}
                    validationSchema={false}
                    onSubmit={this.handleSubmitAttachments}
                  >
                    {({
                      values,
                      errors,
                      touched,
                      isValid,
                      isSubmitting,
                      handleChange,
                      setFieldValue,
                      setFieldTouched,
                      setErrors,
                    }) => {
                      return (
                        <Form>
                          <Modal.Header>
                            <Modal.Title>
                              {this.state.task_language_explicit.task_details.send_mail}
                            </Modal.Title>
                            <button className="close" onClick={this.handleSendMailClose} />
                          </Modal.Header>
                          <Modal.Body>
                            <div>

                              <Row>
                                <ul>
                                  <li>
                                    <input type="checkbox" className="" onChange={(e) => { this.setState({ exclude_task_ref: !this.state.exclude_task_ref }); }} checked={this.state.exclude_task_ref} />{` `}{this.state.task_language_explicit.task_details.ex_task_ref}
                                  </li>
                                  <li>
                                    <label>{this.state.task_language_explicit.task_details.subject}</label>
                                    <input type="text" className="form-control" onChange={(e) => { this.setState({ send_mail_subject: e.target.value }); }} onFocus={() => { this.setState({ subject_error: '' }) }} />
                                    {this.state.subject_error != '' && <p style={{ color: 'red' }} >{this.state.subject_error}</p>}
                                  </li>
                                  <li>
                                    <label>{this.state.task_language_explicit.task_details.email_addr}: <i>({this.state.task_language_explicit.task_details.email_press})</i></label>
                                    <input type="text" className="form-control" onKeyPress={(e) => this.addEmail(e)} onChange={(e) => this.enterEmailAddress(e)} />
                                    {this.state.email_list && this.state.email_list.length > 0 && this.getEmailList()}
                                    {this.state.email_error != '' && <p style={{ color: 'red' }} >{this.state.email_error}</p>}
                                  </li>
                                  <li>
                                    <label>{this.state.task_language_explicit.task_details.notes}</label>
                                    <br />
                                    <textarea onChange={(e) => { this.setState({ send_mail_notes: e.target.value }); }} style={{ width: '100%' }} ></textarea>
                                  </li>
                                  <li>
                                    <label>{(this.state.attachmentList !== "" && (this.state.attachmentList[0].task_discussion_files.length > 0 || this.state.attachmentList[0].task_files.length) > 0) ? `${this.state.task_language_explicit.task_details.attachments}:` : this.state.task_language_explicit.task_details.no_attachments}</label>
                                    {this.state.attachmentList && this.makeDynamicHtml()}
                                    {this.state.files.length > 0 && (
                                      <table
                                        className="responsive-enabled"
                                        data-striping="1"
                                      >
                                        <thead>
                                          <tr>
                                            <th>
                                              {
                                                this.state.task_language_explicit.task_details
                                                  .file_information
                                              }
                                            </th>
                                            <th>
                                              {
                                                this.state.task_language_explicit.task_details
                                                  .operations
                                              }
                                            </th>
                                          </tr>
                                        </thead>

                                        {this.state.files &&
                                          this.state.files.map((file, index) => {
                                            return (
                                              <tbody key={index}>
                                                <tr className="draggable">
                                                  <td>
                                                    <a
                                                      href="#"
                                                      className="tabledrag-handle"
                                                      title={
                                                        this.state.task_language_explicit
                                                          .task_details.drag_to_re_order
                                                      }
                                                    />
                                                    <div className=" form-managed-file">
                                                      <span className="file file-image">
                                                        <Link
                                                          to="#"
                                                          // type="image/jpeg;"
                                                          className="menu-item-link"
                                                        >
                                                          {file.name}
                                                        </Link>
                                                      </span>
                                                    </div>
                                                  </td>
                                                  <td>
                                                    <input
                                                      name={index}
                                                      value={
                                                        this.state.task_language_explicit
                                                          .task_details.remove
                                                      }
                                                      className="button form-submit"
                                                      readOnly={true}
                                                      onClick={(e) =>
                                                        this.deleteFile(e)
                                                      }
                                                    />
                                                  </td>
                                                </tr>
                                              </tbody>
                                            );
                                          })}
                                      </table>
                                    )}

                                    {this.state.attachment_error != '' && <p style={{ color: 'red' }} >{this.state.attachment_error}</p>}
                                    <div
                                      className="messageserror"
                                      style={{
                                        display:
                                          (this.state.rejectedFile &&
                                            this.state.rejectedFile.length) > 0
                                            ? "block"
                                            : "none",
                                      }}
                                    >
                                      <Link to="#" className="close">
                                        <img
                                          src={closeIcon}
                                          width="17.69px"
                                          height="22px"
                                          onClick={(e) => this.removeFile_Error()}
                                        />
                                      </Link>
                                      {this.state.rejectedFile &&
                                        this.state.rejectedFile.map((file, index) => {
                                          return (
                                            <div key={index}>
                                              <ul className="">
                                                <li className="">{file.errorText}</li>
                                              </ul>
                                            </div>
                                          );
                                        })}
                                    </div>
                                  </li>
                                  <li>
                                    {/*MULTER ATTACHMENT AMAN*/}
                                    <Dropzone
                                      //accept = 'image/jpeg, image/png'
                                      onDrop={(acceptedFiles) =>
                                        this.setDropZoneFiles(
                                          acceptedFiles,
                                          setErrors,
                                          setFieldValue
                                        )
                                      }
                                    >
                                      {({ getRootProps, getInputProps }) => (
                                        <section>
                                          <div {...getRootProps()} className="customFile">
                                            <input
                                              {...getInputProps()}
                                              ref={this.inputRef}
                                              style={{ display: "block" }}
                                            />
                                          </div>
                                        </section>
                                      )}
                                    </Dropzone>

                                  </li>

                                </ul>
                              </Row>

                            </div>

                            <ButtonToolbar>

                              <Button
                                className={`btn btn-default`}
                                type="submit"
                              //disabled={isValid ? false : true}
                              >
                                {this.state.task_language_explicit.task_details.send_mail}
                              </Button>

                              <Button
                                className={`btn btn-default btn-cancel`}
                                type="button"
                                onClick={this.handleSendMailClose}
                              >
                                {this.state.task_language_explicit.task_details.cancel}
                              </Button>

                            </ButtonToolbar>
                          </Modal.Body>
                          {/* <Modal.Footer>
                            
                          </Modal.Footer> */}
                        </Form>
                      );
                    }}
                  </Formik>
                </Modal>
              )}

              {this.state.showSendMailEscalationPopup === true && (
                <Modal
                  show={this.state.showSendMailEscalationPopup}
                  onHide={this.handleSendMailEscalationClose}
                  backdrop="static"
                  className="grantmodal"
                >
                  <Formik
                    initialValues={initialValues}
                    validationSchema={false}
                    onSubmit={this.handleSubmitEscalation}
                  >
                    {({
                      values,
                      errors,
                      touched,
                      isValid,
                      isSubmitting,
                      handleChange,
                      setFieldValue,
                      setFieldTouched,
                      setErrors,
                    }) => {
                      return (
                        <Form>
                          <Modal.Header>
                            <Modal.Title>
                              {/* Escalation Email */}{this.state.task_language_explicit.task_details.escalation_email}
                            </Modal.Title>
                            <button className="close" onClick={this.handleSendMailEscalationClose} />
                          </Modal.Header>
                          <Modal.Body>
                            <div>
                              <Row>
                                <Editor
                                  name="comment"
                                  content={values.comment}
                                  init={{
                                    menubar: false,
                                    branding: false,
                                    placeholder: this.state.is_report
                                      ? this.state.task_language_explicit.task_details
                                        .reason_for_cancellation
                                      : this.state.task_language_explicit.task_details
                                        .enter_comments,
                                    plugins:
                                      "link table hr visualblocks code placeholder lists",
                                    toolbar:
                                      "bold italic strikethrough superscript subscript | alignleft aligncenter alignright alignjustify | numlist bullist | removeformat underline | link unlink | blockquote table  hr | visualblocks code ",
                                    content_css: ["/css/editor.css"],
                                  }}
                                  onEditorChange={(value) => {
                                    setFieldValue("comment", value);
                                    this.setState({
                                      comment: value
                                    });
                                  }}
                                />

                                <div className="field-attachment">
                                  <p
                                    className={
                                      this.state.toggleFile === true
                                        ? "attahRight"
                                        : "attahDown"
                                    }
                                    onClick={() => this.handleAttachment()}
                                  >
                                    {this.state.task_language_explicit.task_details.attachment}
                                  </p>
                                </div>

                                {this.state.toggleFile === false &&
                                  this.state.files.length > 0 && (
                                    <table
                                      className="responsive-enabled"
                                      data-striping="1"
                                    >
                                      <thead>
                                        <tr>
                                          <th>
                                            {
                                              this.state.task_language_explicit.task_details
                                                .file_information
                                            }
                                          </th>
                                          <th>
                                            {
                                              this.state.task_language_explicit.task_details
                                                .operations
                                            }
                                          </th>
                                        </tr>
                                      </thead>

                                      {this.state.files &&
                                        this.state.files.map((file, index) => {
                                          return (
                                            <tbody key={index}>
                                              <tr className="draggable">
                                                <td>
                                                  <a
                                                    href="#"
                                                    className="tabledrag-handle"
                                                    title={
                                                      this.state.task_language_explicit
                                                        .task_details.drag_to_re_order
                                                    }
                                                  />
                                                  <div className=" form-managed-file">
                                                    <span className="file file-image">
                                                      <Link
                                                        to="#"
                                                        // type="image/jpeg;"
                                                        className="menu-item-link"
                                                      >
                                                        {file.name}
                                                      </Link>
                                                    </span>
                                                  </div>
                                                </td>
                                                <td>
                                                  <input
                                                    name={index}
                                                    value={
                                                      this.state.task_language_explicit
                                                        .task_details.remove
                                                    }
                                                    className="button form-submit"
                                                    readOnly={true}
                                                    onClick={(e) =>
                                                      this.deleteFile(e)
                                                    }
                                                  />
                                                </td>
                                              </tr>
                                            </tbody>
                                          );
                                        })}
                                    </table>
                                  )}

                                <div
                                  className="messageserror"
                                  style={{
                                    display:
                                      (this.state.rejectedFile &&
                                        this.state.rejectedFile.length) > 0
                                        ? "block"
                                        : "none",
                                  }}
                                >
                                  <Link to="#" className="close">
                                    <img
                                      src={closeIcon}
                                      width="17.69px"
                                      height="22px"
                                      onClick={(e) => this.hideFileError()}
                                    />
                                  </Link>
                                  {this.state.rejectedFile &&
                                    this.state.rejectedFile.map((file, index) => {
                                      return (
                                        <div key={index}>
                                          <ul className="">
                                            <li className="">
                                              {this.state.task_language_explicit.display_error.form_error.error_text_2.replace(
                                                "[file_name]",
                                                file.name
                                              )}
                                              <br />
                                              {
                                                this.state.task_language_explicit.display_error
                                                  .form_error.following_extensions
                                              }
                                            </li>
                                          </ul>
                                        </div>
                                      );
                                    })}
                                </div>

                                <div className="col-md-12">
                                  {this.state.toggleFile === false && (
                                    <Dropzone
                                      onDrop={(acceptedFiles) =>
                                        this.setDropZoneFiles(
                                          acceptedFiles,
                                          setErrors,
                                          setFieldValue
                                        )
                                      }
                                    >
                                      {({ getRootProps, getInputProps }) => (
                                        <section>
                                          <div
                                            {...getRootProps()}
                                            className="customFile"
                                          >
                                            <input
                                              {...getInputProps()}
                                              ref={this.inputRef}
                                              style={{ display: "block" }}
                                            />
                                            {/* <p>Upload file</p> */}
                                          </div>
                                        </section>
                                      )}
                                    </Dropzone>
                                  )}
                                </div>


                              </Row>
                              {this.state.escalation_error != '' && <p style={{ color: 'red' }} >{this.state.escalation_error}</p>}
                            </div>

                            <ButtonToolbar>

                              <Button
                                className={`btn btn-default`}
                                type="submit"
                              //disabled={isValid ? false : true}
                              >
                                {this.state.task_language_explicit.task_details.send_mail}
                              </Button>

                              <Button
                                className={`btn btn-default btn-cancel`}
                                type="button"
                                onClick={this.handleSendMailEscalationClose}
                              >
                                {this.state.task_language_explicit.task_details.cancel}
                              </Button>

                            </ButtonToolbar>
                          </Modal.Body>
                          {/* <Modal.Footer>
                            
                          </Modal.Footer> */}
                        </Form>
                      );
                    }}
                  </Formik>
                </Modal>
              )}

              {this.state.showRDDPopup === true && (
                <Modal
                  show={this.state.showRDDPopup}
                  onHide={this.handleRDDPopupClose}
                  backdrop="static"
                  className="grantmodal editrdd"
                >
                  <Formik
                    initialValues={initialValues}
                    validationSchema={false}
                    onSubmit={this.handleSubmitRDD}
                  >
                    {({
                      values,
                      errors,
                      touched,
                      isValid,
                      isSubmitting,
                      handleChange,
                      setFieldValue,
                      setFieldTouched,
                      setErrors,
                    }) => {
                      return (
                        <Form>
                          <Modal.Header>
                            <Modal.Title>
                              {this.state.task_language_explicit.task_details.edit_rdd}
                            </Modal.Title>
                            <button className="close" onClick={this.handleRDDPopupClose} />
                          </Modal.Header>
                          <Modal.Body>
                            <div>
                              <Row>

                                <DatePicker
                                  className="form-control customInput"
                                  showMonthDropdown
                                  showYearDropdown
                                  minDate={this.state.fromMaxDate}
                                  maxDate={this.state.toMaxDate}
                                  dropdownMode="select"
                                  onChange={(e) => {
                                    this.setState({ rdd_error: '', new_rdd: e });
                                  }}
                                  selected={this.state.new_rdd}
                                  dateFormat="dd/MM/yyyy"
                                  autoComplete="off"
                                  placeholderText={
                                    this.props.dynamic_lang.my_requests.to_date
                                  }
                                />

                              </Row>
                              {this.state.rdd_error != '' && <p style={{ color: 'red' }} >{this.state.rdd_error}</p>}
                            </div>

                            <ButtonToolbar>

                              <Button
                                className={`btn btn-default`}
                                type="submit"
                                disabled={this.state.RDDShow}
                              >
                                {this.state.task_language_explicit.task_details.update}
                              </Button>

                              <Button
                                className={`btn btn-default btn-cancel`}
                                type="button"
                                onClick={this.handleRDDPopupClose}
                              >
                                {this.state.task_language_explicit.task_details.cancel}
                              </Button>

                            </ButtonToolbar>
                          </Modal.Body>
                          {/* <Modal.Footer>
                            
                          </Modal.Footer> */}
                        </Form>
                      );
                    }}
                  </Formik>
                </Modal>
              )}


              <Modal
                show={this.state.showRejectComment}
                onHide={this.handleCloseReject}
                backdrop="static"
                className="grantmodal"
              >
                <Modal.Header>
                  <button className="close" onClick={this.handleCloseReject} />
                </Modal.Header>
                <Modal.Body>
                  <label>Enter Reason</label>
                  <br />
                  <textarea onChange={(e) => { this.setState({ reject_notes: e.target.value }); }} style={{ width: '100%' }} rows="10" ></textarea>

                  <ButtonToolbar>
                    <Button
                      className={`btn btn-default`}
                      type="btton"
                      onClick={(e) => {
                        if (this.state.reject_type == 'invoice') {
                          this.approvePreShip(this.state.reject_id, 'reject', this.state.reject_notes);
                        } else if (this.state.reject_type == 'coa') {
                          this.approvePreShipCOA(this.state.reject_id, 'reject', this.state.reject_notes);
                        }
                      }}
                    >
                      Reject
                    </Button>

                    <Button
                      className={`btn btn-default btn-cancel`}
                      type="button"
                      onClick={this.handleCloseReject}
                    >
                      {this.state.task_language_explicit.task_details.cancel}
                    </Button>

                  </ButtonToolbar>


                </Modal.Body>
              </Modal>
            </div>
          </>
        );
      }
    }
  }
}

const mapStateToProps = (state) => {
  return {
    discussion: state.discussion.discussion,
    task_status_arr: state.user.task_status_arr,
    new_feature_send_mail: state.auth.new_feature_send_mail,
    new_feature_11: state.auth.new_feature_11,
    detailsErr: state.discussion.errors ? state.discussion.errors.data : null,
    selCompany: state.user.selCompany,
    dynamic_lang: state.auth.dynamic_lang,
    tour_counter: state.auth.tour_counter,
    tour_counter_new: state.auth.tour_counter_new,
    login_token: state.auth.token,
    role_finance: state.auth.role_finance,
    //tour_counter_11: state.auth.tour_counter_11
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getDiscussion: (data, onSuccess) =>
      dispatch(fetchUserDiscussion(data, onSuccess)),
    getTaskStatus: (data, onSuccess) => dispatch(getTStatus(data, onSuccess)),
    refFeatureToken: (data, onSuccess, setErrors) =>
      dispatch(refreshFeatureToken(data, onSuccess, setErrors)),
    disconnectUserReload: (data, onSuccess) => dispatch(authLogoutReload(data, onSuccess)),
    updateTourToken: (data) => dispatch(updateToken(data))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TaskDetails);
