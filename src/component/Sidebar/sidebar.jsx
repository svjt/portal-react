import React, { Component } from "react";
import { Link, NavLink } from "react-router-dom";
import { BasicUserData } from "../../shared/helper";
//import ReactDom from 'react-dom';
import { Scrollbars } from "react-custom-scrollbars";
// import {
//   Dropdown,
//   DropdownToggle,
//   DropdownMenu,
//   DropdownItem,
//   UncontrolledDropdown,
// } from "reactstrap";
// import { UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
//import logoImg from '../../assets/images/logo.png';


import leftArrow from '../../assets/images/left-arrow.png';
import rightArrow from '../../assets/images/right-arrow-btn.png';

import { isMobile } from "react-device-detect";

// SATYAJIT
import { connect } from "react-redux";
import { showSidebarMenu, loadSidebarMenu } from "../../store/actions/menu";
import { faHideLeftMenu, onFAToggleRedux } from "../../store/actions/auth";




// SATYAJIT
import whitelogo from "../../assets/images/info-circle-solid.svg";

class Sidebar extends Component {
  constructor(props) {
    super(props);
    //this.node = React.createRef();
  }

  state = {
    toggleMenuClass: false,
    showMobileMenu: false,
    mobile: false,
    isLoading: true
  };

  onMobileClose = () => {
    if (isMobile) {
      var isClassExist = document
        .querySelector(".mobile-menu-icon")
        .classList.contains("mobile-menu-icon-active");
      if (isClassExist) {
        document.querySelector(".body-sidebar").classList.remove("menu-open");
        document
          .querySelector(".mobile-menu-icon")
          .classList.remove("mobile-menu-icon-active");
      } else {
        document.querySelector(".body-sidebar").classList.add("menu-open");
        document
          .querySelector(".mobile-menu-icon")
          .classList.add("mobile-menu-icon-active");
      }
    } else {
      this.props.faLeftMenu()
    }
  }

  componentDidMount = () => {
    //console.log('componentDidMount');
    this.props.loadMenu(() => {
      this.setState({
        toggleMenuClass: this.props.display_menu,
        isLoading: false,
      });
    });

    if (isMobile) {
      //DO NOTHING
    } else {
      this.props.faLeftMenu()
    }

  };

  componentDidUpdate = (prevProps) => {
    //console.log('componentDidUpdate');
    if (this.props.display_menu !== prevProps.display_menu) {
      this.setState({ toggleMenuClass: this.props.display_menu });
    }
  };

  componentWillMount() {
    //console.log('componentWillMount');
    document.addEventListener("mousedown", this.handleClick, false);
  }

  componentWillUnmount() {
    if (this.state.toggleMenuClass) {
      this.onMobileClose();
    }
    document.removeEventListener("mousedown", this.handleClick, false);
  }

  openSideMenu = () => {
    this.props.showMenu(this.state.toggleMenuClass, () => {
      //this.onMobileClose()
      this.setState({ toggleMenuClass: this.props.display_menu });
    });
  };

  closeSideMenu = () => {
    this.props.showMenu(this.state.toggleMenuClass, () => {
      this.setState({ toggleMenuClass: this.props.display_menu });
      this.onMobileClose();
    });
  };

  handleClick = (e) => {
    if (this.node && this.node.contains(e.target)) {
      return;
    }
    this.handleClickOutside();
  };

  handleClickOutside = () => {
    this.setState({ toggleMenuClass: false });
  };

  render() {
    const { isLoggedIn } = this.props;
    if (!isLoggedIn) return null;
    const logged_User = BasicUserData();
    const role_user = logged_User.role;
    const user_lang = logged_User.lang;
    var lang_url = "";
    let lang_class = "";
    if (user_lang == "ja") {
      lang_url = user_lang;
      lang_class = 'ja_sidebar';
    } else if (user_lang == "es") {
      lang_url = user_lang;
      lang_class = 'es_sidebar';
    } else if (user_lang == "zh") {
      lang_url = "zh-hans";
      lang_class = 'zh_sidebar';
    } else if (user_lang == "pt") {
      lang_url = "pt-pt";
      lang_class = 'pt_sidebar';
    } else {
      lang_class = 'en_sidebar';
    }

    const className = this.state.toggleMenuClass
      ? "btn btn-secondary dropdown-toggle request-btn displayMinus"
      : "btn btn-secondary dropdown-toggle request-btn displayPlus";


    let show = true;
    if(window.location.pathname === "/qa_login" || window.location.pathname === "/customer_portal/qa_login" || window.location.pathname === "/new_customer_portal/qa_login" || window.location.pathname === "/qa_form" || window.location.pathname === "/customer_portal/qa_form" || window.location.pathname === "/new_customer_portal/qa_form"){
        show = false
    }  

    if(show){
      return (
        <nav className={`d-md-block customSidebar sidebar menu-left-sec ${lang_class} ${this.props.faSidebar}`}>
          <div className="mobile-menu-icon" onClick={this.onMobileClose}>
            <span className="menu-line-1"></span>
            <span className="menu-line-2"></span>
            <span className="menu-line-3"></span>
          </div>
  
          {/* {this.props.faSidebar &&
            <> */}
              {((this.props.faSidebar === 'fa-sidebar' && this.props.toggleFa==false) || (this.props.faSidebar == '' && this.props.toggleFa==false)) ? <div className="fa-menu-icon" onClick={this.props.onFAToggle}>
                <span className="menu-line-1"></span>
                <span className="menu-line-2"></span>
                <span className="menu-line-3"></span>
                {/* <img src={rightArrow} /> */}
              </div> : <button type="button" className="fa-close-btn" aria-label="Close" onClick={this.props.onFAToggle} >
                  <span aria-hidden="true"><img src={leftArrow} /></span>
                </button>}
            {/* </>} */}
          <div>
            <div className="logo">
              {" "}
              <img src={require("../../assets/images/logo.png")} />{" "}
            </div>
          </div>
          <div className="dropdown requestNav" ref={(node) => (this.node = node)}>
            <button
              onClick={(e) => this.openSideMenu(e)}
              className={className}
              type="button"
              id="dropdownMenu1"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="false"
            >
              {" "}
              {this.props.dynamic_lang.left_menu.raise_service_request}{" "}
            </button>
            <div className="">
              <ul
                className={
                  this.state.toggleMenuClass
                    ? "dropdown-menu multi-level menu-open"
                    : "dropdown-menu multi-level"
                }
              >
                <li className="dropdown-submenu">
                  <Link
                    to="#product-related-enquiries"
                    onClick={() => this.closeSideMenu()}
                  >
                    {this.props.dynamic_lang.left_menu.product_related_enquiries}
                  </Link>
                  <ul className="dropdown-menu">
                    <li className="dropdown-item">
                      <Link
                        to={{
                          pathname: "/general-request",
                        }}
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.general_request}
                      </Link>
                    </li>
  
                    <li className="dropdown-item">
                      <Link
                        to="/request_price_quote"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .request_for_price_quote
                        }
                      </Link>
                    </li>
                  </ul>
                </li>
                <li className="dropdown-submenu">
                  <Link to="#request-for-sample">
                    {this.props.dynamic_lang.left_menu.request_for_sample}
                  </Link>
                  <ul className="dropdown-menu">
                    <li className="dropdown-item">
                      <Link to="/sample" onClick={() => this.closeSideMenu()}>
                        {
                          this.props.dynamic_lang.left_menu
                            .samples_working_impurities
                        }
                      </Link>
                    </li>
                  </ul>
                </li>
                <li className="dropdown-submenu">
                  <Link to="#quality-related">
                    {this.props.dynamic_lang.left_menu.quality_related}
                  </Link>
                  <ul className="dropdown-menu">
                    <li className="dropdown-item">
                      <Link
                        to="/quality-general-request"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .quality_general_request
                        }
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/vendor-questionnaire"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.vendor_questionnaire}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/spec-and-moa"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.spec_moa}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/method-related-queries"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.method_related_queries}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/typical-coa"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.typical_coa}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/recertification-of-coa"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.recertification_coa}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/audit-visit-request"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.audit_visit_request}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/customized-spec-request"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .customized_spec_request
                        }
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/quality-equivalence-request"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .quality_equivalence_request
                        }
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/apostallation-doc"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .apostallation_documents
                        }
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/stability-data"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.stability_data}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/elemental-impurity-declaration"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .elemental_impurity_declaration
                        }
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/residual-solvents-declaration"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .residual_solvents_declaration
                        }
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/storage-transport-declaration"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .storage_transport_declaration
                        }
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/request-additional-declarations"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .request_additional_declarations
                        }
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/genotoxic-impurity-assessment"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .genotoxic_impurity_assessment
                        }
                      </Link>
                    </li>
                  </ul>
                </li>
                <li className="dropdown-submenu">
                  <Link to="#regulatory-request">
                    {this.props.dynamic_lang.left_menu.regulatory_request}
                  </Link>
                  <ul className="dropdown-menu">
                    <li className="dropdown-item">
                      <Link
                        to="/regulatory-general-request"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .regulatory_general_request
                        }
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to={{
                          pathname: "/request-for-loa",
                        }}
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.request_loa}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/request-for-cep-copy"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.request_cep_copy}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/request-for-loc-loe"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.request_loc_loe}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/request-for-dmf"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.request_dmf}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/queries-related-dmf"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.queries_related_dmf}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/queries-related-deficiencies"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .queries_related_deficiencies
                        }
                      </Link>
                    </li>
  
                    <li className="dropdown-item">
                      <Link
                        to="/queries-related-notifications"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .queries_related_notifications
                        }
                      </Link>
                    </li>
  
                    <li className="dropdown-item">
                      <Link
                        to="/quality_overall_summary"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .quality_overall_summary
                        }
                      </Link>
                    </li>
  
                    <li className="dropdown-item">
                      <Link
                        to="/nitrosoamine"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .nitrosoamines_declarations
                        }
                      </Link>
                    </li>
  
                    <li className="dropdown-item">
                      <Link
                        to="/tga-clearance"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .tga_gmp_clearance_documents
                        }
                      </Link>
                    </li>
                  </ul>
                </li>
                <li className="dropdown-submenu">
                  <Link to="#legal-related">
                    {this.props.dynamic_lang.left_menu.legal_related}
                  </Link>
                  <ul className="dropdown-menu">
                    <li className="dropdown-item">
                      <Link
                        to="/legal-general-request"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.legal_general_request}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/cda-sales-agreement"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.cda_sales_agreement}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/quality-agreement"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.quality_agreement}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/ip_related_enquiries"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.ip_related_enquiries}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/master_file_agreement"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.master_file_agreement}
                      </Link>
                    </li>
                  </ul>
                </li>
                <li className="dropdown-submenu">
                  <Link to="#order">
                    {this.props.dynamic_lang.left_menu.order_related}
                  </Link>
                  <ul className="dropdown-menu">
  
                    <li className="dropdown-item">
                      <Link
                        to="/order-general-request"
                        onClick={() => this.closeSideMenu()}
                      >
                        {this.props.dynamic_lang.left_menu.order_general_request}
                      </Link>
                    </li>
  
                    <li className="dropdown-item">
                      <Link to="/new-order" onClick={() => this.closeSideMenu()}>
                        {this.props.dynamic_lang.left_menu.new_order_request}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/proforma-invoice"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .request_proforma_invoice
                        }
                      </Link>
                    </li>
                    {this.props.show_stock_enquiry && 
                    <li className="dropdown-item">
                      <Link to="/stock-enquiry" onClick={() => this.closeSideMenu()}>
                        {this.props.dynamic_lang.left_menu.stock_enquiry_request}
                      </Link>
                    </li>}
                  </ul>
                </li>
                <li className="dropdown-submenu">
                  <Link to="#complaint">
                    {this.props.dynamic_lang.left_menu.raise_complaint}
                  </Link>
                  <ul className="dropdown-menu">
                    <li className="dropdown-item">
                      <Link
                        to="/quality-complaints"
                        onClick={() => this.closeSideMenu()}
                      >
                        {
                          this.props.dynamic_lang.left_menu
                            .quality_related_complaint
                        }{" "}
                      </Link>
                    </li>
                    <li className="dropdown-item">
                      <Link
                        to="/logistic-complaints"
                        onClick={() => this.closeSideMenu()}
                      >
                        {" "}
                        {
                          this.props.dynamic_lang.left_menu
                            .logistics_related_complaint
                        }{" "}
                      </Link>
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
          <div className="clearfix"></div>
  
          <div className="sidebar-sticky">
            <Scrollbars
              //  autoHide
              //  autoHideTimeout={1000}
              //  autoHideDuration={200}
              //  autoHeight
              //  autoHeightMin={0}
              // autoHeightMax={600}
              //  thumbMinSize={30}
              style={{
                minHeight: "calc(150vh - 145px)",
                maxHeight: "calc(150vh - 145px)",
              }}
              //   renderTrackVertical={props =>
              //    <div
              //        {...props}
              //         className="track-vertical"
              //    />
              //  }
              renderThumbVertical={(props) => (
                <div
                  {...props}
                  //className="thumb-vertical"
                  style={{
                    backgroundColor: "rgba(255,255,255,0.5)",
                    width: "5px",
                    borderRadius: "16px",
                  }}
                />
              )}
              renderView={(props) => <div {...props} className="menu-sec" />}
            >
              <ul className="nav flex-column">
                <li className="home">
                  <a
                    href={`${process.env.REACT_APP_PORTAL_URL}${lang_url}`}
                    target="_blank"
                  >
                    {this.props.dynamic_lang.left_menu.home}
                  </a>
                </li>
                <li className="dashboard" onClick={this.onMobileClose}>
                  <NavLink to="/dashboard" activeClassName="is-active">
                    {this.props.dynamic_lang.left_menu.dashboard}
                  </NavLink>
                </li>
                <li className="product" onClick={this.onMobileClose}>
                  <NavLink
                    to="/product-catalogue"
                    activeClassName="is-active"
                  >
                    {this.props.dynamic_lang.left_menu.prod_cata}
                  </NavLink>
                  {/* <a href={`${process.env.REACT_APP_PORTAL_URL}products`} target="_blank" activeClassName="is-active" onClick = {this.onMobileClose}>
                    Product Catalogue
                  </a> */}
                </li>
                {role_user === 1 && (
                  <li className="team" onClick={this.onMobileClose}>
                    <NavLink
                      to="/my-team/"
                      activeClassName="is-active"
  
                    >
                      {this.props.dynamic_lang.left_menu.my_team}
                    </NavLink>
                  </li>
                )}
                <li className="news">
                  <a
                    href={`${process.env.REACT_APP_PORTAL_URL}news`}
                    target="_blank"
                  >
                    {this.props.dynamic_lang.left_menu.news}
                  </a>
                </li>
                <li className="contact" onClick={this.onMobileClose}>
                  <NavLink
                    to="/contact-us"
                    activeClassName="is-active"
                  >
                    {this.props.dynamic_lang.left_menu.contact}
                  </NavLink>
                </li>
                {this.props.faAcess && <li className="dashboard" onClick={this.onMobileClose}>
                  <NavLink to="/product-enquiry" activeClassName="is-active">
                    Formulation Handbook
                  </NavLink>
                </li>}
                <li className="visit-us-btn">
                  <a href="http://www.drreddys.com/" target="_blank">
                    {this.props.dynamic_lang.left_menu.c_web}
                  </a>
                  {/* <Link exa="http://www.drreddys.com/" target="_blank" activeClassName="is-active"> 
                              
                            </Link> */}
                </li>
  
              </ul>
              <div className="products-disclaimer">
                <p>
                  <b>{this.props.dynamic_lang.left_menu.disclaimer}</b>{" "}
                  {this.props.dynamic_lang.left_menu.menu_content}
                </p>
              </div>
            </Scrollbars>
          </div>
        </nav>
      );
    }else{
      return null;
    }
  }
}
// SATYAJIT
const mapStateToProps = (state) => {
  //console.log('sidebar', state)
  return {
    isLoggedIn: state.auth.token,
    dynamic_lang: state.auth.dynamic_lang,
    display_menu: state.menu.display,
    faSidebar: state.auth.faSidebar,
    toggleFa: state.auth.toggleFa,
    faAcess: state.auth.faAcess,
    show_stock_enquiry: state.auth.show_stock_enquiry,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadMenu: (onSuccess) => dispatch(loadSidebarMenu(onSuccess)),
    showMenu: (data, onSuccess) => dispatch(showSidebarMenu(data, onSuccess)),
    faLeftMenu: () => dispatch(faHideLeftMenu()),
    onFAToggle: () => dispatch(onFAToggleRedux())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
