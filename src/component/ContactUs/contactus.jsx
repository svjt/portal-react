import React, { Component } from "react";
import { Link } from "react-router-dom";
// import Layout from "../Layout/layout";
import closeIcon from "../../assets/images/times-solid-red.svg";
import phoneIcon from "../../assets/images/phone-alt-solid.svg";
import { Formik, Field, Form } from "formik";
import * as Yup from "yup";
import axios from "../../shared/axios";
import Loader from "../Loader/loader";
import { htmlDecode } from "../../shared/helper";
import Select from "react-select";

import { BasicUserData } from "../../shared/helper";
import { connect } from "react-redux";

//import closeIcon from "../../assets/images/times-solid-red.svg";
//import console = require("console");

/*Initialization*/
const initialValues = {
  name: "",
  email: "",
  mobile: "",
  company_id: "",
  country_id: "",
  product_id: 0,
  agreeToTerms: "",
  description: "",
};
//const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;
const contactUsvalidation = (refObj) =>
  Yup.object().shape({
    name: Yup.string().trim().required(
      refObj.props.dynamic_lang.display_error.contact_us.name
    ),
    email: Yup.string().trim()
      .required(refObj.props.dynamic_lang.display_error.contact_us.email)
      .email(refObj.props.dynamic_lang.display_error.contact_us.valid_email),
    product_id: Yup.number().default(0),
    country_id: Yup.number().required(
      refObj.props.dynamic_lang.display_error.contact_us.country_id
    ),
    description: Yup.string()
      .trim()
      .required(refObj.props.dynamic_lang.display_error.contact_us.description),
    agreeToTerms: Yup.boolean()
      .label()
      .test(
        "is-true",
        refObj.props.dynamic_lang.display_error.contact_us.terms,
        (value) => value === true
      ),
  });

class contact extends Component {
  state = {
    user: "",
    country: [],
    showLoader: false,
    errMsg: "",
    products: [],
    selectedOption: null,
    product_id: 0,
    isLoading: true,
  };

  getProductList() {
    axios
      .get("/products")
      .then((res) => {
        var product_list = [];
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          product_list.push({
            value: element["product_id"],
            label: htmlDecode(element["product_name"]),
          });
        }
        this.setState({ products: product_list });
      })
      .catch((err) => {});
  }

  getCountryList() {
    axios
      .get("/country")
      .then((res) => {
        this.setState({
          country: [
            {
              country_id: "",
              country_name: this.props.dynamic_lang.contact_us.country,
            },
          ].concat(res.data.data),
        });
      })
      .catch((err) => {});
  }
  componentDidMount() {
    const users = BasicUserData();
    if (users) {
      if (users.role === 2) {
        users.name = `${users.name} (Agent)`;
      }

      this.setState({ user: users });
    }
    this.getCountryList();
    this.getProductList();
    document.title =
      this.props.dynamic_lang.contact_us.header + " | Dr. Reddy's API";

    // set product while coming from product catalouge -- SATYAJIT
    if (this.props.history.location.state !== undefined) {
      this.setState({
        selectedOption: {
          value: this.props.history.location.state.pid,
          label: this.props.history.location.state.pname,
        },
      });
    }
    // ===================//
  }

  removeError = (setErrors) => {
    setErrors({});
  };
  hideServerError = () => {
    this.setState({ errMsg: "" });
  };
  handleSubmit = (values, actions) => {
    this.setState({ showLoader: true });

    axios
      .post("contact", {
        name: values.name.trim(),
        email: values.email.trim(),
        mobile: values.mobile.trim(),
        message: values.description.trim(),
        country_id: values.country_id,
        company_name: values.company_id,
        product_id: values.product_id,
      })
      .then((res) => {
        if (res.status === 200) {
          this.setState({ showLoader: false });
          this.props.history.push({
            pathname: "/dashboard/",

            state: {
              message: this.props.dynamic_lang.contact_us.success_msg,
            },
          });
        } else {
        }
      })
      .catch((err) => {
        this.setState({ showLoader: false });

        actions.setSubmitting(false);
        // console.log(err.data.status);

        if (err.data.status === 2) {
          actions.setErrors(err.data.errors);
        } else {
          this.setState({ errMsg: err.data.message });
        }
      });
  };

  render() {
    //console.log('---',this.state.user);
    if (this.state.user === null) return null;
    const { user } = this.state;

    const newInitialValues = Object.assign(initialValues, {
      name: user.name ? user.name : "",
      email: user.email ? user.email : "",
      mobile: "",
      product_id: user.product_id ? user.product_id : 0,
    });

    return (
      <div className="container-fluid clearfix formSec contactSec">
        <div className="dashboard-content-sec">
          <div className="service-request-form-sec">
            <div className="form-page-title-block">
              <h2>{this.props.dynamic_lang.contact_us.header}</h2>
            </div>
            {this.state.showLoader === true ? <Loader /> : null}
            <Formik
              initialValues={newInitialValues}
              validationSchema={() => contactUsvalidation(this)}
              onSubmit={this.handleSubmit}
            >
              {({
                values,
                errors,
                isValid,
                touched,
                isSubmitting,
                setFieldValue,
                setErrors,
              }) => {
                //console.log("error", errors);
                //console.log("touched", touched);
                return (
                  <Form>
                    {/* {console.log("===============", errors)} */}
                    <div
                      className="messageserror"
                      style={{
                        display:
                          this.state.errMsg &&
                          this.state.errMsg !== null &&
                          this.state.errMsg !== ""
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="contact-us/" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.hideServerError()}
                          alt=""
                        />
                      </Link>

                      <div>
                        <ul className="">
                          <li className="">{this.state.errMsg}</li>
                        </ul>
                      </div>
                    </div>

                    <div
                      className="messageserror"
                      style={{
                        display:
                          (errors.name && touched.name) ||
                          (errors.email && touched.email) ||
                          (errors.description && touched.description) ||
                          (errors.product_id && touched.product_id) ||
                          (errors.country_id && touched.country_id) ||
                          (errors.agreeToTerms && touched.agreeToTerms)
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="contact-us/" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.removeError(setErrors)}
                          alt=""
                        />
                      </Link>

                      <div>
                        <ul className="">
                          {errors.name && touched.name ? (
                            <li className="">{errors.name}</li>
                          ) : null}

                          {errors.email && touched.email ? (
                            <li className="">{errors.email}</li>
                          ) : null}

                          {errors.product_id && touched.product_id ? (
                            <li className="">{errors.product_id}</li>
                          ) : null}

                          {errors.country_id && touched.country_id ? (
                            <li className="">{errors.country_id}</li>
                          ) : null}

                          {/* {errors.mobile && touched.mobile ? (
                              <li className="">{errors.mobile}</li>
                            ) : null}
                            
                            {errors.company_id ? (
                              <li className="">{errors.company_id}</li>
                            ) : null} */}

                          {errors.description && touched.description ? (
                            <li className="">{errors.description}</li>
                          ) : null}

                          {errors.agreeToTerms && touched.agreeToTerms ? (
                            <li className="">{errors.agreeToTerms}</li>
                          ) : null}
                        </ul>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-4">
                        <Field
                          name="name"
                          type="text"
                          className="form-control customInput"
                          placeholder={this.props.dynamic_lang.contact_us.name}
                          value={values.name}
                        />
                      </div>
                      <div className="col-md-4">
                        <Field
                          name="email"
                          type="text"
                          className="form-control customInput"
                          placeholder={this.props.dynamic_lang.contact_us.email}
                          value={values.email}
                        />
                      </div>
                      <div className="col-md-4">
                        <Field
                          name="mobile"
                          type="text"
                          className="form-control customInput"
                          placeholder={
                            this.props.dynamic_lang.contact_us.mobile
                          }
                          value={values.mobile}
                        />
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-3">
                        <Select
                          value={this.state.selectedOption}
                          //onChange={this.handleChange}
                          options={this.state.products}
                          isSearchable={true}
                          isClearable={true}
                          placeholder={
                            this.props.dynamic_lang.contact_us.product
                          }
                          className="product_id"
                          onChange={(e) => {
                            if (e === null || e === "") {
                              setFieldValue("product_id", "");
                              this.setState({ selectedOption: "" });
                            } else {
                              setFieldValue("product_id", e.value);
                              this.setState({ selectedOption: e });
                            }
                          }}
                        />
                      </div>
                      <div className="col-md-3">
                        <Field
                          component="select"
                          name="country_id"
                          className="form-control customeSelect"
                          value={values.country_id}
                        >
                          {this.state.country.map((country) => (
                            <option
                              key={country.country_id}
                              value={country.country_id}
                            >
                              {htmlDecode(country.country_name)}
                            </option>
                          ))}
                        </Field>
                      </div>
                      <div className="col-md-6">
                        <Field
                          name="company_id"
                          type="text"
                          className="form-control customInput"
                          placeholder={
                            this.props.dynamic_lang.contact_us.company
                          }
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-12 asterix">
                        <Field
                          name="description"
                          type="textarea"
                          component="textarea"
                          className="form-control customTextarea"
                          placeholder={
                            this.props.dynamic_lang.contact_us.message
                          }
                          rows="5"
                          cols="60"
                        />
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-12 my-auto">
                        <div className="form-check-inline">
                          <label className="form-check-label">
                            <Field
                              type="checkbox"
                              name="agreeToTerms"
                              className="form-check-input"
                              value={values.agreeToTerms}
                              onChange={(e) => {
                                setFieldValue("agreeToTerms", e.target.checked);
                              }}
                            />
                            {this.props.dynamic_lang.contact_us.agree}{" "}
                            <a
                              href={`${process.env.REACT_APP_PORTAL_URL}${this.props.dynamic_lang.contact_us.privacy_policy_url}`}
                              target="_blank"
                            >
                              {
                                this.props.dynamic_lang.contact_us
                                  .privacy_policy
                              }
                            </a>{" "}
                            {this.props.dynamic_lang.contact_us.and}{" "}
                            <a
                              href={`${process.env.REACT_APP_PORTAL_URL}${this.props.dynamic_lang.contact_us.terms_of_use_url}`}
                              target="_blank"
                            >
                              {this.props.dynamic_lang.contact_us.terms_use}
                            </a>{" "}
                            {this.props.dynamic_lang.contact_us.website}
                          </label>
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col">
                        <button className="btn btn-default ml-0">
                          {this.props.dynamic_lang.contact_us.send_message}
                        </button>
                        <div className="clearfix" />
                        <div className="footerInfo">
                          <h6>
                            <span>
                              {this.props.dynamic_lang.contact_us.email_us}{" "}
                              <a href="mailto:api@drreddys.com">
                                api@drreddys.com
                              </a>{" "}
                            </span>
                            <span className="mobOff">| </span>
                            <span>
                              {" "}
                              <img
                                src={phoneIcon}
                                width="15.55"
                                height="15"
                              />{" "}
                              +91 40 49002253{" "}
                            </span>
                          </h6>
                        </div>
                      </div>
                    </div>
                  </Form>
                );
              }}
            </Formik>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};
//export default contact;
export default connect(mapStateToProps, mapDispatchToProps)(contact);
