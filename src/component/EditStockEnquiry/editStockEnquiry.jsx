import React, { Component } from "react";
import { Link } from "react-router-dom";
import * as Yup from "yup";
// import Select from "react-select";
import { Formik, Form, Field } from "formik";
import closeIcon from "../../assets/images/times-solid-red.svg";
import { connect } from "react-redux";
import Loader from "../Loader/loader";
import { supportedPdfFileType, htmlDecode, BasicUserData } from "../../shared/helper";
import Dropzone from "react-dropzone";
// import swal from "sweetalert";
import axios from "../../shared/axios";

var path = require("path");
var sold_to_party_list = [];

const validationSchema = (refObj) =>
  Yup.object().shape({
    po_no: Yup.string().required("Purchase Order No. required"),
  });

class EditStockEnquiry extends Component {
  constructor(props) {
    super(props);
    this.state = {
      task_id: this.props.match.params.id,
      request_id: "",
      ship_to: 0,
      po_status: 0,
      errMsg: "",
      showLoader: false,
      showBlock: false,
      buttonNext: false,
      sold_to_party: "",
      files: [],
      rejectedFile: [],
      sold_to_party_list: [],
      agent_customer_id: null,
    };
  }

  componentDidMount() {

    //this.setState({ showLoader: true });    
    this.getTaskDetails();
  }

  getTaskDetails() {
    this.setState({ showLoader: true, errMsg: "" });
    axios
      .get(`sap/stock_task_details/${this.state.task_id}`)
      .then((res) => {
        this.setState({ showLoader: false, errMsg: "", request_id: res.data.data.request_id, po_status: res.data.data.po_status, ship_to: res.data.data.ship_to_party,
        agent_customer_id: res.data.data.user_id});
        if (res.data.data.ship_to_party) {
          this.getSoldToList(res.data.data.ship_to_party);
        }
      })
      .catch((err) => {
        this.setState({ showLoader: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.unable_to_fetch_product});
      });
  }

  getSoldToList(shipto_id) {
    this.setState({ showLoader: true, errMsg: "" });
    axios
      .get("sap/customer_soldto/" + shipto_id)
      .then((res) => {
        this.setState({ showLoader: false, errMsg: "", sold_to_party_list: res.data.data });
      })
      .catch((err) => {
        this.setState({ showLoader: false, errMsg: this.props.dynamic_lang.stock_enquiry_new.unable_to_fetch_ship_to_details });
      });
  };

  // getSoldToList() {
  //   this.setState({ showLoader: true, errMsg: "" });
  //   axios
  //     .get("sap/soldto")
  //     .then((res) => {
  //       for (let index = 0; index < res.data.data.length; index++) {
  //         const element = res.data.data[index];
  //         sold_to_party_list.push({
  //           value: element["code"],
  //           label: htmlDecode(element["name"]),
  //         });
  //       }
  //       this.setState({ showLoader: false, errMsg: "" });
  //     })
  //     .catch((err) => {
  //       this.setState({ showLoader: false, errMsg: "Unable to fetch ship to." });
  //     });
  // };

  handleSubmit = (values, actions) => { //console.log(values)
    // let subObj = {
    //   request_id : this.state.request_id,
    //   sold_to_party : this.state.sold_to_party.value
    // }
    if (this.state.files.length === 0) {
      this.setState({ errMsg: this.props.dynamic_lang.stock_enquiry_new.select_file_to_upload });
    } else {
      this.setState({ showLoader: true, errMsg: "" });
      var formData = new FormData();
      formData.append('request_id', this.state.request_id);
      if (this.state.files && this.state.files.length > 0) {
        for (let index = 0; index < this.state.files.length; index++) {
          const element = this.state.files[index];
          formData.append("file", element);
        }
      }
      let userData = BasicUserData();
      if(userData.role === 2){
        if (this.state.agent_customer_id && this.state.agent_customer_id > 0) {
          formData.append('agent_customer_id', this.state.agent_customer_id);
        }
      }
      formData.append("po_no", values.po_no);
      //console.log(formData);
      axios
        .post("sap/customer_approve_po", formData)
        .then((res) => {
          if (res.status === 200) {
            this.setState({ showLoader: false });
            this.props.history.push({
              pathname: "/my_orders",
              state: {
                message: this.props.dynamic_lang.stock_enquiry_new.po_uploaded_successfully,
              },
            });
          } else {
            this.setState({ errMsg: this.props.dynamic_lang.stock_enquiry_new.failed_to_upload_po });
          }
        })
        .catch((err) => {
          this.setState({ showLoader: false });

          //actions.setSubmitting(false);
          //  alert(err.data.status)

          if (err.data.status === 2) {
            actions.setErrors(err.data.errors);
          } else {
            this.setState({ errMsg: err.data.message });
          }
        });
    }

  };

  hideError = () => {
    this.setState({ errMsg: "" });
  };

  removeError = (setErrors) => {
    setErrors({});
  };

  removeFile_Error = () => {
    this.setState({ rejectedFile: [] });
  };
  setDropZoneFiles = (acceptedFiles) => {
    //console.log(acceptedFiles);
    var rejectedFiles = [];
    var uploadFile = [];

    var totalfile = acceptedFiles.length;

    for (var index = 0; index < totalfile; index++) {
      var error = 0;
      var filename = acceptedFiles[index].name.toLowerCase();
      var extension_list = supportedPdfFileType();
      var ext_with_dot = path.extname(filename);
      var file_extension = ext_with_dot.split(".").join("");

      var obj = {};

      var fileErrText = this.props.dynamic_lang.display_error.form_error
      .following_extensions;
      //console.log(file_extension);
      //console.log(extension_list.indexOf(file_extension) );
      if (extension_list.indexOf(file_extension) == '-1') {
        error = error + 1;

        //obj["fileName"] = filename;

        if (totalfile > 1) {
          if (index === 0) {
            obj["errorText"] =
            this.props.dynamic_lang.display_error.form_error.error_text_1.replace(
              "[file_name]",
              filename
            ) +
              fileErrText;
          } else {
            obj["errorText"] =
            this.props.dynamic_lang.display_error.form_error.error_text_2.replace(
              "[file_name]",
              filename
            ) +
              fileErrText;
          }
        } else {
          obj[
            "errorText"
          ] = this.props.dynamic_lang.display_error.form_error.error_text_2.replace(
            "[file_name]",
            filename
          ) + fileErrText;
        }

        rejectedFiles.push(obj);

        // console.log(rejectedFiles);
      }
      // console.log("step2.11");
      if (acceptedFiles[index].size > 50000000) {
        obj[
          "errorText"
        ] = this.props.dynamic_lang.display_error.form_error.allowed_size.replace(
          "[file_name]",
          filename
        );

        error = error + 1;

        rejectedFiles.push(obj);

        // console.log(rejectedFiles);
      }

      if (error === 0) {
        uploadFile.push(acceptedFiles[index]);
      }
    }

    //var prevFiles = this.state.files;
    var newFiles = [];
    // if (prevFiles.length > 0) {
    //   for (let index = 0; index < uploadFile.length; index++) {
    //     var remove = 0;

    //     for (let index2 = 0; index2 < prevFiles.length; index2++) {
    //       if (uploadFile[index].name === prevFiles[index2].name) {
    //         remove = 1;
    //         break;
    //       }
    //     }

    //     if (remove === 0) {
    //       prevFiles.push(uploadFile[index]);
    //     }
    //   }

    //   prevFiles.map((file) => {
    //     file.checked = false;
    //     newFiles.push(file);
    //   });
    // } else {
    uploadFile.map((file) => {
      file.checked = false;
      newFiles.push(file);
    });

    // console.log("acceptedFiles", acceptedFiles);
    // console.log("newFiles", newFiles);
    //}

    this.setState({
      files: newFiles,
      // filesHtml: fileListHtml
    });
    // console.log("newFiles", newFiles);

    this.setState({
      rejectedFile: rejectedFiles,
    });
    // }
  };


  render() {
    const {
      sold_to_party
    } = this.state;
    if (this.state.showLoader) {
      return <Loader />;
      // return <h2>Loading....</h2>
    } else {
      return (
        <>
          <div className="container-fluid clearfix formSec">
            <div className="dashboard-content-sec">

              <div
                className="messageserror"
                style={{
                  display:
                    this.state.errMsg &&
                      this.state.errMsg !== null &&
                      this.state.errMsg !== ""
                      ? "block"
                      : "none",
                }}
              >
                <Link to="#" className="close">
                  <img
                    src={closeIcon}
                    width="22px"
                    height="22px"
                    onClick={(e) => this.hideError()}
                  />
                </Link>
                <div>
                  <ul className="">
                    <li className="">{this.state.errMsg}</li>
                  </ul>
                </div>
              </div>
              <div className="service-request-form-sec newFormSec stockWrapper">
                <div className="form-page-title-block">
                  <h2>{this.state.po_status != 1 ? 
                  this.props.dynamic_lang.stock_enquiry_new.stock_enquiry : 
                  this.props.dynamic_lang.stock_enquiry_new.po_already_updated}
                  </h2>
                  <Link
                    to="/my_orders"
                    className="backLink"
                    id="showHistory"
                  >
                    {this.props.dynamic_lang.stock_enquiry_new.back}
                    </Link>
                </div>

                {this.state.po_status != 1 ? (
                  <Formik
                    //initialValues={newInitialValues}
                    validationSchema={validationSchema}
                    onSubmit={this.handleSubmit}
                  >
                    {({
                      values,
                      errors,
                      touched,
                      setErrors,
                      setFieldValue,
                      setFieldTouched,
                    }) => {
                      return (
                        <Form>
                          <div
                            className="messageserror"
                            style={{
                              display:
                                (errors.po_no)
                                  ? "block"
                                  : "none",
                            }}
                          >
                            <Link to="#" className="close">
                              <img
                                src={closeIcon}
                                width="17.69px"
                                height="22px"
                                onClick={(e) => this.removeError(setErrors)}
                              />
                            </Link>
                            <div>
                              <ul className="">
                                {errors.po_no ? (
                                  <li className="">{errors.po_no}</li>
                                ) : null}
                              </ul>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-12"><label>{this.props.dynamic_lang.stock_enquiry_new.select_sold_to_party}</label></div>
                            <div className="col-md-12">
                              {/* <Select
                                  value={sold_to_party}
                                  options={sold_to_party_list}
                                  isSearchable={true}
                                  isClearable={true}
                                  placeholder="Sold To Party *"
                                  className=""
                                  onChange={(e) => {
                                    if (e === null || e === "") {
                                      setFieldValue("sold_to_party", "");
                                      this.setState({ sold_to_party: "" });
                                    } else {
                                      setFieldValue("sold_to_party", e.value);
                                      this.setState({ sold_to_party: e });
                                    }
                                  }}
                                /> */}
                              <p>{this.state.sold_to_party_list.company_name}</p>
                              <p>
                                {this.state.sold_to_party_list.street != "" && this.state.sold_to_party_list.street != null ? this.state.sold_to_party_list.street + ", " : ""}
                                {this.state.sold_to_party_list.city != "" && this.state.sold_to_party_list.city != null ? this.state.sold_to_party_list.city + ", " : ""}
                                {this.state.sold_to_party_list.postalcode != "" && this.state.sold_to_party_list.postalcode != null ? this.state.sold_to_party_list.postalcode + " " : ""}
                              </p>
                              <p>{this.state.sold_to_party_list.country != "" && this.state.sold_to_party_list.country != null ? this.state.sold_to_party_list.country : ""}</p>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-12"><label>{this.props.dynamic_lang.task_details.po_no}</label></div>
                            <div className="col-md-12">
                              <Field
                                name="po_no"
                                type="text"                                
                                className="form-control customInput"                                
                                placeholder={this.props.dynamic_lang.stock_enquiry_new.enter_po}
                              />
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-12"><label>{this.props.dynamic_lang.stock_enquiry_new.attach_po}</label></div>
                            <div className="col-md-6">
                              <Dropzone
                                onDrop={(acceptedFiles) =>
                                  this.setDropZoneFiles(acceptedFiles)
                                }
                              >
                                {({ getRootProps, getInputProps }) => (
                                  <section>
                                    <div {...getRootProps()} className="customFile">
                                      <input
                                        {...getInputProps()}
                                        ref={this.inputRef}
                                        style={{ display: "block", float: "left" }}
                                        className="mt-0"
                                      />
                                    </div>
                                  </section>
                                )}
                              </Dropzone>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col text-center mb-5">
                              {/* <Link to="#" className=""> */}
                              <button
                                className="btn btn-default"
                                style={{
                                  width: "auto",
                                }}
                              >
                                {this.props.dynamic_lang.stock_enquiry_new.upload_po_proceed}
                                  </button>
                              {/* </Link> */}
                              <span
                                className="btn"
                                style={{
                                  marginLeft: "1%",
                                  paddingTop: "2%",
                                }}
                              >
                              </span>
                            </div>
                          </div>
                        </Form>
                      );
                    }}
                  </Formik>
                ) : (null)
                }
              </div>
            </div>
          </div>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  }; 
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(EditStockEnquiry);
