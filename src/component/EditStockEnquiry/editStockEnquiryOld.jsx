import React, { Component } from "react";
import { Link } from "react-router-dom";
import * as Yup from "yup";
import Select from "react-select";
import { Formik, Form, Field } from "formik";
import closeIcon from "../../assets/images/times-solid-red.svg";
import { connect } from "react-redux";
import Loader from "../Loader/loader";
import { supportedFileType, htmlDecode } from "../../shared/helper";
import Dropzone from "react-dropzone";
import swal from "sweetalert";
import axios from "../../shared/axios";

var path = require("path");
var sold_to_party_list = [];

const validationSchema = (refObj) =>
  Yup.object().shape({
    sold_to_party: Yup.string().required("Ship to party required"),
  });

class EditStockEnquiryOld extends Component {
  constructor(props) {
    super(props);
    this.state = {
      task_id: this.props.match.params.id,
      request_id: "",
      errMsg: "",
      showLoader: false,
      showBlock: false,
      buttonNext: false,
      sold_to_party: "",
      files: [],
      rejectedFile: [],
    };
  }

  componentDidMount() {
    
    //this.setState({ showLoader: true });
    this.getSoldToList();
    this.getTaskDetails();
  }

  getTaskDetails() {
    this.setState({ showLoader: true, errMsg: "" });
    axios
      .get(`sap/stock_task_details/${this.state.task_id}`)
      .then((res) => {
        this.setState({ showLoader: false, errMsg: "", request_id: res.data.data.request_id });      
      })
      .catch((err) => {
        this.setState({ showLoader: false, errMsg: "Unable to fetch product." });  
      });
  }

  getSoldToList() {
    this.setState({ showLoader: true, errMsg: "" });
    axios
      .get("sap/soldto")
      .then((res) => {
        for (let index = 0; index < res.data.data.length; index++) {
          const element = res.data.data[index];
          sold_to_party_list.push({
            value: element["code"],
            label: htmlDecode(element["name"]),
          });
        }
        this.setState({ showLoader: false, errMsg: "" });
      })
      .catch((err) => {
        this.setState({ showLoader: false, errMsg: "Unable to fetch ship to." });
      });
  };
  
  handleSubmit = (values, actions) => {
    this.setState({ showLoader: true, errMsg: "" });
    let subObj = {
      request_id : this.state.request_id,
      sold_to_party : this.state.sold_to_party.value
    }
    
    axios
      .post("sap/approve_po", subObj)
      .then((res) => {
        if (res.status === 200) {
          this.setState({ showLoader: false });
          this.props.history.push({
            pathname: "/my_enquiries",
            state: {
              message: "PO uploaded successfully ",
            },
          });
        } else {
          this.setState({ errMsg: "Failed to Upload PO"});
        }
      })
      .catch((err) => {
        this.setState({ showLoader: false });

        //actions.setSubmitting(false);
        //  alert(err.data.status)

        if (err.data.status === 2) {
          actions.setErrors(err.data.errors);
        } else {
          this.setState({ errMsg: err.data.message });
        }
      });

    
  };

  removeError = (setErrors) => {
    setErrors({});
  };

  removeFile_Error = () => {
    this.setState({ rejectedFile: [] });
  };
  setDropZoneFiles = (acceptedFiles) => {
    console.log(acceptedFiles);
    var rejectedFiles = [];
    var uploadFile = [];

    var totalfile = acceptedFiles.length;

    for (var index = 0; index < totalfile; index++) {
      var error = 0;
      var filename = acceptedFiles[index].name.toLowerCase();
      var extension_list = supportedFileType();
      var ext_with_dot = path.extname(filename);
      var file_extension = ext_with_dot.split(".").join("");

      var obj = {};

      var fileErrText = `\nOnly files with the following extensions are allowed: doc docx pdf txt odt rtf wpd tex wks wps xls xlsx xlr ods csv ppt pptx pps key odp ai bmp gif ico jpeg jpg png svg tif tiff eml.`;

      if (extension_list.indexOf(file_extension) === -1) {
        error = error + 1;

        //obj["fileName"] = filename;

        if (totalfile > 1) {
          if (index === 0) {
            obj["errorText"] =
              `One or more files could not be uploaded.The specified file ${filename} could not be uploaded.` +
              fileErrText;
          } else {
            obj["errorText"] =
              `The specified file ${filename} could not be uploaded.` +
              fileErrText;
          }
        } else {
          obj[
            "errorText"
          ] = `The specified file ${filename} could not be uploaded. ${fileErrText}`;
        }

        rejectedFiles.push(obj);

        console.log(rejectedFiles);
      }
      console.log("step2.11");
      if (acceptedFiles[index].size > 50000000) {
        obj[
          "errorText"
        ] = `The file ${filename} could not be saved because it exceeds 50 MB, the maximum allowed size for uploads.`;

        error = error + 1;

        rejectedFiles.push(obj);

        console.log(rejectedFiles);
      }

      if (error === 0) {
        uploadFile.push(acceptedFiles[index]);
      }
    }

    var prevFiles = this.state.files;
    var newFiles = [];
    if (prevFiles.length > 0) {
      for (let index = 0; index < uploadFile.length; index++) {
        var remove = 0;

        for (let index2 = 0; index2 < prevFiles.length; index2++) {
          if (uploadFile[index].name === prevFiles[index2].name) {
            remove = 1;
            break;
          }
        }

        if (remove === 0) {
          prevFiles.push(uploadFile[index]);
        }
      }

      prevFiles.map((file) => {
        file.checked = false;
        newFiles.push(file);
      });
    } else {
      uploadFile.map((file) => {
        file.checked = false;
        newFiles.push(file);
      });

      console.log("acceptedFiles", acceptedFiles);
      console.log("newFiles", newFiles);
    }

    this.setState({
      files: newFiles,
      // filesHtml: fileListHtml
    });
    console.log("newFiles", newFiles);

    this.setState({
      rejectedFile: rejectedFiles,
    });
    // }
  };

  
  render() {
    const {
      sold_to_party
    } = this.state;
    if (this.state.showLoader) {
      return <Loader />;
      // return <h2>Loading....</h2>
    } else {
      return (
        <>
          <div className="container-fluid clearfix formSec">
            <div className="dashboard-content-sec">
              <div className="service-request-form-sec newFormSec stockWrapper">
                <div className="form-page-title-block">
                    <h2>Edit Stock Enquiry</h2>
                    <Link
                      to="/my_enquiries"
                      className="backLink"
                      id="showHistory"
                    >
                      Back
                    </Link>
                  </div>

          <Formik
            //initialValues={newInitialValues}
            validationSchema={validationSchema}
            onSubmit={this.handleSubmit}
          >
            {({
              values,
              errors,
              touched,
              setErrors,
              setFieldValue,
              setFieldTouched,
            }) => {
              return (
                <Form>
                    <div
                      className="messageserror"
                      style={{
                        display:
                          (errors.sold_to_party || touched.sold_to_party)
                            ? "block"
                            : "none",
                      }}
                    >
                      <Link to="#" className="close">
                        <img
                          src={closeIcon}
                          width="17.69px"
                          height="22px"
                          onClick={(e) => this.removeError(setErrors)}
                        />
                      </Link>
                      <div>
                        <ul className="">
                          {errors.sold_to_party ? (
                            <li className="">{errors.sold_to_party}</li>
                          ) : null}                          
                        </ul>
                      </div>
                    </div>
                    <div className="row">
                    <div className="col-md-12"><label>Please select Sold to Party to proceed</label></div>
                      <div className="col-md-12">
                        <Select
                          value={sold_to_party}
                          options={sold_to_party_list}
                          isSearchable={true}
                          isClearable={true}
                          placeholder="Sold To Party *"
                          className=""
                          onChange={(e) => {
                            if (e === null || e === "") {
                              setFieldValue("sold_to_party", "");
                              this.setState({ sold_to_party: "" });
                            } else {
                              setFieldValue("sold_to_party", e.value);
                              this.setState({ sold_to_party: e });
                            }
                          }}
                        />
                      </div>
                    </div>
                    <div className="row">
                    <div className="col-md-12"><label>Attach PO</label></div>
                      <div className="col-md-6">
                        <Dropzone
                          onDrop={(acceptedFiles) =>
                            this.setDropZoneFiles(acceptedFiles)
                          }
                        >
                          {({ getRootProps, getInputProps }) => (
                            <section>
                              <div {...getRootProps()} className="customFile">
                                <input
                                  {...getInputProps()}
                                  ref={this.inputRef}
                                  style={{ display: "block", float: "left" }}
                                  className="mt-0"
                                />
                              </div>
                            </section>
                          )}
                        </Dropzone>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col text-center mb-5">
                        {/* <Link to="#" className=""> */}
                          <button
                            className="btn btn-default"
                            style={{
                              width: "auto",
                            }}
                          >
                            Upload P.O and proceed
                          </button>
                        {/* </Link> */}
                        <span
                          className="btn"
                          style={{
                            marginLeft: "1%",
                            paddingTop: "2%",
                          }}
                        >
                        </span>                        
                      </div>
                    </div>                
                </Form>
              );
            }}
            </Formik>
          </div>
            </div>
            </div>
        </>
      );
    }
  }
}

const mapStateToProps = (state) => {
  return {
    dynamic_lang: state.auth.dynamic_lang,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(EditStockEnquiryOld);
