import React, { Component } from "react";
import { Link } from "react-router-dom";
import Layout from "../Layout/layout";
import axios from "../../shared/axios";
import closeIconSuccess from "../../assets/images/times-solid-green.svg";
import { htmlDecode, BasicUserData } from "../../shared/helper";
import Select from "react-select";
import { showErrorMessage } from "../../shared/handle_error";
import whitelogo from "../../assets/images/info-circle-solid.svg";
import closeIconSelect from "../../assets/images/close.svg";

import { connect } from "react-redux";
import { fetchUserData, setUserCompany } from "../../store/actions/user";
import { hideRatingPopup,hideExplicitRatingPopup } from "../../store/actions/auth";

import Rating from "react-rating";
import {
  Row,
  ButtonToolbar,
  Button,
  Modal,
  Col,
  OverlayTrigger,
} from "react-bootstrap";
import { Formik, Form } from "formik";
import { Tooltip } from "reactstrap";

var message = "";

class Dashboard extends Component {
  constructor(props) {
    super(props);
    message = this.props.location.state
      ? this.props.location.state.message
      : "";
  }

  state = {
    companyList: [],
    dashboardData: null,
    dynamic_lang: "",
    isLoading: true,
    task_id: 0,
    task_ref: "",
    req_name: "",
    rating_err: "",
    showRRPopup: false,
    showPendingPopup: false,
    showThankyouPopup: false,
    pendingRatings: false,
    task_rating: 0,
    options_text: "",
    question_text: "",
    ratingHtml: "",
    ratingHtml2: "",
    selOptions: [],
    tooltipOpen2: false,
    pending_requests: [],
    ratingComment: "",
    popup_lang: {},
  };

  componentDidMount() {
    let userData = BasicUserData();

    if (userData) {
      this.props.fetchUser(userData, () => {
        if (this.props.show_rating_popup) {
          //alert('SHOW DYNAMIC POPUP');
          axios
            .get("tasks/pending-rating-dashboard")
            .then((res) => {
              this.showRatingPopup(
                res.data.data[0].task_id,
                res.data.data[0].task_ref,
                res.data.data[0].req_name,
                res.data.data[0].task_language
              );
              this.props.hideRating();
            })
            .catch((err) => {
              this.props.hideRating();
            });
        }

        if (this.props.show_explicit_rating_popup) {
          //alert('SHOW DYNAMIC POPUP');
          axios
            .get(`tasks/pending-rating-dashboard-explicit/${this.props.task_id_cypher}`)
            .then((res) => {
              this.showRatingPopup(
                res.data.data[0].task_id,
                res.data.data[0].task_ref,
                res.data.data[0].req_name,
                res.data.data[0].task_language
              );
              this.props.hideExplicitRating();
            })
            .catch((err) => {
              this.props.hideExplicitRating();
            });
        }

        this.setState({
          companyList: this.props.company,
          dashboardData: this.props.dashboardData,
          isLoading: false,
        });
      });
    }

    if (this.props.selCompany !== null && this.props.selCompany !== undefined) {
      this.props.setCompany(this.props.selCompany);
    }

    if (message !== null) {
      this.props.history.push({
        pathname: "",
        state: "",
      });
    }
    document.title =
      this.props.dynamic_lang.dashboard.title + "| Dr. Reddy's API"; 
  }

  componentDidUpdate(prevProps) {
    if (this.props.dashboardData !== prevProps.dashboardData) {
      this.setState({ dashboardData: this.props.dashboardData });
    }
  }

  hideSuccessMsg = () => {
    message = "";
  };

  toggle2 = () => {
    this.setState({
      tooltipOpen2: !this.state.tooltipOpen2,
    });
  };

  getRatings = (request) => {
    // console.log(request.rating_skipped, request.rating);

    if (request.close_status == 1) {
      if (request.rating_skipped == 0) {
        if (request.rating > 0) {
          return (
            <Rating
              emptySymbol={
                <img
                  src={require("../../assets/images/uncheck-star.svg")}
                  alt=""
                  className="icon"
                />
              }
              placeholderSymbol={
                <img
                  src={require("../../assets/images/uncheck-stars.svg")}
                  alt=""
                  className="icon"
                />
              }
              fullSymbol={
                <img
                  src={require("../../assets/images/uncheck-stars.svg")}
                  alt=""
                  className="icon"
                />
              }
              initialRating={request.rating}
              readonly
            />
          );
        } else {
          return (
            <a
              title={this.props.dynamic_lang.my_requests.share_feedback}
              href="#"
              onClick={(e) =>
                this.showRatingPopup(
                  request.task_id,
                  request.task_ref,
                  request.req_name,
                  request.task_language
                )
              }
            >
              {this.props.dynamic_lang.my_requests.feedback}
            </a>
          );
        }
      } else {
        return (
          <>
            <a
              title={this.props.dynamic_lang.my_requests.share_feedback}
              href="#"
              onClick={(e) =>
                this.showRatingPopup(
                  request.task_id,
                  request.task_ref,
                  request.req_name,
                  request.task_language
                )
              }
            >
              <Rating
                emptySymbol={
                  <img
                    src={require("../../assets/images/uncheck-star.svg")}
                    alt=""
                    className="icon"
                  />
                }
                placeholderSymbol={
                  <img
                    src={require("../../assets/images/uncheck-stars.svg")}
                    alt=""
                    className="icon"
                  />
                }
                fullSymbol={
                  <img
                    src={require("../../assets/images/uncheck-stars.svg")}
                    alt=""
                    className="icon"
                  />
                }
                initialRating={0}
                readonly
              />
            </a>
          </>
        );
      }
    } else {
      return null;
    }
  };

  showRatingPopup = (task_id, task_ref, req_name, lang) => {
    let lang_data = require(`../../assets/lang/${lang}.json`);
    this.setState({ popup_lang: lang_data }, () => {
      this.setState({
        optionsArr: [],
        questionsArr: [],
        showPendingPopup: false,
        showThankyouPopup: false,
        pendingRatings: false,
      });
      axios
        .get(`tasks/rating-options/${task_id}`)
        .then((res) => {
          // console.log("options", res.data.options);
          // console.log("questions", res.data.questions);
          this.setState(
            {
              optionsArr: res.data.options,
              questionsArr: res.data.questions,
              task_rating: 0,
              task_id: task_id,
              task_ref: task_ref,
              req_name: req_name,
            },
            () => {
              this.setQuestion();
            }
          );
        })
        .catch((err) => {});
    });
  };

  setQuestion = () => {
    let question = this.state.popup_lang.my_requests.initial_review_text;
    let options = "";
    let html2 = "";
    for (let index = 0; index < this.state.questionsArr.length; index++) {
      const element = this.state.questionsArr[index];

      let rate = element.rating.split(",");
      if (rate.length == 1) {
        if (rate[0] == this.state.task_rating) {
          question = element.name;
          break;
        }
      } else {
        for (let index = 0; index < rate.length; index++) {
          if (rate[index] == this.state.task_rating) {
            question = element.name;
            break;
          }
        }
        break;
      }
    }

    let html = question;

    this.setState({
      ratingHtml: html,
      ratingHtml2: html2,
      showRRPopup: true,
      rating_err: "",
    });
  };

  changeQuestion = (submitted_rating) => {
    let question = "";
    let options = [];
    let html2 = "";

    for (let index = 0; index < this.state.questionsArr.length; index++) {
      const element = this.state.questionsArr[index];

      let rate = element.rating.split(",");
      if (rate.length == 1) {
        if (rate[0] == submitted_rating) {
          question = element.name;
          break;
        }
      } else {
        for (let index = 0; index < rate.length; index++) {
          if (rate[index] == submitted_rating) {
            question = element.name;
            break;
          }
        }
        //break;
      }
    }

    for (let index = 0; index < this.state.optionsArr.length; index++) {
      const element = this.state.optionsArr[index];

      let rate = element.rating.split(",");
      if (rate.length == 1) {
        if (rate[0] == submitted_rating) {
          options.push(
            <>
              <Col sm={6}>
                <label className="customCheckBoxbtn">
                  <input
                    key={index}
                    type="checkbox"
                    onClick={(e) => {
                      let prev_state = this.state.selOptions;
                      if (e.target.checked) {
                        prev_state.push(element.o_id);
                      } else {
                        const index = prev_state.indexOf(element.o_id);
                        if (index > -1) {
                          prev_state.splice(index, 1);
                        }
                      }
                      this.setState({ selOptions: prev_state });
                    }}
                  />
                  <span className="checkmark ">{htmlDecode(element.name)}</span>
                </label>
              </Col>
            </>
          );
        }
      } else {
        for (let index = 0; index < rate.length; index++) {
          if (rate[index] == submitted_rating) {
            options.push(
              <>
                <Col sm={6}>
                  <label className="customCheckBoxbtn">
                    <input
                      key={index}
                      type="checkbox"
                      onClick={(e) => {
                        let prev_state = this.state.selOptions;
                        if (e.target.checked) {
                          prev_state.push(element.o_id);
                        } else {
                          const index = prev_state.indexOf(element.o_id);
                          if (index > -1) {
                            prev_state.splice(index, 1);
                          }
                        }
                        this.setState({ selOptions: prev_state });
                      }}
                    />
                    <span className="checkmark ">
                      {htmlDecode(element.name)}
                    </span>
                  </label>
                </Col>
              </>
            );
            break;
          }
        }
      }
    }
    html2 = options;

    let html = question;

    this.setState({
      ratingHtml: html,
      ratingHtml2: html2,
      task_rating: submitted_rating,
      rating_err: "",
    });
  };

  handleRatings = (values, actions) => {
    if (this.state.task_rating === 0) {
      this.setState({
        rating_err: this.state.popup_lang.display_error.form_error
          .ratings_before_submitting,
      });
      return false;
    } else {
      this.setState({ showLoader: true });
      axios
        .post("tasks/post-ratings", {
          task_id: this.state.task_id,
          rating: this.state.task_rating,
          comment: this.state.ratingComment.trim(),
          options: this.state.selOptions,
        })
        .then((res) => {
          this.setState({
            optionsArr: [],
            selOptions: [],
            questionsArr: [],
            showRRPopup: false,
            rating_err: "",
            ratingComment: "",
            ratingHtml: "",
            ratingHtml2: "",
            task_rating: 0,
            task_id: 0,
            task_ref: "",
            req_name: "",
            showLoader: false,
          });
          this.openThankyouPopup();
        })
        .catch((err) => {});
    }
  };

  handleRRClose = (e) => {
    this.setState({ showLoader: true });
    e.preventDefault();

    axios
      .post("tasks/skip-ratings", {
        task_id: this.state.task_id,
      })
      .then((res) => {
        this.setState({
          optionsArr: [],
          selOptions: [],
          questionsArr: [],
          showRRPopup: false,
          ratingComment: "",
          ratingHtml: "",
          ratingHtml2: "",
          task_rating: 0,
          task_id: 0,
          task_ref: "",
          req_name: "",
          showLoader: false,
          rating_err: "",
        });

        this.getRequests();
        this.props.history.push({
          pathname: "/my_requests",
        });
      })
      .catch((err) => {});
  };

  openPendingPopup = (e) => {
    this.setState({ showThankyouPopup: false });
    e.preventDefault();
    axios
      .get("tasks/pending-rating-dashboard", {
        task_id: this.state.task_id,
      })
      .then((res) => {
        if (res.data.single_entry === 1) {
          this.showRatingPopup(
            res.data.data[0].task_id,
            res.data.data[0].task_ref,
            res.data.data[0].req_name,
            res.data.data[0].task_language
          );
        } else {
          this.setState({ pending_requests: res.data.data }, () => {
            this.setState({ showPendingPopup: true });
          });
        }
      })
      .catch((err) => {});
  };

  handlePendingClose = (e) => {
    //e.preventDefault();
    this.setState({ showPendingPopup: false });
  };

  openThankyouPopup = () => {
    axios
      .get("tasks/count-pending-rating-dashboard")
      .then((res) => {
        if (res.data.count > 0) {
          this.setState({ showThankyouPopup: true, pendingRatings: true });
        } else {
          this.setState({ showThankyouPopup: true, pendingRatings: false });
        }
      })
      .catch((err) => {});
  };

  handleThankyouClose = (e) => {
    //e.preventDefault();
    this.setState({ showThankyouPopup: false });
  };

  getLanguagePending = (pending) => {
    let lang_data = require(`../../assets/lang/${pending.task_language}.json`);

    return (
      <>
        <p>
          {lang_data.my_requests.task_ref} <strong> {pending.task_ref} </strong>
        </p>
        <p>
          {lang_data.my_requests.description}
          <strong> {pending.req_name}</strong>
        </p>
      </>
    );
  };

 getPendingText = (pending) => {
  let lang_data = require(`../../assets/lang/${pending.task_language}.json`);
  return lang_data.my_requests.rate_now
 }

  render() {
    const { tooltipOpen2 } = this.state;
    return (
      <>
        <div className="container-fluid clearfix">
          <div className="dashboard-content-sec">
            <div>
              {message !== null && message !== "" && (
                <div className="messagessuccess">
                  <Link to="#" className="close">
                    <img
                      src={closeIconSuccess}
                      width="17.69px"
                      height="22px"
                      onClick={(e) => this.hideSuccessMsg()}
                    />
                  </Link>
                  {message}
                </div>
              )}
              <div className="block block-core block-page-title-block">
                <h2>{this.props.dynamic_lang.dashboard.title}</h2>
              </div>

              <div
                id="block-reddy-admin-content"
                className="block block-system block-system-main-block"
              >
                <div className="content">
                  <div className="row clearfix">
                    <div className="col-lg-3 col-md-6 col-sm-12">
                      <div className="dashboard-menu-box">
                        <div className="dashboard-info-box">
                          <Link to="/my_products">
                            {/* <a href={`${process.env.REACT_APP_PORTAL_URL}products`} target='_blank'> */}
                            <img
                              src={require("../../assets/images/dashboard-icon-1.png")}
                            />
                            <h3>
                              {this.props.dynamic_lang.dashboard.m_products}
                            </h3>
                            {/* </a>   */}
                          </Link>
                        </div>
                        <div className="dashboard-info-content">
                          <p>
                            {this.props.dynamic_lang.dashboard.m_products}{" "}
                            <strong>
                              {this.state.dashboardData &&
                                this.state.dashboardData.product}
                            </strong>{" "}
                            {/* <strong>0</strong>{" "} */}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-12">
                      <div className="dashboard-menu-box">
                        <div className="dashboard-info-box">
                          <Link to="/my_requests">
                            <img
                              src={require("../../assets/images/dashboard-icon-2.png")}
                            />
                            <h3>
                              {this.props.dynamic_lang.dashboard.m_requests}
                            </h3>
                          </Link>
                        </div>
                        <div className="dashboard-info-content">
                          <p>
                            {this.props.dynamic_lang.dashboard.s_requests}{" "}
                            <strong>
                              {this.state.dashboardData &&
                                this.state.dashboardData.request}
                            </strong>{" "}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-12">
                      <div className="dashboard-menu-box">
                        <div className="dashboard-info-box">
                          <Link to="/my_orders">
                            <img
                              src={require("../../assets/images/dashboard-icon-7.png")}
                            />
                            <h3>
                              {this.props.dynamic_lang.dashboard.m_orders}
                            </h3>
                          </Link>
                        </div>
                        <div className="dashboard-info-content">
                          <p>
                            {this.props.dynamic_lang.dashboard.orders}{" "}
                            <strong>
                              {this.state.dashboardData &&
                                this.state.dashboardData.orders}
                            </strong>{" "}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-12">
                      <div className="dashboard-menu-box">
                        <div className="dashboard-info-box">
                          <Link to="/my_complaints">
                            <img
                              src={require("../../assets/images/dashboard-icon-6.png")}
                            />
                            <h3>
                              {this.props.dynamic_lang.dashboard.m_complaints}
                            </h3>
                          </Link>
                        </div>
                        <div className="dashboard-info-content">
                          <p>
                            {this.props.dynamic_lang.dashboard.complaints}{" "}
                            <strong>
                              {this.state.dashboardData &&
                                this.state.dashboardData.complaints}
                            </strong>{" "}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-12">
                      <div className="dashboard-menu-box">
                        <div className="dashboard-info-box">
                          <Link to="/my_forecast">
                            <img
                              src={require("../../assets/images/dashboard-icon-5.png")}
                            />
                            <h3>
                              {this.props.dynamic_lang.dashboard.m_forecasts}
                            </h3>
                          </Link>
                        </div>
                        <div className="dashboard-info-content">
                          <p>
                            {this.props.dynamic_lang.dashboard.forecasts}{" "}
                            <strong>
                              {this.state.dashboardData &&
                                this.state.dashboardData.forecasts}
                            </strong>{" "}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-12">
                      <div className="dashboard-menu-box">
                        <div className="dashboard-info-box">
                          <Link to="/coming-soon">
                            <img
                              src={require("../../assets/images/dashboard-icon-9.png")}
                            />
                            <h3>
                              {
                                this.props.dynamic_lang.dashboard
                                  .r_notifications
                              }
                            </h3>
                          </Link>
                        </div>
                        <div className="dashboard-info-content">
                          <p>
                            {this.props.dynamic_lang.dashboard.u_notifications}
                            <strong>
                              {this.state.dashboardData &&
                                this.state.dashboardData.notification}
                            </strong>{" "}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-12">
                      <div className="dashboard-menu-box">
                        <div className="dashboard-info-box">
                          <Link to="/my_payments">
                            <img
                              src={require("../../assets/images/dashboard-icon-8.png")}
                            />
                            <h3>
                              {this.props.dynamic_lang.dashboard.m_payments}
                            </h3>
                          </Link>
                        </div>
                        <div className="dashboard-info-content">
                          <p>
                            {this.props.dynamic_lang.dashboard.p_payments}{" "}
                            <strong>{this.state.dashboardData && this.state.dashboardData.payment}</strong>{" "}
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-3 col-md-6 col-sm-12">
                      <div className="dashboard-menu-box">
                        <div className="dashboard-info-box">
                          <Link to="/coming-soon">
                            <img
                              src={require("../../assets/images/dashboard-icon-3.png")}
                            />
                            <h3>
                              {this.props.dynamic_lang.dashboard.a_discussions}
                            </h3>
                          </Link>
                        </div>
                        <div className="dashboard-info-content">
                          <p>
                            {this.props.dynamic_lang.dashboard.r_discussions}{" "}
                            <strong>
                              {/* {this.state.dashboardData && this.state.dashboardData.discussion} */}
                            </strong>{" "}
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* RATING POPUP */}
        {this.state.showRRPopup === true && (
          <Modal
            show={this.state.showRRPopup}
            onHide={this.handleRRClose}
            backdrop="static"
            className="modalRating"
          >
            <Modal.Header>
              <div className="cntrimg">
                <img src={require("../../assets/images/rat-2.svg")} alt="" />
              </div>

              {/* <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Empty </Tooltip>}>
                <ButtonToolbar>
               
                </ButtonToolbar>
              </OverlayTrigger> */}

              <img
                src={closeIconSelect}
                width="15.44"
                height="18"
                id="DisabledAutoHideExample"
                type="button"
                onClick={(e) => this.handleRRClose(e)}
                className="close"
              />
              <Tooltip
                placement="left"
                isOpen={tooltipOpen2}
                autohide={false}
                target="DisabledAutoHideExample"
                toggle={this.toggle2}
              >
                {this.state.popup_lang.my_requests.give_rating}
              </Tooltip>

              {/* <Button
                className="close"
                onClick={(e) => this.handleRRClose(e)}
              /> */}
            </Modal.Header>
            <Formik
              initialValues={false}
              validationSchema={false}
              onSubmit={this.handleRatings}
            >
              {({
                values,
                errors,
                touched,
                isValid,
                isSubmitting,
                handleChange,
                setFieldValue,
                setFieldTouched,
                setErrors,
              }) => {
                return (
                  <Form>
                    <Modal.Body>
                      <Modal.Title>
                        <div className="ratinfo">
                          <h4>
                            {
                              this.state.popup_lang.my_requests
                                .overall_experience
                            }{" "}
                            {this.state.req_name}-{" "}
                            <Link
                              to={{
                                pathname: `/task-details/${this.state.task_id}`,
                              }}
                              style={{
                                cursor: "pointer",
                                color: "#0056b3",
                                textDecoration: "underline",
                              }}
                            >
                              {this.state.task_ref}{" "}
                            </Link>
                          </h4>
                          <h3></h3>
                        </div>
                      </Modal.Title>
                      <div className="ratinfo starimg">
                        <Rating
                          initialRating={this.state.task_rating}
                          onChange={(rate) => this.changeQuestion(rate)}
                          emptySymbol={
                            <img
                              src={require("../../assets/images/uncheck-star.svg")}
                              alt=""
                              className="icon"
                            />
                          }
                          placeholderSymbol={
                            <img
                              src={require("../../assets/images/uncheck-stars.svg")}
                              alt=""
                              className="icon"
                            />
                          }
                          fullSymbol={
                            <img
                              src={require("../../assets/images/uncheck-stars.svg")}
                              alt=""
                              className="icon"
                            />
                          }
                        />
                        {this.state.rating_err && (
                          <p
                            style={{
                              color: "red",
                              fontSize: "14px",
                              marginBottom: "0px",
                              marginTop: "4px",
                            }}
                          >
                            {this.state.popup_lang.my_requests.forgot_to_rate}
                          </p>
                        )}
                        <textarea
                          className="inputratng"
                          placeholder={htmlDecode(this.state.ratingHtml)}
                          onChange={(e) => {
                            this.setState({ ratingComment: e.target.value });
                          }}
                        ></textarea>
                        <br />
                        <Row className="checkmargin">
                          {this.state.ratingHtml2}
                        </Row>

                        <ButtonToolbar className="txtcntr">
                          <Button
                            className={` submit m-b-0 btn-fill ${
                              isValid ? "btn-custom-green" : "btn-disable"
                            } m-r-10`}
                            type="submit"
                            //disabled={isValid ? false : true}
                          >
                            {this.state.popup_lang.my_requests.submit}
                          </Button>
                        </ButtonToolbar>
                      </div>
                    </Modal.Body>
                    {/* <Modal.Footer>
                        
                      </Modal.Footer> */}
                  </Form>
                );
              }}
            </Formik>
          </Modal>
        )}

        {/* PENDING POPUP */}
        {this.state.showPendingPopup === true && (
          <Modal
            show={this.state.showPendingPopup}
            onHide={this.handlePendingClose}
            backdrop="static"
            className="modlscroll"
          >
            <Modal.Header>
              <div className="cntrimg">
                <img src={require("../../assets/images/rat-2.svg")} alt="" />
              </div>

              <img
                src={closeIconSelect}
                width="15.44"
                height="18"
                id="DisabledAutoHideExample"
                type="button"
                onClick={(e) => this.handlePendingClose(e)}
                className="close"
              />
              <Tooltip
                placement="left"
                isOpen={tooltipOpen2}
                autohide={false}
                target="DisabledAutoHideExample"
                toggle={this.toggle2}
              >
                {this.props.dynamic_lang.my_requests.give_rating}
              </Tooltip>

              {/* <button
                className="close"
                onClick={(e) => this.handlePendingClose(e)}
              /> */}
            </Modal.Header>

            <Modal.Body>
              <div className="content-wrapper">
                <section className="content-header">
                  <div>
                    <div>
                      {/* <div className="d-flex justify-content-between"> */}
                      <Row>
                        <Col sm={6}>
                          <div className="past">
                            <p>
                              {
                                this.props.dynamic_lang.my_requests
                                  .pending_feedback
                              }{" "}
                              <span className="bedge">
                                {this.state.pending_requests.length}
                              </span>
                            </p>
                          </div>
                        </Col>
                        {/* <Col sm={4}>
                          <div className="feedbck">
                            {" "}
                            <p>
                              Feedback Due{" "}
                              <span className="bedge">
                                {this.state.pending_requests.length}
                              </span>
                            </p>
                          </div>
                        </Col> */}
                      </Row>
                      {/* </div> */}
                      {/* <div className="revbox">
                                    <div className="p-d-15"> */}
                      {this.state.pending_requests.map((pending, i) => {
                        return (
                          <div className="revbox">
                            <div className="p-d-15">
                              <Row>
                                <Col sm={8}>
                                  <div className="enqnmbr">
                                    {this.getLanguagePending(pending)}
                                  </div>
                                </Col>
                                <Col sm={4}>
                                  {/* <div className="pndngfeedbck"> */}
                                  <div className="pndng">
                                    {/* <p>Pending feedback 
                                                      <OverlayTrigger overlay={<Tooltip id="tooltip-disabled">Empty</Tooltip>}>
                                                          <a href="#" className="infoIcon"><img src={require('../../assets/images/tooltip-i.svg')} alt=""
                                                          className="m-l-5"/></a>
                                                          </OverlayTrigger>
                                                      </p> */}
                                    <Link
                                      className="ratebtn"
                                      to="#"
                                      onClick={(e) =>
                                        this.showRatingPopup(
                                          pending.task_id,
                                          pending.task_ref,
                                          pending.req_name,
                                          pending.task_language
                                        )
                                      }
                                    >
                                      {this.getPendingText(pending)}
                                      
                                    </Link>
                                  </div>
                                  {/* </div> */}
                                </Col>
                              </Row>
                            </div>
                          </div>
                        );
                      })}
                      ;{/* </div>
                                </div> */}
                    </div>
                  </div>
                </section>
              </div>
            </Modal.Body>
          </Modal>
        )}

        {/* THANK YOU POPUP */}
        {this.state.showThankyouPopup === true && (
          <Modal
            show={this.state.showThankyouPopup}
            onHide={this.handleThankyouClose}
            backdrop="static"
            className="modalthnx"
          >
            <Modal.Header>
              <div className="thankuhead">
                <img
                  src={require("../../assets/images/thank-You-1.svg")}
                  alt=""
                />
                <button
                  className="close"
                  onClick={(e) => this.handleThankyouClose(e)}
                />
                <p className="thankutxt">
                  {this.state.popup_lang.my_requests.thank_you}
                </p>
              </div>
            </Modal.Header>

            <Modal.Body>
              <div className="infopara">
                <p>{this.state.popup_lang.my_requests.sharing_feedback}</p>
                <p>{this.state.popup_lang.my_requests.overall_process}</p>
                {this.state.pendingRatings && (
                  <>
                    <p className="txtcntr">
                      {this.state.popup_lang.my_requests.pending_reviews}
                    </p>
                    <div className="d-flex justify-content-center infopara">
                      <Link to="#" onClick={(e) => this.openPendingPopup(e)}>
                        {this.state.popup_lang.my_requests.share_your_feedback}
                      </Link>
                    </div>
                  </>
                )}
              </div>
            </Modal.Body>
          </Modal>
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dashboardData: state.user.dashboardData,
    company: state.user.company,
    dynamic_lang: state.auth.dynamic_lang,
    show_rating_popup: state.auth.show_rating_popup,
    task_id_cypher: state.auth.task_id_cypher,
    show_explicit_rating_popup: state.auth.show_explicit_rating_popup,
    
    selCompany: state.user.selCompany,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchUser: (data, onSuccess) => dispatch(fetchUserData(data, onSuccess)),
    setCompany: (data, onSuccess) => dispatch(setUserCompany(data, onSuccess)),
    hideRating: () => dispatch(hideRatingPopup()),
    hideExplicitRating: () => dispatch(hideExplicitRatingPopup()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
