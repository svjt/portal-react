import React, { Component } from 'react';
import Layout from '../Layout/layout';
import axios from "../../shared/axios";

class MyPayments extends Component {

    state = {
        requestsList: []
    };
    componentDidMount() {
        this.getRequests();
    }

    getRequests() {
        axios
            .get("/request-list")
            .then(res => {
                console.log('data',res.data.data)
                this.setState({ requestsList: res.data.data });
            })
            .catch(err => { });
    }
    render() {
        console.log('Requests', this.state.requestsList)
        return (
                <div className="container-fluid clearfix">
                    <div className="dashboard-content-sec">
                        <div>
                            <div className="block block-core block-page-title-block">
                                <h2>My Payments</h2>
                            </div>
                            <table>
                                <thead>
                                    <tr>
                                        <th>Order Number</th>
                                        <th>Sales Order</th>
                                        <th>Invoice</th>
                                        <th>Request Date of Delivery</th>
                                        <th>SO Amount</th>
                                        <th>Order Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.state.requestsList.map((request, key) => (
                                        <tr key={key}>                                  
                                            <td><a href='#'>{request.task_ref}</a></td>
                                            <td>{request.title}</td>
                                            <td>Abacavir Base</td>
                                            <td>17 Jul 2019</td>
                                            <td>{request.first_name} {request.last_name}</td>
                                            <td>26 Jun 2019</td>
                                            <td>In Progress</td>                                  
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        );
    }
}

export default MyPayments;