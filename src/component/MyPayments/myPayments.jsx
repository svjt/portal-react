import React, { Component } from 'react';
import { Link } from "react-router-dom";
import Pagination from "react-js-pagination";
import { Button } from "react-bootstrap";
// import Layout from '../Layout/layout';
import Loader from "../Loader/loader";
import axios from "../../shared/axios";
import { connect } from "react-redux";
import { isMobile } from "react-device-detect";
import dateFormat from "dateformat";
import DatePicker from "react-datepicker";
import Autosuggest from "react-autosuggest";
import "react-datepicker/dist/react-datepicker.css";
import closeIconSelect from "../../assets/images/close.svg";
import { htmlDecode, BasicUserData } from "../../shared/helper";

let message = "";

class MyPayments extends Component {
    constructor(props) {
        super(props);
        message = this.props.location.state
            ? this.props.location.state.message
            : "";
    };
    state = {
        paymentList: [],
        activePage: 1,
        count_req: 0,
        itemPerPage: 10,
        selectedCompany: null,
        value: "", // for auto-suggest
        suggestions: [], // for auto-suggest
        searchkey: "",
        query_string: "",
        selectedFrom: "",
        selectedTo: "",
        isLoading: false
    };
    componentDidMount() {
        document.title = this.props.dynamic_lang.dashboard.m_payments + " | Dr.Reddys API";
        if (isMobile) {
            const datePickers = document.getElementsByClassName(
                "react-datepicker__input-container"
            );
            Array.from(datePickers).forEach((el) =>
                el.childNodes[0].setAttribute("readOnly", true)
            );
        }
        if (message !== null) {
            this.props.history.push({
                pathname: "",
                state: "",
            });
        }
        let userData = BasicUserData();
        if (userData) {
            this.setState({
                isLoading: false,
            });
        }
        this.getPayment();
    }

    getPayment(page = 1, search = "", sel_evnt = null, manual_search = "") {
        this.setState({
            isLoading: true,
        });
        let userData = BasicUserData();
        if (userData.role === 2) {
            if (sel_evnt != null) {
                this.setState({ selectedCompany: sel_evnt });
            }

            if (this.props.selCompany && this.props.selCompany.value > 0) {
                let url = `/payment-list?cid=${this.props.selCompany.value}&page=${page}`;

                if (search != "") {
                    url += `&s=${search}`;
                }
                if (manual_search != "") {
                    url += `&${manual_search}`;
                }

                axios
                    .get(url)
                    .then((res) => {
                        this.setState({
                            paymentList: res.data.data,
                            count_req: res.data.count,
                            isLoading: false,
                        });
                    })
                    .catch((err) => { });
            } else {
                this.setState({ isLoading: false });
            }
        } else {
            let url = `/payment-list?page=${page}`;
            if (search != "") {
                url += `&s=${search}`;
            }
            if (manual_search != "") {
                url += `&${manual_search}`;
            }
            axios
                .get(url)
                .then((res) => {
                    this.setState({
                        paymentList: res.data.data,
                        count_req: res.data.count,
                        isLoading: false,
                    });
                })
                .catch((err) => { });
        }
    };

    handlePageChange = (pageNumber) => {
        this.setState({ activePage: pageNumber });
        var searchValue = "";
        if (this.state.searchkey != "") searchValue = this.state.searchkey;
        this.getPayment(
            pageNumber > 0 ? pageNumber : 1,
            searchValue,
            null,
            this.state.query_string
        );
    };

    searchDate = (e) => {
        let query_string = "";
        if (this.state.selectedFrom !== "" && this.state.selectedFrom !== null) {
            query_string += `fdt=${dateFormat(
                this.state.selectedFrom,
                "yyyy-mm-dd"
            )}&`;
        }
        if (this.state.selectedTo !== "" && this.state.selectedTo !== null) {
            query_string += `tdt=${dateFormat(this.state.selectedTo, "yyyy-mm-dd")}&`;
        }
        if (query_string !== "") {
            query_string = query_string.substring(0, query_string.length - 1);
            this.setState({ query_string: query_string, activePage: 1 });
            this.getPayment(1, this.state.searchkey, null, query_string);
        }
    };

    revertDateSearch = () => {
        this.setState({
            selectedFrom: "",
            selectedTo: "",
            query_string: "",
            activePage: 1,
        });
        this.getPayment(1, this.state.searchkey);
    };

    clearAutoSuggest = () => {
        this.getPayment(1, "", null, this.state.query_string);
        this.setState({ value: "" });
    };

    // ==== Autosuggest for Reference Query DRL ==== //
    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: [],
        });
    };
    // ==== Autosuggest for Reference Query DRL ==== //
    onSuggestionsFetchRequested = ({ value }) => {
        const inputValue = value.toLowerCase();
        const inputLength = inputValue.length;

        if (inputLength === 0) {
            return [];
        } else {
            if (inputLength > 0) {
                let userData = BasicUserData();
                if (userData.role === 2) {
                    if (this.props.selCompany && this.props.selCompany.value > 0) {
                        axios
                            .get(`/payment-search/${value}/?cid=${this.props.selCompany.value}`)
                            .then((res) => {
                                this.setState({
                                    suggestions: res.data.data,
                                });
                                return this.state.suggestions;
                            })
                            .catch((err) => {
                                console.log("==========", err);
                            });
                    }
                } else {
                    axios
                        .get(`/payment-search/${value}`)
                        .then((res) => {
                            this.setState({
                                suggestions: res.data.data,
                            });
                            return this.state.suggestions;
                        })
                        .catch((err) => {
                            console.log("==========", err);
                        });
                }
            }
        }
    };
    getSuggestionValue = (suggestion) => {
        return suggestion.label;
    };

    renderSuggestion = (suggestion) => {
        return (
            <div>
                <Link to="#">{suggestion.label}</Link>
            </div>
        );
    };

    enterPressed = (event) => {
        var code = event.keyCode || event.which;
        if (code === 13) {
            //13 is the enter keycode
            this.setState({
                searchkey: event.target.value,
            });
            this.getPayment(1, event.target.value, null, this.state.query_string);
        }
    };

    // ==== Autosuggest for Reference Query DRL ==== //
    onReferenceChange = (event, { newValue }) => {
        this.setState(
            {
                value: newValue,
            },
            () => console.log("entered", this.state.value)
        );
    };

    render() {
        const {
            selectedCompany,
            value,
            suggestions
        } = this.state;
        const inputProps = {
            placeholder: this.props.dynamic_lang.my_payment.keywords_attachments,
            value,
            type: "search",
            onChange: this.onReferenceChange,
            onKeyPress: this.enterPressed.bind(this),
            className: this.state.value.length > 0 ? "customInputLessBg" : "customInput",
        };
        if (this.state.isLoading === true) {
            return (
                <>
                    <div className="loginLoader">
                        <div className="lds-spinner">
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <div></div>
                            <span>Please Wait…</span>
                        </div>
                    </div>
                </>
            );
        } else {

            return (
                <>
                    <div className="container-fluid clearfix listingSec">
                        <div className="dashboard-content-sec">
                            <div className="col-md-12 form-search-section formSearchrequest">
                                <ul className="mb-0">
                                    <li>
                                        <DatePicker
                                            className="form-control customInput"
                                            name="fromDate"
                                            showMonthDropdown
                                            showYearDropdown
                                            minDate={this.state.fromMaxDate}
                                            maxDate={this.state.toMaxDate}
                                            dropdownMode="select"
                                            onChange={(e) => {
                                                this.setState({ selectedFrom: e });
                                            }}
                                            selected={this.state.selectedFrom}
                                            dateFormat="dd/MM/yyyy"
                                            autoComplete="off"
                                            placeholderText={this.props.dynamic_lang.my_payment.from_date}
                                        />
                                    </li>
                                    <li>
                                        <DatePicker
                                            className="form-control customInput"
                                            showMonthDropdown
                                            showYearDropdown
                                            minDate={this.state.fromMaxDate}
                                            maxDate={this.state.toMaxDate}
                                            dropdownMode="select"
                                            onChange={(e) => {
                                                this.setState({ selectedTo: e });
                                            }}
                                            selected={this.state.selectedTo}
                                            dateFormat="dd/MM/yyyy"
                                            autoComplete="off"
                                            placeholderText={this.props.dynamic_lang.my_payment.to_date}
                                        />
                                    </li>
                                    <li>
                                        <Button
                                            className={`btn-default m-r-10`}
                                            onClick={(e) => this.searchDate(e)}
                                        >
                                            {this.props.dynamic_lang.my_payment.filter}
                                        </Button>

                                        {(this.state.selectedFrom != "" ||
                                            this.state.selectedTo != "") && (
                                                <Button
                                                    className={`btn btn-default btn-cancel`}
                                                    onClick={(e) => this.revertDateSearch(e)}
                                                >
                                                    {this.props.dynamic_lang.my_payment.cancel}
                                                </Button>
                                            )}
                                    </li>
                                </ul>

                            </div>


                            <div className="service-request-form-sec">

                                <div className="row">
                                    <div className="col-md-9 form-page-title2">
                                        <h2>{this.props.dynamic_lang.dashboard.m_payments}</h2>
                                    </div>
                                    <div className="col-md-3 form-search-section formSearchrequest">
                                        <ul className="mb-0">
                                            <li>
                                                <Autosuggest
                                                    suggestions={suggestions}
                                                    onSuggestionsFetchRequested={
                                                        this.onSuggestionsFetchRequested
                                                    }
                                                    onSuggestionsClearRequested={
                                                        this.onSuggestionsClearRequested
                                                    }
                                                    getSuggestionValue={(suggestion) =>
                                                        this.getSuggestionValue(suggestion)
                                                    }
                                                    renderSuggestion={this.renderSuggestion}
                                                    inputProps={inputProps}
                                                />
                                                {this.state.value !== "" && (
                                                    <div className="close-icon-container">
                                                        <a
                                                            className="close-icon"
                                                            onClick={this.clearAutoSuggest}
                                                        >
                                                            <img
                                                                src={closeIconSelect}
                                                                width="10"
                                                                height="10"
                                                            />
                                                        </a>
                                                    </div>
                                                )}
                                            </li>
                                        </ul>
                                    </div>
                                </div>

                                {this.state.isLoading === true ? (
                                    <Loader />
                                ) : (
                                    <div className="listing-table table-responsive orderTable">
                                        <table className="" style={{ width: "100%" }}>
                                            <thead>
                                                <tr>
                                                    <th>{this.props.dynamic_lang.my_payment.customer_name}</th>
                                                    <th>{this.props.dynamic_lang.my_payment.customer_reference}</th>
                                                    <th>{this.props.dynamic_lang.my_payment.cust_ref_date}</th>
                                                    {/* <th>{this.props.dynamic_lang.my_payment.sales_document}</th>
                                                    <th>{this.props.dynamic_lang.my_payment.sales_order_date}</th> */}
                                                    {/* <th>{this.props.dynamic_lang.my_payment.customer_name}</th> */}
                                                    <th>{this.props.dynamic_lang.my_payment.product_name}</th>
                                                    <th>{this.props.dynamic_lang.my_payment.billed_quantity}</th>
                                                    {/* <th>{this.props.dynamic_lang.my_payment.uom}</th> */}
                                                    <th>{this.props.dynamic_lang.my_payment.invoice_no}</th>
                                                    <th>{this.props.dynamic_lang.my_payment.invoice_date}</th>
                                                    <th>{this.props.dynamic_lang.my_payment.invoice_value}</th>
                                                    <th>{this.props.dynamic_lang.my_payment.incoterms}</th>
                                                    <th>{this.props.dynamic_lang.my_payment.payment_terms}</th>
                                                    <th>{this.props.dynamic_lang.my_payment.payment_status}</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {this.state.paymentList.map((payment, key) => (
                                                    <tr key={key}>
                                                        <td>{payment.ref_name}</td>
                                                        <td>{payment.po_number != null && payment.po_number != '' ?
                                                            (
                                                                <Link
                                                                    to={{
                                                                        pathname: `/task-details/${payment.task_id}`,
                                                                        state: { fromDashboardpayment: true },
                                                                    }}
                                                                    style={{ cursor: "pointer" }}
                                                                >{payment.po_number} </Link>) : '-'}</td>
                                                        <td>{payment.po_number != null && payment.po_number != '' ? payment.po_date : '-'}</td>
                                                        {/* <td>{payment.sales_order_no != null && payment.sales_order_no != '' ?
                                                            (
                                                                <Link
                                                                    to={{
                                                                        pathname: `/task-details/${payment.task_id}`,
                                                                        state: { fromDashboardpayment: true },
                                                                    }}
                                                                    style={{ cursor: "pointer" }}
                                                                >{payment.sales_order_no}
                                                                </Link>) : '-'}

                                                        </td>
                                                        <td>{payment.sales_order_no != null && payment.sales_order_no != '' ? payment.sales_order_date : ""}</td> */}
                                                        {/* <td>{payment.customer_name}</td> */}
                                                        <td>{payment.product_name}</td>
                                                        <td>{(payment.billed_quantity!='') ? payment.billed_quantity : '0'}</td>
                                                        {/* <td>{payment.uom}</td> */}
                                                        <td><Link
                                                            to={{
                                                                pathname: `/task-details/${payment.task_id}`,
                                                                state: { fromDashboardpayment: true },
                                                            }}
                                                            style={{ cursor: "pointer" }}
                                                        >{payment.invoice_number}</Link></td>
                                                        <td>{payment.billing_date}</td>
                                                        <td>{(payment.currency!='' && payment.currency!=null) ? payment.currency : "INR"} {(payment.invoice_value!='') ? payment.invoice_value : '0'}</td>
                                                        <td>{payment.incoterms}</td>
                                                        <td>{payment.payment_terms}</td>
                                                        <td>{payment.payment_status}</td>
                                                    </tr>
                                                ))}
                                                {this.state.paymentList.length === 0 && (
                                                    <tr>
                                                        <td colSpan="11" align="center">
                                                            {
                                                                this.props.dynamic_lang.my_payment
                                                                    .no_data_display
                                                            }
                                                        </td>
                                                    </tr>
                                                )}
                                            </tbody>
                                        </table>
                                    </div>
                                )}

                                <div className="clearfix" />
                                {this.state.count_req > this.state.itemPerPage ? (
                                    <div className="pagination-area">
                                        <div className="paginationOuter text-right">
                                            <Pagination
                                                hideDisabled
                                                hideFirstLastPages
                                                prevPageText="‹‹"
                                                nextPageText="››"
                                                activePage={this.state.activePage}
                                                itemsCountPerPage={this.state.itemPerPage}
                                                totalItemsCount={this.state.count_req}
                                                itemClass="nav-item"
                                                linkClass="nav-link"
                                                activeClass="active"
                                                pageRangeDisplayed="1"
                                                onChange={this.handlePageChange}
                                            />
                                        </div>
                                    </div>
                                ) : null}
                                <div className="clearfix" />
                            </div>
                        </div>
                    </div>
                </>
            );
        }
    }
}

const mapStateToProps = (state) => {
    return {
        display_menu: state.menu.display,
        dynamic_lang: state.auth.dynamic_lang,
        selCompany: state.user.selCompany,
    }
}
export default connect(mapStateToProps)(MyPayments);