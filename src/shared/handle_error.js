import swal from "sweetalert";
require('dotenv').config();

export function showErrorMessage(errText,props){

  swal({
    closeOnClickOutside: false,
    title: "Access Denied",
    text: errText,
    icon: "error"
  }).then(()=> {
    window.location.href=`${process.env.PUBLIC_URL}/dashboard`;
  });

}
