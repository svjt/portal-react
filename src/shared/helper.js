//SATYAJIT
import jwt_decode from 'jwt-decode';
import {store} from '../store/createStore';

export function htmlDecode( string ) {
    const Entities = require('html-entities').AllHtmlEntities;
    const entities = new Entities();
    return entities.decode(string);
}

export function inArray(needle, haystack){
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] === needle) return true;
    }
    return false;
}
export function supportedPdfFileType (){
    return [
        "pdf"
    ]
}

export function supportedFileType (){
   return [
        "doc",
        "docx",
        "pdf",
        "txt",
        "odt",
        "rtf",
        "wpd",
        "tex",
        "wks",
        "wps",
        "xls",
        "xlsx",
        "xlr",
        "ods",
        "csv",
        "ppt",
        "pptx",
        "pps",
        "key",
        "odp",
        "ai",
        "bmp",
        "gif",
        "ico",
        "jpeg",
        "jpg",
        "png",
        "svg",
        "tif",
        "tiff",
        "eml",
        "zip"
      ];
}

export function BasicUserData(){
    const current_store = store.getState();
    if(current_store.auth.token){
        let token = jwt_decode(current_store.auth.token);
        let user = {
            "name": token.name,
            "lang": token.lang,
            "email": token.email,
            "profile_pic": token.profile_pic,
            "mobile": token.mobile,
            "choose": token.choose,
            "role" : token.role,
            "since" : token.since
        }
        return user;
    }else{
        return null;
    }
}

export function getUserLanguage(){
    const current_store = store.getState();
    if(current_store.auth.token){
        let token = jwt_decode(current_store.auth.token);
        return token.lang;
    }else{
        return null;
    }
}

export function parseToken( token ){
    let tk = jwt_decode( token );
    return tk.choose;
}

export function checkEmployee (){
    const current_store = store.getState();
    if(current_store.auth.token){
        let token = jwt_decode(current_store.auth.token);
        return token.empe;
    }else{
        return null;
    }
}