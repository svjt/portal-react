import axios from "axios";
import {store} from '../store/createStore';

const instance = axios.create({
  baseURL: process.env.REACT_APP_API_URL
});

// Add a request interceptor

instance.interceptors.request.use(
  config => {
    const current_store = store.getState();
    // Insert authorization token on request call
    const auth_token = current_store.auth.token;
    if (auth_token){
      config.headers["Authorization"] = `Bearer ${auth_token}`;
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);

// Add a response interceptor
instance.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    if(error.response.status === 401){
        localStorage.clear('persist:root');
        //sessionStorage.clear();
        //store.dispatch(authLogout());
        window.location.href = `${process.env.REACT_APP_PORTAL_URL}customer_portal/login`;     // SATYAJIT
    }else{
        //store.dispatch(authLogout());
        return Promise.reject(error.response);
    }
  }
);

export default instance;
