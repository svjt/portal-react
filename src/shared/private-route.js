import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { BasicUserData } from "./helper";

import { createBrowserHistory } from 'history';
var history = createBrowserHistory();

export const PrivateRoute = ({ component: Component, isLoggedIn, ...rest }) => (
    <Route {...rest} render={props => (
            isLoggedIn
                ? <Component {...props} />
                : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        
        )} />
)